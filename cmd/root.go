package cmd

import (
	"os"

	"github.com/powerman/structlog"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"lab.aviproduction.org/aggregator/go-kit/pkg/def"
)

var (
	cfgFile string
	log     = structlog.New(structlog.KeyUnit, "main")
)

var rootCmd = &cobra.Command{
	Use:   "nft-scan",
	Short: "Main data grubber",
	Long:  "Main data grubber",
}

func Execute() {
	def.Init()
	if err := rootCmd.Execute(); err != nil {
		log.Fatalln("Execute", "err", err.Error())
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is ./config.yaml)")
}

func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		viper.AddConfigPath(".")
		viper.SetConfigName("config")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		log.Println("Using config file:", viper.ConfigFileUsed())
	}
}
