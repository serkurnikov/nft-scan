package cmd

func init() {
	rootCmd.AddCommand(pollingCmd)
	rootCmd.AddCommand(generateCmd)
}
