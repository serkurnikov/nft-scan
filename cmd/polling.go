package cmd

import (
	"context"

	"github.com/powerman/structlog"
	"github.com/spf13/cobra"
	"lab.aviproduction.org/aggregator/go-kit/pkg/mongo"
	nts "lab.aviproduction.org/aggregator/go-kit/pkg/nats"
	"lab.aviproduction.org/aggregator/go-kit/pkg/workerpool"

	"lab.aviproduction.org/aggregator/nft-scan/config"
	"lab.aviproduction.org/aggregator/nft-scan/internal/app"
	"lab.aviproduction.org/aggregator/nft-scan/internal/dal"
	ethereum "lab.aviproduction.org/aggregator/nft-scan/pkg/eth"
)

var pollingCmd = &cobra.Command{
	Use:       "polling",
	Short:     "Start scanning networks with https only (short polling)",
	Long:      "Read and process blocks, save data bo DB",
	ValidArgs: []string{"on", "off"},
	Run: func(cmd *cobra.Command, args []string) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		logger := structlog.FromContext(ctx, nil)
		logger.SetLogLevel(structlog.INF)

		cfg := config.InitConfiguration(logger)
		listener := ethereum.InitListener(cfg, logger)
		err := nts.InitNats(cfg.NatsURL)
		if err != nil {
			logger.Fatalln("pollingCmd", "failed Polling", err.Error())
		}

		wp := workerpool.Init(logger, cfg.WpSize)
		repo, _ := dal.New(
			&mongo.Config{
				URL:      cfg.MongoURL,
				Database: cfg.MongoDatabase,
			}, wp, cfg.NetworkCfg.Name, logger)

		application := app.New(repo, wp, cfg, listener, logger)
		err = application.Polling(ctx)
		if err != nil {
			logger.Fatalln("pollingCmd", "failed Polling", err.Error())
		}
	},
}
