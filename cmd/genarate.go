package cmd

import (
	"context"
	"fmt"
	"regexp"
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/powerman/structlog"
	"github.com/spf13/cobra"
	"lab.aviproduction.org/aggregator/go-kit/pkg/file"

	abiapi "lab.aviproduction.org/aggregator/nft-scan/internal/proxying/abiapi"
	api "lab.aviproduction.org/aggregator/nft-scan/internal/proxying/nftscan"
)

var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "Start generating marketplace resource code",
	Long:  "Fetch data from NFT scan api, save data and generate go files",
	Run: func(cmd *cobra.Command, args []string) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		logger := structlog.FromContext(ctx, nil)
		logger.SetLogLevel(structlog.INF)

		nftClient := api.NewClient(logger)
		abiClient := abiapi.NewClient(logger)
		fileManager := file.New(logger)

		for _, chainType := range api.GetSupportedMarketPlaceChainTypes() {
			marketPlaces, err := nftClient.GetNFTsMarketPlaces(ctx, api.NFTMarketPlacesRequest{
				Chain: chainType,
				Time:  api.All,
			})
			if err != nil {
				logger.Fatalln("generateCmd", "failed fetch marketplace information", err.Error())
			}

			chainFolderPath := fmt.Sprintf("./pkg/marketplace/%s", chainType.String())
			_ = fileManager.CreateFolder(chainFolderPath)

			for _, market := range marketPlaces.Data {
				contractName := regexp.MustCompile(`[^a-zA-Z]+`).ReplaceAllString(strings.Replace(strings.ToLower(market.ContractName), " ", "_", -1), "")
				marketFolderPath := chainFolderPath + "/" + contractName

				_ = fileManager.CreateFolder(marketFolderPath)
				for i, contract := range market.Contracts {
					logger.Info("Generate contract", "address", contract)
					abi, err := abiClient.GetAbiSource(context.Background(), abiapi.AbiRequest{
						Address:   contract,
						ChainType: chainType,
					})
					if err != nil {
						logger.Warn("generateCmd",
							fmt.Sprintf("failed get abi caller address: %s chain type: %s", contract, chainType), err.Error())
						continue
					}

					libs := make(map[string]string)
					aliases := make(map[string]string)

					pkgName := fmt.Sprintf("%v_%v", contractName, i)
					code, err := bind.Bind([]string{pkgName}, []string{abi.Result}, []string{""}, []map[string]string{}, pkgName, bind.LangGo, libs, aliases)
					if err != nil {
						logger.Fatalln("generateCmd", "failed to generate ABI binding", pkgName, "err", err)
					}

					contractFolder := marketFolderPath + "/" + fmt.Sprintf("type_%v", i)
					_ = fileManager.CreateFolder(contractFolder)

					file, _ := fileManager.CreateFile(contractFolder + fmt.Sprintf("/%s.go", contract))
					_ = fileManager.WriteString(file, code)
				}
			}
		}
	},
}
