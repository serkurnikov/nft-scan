package config

import (
	"io"
	"os"

	"github.com/ethereum/go-ethereum/common"
	"github.com/powerman/structlog"
	"github.com/spf13/viper"
	"gopkg.in/Graylog2/go-gelf.v1/gelf"
	"lab.aviproduction.org/aggregator/go-kit/models"
)

type NetworkConfig struct {
	AvgBlockAppear int    `mapstructure:"agv_block_appear"`
	Name           string `mapstructure:"name"`
	AltName        string `mapstructure:"alt_name"`
	FromBlock      uint64 `mapstructure:"from_block"`
	ToBlock        uint64 `mapstructure:"to_block"`
	CurrencySymbol string `mapstructure:"currency_symbol"`
	HTTP           string `mapstructure:"http"`
	AltHTTP        string `mapstructure:"alt_http"`
	Ws             string `mapstructure:"ws,omitempty"`
}

type Configuration struct {
	MongoURL       string
	MongoDatabase  string
	NatsURL        string
	LogLevel       string
	LogGraylog     string
	MetricsGateway string
	WpSize         int
	NetworkCfg     NetworkConfig
	Topics         [][]common.Hash
}

func InitConfiguration(logger *structlog.Logger) *Configuration {
	var err error
	cfg := Configuration{}

	cfg.Topics = [][]common.Hash{
		{
			models.Transfer.Hash(),
			models.OpenSeaSale.Hash(),
		},
	}
	err = viper.UnmarshalKey("network", &cfg.NetworkCfg)
	if err != nil {
		logger.Fatalln("Init Configuration", "failed to unmarshal config - network", err.Error())
	}
	if cfg.NetworkCfg.AvgBlockAppear == 0 {
		cfg.NetworkCfg.AvgBlockAppear = 60
	}
	if cfg.NetworkCfg.AltName == "" {
		cfg.NetworkCfg.AltName = cfg.NetworkCfg.Name
	}

	cfg.MongoURL = viper.GetString("mongo.url")
	cfg.MongoDatabase = viper.GetString("mongo.database")
	cfg.NatsURL = viper.GetString("nats.url")
	cfg.LogLevel = viper.GetString("log.level")
	cfg.LogGraylog = viper.GetString("log.graylog")
	cfg.MetricsGateway = viper.GetString("metrics.gateway")
	cfg.WpSize = viper.GetInt("workerpool.size")
	if cfg.WpSize == 0 {
		cfg.WpSize = 100
	}

	logger.Info("CFG:", "MongoURL", cfg.MongoURL)
	logger.Info("CFG:", "MongoDatabase", cfg.MongoDatabase)
	logger.Info("CFG:", "NatsUrl", cfg.NatsURL)
	logger.Info("CFG:", "LogLevel", cfg.LogLevel)
	logger.Info("CFG:", "LogGraylog", cfg.LogGraylog)
	logger.Info("CFG:", "MetricsGateway", cfg.MetricsGateway)
	logger.Info("CFG:", "NetworkCfg", cfg.NetworkCfg)

	if cfg.LogGraylog != "" {
		gelfWriter, err := gelf.NewWriter(cfg.LogGraylog)
		if err != nil {
			logger.Fatalln("Init Configuration", "failed create new gelfWriter", err.Error())
		}
		logger.SetOutput(io.MultiWriter(os.Stderr, gelfWriter))
		logger.SetLogLevel(structlog.INF)
		logger.Info("connect to graylog", "graylog", cfg.LogGraylog)
	}
	return &cfg
}
