PROJECT_NAME := "nft-scan"
PKG := $(PROJECT_NAME)
PKG_LIST := $(shell go list ./... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

marketplace: ## Fetch and generate marketplace contracts
	@cd ./pkg/marketplace && rm -r */
	@go run main.go generate

erc721: ## Generate ERC721 token contract
	@solc-select install 0.8.1
	@mkdir -p ./pkg/eth/contracts/erc721
	@cd ./pkg/eth/token/token/ERC721
	@solc --abi --bin ERC721.sol -o build/erc721
	@abigen --bin=./build/erc721/ERC721.bin --abi=./build/erc721/ERC721.abi --pkg=erc721 --out=erc721.go

erc1155: ## Generate ERC1155 token contract
	@solc-select install 0.8.1
	@mkdir -p ./pkg/eth/contracts/erc1155
	@cd ./pkg/eth/token/token/ERC1155
	@solc --abi --bin ERC1155.sol -o build/erc1155
	@abigen --bin=./build/erc1155/ERC1155.bin --abi=./build/erc1155/ERC1155.abi --pkg=erc1155 --out=erc1155.go

lint: ## Lint testing
	@go fmt ./...
	@golangci-lint run -v

test: ## Run unittests
	@go test -short ${PKG_LIST}

prof: ##profiling application
	sh ./scripts/profile.sh;

race: ## Run data race detector
	@go test -race -short ${PKG_LIST}

cover: ## Generate global code coverage report
	@go test ./... -coverprofile=coverage.out -coverpkg=./...
	@go tool cover -html=coverage.out -o cover.html
	@rm -rf ./coverage.out

dep: ## Get the dependencies
	@go get -v -d ./...
	@go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'