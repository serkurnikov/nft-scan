package dao

import (
	"math/big"

	"github.com/ethereum/go-ethereum/common"
)

type NetworkInfo struct {
	Name           string `json:"name" bson:"name"`
	CurrencySymbol string `json:"currencySymbol" bson:"currencySymbol"`
}

type Transfer struct {
	From  common.Address
	To    common.Address
	Value *big.Int
}
