package dao

type (
	MetaDataType string
)

const (
	JSON MetaDataType = "JSON"
	PNG  MetaDataType = "PNG"

	SingleToken uint = 1
)

type Asset struct {
	UUID                 string       `bson:"uuid" yaml:"uuid"`
	ContractAddress      string       `bson:"contractAddress" yaml:"contractAddress"`
	ContractName         string       `bson:"contractName" yaml:"contractName"`
	ContractTokenID      string       `bson:"contractTokenID" yaml:"contractTokenID"`
	Network              string       `bson:"network" yaml:"network"`
	ErcType              ERCType      `bson:"ercType" yaml:"ercType"`
	Amount               uint         `bson:"amount" yaml:"amount"`
	Minter               string       `bson:"minter" yaml:"minter"`
	Owner                string       `bson:"owner" yaml:"owner"`
	MintTimestamp        uint64       `bson:"mintTimestamp" yaml:"mintTimestamp"`
	MintTransactionHash  string       `bson:"mintTransactionHash" yaml:"mintTransactionHash"`
	MintPrice            float64      `bson:"mintPrice" yaml:"mintPrice"`
	TokenURI             string       `bson:"tokenURI" yaml:"tokenURI"`
	MetadataJSON         string       `bson:"metadataJSON" yaml:"metadataJSON"`
	Name                 string       `bson:"name" yaml:"name"`
	Description          string       `bson:"description" yaml:"description"`
	ContentType          string       `bson:"contentType" yaml:"contentType"`
	ContentURI           string       `bson:"contentURI" yaml:"contentURI"`
	ImageURI             string       `bson:"imageURI" yaml:"imageURI"`
	ExternalLink         string       `bson:"externalLink" yaml:"externalLink"`
	LatestTradePrice     float64      `bson:"latestTradePrice" yaml:"latestTradePrice"`
	LatestTradeTimestamp uint64       `bson:"latestTradeTimestamp" yaml:"latestTradeTimestamp"`
	NftScanID            string       `bson:"nftScanID" yaml:"nftScanID"`
	NftScanURI           string       `bson:"nftScanURI" yaml:"nftScanURI"`
	Attributes           []Attributes `bson:"attributes" yaml:"attributes"`
	Error                string       `bson:"error" yaml:"error"`
}

type MetadataJSON struct {
	Name         string       `bson:"name" json:"name" yaml:"name"`
	Description  string       `bson:"description" json:"description" yaml:"description"`
	Image        string       `bson:"image" json:"image" yaml:"image"`
	Edition      interface{}  `bson:"edition" json:"edition" yaml:"edition"`
	AnimationURL string       `bson:"animationURL" json:"animation_url" yaml:"animationURL"`
	Attributes   []Attributes `bson:"attributes" json:"attributes" yaml:"attributes"`
}

type Attributes struct {
	TraitType string      `bson:"traitType" json:"trait_type" yaml:"traitType"`
	Value     interface{} `bson:"value" json:"value" yaml:"value"`
}
