package dao

import (
	"math/big"
	"time"
)

type (
	EventType string
	ERCType   string
)

const (
	EmptyAddress = "0x0000000000000000000000000000000000000000000000000000000000000000"

	Mint        EventType = "Mint"
	Burn        EventType = "Burn"
	Sale        EventType = "Sale"
	Transferred EventType = "Transfer"

	ERC721  ERCType = "ERC721"
	ERC1155 ERCType = "ERC1155"
	NotNFT  ERCType = "NotNFT"
)

type Transaction struct {
	Hash            string    `bson:"hash" yaml:"hash"`
	From            string    `bson:"from" yaml:"from"`
	To              string    `bson:"to" yaml:"to"`
	BlockNumber     uint64    `bson:"blockNumber" yaml:"blockNumber"`
	BlockHash       string    `bson:"blockHash" yaml:"blockHash"`
	GasPrice        *big.Int  `bson:"gasPrice" yaml:"gasPrice"`
	GasUsed         string    `bson:"gasUsed" yaml:"gasUsed"`
	GasFee          uint64    `bson:"gasFee" yaml:"gasFee"`
	Timestamp       time.Time `bson:"timestamp" yaml:"timestamp"`
	ContractAddress string    `bson:"contractAddress" yaml:"contractAddress"`
	ContractName    string    `bson:"contractName" yaml:"contractName"`
	ContractSymbol  string    `bson:"contractSymbol" yaml:"contractSymbol"`
	ContractTokenID *big.Int  `bson:"contractTokenID" yaml:"contractTokenID"`
	Network         string    `bson:"network" yaml:"network"`
	ErcType         ERCType   `bson:"ercType" yaml:"ercType"`
	Send            string    `bson:"send" yaml:"send"`
	Receive         string    `bson:"receive" yaml:"receive"`
	Amount          uint64    `bson:"amount" yaml:"amount"`
	TradeValue      string    `bson:"tradeValue" yaml:"tradeValue"`
	TradePrice      float64   `bson:"tradePrice" yaml:"tradePrice"`
	TradeSymbol     string    `bson:"tradeSymbol" yaml:"tradeSymbol"`
	EventType       EventType `bson:"eventType" yaml:"eventType"`
	ExchangeName    string    `bson:"exchangeName" yaml:"exchangeName"`
	NftScanTxID     uint64    `bson:"nftScanTxID" yaml:"nftScanTxID"`
}
