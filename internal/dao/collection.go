package dao

import "time"

type Collection struct {
	UUID                    string  `bson:"uuid" yaml:"uuid"`
	ContractAddress         string  `bson:"contractAddress" yaml:"contractAddress"`
	Name                    string  `bson:"name" yaml:"name"`
	Network                 string  `bson:"network" yaml:"network"`
	Symbol                  string  `bson:"symbol" yaml:"symbol"`
	Description             string  `bson:"description" yaml:"description"`
	Website                 string  `bson:"website" yaml:"website"`
	Email                   string  `bson:"email" yaml:"email"`
	Twitter                 string  `bson:"twitter" yaml:"twitter"`
	Discord                 string  `bson:"discord" yaml:"discord"`
	Telegram                string  `bson:"telegram" yaml:"telegram"`
	Github                  string  `bson:"github" yaml:"github"`
	Instagram               string  `bson:"instagram" yaml:"instagram"`
	Medium                  string  `bson:"medium" yaml:"medium"`
	LogoURL                 string  `bson:"logoURL" yaml:"logoURL"`
	BannerURL               string  `bson:"bannerURL" yaml:"bannerURL"`
	FeaturedURL             string  `bson:"featuredURL" yaml:"featuredURL"`
	LargeImageURL           string  `bson:"largeImageURL" yaml:"largeImageURL"`
	Attributes              string  `bson:"attributes" yaml:"attributes"`
	ErcType                 ERCType `bson:"ercType" yaml:"ercType"`
	DeployBlockNumber       uint64  `bson:"deployBlockNumber" yaml:"deployBlockNumber"`
	Owner                   string  `bson:"owner" yaml:"owner"`
	Verified                bool    `bson:"verified" yaml:"verified"`
	Royalty                 uint64  `bson:"royalty" yaml:"royalty"`
	ItemsTotal              int     `bson:"itemsTotal" yaml:"itemsTotal"`
	OwnersTotal             int     `bson:"ownersTotal" yaml:"ownersTotal"`
	FloorPrice              float64 `bson:"floorPrice" yaml:"floorPrice"`
	PriceSymbol             string  `bson:"priceSymbol" yaml:"priceSymbol"`
	CollectionsWithSameName string  `bson:"collectionsWithSameName" yaml:"collectionsWithSameName"`
}

type FloorPrice struct {
	MarketPlaceName string    `json:"marketPlaceName"`
	FloorPrice      float64   `json:"floorPrice"`
	PriceCurrency   string    `json:"priceCurrency"`
	RetrievedAt     time.Time `json:"retrievedAt"`
	CollectionUrl   string    `json:"collectionUrl"`
}
