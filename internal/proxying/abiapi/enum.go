package etherscanapi

type (
	ModuleType string
	ActionType string
)

const (
	ContractModule ModuleType = "contract"

	AbiAction    ActionType = "getabi"
	SourceAction ActionType = "getsourcecode"
)

func (t ModuleType) String() string {
	return string(t)
}

func (t ActionType) String() string {
	return string(t)
}
