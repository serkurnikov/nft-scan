package etherscanapi

import api "lab.aviproduction.org/aggregator/nft-scan/internal/proxying/nftscan"

type ResponseError struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

type AbiRequest struct {
	Address   string        `json:"address"`
	ChainType api.ChainType `json:"chainType"`
}

type AbiResponse struct {
	Status  string `json:"status,omitempty"`
	Message string `json:"message,omitempty"`
	Result  string `json:"result,omitempty"`
}
