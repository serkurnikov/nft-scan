package etherscanapi

import (
	"context"
	"strings"

	abi "github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/powerman/structlog"
	"gopkg.in/resty.v1"
	"lab.aviproduction.org/aggregator/go-kit/pkg/apierrors"

	api "lab.aviproduction.org/aggregator/nft-scan/internal/proxying/nftscan"
)

const (
	NotokMessage = "NOTOK"
)

const (
	ArbitrumProxyHost   = "https://api.arbiscan.io/api"
	ArbitrumProxyApiKey = "F8KAHJS4QPTHQNSAIPM2WF5XCRHZFSCZ9H"

	AvalancheProxyHost   = "https://api.snowtrace.io/api"
	AvalancheProxyApiKey = "BGGNJ33IZAV73SPFZRXYWRZKA2HJN486KR"

	BnbProxyHost   = "https://api.bscscan.com/api"
	BnbProxyApiKey = "78ITUP1RDU17H24PHSE6BSPZDUN2GFQWRQ"

	AlternativeEthereumProxyHost = "https://api-goerli.etherscan.io/api"
	EthereumProxyHost            = "https://api.etherscan.io/api"
	EthereumApiKey               = "V87GTS9SFBCARBRTP1RVSMKSDWD33M12DM"

	PolygonProxyHost   = "https://api.polygonscan.com/api"
	PolygonProxyApiKey = "QU2BNV4EP32AZHI6W58UGRGBIXQ2CKCTH2"

	OptimismProxyHost   = "https://api-optimistic.etherscan.io/api"
	OptimismProxyApiKey = "MR1QD9H53EE1IIDJ5FKUBJ722Y9TG4G7I"

	MoonbeamProxyHost   = "https://api-moonbeam.moonscan.io/api"
	MoonbeamProxyApiKey = "EQYQ9RWBP843GMG6SRT896ZYA5PK5XJC7A"

	ModuleKey  = "module"
	ActionKey  = "action"
	AddressKey = "address"
	ApiKey     = "apiKey"
)

type ProxyApi struct {
	Host   string
	ApiKey string
}

func GetProxyApi() func(chainType api.ChainType) ProxyApi {
	theMap := map[api.ChainType]ProxyApi{
		api.Arbitrum: {
			Host:   ArbitrumProxyHost,
			ApiKey: ArbitrumProxyApiKey,
		},
		api.Avalanche: {
			Host:   AvalancheProxyHost,
			ApiKey: AvalancheProxyApiKey,
		},
		api.Bnb: {
			Host:   BnbProxyHost,
			ApiKey: BnbProxyApiKey,
		},
		api.Ethereum: {
			Host:   EthereumProxyHost,
			ApiKey: EthereumApiKey,
		},
		api.Moonbeam: {
			Host:   MoonbeamProxyHost,
			ApiKey: MoonbeamProxyApiKey,
		},
		api.Optimism: {
			Host:   OptimismProxyHost,
			ApiKey: OptimismProxyApiKey,
		},
		api.Polygon: {
			Host:   PolygonProxyHost,
			ApiKey: PolygonProxyApiKey,
		},
	}

	return func(key api.ChainType) ProxyApi {
		return theMap[key]
	}
}

type Client struct {
	client *resty.Client
	log    *structlog.Logger
}

func NewClient(log *structlog.Logger) *Client {
	return &Client{
		client: resty.New(),
		log:    log,
	}
}

func parseError(response *resty.Response, err error) error {
	if err != nil {
		return apierrors.InvalidRequest.WithDescription(err.Error())
	}
	if response.IsSuccess() {
		return nil
	}

	r := response.Error().(*ResponseError)
	if r.Code == 200 {
		return nil
	}

	switch r.Code {
	case 404:
		return apierrors.NotFound
	case 400:
		return apierrors.Validation
	}
	return apierrors.Internal.WithDescription(r.Msg)
}

func (c *Client) GetAbiSource(ctx context.Context, req AbiRequest) (*AbiResponse, error) {
	proxyApi := GetProxyApi()(req.ChainType)
	response, err := c.client.R().
		SetContext(ctx).
		SetResult(&AbiResponse{}).
		SetQueryParams(map[string]string{
			ModuleKey:  ContractModule.String(),
			ActionKey:  AbiAction.String(),
			AddressKey: req.Address,
			ApiKey:     proxyApi.ApiKey,
		}).
		SetError(&ResponseError{}).
		Get(proxyApi.Host)
	if e := parseError(response, err); e != nil {
		return nil, e
	}

	abiResponse := response.Result().(*AbiResponse)
	if abiResponse.Message == NotokMessage {
		return nil, apierrors.NotFound.WithDescription(abiResponse.Result)
	}

	return clearResponseFromArtifacts(abiResponse)
}

func clearResponseFromArtifacts(abiResponse *AbiResponse) (*AbiResponse, error) {
	abiResponse.Result = strings.Replace(abiResponse.Result, "\\", "", -1)
	return abiResponse, nil
}

func (c *Client) ParseAbi(ctx context.Context, req AbiRequest) (*abi.ABI, error) {
	abiSource, err := c.GetAbiSource(ctx, req)
	if err != nil {
		return nil, err
	}

	parsedAbi, err := abi.JSON(strings.NewReader(abiSource.Result))
	if err != nil {
		return nil, err
	}
	return &parsedAbi, nil
}
