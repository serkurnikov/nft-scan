package nftscanapi

import (
	"fmt"

	"lab.aviproduction.org/aggregator/go-kit/models"
)

type (
	ChainType     string
	Time          string
	Sort          string
	SortDirection string
)

const (
	Ethereum  ChainType = "restapi"
	Optimism  ChainType = "optimismapi"
	Bnb       ChainType = "bnbapi"
	Polygon   ChainType = "polygonapi"
	Arbitrum  ChainType = "arbitrumapi"
	Avalanche ChainType = "avaxapi"
	Moonbeam  ChainType = "moonbeamapi"
	Unknown   ChainType = "unknown"

	Day   Time = "1d"
	Week  Time = "7d"
	Month Time = "30d"
	All   Time = "all"

	Volume Sort = "volume"
	Sales  Sort = "sales"
	Wallet Sort = "wallet"

	ASC  SortDirection = "asc"
	DESC SortDirection = "desc"
)

func (c ChainType) String() string {
	return string(c)
}

func (t Time) String() string {
	return string(t)
}

func (s Sort) String() string {
	return string(s)
}

func (s SortDirection) String() string {
	return string(s)
}

func GetSupportedMarketPlaceChainTypes() []ChainType {
	return []ChainType{
		Ethereum,
		Optimism,
		Bnb,
		Polygon,
		Arbitrum,
		Avalanche,
		Moonbeam,
	}
}

func NetworkNameToChainType(n string) (t ChainType, err error) {
	switch n {
	case models.Arbitrum:
		return Arbitrum, nil
	case models.Avalanch:
		return Avalanche, nil
	case models.Bsc:
		return Bnb, nil
	case models.Eth:
		return Ethereum, nil
	case models.Optimism:
		return Optimism, nil
	case models.Moonriver:
		return Moonbeam, nil
	default:
		return Ethereum, nil
	}
}

func StringToChainType(chainType string) (t ChainType, err error) {
	types := map[ChainType]struct{}{
		Ethereum:  {},
		Optimism:  {},
		Bnb:       {},
		Polygon:   {},
		Arbitrum:  {},
		Avalanche: {},
		Moonbeam:  {},
	}

	key := ChainType(chainType)
	_, ok := types[key]
	if !ok {
		return t, fmt.Errorf(`cannot parse:[%s] as chain type`, chainType)
	}
	return key, nil
}

func StringToTimeType(time string) (t Time, err error) {
	types := map[Time]struct{}{
		Day:   {},
		Week:  {},
		Month: {},
		All:   {},
	}

	key := Time(time)
	_, ok := types[key]
	if !ok {
		return t, fmt.Errorf(`cannot parse:[%s] as time type`, time)
	}
	return key, nil
}

func StringToSortType(sort string) (t Sort, err error) {
	types := map[Sort]struct{}{
		Volume: {},
		Sales:  {},
		Wallet: {},
	}

	key := Sort(sort)
	_, ok := types[key]
	if !ok {
		return t, fmt.Errorf(`cannot parse:[%s] as sort type`, sort)
	}
	return key, nil
}

func StringToSortDirectionType(direction string) (t SortDirection, err error) {
	types := map[SortDirection]struct{}{
		ASC:  {},
		DESC: {},
	}

	key := SortDirection(direction)
	_, ok := types[key]
	if !ok {
		return t, fmt.Errorf(`cannot parse:[%s] as sort direction type`, direction)
	}
	return key, nil
}
