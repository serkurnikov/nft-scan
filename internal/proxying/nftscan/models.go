package nftscanapi

type ResponseError struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

type NFTMarketPlacesRequest struct {
	Chain         ChainType     `json:"chain"`
	Time          Time          `json:"time"`
	SortField     Sort          `json:"sort_field"`
	SortDirection SortDirection `json:"sort_direction"`
}

type NFTMarketPlacesResponse struct {
	Code int         `json:"code"`
	Msg  interface{} `json:"msg"`
	Data []Data      `json:"data"`
}
type Data struct {
	ContractName    string   `json:"contract_name"`
	LogoURL         string   `json:"logo_url"`
	Contracts       []string `json:"contracts"`
	Volume          float64  `json:"volume"`
	Gas             float64  `json:"gas"`
	Sales           int      `json:"sales"`
	Wallets         int      `json:"wallets"`
	Volume1DChange  string   `json:"volume1d_change"`
	Volume7DChange  string   `json:"volume7d_change"`
	Volume30DChange string   `json:"volume30d_change"`
}
