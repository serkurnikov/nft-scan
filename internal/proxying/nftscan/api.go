package nftscanapi

import (
	"context"
	"fmt"

	"github.com/powerman/structlog"
	"gopkg.in/resty.v1"
	"lab.aviproduction.org/aggregator/go-kit/pkg/apierrors"
)

const (
	XApiKeyHeader = "X-API-KEY"

	baseURL = "nftscan.com/api/v2"

	//nolint
	apiKey = "6vFLVFPU"
)

type Client struct {
	client *resty.Client
	log    *structlog.Logger
}

func NewClient(log *structlog.Logger) *Client {
	return &Client{
		client: resty.New(),
		log:    log,
	}
}

func parseError(response *resty.Response, err error) error {
	if err != nil {
		return apierrors.InvalidRequest.WithDescription(err.Error())
	}
	if response.IsSuccess() {
		return nil
	}

	r := response.Error().(*ResponseError)
	if r.Code == 200 {
		return nil
	}

	switch r.Code {
	case 404:
		return apierrors.NotFound
	case 400:
		return apierrors.Validation
	}
	return apierrors.Internal.WithDescription(r.Msg)
}

func (c *Client) GetNFTsMarketPlaces(ctx context.Context, req NFTMarketPlacesRequest) (*NFTMarketPlacesResponse, error) {
	response, err := c.client.R().
		SetContext(ctx).
		SetResult(&NFTMarketPlacesResponse{}).
		SetHeader(XApiKeyHeader, apiKey).
		SetError(&ResponseError{}).
		Get(fmt.Sprintf("https://%s.%s/%s", req.Chain, baseURL, "statistics/ranking/marketplace"))
	if e := parseError(response, err); e != nil {
		return nil, e
	}
	return response.Result().(*NFTMarketPlacesResponse), nil
}
