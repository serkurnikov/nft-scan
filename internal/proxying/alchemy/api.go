package alchemyapi

import (
	"context"
	"fmt"

	"github.com/powerman/structlog"
	"gopkg.in/resty.v1"
	"lab.aviproduction.org/aggregator/go-kit/pkg/apierrors"

	"lab.aviproduction.org/aggregator/nft-scan/internal/dao"
)

const (
	baseURL = "https://eth-mainnet.g.alchemy.com/nft/v2"
	//nolint
	apiKey = "UGr-wcV9FKc410u7yhrikUN9zjWszVLR"
)

type Client struct {
	client *resty.Client
	log    *structlog.Logger
}

func NewClient(log *structlog.Logger) *Client {
	return &Client{
		client: resty.New(),
		log:    log,
	}
}

func parseError(response *resty.Response, err error) error {
	if err != nil {
		return apierrors.InvalidRequest.WithDescription(err.Error())
	}
	if response.IsSuccess() {
		return nil
	}

	r := response.Error().(*ResponseError)
	if r.Code == 200 {
		return nil
	}

	switch r.Code {
	case 404:
		return apierrors.NotFound
	case 400:
		return apierrors.Validation
	}
	return apierrors.Internal.WithDescription(r.Msg)
}

func (c *Client) GetNFTs(ctx context.Context, req OwnedNFTRequest) (*OwnedNFTResponse, error) {
	response, err := c.client.R().
		SetContext(ctx).
		SetResult(&OwnedNFTResponse{}).
		SetQueryParam("owner", req.Owner).
		SetError(&ResponseError{}).
		Get(fmt.Sprintf("%s/%s/getNFTs", baseURL, apiKey))
	if e := parseError(response, err); e != nil {
		return nil, e
	}
	return response.Result().(*OwnedNFTResponse), nil
}

func (c *Client) GetOwnersForCollection(ctx context.Context, req OwnersForCollectionRequest) (*OwnersForCollectionResponse, error) {
	response, err := c.client.R().
		SetContext(ctx).
		SetResult(&OwnersForCollectionResponse{}).
		SetQueryParam("contractAddress", req.ContractAddress).
		SetError(&ResponseError{}).
		Get(fmt.Sprintf("%s/%s/getOwnersForCollection", baseURL, apiKey))
	if e := parseError(response, err); e != nil {
		return nil, e
	}
	return response.Result().(*OwnersForCollectionResponse), nil
}

func (c *Client) GetAssetMetaData(ctx context.Context, url string) (*dao.MetadataJSON, error) {
	c.client.SetRedirectPolicy(resty.FlexibleRedirectPolicy(20))
	response, err := c.client.R().
		SetContext(ctx).
		SetResult(&dao.MetadataJSON{}).
		SetError(&ResponseError{}).
		Get(url)
	if e := parseError(response, err); e != nil {
		return nil, e
	}
	return response.Result().(*dao.MetadataJSON), nil
}
