package alchemyapi

import "time"

type ResponseError struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

type OwnedNFTRequest struct {
	Owner string `json:"owner"`
}

type OwnedNFTResponse struct {
	OwnedNFTs  []OwnedNFT `json:"ownedNfts"`
	TotalCount int        `json:"totalCount"`
	BlockHash  string     `json:"blockHash"`
}

type OwnersForCollectionRequest struct {
	ContractAddress string `json:"contractAddress"`
}

type OwnersForCollectionResponse struct {
	OwnerAddresses []string `json:"ownerAddresses"`
}

type OwnedNFT struct {
	Contract        Contract  `json:"caller"`
	ID              ID        `json:"id"`
	Balance         string    `json:"balance"`
	Title           string    `json:"title"`
	Description     string    `json:"description"`
	TokenURI        TokenURI  `json:"tokenUri"`
	Media           []Media   `json:"media"`
	Metadata        Metadata  `json:"metadata"`
	TimeLastUpdated time.Time `json:"timeLastUpdated"`
}

type Contract struct {
	Address string `json:"address"`
}

type TokenMetadata struct {
	TokenType string `json:"tokenType"`
}

type ID struct {
	TokenID       string        `json:"tokenId"`
	TokenMetadata TokenMetadata `json:"metadata"`
}

type TokenURI struct {
	Raw     string `json:"raw"`
	Gateway string `json:"gateway"`
}

type Media struct {
	Raw     string `json:"raw"`
	Gateway string `json:"gateway"`
}

type Attributes struct {
	Value       interface{} `json:"value"`
	DisplayType string      `json:"display_type"`
	TraitType   string      `json:"trait_type"`
}

type Files struct {
	Type string `json:"type"`
	URI  string `json:"uri"`
}

type Creators struct {
	Share   int    `json:"share"`
	Address string `json:"address"`
}

type Properties struct {
	Files    []Files    `json:"files"`
	Creators []Creators `json:"creators"`
}

type Metadata struct {
	Name                 string       `json:"name"`
	Description          string       `json:"description"`
	Image                string       `json:"image"`
	Attributes           []Attributes `json:"attributes"`
	Properties           Properties   `json:"properties"`
	Symbol               string       `json:"symbol"`
	ExternalURL          string       `json:"external_url"`
	SellerFeeBasisPoints int          `json:"seller_fee_basis_points"`
	Edition              int          `json:"edition"`
}
