package dal

import (
	"context"

	"github.com/globalsign/mgo/bson"
	"lab.aviproduction.org/aggregator/go-kit/models"
	"lab.aviproduction.org/aggregator/go-kit/pkg/apierrors"

	"lab.aviproduction.org/aggregator/nft-scan/internal/dao"
)

func (r *Repo) CreateNFTTransaction(ctx context.Context, transaction *dao.Transaction) error {
	_, e := r.Database.Collection(models.NFTTransactionInfo).InsertOne(ctx, transaction)
	return e
}

func (r *Repo) GetNFTTransactionByHash(ctx context.Context, hash string) (*dao.Transaction, error) {
	filter := bson.M{
		"hash": hash,
	}
	transaction := &dao.Transaction{}

	r.Database.Collection(models.NFTTransactionInfo).FindOne(ctx, filter).Decode(transaction)
	if transaction.Hash != hash {
		return nil, apierrors.NotFound.WithDescription("transaction not found")
	}
	return transaction, nil
}

func (r *Repo) GetNFTTransactionList(ctx context.Context) ([]*dao.Transaction, error) {
	filter := bson.M{}
	transactions := make([]*dao.Transaction, 0)
	cur, err := r.Database.Collection(models.NFTTransactionInfo).Find(ctx, filter)
	if err != nil {
		return transactions, err
	}
	defer cur.Close(ctx)

	for cur.Next(ctx) {
		transaction := &dao.Transaction{}
		err := cur.Decode(&transaction)
		if err != nil {
			return transactions, err
		}
		transactions = append(transactions, transaction)
	}
	return transactions, nil
}

func (r *Repo) DeleteNftTransactions(ctx context.Context) (err error) {
	f := bson.M{}
	_, err = r.Database.Collection(models.NFTTransactionInfo).DeleteMany(ctx, f)
	if err != nil {
		return apierrors.Internal.WithDescription(err.Error())
	}
	return nil
}
