package dal

import (
	"context"

	"github.com/gammazero/workerpool"
	"github.com/powerman/structlog"
	"lab.aviproduction.org/aggregator/go-kit/pkg/mongo"
)

type Repo struct {
	*mongo.Repo
	wp      *workerpool.WorkerPool
	logger  *structlog.Logger
	network string
}

func New(cfg *mongo.Config, wp *workerpool.WorkerPool, network string, logger *structlog.Logger) (*Repo, error) {
	r := &Repo{}
	var err error
	if r.Repo, err = mongo.New(cfg, logger); err != nil {
		return nil, err
	}
	r.logger = logger
	r.wp = wp
	r.network = network
	return r, nil
}

func (r *Repo) Disconnect(ctx context.Context) error {
	if err := r.Client.Disconnect(ctx); err != nil {
		return r.logger.Err("disconnect mongo client", "err", err.Error())
	}
	return nil
}
