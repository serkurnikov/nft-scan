package dal

import (
	"context"
	"time"

	"github.com/globalsign/mgo/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"lab.aviproduction.org/aggregator/go-kit/models"
)

func (r *Repo) UpdateBlock(ctx context.Context, blockTimestamp time.Time, networkName string, blockNumber uint64) (err error) {
	filter := bson.M{
		"networkName": networkName,
		"blockNumber": blockNumber,
	}
	update := bson.M{
		"$set": bson.M{
			"ts":          blockTimestamp,
			"networkName": networkName,
			"blockNumber": blockNumber,
		},
	}
	opts := options.Update().SetUpsert(true)
	_, err = r.Database.Collection(models.BlocksCollection).UpdateOne(ctx, filter, update, opts)

	return err
}
