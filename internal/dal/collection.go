package dal

import (
	"context"

	"github.com/globalsign/mgo/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"lab.aviproduction.org/aggregator/go-kit/models"
	"lab.aviproduction.org/aggregator/go-kit/pkg/apierrors"

	"lab.aviproduction.org/aggregator/nft-scan/internal/dao"
)

func (r *Repo) UpdateNFTCollectionInfo(ctx context.Context, collection *dao.Collection) error {
	f := bson.M{
		"uuid": collection.UUID,
	}
	u := bson.M{
		"$set": bson.M{
			"verified":                collection.Verified,
			"royalty":                 collection.Royalty,
			"itemsTotal":              collection.ItemsTotal,
			"ownersTotal":             collection.OwnersTotal,
			"floorPrice":              collection.FloorPrice,
			"priceSymbol":             collection.PriceSymbol,
			"collectionsWithSameName": collection.CollectionsWithSameName,
			"owner":                   collection.Owner,
		},
		"$setOnInsert": bson.M{
			"uuid":              collection.UUID,
			"contractAddress":   collection.ContractAddress,
			"name":              collection.Name,
			"symbol":            collection.Symbol,
			"description":       collection.Description,
			"website":           collection.Website,
			"email":             collection.Email,
			"twitter":           collection.Twitter,
			"discord":           collection.Discord,
			"telegram":          collection.Telegram,
			"github":            collection.Github,
			"instagram":         collection.Instagram,
			"medium":            collection.Medium,
			"logoURL":           collection.LogoURL,
			"bannerURL":         collection.BannerURL,
			"featuredURL":       collection.FeaturedURL,
			"largeImageURL":     collection.LargeImageURL,
			"attributes":        collection.Attributes,
			"ercType":           collection.ErcType,
			"deployBlockNumber": collection.DeployBlockNumber,
		},
	}

	opts := options.Update().SetUpsert(true)
	_, err := r.Database.Collection(models.NFTCollectionInfo).UpdateOne(ctx, f, u, opts)
	return err
}

func (r *Repo) GetNFTCollectionByContractAddress(ctx context.Context, contractAddress string) (*dao.Collection, error) {
	filter := bson.M{
		"contractAddress": contractAddress,
	}
	collection := &dao.Collection{}

	r.Database.Collection(models.NFTCollectionInfo).FindOne(ctx, filter).Decode(collection)
	if collection.ContractAddress != contractAddress {
		return nil, apierrors.NotFound.WithDescription("collection not found")
	}
	return collection, nil
}

func (r *Repo) GetNFTCollectionList(ctx context.Context) ([]*dao.Collection, error) {
	filter := bson.M{}
	collections := make([]*dao.Collection, 0)
	cur, err := r.Database.Collection(models.NFTCollectionInfo).Find(ctx, filter)
	if err != nil {
		return collections, err
	}
	defer cur.Close(ctx)

	for cur.Next(ctx) {
		cl := &dao.Collection{}
		err := cur.Decode(&cl)
		if err != nil {
			return collections, err
		}
		collections = append(collections, cl)
	}
	return collections, nil
}

func (r *Repo) DeleteNftCollections(ctx context.Context) (err error) {
	f := bson.M{}
	_, err = r.Database.Collection(models.NFTCollectionInfo).DeleteMany(ctx, f)
	if err != nil {
		return apierrors.Internal.WithDescription(err.Error())
	}
	return nil
}
