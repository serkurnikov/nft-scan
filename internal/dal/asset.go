package dal

import (
	"context"

	"github.com/globalsign/mgo/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"lab.aviproduction.org/aggregator/go-kit/models"
	"lab.aviproduction.org/aggregator/go-kit/pkg/apierrors"

	"lab.aviproduction.org/aggregator/nft-scan/internal/dao"
)

func (r *Repo) UpdateNFTAssetInfo(ctx context.Context, asset *dao.Asset) error {
	f := bson.M{
		"uuid": asset.UUID,
	}
	u := bson.M{
		"$set": bson.M{
			"latestTradePrice":     asset.LatestTradePrice,
			"latestTradeTimestamp": asset.LatestTradeTimestamp,
		},
		"$setOnInsert": bson.M{
			"uuid":                asset.UUID,
			"contractName":        asset.ContractName,
			"contractTokenID":     asset.ContractTokenID,
			"network":             asset.Network,
			"ercType":             asset.ErcType,
			"amount":              asset.Amount,
			"minter":              asset.Minter,
			"owner":               asset.Owner,
			"mintTimestamp":       asset.MintTimestamp,
			"mintTransactionHash": asset.MintTransactionHash,
			"mintPrice":           asset.MintPrice,
			"tokenURI":            asset.TokenURI,
			"name":                asset.Name,
			"contentType":         asset.ContentType,
			"contentURI":          asset.ContentURI,
			"externalLink":        asset.ExternalLink,
			"nftScanID":           asset.NftScanID,
			"nftScanURI":          asset.NftScanURI,
			"imageURI":            asset.ImageURI,
			"attributes":          asset.Attributes,
			"metadataJSON":        asset.MetadataJSON,
			"description":         asset.Description,
			"error":               asset.Error,
		},
	}

	opts := options.Update().SetUpsert(true)
	_, err := r.Database.Collection(models.NFTAssetInfo).UpdateOne(ctx, f, u, opts)
	return err
}

func (r *Repo) GetNFTAsset(ctx context.Context, contractAddress, contractTokenID string) (*dao.Asset, error) {
	filter := bson.M{
		"contractAddress": contractAddress,
		"contractTokenID": contractTokenID,
	}
	asset := &dao.Asset{}

	r.Database.Collection(models.NFTAssetInfo).FindOne(ctx, filter).Decode(asset)
	if asset.ContractAddress != contractAddress {
		return nil, apierrors.NotFound.WithDescription("collection not found")
	}
	return asset, nil
}

func (r *Repo) DeleteNftAssets(ctx context.Context) (err error) {
	f := bson.M{}
	_, err = r.Database.Collection(models.NFTAssetInfo).DeleteMany(ctx, f)
	if err != nil {
		return apierrors.Internal.WithDescription(err.Error())
	}
	return nil
}
