package integration

import (
	"context"
	"math/rand"

	"lab.aviproduction.org/aggregator/go-kit/pkg/utils"

	"lab.aviproduction.org/aggregator/nft-scan/internal/dao"
)

const (
	testOpenSeaAddress    = "0x00000000006c3852cbef3e08e8df289169ede581"
	testCollectionAddress = "0x87660380eFC270677f62D8D1b45cC1D0b6C60F47"

	DefaultCount = 2
)

func (s *TestSuite) TestAsset() {
	s.Run("parseMetadataJson: positive case", func() {
		for _, m := range s.fixtureInfo.MetadataInfo {
			metadata, err := s.application.ParseMetadataJson(m.TokenURI)
			s.NoError(err)
			s.Equal(&m.MetadataJSON, metadata)
		}
	})
}

func (s *TestSuite) TestCollection() {
	s.Run("check update collection: positive case", func() {
		list := generateListCollections(DefaultCount)

		for _, c := range list {
			err := s.application.Repo.UpdateNFTCollectionInfo(context.Background(), c)
			s.NoError(err)

			data, err := s.application.Repo.GetNFTCollectionByContractAddress(context.Background(), testCollectionAddress)
			s.NoError(err)

			s.Equal(&dao.Collection{
				ContractAddress:   list[0].ContractAddress,
				Name:              list[0].Name,
				Symbol:            list[0].Symbol,
				Description:       list[0].Description,
				Website:           list[0].Website,
				Email:             list[0].Email,
				Twitter:           list[0].Twitter,
				Discord:           list[0].Discord,
				Telegram:          list[0].Telegram,
				Github:            list[0].Github,
				Instagram:         list[0].Instagram,
				Medium:            list[0].Medium,
				LogoURL:           list[0].LogoURL,
				BannerURL:         list[0].BannerURL,
				FeaturedURL:       list[0].FeaturedURL,
				LargeImageURL:     list[0].LargeImageURL,
				Attributes:        list[0].Attributes,
				ErcType:           list[0].ErcType,
				DeployBlockNumber: list[0].DeployBlockNumber,

				Owner:                   c.Owner,
				Verified:                c.Verified,
				Royalty:                 c.Royalty,
				ItemsTotal:              c.ItemsTotal,
				OwnersTotal:             c.OwnersTotal,
				FloorPrice:              c.FloorPrice,
				PriceSymbol:             c.PriceSymbol,
				CollectionsWithSameName: c.CollectionsWithSameName,
			}, data)
		}
	})

	s.Run("check duplicate: positive case", func() {
		list, err := s.application.Repo.GetNFTCollectionList(context.Background())
		s.NoError(err)
		s.Equal(1, len(list))

		err = s.application.Repo.DeleteNftCollections(context.Background())
		s.NoError(err)
	})
}

func generateListCollections(count int) (list []*dao.Collection) {
	list = make([]*dao.Collection, 0, count)
	for i := 0; i < count; i++ {
		list = append(list, generateCollection())
	}
	return
}

func generateCollection() *dao.Collection {
	return &dao.Collection{
		ContractAddress:   testCollectionAddress,
		Name:              utils.RandStringBytes(5),
		Symbol:            utils.RandStringBytes(5),
		Description:       utils.RandStringBytes(100),
		Website:           utils.RandStringBytes(5),
		Email:             utils.RandStringBytes(5),
		Twitter:           utils.RandStringBytes(5),
		Discord:           utils.RandStringBytes(5),
		Telegram:          utils.RandStringBytes(5),
		Github:            utils.RandStringBytes(5),
		Instagram:         utils.RandStringBytes(5),
		Medium:            utils.RandStringBytes(5),
		LogoURL:           utils.RandStringBytes(5),
		BannerURL:         utils.RandStringBytes(5),
		FeaturedURL:       utils.RandStringBytes(5),
		LargeImageURL:     utils.RandStringBytes(5),
		Attributes:        utils.RandStringBytes(5),
		ErcType:           dao.ERC721,
		DeployBlockNumber: uint64(rand.Intn(100-5) + 5),

		Verified:                true,
		Owner:                   utils.RandStringBytes(5),
		Royalty:                 uint64(rand.Intn(100-5) + 5),
		ItemsTotal:              rand.Intn(100-5) + 5,
		FloorPrice:              float64(uint64(rand.Intn(100-5) + 5)),
		PriceSymbol:             utils.RandStringBytes(5),
		CollectionsWithSameName: utils.RandStringBytes(5),
	}
}
