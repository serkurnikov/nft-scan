package integration

import (
	"context"
	"fmt"
	"os"
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	"github.com/powerman/structlog"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/suite"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"lab.aviproduction.org/aggregator/go-kit/models"
	"lab.aviproduction.org/aggregator/go-kit/pkg/def"
	nts "lab.aviproduction.org/aggregator/go-kit/pkg/nats"
	"lab.aviproduction.org/aggregator/go-kit/pkg/workerpool"

	repo "lab.aviproduction.org/aggregator/go-kit/pkg/mongo"

	"lab.aviproduction.org/aggregator/nft-scan/config"
	"lab.aviproduction.org/aggregator/nft-scan/internal/app"
	"lab.aviproduction.org/aggregator/nft-scan/internal/dal"
	"lab.aviproduction.org/aggregator/nft-scan/internal/test/integration/fixtures"
	ethereum "lab.aviproduction.org/aggregator/nft-scan/pkg/eth"
)

const (
	MongoDataBaseName = "test"

	TestNetworkName           = "eth"
	TestNetworkCurrencySymbol = "ETH"
	TestNetworkHTTP           = "https://eth.focusnodes.com"
	TestNetworkWS             = "ws://eth-ws.focusnodes.com"

	localHost = "localhost"
)

func TestRepositorySuite(t *testing.T) {
	suite.Run(t, new(TestSuite))
}

type TestSuite struct {
	suite.Suite
	db          *mongo.Client
	application *app.App
	listener    *ethereum.Listener

	pool *dockertest.Pool

	mongoResource *dockertest.Resource
	natsResource  *dockertest.Resource

	logger *structlog.Logger

	reg *prometheus.Registry

	fixtureInfo *fixtures.FixtureInfo
}

func (s *TestSuite) SetupSuite() {
	s.reg = prometheus.NewPedanticRegistry()
	def.Init()

	s.logger = structlog.DefaultLogger

	var err error
	s.pool, err = dockertest.NewPool("")
	if err != nil {
		s.logger.Fatalf("Could not connect to docker: %s", err)
	}

	s.mongoResource, err = s.pool.RunWithOptions(&dockertest.RunOptions{
		Repository: "mongo",
		Tag:        "5.0",
	}, func(config *docker.HostConfig) {
		// set AutoRemove to true so that stopped container goes away by itself
		config.AutoRemove = true
		config.RestartPolicy = docker.RestartPolicy{
			Name: "no",
		}
	})
	if err != nil {
		s.logger.Fatalf("Could not start mongo resource: %s", err)
	}

	mongodbHost := os.Getenv("MONGODB_HOST") // this may be set by gitlab-ci when in DinD mode
	if os.Getenv("MONGODB_HOST") == "" {
		mongodbHost = localHost
	}
	MongoURL := fmt.Sprintf("mongodb://%s:%s", mongodbHost, s.mongoResource.GetPort("27017/tcp"))

	err = s.pool.Retry(func() error {
		s.db, err = mongo.Connect(
			context.TODO(),
			options.Client().ApplyURI(
				MongoURL,
			),
		)
		if err != nil {
			return err
		}
		return s.db.Ping(context.TODO(), nil)
	})
	if err != nil {
		s.logger.Fatalf("Could not connect to mongo docker: %s", err)
	}

	s.natsResource, err = s.pool.Run("nats", "latest", nil)
	if err != nil {
		s.logger.Fatalf("Could not start resource: %s", err)
	}

	natsHost := os.Getenv("NATS_HOST") // this may be set by gitlab-ci when in DinD mode
	if os.Getenv("NATS_HOST") == "" {
		natsHost = localHost
	}
	NatsURL := fmt.Sprintf("nats://%s:%s", natsHost, s.natsResource.GetPort("4222/tcp"))
	if err = s.pool.Retry(func() error {
		err = nts.InitNats(NatsURL)
		if err != nil {
			return err
		}
		return nil
	}); err != nil {
		s.logger.Fatalf("Could not connect to nats docker: %s", err)
	}
	s.setupApplication(MongoURL, NatsURL)
}

func (s *TestSuite) TearDownSuite() {
	if err := s.pool.Purge(s.mongoResource); err != nil {
		s.logger.Fatalf("Could not purge mongo resource: %s", err)
	}

	if err := s.db.Disconnect(context.TODO()); err != nil {
		s.logger.Fatalf("Could not disconnect mongo: %s", err)
	}

	if err := s.pool.Purge(s.natsResource); err != nil {
		s.logger.Fatalf("Could not purge nats resource: %s", err)
	}
}

func (s *TestSuite) setupApplication(MongoURL, NatsURL string) {
	cfg := &config.Configuration{
		MongoURL:      MongoURL,
		NatsURL:       NatsURL,
		MongoDatabase: MongoDataBaseName,
		WpSize:        100,
		NetworkCfg: config.NetworkConfig{
			Name:           TestNetworkName,
			CurrencySymbol: TestNetworkCurrencySymbol,
			HTTP:           TestNetworkHTTP,
			Ws:             TestNetworkWS,
		},
		Topics: [][]common.Hash{
			{
				models.Transfer.Hash(),
				models.OpenSeaSale.Hash(),
			},
		},
	}

	listener := ethereum.InitListener(cfg, s.logger)
	err := nts.InitNats(cfg.NatsURL)
	if err != nil {
		s.logger.Fatalln("pollingCmd", "failed Polling", err.Error())
	}

	wp := workerpool.Init(s.logger, cfg.WpSize)
	r, _ := dal.New(&repo.Config{
		URL:      cfg.MongoURL,
		Database: cfg.MongoDatabase,
	}, wp, cfg.NetworkCfg.Name, s.logger)

	s.listener = listener
	s.application = app.New(r, wp, cfg, s.listener, s.logger)

	fixtureInfo, err := fixtures.GetFixtures(s.listener)
	if err != nil {
		s.logger.Fatalf("Could not insert fixtures to database: %s", err)
	}
	s.fixtureInfo = &fixtureInfo
}
