package integration

import (
	"context"

	abiapi "lab.aviproduction.org/aggregator/nft-scan/internal/proxying/abiapi"
	nftscanapi "lab.aviproduction.org/aggregator/nft-scan/internal/proxying/nftscan"
	"lab.aviproduction.org/aggregator/nft-scan/internal/test/integration/fixtures"
	"lab.aviproduction.org/aggregator/nft-scan/pkg/caller"
)

func (s *TestSuite) TestEtherScanApi() {
	client := abiapi.NewClient(s.logger)
	abi, err := client.GetAbiSource(context.Background(), abiapi.AbiRequest{
		Address:   testOpenSeaAddress,
		ChainType: nftscanapi.Ethereum,
	})
	s.NoError(err)

	c, err := caller.InitiateContractCaller(testOpenSeaAddress, abi.Result, s.listener.GetHTTPClient())
	s.NoError(err)

	s.Run("test call caller name: positive case", func() {
		contractName, err := c.Call("name")
		s.NoError(err)
		s.Equal("Seaport", contractName[0].(string))
	})

	s.Run("test transfers: positive case", func() {
		for _, log := range s.fixtureInfo.Logs[fixtures.TransferLogsFixture] {
			err = s.application.ProcessingTransfer(context.Background(), log)
			s.NoError(err)
		}
	})

	s.Run("test openSeaOrders: positive case", func() {
		for _, log := range s.fixtureInfo.Logs[fixtures.OpenSeaOrdersLogsFixture] {
			_ = s.application.ProcessingSale(context.Background(), log)
		}
	})
}
