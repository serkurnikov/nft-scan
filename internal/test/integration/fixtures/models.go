package fixtures

import "lab.aviproduction.org/aggregator/nft-scan/internal/dao"

type (
	FixtureType string
)

const (
	TransferLogsFixture      FixtureType = "transfers"
	MetadataInfoFixture      FixtureType = "metadata"
	OpenSeaOrdersLogsFixture FixtureType = "openSeaOrders"
)

func (t FixtureType) String() string {
	return string(t)
}

type FixtureLog struct {
	Topic       string  `yaml:"topic"`
	Address     string  `yaml:"address"`
	BlockNumber uint64  `yaml:"blockNumber"`
	TxIndex     []uint8 `yaml:"txIndex"`
}

type MetadataInfo struct {
	TokenURI     string           `yaml:"tokenURI"`
	MetadataJSON dao.MetadataJSON `yaml:"metadataJSON"`
}
