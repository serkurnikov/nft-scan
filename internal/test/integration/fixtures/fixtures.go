package fixtures

import (
	"context"
	"fmt"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/spf13/viper"

	"lab.aviproduction.org/aggregator/nft-scan/pkg/eth"
)

type FixtureInfo struct {
	listener *eth.Listener

	MetadataInfo []MetadataInfo
	Logs         map[FixtureType][]types.Log
}

func GetFixtures(listener *eth.Listener) (FixtureInfo, error) {
	fixtures := []FixtureType{
		MetadataInfoFixture, TransferLogsFixture, OpenSeaOrdersLogsFixture,
	}

	f := FixtureInfo{
		listener: listener,
	}
	f.Logs = make(map[FixtureType][]types.Log)
	for _, fixture := range fixtures {
		if err := f.loadFixtures(fixture); err != nil {
			return FixtureInfo{}, err
		}
	}
	return f, nil
}

func (f *FixtureInfo) loadFixtures(fixture FixtureType) error {
	viper.SetConfigType("yaml")
	viper.SetConfigName("fixtures")
	viper.AddConfigPath("./fixtures")
	err := viper.ReadInConfig()
	if err != nil {
		return fmt.Errorf(fmt.Sprintf("Fatal error fixture file: %e", err))
	}

	switch fixture {
	case MetadataInfoFixture:
		metadataInfo, err := getMetadataFixtures()
		if err != nil {
			return err
		}
		f.MetadataInfo = metadataInfo
	case TransferLogsFixture, OpenSeaOrdersLogsFixture:
		logs, err := f.getLogsFixtures(fixture)
		if err != nil {
			return err
		}
		f.Logs[fixture] = logs
	default:
		return fmt.Errorf(fmt.Sprintf("cannot load fixture: %s", fixture))
	}
	return nil
}

func getMetadataFixtures() ([]MetadataInfo, error) {
	metadataInfo := make([]MetadataInfo, 0)
	if err := viper.UnmarshalKey("metadata", &metadataInfo); err != nil {
		return nil, err
	}
	return metadataInfo, nil
}

func (f *FixtureInfo) getLogsFixtures(fixtureType FixtureType) ([]types.Log, error) {
	fixtureLog := FixtureLog{}
	if err := viper.UnmarshalKey(fixtureType.String(), &fixtureLog); err != nil {
		return nil, err
	}

	logs := make([]types.Log, 0)
	result, err := f.listener.GetHistoryLogs(
		context.Background(), fixtureLog.BlockNumber, fixtureLog.BlockNumber,
		[]common.Address{
			common.HexToAddress(fixtureLog.Address),
		}, [][]common.Hash{
			{
				common.HexToHash(fixtureLog.Topic),
			},
		})
	if err != nil {
		return nil, err
	}
	return append(logs, result...), nil
}
