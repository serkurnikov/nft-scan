package app

import (
	"context"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"lab.aviproduction.org/aggregator/go-kit/pkg/apierrors"

	"lab.aviproduction.org/aggregator/nft-scan/internal/dao"
	"lab.aviproduction.org/aggregator/nft-scan/pkg/eth/contracts/erc1155"
	"lab.aviproduction.org/aggregator/nft-scan/pkg/eth/contracts/erc165"
	"lab.aviproduction.org/aggregator/nft-scan/pkg/eth/contracts/erc721"
)

const (
	uuidFormat = "%s:%s:%s"

	logoFormat   = "https://logo.nftscan.com/logo/%s.png"
	bannerFormat = "https://logo.nftscan.com/banner/%s.png"
)

func (a *App) ProcessingTransfer(ctx context.Context, eventLog types.Log) error {
	if len(eventLog.Topics) != topicsNFTDefaultNumbers {
		return apierrors.Internal.WithDescription("token value is invalid")
	}

	tx, err := a.createTransaction(ctx, eventLog)
	if err != nil {
		return apierrors.Internal.WithDescription(err.Error())
	}

	err = a.updateCollection(ctx, tx)
	if err != nil {
		return apierrors.Internal.WithDescription(err.Error())
	}

	asset := a.updateAsset(tx)
	err = a.Repo.UpdateNFTAssetInfo(ctx, asset)
	if err != nil {
		return apierrors.Internal.WithDescription(err.Error())
	}
	return nil
}

func (a *App) checkErcType(address common.Address) (dao.ERCType, error) {
	if ok, _ := a.isType721(address); ok {
		return dao.ERC721, nil
	}

	if ok, _ := a.isType1155(address); ok {
		return dao.ERC1155, nil
	}
	return dao.NotNFT, apierrors.Validation.WithDescription("token type is not NFT")
}

func (a *App) isType721(address common.Address) (bool, error) {
	t, err := erc721.NewErc721(address, a.listener.GetHTTPClient())
	if err != nil {
		return false, err
	}
	return t.SupportsInterface(nil, erc165.InterfaceIdErc721)
}

func (a *App) isType1155(address common.Address) (bool, error) {
	t, err := erc1155.NewErc1155(address, a.listener.GetHTTPClient())
	if err != nil {
		return false, err
	}
	return t.SupportsInterface(nil, erc165.InterfaceIdErc1155)
}

func (a *App) getTokenInstance(address common.Address, ercType dao.ERCType) (interface{}, error) {
	switch ercType {
	case dao.ERC721:
		t, err := erc721.NewErc721(address, a.listener.GetHTTPClient())
		if err != nil {
			return nil, err
		}
		return t, nil
	case dao.ERC1155:
		t, err := erc1155.NewErc1155(address, a.listener.GetHTTPClient())
		if err != nil {
			return nil, err
		}
		return t, nil
	default:
		return nil, apierrors.Internal.WithDescription("failed get NFT token instance")
	}
}
