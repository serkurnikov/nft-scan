//go:generate mockgen -package=$GOPACKAGE -source=$GOFILE -destination=mock.$GOFILE Appl,Repo

package app

import (
	"context"
	"time"

	"github.com/gammazero/workerpool"
	"github.com/powerman/structlog"
	"lab.aviproduction.org/aggregator/go-kit/pkg/cache/memory"

	"lab.aviproduction.org/aggregator/nft-scan/config"
	"lab.aviproduction.org/aggregator/nft-scan/internal/dal"
	"lab.aviproduction.org/aggregator/nft-scan/internal/dao"
	ethereum "lab.aviproduction.org/aggregator/nft-scan/pkg/eth"
	"lab.aviproduction.org/aggregator/nft-scan/pkg/marketplace"
	"lab.aviproduction.org/aggregator/nft-scan/pkg/parse"
)

type (
	Ctx = context.Context

	Appl interface {
		Polling(ctx context.Context) error
	}

	Repo interface {
		Disconnect(ctx context.Context) error

		UpdateBlock(ctx context.Context, blockTimestamp time.Time, networkName string, blockNumber uint64) (err error)

		UpdateNFTAssetInfo(ctx context.Context, asset *dao.Asset) error
		GetNFTAsset(ctx context.Context, contractAddress, contractTokenID string) (*dao.Asset, error)
		DeleteNftAssets(ctx context.Context) (err error)

		UpdateNFTCollectionInfo(ctx context.Context, collection *dao.Collection) error
		GetNFTCollectionByContractAddress(ctx context.Context, contractAddress string) (*dao.Collection, error)
		GetNFTCollectionList(ctx context.Context) ([]*dao.Collection, error)
		DeleteNftCollections(ctx context.Context) (err error)

		CreateNFTTransaction(ctx context.Context, transaction *dao.Transaction) error
		GetNFTTransactionByHash(ctx context.Context, hash string) (*dao.Transaction, error)
		DeleteNftTransactions(ctx context.Context) (err error)
	}

	App struct {
		Repo          Repo
		Cache         *memory.Storage
		marketManager *marketplace.Manager
		bankContracts *parse.BankContracts
		wp            *workerpool.WorkerPool
		listener      *ethereum.Listener
		logger        *structlog.Logger
		cfg           *config.Configuration
	}
)

func New(repo *dal.Repo, wp *workerpool.WorkerPool, cfg *config.Configuration, listener *ethereum.Listener, logger *structlog.Logger) *App {
	a := &App{
		Repo:          repo,
		Cache:         memory.InitCache(memory.CachedTime, memory.ClearedTime),
		marketManager: marketplace.New(logger),
		bankContracts: parse.New(listener, logger),
		wp:            wp,
		listener:      listener,
		cfg:           cfg,
		logger:        logger,
	}
	return a
}
