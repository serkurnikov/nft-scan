package app

import (
	"context"
	"fmt"
	"math/big"
	"sync"

	"github.com/ethereum/go-ethereum/common"
	"lab.aviproduction.org/aggregator/go-kit/pkg/apierrors"

	"lab.aviproduction.org/aggregator/nft-scan/internal/dao"
	nftscanapi "lab.aviproduction.org/aggregator/nft-scan/internal/proxying/nftscan"
	cl "lab.aviproduction.org/aggregator/nft-scan/pkg/collection"
)

func (a *App) updateCollection(ctx context.Context, transaction *dao.Transaction) error {
	var collection *dao.Collection
	uuid := fmt.Sprintf(uuidFormat,
		transaction.Network,
		transaction.ContractAddress,
		transaction.ContractSymbol)

	data, ok := a.Cache.Get(uuid)
	if ok {
		collection = data.(*dao.Collection)
	} else {
		collection = &dao.Collection{
			UUID:            uuid,
			ContractAddress: transaction.ContractAddress,
			Name:            transaction.ContractName,
			Symbol:          transaction.ContractSymbol,
			Network:         transaction.Network,
			ErcType:         transaction.ErcType,
			LogoURL:         fmt.Sprintf(logoFormat, transaction.ContractAddress),
			BannerURL:       fmt.Sprintf(bannerFormat, transaction.ContractAddress),
			Verified:        false,
			PriceSymbol:     transaction.TradeSymbol,
		}
	}

	//todo add cache
	c, err := cl.NewCollection(common.HexToAddress(transaction.ContractAddress), a.listener.GetHTTPClient())
	if err == nil {
		if owner, err := c.Owner(nil); err == nil {
			collection.Owner = owner.Hex()
		}
	}
	a.Cache.Set(uuid, collection, 0)
	return a.Repo.UpdateNFTCollectionInfo(ctx, collection)
}

func (a *App) insertAllTokensCollection(ctx context.Context, transaction *dao.Transaction) (items int, err error) {
	chain, err := getChainType(transaction.Network)
	if err != nil {
		return 0, err
	}

	c, err := a.bankContracts.GetContractByAddress(chain, transaction.ContractAddress)
	if err != nil {
		return 0, err
	}

	var wg sync.WaitGroup
	assets := make(chan *dao.Asset)

	if totalSupply, ok := c.GetContractTotalSupply(); ok {
		for i := 0; i < totalSupply; i++ {
			wg.Add(1)
			go func(id int) {
				defer wg.Done()

				tokenURI, err := c.GetTokenURI(int64(id))
				if err != nil || len(tokenURI) == 0 {
					a.logger.Warn("failed get items", "current item", id)
					return
				}

				transaction.ContractTokenID = big.NewInt(int64(id))
				assets <- a.updateAsset(transaction)
				items++
				a.logger.Info("get items", "current item", id)
			}(i)
		}
	}

	go func() {
		wg.Wait()
		close(assets)
	}()

	for asset := range assets {
		err = a.Repo.UpdateNFTAssetInfo(ctx, asset)
		if err != nil {
			return 0, err
		}
	}
	return items, nil
}

func getChainType(network string) (nftscanapi.ChainType, error) {
	chain, err := nftscanapi.NetworkNameToChainType(network)
	if err != nil {
		return nftscanapi.Unknown, apierrors.Internal.WithDescription(
			fmt.Sprintf("unknown chain type for network: %s", network))
	}
	return chain, nil
}
