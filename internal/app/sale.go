package app

import (
	"context"

	"github.com/ethereum/go-ethereum/core/types"
	"lab.aviproduction.org/aggregator/go-kit/models"
	"lab.aviproduction.org/aggregator/go-kit/pkg/apierrors"
)

func (a *App) ProcessingSale(ctx context.Context, log types.Log) error {
	_, err := a.createTransaction(ctx, log)
	if err != nil {
		return apierrors.Internal.WithDescription(err.Error())
	}

	switch log.Topics[0] {
	case models.OpenSeaSale.Hash():
		return a.processingOpenSeaOrder(ctx, log)
	}
	return nil
}

func (a *App) processingOpenSeaOrder(ctx context.Context, log types.Log) error {
	_, err := a.marketManager.ProcessOpenSeaSaleOrder(ctx, log)
	if err != nil {
		return err
	}
	return nil
}
