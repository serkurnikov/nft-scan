package app

import (
	"context"
	"math/big"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/ethereum/go-ethereum/core/types"
	"lab.aviproduction.org/aggregator/go-kit/models"
	nts "lab.aviproduction.org/aggregator/go-kit/pkg/nats"

	"lab.aviproduction.org/aggregator/nft-scan/internal/dao"
)

var (
	networkLastBlock uint64
)

const (
	topicsNFTDefaultNumbers = 4
)

func (a *App) Polling(ctx context.Context) error {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	updateNetworksTicker := time.NewTicker(time.Duration(a.cfg.NetworkCfg.AvgBlockAppear) * time.Second)
	for {
		select {
		case <-updateNetworksTicker.C:
			a.updateNetworks(ctx)
		case <-c:
			a.logger.Println("Shutting down...")
			a.wp.Stop()
			err := a.Repo.Disconnect(ctx)
			if err != nil {
				a.logger.Println("failed disconnect database...", err.Error())
			}
			nts.Close()
			os.Exit(0)
		}
	}
}

func (a *App) updateNetworks(ctx context.Context) {
	if networkLastBlock == 0 {
		block, err := a.listener.GetLastBlock(ctx)
		if err != nil {
			a.logger.PrintErr("updateNetworks", "err", err.Error())
			return
		}
		networkLastBlock = block
	}
	a.logger.Debug("updateNetworks", "networkLastBlock", networkLastBlock)
	networkInfo := a.listener.GetNetworkInfo()

	lastBlock, err := a.listener.GetLastBlock(ctx)
	if err != nil {
		a.logger.PrintErr("updateNetworks", "err", err.Error())
		return
	}
	lastBlock--
	firstBlock := networkLastBlock + 1

	networkInfoCopy := *networkInfo
	blockTSMap := make(map[int64]time.Time)
	var blockTSMapM sync.Mutex
	var blockTSMapWg sync.WaitGroup

	for currentBlock := firstBlock; currentBlock <= lastBlock; currentBlock++ {
		blockTSMapWg.Add(1)
		networkLastBlock = currentBlock

		b := currentBlock
		go func() {
			defer blockTSMapWg.Done()

			headerEvent, e := a.listener.GetHeaderByNumber(ctx, big.NewInt(int64(b)))
			if e != nil {
				a.logger.PrintErr("updateNetworks", "can't get header for block", b, "err", e.Error())
				return
			}

			if err = a.processBlock(ctx, headerEvent, networkInfoCopy); err != nil {
				a.logger.PrintErr("can't processBlock", "err", err.Error())
				return
			}

			blockTSMapM.Lock()
			blockTSMap[int64(b)] = time.Unix(int64(headerEvent.Time), 0)
			blockTSMapM.Unlock()
		}()
	}
	blockTSMapWg.Wait()

	logs, err := a.listener.GetHistoryLogs(ctx, firstBlock, lastBlock, nil, a.cfg.Topics)
	if err != nil {
		a.logger.Println("Failed GetHistoryLogs:", err.Error())
	}
	a.logger.Println("Loaded logs from blocks:", firstBlock, lastBlock, "(", len(logs), ")")

	//nolint
	for _, eventLog := range logs {
		currentLog := eventLog

		switch currentLog.Topics[0] {
		case models.OpenSeaSale.Hash():
			a.wp.Submit(func() {
				if err = a.ProcessingSale(ctx, currentLog); err != nil {
					a.logger.Debug("Failed processing sale:", "desc", err.Error())
				}
			})
		case models.Transfer.Hash():
			a.wp.Submit(func() {
				if err = a.ProcessingTransfer(ctx, currentLog); err != nil {
					a.logger.Debug("Failed processing transfer:", "address", err.Error())
				}
			})
		}
	}
	a.logger.Println("Complete processing for block:", firstBlock, lastBlock, "pool size:", a.wp.WaitingQueueSize())
}

func (a *App) processBlock(ctx context.Context, header *types.Header, network dao.NetworkInfo) error {
	a.logger.Debug("processBlock", "network name", network.Name, "new block", header.Number)
	return a.Repo.UpdateBlock(ctx, time.Unix(int64(header.Time), 0), network.Name, header.Number.Uint64())
}
