package app

import (
	"context"
	"fmt"
	"time"

	"github.com/ethereum/go-ethereum/core/types"
	"lab.aviproduction.org/aggregator/go-kit/models"
	"lab.aviproduction.org/aggregator/go-kit/pkg/apierrors"

	"lab.aviproduction.org/aggregator/nft-scan/internal/dao"
	"lab.aviproduction.org/aggregator/nft-scan/pkg/eth/contracts/erc721"
)

func (a *App) createTransaction(ctx context.Context, eventLog types.Log) (*dao.Transaction, error) {
	ercType, err := a.checkErcType(eventLog.Address)
	if err != nil {
		return nil, err
	}

	transaction := &dao.Transaction{
		Hash:            eventLog.TxHash.Hex(),
		From:            eventLog.Topics[1].Hex(),
		To:              eventLog.Topics[2].Hex(),
		BlockNumber:     eventLog.BlockNumber,
		BlockHash:       eventLog.BlockHash.Hex(),
		Timestamp:       time.Now(),
		ContractAddress: eventLog.Address.Hex(),
		ContractTokenID: eventLog.Topics[3].Big(),
		Network:         a.listener.GetNetworkInfo().Name,
		TradeSymbol:     a.listener.GetNetworkInfo().CurrencySymbol,
		EventType:       checkEventType(eventLog),
		ErcType:         ercType,
	}

	if tx, err := a.listener.GetTransactionByHash(eventLog.TxHash); err == nil {
		transaction.GasPrice = tx.GasPrice()
		transaction.GasFee = tx.Gas()
		transaction.To = tx.To().Hex()
	}

	switch transaction.ErcType {
	case dao.ERC721:
		if res, err := a.getTokenInstance(eventLog.Address, dao.ERC721); err == nil {
			t := res.(*erc721.Erc721)

			if contractName, err := t.Name(nil); err == nil && contractName != "" {
				transaction.ContractName = contractName
			} else {
				return nil, err
			}

			if contractSymbol, err := t.Symbol(nil); err == nil && contractSymbol != "" {
				transaction.ContractSymbol = contractSymbol
			} else {
				return nil, err
			}
		}
	case dao.ERC1155:
	}

	if err = a.Repo.CreateNFTTransaction(ctx, transaction); err != nil {
		return nil, apierrors.Internal.WithDescription(fmt.Sprintf("failed createNFTTransaction: %s", err.Error()))
	}
	return transaction, nil
}

func checkEventType(log types.Log) dao.EventType {
	if log.Topics[0].Hex() == dao.EmptyAddress {
		return dao.Mint
	}

	if log.Topics[1].Hex() == dao.EmptyAddress {
		return dao.Burn
	}

	switch log.Topics[0] {
	case models.OpenSeaSale.Hash():
		return dao.Sale
	}
	return dao.Transferred
}
