package app

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/ethereum/go-ethereum/common"
	"lab.aviproduction.org/aggregator/go-kit/pkg/apierrors"

	"lab.aviproduction.org/aggregator/nft-scan/internal/dao"
	alchemyapi "lab.aviproduction.org/aggregator/nft-scan/internal/proxying/alchemy"
	"lab.aviproduction.org/aggregator/nft-scan/pkg/eth/contracts/erc721"
)

const (
	ipfsURL = "https://%s/ipfs/%s"

	ipfs    = "ipfs://"
	httpURL = "http"
)

var gatewayURLs = []string{
	"ipfs.io",
	"gateway.ipfs.io",
	"storry.tv",
	"cloudflare-ipfs.com",
	"cf-ipfs.com",
}

func (a *App) updateAsset(transaction *dao.Transaction) *dao.Asset {
	var asset *dao.Asset
	uuid := fmt.Sprintf(uuidFormat,
		transaction.Network,
		transaction.ContractAddress,
		transaction.ContractTokenID.String())

	data, ok := a.Cache.Get(uuid)
	if ok {
		asset = data.(*dao.Asset)
	} else {
		asset = &dao.Asset{
			UUID:            uuid,
			ContractAddress: transaction.ContractAddress,
			ContractName:    transaction.ContractName,
			ContractTokenID: transaction.ContractTokenID.String(),
			Network:         transaction.Network,
			ErcType:         transaction.ErcType,
		}
	}

	switch transaction.ErcType {
	case dao.ERC721:
		if res, err := a.getTokenInstance(common.HexToAddress(transaction.ContractAddress), dao.ERC721); err == nil {
			t := res.(*erc721.Erc721)

			owner, err := t.OwnerOf(nil, transaction.ContractTokenID)
			if err == nil {
				asset.Owner = owner.Hex()
			}

			if len(asset.TokenURI) == 0 {
				asset.Amount = dao.SingleToken
				URI, err := t.TokenURI(nil, transaction.ContractTokenID)
				if err == nil {
					asset.TokenURI = URI
					metadata, err := a.ParseMetadataJson(URI)
					if err != nil {
						asset.Error = err.Error()
					}
					asset.Name = metadata.Name
					asset.Attributes = metadata.Attributes
					asset.ImageURI = metadata.Image
					asset.Description = metadata.Description

					JSON, err := json.Marshal(metadata)
					if err == nil {
						asset.MetadataJSON = string(JSON)
					}
				}
			}
		}
	case dao.ERC1155:
	}
	a.Cache.Set(uuid, asset, 0)
	return asset
}

func (a *App) ParseMetadataJson(tokenURI string) (*dao.MetadataJSON, error) {
	client := alchemyapi.NewClient(a.logger)

	if tokenURI == "" {
		return &dao.MetadataJSON{}, apierrors.Validation.WithDescription("tokenURI is invalid")
	}

	var search []string
	if strings.Contains(tokenURI, httpURL) {
		search = []string{tokenURI}
	}

	if strings.Contains(tokenURI, ipfs) {
		for _, u := range gatewayURLs {
			search = append(search, fmt.Sprintf(ipfsURL, u, strings.Replace(tokenURI, ipfs, "", 1)))
		}
	}

	for _, url := range search {
		metadata, err := client.GetAssetMetaData(context.Background(), url)
		if err != nil {
			continue
		}
		return metadata, err
	}
	return &dao.MetadataJSON{}, apierrors.NotFound.WithDescription("failed fetch metadata")
}
