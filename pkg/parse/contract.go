package parse

import (
	"context"
	"math/big"
	"strconv"
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"

	abiapi "lab.aviproduction.org/aggregator/nft-scan/internal/proxying/abiapi"

	nftscanapi "lab.aviproduction.org/aggregator/nft-scan/internal/proxying/nftscan"
	"lab.aviproduction.org/aggregator/nft-scan/pkg/caller"
	"lab.aviproduction.org/aggregator/nft-scan/pkg/eth"
)

const (
	owner             = "owner"
	balanceOfMethod   = "balanceOf"
	tokenURIMethod    = "tokenURI"
	totalSupplyMethod = "totalSupply"
	royaltyInfoMethod = "royaltyInfo"
)

type Contract struct {
	Address        string               `bson:"address" json:"address"`
	ChainType      nftscanapi.ChainType `bson:"chainType" json:"chainType"`
	AbiPackSource  string               `bson:"abiPackSource" json:"abiPackSource"`
	ABI            *abi.ABI             `bson:"abi" json:"abi"`
	contractCaller *caller.Caller
}

type ContractInterface interface {
	GetTokenURI(tokenID int) string
	GetContractTotalSupply() (int, bool)
	GetBalanceOf() (uint, bool)
	GetOwner() (common.Address, error)
}

func initiateContract(client *abiapi.Client, listener *eth.Listener, chainType nftscanapi.ChainType, address string) (*Contract, error) {
	abiSource, err := client.GetAbiSource(context.Background(), abiapi.AbiRequest{
		Address:   address,
		ChainType: chainType,
	})
	if err != nil {
		return nil, err
	}

	parsedAbi, err := abi.JSON(strings.NewReader(abiSource.Result))
	if err != nil {
		return nil, err
	}

	c, err := caller.InitiateContractCaller(address, abiSource.Result, listener.GetHTTPClient())
	if err != nil {
		return nil, err
	}

	return &Contract{
		Address:        address,
		ChainType:      chainType,
		ABI:            &parsedAbi,
		AbiPackSource:  abiSource.Result,
		contractCaller: c,
	}, nil
}

func (c *Contract) GetOwner() (common.Address, error) {
	out, err := c.contractCaller.Call(owner)
	if err != nil {
		return *new(common.Address), err
	}
	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)
	return out0, nil
}

func (c *Contract) GetTokenURI(id int64) (string, error) {
	tokenId := big.NewInt(id)
	out, err := c.contractCaller.CallWithParameter(tokenURIMethod, &tokenId)
	if err != nil {
		return "", err
	}
	return out[0].(string), nil
}

func (c *Contract) GetContractTotalSupply() (int, bool) {
	out, err := c.contractCaller.Call(totalSupplyMethod)
	if err != nil {
		return 0, false
	}

	result, err := strconv.Atoi((*abi.ConvertType(out[0], new(*big.Int)).(**big.Int)).String())
	if err != nil {
		return 0, false
	}
	return result, true
}

func (c *Contract) GetBalanceOf(owner common.Address) (uint, bool) {
	out, err := c.contractCaller.CallWithParameter(balanceOfMethod, owner)
	if err != nil {
		return 0, false
	}
	return out[0].(uint), true
}
