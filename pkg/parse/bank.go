package parse

import (
	"github.com/powerman/structlog"

	nftscanapi "lab.aviproduction.org/aggregator/nft-scan/internal/proxying/nftscan"
	"lab.aviproduction.org/aggregator/nft-scan/pkg/eth"

	api "lab.aviproduction.org/aggregator/nft-scan/internal/proxying/abiapi"
)

type Contracts map[string]*Contract
type BankContracts struct {
	client         *api.Client
	listener       *eth.Listener
	chainContracts map[nftscanapi.ChainType]Contracts
}

func New(listener *eth.Listener, logger *structlog.Logger) *BankContracts {
	abiClient := api.NewClient(logger)
	chainContracts := make(map[nftscanapi.ChainType]Contracts)

	for _, chainType := range nftscanapi.GetSupportedMarketPlaceChainTypes() {
		chainContracts[chainType] = make(map[string]*Contract)
	}
	return &BankContracts{
		client:         abiClient,
		listener:       listener,
		chainContracts: chainContracts,
	}
}

func (b *BankContracts) GetContractByAddress(chain nftscanapi.ChainType, a string) (*Contract, error) {
	if c, ok := b.chainContracts[chain][a]; ok {
		return c, nil
	}

	c, err := initiateContract(b.client, b.listener, chain, a)
	if err != nil {
		return nil, err
	}

	b.chainContracts[chain][a] = c
	return c, nil
}
