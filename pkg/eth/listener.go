package eth

import (
	"context"
	"fmt"
	"math/big"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/powerman/structlog"
	"lab.aviproduction.org/aggregator/go-kit/pkg/apierrors"
	"lab.aviproduction.org/aggregator/go-kit/pkg/cache/memory"

	"lab.aviproduction.org/aggregator/nft-scan/config"
	"lab.aviproduction.org/aggregator/nft-scan/internal/dao"
	"lab.aviproduction.org/aggregator/nft-scan/pkg/eth/uniswapv2"
)

type Listener struct {
	TokenABI           abi.ABI
	lastBlockNumber    *big.Int
	lastBlockTimestamp time.Time

	networkConfig *config.NetworkConfig
	httpClient    *ethclient.Client
	wsClient      *ethclient.Client

	transactionCache *memory.Storage
}

func InitListener(cfg *config.Configuration, logger *structlog.Logger) *Listener {
	var err error
	l := Listener{}

	l.networkConfig = &cfg.NetworkCfg

	logger.Println(l.networkConfig.Name, "HTTP = ", l.networkConfig.HTTP)

	l.httpClient, err = ethclient.Dial(l.networkConfig.HTTP)
	if err != nil {
		logger.Fatalln("InitListener", "could not connect to ", l.networkConfig.Name, " HTTP gateway:", err.Error())
	}

	if l.networkConfig.Ws != "" {
		logger.Println(l.networkConfig.Name, "WS =", l.networkConfig.Ws)
		l.wsClient, err = ethclient.Dial(l.networkConfig.Ws)
		if err != nil {
			logger.Fatalln("InitListener", "could not connect to ", l.networkConfig.Name, " WS gateway:", err)
		}
	}

	l.TokenABI, err = abi.JSON(strings.NewReader(uniswapv2.Uniswapv2MetaData.ABI))
	if err != nil {
		logger.Fatalln("InitListener", "tokenABI err", err.Error())
	}

	l.lastBlockNumber = big.NewInt(0)
	l.lastBlockTimestamp = time.Now()

	l.transactionCache = memory.InitCache(10*time.Minute, 20*time.Minute)
	return &l
}

func (l *Listener) GetHTTPClient() *ethclient.Client {
	return l.httpClient
}

func (l *Listener) CloseHTTPClient() {
	l.httpClient.Close()
}

func (l *Listener) GetLastBlock(ctx context.Context) (uint64, error) {
	if l.httpClient == nil {
		return 0, apierrors.Internal.WithDescription("No active http connection")
	}
	return l.httpClient.BlockNumber(ctx)
}

func (l *Listener) GetHeaderByNumber(ctx context.Context, number *big.Int) (*types.Header, error) {
	if l.httpClient == nil {
		return nil, apierrors.Internal.WithDescription("No active http connection")
	}
	return l.httpClient.HeaderByNumber(ctx, number)
}

func (l *Listener) GetNetworkInfo() *dao.NetworkInfo {
	return &dao.NetworkInfo{
		Name:           l.networkConfig.Name,
		CurrencySymbol: l.networkConfig.CurrencySymbol,
	}
}

func (l *Listener) GetHistoryLogs(ctx context.Context, fromBlock, toBlock uint64, addresses []common.Address, topics [][]common.Hash) ([]types.Log, error) {
	if l.httpClient == nil {
		return nil, apierrors.Internal.WithDescription("No active http connection")
	}

	query := ethereum.FilterQuery{
		FromBlock: big.NewInt(int64(fromBlock)),
		ToBlock:   big.NewInt(int64(toBlock)),
		Addresses: addresses,
		Topics:    topics,
	}

	logs, err := l.httpClient.FilterLogs(ctx, query)
	if err != nil {
		return nil, err
	}
	return logs, nil
}

func (l *Listener) GetLogsByTxHash(txHash common.Hash) ([]*types.Log, error) {
	if l.httpClient == nil {
		return nil, apierrors.Internal.WithDescription("No active http connection")
	}

	receipt, err := l.httpClient.TransactionReceipt(context.Background(), txHash)
	if err != nil {
		return nil, apierrors.Internal.WithDescription(fmt.Sprintf("TransactionReceipt: %v", err.Error()))
	}
	return receipt.Logs, nil
}

func (l *Listener) GetTransactionByHash(txHash common.Hash) (*types.Transaction, error) {
	if l.httpClient == nil {
		return nil, apierrors.Internal.WithDescription("No active http connection")
	}

	if tx, ok := l.transactionCache.Get(txHash.Hex()); ok {
		return tx.(*types.Transaction), nil
	}

	tx, _, err := l.httpClient.TransactionByHash(context.Background(), txHash)
	if err != nil {
		return nil, err
	}
	l.transactionCache.Set(txHash.Hex(), tx, 0)
	return tx, nil
}
