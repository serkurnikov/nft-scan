// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package rarible_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Rarible0MetaData contains all meta data concerning the Rarible0 contract.
var Rarible0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_logic\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"admin_\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"_data\",\"type\":\"bytes\"}],\"stateMutability\":\"payable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"previousAdmin\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"newAdmin\",\"type\":\"address\"}],\"name\":\"AdminChanged\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"beacon\",\"type\":\"address\"}],\"name\":\"BeaconUpgraded\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"implementation\",\"type\":\"address\"}],\"name\":\"Upgraded\",\"type\":\"event\"},{\"stateMutability\":\"payable\",\"type\":\"fallback\"},{\"inputs\":[],\"name\":\"admin\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"admin_\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newAdmin\",\"type\":\"address\"}],\"name\":\"changeAdmin\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"implementation\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"implementation_\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newImplementation\",\"type\":\"address\"}],\"name\":\"upgradeTo\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newImplementation\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"data\",\"type\":\"bytes\"}],\"name\":\"upgradeToAndCall\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
}

// Rarible0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Rarible0MetaData.ABI instead.
var Rarible0ABI = Rarible0MetaData.ABI

// Rarible0 is an auto generated Go binding around an Ethereum contract.
type Rarible0 struct {
	Rarible0Caller     // Read-only binding to the contract
	Rarible0Transactor // Write-only binding to the contract
	Rarible0Filterer   // Log filterer for contract events
}

// Rarible0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Rarible0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Rarible0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Rarible0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Rarible0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Rarible0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Rarible0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Rarible0Session struct {
	Contract     *Rarible0         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Rarible0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Rarible0CallerSession struct {
	Contract *Rarible0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// Rarible0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Rarible0TransactorSession struct {
	Contract     *Rarible0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// Rarible0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Rarible0Raw struct {
	Contract *Rarible0 // Generic contract binding to access the raw methods on
}

// Rarible0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Rarible0CallerRaw struct {
	Contract *Rarible0Caller // Generic read-only contract binding to access the raw methods on
}

// Rarible0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Rarible0TransactorRaw struct {
	Contract *Rarible0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewRarible0 creates a new instance of Rarible0, bound to a specific deployed contract.
func NewRarible0(address common.Address, backend bind.ContractBackend) (*Rarible0, error) {
	contract, err := bindRarible0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Rarible0{Rarible0Caller: Rarible0Caller{contract: contract}, Rarible0Transactor: Rarible0Transactor{contract: contract}, Rarible0Filterer: Rarible0Filterer{contract: contract}}, nil
}

// NewRarible0Caller creates a new read-only instance of Rarible0, bound to a specific deployed contract.
func NewRarible0Caller(address common.Address, caller bind.ContractCaller) (*Rarible0Caller, error) {
	contract, err := bindRarible0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Rarible0Caller{contract: contract}, nil
}

// NewRarible0Transactor creates a new write-only instance of Rarible0, bound to a specific deployed contract.
func NewRarible0Transactor(address common.Address, transactor bind.ContractTransactor) (*Rarible0Transactor, error) {
	contract, err := bindRarible0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Rarible0Transactor{contract: contract}, nil
}

// NewRarible0Filterer creates a new log filterer instance of Rarible0, bound to a specific deployed contract.
func NewRarible0Filterer(address common.Address, filterer bind.ContractFilterer) (*Rarible0Filterer, error) {
	contract, err := bindRarible0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Rarible0Filterer{contract: contract}, nil
}

// bindRarible0 binds a generic wrapper to an already deployed contract.
func bindRarible0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Rarible0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Rarible0 *Rarible0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Rarible0.Contract.Rarible0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Rarible0 *Rarible0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Rarible0.Contract.Rarible0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Rarible0 *Rarible0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Rarible0.Contract.Rarible0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Rarible0 *Rarible0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Rarible0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Rarible0 *Rarible0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Rarible0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Rarible0 *Rarible0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Rarible0.Contract.contract.Transact(opts, method, params...)
}

// Admin is a paid mutator transaction binding the contract method 0xf851a440.
//
// Solidity: function admin() returns(address admin_)
func (_Rarible0 *Rarible0Transactor) Admin(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Rarible0.contract.Transact(opts, "admin")
}

// Admin is a paid mutator transaction binding the contract method 0xf851a440.
//
// Solidity: function admin() returns(address admin_)
func (_Rarible0 *Rarible0Session) Admin() (*types.Transaction, error) {
	return _Rarible0.Contract.Admin(&_Rarible0.TransactOpts)
}

// Admin is a paid mutator transaction binding the contract method 0xf851a440.
//
// Solidity: function admin() returns(address admin_)
func (_Rarible0 *Rarible0TransactorSession) Admin() (*types.Transaction, error) {
	return _Rarible0.Contract.Admin(&_Rarible0.TransactOpts)
}

// ChangeAdmin is a paid mutator transaction binding the contract method 0x8f283970.
//
// Solidity: function changeAdmin(address newAdmin) returns()
func (_Rarible0 *Rarible0Transactor) ChangeAdmin(opts *bind.TransactOpts, newAdmin common.Address) (*types.Transaction, error) {
	return _Rarible0.contract.Transact(opts, "changeAdmin", newAdmin)
}

// ChangeAdmin is a paid mutator transaction binding the contract method 0x8f283970.
//
// Solidity: function changeAdmin(address newAdmin) returns()
func (_Rarible0 *Rarible0Session) ChangeAdmin(newAdmin common.Address) (*types.Transaction, error) {
	return _Rarible0.Contract.ChangeAdmin(&_Rarible0.TransactOpts, newAdmin)
}

// ChangeAdmin is a paid mutator transaction binding the contract method 0x8f283970.
//
// Solidity: function changeAdmin(address newAdmin) returns()
func (_Rarible0 *Rarible0TransactorSession) ChangeAdmin(newAdmin common.Address) (*types.Transaction, error) {
	return _Rarible0.Contract.ChangeAdmin(&_Rarible0.TransactOpts, newAdmin)
}

// Implementation is a paid mutator transaction binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() returns(address implementation_)
func (_Rarible0 *Rarible0Transactor) Implementation(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Rarible0.contract.Transact(opts, "implementation")
}

// Implementation is a paid mutator transaction binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() returns(address implementation_)
func (_Rarible0 *Rarible0Session) Implementation() (*types.Transaction, error) {
	return _Rarible0.Contract.Implementation(&_Rarible0.TransactOpts)
}

// Implementation is a paid mutator transaction binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() returns(address implementation_)
func (_Rarible0 *Rarible0TransactorSession) Implementation() (*types.Transaction, error) {
	return _Rarible0.Contract.Implementation(&_Rarible0.TransactOpts)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address newImplementation) returns()
func (_Rarible0 *Rarible0Transactor) UpgradeTo(opts *bind.TransactOpts, newImplementation common.Address) (*types.Transaction, error) {
	return _Rarible0.contract.Transact(opts, "upgradeTo", newImplementation)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address newImplementation) returns()
func (_Rarible0 *Rarible0Session) UpgradeTo(newImplementation common.Address) (*types.Transaction, error) {
	return _Rarible0.Contract.UpgradeTo(&_Rarible0.TransactOpts, newImplementation)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address newImplementation) returns()
func (_Rarible0 *Rarible0TransactorSession) UpgradeTo(newImplementation common.Address) (*types.Transaction, error) {
	return _Rarible0.Contract.UpgradeTo(&_Rarible0.TransactOpts, newImplementation)
}

// UpgradeToAndCall is a paid mutator transaction binding the contract method 0x4f1ef286.
//
// Solidity: function upgradeToAndCall(address newImplementation, bytes data) payable returns()
func (_Rarible0 *Rarible0Transactor) UpgradeToAndCall(opts *bind.TransactOpts, newImplementation common.Address, data []byte) (*types.Transaction, error) {
	return _Rarible0.contract.Transact(opts, "upgradeToAndCall", newImplementation, data)
}

// UpgradeToAndCall is a paid mutator transaction binding the contract method 0x4f1ef286.
//
// Solidity: function upgradeToAndCall(address newImplementation, bytes data) payable returns()
func (_Rarible0 *Rarible0Session) UpgradeToAndCall(newImplementation common.Address, data []byte) (*types.Transaction, error) {
	return _Rarible0.Contract.UpgradeToAndCall(&_Rarible0.TransactOpts, newImplementation, data)
}

// UpgradeToAndCall is a paid mutator transaction binding the contract method 0x4f1ef286.
//
// Solidity: function upgradeToAndCall(address newImplementation, bytes data) payable returns()
func (_Rarible0 *Rarible0TransactorSession) UpgradeToAndCall(newImplementation common.Address, data []byte) (*types.Transaction, error) {
	return _Rarible0.Contract.UpgradeToAndCall(&_Rarible0.TransactOpts, newImplementation, data)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Rarible0 *Rarible0Transactor) Fallback(opts *bind.TransactOpts, calldata []byte) (*types.Transaction, error) {
	return _Rarible0.contract.RawTransact(opts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Rarible0 *Rarible0Session) Fallback(calldata []byte) (*types.Transaction, error) {
	return _Rarible0.Contract.Fallback(&_Rarible0.TransactOpts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Rarible0 *Rarible0TransactorSession) Fallback(calldata []byte) (*types.Transaction, error) {
	return _Rarible0.Contract.Fallback(&_Rarible0.TransactOpts, calldata)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Rarible0 *Rarible0Transactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Rarible0.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Rarible0 *Rarible0Session) Receive() (*types.Transaction, error) {
	return _Rarible0.Contract.Receive(&_Rarible0.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Rarible0 *Rarible0TransactorSession) Receive() (*types.Transaction, error) {
	return _Rarible0.Contract.Receive(&_Rarible0.TransactOpts)
}

// Rarible0AdminChangedIterator is returned from FilterAdminChanged and is used to iterate over the raw logs and unpacked data for AdminChanged events raised by the Rarible0 contract.
type Rarible0AdminChangedIterator struct {
	Event *Rarible0AdminChanged // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Rarible0AdminChangedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Rarible0AdminChanged)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Rarible0AdminChanged)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Rarible0AdminChangedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Rarible0AdminChangedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Rarible0AdminChanged represents a AdminChanged event raised by the Rarible0 contract.
type Rarible0AdminChanged struct {
	PreviousAdmin common.Address
	NewAdmin      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterAdminChanged is a free log retrieval operation binding the contract event 0x7e644d79422f17c01e4894b5f4f588d331ebfa28653d42ae832dc59e38c9798f.
//
// Solidity: event AdminChanged(address previousAdmin, address newAdmin)
func (_Rarible0 *Rarible0Filterer) FilterAdminChanged(opts *bind.FilterOpts) (*Rarible0AdminChangedIterator, error) {

	logs, sub, err := _Rarible0.contract.FilterLogs(opts, "AdminChanged")
	if err != nil {
		return nil, err
	}
	return &Rarible0AdminChangedIterator{contract: _Rarible0.contract, event: "AdminChanged", logs: logs, sub: sub}, nil
}

// WatchAdminChanged is a free log subscription operation binding the contract event 0x7e644d79422f17c01e4894b5f4f588d331ebfa28653d42ae832dc59e38c9798f.
//
// Solidity: event AdminChanged(address previousAdmin, address newAdmin)
func (_Rarible0 *Rarible0Filterer) WatchAdminChanged(opts *bind.WatchOpts, sink chan<- *Rarible0AdminChanged) (event.Subscription, error) {

	logs, sub, err := _Rarible0.contract.WatchLogs(opts, "AdminChanged")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Rarible0AdminChanged)
				if err := _Rarible0.contract.UnpackLog(event, "AdminChanged", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminChanged is a log parse operation binding the contract event 0x7e644d79422f17c01e4894b5f4f588d331ebfa28653d42ae832dc59e38c9798f.
//
// Solidity: event AdminChanged(address previousAdmin, address newAdmin)
func (_Rarible0 *Rarible0Filterer) ParseAdminChanged(log types.Log) (*Rarible0AdminChanged, error) {
	event := new(Rarible0AdminChanged)
	if err := _Rarible0.contract.UnpackLog(event, "AdminChanged", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Rarible0BeaconUpgradedIterator is returned from FilterBeaconUpgraded and is used to iterate over the raw logs and unpacked data for BeaconUpgraded events raised by the Rarible0 contract.
type Rarible0BeaconUpgradedIterator struct {
	Event *Rarible0BeaconUpgraded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Rarible0BeaconUpgradedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Rarible0BeaconUpgraded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Rarible0BeaconUpgraded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Rarible0BeaconUpgradedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Rarible0BeaconUpgradedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Rarible0BeaconUpgraded represents a BeaconUpgraded event raised by the Rarible0 contract.
type Rarible0BeaconUpgraded struct {
	Beacon common.Address
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterBeaconUpgraded is a free log retrieval operation binding the contract event 0x1cf3b03a6cf19fa2baba4df148e9dcabedea7f8a5c07840e207e5c089be95d3e.
//
// Solidity: event BeaconUpgraded(address indexed beacon)
func (_Rarible0 *Rarible0Filterer) FilterBeaconUpgraded(opts *bind.FilterOpts, beacon []common.Address) (*Rarible0BeaconUpgradedIterator, error) {

	var beaconRule []interface{}
	for _, beaconItem := range beacon {
		beaconRule = append(beaconRule, beaconItem)
	}

	logs, sub, err := _Rarible0.contract.FilterLogs(opts, "BeaconUpgraded", beaconRule)
	if err != nil {
		return nil, err
	}
	return &Rarible0BeaconUpgradedIterator{contract: _Rarible0.contract, event: "BeaconUpgraded", logs: logs, sub: sub}, nil
}

// WatchBeaconUpgraded is a free log subscription operation binding the contract event 0x1cf3b03a6cf19fa2baba4df148e9dcabedea7f8a5c07840e207e5c089be95d3e.
//
// Solidity: event BeaconUpgraded(address indexed beacon)
func (_Rarible0 *Rarible0Filterer) WatchBeaconUpgraded(opts *bind.WatchOpts, sink chan<- *Rarible0BeaconUpgraded, beacon []common.Address) (event.Subscription, error) {

	var beaconRule []interface{}
	for _, beaconItem := range beacon {
		beaconRule = append(beaconRule, beaconItem)
	}

	logs, sub, err := _Rarible0.contract.WatchLogs(opts, "BeaconUpgraded", beaconRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Rarible0BeaconUpgraded)
				if err := _Rarible0.contract.UnpackLog(event, "BeaconUpgraded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBeaconUpgraded is a log parse operation binding the contract event 0x1cf3b03a6cf19fa2baba4df148e9dcabedea7f8a5c07840e207e5c089be95d3e.
//
// Solidity: event BeaconUpgraded(address indexed beacon)
func (_Rarible0 *Rarible0Filterer) ParseBeaconUpgraded(log types.Log) (*Rarible0BeaconUpgraded, error) {
	event := new(Rarible0BeaconUpgraded)
	if err := _Rarible0.contract.UnpackLog(event, "BeaconUpgraded", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Rarible0UpgradedIterator is returned from FilterUpgraded and is used to iterate over the raw logs and unpacked data for Upgraded events raised by the Rarible0 contract.
type Rarible0UpgradedIterator struct {
	Event *Rarible0Upgraded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Rarible0UpgradedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Rarible0Upgraded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Rarible0Upgraded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Rarible0UpgradedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Rarible0UpgradedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Rarible0Upgraded represents a Upgraded event raised by the Rarible0 contract.
type Rarible0Upgraded struct {
	Implementation common.Address
	Raw            types.Log // Blockchain specific contextual infos
}

// FilterUpgraded is a free log retrieval operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Rarible0 *Rarible0Filterer) FilterUpgraded(opts *bind.FilterOpts, implementation []common.Address) (*Rarible0UpgradedIterator, error) {

	var implementationRule []interface{}
	for _, implementationItem := range implementation {
		implementationRule = append(implementationRule, implementationItem)
	}

	logs, sub, err := _Rarible0.contract.FilterLogs(opts, "Upgraded", implementationRule)
	if err != nil {
		return nil, err
	}
	return &Rarible0UpgradedIterator{contract: _Rarible0.contract, event: "Upgraded", logs: logs, sub: sub}, nil
}

// WatchUpgraded is a free log subscription operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Rarible0 *Rarible0Filterer) WatchUpgraded(opts *bind.WatchOpts, sink chan<- *Rarible0Upgraded, implementation []common.Address) (event.Subscription, error) {

	var implementationRule []interface{}
	for _, implementationItem := range implementation {
		implementationRule = append(implementationRule, implementationItem)
	}

	logs, sub, err := _Rarible0.contract.WatchLogs(opts, "Upgraded", implementationRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Rarible0Upgraded)
				if err := _Rarible0.contract.UnpackLog(event, "Upgraded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUpgraded is a log parse operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Rarible0 *Rarible0Filterer) ParseUpgraded(log types.Log) (*Rarible0Upgraded, error) {
	event := new(Rarible0Upgraded)
	if err := _Rarible0.contract.UnpackLog(event, "Upgraded", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
