// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package opensea_1

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// LibOrderOrder is an auto generated low-level Go binding around an user-defined struct.
type LibOrderOrder struct {
	MakerAddress          common.Address
	TakerAddress          common.Address
	FeeRecipientAddress   common.Address
	SenderAddress         common.Address
	MakerAssetAmount      *big.Int
	TakerAssetAmount      *big.Int
	MakerFee              *big.Int
	TakerFee              *big.Int
	ExpirationTimeSeconds *big.Int
	Salt                  *big.Int
	MakerAssetData        []byte
	TakerAssetData        []byte
	MakerFeeAssetData     []byte
	TakerFeeAssetData     []byte
}

// ZeroExFeeWrapperFeeData is an auto generated low-level Go binding around an user-defined struct.
type ZeroExFeeWrapperFeeData struct {
	Recipient          common.Address
	PaymentTokenAmount *big.Int
}

// Opensea1MetaData contains all meta data concerning the Opensea1 contract.
var Opensea1MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"exchange\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"inputs\":[],\"name\":\"getExchange\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"}],\"name\":\"isOwner\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"makerAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"takerAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"feeRecipientAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"senderAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"makerAssetAmount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"takerAssetAmount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"makerFee\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"takerFee\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"expirationTimeSeconds\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"makerAssetData\",\"type\":\"bytes\"},{\"internalType\":\"bytes\",\"name\":\"takerAssetData\",\"type\":\"bytes\"},{\"internalType\":\"bytes\",\"name\":\"makerFeeAssetData\",\"type\":\"bytes\"},{\"internalType\":\"bytes\",\"name\":\"takerFeeAssetData\",\"type\":\"bytes\"}],\"internalType\":\"structLibOrder.Order\",\"name\":\"leftOrder\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"makerAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"takerAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"feeRecipientAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"senderAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"makerAssetAmount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"takerAssetAmount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"makerFee\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"takerFee\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"expirationTimeSeconds\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"makerAssetData\",\"type\":\"bytes\"},{\"internalType\":\"bytes\",\"name\":\"takerAssetData\",\"type\":\"bytes\"},{\"internalType\":\"bytes\",\"name\":\"makerFeeAssetData\",\"type\":\"bytes\"},{\"internalType\":\"bytes\",\"name\":\"takerFeeAssetData\",\"type\":\"bytes\"}],\"internalType\":\"structLibOrder.Order\",\"name\":\"rightOrder\",\"type\":\"tuple\"},{\"internalType\":\"bytes\",\"name\":\"leftSignature\",\"type\":\"bytes\"},{\"internalType\":\"bytes\",\"name\":\"rightSignature\",\"type\":\"bytes\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"paymentTokenAmount\",\"type\":\"uint256\"}],\"internalType\":\"structZeroExFeeWrapper.FeeData[]\",\"name\":\"feeData\",\"type\":\"tuple[]\"},{\"internalType\":\"address\",\"name\":\"paymentTokenAddress\",\"type\":\"address\"}],\"name\":\"matchOrders\",\"outputs\":[{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"target\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"callData\",\"type\":\"bytes\"}],\"name\":\"proxyCall\",\"outputs\":[{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"exchange\",\"type\":\"address\"}],\"name\":\"setExchange\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"isOwner\",\"type\":\"bool\"}],\"name\":\"setOwner\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// Opensea1ABI is the input ABI used to generate the binding from.
// Deprecated: Use Opensea1MetaData.ABI instead.
var Opensea1ABI = Opensea1MetaData.ABI

// Opensea1 is an auto generated Go binding around an Ethereum contract.
type Opensea1 struct {
	Opensea1Caller     // Read-only binding to the contract
	Opensea1Transactor // Write-only binding to the contract
	Opensea1Filterer   // Log filterer for contract events
}

// Opensea1Caller is an auto generated read-only Go binding around an Ethereum contract.
type Opensea1Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Opensea1Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Opensea1Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Opensea1Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Opensea1Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Opensea1Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Opensea1Session struct {
	Contract     *Opensea1         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Opensea1CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Opensea1CallerSession struct {
	Contract *Opensea1Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// Opensea1TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Opensea1TransactorSession struct {
	Contract     *Opensea1Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// Opensea1Raw is an auto generated low-level Go binding around an Ethereum contract.
type Opensea1Raw struct {
	Contract *Opensea1 // Generic contract binding to access the raw methods on
}

// Opensea1CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Opensea1CallerRaw struct {
	Contract *Opensea1Caller // Generic read-only contract binding to access the raw methods on
}

// Opensea1TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Opensea1TransactorRaw struct {
	Contract *Opensea1Transactor // Generic write-only contract binding to access the raw methods on
}

// NewOpensea1 creates a new instance of Opensea1, bound to a specific deployed contract.
func NewOpensea1(address common.Address, backend bind.ContractBackend) (*Opensea1, error) {
	contract, err := bindOpensea1(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Opensea1{Opensea1Caller: Opensea1Caller{contract: contract}, Opensea1Transactor: Opensea1Transactor{contract: contract}, Opensea1Filterer: Opensea1Filterer{contract: contract}}, nil
}

// NewOpensea1Caller creates a new read-only instance of Opensea1, bound to a specific deployed contract.
func NewOpensea1Caller(address common.Address, caller bind.ContractCaller) (*Opensea1Caller, error) {
	contract, err := bindOpensea1(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Opensea1Caller{contract: contract}, nil
}

// NewOpensea1Transactor creates a new write-only instance of Opensea1, bound to a specific deployed contract.
func NewOpensea1Transactor(address common.Address, transactor bind.ContractTransactor) (*Opensea1Transactor, error) {
	contract, err := bindOpensea1(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Opensea1Transactor{contract: contract}, nil
}

// NewOpensea1Filterer creates a new log filterer instance of Opensea1, bound to a specific deployed contract.
func NewOpensea1Filterer(address common.Address, filterer bind.ContractFilterer) (*Opensea1Filterer, error) {
	contract, err := bindOpensea1(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Opensea1Filterer{contract: contract}, nil
}

// bindOpensea1 binds a generic wrapper to an already deployed contract.
func bindOpensea1(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Opensea1ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Opensea1 *Opensea1Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Opensea1.Contract.Opensea1Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Opensea1 *Opensea1Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Opensea1.Contract.Opensea1Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Opensea1 *Opensea1Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Opensea1.Contract.Opensea1Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Opensea1 *Opensea1CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Opensea1.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Opensea1 *Opensea1TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Opensea1.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Opensea1 *Opensea1TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Opensea1.Contract.contract.Transact(opts, method, params...)
}

// GetExchange is a free data retrieval call binding the contract method 0xf807cd22.
//
// Solidity: function getExchange() view returns(address)
func (_Opensea1 *Opensea1Caller) GetExchange(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Opensea1.contract.Call(opts, &out, "getExchange")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetExchange is a free data retrieval call binding the contract method 0xf807cd22.
//
// Solidity: function getExchange() view returns(address)
func (_Opensea1 *Opensea1Session) GetExchange() (common.Address, error) {
	return _Opensea1.Contract.GetExchange(&_Opensea1.CallOpts)
}

// GetExchange is a free data retrieval call binding the contract method 0xf807cd22.
//
// Solidity: function getExchange() view returns(address)
func (_Opensea1 *Opensea1CallerSession) GetExchange() (common.Address, error) {
	return _Opensea1.Contract.GetExchange(&_Opensea1.CallOpts)
}

// IsOwner is a free data retrieval call binding the contract method 0x2f54bf6e.
//
// Solidity: function isOwner(address owner) view returns(bool)
func (_Opensea1 *Opensea1Caller) IsOwner(opts *bind.CallOpts, owner common.Address) (bool, error) {
	var out []interface{}
	err := _Opensea1.contract.Call(opts, &out, "isOwner", owner)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsOwner is a free data retrieval call binding the contract method 0x2f54bf6e.
//
// Solidity: function isOwner(address owner) view returns(bool)
func (_Opensea1 *Opensea1Session) IsOwner(owner common.Address) (bool, error) {
	return _Opensea1.Contract.IsOwner(&_Opensea1.CallOpts, owner)
}

// IsOwner is a free data retrieval call binding the contract method 0x2f54bf6e.
//
// Solidity: function isOwner(address owner) view returns(bool)
func (_Opensea1 *Opensea1CallerSession) IsOwner(owner common.Address) (bool, error) {
	return _Opensea1.Contract.IsOwner(&_Opensea1.CallOpts, owner)
}

// MatchOrders is a paid mutator transaction binding the contract method 0xbbbfa60c.
//
// Solidity: function matchOrders((address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes,bytes,bytes) leftOrder, (address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes,bytes,bytes) rightOrder, bytes leftSignature, bytes rightSignature, (address,uint256)[] feeData, address paymentTokenAddress) payable returns(bytes)
func (_Opensea1 *Opensea1Transactor) MatchOrders(opts *bind.TransactOpts, leftOrder LibOrderOrder, rightOrder LibOrderOrder, leftSignature []byte, rightSignature []byte, feeData []ZeroExFeeWrapperFeeData, paymentTokenAddress common.Address) (*types.Transaction, error) {
	return _Opensea1.contract.Transact(opts, "matchOrders", leftOrder, rightOrder, leftSignature, rightSignature, feeData, paymentTokenAddress)
}

// MatchOrders is a paid mutator transaction binding the contract method 0xbbbfa60c.
//
// Solidity: function matchOrders((address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes,bytes,bytes) leftOrder, (address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes,bytes,bytes) rightOrder, bytes leftSignature, bytes rightSignature, (address,uint256)[] feeData, address paymentTokenAddress) payable returns(bytes)
func (_Opensea1 *Opensea1Session) MatchOrders(leftOrder LibOrderOrder, rightOrder LibOrderOrder, leftSignature []byte, rightSignature []byte, feeData []ZeroExFeeWrapperFeeData, paymentTokenAddress common.Address) (*types.Transaction, error) {
	return _Opensea1.Contract.MatchOrders(&_Opensea1.TransactOpts, leftOrder, rightOrder, leftSignature, rightSignature, feeData, paymentTokenAddress)
}

// MatchOrders is a paid mutator transaction binding the contract method 0xbbbfa60c.
//
// Solidity: function matchOrders((address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes,bytes,bytes) leftOrder, (address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes,bytes,bytes) rightOrder, bytes leftSignature, bytes rightSignature, (address,uint256)[] feeData, address paymentTokenAddress) payable returns(bytes)
func (_Opensea1 *Opensea1TransactorSession) MatchOrders(leftOrder LibOrderOrder, rightOrder LibOrderOrder, leftSignature []byte, rightSignature []byte, feeData []ZeroExFeeWrapperFeeData, paymentTokenAddress common.Address) (*types.Transaction, error) {
	return _Opensea1.Contract.MatchOrders(&_Opensea1.TransactOpts, leftOrder, rightOrder, leftSignature, rightSignature, feeData, paymentTokenAddress)
}

// ProxyCall is a paid mutator transaction binding the contract method 0x2a31f6b4.
//
// Solidity: function proxyCall(address target, bytes callData) payable returns(bytes)
func (_Opensea1 *Opensea1Transactor) ProxyCall(opts *bind.TransactOpts, target common.Address, callData []byte) (*types.Transaction, error) {
	return _Opensea1.contract.Transact(opts, "proxyCall", target, callData)
}

// ProxyCall is a paid mutator transaction binding the contract method 0x2a31f6b4.
//
// Solidity: function proxyCall(address target, bytes callData) payable returns(bytes)
func (_Opensea1 *Opensea1Session) ProxyCall(target common.Address, callData []byte) (*types.Transaction, error) {
	return _Opensea1.Contract.ProxyCall(&_Opensea1.TransactOpts, target, callData)
}

// ProxyCall is a paid mutator transaction binding the contract method 0x2a31f6b4.
//
// Solidity: function proxyCall(address target, bytes callData) payable returns(bytes)
func (_Opensea1 *Opensea1TransactorSession) ProxyCall(target common.Address, callData []byte) (*types.Transaction, error) {
	return _Opensea1.Contract.ProxyCall(&_Opensea1.TransactOpts, target, callData)
}

// SetExchange is a paid mutator transaction binding the contract method 0x67b1f5df.
//
// Solidity: function setExchange(address exchange) returns()
func (_Opensea1 *Opensea1Transactor) SetExchange(opts *bind.TransactOpts, exchange common.Address) (*types.Transaction, error) {
	return _Opensea1.contract.Transact(opts, "setExchange", exchange)
}

// SetExchange is a paid mutator transaction binding the contract method 0x67b1f5df.
//
// Solidity: function setExchange(address exchange) returns()
func (_Opensea1 *Opensea1Session) SetExchange(exchange common.Address) (*types.Transaction, error) {
	return _Opensea1.Contract.SetExchange(&_Opensea1.TransactOpts, exchange)
}

// SetExchange is a paid mutator transaction binding the contract method 0x67b1f5df.
//
// Solidity: function setExchange(address exchange) returns()
func (_Opensea1 *Opensea1TransactorSession) SetExchange(exchange common.Address) (*types.Transaction, error) {
	return _Opensea1.Contract.SetExchange(&_Opensea1.TransactOpts, exchange)
}

// SetOwner is a paid mutator transaction binding the contract method 0x516c731c.
//
// Solidity: function setOwner(address owner, bool isOwner) returns()
func (_Opensea1 *Opensea1Transactor) SetOwner(opts *bind.TransactOpts, owner common.Address, isOwner bool) (*types.Transaction, error) {
	return _Opensea1.contract.Transact(opts, "setOwner", owner, isOwner)
}

// SetOwner is a paid mutator transaction binding the contract method 0x516c731c.
//
// Solidity: function setOwner(address owner, bool isOwner) returns()
func (_Opensea1 *Opensea1Session) SetOwner(owner common.Address, isOwner bool) (*types.Transaction, error) {
	return _Opensea1.Contract.SetOwner(&_Opensea1.TransactOpts, owner, isOwner)
}

// SetOwner is a paid mutator transaction binding the contract method 0x516c731c.
//
// Solidity: function setOwner(address owner, bool isOwner) returns()
func (_Opensea1 *Opensea1TransactorSession) SetOwner(owner common.Address, isOwner bool) (*types.Transaction, error) {
	return _Opensea1.Contract.SetOwner(&_Opensea1.TransactOpts, owner, isOwner)
}
