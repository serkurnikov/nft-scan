// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package element_1

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// ElementExSwapConverstionDetails is an auto generated low-level Go binding around an user-defined struct.
type ElementExSwapConverstionDetails struct {
	ConversionData []byte
}

// ElementExSwapERC20Details is an auto generated low-level Go binding around an user-defined struct.
type ElementExSwapERC20Details struct {
	TokenAddrs []common.Address
	Amounts    []*big.Int
}

// ElementExSwapSimpleTrades is an auto generated low-level Go binding around an user-defined struct.
type ElementExSwapSimpleTrades struct {
	Value     *big.Int
	TradeData []byte
}

// ElementExSwapTradeDetails is an auto generated low-level Go binding around an user-defined struct.
type ElementExSwapTradeDetails struct {
	MarketId  *big.Int
	Value     *big.Int
	TradeData []byte
}

// Element1MetaData contains all meta data concerning the Element1 contract.
var Element1MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address[]\",\"name\":\"_proxies\",\"type\":\"address[]\"},{\"internalType\":\"bool[]\",\"name\":\"_isLibs\",\"type\":\"bool[]\"},{\"internalType\":\"bool[]\",\"name\":\"_partialFill\",\"type\":\"bool[]\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"tradeInfo\",\"type\":\"uint256\"}],\"name\":\"TradeNotFilled\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"market\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"tradeInfo\",\"type\":\"uint256\"}],\"name\":\"TradeNotFilledSingleMarket\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_proxy\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_isLib\",\"type\":\"bool\"},{\"internalType\":\"bool\",\"name\":\"_partialFill\",\"type\":\"bool\"}],\"name\":\"addMarket\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"marketProxy\",\"type\":\"address\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structElementExSwap.SimpleTrades[]\",\"name\":\"tradeDetails\",\"type\":\"tuple[]\"}],\"name\":\"batchBuyFromSingleMarketWithETH\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"address[]\",\"name\":\"tokenAddrs\",\"type\":\"address[]\"},{\"internalType\":\"uint256[]\",\"name\":\"amounts\",\"type\":\"uint256[]\"}],\"internalType\":\"structElementExSwap.ERC20Details\",\"name\":\"erc20Details\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"marketId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structElementExSwap.TradeDetails[]\",\"name\":\"tradeDetails\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"bytes\",\"name\":\"conversionData\",\"type\":\"bytes\"}],\"internalType\":\"structElementExSwap.ConverstionDetails[]\",\"name\":\"converstionDetails\",\"type\":\"tuple[]\"},{\"internalType\":\"address[]\",\"name\":\"dustTokens\",\"type\":\"address[]\"}],\"name\":\"batchBuyWithERC20s\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"address[]\",\"name\":\"tokenAddrs\",\"type\":\"address[]\"},{\"internalType\":\"uint256[]\",\"name\":\"amounts\",\"type\":\"uint256[]\"}],\"internalType\":\"structElementExSwap.ERC20Details\",\"name\":\"erc20Details\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"marketId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structElementExSwap.TradeDetails[]\",\"name\":\"tradeDetails\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"bytes\",\"name\":\"conversionData\",\"type\":\"bytes\"}],\"internalType\":\"structElementExSwap.ConverstionDetails[]\",\"name\":\"converstionDetails\",\"type\":\"tuple[]\"},{\"internalType\":\"address[]\",\"name\":\"dustTokens\",\"type\":\"address[]\"}],\"name\":\"batchBuyWithERC20sSimulate\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"marketId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structElementExSwap.TradeDetails[]\",\"name\":\"tradeDetails\",\"type\":\"tuple[]\"}],\"name\":\"batchBuyWithETH\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"marketId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structElementExSwap.TradeDetails[]\",\"name\":\"tradeDetails\",\"type\":\"tuple[]\"}],\"name\":\"batchBuyWithETHSimulate\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"marketProxy\",\"type\":\"address\"},{\"components\":[{\"internalType\":\"address[]\",\"name\":\"tokenAddrs\",\"type\":\"address[]\"},{\"internalType\":\"uint256[]\",\"name\":\"amounts\",\"type\":\"uint256[]\"}],\"internalType\":\"structElementExSwap.ERC20Details\",\"name\":\"erc20Details\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structElementExSwap.SimpleTrades\",\"name\":\"tradeDetails\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"bytes\",\"name\":\"conversionData\",\"type\":\"bytes\"}],\"internalType\":\"structElementExSwap.ConverstionDetails[]\",\"name\":\"converstionDetails\",\"type\":\"tuple[]\"},{\"internalType\":\"address[]\",\"name\":\"dustTokens\",\"type\":\"address[]\"}],\"name\":\"buyOneWithERC20s\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"marketProxy\",\"type\":\"address\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structElementExSwap.SimpleTrades\",\"name\":\"tradeDetail\",\"type\":\"tuple\"}],\"name\":\"buyOneWithETH\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"closeAllTrades\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"converter\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"guardian\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"markets\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"proxy\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"isLib\",\"type\":\"bool\"},{\"internalType\":\"bool\",\"name\":\"isActive\",\"type\":\"bool\"},{\"internalType\":\"bool\",\"name\":\"partialFill\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256[]\",\"name\":\"\",\"type\":\"uint256[]\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC1155BatchReceived\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC1155Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC721Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC721Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"openForTrades\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"punkProxy\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"ids\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256[]\",\"name\":\"amounts\",\"type\":\"uint256[]\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"rescueERC1155\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"rescueERC20\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"ids\",\"type\":\"uint256[]\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"rescueERC721\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"rescueETH\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_converter\",\"type\":\"address\"}],\"name\":\"setConverter\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_marketId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_newProxy\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_isLib\",\"type\":\"bool\"},{\"internalType\":\"bool\",\"name\":\"_partialFill\",\"type\":\"bool\"}],\"name\":\"setMarketProxy\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_marketId\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"_newStatus\",\"type\":\"bool\"}],\"name\":\"setMarketStatus\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contractIERC20\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"setOneTimeApproval\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bool\",\"name\":\"_openForTrades\",\"type\":\"bool\"}],\"name\":\"setOpenForTrades\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes4\",\"name\":\"interfaceId\",\"type\":\"bytes4\"}],\"name\":\"supportsInterface\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_guardian\",\"type\":\"address\"}],\"name\":\"updateGuardian\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
}

// Element1ABI is the input ABI used to generate the binding from.
// Deprecated: Use Element1MetaData.ABI instead.
var Element1ABI = Element1MetaData.ABI

// Element1 is an auto generated Go binding around an Ethereum contract.
type Element1 struct {
	Element1Caller     // Read-only binding to the contract
	Element1Transactor // Write-only binding to the contract
	Element1Filterer   // Log filterer for contract events
}

// Element1Caller is an auto generated read-only Go binding around an Ethereum contract.
type Element1Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Element1Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Element1Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Element1Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Element1Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Element1Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Element1Session struct {
	Contract     *Element1         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Element1CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Element1CallerSession struct {
	Contract *Element1Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// Element1TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Element1TransactorSession struct {
	Contract     *Element1Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// Element1Raw is an auto generated low-level Go binding around an Ethereum contract.
type Element1Raw struct {
	Contract *Element1 // Generic contract binding to access the raw methods on
}

// Element1CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Element1CallerRaw struct {
	Contract *Element1Caller // Generic read-only contract binding to access the raw methods on
}

// Element1TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Element1TransactorRaw struct {
	Contract *Element1Transactor // Generic write-only contract binding to access the raw methods on
}

// NewElement1 creates a new instance of Element1, bound to a specific deployed contract.
func NewElement1(address common.Address, backend bind.ContractBackend) (*Element1, error) {
	contract, err := bindElement1(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Element1{Element1Caller: Element1Caller{contract: contract}, Element1Transactor: Element1Transactor{contract: contract}, Element1Filterer: Element1Filterer{contract: contract}}, nil
}

// NewElement1Caller creates a new read-only instance of Element1, bound to a specific deployed contract.
func NewElement1Caller(address common.Address, caller bind.ContractCaller) (*Element1Caller, error) {
	contract, err := bindElement1(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Element1Caller{contract: contract}, nil
}

// NewElement1Transactor creates a new write-only instance of Element1, bound to a specific deployed contract.
func NewElement1Transactor(address common.Address, transactor bind.ContractTransactor) (*Element1Transactor, error) {
	contract, err := bindElement1(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Element1Transactor{contract: contract}, nil
}

// NewElement1Filterer creates a new log filterer instance of Element1, bound to a specific deployed contract.
func NewElement1Filterer(address common.Address, filterer bind.ContractFilterer) (*Element1Filterer, error) {
	contract, err := bindElement1(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Element1Filterer{contract: contract}, nil
}

// bindElement1 binds a generic wrapper to an already deployed contract.
func bindElement1(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Element1ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Element1 *Element1Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Element1.Contract.Element1Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Element1 *Element1Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Element1.Contract.Element1Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Element1 *Element1Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Element1.Contract.Element1Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Element1 *Element1CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Element1.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Element1 *Element1TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Element1.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Element1 *Element1TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Element1.Contract.contract.Transact(opts, method, params...)
}

// Converter is a free data retrieval call binding the contract method 0xbd38837b.
//
// Solidity: function converter() view returns(address)
func (_Element1 *Element1Caller) Converter(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Element1.contract.Call(opts, &out, "converter")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Converter is a free data retrieval call binding the contract method 0xbd38837b.
//
// Solidity: function converter() view returns(address)
func (_Element1 *Element1Session) Converter() (common.Address, error) {
	return _Element1.Contract.Converter(&_Element1.CallOpts)
}

// Converter is a free data retrieval call binding the contract method 0xbd38837b.
//
// Solidity: function converter() view returns(address)
func (_Element1 *Element1CallerSession) Converter() (common.Address, error) {
	return _Element1.Contract.Converter(&_Element1.CallOpts)
}

// Guardian is a free data retrieval call binding the contract method 0x452a9320.
//
// Solidity: function guardian() view returns(address)
func (_Element1 *Element1Caller) Guardian(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Element1.contract.Call(opts, &out, "guardian")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Guardian is a free data retrieval call binding the contract method 0x452a9320.
//
// Solidity: function guardian() view returns(address)
func (_Element1 *Element1Session) Guardian() (common.Address, error) {
	return _Element1.Contract.Guardian(&_Element1.CallOpts)
}

// Guardian is a free data retrieval call binding the contract method 0x452a9320.
//
// Solidity: function guardian() view returns(address)
func (_Element1 *Element1CallerSession) Guardian() (common.Address, error) {
	return _Element1.Contract.Guardian(&_Element1.CallOpts)
}

// Markets is a free data retrieval call binding the contract method 0xb1283e77.
//
// Solidity: function markets(uint256 ) view returns(address proxy, bool isLib, bool isActive, bool partialFill)
func (_Element1 *Element1Caller) Markets(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Proxy       common.Address
	IsLib       bool
	IsActive    bool
	PartialFill bool
}, error) {
	var out []interface{}
	err := _Element1.contract.Call(opts, &out, "markets", arg0)

	outstruct := new(struct {
		Proxy       common.Address
		IsLib       bool
		IsActive    bool
		PartialFill bool
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Proxy = *abi.ConvertType(out[0], new(common.Address)).(*common.Address)
	outstruct.IsLib = *abi.ConvertType(out[1], new(bool)).(*bool)
	outstruct.IsActive = *abi.ConvertType(out[2], new(bool)).(*bool)
	outstruct.PartialFill = *abi.ConvertType(out[3], new(bool)).(*bool)

	return *outstruct, err

}

// Markets is a free data retrieval call binding the contract method 0xb1283e77.
//
// Solidity: function markets(uint256 ) view returns(address proxy, bool isLib, bool isActive, bool partialFill)
func (_Element1 *Element1Session) Markets(arg0 *big.Int) (struct {
	Proxy       common.Address
	IsLib       bool
	IsActive    bool
	PartialFill bool
}, error) {
	return _Element1.Contract.Markets(&_Element1.CallOpts, arg0)
}

// Markets is a free data retrieval call binding the contract method 0xb1283e77.
//
// Solidity: function markets(uint256 ) view returns(address proxy, bool isLib, bool isActive, bool partialFill)
func (_Element1 *Element1CallerSession) Markets(arg0 *big.Int) (struct {
	Proxy       common.Address
	IsLib       bool
	IsActive    bool
	PartialFill bool
}, error) {
	return _Element1.Contract.Markets(&_Element1.CallOpts, arg0)
}

// OpenForTrades is a free data retrieval call binding the contract method 0xa1b62797.
//
// Solidity: function openForTrades() view returns(bool)
func (_Element1 *Element1Caller) OpenForTrades(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Element1.contract.Call(opts, &out, "openForTrades")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// OpenForTrades is a free data retrieval call binding the contract method 0xa1b62797.
//
// Solidity: function openForTrades() view returns(bool)
func (_Element1 *Element1Session) OpenForTrades() (bool, error) {
	return _Element1.Contract.OpenForTrades(&_Element1.CallOpts)
}

// OpenForTrades is a free data retrieval call binding the contract method 0xa1b62797.
//
// Solidity: function openForTrades() view returns(bool)
func (_Element1 *Element1CallerSession) OpenForTrades() (bool, error) {
	return _Element1.Contract.OpenForTrades(&_Element1.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Element1 *Element1Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Element1.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Element1 *Element1Session) Owner() (common.Address, error) {
	return _Element1.Contract.Owner(&_Element1.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Element1 *Element1CallerSession) Owner() (common.Address, error) {
	return _Element1.Contract.Owner(&_Element1.CallOpts)
}

// PunkProxy is a free data retrieval call binding the contract method 0x6335f25e.
//
// Solidity: function punkProxy() view returns(address)
func (_Element1 *Element1Caller) PunkProxy(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Element1.contract.Call(opts, &out, "punkProxy")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// PunkProxy is a free data retrieval call binding the contract method 0x6335f25e.
//
// Solidity: function punkProxy() view returns(address)
func (_Element1 *Element1Session) PunkProxy() (common.Address, error) {
	return _Element1.Contract.PunkProxy(&_Element1.CallOpts)
}

// PunkProxy is a free data retrieval call binding the contract method 0x6335f25e.
//
// Solidity: function punkProxy() view returns(address)
func (_Element1 *Element1CallerSession) PunkProxy() (common.Address, error) {
	return _Element1.Contract.PunkProxy(&_Element1.CallOpts)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Element1 *Element1Caller) SupportsInterface(opts *bind.CallOpts, interfaceId [4]byte) (bool, error) {
	var out []interface{}
	err := _Element1.contract.Call(opts, &out, "supportsInterface", interfaceId)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Element1 *Element1Session) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _Element1.Contract.SupportsInterface(&_Element1.CallOpts, interfaceId)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Element1 *Element1CallerSession) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _Element1.Contract.SupportsInterface(&_Element1.CallOpts, interfaceId)
}

// AddMarket is a paid mutator transaction binding the contract method 0xa65108df.
//
// Solidity: function addMarket(address _proxy, bool _isLib, bool _partialFill) returns()
func (_Element1 *Element1Transactor) AddMarket(opts *bind.TransactOpts, _proxy common.Address, _isLib bool, _partialFill bool) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "addMarket", _proxy, _isLib, _partialFill)
}

// AddMarket is a paid mutator transaction binding the contract method 0xa65108df.
//
// Solidity: function addMarket(address _proxy, bool _isLib, bool _partialFill) returns()
func (_Element1 *Element1Session) AddMarket(_proxy common.Address, _isLib bool, _partialFill bool) (*types.Transaction, error) {
	return _Element1.Contract.AddMarket(&_Element1.TransactOpts, _proxy, _isLib, _partialFill)
}

// AddMarket is a paid mutator transaction binding the contract method 0xa65108df.
//
// Solidity: function addMarket(address _proxy, bool _isLib, bool _partialFill) returns()
func (_Element1 *Element1TransactorSession) AddMarket(_proxy common.Address, _isLib bool, _partialFill bool) (*types.Transaction, error) {
	return _Element1.Contract.AddMarket(&_Element1.TransactOpts, _proxy, _isLib, _partialFill)
}

// BatchBuyFromSingleMarketWithETH is a paid mutator transaction binding the contract method 0x0d46c7f1.
//
// Solidity: function batchBuyFromSingleMarketWithETH(address marketProxy, (uint256,bytes)[] tradeDetails) payable returns()
func (_Element1 *Element1Transactor) BatchBuyFromSingleMarketWithETH(opts *bind.TransactOpts, marketProxy common.Address, tradeDetails []ElementExSwapSimpleTrades) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "batchBuyFromSingleMarketWithETH", marketProxy, tradeDetails)
}

// BatchBuyFromSingleMarketWithETH is a paid mutator transaction binding the contract method 0x0d46c7f1.
//
// Solidity: function batchBuyFromSingleMarketWithETH(address marketProxy, (uint256,bytes)[] tradeDetails) payable returns()
func (_Element1 *Element1Session) BatchBuyFromSingleMarketWithETH(marketProxy common.Address, tradeDetails []ElementExSwapSimpleTrades) (*types.Transaction, error) {
	return _Element1.Contract.BatchBuyFromSingleMarketWithETH(&_Element1.TransactOpts, marketProxy, tradeDetails)
}

// BatchBuyFromSingleMarketWithETH is a paid mutator transaction binding the contract method 0x0d46c7f1.
//
// Solidity: function batchBuyFromSingleMarketWithETH(address marketProxy, (uint256,bytes)[] tradeDetails) payable returns()
func (_Element1 *Element1TransactorSession) BatchBuyFromSingleMarketWithETH(marketProxy common.Address, tradeDetails []ElementExSwapSimpleTrades) (*types.Transaction, error) {
	return _Element1.Contract.BatchBuyFromSingleMarketWithETH(&_Element1.TransactOpts, marketProxy, tradeDetails)
}

// BatchBuyWithERC20s is a paid mutator transaction binding the contract method 0x09ba153d.
//
// Solidity: function batchBuyWithERC20s((address[],uint256[]) erc20Details, (uint256,uint256,bytes)[] tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Element1 *Element1Transactor) BatchBuyWithERC20s(opts *bind.TransactOpts, erc20Details ElementExSwapERC20Details, tradeDetails []ElementExSwapTradeDetails, converstionDetails []ElementExSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "batchBuyWithERC20s", erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BatchBuyWithERC20s is a paid mutator transaction binding the contract method 0x09ba153d.
//
// Solidity: function batchBuyWithERC20s((address[],uint256[]) erc20Details, (uint256,uint256,bytes)[] tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Element1 *Element1Session) BatchBuyWithERC20s(erc20Details ElementExSwapERC20Details, tradeDetails []ElementExSwapTradeDetails, converstionDetails []ElementExSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Element1.Contract.BatchBuyWithERC20s(&_Element1.TransactOpts, erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BatchBuyWithERC20s is a paid mutator transaction binding the contract method 0x09ba153d.
//
// Solidity: function batchBuyWithERC20s((address[],uint256[]) erc20Details, (uint256,uint256,bytes)[] tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Element1 *Element1TransactorSession) BatchBuyWithERC20s(erc20Details ElementExSwapERC20Details, tradeDetails []ElementExSwapTradeDetails, converstionDetails []ElementExSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Element1.Contract.BatchBuyWithERC20s(&_Element1.TransactOpts, erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BatchBuyWithERC20sSimulate is a paid mutator transaction binding the contract method 0x71b3a2f0.
//
// Solidity: function batchBuyWithERC20sSimulate((address[],uint256[]) erc20Details, (uint256,uint256,bytes)[] tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Element1 *Element1Transactor) BatchBuyWithERC20sSimulate(opts *bind.TransactOpts, erc20Details ElementExSwapERC20Details, tradeDetails []ElementExSwapTradeDetails, converstionDetails []ElementExSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "batchBuyWithERC20sSimulate", erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BatchBuyWithERC20sSimulate is a paid mutator transaction binding the contract method 0x71b3a2f0.
//
// Solidity: function batchBuyWithERC20sSimulate((address[],uint256[]) erc20Details, (uint256,uint256,bytes)[] tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Element1 *Element1Session) BatchBuyWithERC20sSimulate(erc20Details ElementExSwapERC20Details, tradeDetails []ElementExSwapTradeDetails, converstionDetails []ElementExSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Element1.Contract.BatchBuyWithERC20sSimulate(&_Element1.TransactOpts, erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BatchBuyWithERC20sSimulate is a paid mutator transaction binding the contract method 0x71b3a2f0.
//
// Solidity: function batchBuyWithERC20sSimulate((address[],uint256[]) erc20Details, (uint256,uint256,bytes)[] tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Element1 *Element1TransactorSession) BatchBuyWithERC20sSimulate(erc20Details ElementExSwapERC20Details, tradeDetails []ElementExSwapTradeDetails, converstionDetails []ElementExSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Element1.Contract.BatchBuyWithERC20sSimulate(&_Element1.TransactOpts, erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BatchBuyWithETH is a paid mutator transaction binding the contract method 0x9a2b8115.
//
// Solidity: function batchBuyWithETH((uint256,uint256,bytes)[] tradeDetails) payable returns()
func (_Element1 *Element1Transactor) BatchBuyWithETH(opts *bind.TransactOpts, tradeDetails []ElementExSwapTradeDetails) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "batchBuyWithETH", tradeDetails)
}

// BatchBuyWithETH is a paid mutator transaction binding the contract method 0x9a2b8115.
//
// Solidity: function batchBuyWithETH((uint256,uint256,bytes)[] tradeDetails) payable returns()
func (_Element1 *Element1Session) BatchBuyWithETH(tradeDetails []ElementExSwapTradeDetails) (*types.Transaction, error) {
	return _Element1.Contract.BatchBuyWithETH(&_Element1.TransactOpts, tradeDetails)
}

// BatchBuyWithETH is a paid mutator transaction binding the contract method 0x9a2b8115.
//
// Solidity: function batchBuyWithETH((uint256,uint256,bytes)[] tradeDetails) payable returns()
func (_Element1 *Element1TransactorSession) BatchBuyWithETH(tradeDetails []ElementExSwapTradeDetails) (*types.Transaction, error) {
	return _Element1.Contract.BatchBuyWithETH(&_Element1.TransactOpts, tradeDetails)
}

// BatchBuyWithETHSimulate is a paid mutator transaction binding the contract method 0xd9d8f12f.
//
// Solidity: function batchBuyWithETHSimulate((uint256,uint256,bytes)[] tradeDetails) payable returns()
func (_Element1 *Element1Transactor) BatchBuyWithETHSimulate(opts *bind.TransactOpts, tradeDetails []ElementExSwapTradeDetails) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "batchBuyWithETHSimulate", tradeDetails)
}

// BatchBuyWithETHSimulate is a paid mutator transaction binding the contract method 0xd9d8f12f.
//
// Solidity: function batchBuyWithETHSimulate((uint256,uint256,bytes)[] tradeDetails) payable returns()
func (_Element1 *Element1Session) BatchBuyWithETHSimulate(tradeDetails []ElementExSwapTradeDetails) (*types.Transaction, error) {
	return _Element1.Contract.BatchBuyWithETHSimulate(&_Element1.TransactOpts, tradeDetails)
}

// BatchBuyWithETHSimulate is a paid mutator transaction binding the contract method 0xd9d8f12f.
//
// Solidity: function batchBuyWithETHSimulate((uint256,uint256,bytes)[] tradeDetails) payable returns()
func (_Element1 *Element1TransactorSession) BatchBuyWithETHSimulate(tradeDetails []ElementExSwapTradeDetails) (*types.Transaction, error) {
	return _Element1.Contract.BatchBuyWithETHSimulate(&_Element1.TransactOpts, tradeDetails)
}

// BuyOneWithERC20s is a paid mutator transaction binding the contract method 0xa96dc30b.
//
// Solidity: function buyOneWithERC20s(address marketProxy, (address[],uint256[]) erc20Details, (uint256,bytes) tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Element1 *Element1Transactor) BuyOneWithERC20s(opts *bind.TransactOpts, marketProxy common.Address, erc20Details ElementExSwapERC20Details, tradeDetails ElementExSwapSimpleTrades, converstionDetails []ElementExSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "buyOneWithERC20s", marketProxy, erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BuyOneWithERC20s is a paid mutator transaction binding the contract method 0xa96dc30b.
//
// Solidity: function buyOneWithERC20s(address marketProxy, (address[],uint256[]) erc20Details, (uint256,bytes) tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Element1 *Element1Session) BuyOneWithERC20s(marketProxy common.Address, erc20Details ElementExSwapERC20Details, tradeDetails ElementExSwapSimpleTrades, converstionDetails []ElementExSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Element1.Contract.BuyOneWithERC20s(&_Element1.TransactOpts, marketProxy, erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BuyOneWithERC20s is a paid mutator transaction binding the contract method 0xa96dc30b.
//
// Solidity: function buyOneWithERC20s(address marketProxy, (address[],uint256[]) erc20Details, (uint256,bytes) tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Element1 *Element1TransactorSession) BuyOneWithERC20s(marketProxy common.Address, erc20Details ElementExSwapERC20Details, tradeDetails ElementExSwapSimpleTrades, converstionDetails []ElementExSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Element1.Contract.BuyOneWithERC20s(&_Element1.TransactOpts, marketProxy, erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BuyOneWithETH is a paid mutator transaction binding the contract method 0x6bd05c8e.
//
// Solidity: function buyOneWithETH(address marketProxy, (uint256,bytes) tradeDetail) payable returns()
func (_Element1 *Element1Transactor) BuyOneWithETH(opts *bind.TransactOpts, marketProxy common.Address, tradeDetail ElementExSwapSimpleTrades) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "buyOneWithETH", marketProxy, tradeDetail)
}

// BuyOneWithETH is a paid mutator transaction binding the contract method 0x6bd05c8e.
//
// Solidity: function buyOneWithETH(address marketProxy, (uint256,bytes) tradeDetail) payable returns()
func (_Element1 *Element1Session) BuyOneWithETH(marketProxy common.Address, tradeDetail ElementExSwapSimpleTrades) (*types.Transaction, error) {
	return _Element1.Contract.BuyOneWithETH(&_Element1.TransactOpts, marketProxy, tradeDetail)
}

// BuyOneWithETH is a paid mutator transaction binding the contract method 0x6bd05c8e.
//
// Solidity: function buyOneWithETH(address marketProxy, (uint256,bytes) tradeDetail) payable returns()
func (_Element1 *Element1TransactorSession) BuyOneWithETH(marketProxy common.Address, tradeDetail ElementExSwapSimpleTrades) (*types.Transaction, error) {
	return _Element1.Contract.BuyOneWithETH(&_Element1.TransactOpts, marketProxy, tradeDetail)
}

// CloseAllTrades is a paid mutator transaction binding the contract method 0xc5cadd7f.
//
// Solidity: function closeAllTrades() returns()
func (_Element1 *Element1Transactor) CloseAllTrades(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "closeAllTrades")
}

// CloseAllTrades is a paid mutator transaction binding the contract method 0xc5cadd7f.
//
// Solidity: function closeAllTrades() returns()
func (_Element1 *Element1Session) CloseAllTrades() (*types.Transaction, error) {
	return _Element1.Contract.CloseAllTrades(&_Element1.TransactOpts)
}

// CloseAllTrades is a paid mutator transaction binding the contract method 0xc5cadd7f.
//
// Solidity: function closeAllTrades() returns()
func (_Element1 *Element1TransactorSession) CloseAllTrades() (*types.Transaction, error) {
	return _Element1.Contract.CloseAllTrades(&_Element1.TransactOpts)
}

// OnERC1155BatchReceived is a paid mutator transaction binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address , address , uint256[] , uint256[] , bytes ) returns(bytes4)
func (_Element1 *Element1Transactor) OnERC1155BatchReceived(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 []*big.Int, arg3 []*big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "onERC1155BatchReceived", arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155BatchReceived is a paid mutator transaction binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address , address , uint256[] , uint256[] , bytes ) returns(bytes4)
func (_Element1 *Element1Session) OnERC1155BatchReceived(arg0 common.Address, arg1 common.Address, arg2 []*big.Int, arg3 []*big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Element1.Contract.OnERC1155BatchReceived(&_Element1.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155BatchReceived is a paid mutator transaction binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address , address , uint256[] , uint256[] , bytes ) returns(bytes4)
func (_Element1 *Element1TransactorSession) OnERC1155BatchReceived(arg0 common.Address, arg1 common.Address, arg2 []*big.Int, arg3 []*big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Element1.Contract.OnERC1155BatchReceived(&_Element1.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155Received is a paid mutator transaction binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address , address , uint256 , uint256 , bytes ) returns(bytes4)
func (_Element1 *Element1Transactor) OnERC1155Received(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 *big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "onERC1155Received", arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155Received is a paid mutator transaction binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address , address , uint256 , uint256 , bytes ) returns(bytes4)
func (_Element1 *Element1Session) OnERC1155Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 *big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Element1.Contract.OnERC1155Received(&_Element1.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155Received is a paid mutator transaction binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address , address , uint256 , uint256 , bytes ) returns(bytes4)
func (_Element1 *Element1TransactorSession) OnERC1155Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 *big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Element1.Contract.OnERC1155Received(&_Element1.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Element1 *Element1Transactor) OnERC721Received(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "onERC721Received", arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Element1 *Element1Session) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Element1.Contract.OnERC721Received(&_Element1.TransactOpts, arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Element1 *Element1TransactorSession) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Element1.Contract.OnERC721Received(&_Element1.TransactOpts, arg0, arg1, arg2, arg3)
}

// OnERC721Received0 is a paid mutator transaction binding the contract method 0xf0b9e5ba.
//
// Solidity: function onERC721Received(address , uint256 , bytes ) returns(bytes4)
func (_Element1 *Element1Transactor) OnERC721Received0(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int, arg2 []byte) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "onERC721Received0", arg0, arg1, arg2)
}

// OnERC721Received0 is a paid mutator transaction binding the contract method 0xf0b9e5ba.
//
// Solidity: function onERC721Received(address , uint256 , bytes ) returns(bytes4)
func (_Element1 *Element1Session) OnERC721Received0(arg0 common.Address, arg1 *big.Int, arg2 []byte) (*types.Transaction, error) {
	return _Element1.Contract.OnERC721Received0(&_Element1.TransactOpts, arg0, arg1, arg2)
}

// OnERC721Received0 is a paid mutator transaction binding the contract method 0xf0b9e5ba.
//
// Solidity: function onERC721Received(address , uint256 , bytes ) returns(bytes4)
func (_Element1 *Element1TransactorSession) OnERC721Received0(arg0 common.Address, arg1 *big.Int, arg2 []byte) (*types.Transaction, error) {
	return _Element1.Contract.OnERC721Received0(&_Element1.TransactOpts, arg0, arg1, arg2)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Element1 *Element1Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Element1 *Element1Session) RenounceOwnership() (*types.Transaction, error) {
	return _Element1.Contract.RenounceOwnership(&_Element1.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Element1 *Element1TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Element1.Contract.RenounceOwnership(&_Element1.TransactOpts)
}

// RescueERC1155 is a paid mutator transaction binding the contract method 0xb7ce33a2.
//
// Solidity: function rescueERC1155(address asset, uint256[] ids, uint256[] amounts, address recipient) returns()
func (_Element1 *Element1Transactor) RescueERC1155(opts *bind.TransactOpts, asset common.Address, ids []*big.Int, amounts []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "rescueERC1155", asset, ids, amounts, recipient)
}

// RescueERC1155 is a paid mutator transaction binding the contract method 0xb7ce33a2.
//
// Solidity: function rescueERC1155(address asset, uint256[] ids, uint256[] amounts, address recipient) returns()
func (_Element1 *Element1Session) RescueERC1155(asset common.Address, ids []*big.Int, amounts []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Element1.Contract.RescueERC1155(&_Element1.TransactOpts, asset, ids, amounts, recipient)
}

// RescueERC1155 is a paid mutator transaction binding the contract method 0xb7ce33a2.
//
// Solidity: function rescueERC1155(address asset, uint256[] ids, uint256[] amounts, address recipient) returns()
func (_Element1 *Element1TransactorSession) RescueERC1155(asset common.Address, ids []*big.Int, amounts []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Element1.Contract.RescueERC1155(&_Element1.TransactOpts, asset, ids, amounts, recipient)
}

// RescueERC20 is a paid mutator transaction binding the contract method 0x5d799f87.
//
// Solidity: function rescueERC20(address asset, address recipient) returns()
func (_Element1 *Element1Transactor) RescueERC20(opts *bind.TransactOpts, asset common.Address, recipient common.Address) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "rescueERC20", asset, recipient)
}

// RescueERC20 is a paid mutator transaction binding the contract method 0x5d799f87.
//
// Solidity: function rescueERC20(address asset, address recipient) returns()
func (_Element1 *Element1Session) RescueERC20(asset common.Address, recipient common.Address) (*types.Transaction, error) {
	return _Element1.Contract.RescueERC20(&_Element1.TransactOpts, asset, recipient)
}

// RescueERC20 is a paid mutator transaction binding the contract method 0x5d799f87.
//
// Solidity: function rescueERC20(address asset, address recipient) returns()
func (_Element1 *Element1TransactorSession) RescueERC20(asset common.Address, recipient common.Address) (*types.Transaction, error) {
	return _Element1.Contract.RescueERC20(&_Element1.TransactOpts, asset, recipient)
}

// RescueERC721 is a paid mutator transaction binding the contract method 0x26e2dca2.
//
// Solidity: function rescueERC721(address asset, uint256[] ids, address recipient) returns()
func (_Element1 *Element1Transactor) RescueERC721(opts *bind.TransactOpts, asset common.Address, ids []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "rescueERC721", asset, ids, recipient)
}

// RescueERC721 is a paid mutator transaction binding the contract method 0x26e2dca2.
//
// Solidity: function rescueERC721(address asset, uint256[] ids, address recipient) returns()
func (_Element1 *Element1Session) RescueERC721(asset common.Address, ids []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Element1.Contract.RescueERC721(&_Element1.TransactOpts, asset, ids, recipient)
}

// RescueERC721 is a paid mutator transaction binding the contract method 0x26e2dca2.
//
// Solidity: function rescueERC721(address asset, uint256[] ids, address recipient) returns()
func (_Element1 *Element1TransactorSession) RescueERC721(asset common.Address, ids []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Element1.Contract.RescueERC721(&_Element1.TransactOpts, asset, ids, recipient)
}

// RescueETH is a paid mutator transaction binding the contract method 0x04824e70.
//
// Solidity: function rescueETH(address recipient) returns()
func (_Element1 *Element1Transactor) RescueETH(opts *bind.TransactOpts, recipient common.Address) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "rescueETH", recipient)
}

// RescueETH is a paid mutator transaction binding the contract method 0x04824e70.
//
// Solidity: function rescueETH(address recipient) returns()
func (_Element1 *Element1Session) RescueETH(recipient common.Address) (*types.Transaction, error) {
	return _Element1.Contract.RescueETH(&_Element1.TransactOpts, recipient)
}

// RescueETH is a paid mutator transaction binding the contract method 0x04824e70.
//
// Solidity: function rescueETH(address recipient) returns()
func (_Element1 *Element1TransactorSession) RescueETH(recipient common.Address) (*types.Transaction, error) {
	return _Element1.Contract.RescueETH(&_Element1.TransactOpts, recipient)
}

// SetConverter is a paid mutator transaction binding the contract method 0xb19337a4.
//
// Solidity: function setConverter(address _converter) returns()
func (_Element1 *Element1Transactor) SetConverter(opts *bind.TransactOpts, _converter common.Address) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "setConverter", _converter)
}

// SetConverter is a paid mutator transaction binding the contract method 0xb19337a4.
//
// Solidity: function setConverter(address _converter) returns()
func (_Element1 *Element1Session) SetConverter(_converter common.Address) (*types.Transaction, error) {
	return _Element1.Contract.SetConverter(&_Element1.TransactOpts, _converter)
}

// SetConverter is a paid mutator transaction binding the contract method 0xb19337a4.
//
// Solidity: function setConverter(address _converter) returns()
func (_Element1 *Element1TransactorSession) SetConverter(_converter common.Address) (*types.Transaction, error) {
	return _Element1.Contract.SetConverter(&_Element1.TransactOpts, _converter)
}

// SetMarketProxy is a paid mutator transaction binding the contract method 0xe1d8ad53.
//
// Solidity: function setMarketProxy(uint256 _marketId, address _newProxy, bool _isLib, bool _partialFill) returns()
func (_Element1 *Element1Transactor) SetMarketProxy(opts *bind.TransactOpts, _marketId *big.Int, _newProxy common.Address, _isLib bool, _partialFill bool) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "setMarketProxy", _marketId, _newProxy, _isLib, _partialFill)
}

// SetMarketProxy is a paid mutator transaction binding the contract method 0xe1d8ad53.
//
// Solidity: function setMarketProxy(uint256 _marketId, address _newProxy, bool _isLib, bool _partialFill) returns()
func (_Element1 *Element1Session) SetMarketProxy(_marketId *big.Int, _newProxy common.Address, _isLib bool, _partialFill bool) (*types.Transaction, error) {
	return _Element1.Contract.SetMarketProxy(&_Element1.TransactOpts, _marketId, _newProxy, _isLib, _partialFill)
}

// SetMarketProxy is a paid mutator transaction binding the contract method 0xe1d8ad53.
//
// Solidity: function setMarketProxy(uint256 _marketId, address _newProxy, bool _isLib, bool _partialFill) returns()
func (_Element1 *Element1TransactorSession) SetMarketProxy(_marketId *big.Int, _newProxy common.Address, _isLib bool, _partialFill bool) (*types.Transaction, error) {
	return _Element1.Contract.SetMarketProxy(&_Element1.TransactOpts, _marketId, _newProxy, _isLib, _partialFill)
}

// SetMarketStatus is a paid mutator transaction binding the contract method 0x615fc1bb.
//
// Solidity: function setMarketStatus(uint256 _marketId, bool _newStatus) returns()
func (_Element1 *Element1Transactor) SetMarketStatus(opts *bind.TransactOpts, _marketId *big.Int, _newStatus bool) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "setMarketStatus", _marketId, _newStatus)
}

// SetMarketStatus is a paid mutator transaction binding the contract method 0x615fc1bb.
//
// Solidity: function setMarketStatus(uint256 _marketId, bool _newStatus) returns()
func (_Element1 *Element1Session) SetMarketStatus(_marketId *big.Int, _newStatus bool) (*types.Transaction, error) {
	return _Element1.Contract.SetMarketStatus(&_Element1.TransactOpts, _marketId, _newStatus)
}

// SetMarketStatus is a paid mutator transaction binding the contract method 0x615fc1bb.
//
// Solidity: function setMarketStatus(uint256 _marketId, bool _newStatus) returns()
func (_Element1 *Element1TransactorSession) SetMarketStatus(_marketId *big.Int, _newStatus bool) (*types.Transaction, error) {
	return _Element1.Contract.SetMarketStatus(&_Element1.TransactOpts, _marketId, _newStatus)
}

// SetOneTimeApproval is a paid mutator transaction binding the contract method 0x9f2ba09b.
//
// Solidity: function setOneTimeApproval(address token, address operator, uint256 amount) returns()
func (_Element1 *Element1Transactor) SetOneTimeApproval(opts *bind.TransactOpts, token common.Address, operator common.Address, amount *big.Int) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "setOneTimeApproval", token, operator, amount)
}

// SetOneTimeApproval is a paid mutator transaction binding the contract method 0x9f2ba09b.
//
// Solidity: function setOneTimeApproval(address token, address operator, uint256 amount) returns()
func (_Element1 *Element1Session) SetOneTimeApproval(token common.Address, operator common.Address, amount *big.Int) (*types.Transaction, error) {
	return _Element1.Contract.SetOneTimeApproval(&_Element1.TransactOpts, token, operator, amount)
}

// SetOneTimeApproval is a paid mutator transaction binding the contract method 0x9f2ba09b.
//
// Solidity: function setOneTimeApproval(address token, address operator, uint256 amount) returns()
func (_Element1 *Element1TransactorSession) SetOneTimeApproval(token common.Address, operator common.Address, amount *big.Int) (*types.Transaction, error) {
	return _Element1.Contract.SetOneTimeApproval(&_Element1.TransactOpts, token, operator, amount)
}

// SetOpenForTrades is a paid mutator transaction binding the contract method 0x83206e80.
//
// Solidity: function setOpenForTrades(bool _openForTrades) returns()
func (_Element1 *Element1Transactor) SetOpenForTrades(opts *bind.TransactOpts, _openForTrades bool) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "setOpenForTrades", _openForTrades)
}

// SetOpenForTrades is a paid mutator transaction binding the contract method 0x83206e80.
//
// Solidity: function setOpenForTrades(bool _openForTrades) returns()
func (_Element1 *Element1Session) SetOpenForTrades(_openForTrades bool) (*types.Transaction, error) {
	return _Element1.Contract.SetOpenForTrades(&_Element1.TransactOpts, _openForTrades)
}

// SetOpenForTrades is a paid mutator transaction binding the contract method 0x83206e80.
//
// Solidity: function setOpenForTrades(bool _openForTrades) returns()
func (_Element1 *Element1TransactorSession) SetOpenForTrades(_openForTrades bool) (*types.Transaction, error) {
	return _Element1.Contract.SetOpenForTrades(&_Element1.TransactOpts, _openForTrades)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Element1 *Element1Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Element1 *Element1Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Element1.Contract.TransferOwnership(&_Element1.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Element1 *Element1TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Element1.Contract.TransferOwnership(&_Element1.TransactOpts, newOwner)
}

// UpdateGuardian is a paid mutator transaction binding the contract method 0xfc525395.
//
// Solidity: function updateGuardian(address _guardian) returns()
func (_Element1 *Element1Transactor) UpdateGuardian(opts *bind.TransactOpts, _guardian common.Address) (*types.Transaction, error) {
	return _Element1.contract.Transact(opts, "updateGuardian", _guardian)
}

// UpdateGuardian is a paid mutator transaction binding the contract method 0xfc525395.
//
// Solidity: function updateGuardian(address _guardian) returns()
func (_Element1 *Element1Session) UpdateGuardian(_guardian common.Address) (*types.Transaction, error) {
	return _Element1.Contract.UpdateGuardian(&_Element1.TransactOpts, _guardian)
}

// UpdateGuardian is a paid mutator transaction binding the contract method 0xfc525395.
//
// Solidity: function updateGuardian(address _guardian) returns()
func (_Element1 *Element1TransactorSession) UpdateGuardian(_guardian common.Address) (*types.Transaction, error) {
	return _Element1.Contract.UpdateGuardian(&_Element1.TransactOpts, _guardian)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Element1 *Element1Transactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Element1.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Element1 *Element1Session) Receive() (*types.Transaction, error) {
	return _Element1.Contract.Receive(&_Element1.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Element1 *Element1TransactorSession) Receive() (*types.Transaction, error) {
	return _Element1.Contract.Receive(&_Element1.TransactOpts)
}

// Element1OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Element1 contract.
type Element1OwnershipTransferredIterator struct {
	Event *Element1OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Element1OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Element1OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Element1OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Element1OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Element1OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Element1OwnershipTransferred represents a OwnershipTransferred event raised by the Element1 contract.
type Element1OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Element1 *Element1Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Element1OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Element1.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Element1OwnershipTransferredIterator{contract: _Element1.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Element1 *Element1Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Element1OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Element1.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Element1OwnershipTransferred)
				if err := _Element1.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Element1 *Element1Filterer) ParseOwnershipTransferred(log types.Log) (*Element1OwnershipTransferred, error) {
	event := new(Element1OwnershipTransferred)
	if err := _Element1.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Element1TradeNotFilledIterator is returned from FilterTradeNotFilled and is used to iterate over the raw logs and unpacked data for TradeNotFilled events raised by the Element1 contract.
type Element1TradeNotFilledIterator struct {
	Event *Element1TradeNotFilled // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Element1TradeNotFilledIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Element1TradeNotFilled)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Element1TradeNotFilled)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Element1TradeNotFilledIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Element1TradeNotFilledIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Element1TradeNotFilled represents a TradeNotFilled event raised by the Element1 contract.
type Element1TradeNotFilled struct {
	TradeInfo *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterTradeNotFilled is a free log retrieval operation binding the contract event 0x874f66c6f8a7d2d69aa7062e49da26b806dd1c277e9e6cdb11213adefa38a6c6.
//
// Solidity: event TradeNotFilled(uint256 tradeInfo)
func (_Element1 *Element1Filterer) FilterTradeNotFilled(opts *bind.FilterOpts) (*Element1TradeNotFilledIterator, error) {

	logs, sub, err := _Element1.contract.FilterLogs(opts, "TradeNotFilled")
	if err != nil {
		return nil, err
	}
	return &Element1TradeNotFilledIterator{contract: _Element1.contract, event: "TradeNotFilled", logs: logs, sub: sub}, nil
}

// WatchTradeNotFilled is a free log subscription operation binding the contract event 0x874f66c6f8a7d2d69aa7062e49da26b806dd1c277e9e6cdb11213adefa38a6c6.
//
// Solidity: event TradeNotFilled(uint256 tradeInfo)
func (_Element1 *Element1Filterer) WatchTradeNotFilled(opts *bind.WatchOpts, sink chan<- *Element1TradeNotFilled) (event.Subscription, error) {

	logs, sub, err := _Element1.contract.WatchLogs(opts, "TradeNotFilled")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Element1TradeNotFilled)
				if err := _Element1.contract.UnpackLog(event, "TradeNotFilled", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTradeNotFilled is a log parse operation binding the contract event 0x874f66c6f8a7d2d69aa7062e49da26b806dd1c277e9e6cdb11213adefa38a6c6.
//
// Solidity: event TradeNotFilled(uint256 tradeInfo)
func (_Element1 *Element1Filterer) ParseTradeNotFilled(log types.Log) (*Element1TradeNotFilled, error) {
	event := new(Element1TradeNotFilled)
	if err := _Element1.contract.UnpackLog(event, "TradeNotFilled", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Element1TradeNotFilledSingleMarketIterator is returned from FilterTradeNotFilledSingleMarket and is used to iterate over the raw logs and unpacked data for TradeNotFilledSingleMarket events raised by the Element1 contract.
type Element1TradeNotFilledSingleMarketIterator struct {
	Event *Element1TradeNotFilledSingleMarket // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Element1TradeNotFilledSingleMarketIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Element1TradeNotFilledSingleMarket)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Element1TradeNotFilledSingleMarket)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Element1TradeNotFilledSingleMarketIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Element1TradeNotFilledSingleMarketIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Element1TradeNotFilledSingleMarket represents a TradeNotFilledSingleMarket event raised by the Element1 contract.
type Element1TradeNotFilledSingleMarket struct {
	Market    common.Address
	TradeInfo *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterTradeNotFilledSingleMarket is a free log retrieval operation binding the contract event 0x331c9f71e5884f78bd17ddfa4925ef8e298e93d8fbd884867d5fab3b90ef4267.
//
// Solidity: event TradeNotFilledSingleMarket(address market, uint256 tradeInfo)
func (_Element1 *Element1Filterer) FilterTradeNotFilledSingleMarket(opts *bind.FilterOpts) (*Element1TradeNotFilledSingleMarketIterator, error) {

	logs, sub, err := _Element1.contract.FilterLogs(opts, "TradeNotFilledSingleMarket")
	if err != nil {
		return nil, err
	}
	return &Element1TradeNotFilledSingleMarketIterator{contract: _Element1.contract, event: "TradeNotFilledSingleMarket", logs: logs, sub: sub}, nil
}

// WatchTradeNotFilledSingleMarket is a free log subscription operation binding the contract event 0x331c9f71e5884f78bd17ddfa4925ef8e298e93d8fbd884867d5fab3b90ef4267.
//
// Solidity: event TradeNotFilledSingleMarket(address market, uint256 tradeInfo)
func (_Element1 *Element1Filterer) WatchTradeNotFilledSingleMarket(opts *bind.WatchOpts, sink chan<- *Element1TradeNotFilledSingleMarket) (event.Subscription, error) {

	logs, sub, err := _Element1.contract.WatchLogs(opts, "TradeNotFilledSingleMarket")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Element1TradeNotFilledSingleMarket)
				if err := _Element1.contract.UnpackLog(event, "TradeNotFilledSingleMarket", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTradeNotFilledSingleMarket is a log parse operation binding the contract event 0x331c9f71e5884f78bd17ddfa4925ef8e298e93d8fbd884867d5fab3b90ef4267.
//
// Solidity: event TradeNotFilledSingleMarket(address market, uint256 tradeInfo)
func (_Element1 *Element1Filterer) ParseTradeNotFilledSingleMarket(log types.Log) (*Element1TradeNotFilledSingleMarket, error) {
	event := new(Element1TradeNotFilledSingleMarket)
	if err := _Element1.contract.UnpackLog(event, "TradeNotFilledSingleMarket", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
