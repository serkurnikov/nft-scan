// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package okxnft_3

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// FlashSales1155FlashSale is an auto generated low-level Go binding around an user-defined struct.
type FlashSales1155FlashSale struct {
	Seller             common.Address
	TokenAddress       common.Address
	Id                 *big.Int
	RemainingAmount    *big.Int
	PayTokenAddress    common.Address
	Price              *big.Int
	Receiver           common.Address
	PurchaseLimitation *big.Int
	StartTime          *big.Int
	EndTime            *big.Int
	IsAvailable        bool
}

// Okxnft3MetaData contains all meta data concerning the Okxnft3 contract.
var Okxnft3MetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"saleId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"}],\"name\":\"CancelFlashSale\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"saleId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"}],\"name\":\"FlashSaleExpired\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"erc20Addr\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"jurisdiction\",\"type\":\"bool\"}],\"name\":\"PaymentWhitelistChange\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"saleId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"tokenAddress\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"payTokenAddress\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"totalPayment\",\"type\":\"uint256\"}],\"name\":\"Purchase\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"saleId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"flashSaleSetter\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"tokenAddress\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"remainingAmount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"payTokenAddress\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"receiver\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"purchaseLimitation\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"endTime\",\"type\":\"uint256\"}],\"name\":\"SetFlashSale\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"memberAddr\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"jurisdiction\",\"type\":\"bool\"}],\"name\":\"SetWhitelist\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"saleId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"newTokenAddress\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"newId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"newRemainingAmount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"newPayTokenAddress\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"newPrice\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"newReceiver\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"newPurchaseLimitation\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"newStartTime\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"newEndTime\",\"type\":\"uint256\"}],\"name\":\"UpdateFlashSale\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"saleId\",\"type\":\"uint256\"}],\"name\":\"cancelFlashSale\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getCurrentSaleId\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"saleId\",\"type\":\"uint256\"}],\"name\":\"getFlashSale\",\"outputs\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"tokenAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"remainingAmount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"payTokenAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"receiver\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"purchaseLimitation\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endTime\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"isAvailable\",\"type\":\"bool\"}],\"internalType\":\"structFlashSales1155.FlashSale\",\"name\":\"flashSale\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"saleId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"}],\"name\":\"getFlashSalePurchaseRecord\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc20Addr\",\"type\":\"address\"}],\"name\":\"getPaymentWhitelist\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"memberAddr\",\"type\":\"address\"}],\"name\":\"getWhitelist\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_newOwner\",\"type\":\"address\"}],\"name\":\"init\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"saleId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"purchase\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"tokenAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"payTokenAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"receiver\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"purchaseLimitation\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"duration\",\"type\":\"uint256\"}],\"name\":\"setFlashSale\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc20Addr\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"jurisdiction\",\"type\":\"bool\"}],\"name\":\"setPaymentWhitelist\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"memberAddr\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"jurisdiction\",\"type\":\"bool\"}],\"name\":\"setWhitelist\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"saleId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"newTokenAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"newId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"newAmount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"newPayTokenAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"newPrice\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"newReceiver\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"newPurchaseLimitation\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"newStartTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"newDuration\",\"type\":\"uint256\"}],\"name\":\"updateFlashSale\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// Okxnft3ABI is the input ABI used to generate the binding from.
// Deprecated: Use Okxnft3MetaData.ABI instead.
var Okxnft3ABI = Okxnft3MetaData.ABI

// Okxnft3 is an auto generated Go binding around an Ethereum contract.
type Okxnft3 struct {
	Okxnft3Caller     // Read-only binding to the contract
	Okxnft3Transactor // Write-only binding to the contract
	Okxnft3Filterer   // Log filterer for contract events
}

// Okxnft3Caller is an auto generated read-only Go binding around an Ethereum contract.
type Okxnft3Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Okxnft3Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Okxnft3Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Okxnft3Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Okxnft3Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Okxnft3Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Okxnft3Session struct {
	Contract     *Okxnft3          // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Okxnft3CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Okxnft3CallerSession struct {
	Contract *Okxnft3Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts  // Call options to use throughout this session
}

// Okxnft3TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Okxnft3TransactorSession struct {
	Contract     *Okxnft3Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts  // Transaction auth options to use throughout this session
}

// Okxnft3Raw is an auto generated low-level Go binding around an Ethereum contract.
type Okxnft3Raw struct {
	Contract *Okxnft3 // Generic contract binding to access the raw methods on
}

// Okxnft3CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Okxnft3CallerRaw struct {
	Contract *Okxnft3Caller // Generic read-only contract binding to access the raw methods on
}

// Okxnft3TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Okxnft3TransactorRaw struct {
	Contract *Okxnft3Transactor // Generic write-only contract binding to access the raw methods on
}

// NewOkxnft3 creates a new instance of Okxnft3, bound to a specific deployed contract.
func NewOkxnft3(address common.Address, backend bind.ContractBackend) (*Okxnft3, error) {
	contract, err := bindOkxnft3(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Okxnft3{Okxnft3Caller: Okxnft3Caller{contract: contract}, Okxnft3Transactor: Okxnft3Transactor{contract: contract}, Okxnft3Filterer: Okxnft3Filterer{contract: contract}}, nil
}

// NewOkxnft3Caller creates a new read-only instance of Okxnft3, bound to a specific deployed contract.
func NewOkxnft3Caller(address common.Address, caller bind.ContractCaller) (*Okxnft3Caller, error) {
	contract, err := bindOkxnft3(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Okxnft3Caller{contract: contract}, nil
}

// NewOkxnft3Transactor creates a new write-only instance of Okxnft3, bound to a specific deployed contract.
func NewOkxnft3Transactor(address common.Address, transactor bind.ContractTransactor) (*Okxnft3Transactor, error) {
	contract, err := bindOkxnft3(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Okxnft3Transactor{contract: contract}, nil
}

// NewOkxnft3Filterer creates a new log filterer instance of Okxnft3, bound to a specific deployed contract.
func NewOkxnft3Filterer(address common.Address, filterer bind.ContractFilterer) (*Okxnft3Filterer, error) {
	contract, err := bindOkxnft3(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Okxnft3Filterer{contract: contract}, nil
}

// bindOkxnft3 binds a generic wrapper to an already deployed contract.
func bindOkxnft3(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Okxnft3ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Okxnft3 *Okxnft3Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Okxnft3.Contract.Okxnft3Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Okxnft3 *Okxnft3Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Okxnft3.Contract.Okxnft3Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Okxnft3 *Okxnft3Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Okxnft3.Contract.Okxnft3Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Okxnft3 *Okxnft3CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Okxnft3.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Okxnft3 *Okxnft3TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Okxnft3.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Okxnft3 *Okxnft3TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Okxnft3.Contract.contract.Transact(opts, method, params...)
}

// GetCurrentSaleId is a free data retrieval call binding the contract method 0x212ea247.
//
// Solidity: function getCurrentSaleId() view returns(uint256)
func (_Okxnft3 *Okxnft3Caller) GetCurrentSaleId(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Okxnft3.contract.Call(opts, &out, "getCurrentSaleId")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetCurrentSaleId is a free data retrieval call binding the contract method 0x212ea247.
//
// Solidity: function getCurrentSaleId() view returns(uint256)
func (_Okxnft3 *Okxnft3Session) GetCurrentSaleId() (*big.Int, error) {
	return _Okxnft3.Contract.GetCurrentSaleId(&_Okxnft3.CallOpts)
}

// GetCurrentSaleId is a free data retrieval call binding the contract method 0x212ea247.
//
// Solidity: function getCurrentSaleId() view returns(uint256)
func (_Okxnft3 *Okxnft3CallerSession) GetCurrentSaleId() (*big.Int, error) {
	return _Okxnft3.Contract.GetCurrentSaleId(&_Okxnft3.CallOpts)
}

// GetFlashSale is a free data retrieval call binding the contract method 0x0452b968.
//
// Solidity: function getFlashSale(uint256 saleId) view returns((address,address,uint256,uint256,address,uint256,address,uint256,uint256,uint256,bool) flashSale)
func (_Okxnft3 *Okxnft3Caller) GetFlashSale(opts *bind.CallOpts, saleId *big.Int) (FlashSales1155FlashSale, error) {
	var out []interface{}
	err := _Okxnft3.contract.Call(opts, &out, "getFlashSale", saleId)

	if err != nil {
		return *new(FlashSales1155FlashSale), err
	}

	out0 := *abi.ConvertType(out[0], new(FlashSales1155FlashSale)).(*FlashSales1155FlashSale)

	return out0, err

}

// GetFlashSale is a free data retrieval call binding the contract method 0x0452b968.
//
// Solidity: function getFlashSale(uint256 saleId) view returns((address,address,uint256,uint256,address,uint256,address,uint256,uint256,uint256,bool) flashSale)
func (_Okxnft3 *Okxnft3Session) GetFlashSale(saleId *big.Int) (FlashSales1155FlashSale, error) {
	return _Okxnft3.Contract.GetFlashSale(&_Okxnft3.CallOpts, saleId)
}

// GetFlashSale is a free data retrieval call binding the contract method 0x0452b968.
//
// Solidity: function getFlashSale(uint256 saleId) view returns((address,address,uint256,uint256,address,uint256,address,uint256,uint256,uint256,bool) flashSale)
func (_Okxnft3 *Okxnft3CallerSession) GetFlashSale(saleId *big.Int) (FlashSales1155FlashSale, error) {
	return _Okxnft3.Contract.GetFlashSale(&_Okxnft3.CallOpts, saleId)
}

// GetFlashSalePurchaseRecord is a free data retrieval call binding the contract method 0x40d1d824.
//
// Solidity: function getFlashSalePurchaseRecord(uint256 saleId, address buyer) view returns(uint256)
func (_Okxnft3 *Okxnft3Caller) GetFlashSalePurchaseRecord(opts *bind.CallOpts, saleId *big.Int, buyer common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Okxnft3.contract.Call(opts, &out, "getFlashSalePurchaseRecord", saleId, buyer)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetFlashSalePurchaseRecord is a free data retrieval call binding the contract method 0x40d1d824.
//
// Solidity: function getFlashSalePurchaseRecord(uint256 saleId, address buyer) view returns(uint256)
func (_Okxnft3 *Okxnft3Session) GetFlashSalePurchaseRecord(saleId *big.Int, buyer common.Address) (*big.Int, error) {
	return _Okxnft3.Contract.GetFlashSalePurchaseRecord(&_Okxnft3.CallOpts, saleId, buyer)
}

// GetFlashSalePurchaseRecord is a free data retrieval call binding the contract method 0x40d1d824.
//
// Solidity: function getFlashSalePurchaseRecord(uint256 saleId, address buyer) view returns(uint256)
func (_Okxnft3 *Okxnft3CallerSession) GetFlashSalePurchaseRecord(saleId *big.Int, buyer common.Address) (*big.Int, error) {
	return _Okxnft3.Contract.GetFlashSalePurchaseRecord(&_Okxnft3.CallOpts, saleId, buyer)
}

// GetPaymentWhitelist is a free data retrieval call binding the contract method 0x7a5a2ac0.
//
// Solidity: function getPaymentWhitelist(address erc20Addr) view returns(bool)
func (_Okxnft3 *Okxnft3Caller) GetPaymentWhitelist(opts *bind.CallOpts, erc20Addr common.Address) (bool, error) {
	var out []interface{}
	err := _Okxnft3.contract.Call(opts, &out, "getPaymentWhitelist", erc20Addr)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// GetPaymentWhitelist is a free data retrieval call binding the contract method 0x7a5a2ac0.
//
// Solidity: function getPaymentWhitelist(address erc20Addr) view returns(bool)
func (_Okxnft3 *Okxnft3Session) GetPaymentWhitelist(erc20Addr common.Address) (bool, error) {
	return _Okxnft3.Contract.GetPaymentWhitelist(&_Okxnft3.CallOpts, erc20Addr)
}

// GetPaymentWhitelist is a free data retrieval call binding the contract method 0x7a5a2ac0.
//
// Solidity: function getPaymentWhitelist(address erc20Addr) view returns(bool)
func (_Okxnft3 *Okxnft3CallerSession) GetPaymentWhitelist(erc20Addr common.Address) (bool, error) {
	return _Okxnft3.Contract.GetPaymentWhitelist(&_Okxnft3.CallOpts, erc20Addr)
}

// GetWhitelist is a free data retrieval call binding the contract method 0x30edc0f5.
//
// Solidity: function getWhitelist(address memberAddr) view returns(bool)
func (_Okxnft3 *Okxnft3Caller) GetWhitelist(opts *bind.CallOpts, memberAddr common.Address) (bool, error) {
	var out []interface{}
	err := _Okxnft3.contract.Call(opts, &out, "getWhitelist", memberAddr)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// GetWhitelist is a free data retrieval call binding the contract method 0x30edc0f5.
//
// Solidity: function getWhitelist(address memberAddr) view returns(bool)
func (_Okxnft3 *Okxnft3Session) GetWhitelist(memberAddr common.Address) (bool, error) {
	return _Okxnft3.Contract.GetWhitelist(&_Okxnft3.CallOpts, memberAddr)
}

// GetWhitelist is a free data retrieval call binding the contract method 0x30edc0f5.
//
// Solidity: function getWhitelist(address memberAddr) view returns(bool)
func (_Okxnft3 *Okxnft3CallerSession) GetWhitelist(memberAddr common.Address) (bool, error) {
	return _Okxnft3.Contract.GetWhitelist(&_Okxnft3.CallOpts, memberAddr)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Okxnft3 *Okxnft3Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Okxnft3.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Okxnft3 *Okxnft3Session) Owner() (common.Address, error) {
	return _Okxnft3.Contract.Owner(&_Okxnft3.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Okxnft3 *Okxnft3CallerSession) Owner() (common.Address, error) {
	return _Okxnft3.Contract.Owner(&_Okxnft3.CallOpts)
}

// CancelFlashSale is a paid mutator transaction binding the contract method 0xd6f4935d.
//
// Solidity: function cancelFlashSale(uint256 saleId) returns()
func (_Okxnft3 *Okxnft3Transactor) CancelFlashSale(opts *bind.TransactOpts, saleId *big.Int) (*types.Transaction, error) {
	return _Okxnft3.contract.Transact(opts, "cancelFlashSale", saleId)
}

// CancelFlashSale is a paid mutator transaction binding the contract method 0xd6f4935d.
//
// Solidity: function cancelFlashSale(uint256 saleId) returns()
func (_Okxnft3 *Okxnft3Session) CancelFlashSale(saleId *big.Int) (*types.Transaction, error) {
	return _Okxnft3.Contract.CancelFlashSale(&_Okxnft3.TransactOpts, saleId)
}

// CancelFlashSale is a paid mutator transaction binding the contract method 0xd6f4935d.
//
// Solidity: function cancelFlashSale(uint256 saleId) returns()
func (_Okxnft3 *Okxnft3TransactorSession) CancelFlashSale(saleId *big.Int) (*types.Transaction, error) {
	return _Okxnft3.Contract.CancelFlashSale(&_Okxnft3.TransactOpts, saleId)
}

// Init is a paid mutator transaction binding the contract method 0x19ab453c.
//
// Solidity: function init(address _newOwner) returns()
func (_Okxnft3 *Okxnft3Transactor) Init(opts *bind.TransactOpts, _newOwner common.Address) (*types.Transaction, error) {
	return _Okxnft3.contract.Transact(opts, "init", _newOwner)
}

// Init is a paid mutator transaction binding the contract method 0x19ab453c.
//
// Solidity: function init(address _newOwner) returns()
func (_Okxnft3 *Okxnft3Session) Init(_newOwner common.Address) (*types.Transaction, error) {
	return _Okxnft3.Contract.Init(&_Okxnft3.TransactOpts, _newOwner)
}

// Init is a paid mutator transaction binding the contract method 0x19ab453c.
//
// Solidity: function init(address _newOwner) returns()
func (_Okxnft3 *Okxnft3TransactorSession) Init(_newOwner common.Address) (*types.Transaction, error) {
	return _Okxnft3.Contract.Init(&_Okxnft3.TransactOpts, _newOwner)
}

// Purchase is a paid mutator transaction binding the contract method 0x70876c98.
//
// Solidity: function purchase(uint256 saleId, uint256 amount) returns()
func (_Okxnft3 *Okxnft3Transactor) Purchase(opts *bind.TransactOpts, saleId *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Okxnft3.contract.Transact(opts, "purchase", saleId, amount)
}

// Purchase is a paid mutator transaction binding the contract method 0x70876c98.
//
// Solidity: function purchase(uint256 saleId, uint256 amount) returns()
func (_Okxnft3 *Okxnft3Session) Purchase(saleId *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Okxnft3.Contract.Purchase(&_Okxnft3.TransactOpts, saleId, amount)
}

// Purchase is a paid mutator transaction binding the contract method 0x70876c98.
//
// Solidity: function purchase(uint256 saleId, uint256 amount) returns()
func (_Okxnft3 *Okxnft3TransactorSession) Purchase(saleId *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Okxnft3.Contract.Purchase(&_Okxnft3.TransactOpts, saleId, amount)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Okxnft3 *Okxnft3Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Okxnft3.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Okxnft3 *Okxnft3Session) RenounceOwnership() (*types.Transaction, error) {
	return _Okxnft3.Contract.RenounceOwnership(&_Okxnft3.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Okxnft3 *Okxnft3TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Okxnft3.Contract.RenounceOwnership(&_Okxnft3.TransactOpts)
}

// SetFlashSale is a paid mutator transaction binding the contract method 0x90bfb8cf.
//
// Solidity: function setFlashSale(address tokenAddress, uint256 id, uint256 amount, address payTokenAddress, uint256 price, address receiver, uint256 purchaseLimitation, uint256 startTime, uint256 duration) returns()
func (_Okxnft3 *Okxnft3Transactor) SetFlashSale(opts *bind.TransactOpts, tokenAddress common.Address, id *big.Int, amount *big.Int, payTokenAddress common.Address, price *big.Int, receiver common.Address, purchaseLimitation *big.Int, startTime *big.Int, duration *big.Int) (*types.Transaction, error) {
	return _Okxnft3.contract.Transact(opts, "setFlashSale", tokenAddress, id, amount, payTokenAddress, price, receiver, purchaseLimitation, startTime, duration)
}

// SetFlashSale is a paid mutator transaction binding the contract method 0x90bfb8cf.
//
// Solidity: function setFlashSale(address tokenAddress, uint256 id, uint256 amount, address payTokenAddress, uint256 price, address receiver, uint256 purchaseLimitation, uint256 startTime, uint256 duration) returns()
func (_Okxnft3 *Okxnft3Session) SetFlashSale(tokenAddress common.Address, id *big.Int, amount *big.Int, payTokenAddress common.Address, price *big.Int, receiver common.Address, purchaseLimitation *big.Int, startTime *big.Int, duration *big.Int) (*types.Transaction, error) {
	return _Okxnft3.Contract.SetFlashSale(&_Okxnft3.TransactOpts, tokenAddress, id, amount, payTokenAddress, price, receiver, purchaseLimitation, startTime, duration)
}

// SetFlashSale is a paid mutator transaction binding the contract method 0x90bfb8cf.
//
// Solidity: function setFlashSale(address tokenAddress, uint256 id, uint256 amount, address payTokenAddress, uint256 price, address receiver, uint256 purchaseLimitation, uint256 startTime, uint256 duration) returns()
func (_Okxnft3 *Okxnft3TransactorSession) SetFlashSale(tokenAddress common.Address, id *big.Int, amount *big.Int, payTokenAddress common.Address, price *big.Int, receiver common.Address, purchaseLimitation *big.Int, startTime *big.Int, duration *big.Int) (*types.Transaction, error) {
	return _Okxnft3.Contract.SetFlashSale(&_Okxnft3.TransactOpts, tokenAddress, id, amount, payTokenAddress, price, receiver, purchaseLimitation, startTime, duration)
}

// SetPaymentWhitelist is a paid mutator transaction binding the contract method 0x22ce64a2.
//
// Solidity: function setPaymentWhitelist(address erc20Addr, bool jurisdiction) returns()
func (_Okxnft3 *Okxnft3Transactor) SetPaymentWhitelist(opts *bind.TransactOpts, erc20Addr common.Address, jurisdiction bool) (*types.Transaction, error) {
	return _Okxnft3.contract.Transact(opts, "setPaymentWhitelist", erc20Addr, jurisdiction)
}

// SetPaymentWhitelist is a paid mutator transaction binding the contract method 0x22ce64a2.
//
// Solidity: function setPaymentWhitelist(address erc20Addr, bool jurisdiction) returns()
func (_Okxnft3 *Okxnft3Session) SetPaymentWhitelist(erc20Addr common.Address, jurisdiction bool) (*types.Transaction, error) {
	return _Okxnft3.Contract.SetPaymentWhitelist(&_Okxnft3.TransactOpts, erc20Addr, jurisdiction)
}

// SetPaymentWhitelist is a paid mutator transaction binding the contract method 0x22ce64a2.
//
// Solidity: function setPaymentWhitelist(address erc20Addr, bool jurisdiction) returns()
func (_Okxnft3 *Okxnft3TransactorSession) SetPaymentWhitelist(erc20Addr common.Address, jurisdiction bool) (*types.Transaction, error) {
	return _Okxnft3.Contract.SetPaymentWhitelist(&_Okxnft3.TransactOpts, erc20Addr, jurisdiction)
}

// SetWhitelist is a paid mutator transaction binding the contract method 0x53d6fd59.
//
// Solidity: function setWhitelist(address memberAddr, bool jurisdiction) returns()
func (_Okxnft3 *Okxnft3Transactor) SetWhitelist(opts *bind.TransactOpts, memberAddr common.Address, jurisdiction bool) (*types.Transaction, error) {
	return _Okxnft3.contract.Transact(opts, "setWhitelist", memberAddr, jurisdiction)
}

// SetWhitelist is a paid mutator transaction binding the contract method 0x53d6fd59.
//
// Solidity: function setWhitelist(address memberAddr, bool jurisdiction) returns()
func (_Okxnft3 *Okxnft3Session) SetWhitelist(memberAddr common.Address, jurisdiction bool) (*types.Transaction, error) {
	return _Okxnft3.Contract.SetWhitelist(&_Okxnft3.TransactOpts, memberAddr, jurisdiction)
}

// SetWhitelist is a paid mutator transaction binding the contract method 0x53d6fd59.
//
// Solidity: function setWhitelist(address memberAddr, bool jurisdiction) returns()
func (_Okxnft3 *Okxnft3TransactorSession) SetWhitelist(memberAddr common.Address, jurisdiction bool) (*types.Transaction, error) {
	return _Okxnft3.Contract.SetWhitelist(&_Okxnft3.TransactOpts, memberAddr, jurisdiction)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Okxnft3 *Okxnft3Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Okxnft3.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Okxnft3 *Okxnft3Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Okxnft3.Contract.TransferOwnership(&_Okxnft3.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Okxnft3 *Okxnft3TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Okxnft3.Contract.TransferOwnership(&_Okxnft3.TransactOpts, newOwner)
}

// UpdateFlashSale is a paid mutator transaction binding the contract method 0x1f4e4a44.
//
// Solidity: function updateFlashSale(uint256 saleId, address newTokenAddress, uint256 newId, uint256 newAmount, address newPayTokenAddress, uint256 newPrice, address newReceiver, uint256 newPurchaseLimitation, uint256 newStartTime, uint256 newDuration) returns()
func (_Okxnft3 *Okxnft3Transactor) UpdateFlashSale(opts *bind.TransactOpts, saleId *big.Int, newTokenAddress common.Address, newId *big.Int, newAmount *big.Int, newPayTokenAddress common.Address, newPrice *big.Int, newReceiver common.Address, newPurchaseLimitation *big.Int, newStartTime *big.Int, newDuration *big.Int) (*types.Transaction, error) {
	return _Okxnft3.contract.Transact(opts, "updateFlashSale", saleId, newTokenAddress, newId, newAmount, newPayTokenAddress, newPrice, newReceiver, newPurchaseLimitation, newStartTime, newDuration)
}

// UpdateFlashSale is a paid mutator transaction binding the contract method 0x1f4e4a44.
//
// Solidity: function updateFlashSale(uint256 saleId, address newTokenAddress, uint256 newId, uint256 newAmount, address newPayTokenAddress, uint256 newPrice, address newReceiver, uint256 newPurchaseLimitation, uint256 newStartTime, uint256 newDuration) returns()
func (_Okxnft3 *Okxnft3Session) UpdateFlashSale(saleId *big.Int, newTokenAddress common.Address, newId *big.Int, newAmount *big.Int, newPayTokenAddress common.Address, newPrice *big.Int, newReceiver common.Address, newPurchaseLimitation *big.Int, newStartTime *big.Int, newDuration *big.Int) (*types.Transaction, error) {
	return _Okxnft3.Contract.UpdateFlashSale(&_Okxnft3.TransactOpts, saleId, newTokenAddress, newId, newAmount, newPayTokenAddress, newPrice, newReceiver, newPurchaseLimitation, newStartTime, newDuration)
}

// UpdateFlashSale is a paid mutator transaction binding the contract method 0x1f4e4a44.
//
// Solidity: function updateFlashSale(uint256 saleId, address newTokenAddress, uint256 newId, uint256 newAmount, address newPayTokenAddress, uint256 newPrice, address newReceiver, uint256 newPurchaseLimitation, uint256 newStartTime, uint256 newDuration) returns()
func (_Okxnft3 *Okxnft3TransactorSession) UpdateFlashSale(saleId *big.Int, newTokenAddress common.Address, newId *big.Int, newAmount *big.Int, newPayTokenAddress common.Address, newPrice *big.Int, newReceiver common.Address, newPurchaseLimitation *big.Int, newStartTime *big.Int, newDuration *big.Int) (*types.Transaction, error) {
	return _Okxnft3.Contract.UpdateFlashSale(&_Okxnft3.TransactOpts, saleId, newTokenAddress, newId, newAmount, newPayTokenAddress, newPrice, newReceiver, newPurchaseLimitation, newStartTime, newDuration)
}

// Okxnft3CancelFlashSaleIterator is returned from FilterCancelFlashSale and is used to iterate over the raw logs and unpacked data for CancelFlashSale events raised by the Okxnft3 contract.
type Okxnft3CancelFlashSaleIterator struct {
	Event *Okxnft3CancelFlashSale // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Okxnft3CancelFlashSaleIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Okxnft3CancelFlashSale)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Okxnft3CancelFlashSale)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Okxnft3CancelFlashSaleIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Okxnft3CancelFlashSaleIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Okxnft3CancelFlashSale represents a CancelFlashSale event raised by the Okxnft3 contract.
type Okxnft3CancelFlashSale struct {
	SaleId   *big.Int
	Operator common.Address
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterCancelFlashSale is a free log retrieval operation binding the contract event 0xf3da86c38149b3ff13eb97ec00d936dffd9cb29a827e1c0fc9dae4a939137fc8.
//
// Solidity: event CancelFlashSale(uint256 saleId, address operator)
func (_Okxnft3 *Okxnft3Filterer) FilterCancelFlashSale(opts *bind.FilterOpts) (*Okxnft3CancelFlashSaleIterator, error) {

	logs, sub, err := _Okxnft3.contract.FilterLogs(opts, "CancelFlashSale")
	if err != nil {
		return nil, err
	}
	return &Okxnft3CancelFlashSaleIterator{contract: _Okxnft3.contract, event: "CancelFlashSale", logs: logs, sub: sub}, nil
}

// WatchCancelFlashSale is a free log subscription operation binding the contract event 0xf3da86c38149b3ff13eb97ec00d936dffd9cb29a827e1c0fc9dae4a939137fc8.
//
// Solidity: event CancelFlashSale(uint256 saleId, address operator)
func (_Okxnft3 *Okxnft3Filterer) WatchCancelFlashSale(opts *bind.WatchOpts, sink chan<- *Okxnft3CancelFlashSale) (event.Subscription, error) {

	logs, sub, err := _Okxnft3.contract.WatchLogs(opts, "CancelFlashSale")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Okxnft3CancelFlashSale)
				if err := _Okxnft3.contract.UnpackLog(event, "CancelFlashSale", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCancelFlashSale is a log parse operation binding the contract event 0xf3da86c38149b3ff13eb97ec00d936dffd9cb29a827e1c0fc9dae4a939137fc8.
//
// Solidity: event CancelFlashSale(uint256 saleId, address operator)
func (_Okxnft3 *Okxnft3Filterer) ParseCancelFlashSale(log types.Log) (*Okxnft3CancelFlashSale, error) {
	event := new(Okxnft3CancelFlashSale)
	if err := _Okxnft3.contract.UnpackLog(event, "CancelFlashSale", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Okxnft3FlashSaleExpiredIterator is returned from FilterFlashSaleExpired and is used to iterate over the raw logs and unpacked data for FlashSaleExpired events raised by the Okxnft3 contract.
type Okxnft3FlashSaleExpiredIterator struct {
	Event *Okxnft3FlashSaleExpired // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Okxnft3FlashSaleExpiredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Okxnft3FlashSaleExpired)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Okxnft3FlashSaleExpired)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Okxnft3FlashSaleExpiredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Okxnft3FlashSaleExpiredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Okxnft3FlashSaleExpired represents a FlashSaleExpired event raised by the Okxnft3 contract.
type Okxnft3FlashSaleExpired struct {
	SaleId   *big.Int
	Operator common.Address
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterFlashSaleExpired is a free log retrieval operation binding the contract event 0x90022e1d8eedbbb3c33615d39a0b09d7b55aea384712055d32661146c5a661f3.
//
// Solidity: event FlashSaleExpired(uint256 saleId, address operator)
func (_Okxnft3 *Okxnft3Filterer) FilterFlashSaleExpired(opts *bind.FilterOpts) (*Okxnft3FlashSaleExpiredIterator, error) {

	logs, sub, err := _Okxnft3.contract.FilterLogs(opts, "FlashSaleExpired")
	if err != nil {
		return nil, err
	}
	return &Okxnft3FlashSaleExpiredIterator{contract: _Okxnft3.contract, event: "FlashSaleExpired", logs: logs, sub: sub}, nil
}

// WatchFlashSaleExpired is a free log subscription operation binding the contract event 0x90022e1d8eedbbb3c33615d39a0b09d7b55aea384712055d32661146c5a661f3.
//
// Solidity: event FlashSaleExpired(uint256 saleId, address operator)
func (_Okxnft3 *Okxnft3Filterer) WatchFlashSaleExpired(opts *bind.WatchOpts, sink chan<- *Okxnft3FlashSaleExpired) (event.Subscription, error) {

	logs, sub, err := _Okxnft3.contract.WatchLogs(opts, "FlashSaleExpired")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Okxnft3FlashSaleExpired)
				if err := _Okxnft3.contract.UnpackLog(event, "FlashSaleExpired", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseFlashSaleExpired is a log parse operation binding the contract event 0x90022e1d8eedbbb3c33615d39a0b09d7b55aea384712055d32661146c5a661f3.
//
// Solidity: event FlashSaleExpired(uint256 saleId, address operator)
func (_Okxnft3 *Okxnft3Filterer) ParseFlashSaleExpired(log types.Log) (*Okxnft3FlashSaleExpired, error) {
	event := new(Okxnft3FlashSaleExpired)
	if err := _Okxnft3.contract.UnpackLog(event, "FlashSaleExpired", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Okxnft3OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Okxnft3 contract.
type Okxnft3OwnershipTransferredIterator struct {
	Event *Okxnft3OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Okxnft3OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Okxnft3OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Okxnft3OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Okxnft3OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Okxnft3OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Okxnft3OwnershipTransferred represents a OwnershipTransferred event raised by the Okxnft3 contract.
type Okxnft3OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Okxnft3 *Okxnft3Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Okxnft3OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Okxnft3.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Okxnft3OwnershipTransferredIterator{contract: _Okxnft3.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Okxnft3 *Okxnft3Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Okxnft3OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Okxnft3.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Okxnft3OwnershipTransferred)
				if err := _Okxnft3.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Okxnft3 *Okxnft3Filterer) ParseOwnershipTransferred(log types.Log) (*Okxnft3OwnershipTransferred, error) {
	event := new(Okxnft3OwnershipTransferred)
	if err := _Okxnft3.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Okxnft3PaymentWhitelistChangeIterator is returned from FilterPaymentWhitelistChange and is used to iterate over the raw logs and unpacked data for PaymentWhitelistChange events raised by the Okxnft3 contract.
type Okxnft3PaymentWhitelistChangeIterator struct {
	Event *Okxnft3PaymentWhitelistChange // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Okxnft3PaymentWhitelistChangeIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Okxnft3PaymentWhitelistChange)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Okxnft3PaymentWhitelistChange)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Okxnft3PaymentWhitelistChangeIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Okxnft3PaymentWhitelistChangeIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Okxnft3PaymentWhitelistChange represents a PaymentWhitelistChange event raised by the Okxnft3 contract.
type Okxnft3PaymentWhitelistChange struct {
	Erc20Addr    common.Address
	Jurisdiction bool
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterPaymentWhitelistChange is a free log retrieval operation binding the contract event 0x686a81d4d0c55e25b2a748f3245a9afbb117be5c48482310ac2ad48833899d02.
//
// Solidity: event PaymentWhitelistChange(address erc20Addr, bool jurisdiction)
func (_Okxnft3 *Okxnft3Filterer) FilterPaymentWhitelistChange(opts *bind.FilterOpts) (*Okxnft3PaymentWhitelistChangeIterator, error) {

	logs, sub, err := _Okxnft3.contract.FilterLogs(opts, "PaymentWhitelistChange")
	if err != nil {
		return nil, err
	}
	return &Okxnft3PaymentWhitelistChangeIterator{contract: _Okxnft3.contract, event: "PaymentWhitelistChange", logs: logs, sub: sub}, nil
}

// WatchPaymentWhitelistChange is a free log subscription operation binding the contract event 0x686a81d4d0c55e25b2a748f3245a9afbb117be5c48482310ac2ad48833899d02.
//
// Solidity: event PaymentWhitelistChange(address erc20Addr, bool jurisdiction)
func (_Okxnft3 *Okxnft3Filterer) WatchPaymentWhitelistChange(opts *bind.WatchOpts, sink chan<- *Okxnft3PaymentWhitelistChange) (event.Subscription, error) {

	logs, sub, err := _Okxnft3.contract.WatchLogs(opts, "PaymentWhitelistChange")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Okxnft3PaymentWhitelistChange)
				if err := _Okxnft3.contract.UnpackLog(event, "PaymentWhitelistChange", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePaymentWhitelistChange is a log parse operation binding the contract event 0x686a81d4d0c55e25b2a748f3245a9afbb117be5c48482310ac2ad48833899d02.
//
// Solidity: event PaymentWhitelistChange(address erc20Addr, bool jurisdiction)
func (_Okxnft3 *Okxnft3Filterer) ParsePaymentWhitelistChange(log types.Log) (*Okxnft3PaymentWhitelistChange, error) {
	event := new(Okxnft3PaymentWhitelistChange)
	if err := _Okxnft3.contract.UnpackLog(event, "PaymentWhitelistChange", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Okxnft3PurchaseIterator is returned from FilterPurchase and is used to iterate over the raw logs and unpacked data for Purchase events raised by the Okxnft3 contract.
type Okxnft3PurchaseIterator struct {
	Event *Okxnft3Purchase // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Okxnft3PurchaseIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Okxnft3Purchase)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Okxnft3Purchase)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Okxnft3PurchaseIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Okxnft3PurchaseIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Okxnft3Purchase represents a Purchase event raised by the Okxnft3 contract.
type Okxnft3Purchase struct {
	SaleId          *big.Int
	Buyer           common.Address
	TokenAddress    common.Address
	Id              *big.Int
	Amount          *big.Int
	PayTokenAddress common.Address
	TotalPayment    *big.Int
	Raw             types.Log // Blockchain specific contextual infos
}

// FilterPurchase is a free log retrieval operation binding the contract event 0xb61d698fcfa97cf1199a5bb1b8fae383a4d712f57b20ec4c45c343ded8770d7f.
//
// Solidity: event Purchase(uint256 saleId, address buyer, address tokenAddress, uint256 id, uint256 amount, address payTokenAddress, uint256 totalPayment)
func (_Okxnft3 *Okxnft3Filterer) FilterPurchase(opts *bind.FilterOpts) (*Okxnft3PurchaseIterator, error) {

	logs, sub, err := _Okxnft3.contract.FilterLogs(opts, "Purchase")
	if err != nil {
		return nil, err
	}
	return &Okxnft3PurchaseIterator{contract: _Okxnft3.contract, event: "Purchase", logs: logs, sub: sub}, nil
}

// WatchPurchase is a free log subscription operation binding the contract event 0xb61d698fcfa97cf1199a5bb1b8fae383a4d712f57b20ec4c45c343ded8770d7f.
//
// Solidity: event Purchase(uint256 saleId, address buyer, address tokenAddress, uint256 id, uint256 amount, address payTokenAddress, uint256 totalPayment)
func (_Okxnft3 *Okxnft3Filterer) WatchPurchase(opts *bind.WatchOpts, sink chan<- *Okxnft3Purchase) (event.Subscription, error) {

	logs, sub, err := _Okxnft3.contract.WatchLogs(opts, "Purchase")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Okxnft3Purchase)
				if err := _Okxnft3.contract.UnpackLog(event, "Purchase", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePurchase is a log parse operation binding the contract event 0xb61d698fcfa97cf1199a5bb1b8fae383a4d712f57b20ec4c45c343ded8770d7f.
//
// Solidity: event Purchase(uint256 saleId, address buyer, address tokenAddress, uint256 id, uint256 amount, address payTokenAddress, uint256 totalPayment)
func (_Okxnft3 *Okxnft3Filterer) ParsePurchase(log types.Log) (*Okxnft3Purchase, error) {
	event := new(Okxnft3Purchase)
	if err := _Okxnft3.contract.UnpackLog(event, "Purchase", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Okxnft3SetFlashSaleIterator is returned from FilterSetFlashSale and is used to iterate over the raw logs and unpacked data for SetFlashSale events raised by the Okxnft3 contract.
type Okxnft3SetFlashSaleIterator struct {
	Event *Okxnft3SetFlashSale // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Okxnft3SetFlashSaleIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Okxnft3SetFlashSale)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Okxnft3SetFlashSale)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Okxnft3SetFlashSaleIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Okxnft3SetFlashSaleIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Okxnft3SetFlashSale represents a SetFlashSale event raised by the Okxnft3 contract.
type Okxnft3SetFlashSale struct {
	SaleId             *big.Int
	FlashSaleSetter    common.Address
	TokenAddress       common.Address
	Id                 *big.Int
	RemainingAmount    *big.Int
	PayTokenAddress    common.Address
	Price              *big.Int
	Receiver           common.Address
	PurchaseLimitation *big.Int
	StartTime          *big.Int
	EndTime            *big.Int
	Raw                types.Log // Blockchain specific contextual infos
}

// FilterSetFlashSale is a free log retrieval operation binding the contract event 0x8eedbedfb1ac65f891c5371476fb4bbfe44cf7e0f7762670fe54071a11f0157c.
//
// Solidity: event SetFlashSale(uint256 saleId, address flashSaleSetter, address tokenAddress, uint256 id, uint256 remainingAmount, address payTokenAddress, uint256 price, address receiver, uint256 purchaseLimitation, uint256 startTime, uint256 endTime)
func (_Okxnft3 *Okxnft3Filterer) FilterSetFlashSale(opts *bind.FilterOpts) (*Okxnft3SetFlashSaleIterator, error) {

	logs, sub, err := _Okxnft3.contract.FilterLogs(opts, "SetFlashSale")
	if err != nil {
		return nil, err
	}
	return &Okxnft3SetFlashSaleIterator{contract: _Okxnft3.contract, event: "SetFlashSale", logs: logs, sub: sub}, nil
}

// WatchSetFlashSale is a free log subscription operation binding the contract event 0x8eedbedfb1ac65f891c5371476fb4bbfe44cf7e0f7762670fe54071a11f0157c.
//
// Solidity: event SetFlashSale(uint256 saleId, address flashSaleSetter, address tokenAddress, uint256 id, uint256 remainingAmount, address payTokenAddress, uint256 price, address receiver, uint256 purchaseLimitation, uint256 startTime, uint256 endTime)
func (_Okxnft3 *Okxnft3Filterer) WatchSetFlashSale(opts *bind.WatchOpts, sink chan<- *Okxnft3SetFlashSale) (event.Subscription, error) {

	logs, sub, err := _Okxnft3.contract.WatchLogs(opts, "SetFlashSale")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Okxnft3SetFlashSale)
				if err := _Okxnft3.contract.UnpackLog(event, "SetFlashSale", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseSetFlashSale is a log parse operation binding the contract event 0x8eedbedfb1ac65f891c5371476fb4bbfe44cf7e0f7762670fe54071a11f0157c.
//
// Solidity: event SetFlashSale(uint256 saleId, address flashSaleSetter, address tokenAddress, uint256 id, uint256 remainingAmount, address payTokenAddress, uint256 price, address receiver, uint256 purchaseLimitation, uint256 startTime, uint256 endTime)
func (_Okxnft3 *Okxnft3Filterer) ParseSetFlashSale(log types.Log) (*Okxnft3SetFlashSale, error) {
	event := new(Okxnft3SetFlashSale)
	if err := _Okxnft3.contract.UnpackLog(event, "SetFlashSale", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Okxnft3SetWhitelistIterator is returned from FilterSetWhitelist and is used to iterate over the raw logs and unpacked data for SetWhitelist events raised by the Okxnft3 contract.
type Okxnft3SetWhitelistIterator struct {
	Event *Okxnft3SetWhitelist // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Okxnft3SetWhitelistIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Okxnft3SetWhitelist)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Okxnft3SetWhitelist)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Okxnft3SetWhitelistIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Okxnft3SetWhitelistIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Okxnft3SetWhitelist represents a SetWhitelist event raised by the Okxnft3 contract.
type Okxnft3SetWhitelist struct {
	MemberAddr   common.Address
	Jurisdiction bool
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterSetWhitelist is a free log retrieval operation binding the contract event 0xf6019ec0a78d156d249a1ec7579e2321f6ac7521d6e1d2eacf90ba4a184dcceb.
//
// Solidity: event SetWhitelist(address memberAddr, bool jurisdiction)
func (_Okxnft3 *Okxnft3Filterer) FilterSetWhitelist(opts *bind.FilterOpts) (*Okxnft3SetWhitelistIterator, error) {

	logs, sub, err := _Okxnft3.contract.FilterLogs(opts, "SetWhitelist")
	if err != nil {
		return nil, err
	}
	return &Okxnft3SetWhitelistIterator{contract: _Okxnft3.contract, event: "SetWhitelist", logs: logs, sub: sub}, nil
}

// WatchSetWhitelist is a free log subscription operation binding the contract event 0xf6019ec0a78d156d249a1ec7579e2321f6ac7521d6e1d2eacf90ba4a184dcceb.
//
// Solidity: event SetWhitelist(address memberAddr, bool jurisdiction)
func (_Okxnft3 *Okxnft3Filterer) WatchSetWhitelist(opts *bind.WatchOpts, sink chan<- *Okxnft3SetWhitelist) (event.Subscription, error) {

	logs, sub, err := _Okxnft3.contract.WatchLogs(opts, "SetWhitelist")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Okxnft3SetWhitelist)
				if err := _Okxnft3.contract.UnpackLog(event, "SetWhitelist", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseSetWhitelist is a log parse operation binding the contract event 0xf6019ec0a78d156d249a1ec7579e2321f6ac7521d6e1d2eacf90ba4a184dcceb.
//
// Solidity: event SetWhitelist(address memberAddr, bool jurisdiction)
func (_Okxnft3 *Okxnft3Filterer) ParseSetWhitelist(log types.Log) (*Okxnft3SetWhitelist, error) {
	event := new(Okxnft3SetWhitelist)
	if err := _Okxnft3.contract.UnpackLog(event, "SetWhitelist", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Okxnft3UpdateFlashSaleIterator is returned from FilterUpdateFlashSale and is used to iterate over the raw logs and unpacked data for UpdateFlashSale events raised by the Okxnft3 contract.
type Okxnft3UpdateFlashSaleIterator struct {
	Event *Okxnft3UpdateFlashSale // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Okxnft3UpdateFlashSaleIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Okxnft3UpdateFlashSale)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Okxnft3UpdateFlashSale)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Okxnft3UpdateFlashSaleIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Okxnft3UpdateFlashSaleIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Okxnft3UpdateFlashSale represents a UpdateFlashSale event raised by the Okxnft3 contract.
type Okxnft3UpdateFlashSale struct {
	SaleId                *big.Int
	Operator              common.Address
	NewTokenAddress       common.Address
	NewId                 *big.Int
	NewRemainingAmount    *big.Int
	NewPayTokenAddress    common.Address
	NewPrice              *big.Int
	NewReceiver           common.Address
	NewPurchaseLimitation *big.Int
	NewStartTime          *big.Int
	NewEndTime            *big.Int
	Raw                   types.Log // Blockchain specific contextual infos
}

// FilterUpdateFlashSale is a free log retrieval operation binding the contract event 0xb9337dc4995191c2c3036e222f992474c8b6e41e5f34adbc3e9f9c600059affb.
//
// Solidity: event UpdateFlashSale(uint256 saleId, address operator, address newTokenAddress, uint256 newId, uint256 newRemainingAmount, address newPayTokenAddress, uint256 newPrice, address newReceiver, uint256 newPurchaseLimitation, uint256 newStartTime, uint256 newEndTime)
func (_Okxnft3 *Okxnft3Filterer) FilterUpdateFlashSale(opts *bind.FilterOpts) (*Okxnft3UpdateFlashSaleIterator, error) {

	logs, sub, err := _Okxnft3.contract.FilterLogs(opts, "UpdateFlashSale")
	if err != nil {
		return nil, err
	}
	return &Okxnft3UpdateFlashSaleIterator{contract: _Okxnft3.contract, event: "UpdateFlashSale", logs: logs, sub: sub}, nil
}

// WatchUpdateFlashSale is a free log subscription operation binding the contract event 0xb9337dc4995191c2c3036e222f992474c8b6e41e5f34adbc3e9f9c600059affb.
//
// Solidity: event UpdateFlashSale(uint256 saleId, address operator, address newTokenAddress, uint256 newId, uint256 newRemainingAmount, address newPayTokenAddress, uint256 newPrice, address newReceiver, uint256 newPurchaseLimitation, uint256 newStartTime, uint256 newEndTime)
func (_Okxnft3 *Okxnft3Filterer) WatchUpdateFlashSale(opts *bind.WatchOpts, sink chan<- *Okxnft3UpdateFlashSale) (event.Subscription, error) {

	logs, sub, err := _Okxnft3.contract.WatchLogs(opts, "UpdateFlashSale")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Okxnft3UpdateFlashSale)
				if err := _Okxnft3.contract.UnpackLog(event, "UpdateFlashSale", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUpdateFlashSale is a log parse operation binding the contract event 0xb9337dc4995191c2c3036e222f992474c8b6e41e5f34adbc3e9f9c600059affb.
//
// Solidity: event UpdateFlashSale(uint256 saleId, address operator, address newTokenAddress, uint256 newId, uint256 newRemainingAmount, address newPayTokenAddress, uint256 newPrice, address newReceiver, uint256 newPurchaseLimitation, uint256 newStartTime, uint256 newEndTime)
func (_Okxnft3 *Okxnft3Filterer) ParseUpdateFlashSale(log types.Log) (*Okxnft3UpdateFlashSale, error) {
	event := new(Okxnft3UpdateFlashSale)
	if err := _Okxnft3.contract.UnpackLog(event, "UpdateFlashSale", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
