// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package ghostmarket_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Ghostmarket0MetaData contains all meta data concerning the Ghostmarket0 contract.
var Ghostmarket0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_logic\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"admin_\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"_data\",\"type\":\"bytes\"}],\"stateMutability\":\"payable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"previousAdmin\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"newAdmin\",\"type\":\"address\"}],\"name\":\"AdminChanged\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"beacon\",\"type\":\"address\"}],\"name\":\"BeaconUpgraded\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"implementation\",\"type\":\"address\"}],\"name\":\"Upgraded\",\"type\":\"event\"},{\"stateMutability\":\"payable\",\"type\":\"fallback\"},{\"inputs\":[],\"name\":\"admin\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"admin_\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newAdmin\",\"type\":\"address\"}],\"name\":\"changeAdmin\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"implementation\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"implementation_\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newImplementation\",\"type\":\"address\"}],\"name\":\"upgradeTo\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newImplementation\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"data\",\"type\":\"bytes\"}],\"name\":\"upgradeToAndCall\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
}

// Ghostmarket0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Ghostmarket0MetaData.ABI instead.
var Ghostmarket0ABI = Ghostmarket0MetaData.ABI

// Ghostmarket0 is an auto generated Go binding around an Ethereum contract.
type Ghostmarket0 struct {
	Ghostmarket0Caller     // Read-only binding to the contract
	Ghostmarket0Transactor // Write-only binding to the contract
	Ghostmarket0Filterer   // Log filterer for contract events
}

// Ghostmarket0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Ghostmarket0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Ghostmarket0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Ghostmarket0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Ghostmarket0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Ghostmarket0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Ghostmarket0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Ghostmarket0Session struct {
	Contract     *Ghostmarket0     // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Ghostmarket0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Ghostmarket0CallerSession struct {
	Contract *Ghostmarket0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts       // Call options to use throughout this session
}

// Ghostmarket0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Ghostmarket0TransactorSession struct {
	Contract     *Ghostmarket0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts       // Transaction auth options to use throughout this session
}

// Ghostmarket0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Ghostmarket0Raw struct {
	Contract *Ghostmarket0 // Generic contract binding to access the raw methods on
}

// Ghostmarket0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Ghostmarket0CallerRaw struct {
	Contract *Ghostmarket0Caller // Generic read-only contract binding to access the raw methods on
}

// Ghostmarket0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Ghostmarket0TransactorRaw struct {
	Contract *Ghostmarket0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewGhostmarket0 creates a new instance of Ghostmarket0, bound to a specific deployed contract.
func NewGhostmarket0(address common.Address, backend bind.ContractBackend) (*Ghostmarket0, error) {
	contract, err := bindGhostmarket0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Ghostmarket0{Ghostmarket0Caller: Ghostmarket0Caller{contract: contract}, Ghostmarket0Transactor: Ghostmarket0Transactor{contract: contract}, Ghostmarket0Filterer: Ghostmarket0Filterer{contract: contract}}, nil
}

// NewGhostmarket0Caller creates a new read-only instance of Ghostmarket0, bound to a specific deployed contract.
func NewGhostmarket0Caller(address common.Address, caller bind.ContractCaller) (*Ghostmarket0Caller, error) {
	contract, err := bindGhostmarket0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Ghostmarket0Caller{contract: contract}, nil
}

// NewGhostmarket0Transactor creates a new write-only instance of Ghostmarket0, bound to a specific deployed contract.
func NewGhostmarket0Transactor(address common.Address, transactor bind.ContractTransactor) (*Ghostmarket0Transactor, error) {
	contract, err := bindGhostmarket0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Ghostmarket0Transactor{contract: contract}, nil
}

// NewGhostmarket0Filterer creates a new log filterer instance of Ghostmarket0, bound to a specific deployed contract.
func NewGhostmarket0Filterer(address common.Address, filterer bind.ContractFilterer) (*Ghostmarket0Filterer, error) {
	contract, err := bindGhostmarket0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Ghostmarket0Filterer{contract: contract}, nil
}

// bindGhostmarket0 binds a generic wrapper to an already deployed contract.
func bindGhostmarket0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Ghostmarket0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Ghostmarket0 *Ghostmarket0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Ghostmarket0.Contract.Ghostmarket0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Ghostmarket0 *Ghostmarket0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ghostmarket0.Contract.Ghostmarket0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Ghostmarket0 *Ghostmarket0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Ghostmarket0.Contract.Ghostmarket0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Ghostmarket0 *Ghostmarket0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Ghostmarket0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Ghostmarket0 *Ghostmarket0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ghostmarket0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Ghostmarket0 *Ghostmarket0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Ghostmarket0.Contract.contract.Transact(opts, method, params...)
}

// Admin is a paid mutator transaction binding the contract method 0xf851a440.
//
// Solidity: function admin() returns(address admin_)
func (_Ghostmarket0 *Ghostmarket0Transactor) Admin(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ghostmarket0.contract.Transact(opts, "admin")
}

// Admin is a paid mutator transaction binding the contract method 0xf851a440.
//
// Solidity: function admin() returns(address admin_)
func (_Ghostmarket0 *Ghostmarket0Session) Admin() (*types.Transaction, error) {
	return _Ghostmarket0.Contract.Admin(&_Ghostmarket0.TransactOpts)
}

// Admin is a paid mutator transaction binding the contract method 0xf851a440.
//
// Solidity: function admin() returns(address admin_)
func (_Ghostmarket0 *Ghostmarket0TransactorSession) Admin() (*types.Transaction, error) {
	return _Ghostmarket0.Contract.Admin(&_Ghostmarket0.TransactOpts)
}

// ChangeAdmin is a paid mutator transaction binding the contract method 0x8f283970.
//
// Solidity: function changeAdmin(address newAdmin) returns()
func (_Ghostmarket0 *Ghostmarket0Transactor) ChangeAdmin(opts *bind.TransactOpts, newAdmin common.Address) (*types.Transaction, error) {
	return _Ghostmarket0.contract.Transact(opts, "changeAdmin", newAdmin)
}

// ChangeAdmin is a paid mutator transaction binding the contract method 0x8f283970.
//
// Solidity: function changeAdmin(address newAdmin) returns()
func (_Ghostmarket0 *Ghostmarket0Session) ChangeAdmin(newAdmin common.Address) (*types.Transaction, error) {
	return _Ghostmarket0.Contract.ChangeAdmin(&_Ghostmarket0.TransactOpts, newAdmin)
}

// ChangeAdmin is a paid mutator transaction binding the contract method 0x8f283970.
//
// Solidity: function changeAdmin(address newAdmin) returns()
func (_Ghostmarket0 *Ghostmarket0TransactorSession) ChangeAdmin(newAdmin common.Address) (*types.Transaction, error) {
	return _Ghostmarket0.Contract.ChangeAdmin(&_Ghostmarket0.TransactOpts, newAdmin)
}

// Implementation is a paid mutator transaction binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() returns(address implementation_)
func (_Ghostmarket0 *Ghostmarket0Transactor) Implementation(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ghostmarket0.contract.Transact(opts, "implementation")
}

// Implementation is a paid mutator transaction binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() returns(address implementation_)
func (_Ghostmarket0 *Ghostmarket0Session) Implementation() (*types.Transaction, error) {
	return _Ghostmarket0.Contract.Implementation(&_Ghostmarket0.TransactOpts)
}

// Implementation is a paid mutator transaction binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() returns(address implementation_)
func (_Ghostmarket0 *Ghostmarket0TransactorSession) Implementation() (*types.Transaction, error) {
	return _Ghostmarket0.Contract.Implementation(&_Ghostmarket0.TransactOpts)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address newImplementation) returns()
func (_Ghostmarket0 *Ghostmarket0Transactor) UpgradeTo(opts *bind.TransactOpts, newImplementation common.Address) (*types.Transaction, error) {
	return _Ghostmarket0.contract.Transact(opts, "upgradeTo", newImplementation)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address newImplementation) returns()
func (_Ghostmarket0 *Ghostmarket0Session) UpgradeTo(newImplementation common.Address) (*types.Transaction, error) {
	return _Ghostmarket0.Contract.UpgradeTo(&_Ghostmarket0.TransactOpts, newImplementation)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address newImplementation) returns()
func (_Ghostmarket0 *Ghostmarket0TransactorSession) UpgradeTo(newImplementation common.Address) (*types.Transaction, error) {
	return _Ghostmarket0.Contract.UpgradeTo(&_Ghostmarket0.TransactOpts, newImplementation)
}

// UpgradeToAndCall is a paid mutator transaction binding the contract method 0x4f1ef286.
//
// Solidity: function upgradeToAndCall(address newImplementation, bytes data) payable returns()
func (_Ghostmarket0 *Ghostmarket0Transactor) UpgradeToAndCall(opts *bind.TransactOpts, newImplementation common.Address, data []byte) (*types.Transaction, error) {
	return _Ghostmarket0.contract.Transact(opts, "upgradeToAndCall", newImplementation, data)
}

// UpgradeToAndCall is a paid mutator transaction binding the contract method 0x4f1ef286.
//
// Solidity: function upgradeToAndCall(address newImplementation, bytes data) payable returns()
func (_Ghostmarket0 *Ghostmarket0Session) UpgradeToAndCall(newImplementation common.Address, data []byte) (*types.Transaction, error) {
	return _Ghostmarket0.Contract.UpgradeToAndCall(&_Ghostmarket0.TransactOpts, newImplementation, data)
}

// UpgradeToAndCall is a paid mutator transaction binding the contract method 0x4f1ef286.
//
// Solidity: function upgradeToAndCall(address newImplementation, bytes data) payable returns()
func (_Ghostmarket0 *Ghostmarket0TransactorSession) UpgradeToAndCall(newImplementation common.Address, data []byte) (*types.Transaction, error) {
	return _Ghostmarket0.Contract.UpgradeToAndCall(&_Ghostmarket0.TransactOpts, newImplementation, data)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Ghostmarket0 *Ghostmarket0Transactor) Fallback(opts *bind.TransactOpts, calldata []byte) (*types.Transaction, error) {
	return _Ghostmarket0.contract.RawTransact(opts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Ghostmarket0 *Ghostmarket0Session) Fallback(calldata []byte) (*types.Transaction, error) {
	return _Ghostmarket0.Contract.Fallback(&_Ghostmarket0.TransactOpts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Ghostmarket0 *Ghostmarket0TransactorSession) Fallback(calldata []byte) (*types.Transaction, error) {
	return _Ghostmarket0.Contract.Fallback(&_Ghostmarket0.TransactOpts, calldata)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Ghostmarket0 *Ghostmarket0Transactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ghostmarket0.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Ghostmarket0 *Ghostmarket0Session) Receive() (*types.Transaction, error) {
	return _Ghostmarket0.Contract.Receive(&_Ghostmarket0.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Ghostmarket0 *Ghostmarket0TransactorSession) Receive() (*types.Transaction, error) {
	return _Ghostmarket0.Contract.Receive(&_Ghostmarket0.TransactOpts)
}

// Ghostmarket0AdminChangedIterator is returned from FilterAdminChanged and is used to iterate over the raw logs and unpacked data for AdminChanged events raised by the Ghostmarket0 contract.
type Ghostmarket0AdminChangedIterator struct {
	Event *Ghostmarket0AdminChanged // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Ghostmarket0AdminChangedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Ghostmarket0AdminChanged)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Ghostmarket0AdminChanged)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Ghostmarket0AdminChangedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Ghostmarket0AdminChangedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Ghostmarket0AdminChanged represents a AdminChanged event raised by the Ghostmarket0 contract.
type Ghostmarket0AdminChanged struct {
	PreviousAdmin common.Address
	NewAdmin      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterAdminChanged is a free log retrieval operation binding the contract event 0x7e644d79422f17c01e4894b5f4f588d331ebfa28653d42ae832dc59e38c9798f.
//
// Solidity: event AdminChanged(address previousAdmin, address newAdmin)
func (_Ghostmarket0 *Ghostmarket0Filterer) FilterAdminChanged(opts *bind.FilterOpts) (*Ghostmarket0AdminChangedIterator, error) {

	logs, sub, err := _Ghostmarket0.contract.FilterLogs(opts, "AdminChanged")
	if err != nil {
		return nil, err
	}
	return &Ghostmarket0AdminChangedIterator{contract: _Ghostmarket0.contract, event: "AdminChanged", logs: logs, sub: sub}, nil
}

// WatchAdminChanged is a free log subscription operation binding the contract event 0x7e644d79422f17c01e4894b5f4f588d331ebfa28653d42ae832dc59e38c9798f.
//
// Solidity: event AdminChanged(address previousAdmin, address newAdmin)
func (_Ghostmarket0 *Ghostmarket0Filterer) WatchAdminChanged(opts *bind.WatchOpts, sink chan<- *Ghostmarket0AdminChanged) (event.Subscription, error) {

	logs, sub, err := _Ghostmarket0.contract.WatchLogs(opts, "AdminChanged")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Ghostmarket0AdminChanged)
				if err := _Ghostmarket0.contract.UnpackLog(event, "AdminChanged", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminChanged is a log parse operation binding the contract event 0x7e644d79422f17c01e4894b5f4f588d331ebfa28653d42ae832dc59e38c9798f.
//
// Solidity: event AdminChanged(address previousAdmin, address newAdmin)
func (_Ghostmarket0 *Ghostmarket0Filterer) ParseAdminChanged(log types.Log) (*Ghostmarket0AdminChanged, error) {
	event := new(Ghostmarket0AdminChanged)
	if err := _Ghostmarket0.contract.UnpackLog(event, "AdminChanged", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Ghostmarket0BeaconUpgradedIterator is returned from FilterBeaconUpgraded and is used to iterate over the raw logs and unpacked data for BeaconUpgraded events raised by the Ghostmarket0 contract.
type Ghostmarket0BeaconUpgradedIterator struct {
	Event *Ghostmarket0BeaconUpgraded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Ghostmarket0BeaconUpgradedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Ghostmarket0BeaconUpgraded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Ghostmarket0BeaconUpgraded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Ghostmarket0BeaconUpgradedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Ghostmarket0BeaconUpgradedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Ghostmarket0BeaconUpgraded represents a BeaconUpgraded event raised by the Ghostmarket0 contract.
type Ghostmarket0BeaconUpgraded struct {
	Beacon common.Address
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterBeaconUpgraded is a free log retrieval operation binding the contract event 0x1cf3b03a6cf19fa2baba4df148e9dcabedea7f8a5c07840e207e5c089be95d3e.
//
// Solidity: event BeaconUpgraded(address indexed beacon)
func (_Ghostmarket0 *Ghostmarket0Filterer) FilterBeaconUpgraded(opts *bind.FilterOpts, beacon []common.Address) (*Ghostmarket0BeaconUpgradedIterator, error) {

	var beaconRule []interface{}
	for _, beaconItem := range beacon {
		beaconRule = append(beaconRule, beaconItem)
	}

	logs, sub, err := _Ghostmarket0.contract.FilterLogs(opts, "BeaconUpgraded", beaconRule)
	if err != nil {
		return nil, err
	}
	return &Ghostmarket0BeaconUpgradedIterator{contract: _Ghostmarket0.contract, event: "BeaconUpgraded", logs: logs, sub: sub}, nil
}

// WatchBeaconUpgraded is a free log subscription operation binding the contract event 0x1cf3b03a6cf19fa2baba4df148e9dcabedea7f8a5c07840e207e5c089be95d3e.
//
// Solidity: event BeaconUpgraded(address indexed beacon)
func (_Ghostmarket0 *Ghostmarket0Filterer) WatchBeaconUpgraded(opts *bind.WatchOpts, sink chan<- *Ghostmarket0BeaconUpgraded, beacon []common.Address) (event.Subscription, error) {

	var beaconRule []interface{}
	for _, beaconItem := range beacon {
		beaconRule = append(beaconRule, beaconItem)
	}

	logs, sub, err := _Ghostmarket0.contract.WatchLogs(opts, "BeaconUpgraded", beaconRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Ghostmarket0BeaconUpgraded)
				if err := _Ghostmarket0.contract.UnpackLog(event, "BeaconUpgraded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBeaconUpgraded is a log parse operation binding the contract event 0x1cf3b03a6cf19fa2baba4df148e9dcabedea7f8a5c07840e207e5c089be95d3e.
//
// Solidity: event BeaconUpgraded(address indexed beacon)
func (_Ghostmarket0 *Ghostmarket0Filterer) ParseBeaconUpgraded(log types.Log) (*Ghostmarket0BeaconUpgraded, error) {
	event := new(Ghostmarket0BeaconUpgraded)
	if err := _Ghostmarket0.contract.UnpackLog(event, "BeaconUpgraded", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Ghostmarket0UpgradedIterator is returned from FilterUpgraded and is used to iterate over the raw logs and unpacked data for Upgraded events raised by the Ghostmarket0 contract.
type Ghostmarket0UpgradedIterator struct {
	Event *Ghostmarket0Upgraded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Ghostmarket0UpgradedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Ghostmarket0Upgraded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Ghostmarket0Upgraded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Ghostmarket0UpgradedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Ghostmarket0UpgradedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Ghostmarket0Upgraded represents a Upgraded event raised by the Ghostmarket0 contract.
type Ghostmarket0Upgraded struct {
	Implementation common.Address
	Raw            types.Log // Blockchain specific contextual infos
}

// FilterUpgraded is a free log retrieval operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Ghostmarket0 *Ghostmarket0Filterer) FilterUpgraded(opts *bind.FilterOpts, implementation []common.Address) (*Ghostmarket0UpgradedIterator, error) {

	var implementationRule []interface{}
	for _, implementationItem := range implementation {
		implementationRule = append(implementationRule, implementationItem)
	}

	logs, sub, err := _Ghostmarket0.contract.FilterLogs(opts, "Upgraded", implementationRule)
	if err != nil {
		return nil, err
	}
	return &Ghostmarket0UpgradedIterator{contract: _Ghostmarket0.contract, event: "Upgraded", logs: logs, sub: sub}, nil
}

// WatchUpgraded is a free log subscription operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Ghostmarket0 *Ghostmarket0Filterer) WatchUpgraded(opts *bind.WatchOpts, sink chan<- *Ghostmarket0Upgraded, implementation []common.Address) (event.Subscription, error) {

	var implementationRule []interface{}
	for _, implementationItem := range implementation {
		implementationRule = append(implementationRule, implementationItem)
	}

	logs, sub, err := _Ghostmarket0.contract.WatchLogs(opts, "Upgraded", implementationRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Ghostmarket0Upgraded)
				if err := _Ghostmarket0.contract.UnpackLog(event, "Upgraded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUpgraded is a log parse operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Ghostmarket0 *Ghostmarket0Filterer) ParseUpgraded(log types.Log) (*Ghostmarket0Upgraded, error) {
	event := new(Ghostmarket0Upgraded)
	if err := _Ghostmarket0.contract.UnpackLog(event, "Upgraded", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
