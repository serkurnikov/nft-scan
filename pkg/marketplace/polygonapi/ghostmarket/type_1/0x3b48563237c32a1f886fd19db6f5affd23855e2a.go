// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package ghostmarket_1

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Ghostmarket1MetaData contains all meta data concerning the Ghostmarket1 contract.
var Ghostmarket1MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"initialLogic\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"initialAdmin\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"_data\",\"type\":\"bytes\"}],\"stateMutability\":\"payable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"previousAdmin\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"newAdmin\",\"type\":\"address\"}],\"name\":\"AdminChanged\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"implementation\",\"type\":\"address\"}],\"name\":\"Upgraded\",\"type\":\"event\"},{\"stateMutability\":\"payable\",\"type\":\"fallback\"},{\"inputs\":[],\"name\":\"admin\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newAdmin\",\"type\":\"address\"}],\"name\":\"changeAdmin\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"implementation\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newImplementation\",\"type\":\"address\"}],\"name\":\"upgradeTo\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newImplementation\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"data\",\"type\":\"bytes\"}],\"name\":\"upgradeToAndCall\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
}

// Ghostmarket1ABI is the input ABI used to generate the binding from.
// Deprecated: Use Ghostmarket1MetaData.ABI instead.
var Ghostmarket1ABI = Ghostmarket1MetaData.ABI

// Ghostmarket1 is an auto generated Go binding around an Ethereum contract.
type Ghostmarket1 struct {
	Ghostmarket1Caller     // Read-only binding to the contract
	Ghostmarket1Transactor // Write-only binding to the contract
	Ghostmarket1Filterer   // Log filterer for contract events
}

// Ghostmarket1Caller is an auto generated read-only Go binding around an Ethereum contract.
type Ghostmarket1Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Ghostmarket1Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Ghostmarket1Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Ghostmarket1Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Ghostmarket1Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Ghostmarket1Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Ghostmarket1Session struct {
	Contract     *Ghostmarket1     // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Ghostmarket1CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Ghostmarket1CallerSession struct {
	Contract *Ghostmarket1Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts       // Call options to use throughout this session
}

// Ghostmarket1TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Ghostmarket1TransactorSession struct {
	Contract     *Ghostmarket1Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts       // Transaction auth options to use throughout this session
}

// Ghostmarket1Raw is an auto generated low-level Go binding around an Ethereum contract.
type Ghostmarket1Raw struct {
	Contract *Ghostmarket1 // Generic contract binding to access the raw methods on
}

// Ghostmarket1CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Ghostmarket1CallerRaw struct {
	Contract *Ghostmarket1Caller // Generic read-only contract binding to access the raw methods on
}

// Ghostmarket1TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Ghostmarket1TransactorRaw struct {
	Contract *Ghostmarket1Transactor // Generic write-only contract binding to access the raw methods on
}

// NewGhostmarket1 creates a new instance of Ghostmarket1, bound to a specific deployed contract.
func NewGhostmarket1(address common.Address, backend bind.ContractBackend) (*Ghostmarket1, error) {
	contract, err := bindGhostmarket1(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Ghostmarket1{Ghostmarket1Caller: Ghostmarket1Caller{contract: contract}, Ghostmarket1Transactor: Ghostmarket1Transactor{contract: contract}, Ghostmarket1Filterer: Ghostmarket1Filterer{contract: contract}}, nil
}

// NewGhostmarket1Caller creates a new read-only instance of Ghostmarket1, bound to a specific deployed contract.
func NewGhostmarket1Caller(address common.Address, caller bind.ContractCaller) (*Ghostmarket1Caller, error) {
	contract, err := bindGhostmarket1(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Ghostmarket1Caller{contract: contract}, nil
}

// NewGhostmarket1Transactor creates a new write-only instance of Ghostmarket1, bound to a specific deployed contract.
func NewGhostmarket1Transactor(address common.Address, transactor bind.ContractTransactor) (*Ghostmarket1Transactor, error) {
	contract, err := bindGhostmarket1(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Ghostmarket1Transactor{contract: contract}, nil
}

// NewGhostmarket1Filterer creates a new log filterer instance of Ghostmarket1, bound to a specific deployed contract.
func NewGhostmarket1Filterer(address common.Address, filterer bind.ContractFilterer) (*Ghostmarket1Filterer, error) {
	contract, err := bindGhostmarket1(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Ghostmarket1Filterer{contract: contract}, nil
}

// bindGhostmarket1 binds a generic wrapper to an already deployed contract.
func bindGhostmarket1(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Ghostmarket1ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Ghostmarket1 *Ghostmarket1Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Ghostmarket1.Contract.Ghostmarket1Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Ghostmarket1 *Ghostmarket1Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ghostmarket1.Contract.Ghostmarket1Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Ghostmarket1 *Ghostmarket1Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Ghostmarket1.Contract.Ghostmarket1Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Ghostmarket1 *Ghostmarket1CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Ghostmarket1.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Ghostmarket1 *Ghostmarket1TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ghostmarket1.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Ghostmarket1 *Ghostmarket1TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Ghostmarket1.Contract.contract.Transact(opts, method, params...)
}

// Admin is a paid mutator transaction binding the contract method 0xf851a440.
//
// Solidity: function admin() returns(address)
func (_Ghostmarket1 *Ghostmarket1Transactor) Admin(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ghostmarket1.contract.Transact(opts, "admin")
}

// Admin is a paid mutator transaction binding the contract method 0xf851a440.
//
// Solidity: function admin() returns(address)
func (_Ghostmarket1 *Ghostmarket1Session) Admin() (*types.Transaction, error) {
	return _Ghostmarket1.Contract.Admin(&_Ghostmarket1.TransactOpts)
}

// Admin is a paid mutator transaction binding the contract method 0xf851a440.
//
// Solidity: function admin() returns(address)
func (_Ghostmarket1 *Ghostmarket1TransactorSession) Admin() (*types.Transaction, error) {
	return _Ghostmarket1.Contract.Admin(&_Ghostmarket1.TransactOpts)
}

// ChangeAdmin is a paid mutator transaction binding the contract method 0x8f283970.
//
// Solidity: function changeAdmin(address newAdmin) returns()
func (_Ghostmarket1 *Ghostmarket1Transactor) ChangeAdmin(opts *bind.TransactOpts, newAdmin common.Address) (*types.Transaction, error) {
	return _Ghostmarket1.contract.Transact(opts, "changeAdmin", newAdmin)
}

// ChangeAdmin is a paid mutator transaction binding the contract method 0x8f283970.
//
// Solidity: function changeAdmin(address newAdmin) returns()
func (_Ghostmarket1 *Ghostmarket1Session) ChangeAdmin(newAdmin common.Address) (*types.Transaction, error) {
	return _Ghostmarket1.Contract.ChangeAdmin(&_Ghostmarket1.TransactOpts, newAdmin)
}

// ChangeAdmin is a paid mutator transaction binding the contract method 0x8f283970.
//
// Solidity: function changeAdmin(address newAdmin) returns()
func (_Ghostmarket1 *Ghostmarket1TransactorSession) ChangeAdmin(newAdmin common.Address) (*types.Transaction, error) {
	return _Ghostmarket1.Contract.ChangeAdmin(&_Ghostmarket1.TransactOpts, newAdmin)
}

// Implementation is a paid mutator transaction binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() returns(address)
func (_Ghostmarket1 *Ghostmarket1Transactor) Implementation(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ghostmarket1.contract.Transact(opts, "implementation")
}

// Implementation is a paid mutator transaction binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() returns(address)
func (_Ghostmarket1 *Ghostmarket1Session) Implementation() (*types.Transaction, error) {
	return _Ghostmarket1.Contract.Implementation(&_Ghostmarket1.TransactOpts)
}

// Implementation is a paid mutator transaction binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() returns(address)
func (_Ghostmarket1 *Ghostmarket1TransactorSession) Implementation() (*types.Transaction, error) {
	return _Ghostmarket1.Contract.Implementation(&_Ghostmarket1.TransactOpts)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address newImplementation) returns()
func (_Ghostmarket1 *Ghostmarket1Transactor) UpgradeTo(opts *bind.TransactOpts, newImplementation common.Address) (*types.Transaction, error) {
	return _Ghostmarket1.contract.Transact(opts, "upgradeTo", newImplementation)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address newImplementation) returns()
func (_Ghostmarket1 *Ghostmarket1Session) UpgradeTo(newImplementation common.Address) (*types.Transaction, error) {
	return _Ghostmarket1.Contract.UpgradeTo(&_Ghostmarket1.TransactOpts, newImplementation)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address newImplementation) returns()
func (_Ghostmarket1 *Ghostmarket1TransactorSession) UpgradeTo(newImplementation common.Address) (*types.Transaction, error) {
	return _Ghostmarket1.Contract.UpgradeTo(&_Ghostmarket1.TransactOpts, newImplementation)
}

// UpgradeToAndCall is a paid mutator transaction binding the contract method 0x4f1ef286.
//
// Solidity: function upgradeToAndCall(address newImplementation, bytes data) payable returns()
func (_Ghostmarket1 *Ghostmarket1Transactor) UpgradeToAndCall(opts *bind.TransactOpts, newImplementation common.Address, data []byte) (*types.Transaction, error) {
	return _Ghostmarket1.contract.Transact(opts, "upgradeToAndCall", newImplementation, data)
}

// UpgradeToAndCall is a paid mutator transaction binding the contract method 0x4f1ef286.
//
// Solidity: function upgradeToAndCall(address newImplementation, bytes data) payable returns()
func (_Ghostmarket1 *Ghostmarket1Session) UpgradeToAndCall(newImplementation common.Address, data []byte) (*types.Transaction, error) {
	return _Ghostmarket1.Contract.UpgradeToAndCall(&_Ghostmarket1.TransactOpts, newImplementation, data)
}

// UpgradeToAndCall is a paid mutator transaction binding the contract method 0x4f1ef286.
//
// Solidity: function upgradeToAndCall(address newImplementation, bytes data) payable returns()
func (_Ghostmarket1 *Ghostmarket1TransactorSession) UpgradeToAndCall(newImplementation common.Address, data []byte) (*types.Transaction, error) {
	return _Ghostmarket1.Contract.UpgradeToAndCall(&_Ghostmarket1.TransactOpts, newImplementation, data)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Ghostmarket1 *Ghostmarket1Transactor) Fallback(opts *bind.TransactOpts, calldata []byte) (*types.Transaction, error) {
	return _Ghostmarket1.contract.RawTransact(opts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Ghostmarket1 *Ghostmarket1Session) Fallback(calldata []byte) (*types.Transaction, error) {
	return _Ghostmarket1.Contract.Fallback(&_Ghostmarket1.TransactOpts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Ghostmarket1 *Ghostmarket1TransactorSession) Fallback(calldata []byte) (*types.Transaction, error) {
	return _Ghostmarket1.Contract.Fallback(&_Ghostmarket1.TransactOpts, calldata)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Ghostmarket1 *Ghostmarket1Transactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ghostmarket1.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Ghostmarket1 *Ghostmarket1Session) Receive() (*types.Transaction, error) {
	return _Ghostmarket1.Contract.Receive(&_Ghostmarket1.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Ghostmarket1 *Ghostmarket1TransactorSession) Receive() (*types.Transaction, error) {
	return _Ghostmarket1.Contract.Receive(&_Ghostmarket1.TransactOpts)
}

// Ghostmarket1AdminChangedIterator is returned from FilterAdminChanged and is used to iterate over the raw logs and unpacked data for AdminChanged events raised by the Ghostmarket1 contract.
type Ghostmarket1AdminChangedIterator struct {
	Event *Ghostmarket1AdminChanged // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Ghostmarket1AdminChangedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Ghostmarket1AdminChanged)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Ghostmarket1AdminChanged)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Ghostmarket1AdminChangedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Ghostmarket1AdminChangedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Ghostmarket1AdminChanged represents a AdminChanged event raised by the Ghostmarket1 contract.
type Ghostmarket1AdminChanged struct {
	PreviousAdmin common.Address
	NewAdmin      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterAdminChanged is a free log retrieval operation binding the contract event 0x7e644d79422f17c01e4894b5f4f588d331ebfa28653d42ae832dc59e38c9798f.
//
// Solidity: event AdminChanged(address previousAdmin, address newAdmin)
func (_Ghostmarket1 *Ghostmarket1Filterer) FilterAdminChanged(opts *bind.FilterOpts) (*Ghostmarket1AdminChangedIterator, error) {

	logs, sub, err := _Ghostmarket1.contract.FilterLogs(opts, "AdminChanged")
	if err != nil {
		return nil, err
	}
	return &Ghostmarket1AdminChangedIterator{contract: _Ghostmarket1.contract, event: "AdminChanged", logs: logs, sub: sub}, nil
}

// WatchAdminChanged is a free log subscription operation binding the contract event 0x7e644d79422f17c01e4894b5f4f588d331ebfa28653d42ae832dc59e38c9798f.
//
// Solidity: event AdminChanged(address previousAdmin, address newAdmin)
func (_Ghostmarket1 *Ghostmarket1Filterer) WatchAdminChanged(opts *bind.WatchOpts, sink chan<- *Ghostmarket1AdminChanged) (event.Subscription, error) {

	logs, sub, err := _Ghostmarket1.contract.WatchLogs(opts, "AdminChanged")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Ghostmarket1AdminChanged)
				if err := _Ghostmarket1.contract.UnpackLog(event, "AdminChanged", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminChanged is a log parse operation binding the contract event 0x7e644d79422f17c01e4894b5f4f588d331ebfa28653d42ae832dc59e38c9798f.
//
// Solidity: event AdminChanged(address previousAdmin, address newAdmin)
func (_Ghostmarket1 *Ghostmarket1Filterer) ParseAdminChanged(log types.Log) (*Ghostmarket1AdminChanged, error) {
	event := new(Ghostmarket1AdminChanged)
	if err := _Ghostmarket1.contract.UnpackLog(event, "AdminChanged", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Ghostmarket1UpgradedIterator is returned from FilterUpgraded and is used to iterate over the raw logs and unpacked data for Upgraded events raised by the Ghostmarket1 contract.
type Ghostmarket1UpgradedIterator struct {
	Event *Ghostmarket1Upgraded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Ghostmarket1UpgradedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Ghostmarket1Upgraded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Ghostmarket1Upgraded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Ghostmarket1UpgradedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Ghostmarket1UpgradedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Ghostmarket1Upgraded represents a Upgraded event raised by the Ghostmarket1 contract.
type Ghostmarket1Upgraded struct {
	Implementation common.Address
	Raw            types.Log // Blockchain specific contextual infos
}

// FilterUpgraded is a free log retrieval operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Ghostmarket1 *Ghostmarket1Filterer) FilterUpgraded(opts *bind.FilterOpts, implementation []common.Address) (*Ghostmarket1UpgradedIterator, error) {

	var implementationRule []interface{}
	for _, implementationItem := range implementation {
		implementationRule = append(implementationRule, implementationItem)
	}

	logs, sub, err := _Ghostmarket1.contract.FilterLogs(opts, "Upgraded", implementationRule)
	if err != nil {
		return nil, err
	}
	return &Ghostmarket1UpgradedIterator{contract: _Ghostmarket1.contract, event: "Upgraded", logs: logs, sub: sub}, nil
}

// WatchUpgraded is a free log subscription operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Ghostmarket1 *Ghostmarket1Filterer) WatchUpgraded(opts *bind.WatchOpts, sink chan<- *Ghostmarket1Upgraded, implementation []common.Address) (event.Subscription, error) {

	var implementationRule []interface{}
	for _, implementationItem := range implementation {
		implementationRule = append(implementationRule, implementationItem)
	}

	logs, sub, err := _Ghostmarket1.contract.WatchLogs(opts, "Upgraded", implementationRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Ghostmarket1Upgraded)
				if err := _Ghostmarket1.contract.UnpackLog(event, "Upgraded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUpgraded is a log parse operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Ghostmarket1 *Ghostmarket1Filterer) ParseUpgraded(log types.Log) (*Ghostmarket1Upgraded, error) {
	event := new(Ghostmarket1Upgraded)
	if err := _Ghostmarket1.contract.UnpackLog(event, "Upgraded", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
