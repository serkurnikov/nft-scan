// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package yetiswap_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Yetiswap0MetaData contains all meta data concerning the Yetiswap0 contract.
var Yetiswap0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_baseToken\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_commissionRateYTS\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_commissionRateNonYTS\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_yetiswapNFT\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_commissionTaker\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"collectionContract\",\"type\":\"address\"}],\"name\":\"CollectionRemoved\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_commissionRateNonYTS\",\"type\":\"uint256\"}],\"name\":\"CommissionChangedForNonYTS\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_commissionRateYTS\",\"type\":\"uint256\"}],\"name\":\"CommissionChangedForYTS\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_commissionTaker\",\"type\":\"address\"}],\"name\":\"CommissionTakerChanged\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"collectionContract\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_rewardRate\",\"type\":\"uint256\"}],\"name\":\"CreatorRateChanged\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"categoryId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"_categoryName\",\"type\":\"string\"}],\"name\":\"NewCategoryAdded\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"collectionContract\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"creatorAddress\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_rewardRate\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_category\",\"type\":\"uint256\"}],\"name\":\"NewCollectionAdded\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"tokenContract\",\"type\":\"address\"}],\"name\":\"NewTokenAdded\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"nftContract\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"creator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"nftId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"saleToken\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"category\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"saleIndex\",\"type\":\"uint256\"}],\"name\":\"TokenOnSale\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"tokenContract\",\"type\":\"address\"}],\"name\":\"TokenRemoved\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"saleIndex\",\"type\":\"uint256\"}],\"name\":\"TokenRemovedFromSale\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"saleIndex\",\"type\":\"uint256\"}],\"name\":\"TokenSold\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_categoryName\",\"type\":\"string\"}],\"name\":\"addNewCategory\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"collectionContract\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"creatorAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_rewardRate\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_category\",\"type\":\"uint256\"}],\"name\":\"addWhitelistedCollection\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"tokenContract\",\"type\":\"address\"}],\"name\":\"addWhitelistedToken\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"baseToken\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"buyNFT\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"buyNFTwithAvax\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"categories\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"categoryLength\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"collectionCategory\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"commissionRateNonYTS\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"commissionRateYTS\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"commissionTaker\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"creatorRate\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"creators\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"getCommissionRate\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"CommissionRate\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"getCreatorRate\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"NFTcreatorRate\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"getSale\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"tokenContract\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"saleToken\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"category\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"creator\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"getWhitelistedCollection\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"getWhitelistedToken\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"stakeContract\",\"type\":\"address\"}],\"name\":\"isWhitelistedCollection\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"tokenContract\",\"type\":\"address\"}],\"name\":\"isWhitelistedToken\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"numCollections\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"onSaleNftAmount\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"removeNFT\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"collectionContract\",\"type\":\"address\"}],\"name\":\"removeWhitelistedCollection\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"tokenContract\",\"type\":\"address\"}],\"name\":\"removeWhitelistedToken\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_tokenContract\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_saleToken\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_category\",\"type\":\"uint256\"}],\"name\":\"saleNFT\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_commissionRateYTS\",\"type\":\"uint256\"}],\"name\":\"setCommissionRate\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_commissionRateNonYTS\",\"type\":\"uint256\"}],\"name\":\"setCommissionRateAvax\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_commissionTaker\",\"type\":\"address\"}],\"name\":\"setCommissionTaker\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"collectionContract\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_rewardRate\",\"type\":\"uint256\"}],\"name\":\"updateCreatorRate\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"yetiswapNFT\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
}

// Yetiswap0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Yetiswap0MetaData.ABI instead.
var Yetiswap0ABI = Yetiswap0MetaData.ABI

// Yetiswap0 is an auto generated Go binding around an Ethereum contract.
type Yetiswap0 struct {
	Yetiswap0Caller     // Read-only binding to the contract
	Yetiswap0Transactor // Write-only binding to the contract
	Yetiswap0Filterer   // Log filterer for contract events
}

// Yetiswap0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Yetiswap0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Yetiswap0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Yetiswap0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Yetiswap0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Yetiswap0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Yetiswap0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Yetiswap0Session struct {
	Contract     *Yetiswap0        // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Yetiswap0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Yetiswap0CallerSession struct {
	Contract *Yetiswap0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts    // Call options to use throughout this session
}

// Yetiswap0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Yetiswap0TransactorSession struct {
	Contract     *Yetiswap0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts    // Transaction auth options to use throughout this session
}

// Yetiswap0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Yetiswap0Raw struct {
	Contract *Yetiswap0 // Generic contract binding to access the raw methods on
}

// Yetiswap0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Yetiswap0CallerRaw struct {
	Contract *Yetiswap0Caller // Generic read-only contract binding to access the raw methods on
}

// Yetiswap0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Yetiswap0TransactorRaw struct {
	Contract *Yetiswap0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewYetiswap0 creates a new instance of Yetiswap0, bound to a specific deployed contract.
func NewYetiswap0(address common.Address, backend bind.ContractBackend) (*Yetiswap0, error) {
	contract, err := bindYetiswap0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Yetiswap0{Yetiswap0Caller: Yetiswap0Caller{contract: contract}, Yetiswap0Transactor: Yetiswap0Transactor{contract: contract}, Yetiswap0Filterer: Yetiswap0Filterer{contract: contract}}, nil
}

// NewYetiswap0Caller creates a new read-only instance of Yetiswap0, bound to a specific deployed contract.
func NewYetiswap0Caller(address common.Address, caller bind.ContractCaller) (*Yetiswap0Caller, error) {
	contract, err := bindYetiswap0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Yetiswap0Caller{contract: contract}, nil
}

// NewYetiswap0Transactor creates a new write-only instance of Yetiswap0, bound to a specific deployed contract.
func NewYetiswap0Transactor(address common.Address, transactor bind.ContractTransactor) (*Yetiswap0Transactor, error) {
	contract, err := bindYetiswap0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Yetiswap0Transactor{contract: contract}, nil
}

// NewYetiswap0Filterer creates a new log filterer instance of Yetiswap0, bound to a specific deployed contract.
func NewYetiswap0Filterer(address common.Address, filterer bind.ContractFilterer) (*Yetiswap0Filterer, error) {
	contract, err := bindYetiswap0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Yetiswap0Filterer{contract: contract}, nil
}

// bindYetiswap0 binds a generic wrapper to an already deployed contract.
func bindYetiswap0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Yetiswap0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Yetiswap0 *Yetiswap0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Yetiswap0.Contract.Yetiswap0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Yetiswap0 *Yetiswap0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Yetiswap0.Contract.Yetiswap0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Yetiswap0 *Yetiswap0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Yetiswap0.Contract.Yetiswap0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Yetiswap0 *Yetiswap0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Yetiswap0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Yetiswap0 *Yetiswap0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Yetiswap0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Yetiswap0 *Yetiswap0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Yetiswap0.Contract.contract.Transact(opts, method, params...)
}

// BaseToken is a free data retrieval call binding the contract method 0xc55dae63.
//
// Solidity: function baseToken() view returns(address)
func (_Yetiswap0 *Yetiswap0Caller) BaseToken(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "baseToken")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// BaseToken is a free data retrieval call binding the contract method 0xc55dae63.
//
// Solidity: function baseToken() view returns(address)
func (_Yetiswap0 *Yetiswap0Session) BaseToken() (common.Address, error) {
	return _Yetiswap0.Contract.BaseToken(&_Yetiswap0.CallOpts)
}

// BaseToken is a free data retrieval call binding the contract method 0xc55dae63.
//
// Solidity: function baseToken() view returns(address)
func (_Yetiswap0 *Yetiswap0CallerSession) BaseToken() (common.Address, error) {
	return _Yetiswap0.Contract.BaseToken(&_Yetiswap0.CallOpts)
}

// Categories is a free data retrieval call binding the contract method 0xc6cdbe5e.
//
// Solidity: function categories(uint256 ) view returns(string)
func (_Yetiswap0 *Yetiswap0Caller) Categories(opts *bind.CallOpts, arg0 *big.Int) (string, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "categories", arg0)

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Categories is a free data retrieval call binding the contract method 0xc6cdbe5e.
//
// Solidity: function categories(uint256 ) view returns(string)
func (_Yetiswap0 *Yetiswap0Session) Categories(arg0 *big.Int) (string, error) {
	return _Yetiswap0.Contract.Categories(&_Yetiswap0.CallOpts, arg0)
}

// Categories is a free data retrieval call binding the contract method 0xc6cdbe5e.
//
// Solidity: function categories(uint256 ) view returns(string)
func (_Yetiswap0 *Yetiswap0CallerSession) Categories(arg0 *big.Int) (string, error) {
	return _Yetiswap0.Contract.Categories(&_Yetiswap0.CallOpts, arg0)
}

// CategoryLength is a free data retrieval call binding the contract method 0xee724fcf.
//
// Solidity: function categoryLength() view returns(uint256)
func (_Yetiswap0 *Yetiswap0Caller) CategoryLength(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "categoryLength")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CategoryLength is a free data retrieval call binding the contract method 0xee724fcf.
//
// Solidity: function categoryLength() view returns(uint256)
func (_Yetiswap0 *Yetiswap0Session) CategoryLength() (*big.Int, error) {
	return _Yetiswap0.Contract.CategoryLength(&_Yetiswap0.CallOpts)
}

// CategoryLength is a free data retrieval call binding the contract method 0xee724fcf.
//
// Solidity: function categoryLength() view returns(uint256)
func (_Yetiswap0 *Yetiswap0CallerSession) CategoryLength() (*big.Int, error) {
	return _Yetiswap0.Contract.CategoryLength(&_Yetiswap0.CallOpts)
}

// CollectionCategory is a free data retrieval call binding the contract method 0xa3676d78.
//
// Solidity: function collectionCategory(address ) view returns(uint256)
func (_Yetiswap0 *Yetiswap0Caller) CollectionCategory(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "collectionCategory", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CollectionCategory is a free data retrieval call binding the contract method 0xa3676d78.
//
// Solidity: function collectionCategory(address ) view returns(uint256)
func (_Yetiswap0 *Yetiswap0Session) CollectionCategory(arg0 common.Address) (*big.Int, error) {
	return _Yetiswap0.Contract.CollectionCategory(&_Yetiswap0.CallOpts, arg0)
}

// CollectionCategory is a free data retrieval call binding the contract method 0xa3676d78.
//
// Solidity: function collectionCategory(address ) view returns(uint256)
func (_Yetiswap0 *Yetiswap0CallerSession) CollectionCategory(arg0 common.Address) (*big.Int, error) {
	return _Yetiswap0.Contract.CollectionCategory(&_Yetiswap0.CallOpts, arg0)
}

// CommissionRateNonYTS is a free data retrieval call binding the contract method 0x88137a46.
//
// Solidity: function commissionRateNonYTS() view returns(uint256)
func (_Yetiswap0 *Yetiswap0Caller) CommissionRateNonYTS(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "commissionRateNonYTS")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CommissionRateNonYTS is a free data retrieval call binding the contract method 0x88137a46.
//
// Solidity: function commissionRateNonYTS() view returns(uint256)
func (_Yetiswap0 *Yetiswap0Session) CommissionRateNonYTS() (*big.Int, error) {
	return _Yetiswap0.Contract.CommissionRateNonYTS(&_Yetiswap0.CallOpts)
}

// CommissionRateNonYTS is a free data retrieval call binding the contract method 0x88137a46.
//
// Solidity: function commissionRateNonYTS() view returns(uint256)
func (_Yetiswap0 *Yetiswap0CallerSession) CommissionRateNonYTS() (*big.Int, error) {
	return _Yetiswap0.Contract.CommissionRateNonYTS(&_Yetiswap0.CallOpts)
}

// CommissionRateYTS is a free data retrieval call binding the contract method 0xa546a11b.
//
// Solidity: function commissionRateYTS() view returns(uint256)
func (_Yetiswap0 *Yetiswap0Caller) CommissionRateYTS(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "commissionRateYTS")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CommissionRateYTS is a free data retrieval call binding the contract method 0xa546a11b.
//
// Solidity: function commissionRateYTS() view returns(uint256)
func (_Yetiswap0 *Yetiswap0Session) CommissionRateYTS() (*big.Int, error) {
	return _Yetiswap0.Contract.CommissionRateYTS(&_Yetiswap0.CallOpts)
}

// CommissionRateYTS is a free data retrieval call binding the contract method 0xa546a11b.
//
// Solidity: function commissionRateYTS() view returns(uint256)
func (_Yetiswap0 *Yetiswap0CallerSession) CommissionRateYTS() (*big.Int, error) {
	return _Yetiswap0.Contract.CommissionRateYTS(&_Yetiswap0.CallOpts)
}

// CommissionTaker is a free data retrieval call binding the contract method 0xc557ce70.
//
// Solidity: function commissionTaker() view returns(address)
func (_Yetiswap0 *Yetiswap0Caller) CommissionTaker(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "commissionTaker")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// CommissionTaker is a free data retrieval call binding the contract method 0xc557ce70.
//
// Solidity: function commissionTaker() view returns(address)
func (_Yetiswap0 *Yetiswap0Session) CommissionTaker() (common.Address, error) {
	return _Yetiswap0.Contract.CommissionTaker(&_Yetiswap0.CallOpts)
}

// CommissionTaker is a free data retrieval call binding the contract method 0xc557ce70.
//
// Solidity: function commissionTaker() view returns(address)
func (_Yetiswap0 *Yetiswap0CallerSession) CommissionTaker() (common.Address, error) {
	return _Yetiswap0.Contract.CommissionTaker(&_Yetiswap0.CallOpts)
}

// CreatorRate is a free data retrieval call binding the contract method 0x0bc1ee13.
//
// Solidity: function creatorRate(address ) view returns(uint256)
func (_Yetiswap0 *Yetiswap0Caller) CreatorRate(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "creatorRate", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CreatorRate is a free data retrieval call binding the contract method 0x0bc1ee13.
//
// Solidity: function creatorRate(address ) view returns(uint256)
func (_Yetiswap0 *Yetiswap0Session) CreatorRate(arg0 common.Address) (*big.Int, error) {
	return _Yetiswap0.Contract.CreatorRate(&_Yetiswap0.CallOpts, arg0)
}

// CreatorRate is a free data retrieval call binding the contract method 0x0bc1ee13.
//
// Solidity: function creatorRate(address ) view returns(uint256)
func (_Yetiswap0 *Yetiswap0CallerSession) CreatorRate(arg0 common.Address) (*big.Int, error) {
	return _Yetiswap0.Contract.CreatorRate(&_Yetiswap0.CallOpts, arg0)
}

// Creators is a free data retrieval call binding the contract method 0x933166e1.
//
// Solidity: function creators(address ) view returns(address)
func (_Yetiswap0 *Yetiswap0Caller) Creators(opts *bind.CallOpts, arg0 common.Address) (common.Address, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "creators", arg0)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Creators is a free data retrieval call binding the contract method 0x933166e1.
//
// Solidity: function creators(address ) view returns(address)
func (_Yetiswap0 *Yetiswap0Session) Creators(arg0 common.Address) (common.Address, error) {
	return _Yetiswap0.Contract.Creators(&_Yetiswap0.CallOpts, arg0)
}

// Creators is a free data retrieval call binding the contract method 0x933166e1.
//
// Solidity: function creators(address ) view returns(address)
func (_Yetiswap0 *Yetiswap0CallerSession) Creators(arg0 common.Address) (common.Address, error) {
	return _Yetiswap0.Contract.Creators(&_Yetiswap0.CallOpts, arg0)
}

// GetCommissionRate is a free data retrieval call binding the contract method 0x7a413c33.
//
// Solidity: function getCommissionRate(uint256 index) view returns(uint256 CommissionRate)
func (_Yetiswap0 *Yetiswap0Caller) GetCommissionRate(opts *bind.CallOpts, index *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "getCommissionRate", index)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetCommissionRate is a free data retrieval call binding the contract method 0x7a413c33.
//
// Solidity: function getCommissionRate(uint256 index) view returns(uint256 CommissionRate)
func (_Yetiswap0 *Yetiswap0Session) GetCommissionRate(index *big.Int) (*big.Int, error) {
	return _Yetiswap0.Contract.GetCommissionRate(&_Yetiswap0.CallOpts, index)
}

// GetCommissionRate is a free data retrieval call binding the contract method 0x7a413c33.
//
// Solidity: function getCommissionRate(uint256 index) view returns(uint256 CommissionRate)
func (_Yetiswap0 *Yetiswap0CallerSession) GetCommissionRate(index *big.Int) (*big.Int, error) {
	return _Yetiswap0.Contract.GetCommissionRate(&_Yetiswap0.CallOpts, index)
}

// GetCreatorRate is a free data retrieval call binding the contract method 0x135218ae.
//
// Solidity: function getCreatorRate(uint256 index) view returns(uint256 NFTcreatorRate)
func (_Yetiswap0 *Yetiswap0Caller) GetCreatorRate(opts *bind.CallOpts, index *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "getCreatorRate", index)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetCreatorRate is a free data retrieval call binding the contract method 0x135218ae.
//
// Solidity: function getCreatorRate(uint256 index) view returns(uint256 NFTcreatorRate)
func (_Yetiswap0 *Yetiswap0Session) GetCreatorRate(index *big.Int) (*big.Int, error) {
	return _Yetiswap0.Contract.GetCreatorRate(&_Yetiswap0.CallOpts, index)
}

// GetCreatorRate is a free data retrieval call binding the contract method 0x135218ae.
//
// Solidity: function getCreatorRate(uint256 index) view returns(uint256 NFTcreatorRate)
func (_Yetiswap0 *Yetiswap0CallerSession) GetCreatorRate(index *big.Int) (*big.Int, error) {
	return _Yetiswap0.Contract.GetCreatorRate(&_Yetiswap0.CallOpts, index)
}

// GetSale is a free data retrieval call binding the contract method 0xd8f6d596.
//
// Solidity: function getSale(uint256 index) view returns(address tokenContract, uint256 tokenId, address saleToken, uint256 price, address seller, uint256 category, address creator)
func (_Yetiswap0 *Yetiswap0Caller) GetSale(opts *bind.CallOpts, index *big.Int) (struct {
	TokenContract common.Address
	TokenId       *big.Int
	SaleToken     common.Address
	Price         *big.Int
	Seller        common.Address
	Category      *big.Int
	Creator       common.Address
}, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "getSale", index)

	outstruct := new(struct {
		TokenContract common.Address
		TokenId       *big.Int
		SaleToken     common.Address
		Price         *big.Int
		Seller        common.Address
		Category      *big.Int
		Creator       common.Address
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.TokenContract = *abi.ConvertType(out[0], new(common.Address)).(*common.Address)
	outstruct.TokenId = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.SaleToken = *abi.ConvertType(out[2], new(common.Address)).(*common.Address)
	outstruct.Price = *abi.ConvertType(out[3], new(*big.Int)).(**big.Int)
	outstruct.Seller = *abi.ConvertType(out[4], new(common.Address)).(*common.Address)
	outstruct.Category = *abi.ConvertType(out[5], new(*big.Int)).(**big.Int)
	outstruct.Creator = *abi.ConvertType(out[6], new(common.Address)).(*common.Address)

	return *outstruct, err

}

// GetSale is a free data retrieval call binding the contract method 0xd8f6d596.
//
// Solidity: function getSale(uint256 index) view returns(address tokenContract, uint256 tokenId, address saleToken, uint256 price, address seller, uint256 category, address creator)
func (_Yetiswap0 *Yetiswap0Session) GetSale(index *big.Int) (struct {
	TokenContract common.Address
	TokenId       *big.Int
	SaleToken     common.Address
	Price         *big.Int
	Seller        common.Address
	Category      *big.Int
	Creator       common.Address
}, error) {
	return _Yetiswap0.Contract.GetSale(&_Yetiswap0.CallOpts, index)
}

// GetSale is a free data retrieval call binding the contract method 0xd8f6d596.
//
// Solidity: function getSale(uint256 index) view returns(address tokenContract, uint256 tokenId, address saleToken, uint256 price, address seller, uint256 category, address creator)
func (_Yetiswap0 *Yetiswap0CallerSession) GetSale(index *big.Int) (struct {
	TokenContract common.Address
	TokenId       *big.Int
	SaleToken     common.Address
	Price         *big.Int
	Seller        common.Address
	Category      *big.Int
	Creator       common.Address
}, error) {
	return _Yetiswap0.Contract.GetSale(&_Yetiswap0.CallOpts, index)
}

// GetWhitelistedCollection is a free data retrieval call binding the contract method 0xc5dc2441.
//
// Solidity: function getWhitelistedCollection(uint256 index) view returns(address)
func (_Yetiswap0 *Yetiswap0Caller) GetWhitelistedCollection(opts *bind.CallOpts, index *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "getWhitelistedCollection", index)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetWhitelistedCollection is a free data retrieval call binding the contract method 0xc5dc2441.
//
// Solidity: function getWhitelistedCollection(uint256 index) view returns(address)
func (_Yetiswap0 *Yetiswap0Session) GetWhitelistedCollection(index *big.Int) (common.Address, error) {
	return _Yetiswap0.Contract.GetWhitelistedCollection(&_Yetiswap0.CallOpts, index)
}

// GetWhitelistedCollection is a free data retrieval call binding the contract method 0xc5dc2441.
//
// Solidity: function getWhitelistedCollection(uint256 index) view returns(address)
func (_Yetiswap0 *Yetiswap0CallerSession) GetWhitelistedCollection(index *big.Int) (common.Address, error) {
	return _Yetiswap0.Contract.GetWhitelistedCollection(&_Yetiswap0.CallOpts, index)
}

// GetWhitelistedToken is a free data retrieval call binding the contract method 0x9390649e.
//
// Solidity: function getWhitelistedToken(uint256 index) view returns(address)
func (_Yetiswap0 *Yetiswap0Caller) GetWhitelistedToken(opts *bind.CallOpts, index *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "getWhitelistedToken", index)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetWhitelistedToken is a free data retrieval call binding the contract method 0x9390649e.
//
// Solidity: function getWhitelistedToken(uint256 index) view returns(address)
func (_Yetiswap0 *Yetiswap0Session) GetWhitelistedToken(index *big.Int) (common.Address, error) {
	return _Yetiswap0.Contract.GetWhitelistedToken(&_Yetiswap0.CallOpts, index)
}

// GetWhitelistedToken is a free data retrieval call binding the contract method 0x9390649e.
//
// Solidity: function getWhitelistedToken(uint256 index) view returns(address)
func (_Yetiswap0 *Yetiswap0CallerSession) GetWhitelistedToken(index *big.Int) (common.Address, error) {
	return _Yetiswap0.Contract.GetWhitelistedToken(&_Yetiswap0.CallOpts, index)
}

// IsWhitelistedCollection is a free data retrieval call binding the contract method 0x8214ff3e.
//
// Solidity: function isWhitelistedCollection(address stakeContract) view returns(bool)
func (_Yetiswap0 *Yetiswap0Caller) IsWhitelistedCollection(opts *bind.CallOpts, stakeContract common.Address) (bool, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "isWhitelistedCollection", stakeContract)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsWhitelistedCollection is a free data retrieval call binding the contract method 0x8214ff3e.
//
// Solidity: function isWhitelistedCollection(address stakeContract) view returns(bool)
func (_Yetiswap0 *Yetiswap0Session) IsWhitelistedCollection(stakeContract common.Address) (bool, error) {
	return _Yetiswap0.Contract.IsWhitelistedCollection(&_Yetiswap0.CallOpts, stakeContract)
}

// IsWhitelistedCollection is a free data retrieval call binding the contract method 0x8214ff3e.
//
// Solidity: function isWhitelistedCollection(address stakeContract) view returns(bool)
func (_Yetiswap0 *Yetiswap0CallerSession) IsWhitelistedCollection(stakeContract common.Address) (bool, error) {
	return _Yetiswap0.Contract.IsWhitelistedCollection(&_Yetiswap0.CallOpts, stakeContract)
}

// IsWhitelistedToken is a free data retrieval call binding the contract method 0xab37f486.
//
// Solidity: function isWhitelistedToken(address tokenContract) view returns(bool)
func (_Yetiswap0 *Yetiswap0Caller) IsWhitelistedToken(opts *bind.CallOpts, tokenContract common.Address) (bool, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "isWhitelistedToken", tokenContract)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsWhitelistedToken is a free data retrieval call binding the contract method 0xab37f486.
//
// Solidity: function isWhitelistedToken(address tokenContract) view returns(bool)
func (_Yetiswap0 *Yetiswap0Session) IsWhitelistedToken(tokenContract common.Address) (bool, error) {
	return _Yetiswap0.Contract.IsWhitelistedToken(&_Yetiswap0.CallOpts, tokenContract)
}

// IsWhitelistedToken is a free data retrieval call binding the contract method 0xab37f486.
//
// Solidity: function isWhitelistedToken(address tokenContract) view returns(bool)
func (_Yetiswap0 *Yetiswap0CallerSession) IsWhitelistedToken(tokenContract common.Address) (bool, error) {
	return _Yetiswap0.Contract.IsWhitelistedToken(&_Yetiswap0.CallOpts, tokenContract)
}

// NumCollections is a free data retrieval call binding the contract method 0xe7e6d4a1.
//
// Solidity: function numCollections() view returns(uint256)
func (_Yetiswap0 *Yetiswap0Caller) NumCollections(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "numCollections")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// NumCollections is a free data retrieval call binding the contract method 0xe7e6d4a1.
//
// Solidity: function numCollections() view returns(uint256)
func (_Yetiswap0 *Yetiswap0Session) NumCollections() (*big.Int, error) {
	return _Yetiswap0.Contract.NumCollections(&_Yetiswap0.CallOpts)
}

// NumCollections is a free data retrieval call binding the contract method 0xe7e6d4a1.
//
// Solidity: function numCollections() view returns(uint256)
func (_Yetiswap0 *Yetiswap0CallerSession) NumCollections() (*big.Int, error) {
	return _Yetiswap0.Contract.NumCollections(&_Yetiswap0.CallOpts)
}

// OnSaleNftAmount is a free data retrieval call binding the contract method 0xb28d259d.
//
// Solidity: function onSaleNftAmount() view returns(uint256)
func (_Yetiswap0 *Yetiswap0Caller) OnSaleNftAmount(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "onSaleNftAmount")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// OnSaleNftAmount is a free data retrieval call binding the contract method 0xb28d259d.
//
// Solidity: function onSaleNftAmount() view returns(uint256)
func (_Yetiswap0 *Yetiswap0Session) OnSaleNftAmount() (*big.Int, error) {
	return _Yetiswap0.Contract.OnSaleNftAmount(&_Yetiswap0.CallOpts)
}

// OnSaleNftAmount is a free data retrieval call binding the contract method 0xb28d259d.
//
// Solidity: function onSaleNftAmount() view returns(uint256)
func (_Yetiswap0 *Yetiswap0CallerSession) OnSaleNftAmount() (*big.Int, error) {
	return _Yetiswap0.Contract.OnSaleNftAmount(&_Yetiswap0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Yetiswap0 *Yetiswap0Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Yetiswap0 *Yetiswap0Session) Owner() (common.Address, error) {
	return _Yetiswap0.Contract.Owner(&_Yetiswap0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Yetiswap0 *Yetiswap0CallerSession) Owner() (common.Address, error) {
	return _Yetiswap0.Contract.Owner(&_Yetiswap0.CallOpts)
}

// YetiswapNFT is a free data retrieval call binding the contract method 0x54b209a7.
//
// Solidity: function yetiswapNFT() view returns(address)
func (_Yetiswap0 *Yetiswap0Caller) YetiswapNFT(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Yetiswap0.contract.Call(opts, &out, "yetiswapNFT")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// YetiswapNFT is a free data retrieval call binding the contract method 0x54b209a7.
//
// Solidity: function yetiswapNFT() view returns(address)
func (_Yetiswap0 *Yetiswap0Session) YetiswapNFT() (common.Address, error) {
	return _Yetiswap0.Contract.YetiswapNFT(&_Yetiswap0.CallOpts)
}

// YetiswapNFT is a free data retrieval call binding the contract method 0x54b209a7.
//
// Solidity: function yetiswapNFT() view returns(address)
func (_Yetiswap0 *Yetiswap0CallerSession) YetiswapNFT() (common.Address, error) {
	return _Yetiswap0.Contract.YetiswapNFT(&_Yetiswap0.CallOpts)
}

// AddNewCategory is a paid mutator transaction binding the contract method 0x84a9df77.
//
// Solidity: function addNewCategory(string _categoryName) returns()
func (_Yetiswap0 *Yetiswap0Transactor) AddNewCategory(opts *bind.TransactOpts, _categoryName string) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "addNewCategory", _categoryName)
}

// AddNewCategory is a paid mutator transaction binding the contract method 0x84a9df77.
//
// Solidity: function addNewCategory(string _categoryName) returns()
func (_Yetiswap0 *Yetiswap0Session) AddNewCategory(_categoryName string) (*types.Transaction, error) {
	return _Yetiswap0.Contract.AddNewCategory(&_Yetiswap0.TransactOpts, _categoryName)
}

// AddNewCategory is a paid mutator transaction binding the contract method 0x84a9df77.
//
// Solidity: function addNewCategory(string _categoryName) returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) AddNewCategory(_categoryName string) (*types.Transaction, error) {
	return _Yetiswap0.Contract.AddNewCategory(&_Yetiswap0.TransactOpts, _categoryName)
}

// AddWhitelistedCollection is a paid mutator transaction binding the contract method 0x5c8440ed.
//
// Solidity: function addWhitelistedCollection(address collectionContract, address creatorAddress, uint256 _rewardRate, uint256 _category) returns()
func (_Yetiswap0 *Yetiswap0Transactor) AddWhitelistedCollection(opts *bind.TransactOpts, collectionContract common.Address, creatorAddress common.Address, _rewardRate *big.Int, _category *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "addWhitelistedCollection", collectionContract, creatorAddress, _rewardRate, _category)
}

// AddWhitelistedCollection is a paid mutator transaction binding the contract method 0x5c8440ed.
//
// Solidity: function addWhitelistedCollection(address collectionContract, address creatorAddress, uint256 _rewardRate, uint256 _category) returns()
func (_Yetiswap0 *Yetiswap0Session) AddWhitelistedCollection(collectionContract common.Address, creatorAddress common.Address, _rewardRate *big.Int, _category *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.AddWhitelistedCollection(&_Yetiswap0.TransactOpts, collectionContract, creatorAddress, _rewardRate, _category)
}

// AddWhitelistedCollection is a paid mutator transaction binding the contract method 0x5c8440ed.
//
// Solidity: function addWhitelistedCollection(address collectionContract, address creatorAddress, uint256 _rewardRate, uint256 _category) returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) AddWhitelistedCollection(collectionContract common.Address, creatorAddress common.Address, _rewardRate *big.Int, _category *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.AddWhitelistedCollection(&_Yetiswap0.TransactOpts, collectionContract, creatorAddress, _rewardRate, _category)
}

// AddWhitelistedToken is a paid mutator transaction binding the contract method 0x363cb34d.
//
// Solidity: function addWhitelistedToken(address tokenContract) returns()
func (_Yetiswap0 *Yetiswap0Transactor) AddWhitelistedToken(opts *bind.TransactOpts, tokenContract common.Address) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "addWhitelistedToken", tokenContract)
}

// AddWhitelistedToken is a paid mutator transaction binding the contract method 0x363cb34d.
//
// Solidity: function addWhitelistedToken(address tokenContract) returns()
func (_Yetiswap0 *Yetiswap0Session) AddWhitelistedToken(tokenContract common.Address) (*types.Transaction, error) {
	return _Yetiswap0.Contract.AddWhitelistedToken(&_Yetiswap0.TransactOpts, tokenContract)
}

// AddWhitelistedToken is a paid mutator transaction binding the contract method 0x363cb34d.
//
// Solidity: function addWhitelistedToken(address tokenContract) returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) AddWhitelistedToken(tokenContract common.Address) (*types.Transaction, error) {
	return _Yetiswap0.Contract.AddWhitelistedToken(&_Yetiswap0.TransactOpts, tokenContract)
}

// BuyNFT is a paid mutator transaction binding the contract method 0x51ed8288.
//
// Solidity: function buyNFT(uint256 index) returns()
func (_Yetiswap0 *Yetiswap0Transactor) BuyNFT(opts *bind.TransactOpts, index *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "buyNFT", index)
}

// BuyNFT is a paid mutator transaction binding the contract method 0x51ed8288.
//
// Solidity: function buyNFT(uint256 index) returns()
func (_Yetiswap0 *Yetiswap0Session) BuyNFT(index *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.BuyNFT(&_Yetiswap0.TransactOpts, index)
}

// BuyNFT is a paid mutator transaction binding the contract method 0x51ed8288.
//
// Solidity: function buyNFT(uint256 index) returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) BuyNFT(index *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.BuyNFT(&_Yetiswap0.TransactOpts, index)
}

// BuyNFTwithAvax is a paid mutator transaction binding the contract method 0x91ae9a9a.
//
// Solidity: function buyNFTwithAvax(uint256 index) payable returns()
func (_Yetiswap0 *Yetiswap0Transactor) BuyNFTwithAvax(opts *bind.TransactOpts, index *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "buyNFTwithAvax", index)
}

// BuyNFTwithAvax is a paid mutator transaction binding the contract method 0x91ae9a9a.
//
// Solidity: function buyNFTwithAvax(uint256 index) payable returns()
func (_Yetiswap0 *Yetiswap0Session) BuyNFTwithAvax(index *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.BuyNFTwithAvax(&_Yetiswap0.TransactOpts, index)
}

// BuyNFTwithAvax is a paid mutator transaction binding the contract method 0x91ae9a9a.
//
// Solidity: function buyNFTwithAvax(uint256 index) payable returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) BuyNFTwithAvax(index *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.BuyNFTwithAvax(&_Yetiswap0.TransactOpts, index)
}

// RemoveNFT is a paid mutator transaction binding the contract method 0xee4739cb.
//
// Solidity: function removeNFT(uint256 index) returns()
func (_Yetiswap0 *Yetiswap0Transactor) RemoveNFT(opts *bind.TransactOpts, index *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "removeNFT", index)
}

// RemoveNFT is a paid mutator transaction binding the contract method 0xee4739cb.
//
// Solidity: function removeNFT(uint256 index) returns()
func (_Yetiswap0 *Yetiswap0Session) RemoveNFT(index *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.RemoveNFT(&_Yetiswap0.TransactOpts, index)
}

// RemoveNFT is a paid mutator transaction binding the contract method 0xee4739cb.
//
// Solidity: function removeNFT(uint256 index) returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) RemoveNFT(index *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.RemoveNFT(&_Yetiswap0.TransactOpts, index)
}

// RemoveWhitelistedCollection is a paid mutator transaction binding the contract method 0x62553e14.
//
// Solidity: function removeWhitelistedCollection(address collectionContract) returns()
func (_Yetiswap0 *Yetiswap0Transactor) RemoveWhitelistedCollection(opts *bind.TransactOpts, collectionContract common.Address) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "removeWhitelistedCollection", collectionContract)
}

// RemoveWhitelistedCollection is a paid mutator transaction binding the contract method 0x62553e14.
//
// Solidity: function removeWhitelistedCollection(address collectionContract) returns()
func (_Yetiswap0 *Yetiswap0Session) RemoveWhitelistedCollection(collectionContract common.Address) (*types.Transaction, error) {
	return _Yetiswap0.Contract.RemoveWhitelistedCollection(&_Yetiswap0.TransactOpts, collectionContract)
}

// RemoveWhitelistedCollection is a paid mutator transaction binding the contract method 0x62553e14.
//
// Solidity: function removeWhitelistedCollection(address collectionContract) returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) RemoveWhitelistedCollection(collectionContract common.Address) (*types.Transaction, error) {
	return _Yetiswap0.Contract.RemoveWhitelistedCollection(&_Yetiswap0.TransactOpts, collectionContract)
}

// RemoveWhitelistedToken is a paid mutator transaction binding the contract method 0x1c88705d.
//
// Solidity: function removeWhitelistedToken(address tokenContract) returns()
func (_Yetiswap0 *Yetiswap0Transactor) RemoveWhitelistedToken(opts *bind.TransactOpts, tokenContract common.Address) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "removeWhitelistedToken", tokenContract)
}

// RemoveWhitelistedToken is a paid mutator transaction binding the contract method 0x1c88705d.
//
// Solidity: function removeWhitelistedToken(address tokenContract) returns()
func (_Yetiswap0 *Yetiswap0Session) RemoveWhitelistedToken(tokenContract common.Address) (*types.Transaction, error) {
	return _Yetiswap0.Contract.RemoveWhitelistedToken(&_Yetiswap0.TransactOpts, tokenContract)
}

// RemoveWhitelistedToken is a paid mutator transaction binding the contract method 0x1c88705d.
//
// Solidity: function removeWhitelistedToken(address tokenContract) returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) RemoveWhitelistedToken(tokenContract common.Address) (*types.Transaction, error) {
	return _Yetiswap0.Contract.RemoveWhitelistedToken(&_Yetiswap0.TransactOpts, tokenContract)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Yetiswap0 *Yetiswap0Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Yetiswap0 *Yetiswap0Session) RenounceOwnership() (*types.Transaction, error) {
	return _Yetiswap0.Contract.RenounceOwnership(&_Yetiswap0.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Yetiswap0.Contract.RenounceOwnership(&_Yetiswap0.TransactOpts)
}

// SaleNFT is a paid mutator transaction binding the contract method 0x31ef8e57.
//
// Solidity: function saleNFT(address _tokenContract, uint256 _tokenId, address _saleToken, uint256 _price, uint256 _category) returns()
func (_Yetiswap0 *Yetiswap0Transactor) SaleNFT(opts *bind.TransactOpts, _tokenContract common.Address, _tokenId *big.Int, _saleToken common.Address, _price *big.Int, _category *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "saleNFT", _tokenContract, _tokenId, _saleToken, _price, _category)
}

// SaleNFT is a paid mutator transaction binding the contract method 0x31ef8e57.
//
// Solidity: function saleNFT(address _tokenContract, uint256 _tokenId, address _saleToken, uint256 _price, uint256 _category) returns()
func (_Yetiswap0 *Yetiswap0Session) SaleNFT(_tokenContract common.Address, _tokenId *big.Int, _saleToken common.Address, _price *big.Int, _category *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.SaleNFT(&_Yetiswap0.TransactOpts, _tokenContract, _tokenId, _saleToken, _price, _category)
}

// SaleNFT is a paid mutator transaction binding the contract method 0x31ef8e57.
//
// Solidity: function saleNFT(address _tokenContract, uint256 _tokenId, address _saleToken, uint256 _price, uint256 _category) returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) SaleNFT(_tokenContract common.Address, _tokenId *big.Int, _saleToken common.Address, _price *big.Int, _category *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.SaleNFT(&_Yetiswap0.TransactOpts, _tokenContract, _tokenId, _saleToken, _price, _category)
}

// SetCommissionRate is a paid mutator transaction binding the contract method 0x19fac8fd.
//
// Solidity: function setCommissionRate(uint256 _commissionRateYTS) returns()
func (_Yetiswap0 *Yetiswap0Transactor) SetCommissionRate(opts *bind.TransactOpts, _commissionRateYTS *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "setCommissionRate", _commissionRateYTS)
}

// SetCommissionRate is a paid mutator transaction binding the contract method 0x19fac8fd.
//
// Solidity: function setCommissionRate(uint256 _commissionRateYTS) returns()
func (_Yetiswap0 *Yetiswap0Session) SetCommissionRate(_commissionRateYTS *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.SetCommissionRate(&_Yetiswap0.TransactOpts, _commissionRateYTS)
}

// SetCommissionRate is a paid mutator transaction binding the contract method 0x19fac8fd.
//
// Solidity: function setCommissionRate(uint256 _commissionRateYTS) returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) SetCommissionRate(_commissionRateYTS *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.SetCommissionRate(&_Yetiswap0.TransactOpts, _commissionRateYTS)
}

// SetCommissionRateAvax is a paid mutator transaction binding the contract method 0xab482cea.
//
// Solidity: function setCommissionRateAvax(uint256 _commissionRateNonYTS) returns()
func (_Yetiswap0 *Yetiswap0Transactor) SetCommissionRateAvax(opts *bind.TransactOpts, _commissionRateNonYTS *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "setCommissionRateAvax", _commissionRateNonYTS)
}

// SetCommissionRateAvax is a paid mutator transaction binding the contract method 0xab482cea.
//
// Solidity: function setCommissionRateAvax(uint256 _commissionRateNonYTS) returns()
func (_Yetiswap0 *Yetiswap0Session) SetCommissionRateAvax(_commissionRateNonYTS *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.SetCommissionRateAvax(&_Yetiswap0.TransactOpts, _commissionRateNonYTS)
}

// SetCommissionRateAvax is a paid mutator transaction binding the contract method 0xab482cea.
//
// Solidity: function setCommissionRateAvax(uint256 _commissionRateNonYTS) returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) SetCommissionRateAvax(_commissionRateNonYTS *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.SetCommissionRateAvax(&_Yetiswap0.TransactOpts, _commissionRateNonYTS)
}

// SetCommissionTaker is a paid mutator transaction binding the contract method 0x9eab6ee9.
//
// Solidity: function setCommissionTaker(address _commissionTaker) returns()
func (_Yetiswap0 *Yetiswap0Transactor) SetCommissionTaker(opts *bind.TransactOpts, _commissionTaker common.Address) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "setCommissionTaker", _commissionTaker)
}

// SetCommissionTaker is a paid mutator transaction binding the contract method 0x9eab6ee9.
//
// Solidity: function setCommissionTaker(address _commissionTaker) returns()
func (_Yetiswap0 *Yetiswap0Session) SetCommissionTaker(_commissionTaker common.Address) (*types.Transaction, error) {
	return _Yetiswap0.Contract.SetCommissionTaker(&_Yetiswap0.TransactOpts, _commissionTaker)
}

// SetCommissionTaker is a paid mutator transaction binding the contract method 0x9eab6ee9.
//
// Solidity: function setCommissionTaker(address _commissionTaker) returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) SetCommissionTaker(_commissionTaker common.Address) (*types.Transaction, error) {
	return _Yetiswap0.Contract.SetCommissionTaker(&_Yetiswap0.TransactOpts, _commissionTaker)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Yetiswap0 *Yetiswap0Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Yetiswap0 *Yetiswap0Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Yetiswap0.Contract.TransferOwnership(&_Yetiswap0.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Yetiswap0.Contract.TransferOwnership(&_Yetiswap0.TransactOpts, newOwner)
}

// UpdateCreatorRate is a paid mutator transaction binding the contract method 0xe03adac2.
//
// Solidity: function updateCreatorRate(address collectionContract, uint256 _rewardRate) returns()
func (_Yetiswap0 *Yetiswap0Transactor) UpdateCreatorRate(opts *bind.TransactOpts, collectionContract common.Address, _rewardRate *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.contract.Transact(opts, "updateCreatorRate", collectionContract, _rewardRate)
}

// UpdateCreatorRate is a paid mutator transaction binding the contract method 0xe03adac2.
//
// Solidity: function updateCreatorRate(address collectionContract, uint256 _rewardRate) returns()
func (_Yetiswap0 *Yetiswap0Session) UpdateCreatorRate(collectionContract common.Address, _rewardRate *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.UpdateCreatorRate(&_Yetiswap0.TransactOpts, collectionContract, _rewardRate)
}

// UpdateCreatorRate is a paid mutator transaction binding the contract method 0xe03adac2.
//
// Solidity: function updateCreatorRate(address collectionContract, uint256 _rewardRate) returns()
func (_Yetiswap0 *Yetiswap0TransactorSession) UpdateCreatorRate(collectionContract common.Address, _rewardRate *big.Int) (*types.Transaction, error) {
	return _Yetiswap0.Contract.UpdateCreatorRate(&_Yetiswap0.TransactOpts, collectionContract, _rewardRate)
}

// Yetiswap0CollectionRemovedIterator is returned from FilterCollectionRemoved and is used to iterate over the raw logs and unpacked data for CollectionRemoved events raised by the Yetiswap0 contract.
type Yetiswap0CollectionRemovedIterator struct {
	Event *Yetiswap0CollectionRemoved // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Yetiswap0CollectionRemovedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Yetiswap0CollectionRemoved)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Yetiswap0CollectionRemoved)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Yetiswap0CollectionRemovedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Yetiswap0CollectionRemovedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Yetiswap0CollectionRemoved represents a CollectionRemoved event raised by the Yetiswap0 contract.
type Yetiswap0CollectionRemoved struct {
	CollectionContract common.Address
	Raw                types.Log // Blockchain specific contextual infos
}

// FilterCollectionRemoved is a free log retrieval operation binding the contract event 0xa0691bd707b2f65c33c8343d61c274df72c6b5007937dcfbc31aa5a0d0f6fe3c.
//
// Solidity: event CollectionRemoved(address collectionContract)
func (_Yetiswap0 *Yetiswap0Filterer) FilterCollectionRemoved(opts *bind.FilterOpts) (*Yetiswap0CollectionRemovedIterator, error) {

	logs, sub, err := _Yetiswap0.contract.FilterLogs(opts, "CollectionRemoved")
	if err != nil {
		return nil, err
	}
	return &Yetiswap0CollectionRemovedIterator{contract: _Yetiswap0.contract, event: "CollectionRemoved", logs: logs, sub: sub}, nil
}

// WatchCollectionRemoved is a free log subscription operation binding the contract event 0xa0691bd707b2f65c33c8343d61c274df72c6b5007937dcfbc31aa5a0d0f6fe3c.
//
// Solidity: event CollectionRemoved(address collectionContract)
func (_Yetiswap0 *Yetiswap0Filterer) WatchCollectionRemoved(opts *bind.WatchOpts, sink chan<- *Yetiswap0CollectionRemoved) (event.Subscription, error) {

	logs, sub, err := _Yetiswap0.contract.WatchLogs(opts, "CollectionRemoved")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Yetiswap0CollectionRemoved)
				if err := _Yetiswap0.contract.UnpackLog(event, "CollectionRemoved", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCollectionRemoved is a log parse operation binding the contract event 0xa0691bd707b2f65c33c8343d61c274df72c6b5007937dcfbc31aa5a0d0f6fe3c.
//
// Solidity: event CollectionRemoved(address collectionContract)
func (_Yetiswap0 *Yetiswap0Filterer) ParseCollectionRemoved(log types.Log) (*Yetiswap0CollectionRemoved, error) {
	event := new(Yetiswap0CollectionRemoved)
	if err := _Yetiswap0.contract.UnpackLog(event, "CollectionRemoved", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Yetiswap0CommissionChangedForNonYTSIterator is returned from FilterCommissionChangedForNonYTS and is used to iterate over the raw logs and unpacked data for CommissionChangedForNonYTS events raised by the Yetiswap0 contract.
type Yetiswap0CommissionChangedForNonYTSIterator struct {
	Event *Yetiswap0CommissionChangedForNonYTS // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Yetiswap0CommissionChangedForNonYTSIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Yetiswap0CommissionChangedForNonYTS)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Yetiswap0CommissionChangedForNonYTS)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Yetiswap0CommissionChangedForNonYTSIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Yetiswap0CommissionChangedForNonYTSIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Yetiswap0CommissionChangedForNonYTS represents a CommissionChangedForNonYTS event raised by the Yetiswap0 contract.
type Yetiswap0CommissionChangedForNonYTS struct {
	CommissionRateNonYTS *big.Int
	Raw                  types.Log // Blockchain specific contextual infos
}

// FilterCommissionChangedForNonYTS is a free log retrieval operation binding the contract event 0x2b2a5bdee68c2920f46242409d4458b6aeccc82b303c2bbbe40c0a2ce9305626.
//
// Solidity: event CommissionChangedForNonYTS(uint256 _commissionRateNonYTS)
func (_Yetiswap0 *Yetiswap0Filterer) FilterCommissionChangedForNonYTS(opts *bind.FilterOpts) (*Yetiswap0CommissionChangedForNonYTSIterator, error) {

	logs, sub, err := _Yetiswap0.contract.FilterLogs(opts, "CommissionChangedForNonYTS")
	if err != nil {
		return nil, err
	}
	return &Yetiswap0CommissionChangedForNonYTSIterator{contract: _Yetiswap0.contract, event: "CommissionChangedForNonYTS", logs: logs, sub: sub}, nil
}

// WatchCommissionChangedForNonYTS is a free log subscription operation binding the contract event 0x2b2a5bdee68c2920f46242409d4458b6aeccc82b303c2bbbe40c0a2ce9305626.
//
// Solidity: event CommissionChangedForNonYTS(uint256 _commissionRateNonYTS)
func (_Yetiswap0 *Yetiswap0Filterer) WatchCommissionChangedForNonYTS(opts *bind.WatchOpts, sink chan<- *Yetiswap0CommissionChangedForNonYTS) (event.Subscription, error) {

	logs, sub, err := _Yetiswap0.contract.WatchLogs(opts, "CommissionChangedForNonYTS")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Yetiswap0CommissionChangedForNonYTS)
				if err := _Yetiswap0.contract.UnpackLog(event, "CommissionChangedForNonYTS", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCommissionChangedForNonYTS is a log parse operation binding the contract event 0x2b2a5bdee68c2920f46242409d4458b6aeccc82b303c2bbbe40c0a2ce9305626.
//
// Solidity: event CommissionChangedForNonYTS(uint256 _commissionRateNonYTS)
func (_Yetiswap0 *Yetiswap0Filterer) ParseCommissionChangedForNonYTS(log types.Log) (*Yetiswap0CommissionChangedForNonYTS, error) {
	event := new(Yetiswap0CommissionChangedForNonYTS)
	if err := _Yetiswap0.contract.UnpackLog(event, "CommissionChangedForNonYTS", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Yetiswap0CommissionChangedForYTSIterator is returned from FilterCommissionChangedForYTS and is used to iterate over the raw logs and unpacked data for CommissionChangedForYTS events raised by the Yetiswap0 contract.
type Yetiswap0CommissionChangedForYTSIterator struct {
	Event *Yetiswap0CommissionChangedForYTS // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Yetiswap0CommissionChangedForYTSIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Yetiswap0CommissionChangedForYTS)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Yetiswap0CommissionChangedForYTS)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Yetiswap0CommissionChangedForYTSIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Yetiswap0CommissionChangedForYTSIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Yetiswap0CommissionChangedForYTS represents a CommissionChangedForYTS event raised by the Yetiswap0 contract.
type Yetiswap0CommissionChangedForYTS struct {
	CommissionRateYTS *big.Int
	Raw               types.Log // Blockchain specific contextual infos
}

// FilterCommissionChangedForYTS is a free log retrieval operation binding the contract event 0xfd09a513b7c6c98e35b6bb93fc92b139f5a9aeb189610d40e0fd61d58fd5cbcc.
//
// Solidity: event CommissionChangedForYTS(uint256 _commissionRateYTS)
func (_Yetiswap0 *Yetiswap0Filterer) FilterCommissionChangedForYTS(opts *bind.FilterOpts) (*Yetiswap0CommissionChangedForYTSIterator, error) {

	logs, sub, err := _Yetiswap0.contract.FilterLogs(opts, "CommissionChangedForYTS")
	if err != nil {
		return nil, err
	}
	return &Yetiswap0CommissionChangedForYTSIterator{contract: _Yetiswap0.contract, event: "CommissionChangedForYTS", logs: logs, sub: sub}, nil
}

// WatchCommissionChangedForYTS is a free log subscription operation binding the contract event 0xfd09a513b7c6c98e35b6bb93fc92b139f5a9aeb189610d40e0fd61d58fd5cbcc.
//
// Solidity: event CommissionChangedForYTS(uint256 _commissionRateYTS)
func (_Yetiswap0 *Yetiswap0Filterer) WatchCommissionChangedForYTS(opts *bind.WatchOpts, sink chan<- *Yetiswap0CommissionChangedForYTS) (event.Subscription, error) {

	logs, sub, err := _Yetiswap0.contract.WatchLogs(opts, "CommissionChangedForYTS")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Yetiswap0CommissionChangedForYTS)
				if err := _Yetiswap0.contract.UnpackLog(event, "CommissionChangedForYTS", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCommissionChangedForYTS is a log parse operation binding the contract event 0xfd09a513b7c6c98e35b6bb93fc92b139f5a9aeb189610d40e0fd61d58fd5cbcc.
//
// Solidity: event CommissionChangedForYTS(uint256 _commissionRateYTS)
func (_Yetiswap0 *Yetiswap0Filterer) ParseCommissionChangedForYTS(log types.Log) (*Yetiswap0CommissionChangedForYTS, error) {
	event := new(Yetiswap0CommissionChangedForYTS)
	if err := _Yetiswap0.contract.UnpackLog(event, "CommissionChangedForYTS", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Yetiswap0CommissionTakerChangedIterator is returned from FilterCommissionTakerChanged and is used to iterate over the raw logs and unpacked data for CommissionTakerChanged events raised by the Yetiswap0 contract.
type Yetiswap0CommissionTakerChangedIterator struct {
	Event *Yetiswap0CommissionTakerChanged // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Yetiswap0CommissionTakerChangedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Yetiswap0CommissionTakerChanged)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Yetiswap0CommissionTakerChanged)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Yetiswap0CommissionTakerChangedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Yetiswap0CommissionTakerChangedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Yetiswap0CommissionTakerChanged represents a CommissionTakerChanged event raised by the Yetiswap0 contract.
type Yetiswap0CommissionTakerChanged struct {
	CommissionTaker common.Address
	Raw             types.Log // Blockchain specific contextual infos
}

// FilterCommissionTakerChanged is a free log retrieval operation binding the contract event 0x380e1b1ef6644b91541604dce648aa3bcfd56a46d69cf4ffca30a72e68931bf0.
//
// Solidity: event CommissionTakerChanged(address _commissionTaker)
func (_Yetiswap0 *Yetiswap0Filterer) FilterCommissionTakerChanged(opts *bind.FilterOpts) (*Yetiswap0CommissionTakerChangedIterator, error) {

	logs, sub, err := _Yetiswap0.contract.FilterLogs(opts, "CommissionTakerChanged")
	if err != nil {
		return nil, err
	}
	return &Yetiswap0CommissionTakerChangedIterator{contract: _Yetiswap0.contract, event: "CommissionTakerChanged", logs: logs, sub: sub}, nil
}

// WatchCommissionTakerChanged is a free log subscription operation binding the contract event 0x380e1b1ef6644b91541604dce648aa3bcfd56a46d69cf4ffca30a72e68931bf0.
//
// Solidity: event CommissionTakerChanged(address _commissionTaker)
func (_Yetiswap0 *Yetiswap0Filterer) WatchCommissionTakerChanged(opts *bind.WatchOpts, sink chan<- *Yetiswap0CommissionTakerChanged) (event.Subscription, error) {

	logs, sub, err := _Yetiswap0.contract.WatchLogs(opts, "CommissionTakerChanged")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Yetiswap0CommissionTakerChanged)
				if err := _Yetiswap0.contract.UnpackLog(event, "CommissionTakerChanged", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCommissionTakerChanged is a log parse operation binding the contract event 0x380e1b1ef6644b91541604dce648aa3bcfd56a46d69cf4ffca30a72e68931bf0.
//
// Solidity: event CommissionTakerChanged(address _commissionTaker)
func (_Yetiswap0 *Yetiswap0Filterer) ParseCommissionTakerChanged(log types.Log) (*Yetiswap0CommissionTakerChanged, error) {
	event := new(Yetiswap0CommissionTakerChanged)
	if err := _Yetiswap0.contract.UnpackLog(event, "CommissionTakerChanged", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Yetiswap0CreatorRateChangedIterator is returned from FilterCreatorRateChanged and is used to iterate over the raw logs and unpacked data for CreatorRateChanged events raised by the Yetiswap0 contract.
type Yetiswap0CreatorRateChangedIterator struct {
	Event *Yetiswap0CreatorRateChanged // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Yetiswap0CreatorRateChangedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Yetiswap0CreatorRateChanged)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Yetiswap0CreatorRateChanged)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Yetiswap0CreatorRateChangedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Yetiswap0CreatorRateChangedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Yetiswap0CreatorRateChanged represents a CreatorRateChanged event raised by the Yetiswap0 contract.
type Yetiswap0CreatorRateChanged struct {
	CollectionContract common.Address
	RewardRate         *big.Int
	Raw                types.Log // Blockchain specific contextual infos
}

// FilterCreatorRateChanged is a free log retrieval operation binding the contract event 0xa7423cdbc9422ccce0ec73e11248556c62cac353aadfb8e0ba544172399eace7.
//
// Solidity: event CreatorRateChanged(address collectionContract, uint256 _rewardRate)
func (_Yetiswap0 *Yetiswap0Filterer) FilterCreatorRateChanged(opts *bind.FilterOpts) (*Yetiswap0CreatorRateChangedIterator, error) {

	logs, sub, err := _Yetiswap0.contract.FilterLogs(opts, "CreatorRateChanged")
	if err != nil {
		return nil, err
	}
	return &Yetiswap0CreatorRateChangedIterator{contract: _Yetiswap0.contract, event: "CreatorRateChanged", logs: logs, sub: sub}, nil
}

// WatchCreatorRateChanged is a free log subscription operation binding the contract event 0xa7423cdbc9422ccce0ec73e11248556c62cac353aadfb8e0ba544172399eace7.
//
// Solidity: event CreatorRateChanged(address collectionContract, uint256 _rewardRate)
func (_Yetiswap0 *Yetiswap0Filterer) WatchCreatorRateChanged(opts *bind.WatchOpts, sink chan<- *Yetiswap0CreatorRateChanged) (event.Subscription, error) {

	logs, sub, err := _Yetiswap0.contract.WatchLogs(opts, "CreatorRateChanged")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Yetiswap0CreatorRateChanged)
				if err := _Yetiswap0.contract.UnpackLog(event, "CreatorRateChanged", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCreatorRateChanged is a log parse operation binding the contract event 0xa7423cdbc9422ccce0ec73e11248556c62cac353aadfb8e0ba544172399eace7.
//
// Solidity: event CreatorRateChanged(address collectionContract, uint256 _rewardRate)
func (_Yetiswap0 *Yetiswap0Filterer) ParseCreatorRateChanged(log types.Log) (*Yetiswap0CreatorRateChanged, error) {
	event := new(Yetiswap0CreatorRateChanged)
	if err := _Yetiswap0.contract.UnpackLog(event, "CreatorRateChanged", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Yetiswap0NewCategoryAddedIterator is returned from FilterNewCategoryAdded and is used to iterate over the raw logs and unpacked data for NewCategoryAdded events raised by the Yetiswap0 contract.
type Yetiswap0NewCategoryAddedIterator struct {
	Event *Yetiswap0NewCategoryAdded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Yetiswap0NewCategoryAddedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Yetiswap0NewCategoryAdded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Yetiswap0NewCategoryAdded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Yetiswap0NewCategoryAddedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Yetiswap0NewCategoryAddedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Yetiswap0NewCategoryAdded represents a NewCategoryAdded event raised by the Yetiswap0 contract.
type Yetiswap0NewCategoryAdded struct {
	CategoryId   *big.Int
	CategoryName string
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterNewCategoryAdded is a free log retrieval operation binding the contract event 0xbf62b32abc5f51132062c531c4a6b975bd9e404358eb4c2b0c0ab34f2c040513.
//
// Solidity: event NewCategoryAdded(uint256 categoryId, string _categoryName)
func (_Yetiswap0 *Yetiswap0Filterer) FilterNewCategoryAdded(opts *bind.FilterOpts) (*Yetiswap0NewCategoryAddedIterator, error) {

	logs, sub, err := _Yetiswap0.contract.FilterLogs(opts, "NewCategoryAdded")
	if err != nil {
		return nil, err
	}
	return &Yetiswap0NewCategoryAddedIterator{contract: _Yetiswap0.contract, event: "NewCategoryAdded", logs: logs, sub: sub}, nil
}

// WatchNewCategoryAdded is a free log subscription operation binding the contract event 0xbf62b32abc5f51132062c531c4a6b975bd9e404358eb4c2b0c0ab34f2c040513.
//
// Solidity: event NewCategoryAdded(uint256 categoryId, string _categoryName)
func (_Yetiswap0 *Yetiswap0Filterer) WatchNewCategoryAdded(opts *bind.WatchOpts, sink chan<- *Yetiswap0NewCategoryAdded) (event.Subscription, error) {

	logs, sub, err := _Yetiswap0.contract.WatchLogs(opts, "NewCategoryAdded")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Yetiswap0NewCategoryAdded)
				if err := _Yetiswap0.contract.UnpackLog(event, "NewCategoryAdded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewCategoryAdded is a log parse operation binding the contract event 0xbf62b32abc5f51132062c531c4a6b975bd9e404358eb4c2b0c0ab34f2c040513.
//
// Solidity: event NewCategoryAdded(uint256 categoryId, string _categoryName)
func (_Yetiswap0 *Yetiswap0Filterer) ParseNewCategoryAdded(log types.Log) (*Yetiswap0NewCategoryAdded, error) {
	event := new(Yetiswap0NewCategoryAdded)
	if err := _Yetiswap0.contract.UnpackLog(event, "NewCategoryAdded", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Yetiswap0NewCollectionAddedIterator is returned from FilterNewCollectionAdded and is used to iterate over the raw logs and unpacked data for NewCollectionAdded events raised by the Yetiswap0 contract.
type Yetiswap0NewCollectionAddedIterator struct {
	Event *Yetiswap0NewCollectionAdded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Yetiswap0NewCollectionAddedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Yetiswap0NewCollectionAdded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Yetiswap0NewCollectionAdded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Yetiswap0NewCollectionAddedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Yetiswap0NewCollectionAddedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Yetiswap0NewCollectionAdded represents a NewCollectionAdded event raised by the Yetiswap0 contract.
type Yetiswap0NewCollectionAdded struct {
	CollectionContract common.Address
	CreatorAddress     common.Address
	RewardRate         *big.Int
	Category           *big.Int
	Raw                types.Log // Blockchain specific contextual infos
}

// FilterNewCollectionAdded is a free log retrieval operation binding the contract event 0x0a915915f7031bf0291b33f90935e82b707faf8ea38d806faa01ffa623274184.
//
// Solidity: event NewCollectionAdded(address collectionContract, address creatorAddress, uint256 _rewardRate, uint256 _category)
func (_Yetiswap0 *Yetiswap0Filterer) FilterNewCollectionAdded(opts *bind.FilterOpts) (*Yetiswap0NewCollectionAddedIterator, error) {

	logs, sub, err := _Yetiswap0.contract.FilterLogs(opts, "NewCollectionAdded")
	if err != nil {
		return nil, err
	}
	return &Yetiswap0NewCollectionAddedIterator{contract: _Yetiswap0.contract, event: "NewCollectionAdded", logs: logs, sub: sub}, nil
}

// WatchNewCollectionAdded is a free log subscription operation binding the contract event 0x0a915915f7031bf0291b33f90935e82b707faf8ea38d806faa01ffa623274184.
//
// Solidity: event NewCollectionAdded(address collectionContract, address creatorAddress, uint256 _rewardRate, uint256 _category)
func (_Yetiswap0 *Yetiswap0Filterer) WatchNewCollectionAdded(opts *bind.WatchOpts, sink chan<- *Yetiswap0NewCollectionAdded) (event.Subscription, error) {

	logs, sub, err := _Yetiswap0.contract.WatchLogs(opts, "NewCollectionAdded")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Yetiswap0NewCollectionAdded)
				if err := _Yetiswap0.contract.UnpackLog(event, "NewCollectionAdded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewCollectionAdded is a log parse operation binding the contract event 0x0a915915f7031bf0291b33f90935e82b707faf8ea38d806faa01ffa623274184.
//
// Solidity: event NewCollectionAdded(address collectionContract, address creatorAddress, uint256 _rewardRate, uint256 _category)
func (_Yetiswap0 *Yetiswap0Filterer) ParseNewCollectionAdded(log types.Log) (*Yetiswap0NewCollectionAdded, error) {
	event := new(Yetiswap0NewCollectionAdded)
	if err := _Yetiswap0.contract.UnpackLog(event, "NewCollectionAdded", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Yetiswap0NewTokenAddedIterator is returned from FilterNewTokenAdded and is used to iterate over the raw logs and unpacked data for NewTokenAdded events raised by the Yetiswap0 contract.
type Yetiswap0NewTokenAddedIterator struct {
	Event *Yetiswap0NewTokenAdded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Yetiswap0NewTokenAddedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Yetiswap0NewTokenAdded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Yetiswap0NewTokenAdded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Yetiswap0NewTokenAddedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Yetiswap0NewTokenAddedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Yetiswap0NewTokenAdded represents a NewTokenAdded event raised by the Yetiswap0 contract.
type Yetiswap0NewTokenAdded struct {
	TokenContract common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterNewTokenAdded is a free log retrieval operation binding the contract event 0xae340411f33c346ae24187433a8df6e508be40cf0deb3949d51cea12df7fa021.
//
// Solidity: event NewTokenAdded(address tokenContract)
func (_Yetiswap0 *Yetiswap0Filterer) FilterNewTokenAdded(opts *bind.FilterOpts) (*Yetiswap0NewTokenAddedIterator, error) {

	logs, sub, err := _Yetiswap0.contract.FilterLogs(opts, "NewTokenAdded")
	if err != nil {
		return nil, err
	}
	return &Yetiswap0NewTokenAddedIterator{contract: _Yetiswap0.contract, event: "NewTokenAdded", logs: logs, sub: sub}, nil
}

// WatchNewTokenAdded is a free log subscription operation binding the contract event 0xae340411f33c346ae24187433a8df6e508be40cf0deb3949d51cea12df7fa021.
//
// Solidity: event NewTokenAdded(address tokenContract)
func (_Yetiswap0 *Yetiswap0Filterer) WatchNewTokenAdded(opts *bind.WatchOpts, sink chan<- *Yetiswap0NewTokenAdded) (event.Subscription, error) {

	logs, sub, err := _Yetiswap0.contract.WatchLogs(opts, "NewTokenAdded")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Yetiswap0NewTokenAdded)
				if err := _Yetiswap0.contract.UnpackLog(event, "NewTokenAdded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewTokenAdded is a log parse operation binding the contract event 0xae340411f33c346ae24187433a8df6e508be40cf0deb3949d51cea12df7fa021.
//
// Solidity: event NewTokenAdded(address tokenContract)
func (_Yetiswap0 *Yetiswap0Filterer) ParseNewTokenAdded(log types.Log) (*Yetiswap0NewTokenAdded, error) {
	event := new(Yetiswap0NewTokenAdded)
	if err := _Yetiswap0.contract.UnpackLog(event, "NewTokenAdded", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Yetiswap0OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Yetiswap0 contract.
type Yetiswap0OwnershipTransferredIterator struct {
	Event *Yetiswap0OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Yetiswap0OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Yetiswap0OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Yetiswap0OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Yetiswap0OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Yetiswap0OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Yetiswap0OwnershipTransferred represents a OwnershipTransferred event raised by the Yetiswap0 contract.
type Yetiswap0OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Yetiswap0 *Yetiswap0Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Yetiswap0OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Yetiswap0.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Yetiswap0OwnershipTransferredIterator{contract: _Yetiswap0.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Yetiswap0 *Yetiswap0Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Yetiswap0OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Yetiswap0.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Yetiswap0OwnershipTransferred)
				if err := _Yetiswap0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Yetiswap0 *Yetiswap0Filterer) ParseOwnershipTransferred(log types.Log) (*Yetiswap0OwnershipTransferred, error) {
	event := new(Yetiswap0OwnershipTransferred)
	if err := _Yetiswap0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Yetiswap0TokenOnSaleIterator is returned from FilterTokenOnSale and is used to iterate over the raw logs and unpacked data for TokenOnSale events raised by the Yetiswap0 contract.
type Yetiswap0TokenOnSaleIterator struct {
	Event *Yetiswap0TokenOnSale // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Yetiswap0TokenOnSaleIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Yetiswap0TokenOnSale)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Yetiswap0TokenOnSale)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Yetiswap0TokenOnSaleIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Yetiswap0TokenOnSaleIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Yetiswap0TokenOnSale represents a TokenOnSale event raised by the Yetiswap0 contract.
type Yetiswap0TokenOnSale struct {
	Seller      common.Address
	NftContract common.Address
	Creator     common.Address
	NftId       *big.Int
	SaleToken   common.Address
	Price       *big.Int
	Category    *big.Int
	SaleIndex   *big.Int
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterTokenOnSale is a free log retrieval operation binding the contract event 0x9dc49d0c8dbf4b4ef914b445e53f8de77b76813e6f80919ba14efe49c66c1738.
//
// Solidity: event TokenOnSale(address seller, address nftContract, address creator, uint256 nftId, address saleToken, uint256 price, uint256 category, uint256 saleIndex)
func (_Yetiswap0 *Yetiswap0Filterer) FilterTokenOnSale(opts *bind.FilterOpts) (*Yetiswap0TokenOnSaleIterator, error) {

	logs, sub, err := _Yetiswap0.contract.FilterLogs(opts, "TokenOnSale")
	if err != nil {
		return nil, err
	}
	return &Yetiswap0TokenOnSaleIterator{contract: _Yetiswap0.contract, event: "TokenOnSale", logs: logs, sub: sub}, nil
}

// WatchTokenOnSale is a free log subscription operation binding the contract event 0x9dc49d0c8dbf4b4ef914b445e53f8de77b76813e6f80919ba14efe49c66c1738.
//
// Solidity: event TokenOnSale(address seller, address nftContract, address creator, uint256 nftId, address saleToken, uint256 price, uint256 category, uint256 saleIndex)
func (_Yetiswap0 *Yetiswap0Filterer) WatchTokenOnSale(opts *bind.WatchOpts, sink chan<- *Yetiswap0TokenOnSale) (event.Subscription, error) {

	logs, sub, err := _Yetiswap0.contract.WatchLogs(opts, "TokenOnSale")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Yetiswap0TokenOnSale)
				if err := _Yetiswap0.contract.UnpackLog(event, "TokenOnSale", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokenOnSale is a log parse operation binding the contract event 0x9dc49d0c8dbf4b4ef914b445e53f8de77b76813e6f80919ba14efe49c66c1738.
//
// Solidity: event TokenOnSale(address seller, address nftContract, address creator, uint256 nftId, address saleToken, uint256 price, uint256 category, uint256 saleIndex)
func (_Yetiswap0 *Yetiswap0Filterer) ParseTokenOnSale(log types.Log) (*Yetiswap0TokenOnSale, error) {
	event := new(Yetiswap0TokenOnSale)
	if err := _Yetiswap0.contract.UnpackLog(event, "TokenOnSale", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Yetiswap0TokenRemovedIterator is returned from FilterTokenRemoved and is used to iterate over the raw logs and unpacked data for TokenRemoved events raised by the Yetiswap0 contract.
type Yetiswap0TokenRemovedIterator struct {
	Event *Yetiswap0TokenRemoved // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Yetiswap0TokenRemovedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Yetiswap0TokenRemoved)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Yetiswap0TokenRemoved)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Yetiswap0TokenRemovedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Yetiswap0TokenRemovedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Yetiswap0TokenRemoved represents a TokenRemoved event raised by the Yetiswap0 contract.
type Yetiswap0TokenRemoved struct {
	TokenContract common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterTokenRemoved is a free log retrieval operation binding the contract event 0x4c910b69fe65a61f7531b9c5042b2329ca7179c77290aa7e2eb3afa3c8511fd3.
//
// Solidity: event TokenRemoved(address tokenContract)
func (_Yetiswap0 *Yetiswap0Filterer) FilterTokenRemoved(opts *bind.FilterOpts) (*Yetiswap0TokenRemovedIterator, error) {

	logs, sub, err := _Yetiswap0.contract.FilterLogs(opts, "TokenRemoved")
	if err != nil {
		return nil, err
	}
	return &Yetiswap0TokenRemovedIterator{contract: _Yetiswap0.contract, event: "TokenRemoved", logs: logs, sub: sub}, nil
}

// WatchTokenRemoved is a free log subscription operation binding the contract event 0x4c910b69fe65a61f7531b9c5042b2329ca7179c77290aa7e2eb3afa3c8511fd3.
//
// Solidity: event TokenRemoved(address tokenContract)
func (_Yetiswap0 *Yetiswap0Filterer) WatchTokenRemoved(opts *bind.WatchOpts, sink chan<- *Yetiswap0TokenRemoved) (event.Subscription, error) {

	logs, sub, err := _Yetiswap0.contract.WatchLogs(opts, "TokenRemoved")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Yetiswap0TokenRemoved)
				if err := _Yetiswap0.contract.UnpackLog(event, "TokenRemoved", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokenRemoved is a log parse operation binding the contract event 0x4c910b69fe65a61f7531b9c5042b2329ca7179c77290aa7e2eb3afa3c8511fd3.
//
// Solidity: event TokenRemoved(address tokenContract)
func (_Yetiswap0 *Yetiswap0Filterer) ParseTokenRemoved(log types.Log) (*Yetiswap0TokenRemoved, error) {
	event := new(Yetiswap0TokenRemoved)
	if err := _Yetiswap0.contract.UnpackLog(event, "TokenRemoved", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Yetiswap0TokenRemovedFromSaleIterator is returned from FilterTokenRemovedFromSale and is used to iterate over the raw logs and unpacked data for TokenRemovedFromSale events raised by the Yetiswap0 contract.
type Yetiswap0TokenRemovedFromSaleIterator struct {
	Event *Yetiswap0TokenRemovedFromSale // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Yetiswap0TokenRemovedFromSaleIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Yetiswap0TokenRemovedFromSale)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Yetiswap0TokenRemovedFromSale)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Yetiswap0TokenRemovedFromSaleIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Yetiswap0TokenRemovedFromSaleIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Yetiswap0TokenRemovedFromSale represents a TokenRemovedFromSale event raised by the Yetiswap0 contract.
type Yetiswap0TokenRemovedFromSale struct {
	SaleIndex *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterTokenRemovedFromSale is a free log retrieval operation binding the contract event 0xccd0b7af52bf5f08ec6781299e66283301e612d1ca1ffceab3579a6364073c30.
//
// Solidity: event TokenRemovedFromSale(uint256 saleIndex)
func (_Yetiswap0 *Yetiswap0Filterer) FilterTokenRemovedFromSale(opts *bind.FilterOpts) (*Yetiswap0TokenRemovedFromSaleIterator, error) {

	logs, sub, err := _Yetiswap0.contract.FilterLogs(opts, "TokenRemovedFromSale")
	if err != nil {
		return nil, err
	}
	return &Yetiswap0TokenRemovedFromSaleIterator{contract: _Yetiswap0.contract, event: "TokenRemovedFromSale", logs: logs, sub: sub}, nil
}

// WatchTokenRemovedFromSale is a free log subscription operation binding the contract event 0xccd0b7af52bf5f08ec6781299e66283301e612d1ca1ffceab3579a6364073c30.
//
// Solidity: event TokenRemovedFromSale(uint256 saleIndex)
func (_Yetiswap0 *Yetiswap0Filterer) WatchTokenRemovedFromSale(opts *bind.WatchOpts, sink chan<- *Yetiswap0TokenRemovedFromSale) (event.Subscription, error) {

	logs, sub, err := _Yetiswap0.contract.WatchLogs(opts, "TokenRemovedFromSale")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Yetiswap0TokenRemovedFromSale)
				if err := _Yetiswap0.contract.UnpackLog(event, "TokenRemovedFromSale", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokenRemovedFromSale is a log parse operation binding the contract event 0xccd0b7af52bf5f08ec6781299e66283301e612d1ca1ffceab3579a6364073c30.
//
// Solidity: event TokenRemovedFromSale(uint256 saleIndex)
func (_Yetiswap0 *Yetiswap0Filterer) ParseTokenRemovedFromSale(log types.Log) (*Yetiswap0TokenRemovedFromSale, error) {
	event := new(Yetiswap0TokenRemovedFromSale)
	if err := _Yetiswap0.contract.UnpackLog(event, "TokenRemovedFromSale", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Yetiswap0TokenSoldIterator is returned from FilterTokenSold and is used to iterate over the raw logs and unpacked data for TokenSold events raised by the Yetiswap0 contract.
type Yetiswap0TokenSoldIterator struct {
	Event *Yetiswap0TokenSold // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Yetiswap0TokenSoldIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Yetiswap0TokenSold)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Yetiswap0TokenSold)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Yetiswap0TokenSoldIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Yetiswap0TokenSoldIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Yetiswap0TokenSold represents a TokenSold event raised by the Yetiswap0 contract.
type Yetiswap0TokenSold struct {
	Buyer     common.Address
	SaleIndex *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterTokenSold is a free log retrieval operation binding the contract event 0xfe2ff4cf36ff7d2c2b06eb960897ee0d76d9c3e58da12feb7b93e86b226dd344.
//
// Solidity: event TokenSold(address buyer, uint256 saleIndex)
func (_Yetiswap0 *Yetiswap0Filterer) FilterTokenSold(opts *bind.FilterOpts) (*Yetiswap0TokenSoldIterator, error) {

	logs, sub, err := _Yetiswap0.contract.FilterLogs(opts, "TokenSold")
	if err != nil {
		return nil, err
	}
	return &Yetiswap0TokenSoldIterator{contract: _Yetiswap0.contract, event: "TokenSold", logs: logs, sub: sub}, nil
}

// WatchTokenSold is a free log subscription operation binding the contract event 0xfe2ff4cf36ff7d2c2b06eb960897ee0d76d9c3e58da12feb7b93e86b226dd344.
//
// Solidity: event TokenSold(address buyer, uint256 saleIndex)
func (_Yetiswap0 *Yetiswap0Filterer) WatchTokenSold(opts *bind.WatchOpts, sink chan<- *Yetiswap0TokenSold) (event.Subscription, error) {

	logs, sub, err := _Yetiswap0.contract.WatchLogs(opts, "TokenSold")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Yetiswap0TokenSold)
				if err := _Yetiswap0.contract.UnpackLog(event, "TokenSold", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokenSold is a log parse operation binding the contract event 0xfe2ff4cf36ff7d2c2b06eb960897ee0d76d9c3e58da12feb7b93e86b226dd344.
//
// Solidity: event TokenSold(address buyer, uint256 saleIndex)
func (_Yetiswap0 *Yetiswap0Filterer) ParseTokenSold(log types.Log) (*Yetiswap0TokenSold, error) {
	event := new(Yetiswap0TokenSold)
	if err := _Yetiswap0.contract.UnpackLog(event, "TokenSold", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
