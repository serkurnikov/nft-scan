// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package partyanimals_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// MarketListing is an auto generated low-level Go binding around an user-defined struct.
type MarketListing struct {
	Active          bool
	Id              *big.Int
	TokenId         *big.Int
	Price           *big.Int
	ActiveIndex     *big.Int
	UserActiveIndex *big.Int
	Owner           common.Address
	TokenURI        string
}

// MarketPurchase is an auto generated low-level Go binding around an user-defined struct.
type MarketPurchase struct {
	Listing MarketListing
	Buyer   common.Address
}

// Partyanimals0MetaData contains all meta data concerning the Partyanimals0 contract.
var Partyanimals0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"nft_address\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"dist_fee\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"market_fee\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"components\":[{\"internalType\":\"bool\",\"name\":\"active\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"activeIndex\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"userActiveIndex\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"string\",\"name\":\"tokenURI\",\"type\":\"string\"}],\"indexed\":false,\"internalType\":\"structMarket.Listing\",\"name\":\"listing\",\"type\":\"tuple\"}],\"name\":\"AddedListing\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"components\":[{\"internalType\":\"bool\",\"name\":\"active\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"activeIndex\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"userActiveIndex\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"string\",\"name\":\"tokenURI\",\"type\":\"string\"}],\"indexed\":false,\"internalType\":\"structMarket.Listing\",\"name\":\"listing\",\"type\":\"tuple\"}],\"name\":\"CanceledListing\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"components\":[{\"components\":[{\"internalType\":\"bool\",\"name\":\"active\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"activeIndex\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"userActiveIndex\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"string\",\"name\":\"tokenURI\",\"type\":\"string\"}],\"internalType\":\"structMarket.Listing\",\"name\":\"listing\",\"type\":\"tuple\"},{\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"}],\"indexed\":false,\"internalType\":\"structMarket.Purchase\",\"name\":\"listing\",\"type\":\"tuple\"}],\"name\":\"FilledListing\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"components\":[{\"internalType\":\"bool\",\"name\":\"active\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"activeIndex\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"userActiveIndex\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"string\",\"name\":\"tokenURI\",\"type\":\"string\"}],\"indexed\":false,\"internalType\":\"structMarket.Listing\",\"name\":\"listing\",\"type\":\"tuple\"}],\"name\":\"UpdateListing\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"activeListings\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"addListing\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"newDistFee\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"newMarketFee\",\"type\":\"uint256\"}],\"name\":\"adjustFees\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"allowEmergencyDelisting\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"}],\"name\":\"cancelListing\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"from\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"length\",\"type\":\"uint256\"}],\"name\":\"claimListedRewards\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"from\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"length\",\"type\":\"uint256\"}],\"name\":\"claimOwnedRewards\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"claimRewards\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"closeMarket\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"communityFeePercent\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"communityHoldings\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"communityRewards\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"listingID\",\"type\":\"uint256\"}],\"name\":\"emergencyDelist\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"emergencyDelisting\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"emergencyWithdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"}],\"name\":\"fulfillListing\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"from\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"length\",\"type\":\"uint256\"}],\"name\":\"getActiveListings\",\"outputs\":[{\"components\":[{\"internalType\":\"bool\",\"name\":\"active\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"activeIndex\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"userActiveIndex\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"string\",\"name\":\"tokenURI\",\"type\":\"string\"}],\"internalType\":\"structMarket.Listing[]\",\"name\":\"listing\",\"type\":\"tuple[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"from\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"length\",\"type\":\"uint256\"}],\"name\":\"getMyActiveListings\",\"outputs\":[{\"components\":[{\"internalType\":\"bool\",\"name\":\"active\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"activeIndex\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"userActiveIndex\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"string\",\"name\":\"tokenURI\",\"type\":\"string\"}],\"internalType\":\"structMarket.Listing[]\",\"name\":\"listing\",\"type\":\"tuple[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getMyActiveListingsCount\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getRewards\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"highestSalePrice\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"isMarketOpen\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"listings\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"active\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"activeIndex\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"userActiveIndex\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"string\",\"name\":\"tokenURI\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"marketFeePercent\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"openMarket\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalActiveListings\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalGivenRewardsPerToken\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalListings\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalSales\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalVolume\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"updateListing\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"userActiveListings\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"withdrawBalance\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"withdrawableBalance\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
}

// Partyanimals0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Partyanimals0MetaData.ABI instead.
var Partyanimals0ABI = Partyanimals0MetaData.ABI

// Partyanimals0 is an auto generated Go binding around an Ethereum contract.
type Partyanimals0 struct {
	Partyanimals0Caller     // Read-only binding to the contract
	Partyanimals0Transactor // Write-only binding to the contract
	Partyanimals0Filterer   // Log filterer for contract events
}

// Partyanimals0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Partyanimals0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Partyanimals0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Partyanimals0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Partyanimals0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Partyanimals0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Partyanimals0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Partyanimals0Session struct {
	Contract     *Partyanimals0    // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Partyanimals0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Partyanimals0CallerSession struct {
	Contract *Partyanimals0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts        // Call options to use throughout this session
}

// Partyanimals0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Partyanimals0TransactorSession struct {
	Contract     *Partyanimals0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts        // Transaction auth options to use throughout this session
}

// Partyanimals0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Partyanimals0Raw struct {
	Contract *Partyanimals0 // Generic contract binding to access the raw methods on
}

// Partyanimals0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Partyanimals0CallerRaw struct {
	Contract *Partyanimals0Caller // Generic read-only contract binding to access the raw methods on
}

// Partyanimals0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Partyanimals0TransactorRaw struct {
	Contract *Partyanimals0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewPartyanimals0 creates a new instance of Partyanimals0, bound to a specific deployed contract.
func NewPartyanimals0(address common.Address, backend bind.ContractBackend) (*Partyanimals0, error) {
	contract, err := bindPartyanimals0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Partyanimals0{Partyanimals0Caller: Partyanimals0Caller{contract: contract}, Partyanimals0Transactor: Partyanimals0Transactor{contract: contract}, Partyanimals0Filterer: Partyanimals0Filterer{contract: contract}}, nil
}

// NewPartyanimals0Caller creates a new read-only instance of Partyanimals0, bound to a specific deployed contract.
func NewPartyanimals0Caller(address common.Address, caller bind.ContractCaller) (*Partyanimals0Caller, error) {
	contract, err := bindPartyanimals0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Partyanimals0Caller{contract: contract}, nil
}

// NewPartyanimals0Transactor creates a new write-only instance of Partyanimals0, bound to a specific deployed contract.
func NewPartyanimals0Transactor(address common.Address, transactor bind.ContractTransactor) (*Partyanimals0Transactor, error) {
	contract, err := bindPartyanimals0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Partyanimals0Transactor{contract: contract}, nil
}

// NewPartyanimals0Filterer creates a new log filterer instance of Partyanimals0, bound to a specific deployed contract.
func NewPartyanimals0Filterer(address common.Address, filterer bind.ContractFilterer) (*Partyanimals0Filterer, error) {
	contract, err := bindPartyanimals0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Partyanimals0Filterer{contract: contract}, nil
}

// bindPartyanimals0 binds a generic wrapper to an already deployed contract.
func bindPartyanimals0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Partyanimals0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Partyanimals0 *Partyanimals0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Partyanimals0.Contract.Partyanimals0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Partyanimals0 *Partyanimals0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Partyanimals0.Contract.Partyanimals0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Partyanimals0 *Partyanimals0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Partyanimals0.Contract.Partyanimals0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Partyanimals0 *Partyanimals0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Partyanimals0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Partyanimals0 *Partyanimals0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Partyanimals0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Partyanimals0 *Partyanimals0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Partyanimals0.Contract.contract.Transact(opts, method, params...)
}

// ActiveListings is a free data retrieval call binding the contract method 0x61cf4863.
//
// Solidity: function activeListings(uint256 ) view returns(uint256)
func (_Partyanimals0 *Partyanimals0Caller) ActiveListings(opts *bind.CallOpts, arg0 *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "activeListings", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// ActiveListings is a free data retrieval call binding the contract method 0x61cf4863.
//
// Solidity: function activeListings(uint256 ) view returns(uint256)
func (_Partyanimals0 *Partyanimals0Session) ActiveListings(arg0 *big.Int) (*big.Int, error) {
	return _Partyanimals0.Contract.ActiveListings(&_Partyanimals0.CallOpts, arg0)
}

// ActiveListings is a free data retrieval call binding the contract method 0x61cf4863.
//
// Solidity: function activeListings(uint256 ) view returns(uint256)
func (_Partyanimals0 *Partyanimals0CallerSession) ActiveListings(arg0 *big.Int) (*big.Int, error) {
	return _Partyanimals0.Contract.ActiveListings(&_Partyanimals0.CallOpts, arg0)
}

// CommunityFeePercent is a free data retrieval call binding the contract method 0x74ee6d4f.
//
// Solidity: function communityFeePercent() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Caller) CommunityFeePercent(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "communityFeePercent")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CommunityFeePercent is a free data retrieval call binding the contract method 0x74ee6d4f.
//
// Solidity: function communityFeePercent() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Session) CommunityFeePercent() (*big.Int, error) {
	return _Partyanimals0.Contract.CommunityFeePercent(&_Partyanimals0.CallOpts)
}

// CommunityFeePercent is a free data retrieval call binding the contract method 0x74ee6d4f.
//
// Solidity: function communityFeePercent() view returns(uint256)
func (_Partyanimals0 *Partyanimals0CallerSession) CommunityFeePercent() (*big.Int, error) {
	return _Partyanimals0.Contract.CommunityFeePercent(&_Partyanimals0.CallOpts)
}

// CommunityHoldings is a free data retrieval call binding the contract method 0x297dc18c.
//
// Solidity: function communityHoldings() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Caller) CommunityHoldings(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "communityHoldings")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CommunityHoldings is a free data retrieval call binding the contract method 0x297dc18c.
//
// Solidity: function communityHoldings() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Session) CommunityHoldings() (*big.Int, error) {
	return _Partyanimals0.Contract.CommunityHoldings(&_Partyanimals0.CallOpts)
}

// CommunityHoldings is a free data retrieval call binding the contract method 0x297dc18c.
//
// Solidity: function communityHoldings() view returns(uint256)
func (_Partyanimals0 *Partyanimals0CallerSession) CommunityHoldings() (*big.Int, error) {
	return _Partyanimals0.Contract.CommunityHoldings(&_Partyanimals0.CallOpts)
}

// CommunityRewards is a free data retrieval call binding the contract method 0xf7cae643.
//
// Solidity: function communityRewards(uint256 ) view returns(uint256)
func (_Partyanimals0 *Partyanimals0Caller) CommunityRewards(opts *bind.CallOpts, arg0 *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "communityRewards", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CommunityRewards is a free data retrieval call binding the contract method 0xf7cae643.
//
// Solidity: function communityRewards(uint256 ) view returns(uint256)
func (_Partyanimals0 *Partyanimals0Session) CommunityRewards(arg0 *big.Int) (*big.Int, error) {
	return _Partyanimals0.Contract.CommunityRewards(&_Partyanimals0.CallOpts, arg0)
}

// CommunityRewards is a free data retrieval call binding the contract method 0xf7cae643.
//
// Solidity: function communityRewards(uint256 ) view returns(uint256)
func (_Partyanimals0 *Partyanimals0CallerSession) CommunityRewards(arg0 *big.Int) (*big.Int, error) {
	return _Partyanimals0.Contract.CommunityRewards(&_Partyanimals0.CallOpts, arg0)
}

// EmergencyDelisting is a free data retrieval call binding the contract method 0xaca20f07.
//
// Solidity: function emergencyDelisting() view returns(bool)
func (_Partyanimals0 *Partyanimals0Caller) EmergencyDelisting(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "emergencyDelisting")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// EmergencyDelisting is a free data retrieval call binding the contract method 0xaca20f07.
//
// Solidity: function emergencyDelisting() view returns(bool)
func (_Partyanimals0 *Partyanimals0Session) EmergencyDelisting() (bool, error) {
	return _Partyanimals0.Contract.EmergencyDelisting(&_Partyanimals0.CallOpts)
}

// EmergencyDelisting is a free data retrieval call binding the contract method 0xaca20f07.
//
// Solidity: function emergencyDelisting() view returns(bool)
func (_Partyanimals0 *Partyanimals0CallerSession) EmergencyDelisting() (bool, error) {
	return _Partyanimals0.Contract.EmergencyDelisting(&_Partyanimals0.CallOpts)
}

// GetActiveListings is a free data retrieval call binding the contract method 0xc128b231.
//
// Solidity: function getActiveListings(uint256 from, uint256 length) view returns((bool,uint256,uint256,uint256,uint256,uint256,address,string)[] listing)
func (_Partyanimals0 *Partyanimals0Caller) GetActiveListings(opts *bind.CallOpts, from *big.Int, length *big.Int) ([]MarketListing, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "getActiveListings", from, length)

	if err != nil {
		return *new([]MarketListing), err
	}

	out0 := *abi.ConvertType(out[0], new([]MarketListing)).(*[]MarketListing)

	return out0, err

}

// GetActiveListings is a free data retrieval call binding the contract method 0xc128b231.
//
// Solidity: function getActiveListings(uint256 from, uint256 length) view returns((bool,uint256,uint256,uint256,uint256,uint256,address,string)[] listing)
func (_Partyanimals0 *Partyanimals0Session) GetActiveListings(from *big.Int, length *big.Int) ([]MarketListing, error) {
	return _Partyanimals0.Contract.GetActiveListings(&_Partyanimals0.CallOpts, from, length)
}

// GetActiveListings is a free data retrieval call binding the contract method 0xc128b231.
//
// Solidity: function getActiveListings(uint256 from, uint256 length) view returns((bool,uint256,uint256,uint256,uint256,uint256,address,string)[] listing)
func (_Partyanimals0 *Partyanimals0CallerSession) GetActiveListings(from *big.Int, length *big.Int) ([]MarketListing, error) {
	return _Partyanimals0.Contract.GetActiveListings(&_Partyanimals0.CallOpts, from, length)
}

// GetMyActiveListings is a free data retrieval call binding the contract method 0xc06d61f5.
//
// Solidity: function getMyActiveListings(uint256 from, uint256 length) view returns((bool,uint256,uint256,uint256,uint256,uint256,address,string)[] listing)
func (_Partyanimals0 *Partyanimals0Caller) GetMyActiveListings(opts *bind.CallOpts, from *big.Int, length *big.Int) ([]MarketListing, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "getMyActiveListings", from, length)

	if err != nil {
		return *new([]MarketListing), err
	}

	out0 := *abi.ConvertType(out[0], new([]MarketListing)).(*[]MarketListing)

	return out0, err

}

// GetMyActiveListings is a free data retrieval call binding the contract method 0xc06d61f5.
//
// Solidity: function getMyActiveListings(uint256 from, uint256 length) view returns((bool,uint256,uint256,uint256,uint256,uint256,address,string)[] listing)
func (_Partyanimals0 *Partyanimals0Session) GetMyActiveListings(from *big.Int, length *big.Int) ([]MarketListing, error) {
	return _Partyanimals0.Contract.GetMyActiveListings(&_Partyanimals0.CallOpts, from, length)
}

// GetMyActiveListings is a free data retrieval call binding the contract method 0xc06d61f5.
//
// Solidity: function getMyActiveListings(uint256 from, uint256 length) view returns((bool,uint256,uint256,uint256,uint256,uint256,address,string)[] listing)
func (_Partyanimals0 *Partyanimals0CallerSession) GetMyActiveListings(from *big.Int, length *big.Int) ([]MarketListing, error) {
	return _Partyanimals0.Contract.GetMyActiveListings(&_Partyanimals0.CallOpts, from, length)
}

// GetMyActiveListingsCount is a free data retrieval call binding the contract method 0x15f4d22a.
//
// Solidity: function getMyActiveListingsCount() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Caller) GetMyActiveListingsCount(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "getMyActiveListingsCount")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetMyActiveListingsCount is a free data retrieval call binding the contract method 0x15f4d22a.
//
// Solidity: function getMyActiveListingsCount() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Session) GetMyActiveListingsCount() (*big.Int, error) {
	return _Partyanimals0.Contract.GetMyActiveListingsCount(&_Partyanimals0.CallOpts)
}

// GetMyActiveListingsCount is a free data retrieval call binding the contract method 0x15f4d22a.
//
// Solidity: function getMyActiveListingsCount() view returns(uint256)
func (_Partyanimals0 *Partyanimals0CallerSession) GetMyActiveListingsCount() (*big.Int, error) {
	return _Partyanimals0.Contract.GetMyActiveListingsCount(&_Partyanimals0.CallOpts)
}

// GetRewards is a free data retrieval call binding the contract method 0x0572b0cc.
//
// Solidity: function getRewards() view returns(uint256 amount)
func (_Partyanimals0 *Partyanimals0Caller) GetRewards(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "getRewards")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetRewards is a free data retrieval call binding the contract method 0x0572b0cc.
//
// Solidity: function getRewards() view returns(uint256 amount)
func (_Partyanimals0 *Partyanimals0Session) GetRewards() (*big.Int, error) {
	return _Partyanimals0.Contract.GetRewards(&_Partyanimals0.CallOpts)
}

// GetRewards is a free data retrieval call binding the contract method 0x0572b0cc.
//
// Solidity: function getRewards() view returns(uint256 amount)
func (_Partyanimals0 *Partyanimals0CallerSession) GetRewards() (*big.Int, error) {
	return _Partyanimals0.Contract.GetRewards(&_Partyanimals0.CallOpts)
}

// HighestSalePrice is a free data retrieval call binding the contract method 0x3fe1e117.
//
// Solidity: function highestSalePrice() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Caller) HighestSalePrice(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "highestSalePrice")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// HighestSalePrice is a free data retrieval call binding the contract method 0x3fe1e117.
//
// Solidity: function highestSalePrice() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Session) HighestSalePrice() (*big.Int, error) {
	return _Partyanimals0.Contract.HighestSalePrice(&_Partyanimals0.CallOpts)
}

// HighestSalePrice is a free data retrieval call binding the contract method 0x3fe1e117.
//
// Solidity: function highestSalePrice() view returns(uint256)
func (_Partyanimals0 *Partyanimals0CallerSession) HighestSalePrice() (*big.Int, error) {
	return _Partyanimals0.Contract.HighestSalePrice(&_Partyanimals0.CallOpts)
}

// IsMarketOpen is a free data retrieval call binding the contract method 0xd4ce85f3.
//
// Solidity: function isMarketOpen() view returns(bool)
func (_Partyanimals0 *Partyanimals0Caller) IsMarketOpen(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "isMarketOpen")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsMarketOpen is a free data retrieval call binding the contract method 0xd4ce85f3.
//
// Solidity: function isMarketOpen() view returns(bool)
func (_Partyanimals0 *Partyanimals0Session) IsMarketOpen() (bool, error) {
	return _Partyanimals0.Contract.IsMarketOpen(&_Partyanimals0.CallOpts)
}

// IsMarketOpen is a free data retrieval call binding the contract method 0xd4ce85f3.
//
// Solidity: function isMarketOpen() view returns(bool)
func (_Partyanimals0 *Partyanimals0CallerSession) IsMarketOpen() (bool, error) {
	return _Partyanimals0.Contract.IsMarketOpen(&_Partyanimals0.CallOpts)
}

// Listings is a free data retrieval call binding the contract method 0xde74e57b.
//
// Solidity: function listings(uint256 ) view returns(bool active, uint256 id, uint256 tokenId, uint256 price, uint256 activeIndex, uint256 userActiveIndex, address owner, string tokenURI)
func (_Partyanimals0 *Partyanimals0Caller) Listings(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Active          bool
	Id              *big.Int
	TokenId         *big.Int
	Price           *big.Int
	ActiveIndex     *big.Int
	UserActiveIndex *big.Int
	Owner           common.Address
	TokenURI        string
}, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "listings", arg0)

	outstruct := new(struct {
		Active          bool
		Id              *big.Int
		TokenId         *big.Int
		Price           *big.Int
		ActiveIndex     *big.Int
		UserActiveIndex *big.Int
		Owner           common.Address
		TokenURI        string
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Active = *abi.ConvertType(out[0], new(bool)).(*bool)
	outstruct.Id = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.TokenId = *abi.ConvertType(out[2], new(*big.Int)).(**big.Int)
	outstruct.Price = *abi.ConvertType(out[3], new(*big.Int)).(**big.Int)
	outstruct.ActiveIndex = *abi.ConvertType(out[4], new(*big.Int)).(**big.Int)
	outstruct.UserActiveIndex = *abi.ConvertType(out[5], new(*big.Int)).(**big.Int)
	outstruct.Owner = *abi.ConvertType(out[6], new(common.Address)).(*common.Address)
	outstruct.TokenURI = *abi.ConvertType(out[7], new(string)).(*string)

	return *outstruct, err

}

// Listings is a free data retrieval call binding the contract method 0xde74e57b.
//
// Solidity: function listings(uint256 ) view returns(bool active, uint256 id, uint256 tokenId, uint256 price, uint256 activeIndex, uint256 userActiveIndex, address owner, string tokenURI)
func (_Partyanimals0 *Partyanimals0Session) Listings(arg0 *big.Int) (struct {
	Active          bool
	Id              *big.Int
	TokenId         *big.Int
	Price           *big.Int
	ActiveIndex     *big.Int
	UserActiveIndex *big.Int
	Owner           common.Address
	TokenURI        string
}, error) {
	return _Partyanimals0.Contract.Listings(&_Partyanimals0.CallOpts, arg0)
}

// Listings is a free data retrieval call binding the contract method 0xde74e57b.
//
// Solidity: function listings(uint256 ) view returns(bool active, uint256 id, uint256 tokenId, uint256 price, uint256 activeIndex, uint256 userActiveIndex, address owner, string tokenURI)
func (_Partyanimals0 *Partyanimals0CallerSession) Listings(arg0 *big.Int) (struct {
	Active          bool
	Id              *big.Int
	TokenId         *big.Int
	Price           *big.Int
	ActiveIndex     *big.Int
	UserActiveIndex *big.Int
	Owner           common.Address
	TokenURI        string
}, error) {
	return _Partyanimals0.Contract.Listings(&_Partyanimals0.CallOpts, arg0)
}

// MarketFeePercent is a free data retrieval call binding the contract method 0xaaf51b7e.
//
// Solidity: function marketFeePercent() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Caller) MarketFeePercent(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "marketFeePercent")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MarketFeePercent is a free data retrieval call binding the contract method 0xaaf51b7e.
//
// Solidity: function marketFeePercent() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Session) MarketFeePercent() (*big.Int, error) {
	return _Partyanimals0.Contract.MarketFeePercent(&_Partyanimals0.CallOpts)
}

// MarketFeePercent is a free data retrieval call binding the contract method 0xaaf51b7e.
//
// Solidity: function marketFeePercent() view returns(uint256)
func (_Partyanimals0 *Partyanimals0CallerSession) MarketFeePercent() (*big.Int, error) {
	return _Partyanimals0.Contract.MarketFeePercent(&_Partyanimals0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Partyanimals0 *Partyanimals0Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Partyanimals0 *Partyanimals0Session) Owner() (common.Address, error) {
	return _Partyanimals0.Contract.Owner(&_Partyanimals0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Partyanimals0 *Partyanimals0CallerSession) Owner() (common.Address, error) {
	return _Partyanimals0.Contract.Owner(&_Partyanimals0.CallOpts)
}

// TotalActiveListings is a free data retrieval call binding the contract method 0x741520b0.
//
// Solidity: function totalActiveListings() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Caller) TotalActiveListings(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "totalActiveListings")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalActiveListings is a free data retrieval call binding the contract method 0x741520b0.
//
// Solidity: function totalActiveListings() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Session) TotalActiveListings() (*big.Int, error) {
	return _Partyanimals0.Contract.TotalActiveListings(&_Partyanimals0.CallOpts)
}

// TotalActiveListings is a free data retrieval call binding the contract method 0x741520b0.
//
// Solidity: function totalActiveListings() view returns(uint256)
func (_Partyanimals0 *Partyanimals0CallerSession) TotalActiveListings() (*big.Int, error) {
	return _Partyanimals0.Contract.TotalActiveListings(&_Partyanimals0.CallOpts)
}

// TotalGivenRewardsPerToken is a free data retrieval call binding the contract method 0x6d0c689e.
//
// Solidity: function totalGivenRewardsPerToken() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Caller) TotalGivenRewardsPerToken(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "totalGivenRewardsPerToken")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalGivenRewardsPerToken is a free data retrieval call binding the contract method 0x6d0c689e.
//
// Solidity: function totalGivenRewardsPerToken() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Session) TotalGivenRewardsPerToken() (*big.Int, error) {
	return _Partyanimals0.Contract.TotalGivenRewardsPerToken(&_Partyanimals0.CallOpts)
}

// TotalGivenRewardsPerToken is a free data retrieval call binding the contract method 0x6d0c689e.
//
// Solidity: function totalGivenRewardsPerToken() view returns(uint256)
func (_Partyanimals0 *Partyanimals0CallerSession) TotalGivenRewardsPerToken() (*big.Int, error) {
	return _Partyanimals0.Contract.TotalGivenRewardsPerToken(&_Partyanimals0.CallOpts)
}

// TotalListings is a free data retrieval call binding the contract method 0xc78b616c.
//
// Solidity: function totalListings() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Caller) TotalListings(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "totalListings")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalListings is a free data retrieval call binding the contract method 0xc78b616c.
//
// Solidity: function totalListings() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Session) TotalListings() (*big.Int, error) {
	return _Partyanimals0.Contract.TotalListings(&_Partyanimals0.CallOpts)
}

// TotalListings is a free data retrieval call binding the contract method 0xc78b616c.
//
// Solidity: function totalListings() view returns(uint256)
func (_Partyanimals0 *Partyanimals0CallerSession) TotalListings() (*big.Int, error) {
	return _Partyanimals0.Contract.TotalListings(&_Partyanimals0.CallOpts)
}

// TotalSales is a free data retrieval call binding the contract method 0x6605ff66.
//
// Solidity: function totalSales() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Caller) TotalSales(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "totalSales")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalSales is a free data retrieval call binding the contract method 0x6605ff66.
//
// Solidity: function totalSales() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Session) TotalSales() (*big.Int, error) {
	return _Partyanimals0.Contract.TotalSales(&_Partyanimals0.CallOpts)
}

// TotalSales is a free data retrieval call binding the contract method 0x6605ff66.
//
// Solidity: function totalSales() view returns(uint256)
func (_Partyanimals0 *Partyanimals0CallerSession) TotalSales() (*big.Int, error) {
	return _Partyanimals0.Contract.TotalSales(&_Partyanimals0.CallOpts)
}

// TotalVolume is a free data retrieval call binding the contract method 0x5f81a57c.
//
// Solidity: function totalVolume() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Caller) TotalVolume(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "totalVolume")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalVolume is a free data retrieval call binding the contract method 0x5f81a57c.
//
// Solidity: function totalVolume() view returns(uint256)
func (_Partyanimals0 *Partyanimals0Session) TotalVolume() (*big.Int, error) {
	return _Partyanimals0.Contract.TotalVolume(&_Partyanimals0.CallOpts)
}

// TotalVolume is a free data retrieval call binding the contract method 0x5f81a57c.
//
// Solidity: function totalVolume() view returns(uint256)
func (_Partyanimals0 *Partyanimals0CallerSession) TotalVolume() (*big.Int, error) {
	return _Partyanimals0.Contract.TotalVolume(&_Partyanimals0.CallOpts)
}

// UserActiveListings is a free data retrieval call binding the contract method 0xb2245558.
//
// Solidity: function userActiveListings(address , uint256 ) view returns(uint256)
func (_Partyanimals0 *Partyanimals0Caller) UserActiveListings(opts *bind.CallOpts, arg0 common.Address, arg1 *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "userActiveListings", arg0, arg1)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// UserActiveListings is a free data retrieval call binding the contract method 0xb2245558.
//
// Solidity: function userActiveListings(address , uint256 ) view returns(uint256)
func (_Partyanimals0 *Partyanimals0Session) UserActiveListings(arg0 common.Address, arg1 *big.Int) (*big.Int, error) {
	return _Partyanimals0.Contract.UserActiveListings(&_Partyanimals0.CallOpts, arg0, arg1)
}

// UserActiveListings is a free data retrieval call binding the contract method 0xb2245558.
//
// Solidity: function userActiveListings(address , uint256 ) view returns(uint256)
func (_Partyanimals0 *Partyanimals0CallerSession) UserActiveListings(arg0 common.Address, arg1 *big.Int) (*big.Int, error) {
	return _Partyanimals0.Contract.UserActiveListings(&_Partyanimals0.CallOpts, arg0, arg1)
}

// WithdrawableBalance is a free data retrieval call binding the contract method 0xe62d64f6.
//
// Solidity: function withdrawableBalance() view returns(uint256 value)
func (_Partyanimals0 *Partyanimals0Caller) WithdrawableBalance(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Partyanimals0.contract.Call(opts, &out, "withdrawableBalance")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// WithdrawableBalance is a free data retrieval call binding the contract method 0xe62d64f6.
//
// Solidity: function withdrawableBalance() view returns(uint256 value)
func (_Partyanimals0 *Partyanimals0Session) WithdrawableBalance() (*big.Int, error) {
	return _Partyanimals0.Contract.WithdrawableBalance(&_Partyanimals0.CallOpts)
}

// WithdrawableBalance is a free data retrieval call binding the contract method 0xe62d64f6.
//
// Solidity: function withdrawableBalance() view returns(uint256 value)
func (_Partyanimals0 *Partyanimals0CallerSession) WithdrawableBalance() (*big.Int, error) {
	return _Partyanimals0.Contract.WithdrawableBalance(&_Partyanimals0.CallOpts)
}

// AddListing is a paid mutator transaction binding the contract method 0x836cf6e9.
//
// Solidity: function addListing(uint256 tokenId, uint256 price) returns()
func (_Partyanimals0 *Partyanimals0Transactor) AddListing(opts *bind.TransactOpts, tokenId *big.Int, price *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "addListing", tokenId, price)
}

// AddListing is a paid mutator transaction binding the contract method 0x836cf6e9.
//
// Solidity: function addListing(uint256 tokenId, uint256 price) returns()
func (_Partyanimals0 *Partyanimals0Session) AddListing(tokenId *big.Int, price *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.AddListing(&_Partyanimals0.TransactOpts, tokenId, price)
}

// AddListing is a paid mutator transaction binding the contract method 0x836cf6e9.
//
// Solidity: function addListing(uint256 tokenId, uint256 price) returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) AddListing(tokenId *big.Int, price *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.AddListing(&_Partyanimals0.TransactOpts, tokenId, price)
}

// AdjustFees is a paid mutator transaction binding the contract method 0xb9d2326e.
//
// Solidity: function adjustFees(uint256 newDistFee, uint256 newMarketFee) returns()
func (_Partyanimals0 *Partyanimals0Transactor) AdjustFees(opts *bind.TransactOpts, newDistFee *big.Int, newMarketFee *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "adjustFees", newDistFee, newMarketFee)
}

// AdjustFees is a paid mutator transaction binding the contract method 0xb9d2326e.
//
// Solidity: function adjustFees(uint256 newDistFee, uint256 newMarketFee) returns()
func (_Partyanimals0 *Partyanimals0Session) AdjustFees(newDistFee *big.Int, newMarketFee *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.AdjustFees(&_Partyanimals0.TransactOpts, newDistFee, newMarketFee)
}

// AdjustFees is a paid mutator transaction binding the contract method 0xb9d2326e.
//
// Solidity: function adjustFees(uint256 newDistFee, uint256 newMarketFee) returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) AdjustFees(newDistFee *big.Int, newMarketFee *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.AdjustFees(&_Partyanimals0.TransactOpts, newDistFee, newMarketFee)
}

// AllowEmergencyDelisting is a paid mutator transaction binding the contract method 0x607133a0.
//
// Solidity: function allowEmergencyDelisting() returns()
func (_Partyanimals0 *Partyanimals0Transactor) AllowEmergencyDelisting(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "allowEmergencyDelisting")
}

// AllowEmergencyDelisting is a paid mutator transaction binding the contract method 0x607133a0.
//
// Solidity: function allowEmergencyDelisting() returns()
func (_Partyanimals0 *Partyanimals0Session) AllowEmergencyDelisting() (*types.Transaction, error) {
	return _Partyanimals0.Contract.AllowEmergencyDelisting(&_Partyanimals0.TransactOpts)
}

// AllowEmergencyDelisting is a paid mutator transaction binding the contract method 0x607133a0.
//
// Solidity: function allowEmergencyDelisting() returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) AllowEmergencyDelisting() (*types.Transaction, error) {
	return _Partyanimals0.Contract.AllowEmergencyDelisting(&_Partyanimals0.TransactOpts)
}

// CancelListing is a paid mutator transaction binding the contract method 0x305a67a8.
//
// Solidity: function cancelListing(uint256 id) returns()
func (_Partyanimals0 *Partyanimals0Transactor) CancelListing(opts *bind.TransactOpts, id *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "cancelListing", id)
}

// CancelListing is a paid mutator transaction binding the contract method 0x305a67a8.
//
// Solidity: function cancelListing(uint256 id) returns()
func (_Partyanimals0 *Partyanimals0Session) CancelListing(id *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.CancelListing(&_Partyanimals0.TransactOpts, id)
}

// CancelListing is a paid mutator transaction binding the contract method 0x305a67a8.
//
// Solidity: function cancelListing(uint256 id) returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) CancelListing(id *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.CancelListing(&_Partyanimals0.TransactOpts, id)
}

// ClaimListedRewards is a paid mutator transaction binding the contract method 0xb59da06b.
//
// Solidity: function claimListedRewards(uint256 from, uint256 length) returns()
func (_Partyanimals0 *Partyanimals0Transactor) ClaimListedRewards(opts *bind.TransactOpts, from *big.Int, length *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "claimListedRewards", from, length)
}

// ClaimListedRewards is a paid mutator transaction binding the contract method 0xb59da06b.
//
// Solidity: function claimListedRewards(uint256 from, uint256 length) returns()
func (_Partyanimals0 *Partyanimals0Session) ClaimListedRewards(from *big.Int, length *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.ClaimListedRewards(&_Partyanimals0.TransactOpts, from, length)
}

// ClaimListedRewards is a paid mutator transaction binding the contract method 0xb59da06b.
//
// Solidity: function claimListedRewards(uint256 from, uint256 length) returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) ClaimListedRewards(from *big.Int, length *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.ClaimListedRewards(&_Partyanimals0.TransactOpts, from, length)
}

// ClaimOwnedRewards is a paid mutator transaction binding the contract method 0x25507cbe.
//
// Solidity: function claimOwnedRewards(uint256 from, uint256 length) returns()
func (_Partyanimals0 *Partyanimals0Transactor) ClaimOwnedRewards(opts *bind.TransactOpts, from *big.Int, length *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "claimOwnedRewards", from, length)
}

// ClaimOwnedRewards is a paid mutator transaction binding the contract method 0x25507cbe.
//
// Solidity: function claimOwnedRewards(uint256 from, uint256 length) returns()
func (_Partyanimals0 *Partyanimals0Session) ClaimOwnedRewards(from *big.Int, length *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.ClaimOwnedRewards(&_Partyanimals0.TransactOpts, from, length)
}

// ClaimOwnedRewards is a paid mutator transaction binding the contract method 0x25507cbe.
//
// Solidity: function claimOwnedRewards(uint256 from, uint256 length) returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) ClaimOwnedRewards(from *big.Int, length *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.ClaimOwnedRewards(&_Partyanimals0.TransactOpts, from, length)
}

// ClaimRewards is a paid mutator transaction binding the contract method 0x372500ab.
//
// Solidity: function claimRewards() returns()
func (_Partyanimals0 *Partyanimals0Transactor) ClaimRewards(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "claimRewards")
}

// ClaimRewards is a paid mutator transaction binding the contract method 0x372500ab.
//
// Solidity: function claimRewards() returns()
func (_Partyanimals0 *Partyanimals0Session) ClaimRewards() (*types.Transaction, error) {
	return _Partyanimals0.Contract.ClaimRewards(&_Partyanimals0.TransactOpts)
}

// ClaimRewards is a paid mutator transaction binding the contract method 0x372500ab.
//
// Solidity: function claimRewards() returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) ClaimRewards() (*types.Transaction, error) {
	return _Partyanimals0.Contract.ClaimRewards(&_Partyanimals0.TransactOpts)
}

// CloseMarket is a paid mutator transaction binding the contract method 0xc511ed5e.
//
// Solidity: function closeMarket() returns()
func (_Partyanimals0 *Partyanimals0Transactor) CloseMarket(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "closeMarket")
}

// CloseMarket is a paid mutator transaction binding the contract method 0xc511ed5e.
//
// Solidity: function closeMarket() returns()
func (_Partyanimals0 *Partyanimals0Session) CloseMarket() (*types.Transaction, error) {
	return _Partyanimals0.Contract.CloseMarket(&_Partyanimals0.TransactOpts)
}

// CloseMarket is a paid mutator transaction binding the contract method 0xc511ed5e.
//
// Solidity: function closeMarket() returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) CloseMarket() (*types.Transaction, error) {
	return _Partyanimals0.Contract.CloseMarket(&_Partyanimals0.TransactOpts)
}

// EmergencyDelist is a paid mutator transaction binding the contract method 0x632541b4.
//
// Solidity: function emergencyDelist(uint256 listingID) returns()
func (_Partyanimals0 *Partyanimals0Transactor) EmergencyDelist(opts *bind.TransactOpts, listingID *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "emergencyDelist", listingID)
}

// EmergencyDelist is a paid mutator transaction binding the contract method 0x632541b4.
//
// Solidity: function emergencyDelist(uint256 listingID) returns()
func (_Partyanimals0 *Partyanimals0Session) EmergencyDelist(listingID *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.EmergencyDelist(&_Partyanimals0.TransactOpts, listingID)
}

// EmergencyDelist is a paid mutator transaction binding the contract method 0x632541b4.
//
// Solidity: function emergencyDelist(uint256 listingID) returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) EmergencyDelist(listingID *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.EmergencyDelist(&_Partyanimals0.TransactOpts, listingID)
}

// EmergencyWithdraw is a paid mutator transaction binding the contract method 0xdb2e21bc.
//
// Solidity: function emergencyWithdraw() returns()
func (_Partyanimals0 *Partyanimals0Transactor) EmergencyWithdraw(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "emergencyWithdraw")
}

// EmergencyWithdraw is a paid mutator transaction binding the contract method 0xdb2e21bc.
//
// Solidity: function emergencyWithdraw() returns()
func (_Partyanimals0 *Partyanimals0Session) EmergencyWithdraw() (*types.Transaction, error) {
	return _Partyanimals0.Contract.EmergencyWithdraw(&_Partyanimals0.TransactOpts)
}

// EmergencyWithdraw is a paid mutator transaction binding the contract method 0xdb2e21bc.
//
// Solidity: function emergencyWithdraw() returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) EmergencyWithdraw() (*types.Transaction, error) {
	return _Partyanimals0.Contract.EmergencyWithdraw(&_Partyanimals0.TransactOpts)
}

// FulfillListing is a paid mutator transaction binding the contract method 0x3912fc85.
//
// Solidity: function fulfillListing(uint256 id) payable returns()
func (_Partyanimals0 *Partyanimals0Transactor) FulfillListing(opts *bind.TransactOpts, id *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "fulfillListing", id)
}

// FulfillListing is a paid mutator transaction binding the contract method 0x3912fc85.
//
// Solidity: function fulfillListing(uint256 id) payable returns()
func (_Partyanimals0 *Partyanimals0Session) FulfillListing(id *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.FulfillListing(&_Partyanimals0.TransactOpts, id)
}

// FulfillListing is a paid mutator transaction binding the contract method 0x3912fc85.
//
// Solidity: function fulfillListing(uint256 id) payable returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) FulfillListing(id *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.FulfillListing(&_Partyanimals0.TransactOpts, id)
}

// OpenMarket is a paid mutator transaction binding the contract method 0x3606f5b9.
//
// Solidity: function openMarket() returns()
func (_Partyanimals0 *Partyanimals0Transactor) OpenMarket(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "openMarket")
}

// OpenMarket is a paid mutator transaction binding the contract method 0x3606f5b9.
//
// Solidity: function openMarket() returns()
func (_Partyanimals0 *Partyanimals0Session) OpenMarket() (*types.Transaction, error) {
	return _Partyanimals0.Contract.OpenMarket(&_Partyanimals0.TransactOpts)
}

// OpenMarket is a paid mutator transaction binding the contract method 0x3606f5b9.
//
// Solidity: function openMarket() returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) OpenMarket() (*types.Transaction, error) {
	return _Partyanimals0.Contract.OpenMarket(&_Partyanimals0.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Partyanimals0 *Partyanimals0Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Partyanimals0 *Partyanimals0Session) RenounceOwnership() (*types.Transaction, error) {
	return _Partyanimals0.Contract.RenounceOwnership(&_Partyanimals0.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Partyanimals0.Contract.RenounceOwnership(&_Partyanimals0.TransactOpts)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Partyanimals0 *Partyanimals0Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Partyanimals0 *Partyanimals0Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Partyanimals0.Contract.TransferOwnership(&_Partyanimals0.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Partyanimals0.Contract.TransferOwnership(&_Partyanimals0.TransactOpts, newOwner)
}

// UpdateListing is a paid mutator transaction binding the contract method 0x0179ea88.
//
// Solidity: function updateListing(uint256 id, uint256 price) returns()
func (_Partyanimals0 *Partyanimals0Transactor) UpdateListing(opts *bind.TransactOpts, id *big.Int, price *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "updateListing", id, price)
}

// UpdateListing is a paid mutator transaction binding the contract method 0x0179ea88.
//
// Solidity: function updateListing(uint256 id, uint256 price) returns()
func (_Partyanimals0 *Partyanimals0Session) UpdateListing(id *big.Int, price *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.UpdateListing(&_Partyanimals0.TransactOpts, id, price)
}

// UpdateListing is a paid mutator transaction binding the contract method 0x0179ea88.
//
// Solidity: function updateListing(uint256 id, uint256 price) returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) UpdateListing(id *big.Int, price *big.Int) (*types.Transaction, error) {
	return _Partyanimals0.Contract.UpdateListing(&_Partyanimals0.TransactOpts, id, price)
}

// WithdrawBalance is a paid mutator transaction binding the contract method 0x5fd8c710.
//
// Solidity: function withdrawBalance() returns()
func (_Partyanimals0 *Partyanimals0Transactor) WithdrawBalance(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Partyanimals0.contract.Transact(opts, "withdrawBalance")
}

// WithdrawBalance is a paid mutator transaction binding the contract method 0x5fd8c710.
//
// Solidity: function withdrawBalance() returns()
func (_Partyanimals0 *Partyanimals0Session) WithdrawBalance() (*types.Transaction, error) {
	return _Partyanimals0.Contract.WithdrawBalance(&_Partyanimals0.TransactOpts)
}

// WithdrawBalance is a paid mutator transaction binding the contract method 0x5fd8c710.
//
// Solidity: function withdrawBalance() returns()
func (_Partyanimals0 *Partyanimals0TransactorSession) WithdrawBalance() (*types.Transaction, error) {
	return _Partyanimals0.Contract.WithdrawBalance(&_Partyanimals0.TransactOpts)
}

// Partyanimals0AddedListingIterator is returned from FilterAddedListing and is used to iterate over the raw logs and unpacked data for AddedListing events raised by the Partyanimals0 contract.
type Partyanimals0AddedListingIterator struct {
	Event *Partyanimals0AddedListing // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Partyanimals0AddedListingIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Partyanimals0AddedListing)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Partyanimals0AddedListing)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Partyanimals0AddedListingIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Partyanimals0AddedListingIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Partyanimals0AddedListing represents a AddedListing event raised by the Partyanimals0 contract.
type Partyanimals0AddedListing struct {
	Listing MarketListing
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterAddedListing is a free log retrieval operation binding the contract event 0xf8566eba27fcd34d4f5b8869ac3ea00164e8ad36697569f3671db38361d1cdb0.
//
// Solidity: event AddedListing((bool,uint256,uint256,uint256,uint256,uint256,address,string) listing)
func (_Partyanimals0 *Partyanimals0Filterer) FilterAddedListing(opts *bind.FilterOpts) (*Partyanimals0AddedListingIterator, error) {

	logs, sub, err := _Partyanimals0.contract.FilterLogs(opts, "AddedListing")
	if err != nil {
		return nil, err
	}
	return &Partyanimals0AddedListingIterator{contract: _Partyanimals0.contract, event: "AddedListing", logs: logs, sub: sub}, nil
}

// WatchAddedListing is a free log subscription operation binding the contract event 0xf8566eba27fcd34d4f5b8869ac3ea00164e8ad36697569f3671db38361d1cdb0.
//
// Solidity: event AddedListing((bool,uint256,uint256,uint256,uint256,uint256,address,string) listing)
func (_Partyanimals0 *Partyanimals0Filterer) WatchAddedListing(opts *bind.WatchOpts, sink chan<- *Partyanimals0AddedListing) (event.Subscription, error) {

	logs, sub, err := _Partyanimals0.contract.WatchLogs(opts, "AddedListing")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Partyanimals0AddedListing)
				if err := _Partyanimals0.contract.UnpackLog(event, "AddedListing", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAddedListing is a log parse operation binding the contract event 0xf8566eba27fcd34d4f5b8869ac3ea00164e8ad36697569f3671db38361d1cdb0.
//
// Solidity: event AddedListing((bool,uint256,uint256,uint256,uint256,uint256,address,string) listing)
func (_Partyanimals0 *Partyanimals0Filterer) ParseAddedListing(log types.Log) (*Partyanimals0AddedListing, error) {
	event := new(Partyanimals0AddedListing)
	if err := _Partyanimals0.contract.UnpackLog(event, "AddedListing", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Partyanimals0CanceledListingIterator is returned from FilterCanceledListing and is used to iterate over the raw logs and unpacked data for CanceledListing events raised by the Partyanimals0 contract.
type Partyanimals0CanceledListingIterator struct {
	Event *Partyanimals0CanceledListing // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Partyanimals0CanceledListingIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Partyanimals0CanceledListing)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Partyanimals0CanceledListing)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Partyanimals0CanceledListingIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Partyanimals0CanceledListingIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Partyanimals0CanceledListing represents a CanceledListing event raised by the Partyanimals0 contract.
type Partyanimals0CanceledListing struct {
	Listing MarketListing
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterCanceledListing is a free log retrieval operation binding the contract event 0xe528a318be90ec19d902d617245e04bad4f16f80e3d921e41e967c502c366bf4.
//
// Solidity: event CanceledListing((bool,uint256,uint256,uint256,uint256,uint256,address,string) listing)
func (_Partyanimals0 *Partyanimals0Filterer) FilterCanceledListing(opts *bind.FilterOpts) (*Partyanimals0CanceledListingIterator, error) {

	logs, sub, err := _Partyanimals0.contract.FilterLogs(opts, "CanceledListing")
	if err != nil {
		return nil, err
	}
	return &Partyanimals0CanceledListingIterator{contract: _Partyanimals0.contract, event: "CanceledListing", logs: logs, sub: sub}, nil
}

// WatchCanceledListing is a free log subscription operation binding the contract event 0xe528a318be90ec19d902d617245e04bad4f16f80e3d921e41e967c502c366bf4.
//
// Solidity: event CanceledListing((bool,uint256,uint256,uint256,uint256,uint256,address,string) listing)
func (_Partyanimals0 *Partyanimals0Filterer) WatchCanceledListing(opts *bind.WatchOpts, sink chan<- *Partyanimals0CanceledListing) (event.Subscription, error) {

	logs, sub, err := _Partyanimals0.contract.WatchLogs(opts, "CanceledListing")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Partyanimals0CanceledListing)
				if err := _Partyanimals0.contract.UnpackLog(event, "CanceledListing", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCanceledListing is a log parse operation binding the contract event 0xe528a318be90ec19d902d617245e04bad4f16f80e3d921e41e967c502c366bf4.
//
// Solidity: event CanceledListing((bool,uint256,uint256,uint256,uint256,uint256,address,string) listing)
func (_Partyanimals0 *Partyanimals0Filterer) ParseCanceledListing(log types.Log) (*Partyanimals0CanceledListing, error) {
	event := new(Partyanimals0CanceledListing)
	if err := _Partyanimals0.contract.UnpackLog(event, "CanceledListing", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Partyanimals0FilledListingIterator is returned from FilterFilledListing and is used to iterate over the raw logs and unpacked data for FilledListing events raised by the Partyanimals0 contract.
type Partyanimals0FilledListingIterator struct {
	Event *Partyanimals0FilledListing // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Partyanimals0FilledListingIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Partyanimals0FilledListing)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Partyanimals0FilledListing)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Partyanimals0FilledListingIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Partyanimals0FilledListingIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Partyanimals0FilledListing represents a FilledListing event raised by the Partyanimals0 contract.
type Partyanimals0FilledListing struct {
	Listing MarketPurchase
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterFilledListing is a free log retrieval operation binding the contract event 0x39291c34aa742133bb74dc9eff45ef96ee9d628242ab8e2bd2dc403f8679661a.
//
// Solidity: event FilledListing(((bool,uint256,uint256,uint256,uint256,uint256,address,string),address) listing)
func (_Partyanimals0 *Partyanimals0Filterer) FilterFilledListing(opts *bind.FilterOpts) (*Partyanimals0FilledListingIterator, error) {

	logs, sub, err := _Partyanimals0.contract.FilterLogs(opts, "FilledListing")
	if err != nil {
		return nil, err
	}
	return &Partyanimals0FilledListingIterator{contract: _Partyanimals0.contract, event: "FilledListing", logs: logs, sub: sub}, nil
}

// WatchFilledListing is a free log subscription operation binding the contract event 0x39291c34aa742133bb74dc9eff45ef96ee9d628242ab8e2bd2dc403f8679661a.
//
// Solidity: event FilledListing(((bool,uint256,uint256,uint256,uint256,uint256,address,string),address) listing)
func (_Partyanimals0 *Partyanimals0Filterer) WatchFilledListing(opts *bind.WatchOpts, sink chan<- *Partyanimals0FilledListing) (event.Subscription, error) {

	logs, sub, err := _Partyanimals0.contract.WatchLogs(opts, "FilledListing")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Partyanimals0FilledListing)
				if err := _Partyanimals0.contract.UnpackLog(event, "FilledListing", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseFilledListing is a log parse operation binding the contract event 0x39291c34aa742133bb74dc9eff45ef96ee9d628242ab8e2bd2dc403f8679661a.
//
// Solidity: event FilledListing(((bool,uint256,uint256,uint256,uint256,uint256,address,string),address) listing)
func (_Partyanimals0 *Partyanimals0Filterer) ParseFilledListing(log types.Log) (*Partyanimals0FilledListing, error) {
	event := new(Partyanimals0FilledListing)
	if err := _Partyanimals0.contract.UnpackLog(event, "FilledListing", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Partyanimals0OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Partyanimals0 contract.
type Partyanimals0OwnershipTransferredIterator struct {
	Event *Partyanimals0OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Partyanimals0OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Partyanimals0OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Partyanimals0OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Partyanimals0OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Partyanimals0OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Partyanimals0OwnershipTransferred represents a OwnershipTransferred event raised by the Partyanimals0 contract.
type Partyanimals0OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Partyanimals0 *Partyanimals0Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Partyanimals0OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Partyanimals0.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Partyanimals0OwnershipTransferredIterator{contract: _Partyanimals0.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Partyanimals0 *Partyanimals0Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Partyanimals0OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Partyanimals0.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Partyanimals0OwnershipTransferred)
				if err := _Partyanimals0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Partyanimals0 *Partyanimals0Filterer) ParseOwnershipTransferred(log types.Log) (*Partyanimals0OwnershipTransferred, error) {
	event := new(Partyanimals0OwnershipTransferred)
	if err := _Partyanimals0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Partyanimals0UpdateListingIterator is returned from FilterUpdateListing and is used to iterate over the raw logs and unpacked data for UpdateListing events raised by the Partyanimals0 contract.
type Partyanimals0UpdateListingIterator struct {
	Event *Partyanimals0UpdateListing // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Partyanimals0UpdateListingIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Partyanimals0UpdateListing)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Partyanimals0UpdateListing)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Partyanimals0UpdateListingIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Partyanimals0UpdateListingIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Partyanimals0UpdateListing represents a UpdateListing event raised by the Partyanimals0 contract.
type Partyanimals0UpdateListing struct {
	Listing MarketListing
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterUpdateListing is a free log retrieval operation binding the contract event 0x6573c92d06624f1550d2e52db2c9ef1d734beffdbb4d55cc366e6a5a4302edbb.
//
// Solidity: event UpdateListing((bool,uint256,uint256,uint256,uint256,uint256,address,string) listing)
func (_Partyanimals0 *Partyanimals0Filterer) FilterUpdateListing(opts *bind.FilterOpts) (*Partyanimals0UpdateListingIterator, error) {

	logs, sub, err := _Partyanimals0.contract.FilterLogs(opts, "UpdateListing")
	if err != nil {
		return nil, err
	}
	return &Partyanimals0UpdateListingIterator{contract: _Partyanimals0.contract, event: "UpdateListing", logs: logs, sub: sub}, nil
}

// WatchUpdateListing is a free log subscription operation binding the contract event 0x6573c92d06624f1550d2e52db2c9ef1d734beffdbb4d55cc366e6a5a4302edbb.
//
// Solidity: event UpdateListing((bool,uint256,uint256,uint256,uint256,uint256,address,string) listing)
func (_Partyanimals0 *Partyanimals0Filterer) WatchUpdateListing(opts *bind.WatchOpts, sink chan<- *Partyanimals0UpdateListing) (event.Subscription, error) {

	logs, sub, err := _Partyanimals0.contract.WatchLogs(opts, "UpdateListing")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Partyanimals0UpdateListing)
				if err := _Partyanimals0.contract.UnpackLog(event, "UpdateListing", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUpdateListing is a log parse operation binding the contract event 0x6573c92d06624f1550d2e52db2c9ef1d734beffdbb4d55cc366e6a5a4302edbb.
//
// Solidity: event UpdateListing((bool,uint256,uint256,uint256,uint256,uint256,address,string) listing)
func (_Partyanimals0 *Partyanimals0Filterer) ParseUpdateListing(log types.Log) (*Partyanimals0UpdateListing, error) {
	event := new(Partyanimals0UpdateListing)
	if err := _Partyanimals0.contract.UnpackLog(event, "UpdateListing", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
