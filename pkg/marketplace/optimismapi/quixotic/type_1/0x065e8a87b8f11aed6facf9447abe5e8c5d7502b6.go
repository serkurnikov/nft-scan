// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package quixotic_1

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// ExchangeV4DutchAuctionOrder is an auto generated low-level Go binding around an user-defined struct.
type ExchangeV4DutchAuctionOrder struct {
	Seller               common.Address
	ContractAddress      common.Address
	TokenId              *big.Int
	StartTime            *big.Int
	EndTime              *big.Int
	StartPrice           *big.Int
	EndPrice             *big.Int
	Quantity             *big.Int
	CreatedAtBlockNumber *big.Int
	PaymentERC20         common.Address
}

// Quixotic1MetaData contains all meta data concerning the Quixotic1 contract.
var Quixotic1MetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"addresspayable\",\"name\":\"buyer\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"BuyOrderFilled\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"addresspayable\",\"name\":\"buyer\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"DutchAuctionFilled\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Paused\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"addresspayable\",\"name\":\"buyer\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"SellOrderFilled\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Unpaused\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startPrice\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endPrice\",\"type\":\"uint256\"}],\"name\":\"calculateCurrentPrice\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"addresspayable\",\"name\":\"buyer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"expiration\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"quantity\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"paymentERC20\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"signature\",\"type\":\"bytes\"}],\"name\":\"cancelBuyOrder\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"addr\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"tokenAddr\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"cancelPreviousSellOrders\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"addresspayable\",\"name\":\"buyer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"expiration\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"quantity\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"paymentERC20\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"signature\",\"type\":\"bytes\"},{\"internalType\":\"addresspayable\",\"name\":\"seller\",\"type\":\"address\"}],\"name\":\"fillBuyOrder\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"addresspayable\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startPrice\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endPrice\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"quantity\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"createdAtBlockNumber\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"paymentERC20\",\"type\":\"address\"}],\"internalType\":\"structExchangeV4.DutchAuctionOrder\",\"name\":\"dutchAuctionOrder\",\"type\":\"tuple\"},{\"internalType\":\"bytes\",\"name\":\"signature\",\"type\":\"bytes\"},{\"internalType\":\"addresspayable\",\"name\":\"buyer\",\"type\":\"address\"}],\"name\":\"fillDutchAuctionOrder\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"addresspayable\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"expiration\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"quantity\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"createdAtBlockNumber\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"paymentERC20\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"signature\",\"type\":\"bytes\"},{\"internalType\":\"addresspayable\",\"name\":\"buyer\",\"type\":\"address\"}],\"name\":\"fillSellOrder\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"}],\"name\":\"getRoyaltyPayoutAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"}],\"name\":\"getRoyaltyPayoutRate\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes\",\"name\":\"signature\",\"type\":\"bytes\"}],\"name\":\"isOrderCancelled\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"pause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"paused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"addresspayable\",\"name\":\"_newMakerWallet\",\"type\":\"address\"}],\"name\":\"setMakerWallet\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_royaltyRegistry\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_cancellationRegistry\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_paymentERC20Registry\",\"type\":\"address\"}],\"name\":\"setRegistryContracts\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"},{\"internalType\":\"addresspayable\",\"name\":\"_payoutAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_payoutPerMille\",\"type\":\"uint256\"}],\"name\":\"setRoyalty\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"unpause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// Quixotic1ABI is the input ABI used to generate the binding from.
// Deprecated: Use Quixotic1MetaData.ABI instead.
var Quixotic1ABI = Quixotic1MetaData.ABI

// Quixotic1 is an auto generated Go binding around an Ethereum contract.
type Quixotic1 struct {
	Quixotic1Caller     // Read-only binding to the contract
	Quixotic1Transactor // Write-only binding to the contract
	Quixotic1Filterer   // Log filterer for contract events
}

// Quixotic1Caller is an auto generated read-only Go binding around an Ethereum contract.
type Quixotic1Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Quixotic1Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Quixotic1Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Quixotic1Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Quixotic1Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Quixotic1Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Quixotic1Session struct {
	Contract     *Quixotic1        // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Quixotic1CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Quixotic1CallerSession struct {
	Contract *Quixotic1Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts    // Call options to use throughout this session
}

// Quixotic1TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Quixotic1TransactorSession struct {
	Contract     *Quixotic1Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts    // Transaction auth options to use throughout this session
}

// Quixotic1Raw is an auto generated low-level Go binding around an Ethereum contract.
type Quixotic1Raw struct {
	Contract *Quixotic1 // Generic contract binding to access the raw methods on
}

// Quixotic1CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Quixotic1CallerRaw struct {
	Contract *Quixotic1Caller // Generic read-only contract binding to access the raw methods on
}

// Quixotic1TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Quixotic1TransactorRaw struct {
	Contract *Quixotic1Transactor // Generic write-only contract binding to access the raw methods on
}

// NewQuixotic1 creates a new instance of Quixotic1, bound to a specific deployed contract.
func NewQuixotic1(address common.Address, backend bind.ContractBackend) (*Quixotic1, error) {
	contract, err := bindQuixotic1(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Quixotic1{Quixotic1Caller: Quixotic1Caller{contract: contract}, Quixotic1Transactor: Quixotic1Transactor{contract: contract}, Quixotic1Filterer: Quixotic1Filterer{contract: contract}}, nil
}

// NewQuixotic1Caller creates a new read-only instance of Quixotic1, bound to a specific deployed contract.
func NewQuixotic1Caller(address common.Address, caller bind.ContractCaller) (*Quixotic1Caller, error) {
	contract, err := bindQuixotic1(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Quixotic1Caller{contract: contract}, nil
}

// NewQuixotic1Transactor creates a new write-only instance of Quixotic1, bound to a specific deployed contract.
func NewQuixotic1Transactor(address common.Address, transactor bind.ContractTransactor) (*Quixotic1Transactor, error) {
	contract, err := bindQuixotic1(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Quixotic1Transactor{contract: contract}, nil
}

// NewQuixotic1Filterer creates a new log filterer instance of Quixotic1, bound to a specific deployed contract.
func NewQuixotic1Filterer(address common.Address, filterer bind.ContractFilterer) (*Quixotic1Filterer, error) {
	contract, err := bindQuixotic1(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Quixotic1Filterer{contract: contract}, nil
}

// bindQuixotic1 binds a generic wrapper to an already deployed contract.
func bindQuixotic1(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Quixotic1ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Quixotic1 *Quixotic1Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Quixotic1.Contract.Quixotic1Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Quixotic1 *Quixotic1Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Quixotic1.Contract.Quixotic1Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Quixotic1 *Quixotic1Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Quixotic1.Contract.Quixotic1Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Quixotic1 *Quixotic1CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Quixotic1.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Quixotic1 *Quixotic1TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Quixotic1.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Quixotic1 *Quixotic1TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Quixotic1.Contract.contract.Transact(opts, method, params...)
}

// CalculateCurrentPrice is a free data retrieval call binding the contract method 0xc3fbdb7c.
//
// Solidity: function calculateCurrentPrice(uint256 startTime, uint256 endTime, uint256 startPrice, uint256 endPrice) view returns(uint256)
func (_Quixotic1 *Quixotic1Caller) CalculateCurrentPrice(opts *bind.CallOpts, startTime *big.Int, endTime *big.Int, startPrice *big.Int, endPrice *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Quixotic1.contract.Call(opts, &out, "calculateCurrentPrice", startTime, endTime, startPrice, endPrice)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CalculateCurrentPrice is a free data retrieval call binding the contract method 0xc3fbdb7c.
//
// Solidity: function calculateCurrentPrice(uint256 startTime, uint256 endTime, uint256 startPrice, uint256 endPrice) view returns(uint256)
func (_Quixotic1 *Quixotic1Session) CalculateCurrentPrice(startTime *big.Int, endTime *big.Int, startPrice *big.Int, endPrice *big.Int) (*big.Int, error) {
	return _Quixotic1.Contract.CalculateCurrentPrice(&_Quixotic1.CallOpts, startTime, endTime, startPrice, endPrice)
}

// CalculateCurrentPrice is a free data retrieval call binding the contract method 0xc3fbdb7c.
//
// Solidity: function calculateCurrentPrice(uint256 startTime, uint256 endTime, uint256 startPrice, uint256 endPrice) view returns(uint256)
func (_Quixotic1 *Quixotic1CallerSession) CalculateCurrentPrice(startTime *big.Int, endTime *big.Int, startPrice *big.Int, endPrice *big.Int) (*big.Int, error) {
	return _Quixotic1.Contract.CalculateCurrentPrice(&_Quixotic1.CallOpts, startTime, endTime, startPrice, endPrice)
}

// GetRoyaltyPayoutAddress is a free data retrieval call binding the contract method 0x2df5cb23.
//
// Solidity: function getRoyaltyPayoutAddress(address contractAddress) view returns(address)
func (_Quixotic1 *Quixotic1Caller) GetRoyaltyPayoutAddress(opts *bind.CallOpts, contractAddress common.Address) (common.Address, error) {
	var out []interface{}
	err := _Quixotic1.contract.Call(opts, &out, "getRoyaltyPayoutAddress", contractAddress)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetRoyaltyPayoutAddress is a free data retrieval call binding the contract method 0x2df5cb23.
//
// Solidity: function getRoyaltyPayoutAddress(address contractAddress) view returns(address)
func (_Quixotic1 *Quixotic1Session) GetRoyaltyPayoutAddress(contractAddress common.Address) (common.Address, error) {
	return _Quixotic1.Contract.GetRoyaltyPayoutAddress(&_Quixotic1.CallOpts, contractAddress)
}

// GetRoyaltyPayoutAddress is a free data retrieval call binding the contract method 0x2df5cb23.
//
// Solidity: function getRoyaltyPayoutAddress(address contractAddress) view returns(address)
func (_Quixotic1 *Quixotic1CallerSession) GetRoyaltyPayoutAddress(contractAddress common.Address) (common.Address, error) {
	return _Quixotic1.Contract.GetRoyaltyPayoutAddress(&_Quixotic1.CallOpts, contractAddress)
}

// GetRoyaltyPayoutRate is a free data retrieval call binding the contract method 0xec4210ff.
//
// Solidity: function getRoyaltyPayoutRate(address contractAddress) view returns(uint256)
func (_Quixotic1 *Quixotic1Caller) GetRoyaltyPayoutRate(opts *bind.CallOpts, contractAddress common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Quixotic1.contract.Call(opts, &out, "getRoyaltyPayoutRate", contractAddress)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetRoyaltyPayoutRate is a free data retrieval call binding the contract method 0xec4210ff.
//
// Solidity: function getRoyaltyPayoutRate(address contractAddress) view returns(uint256)
func (_Quixotic1 *Quixotic1Session) GetRoyaltyPayoutRate(contractAddress common.Address) (*big.Int, error) {
	return _Quixotic1.Contract.GetRoyaltyPayoutRate(&_Quixotic1.CallOpts, contractAddress)
}

// GetRoyaltyPayoutRate is a free data retrieval call binding the contract method 0xec4210ff.
//
// Solidity: function getRoyaltyPayoutRate(address contractAddress) view returns(uint256)
func (_Quixotic1 *Quixotic1CallerSession) GetRoyaltyPayoutRate(contractAddress common.Address) (*big.Int, error) {
	return _Quixotic1.Contract.GetRoyaltyPayoutRate(&_Quixotic1.CallOpts, contractAddress)
}

// IsOrderCancelled is a free data retrieval call binding the contract method 0x563166a9.
//
// Solidity: function isOrderCancelled(bytes signature) view returns(bool)
func (_Quixotic1 *Quixotic1Caller) IsOrderCancelled(opts *bind.CallOpts, signature []byte) (bool, error) {
	var out []interface{}
	err := _Quixotic1.contract.Call(opts, &out, "isOrderCancelled", signature)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsOrderCancelled is a free data retrieval call binding the contract method 0x563166a9.
//
// Solidity: function isOrderCancelled(bytes signature) view returns(bool)
func (_Quixotic1 *Quixotic1Session) IsOrderCancelled(signature []byte) (bool, error) {
	return _Quixotic1.Contract.IsOrderCancelled(&_Quixotic1.CallOpts, signature)
}

// IsOrderCancelled is a free data retrieval call binding the contract method 0x563166a9.
//
// Solidity: function isOrderCancelled(bytes signature) view returns(bool)
func (_Quixotic1 *Quixotic1CallerSession) IsOrderCancelled(signature []byte) (bool, error) {
	return _Quixotic1.Contract.IsOrderCancelled(&_Quixotic1.CallOpts, signature)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Quixotic1 *Quixotic1Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Quixotic1.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Quixotic1 *Quixotic1Session) Owner() (common.Address, error) {
	return _Quixotic1.Contract.Owner(&_Quixotic1.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Quixotic1 *Quixotic1CallerSession) Owner() (common.Address, error) {
	return _Quixotic1.Contract.Owner(&_Quixotic1.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Quixotic1 *Quixotic1Caller) Paused(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Quixotic1.contract.Call(opts, &out, "paused")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Quixotic1 *Quixotic1Session) Paused() (bool, error) {
	return _Quixotic1.Contract.Paused(&_Quixotic1.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Quixotic1 *Quixotic1CallerSession) Paused() (bool, error) {
	return _Quixotic1.Contract.Paused(&_Quixotic1.CallOpts)
}

// CancelBuyOrder is a paid mutator transaction binding the contract method 0x0b83196e.
//
// Solidity: function cancelBuyOrder(address buyer, address contractAddress, uint256 tokenId, uint256 startTime, uint256 expiration, uint256 price, uint256 quantity, address paymentERC20, bytes signature) returns()
func (_Quixotic1 *Quixotic1Transactor) CancelBuyOrder(opts *bind.TransactOpts, buyer common.Address, contractAddress common.Address, tokenId *big.Int, startTime *big.Int, expiration *big.Int, price *big.Int, quantity *big.Int, paymentERC20 common.Address, signature []byte) (*types.Transaction, error) {
	return _Quixotic1.contract.Transact(opts, "cancelBuyOrder", buyer, contractAddress, tokenId, startTime, expiration, price, quantity, paymentERC20, signature)
}

// CancelBuyOrder is a paid mutator transaction binding the contract method 0x0b83196e.
//
// Solidity: function cancelBuyOrder(address buyer, address contractAddress, uint256 tokenId, uint256 startTime, uint256 expiration, uint256 price, uint256 quantity, address paymentERC20, bytes signature) returns()
func (_Quixotic1 *Quixotic1Session) CancelBuyOrder(buyer common.Address, contractAddress common.Address, tokenId *big.Int, startTime *big.Int, expiration *big.Int, price *big.Int, quantity *big.Int, paymentERC20 common.Address, signature []byte) (*types.Transaction, error) {
	return _Quixotic1.Contract.CancelBuyOrder(&_Quixotic1.TransactOpts, buyer, contractAddress, tokenId, startTime, expiration, price, quantity, paymentERC20, signature)
}

// CancelBuyOrder is a paid mutator transaction binding the contract method 0x0b83196e.
//
// Solidity: function cancelBuyOrder(address buyer, address contractAddress, uint256 tokenId, uint256 startTime, uint256 expiration, uint256 price, uint256 quantity, address paymentERC20, bytes signature) returns()
func (_Quixotic1 *Quixotic1TransactorSession) CancelBuyOrder(buyer common.Address, contractAddress common.Address, tokenId *big.Int, startTime *big.Int, expiration *big.Int, price *big.Int, quantity *big.Int, paymentERC20 common.Address, signature []byte) (*types.Transaction, error) {
	return _Quixotic1.Contract.CancelBuyOrder(&_Quixotic1.TransactOpts, buyer, contractAddress, tokenId, startTime, expiration, price, quantity, paymentERC20, signature)
}

// CancelPreviousSellOrders is a paid mutator transaction binding the contract method 0x083d089d.
//
// Solidity: function cancelPreviousSellOrders(address addr, address tokenAddr, uint256 tokenId) returns()
func (_Quixotic1 *Quixotic1Transactor) CancelPreviousSellOrders(opts *bind.TransactOpts, addr common.Address, tokenAddr common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Quixotic1.contract.Transact(opts, "cancelPreviousSellOrders", addr, tokenAddr, tokenId)
}

// CancelPreviousSellOrders is a paid mutator transaction binding the contract method 0x083d089d.
//
// Solidity: function cancelPreviousSellOrders(address addr, address tokenAddr, uint256 tokenId) returns()
func (_Quixotic1 *Quixotic1Session) CancelPreviousSellOrders(addr common.Address, tokenAddr common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Quixotic1.Contract.CancelPreviousSellOrders(&_Quixotic1.TransactOpts, addr, tokenAddr, tokenId)
}

// CancelPreviousSellOrders is a paid mutator transaction binding the contract method 0x083d089d.
//
// Solidity: function cancelPreviousSellOrders(address addr, address tokenAddr, uint256 tokenId) returns()
func (_Quixotic1 *Quixotic1TransactorSession) CancelPreviousSellOrders(addr common.Address, tokenAddr common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Quixotic1.Contract.CancelPreviousSellOrders(&_Quixotic1.TransactOpts, addr, tokenAddr, tokenId)
}

// FillBuyOrder is a paid mutator transaction binding the contract method 0xf8056016.
//
// Solidity: function fillBuyOrder(address buyer, address contractAddress, uint256 tokenId, uint256 startTime, uint256 expiration, uint256 price, uint256 quantity, address paymentERC20, bytes signature, address seller) payable returns()
func (_Quixotic1 *Quixotic1Transactor) FillBuyOrder(opts *bind.TransactOpts, buyer common.Address, contractAddress common.Address, tokenId *big.Int, startTime *big.Int, expiration *big.Int, price *big.Int, quantity *big.Int, paymentERC20 common.Address, signature []byte, seller common.Address) (*types.Transaction, error) {
	return _Quixotic1.contract.Transact(opts, "fillBuyOrder", buyer, contractAddress, tokenId, startTime, expiration, price, quantity, paymentERC20, signature, seller)
}

// FillBuyOrder is a paid mutator transaction binding the contract method 0xf8056016.
//
// Solidity: function fillBuyOrder(address buyer, address contractAddress, uint256 tokenId, uint256 startTime, uint256 expiration, uint256 price, uint256 quantity, address paymentERC20, bytes signature, address seller) payable returns()
func (_Quixotic1 *Quixotic1Session) FillBuyOrder(buyer common.Address, contractAddress common.Address, tokenId *big.Int, startTime *big.Int, expiration *big.Int, price *big.Int, quantity *big.Int, paymentERC20 common.Address, signature []byte, seller common.Address) (*types.Transaction, error) {
	return _Quixotic1.Contract.FillBuyOrder(&_Quixotic1.TransactOpts, buyer, contractAddress, tokenId, startTime, expiration, price, quantity, paymentERC20, signature, seller)
}

// FillBuyOrder is a paid mutator transaction binding the contract method 0xf8056016.
//
// Solidity: function fillBuyOrder(address buyer, address contractAddress, uint256 tokenId, uint256 startTime, uint256 expiration, uint256 price, uint256 quantity, address paymentERC20, bytes signature, address seller) payable returns()
func (_Quixotic1 *Quixotic1TransactorSession) FillBuyOrder(buyer common.Address, contractAddress common.Address, tokenId *big.Int, startTime *big.Int, expiration *big.Int, price *big.Int, quantity *big.Int, paymentERC20 common.Address, signature []byte, seller common.Address) (*types.Transaction, error) {
	return _Quixotic1.Contract.FillBuyOrder(&_Quixotic1.TransactOpts, buyer, contractAddress, tokenId, startTime, expiration, price, quantity, paymentERC20, signature, seller)
}

// FillDutchAuctionOrder is a paid mutator transaction binding the contract method 0x75eeb98a.
//
// Solidity: function fillDutchAuctionOrder((address,address,uint256,uint256,uint256,uint256,uint256,uint256,uint256,address) dutchAuctionOrder, bytes signature, address buyer) payable returns()
func (_Quixotic1 *Quixotic1Transactor) FillDutchAuctionOrder(opts *bind.TransactOpts, dutchAuctionOrder ExchangeV4DutchAuctionOrder, signature []byte, buyer common.Address) (*types.Transaction, error) {
	return _Quixotic1.contract.Transact(opts, "fillDutchAuctionOrder", dutchAuctionOrder, signature, buyer)
}

// FillDutchAuctionOrder is a paid mutator transaction binding the contract method 0x75eeb98a.
//
// Solidity: function fillDutchAuctionOrder((address,address,uint256,uint256,uint256,uint256,uint256,uint256,uint256,address) dutchAuctionOrder, bytes signature, address buyer) payable returns()
func (_Quixotic1 *Quixotic1Session) FillDutchAuctionOrder(dutchAuctionOrder ExchangeV4DutchAuctionOrder, signature []byte, buyer common.Address) (*types.Transaction, error) {
	return _Quixotic1.Contract.FillDutchAuctionOrder(&_Quixotic1.TransactOpts, dutchAuctionOrder, signature, buyer)
}

// FillDutchAuctionOrder is a paid mutator transaction binding the contract method 0x75eeb98a.
//
// Solidity: function fillDutchAuctionOrder((address,address,uint256,uint256,uint256,uint256,uint256,uint256,uint256,address) dutchAuctionOrder, bytes signature, address buyer) payable returns()
func (_Quixotic1 *Quixotic1TransactorSession) FillDutchAuctionOrder(dutchAuctionOrder ExchangeV4DutchAuctionOrder, signature []byte, buyer common.Address) (*types.Transaction, error) {
	return _Quixotic1.Contract.FillDutchAuctionOrder(&_Quixotic1.TransactOpts, dutchAuctionOrder, signature, buyer)
}

// FillSellOrder is a paid mutator transaction binding the contract method 0x912d97fc.
//
// Solidity: function fillSellOrder(address seller, address contractAddress, uint256 tokenId, uint256 startTime, uint256 expiration, uint256 price, uint256 quantity, uint256 createdAtBlockNumber, address paymentERC20, bytes signature, address buyer) payable returns()
func (_Quixotic1 *Quixotic1Transactor) FillSellOrder(opts *bind.TransactOpts, seller common.Address, contractAddress common.Address, tokenId *big.Int, startTime *big.Int, expiration *big.Int, price *big.Int, quantity *big.Int, createdAtBlockNumber *big.Int, paymentERC20 common.Address, signature []byte, buyer common.Address) (*types.Transaction, error) {
	return _Quixotic1.contract.Transact(opts, "fillSellOrder", seller, contractAddress, tokenId, startTime, expiration, price, quantity, createdAtBlockNumber, paymentERC20, signature, buyer)
}

// FillSellOrder is a paid mutator transaction binding the contract method 0x912d97fc.
//
// Solidity: function fillSellOrder(address seller, address contractAddress, uint256 tokenId, uint256 startTime, uint256 expiration, uint256 price, uint256 quantity, uint256 createdAtBlockNumber, address paymentERC20, bytes signature, address buyer) payable returns()
func (_Quixotic1 *Quixotic1Session) FillSellOrder(seller common.Address, contractAddress common.Address, tokenId *big.Int, startTime *big.Int, expiration *big.Int, price *big.Int, quantity *big.Int, createdAtBlockNumber *big.Int, paymentERC20 common.Address, signature []byte, buyer common.Address) (*types.Transaction, error) {
	return _Quixotic1.Contract.FillSellOrder(&_Quixotic1.TransactOpts, seller, contractAddress, tokenId, startTime, expiration, price, quantity, createdAtBlockNumber, paymentERC20, signature, buyer)
}

// FillSellOrder is a paid mutator transaction binding the contract method 0x912d97fc.
//
// Solidity: function fillSellOrder(address seller, address contractAddress, uint256 tokenId, uint256 startTime, uint256 expiration, uint256 price, uint256 quantity, uint256 createdAtBlockNumber, address paymentERC20, bytes signature, address buyer) payable returns()
func (_Quixotic1 *Quixotic1TransactorSession) FillSellOrder(seller common.Address, contractAddress common.Address, tokenId *big.Int, startTime *big.Int, expiration *big.Int, price *big.Int, quantity *big.Int, createdAtBlockNumber *big.Int, paymentERC20 common.Address, signature []byte, buyer common.Address) (*types.Transaction, error) {
	return _Quixotic1.Contract.FillSellOrder(&_Quixotic1.TransactOpts, seller, contractAddress, tokenId, startTime, expiration, price, quantity, createdAtBlockNumber, paymentERC20, signature, buyer)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_Quixotic1 *Quixotic1Transactor) Pause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Quixotic1.contract.Transact(opts, "pause")
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_Quixotic1 *Quixotic1Session) Pause() (*types.Transaction, error) {
	return _Quixotic1.Contract.Pause(&_Quixotic1.TransactOpts)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_Quixotic1 *Quixotic1TransactorSession) Pause() (*types.Transaction, error) {
	return _Quixotic1.Contract.Pause(&_Quixotic1.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Quixotic1 *Quixotic1Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Quixotic1.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Quixotic1 *Quixotic1Session) RenounceOwnership() (*types.Transaction, error) {
	return _Quixotic1.Contract.RenounceOwnership(&_Quixotic1.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Quixotic1 *Quixotic1TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Quixotic1.Contract.RenounceOwnership(&_Quixotic1.TransactOpts)
}

// SetMakerWallet is a paid mutator transaction binding the contract method 0x2b812a34.
//
// Solidity: function setMakerWallet(address _newMakerWallet) returns()
func (_Quixotic1 *Quixotic1Transactor) SetMakerWallet(opts *bind.TransactOpts, _newMakerWallet common.Address) (*types.Transaction, error) {
	return _Quixotic1.contract.Transact(opts, "setMakerWallet", _newMakerWallet)
}

// SetMakerWallet is a paid mutator transaction binding the contract method 0x2b812a34.
//
// Solidity: function setMakerWallet(address _newMakerWallet) returns()
func (_Quixotic1 *Quixotic1Session) SetMakerWallet(_newMakerWallet common.Address) (*types.Transaction, error) {
	return _Quixotic1.Contract.SetMakerWallet(&_Quixotic1.TransactOpts, _newMakerWallet)
}

// SetMakerWallet is a paid mutator transaction binding the contract method 0x2b812a34.
//
// Solidity: function setMakerWallet(address _newMakerWallet) returns()
func (_Quixotic1 *Quixotic1TransactorSession) SetMakerWallet(_newMakerWallet common.Address) (*types.Transaction, error) {
	return _Quixotic1.Contract.SetMakerWallet(&_Quixotic1.TransactOpts, _newMakerWallet)
}

// SetRegistryContracts is a paid mutator transaction binding the contract method 0x47871465.
//
// Solidity: function setRegistryContracts(address _royaltyRegistry, address _cancellationRegistry, address _paymentERC20Registry) returns()
func (_Quixotic1 *Quixotic1Transactor) SetRegistryContracts(opts *bind.TransactOpts, _royaltyRegistry common.Address, _cancellationRegistry common.Address, _paymentERC20Registry common.Address) (*types.Transaction, error) {
	return _Quixotic1.contract.Transact(opts, "setRegistryContracts", _royaltyRegistry, _cancellationRegistry, _paymentERC20Registry)
}

// SetRegistryContracts is a paid mutator transaction binding the contract method 0x47871465.
//
// Solidity: function setRegistryContracts(address _royaltyRegistry, address _cancellationRegistry, address _paymentERC20Registry) returns()
func (_Quixotic1 *Quixotic1Session) SetRegistryContracts(_royaltyRegistry common.Address, _cancellationRegistry common.Address, _paymentERC20Registry common.Address) (*types.Transaction, error) {
	return _Quixotic1.Contract.SetRegistryContracts(&_Quixotic1.TransactOpts, _royaltyRegistry, _cancellationRegistry, _paymentERC20Registry)
}

// SetRegistryContracts is a paid mutator transaction binding the contract method 0x47871465.
//
// Solidity: function setRegistryContracts(address _royaltyRegistry, address _cancellationRegistry, address _paymentERC20Registry) returns()
func (_Quixotic1 *Quixotic1TransactorSession) SetRegistryContracts(_royaltyRegistry common.Address, _cancellationRegistry common.Address, _paymentERC20Registry common.Address) (*types.Transaction, error) {
	return _Quixotic1.Contract.SetRegistryContracts(&_Quixotic1.TransactOpts, _royaltyRegistry, _cancellationRegistry, _paymentERC20Registry)
}

// SetRoyalty is a paid mutator transaction binding the contract method 0x55c338aa.
//
// Solidity: function setRoyalty(address contractAddress, address _payoutAddress, uint256 _payoutPerMille) returns()
func (_Quixotic1 *Quixotic1Transactor) SetRoyalty(opts *bind.TransactOpts, contractAddress common.Address, _payoutAddress common.Address, _payoutPerMille *big.Int) (*types.Transaction, error) {
	return _Quixotic1.contract.Transact(opts, "setRoyalty", contractAddress, _payoutAddress, _payoutPerMille)
}

// SetRoyalty is a paid mutator transaction binding the contract method 0x55c338aa.
//
// Solidity: function setRoyalty(address contractAddress, address _payoutAddress, uint256 _payoutPerMille) returns()
func (_Quixotic1 *Quixotic1Session) SetRoyalty(contractAddress common.Address, _payoutAddress common.Address, _payoutPerMille *big.Int) (*types.Transaction, error) {
	return _Quixotic1.Contract.SetRoyalty(&_Quixotic1.TransactOpts, contractAddress, _payoutAddress, _payoutPerMille)
}

// SetRoyalty is a paid mutator transaction binding the contract method 0x55c338aa.
//
// Solidity: function setRoyalty(address contractAddress, address _payoutAddress, uint256 _payoutPerMille) returns()
func (_Quixotic1 *Quixotic1TransactorSession) SetRoyalty(contractAddress common.Address, _payoutAddress common.Address, _payoutPerMille *big.Int) (*types.Transaction, error) {
	return _Quixotic1.Contract.SetRoyalty(&_Quixotic1.TransactOpts, contractAddress, _payoutAddress, _payoutPerMille)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Quixotic1 *Quixotic1Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Quixotic1.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Quixotic1 *Quixotic1Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Quixotic1.Contract.TransferOwnership(&_Quixotic1.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Quixotic1 *Quixotic1TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Quixotic1.Contract.TransferOwnership(&_Quixotic1.TransactOpts, newOwner)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_Quixotic1 *Quixotic1Transactor) Unpause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Quixotic1.contract.Transact(opts, "unpause")
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_Quixotic1 *Quixotic1Session) Unpause() (*types.Transaction, error) {
	return _Quixotic1.Contract.Unpause(&_Quixotic1.TransactOpts)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_Quixotic1 *Quixotic1TransactorSession) Unpause() (*types.Transaction, error) {
	return _Quixotic1.Contract.Unpause(&_Quixotic1.TransactOpts)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_Quixotic1 *Quixotic1Transactor) Withdraw(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Quixotic1.contract.Transact(opts, "withdraw")
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_Quixotic1 *Quixotic1Session) Withdraw() (*types.Transaction, error) {
	return _Quixotic1.Contract.Withdraw(&_Quixotic1.TransactOpts)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_Quixotic1 *Quixotic1TransactorSession) Withdraw() (*types.Transaction, error) {
	return _Quixotic1.Contract.Withdraw(&_Quixotic1.TransactOpts)
}

// Quixotic1BuyOrderFilledIterator is returned from FilterBuyOrderFilled and is used to iterate over the raw logs and unpacked data for BuyOrderFilled events raised by the Quixotic1 contract.
type Quixotic1BuyOrderFilledIterator struct {
	Event *Quixotic1BuyOrderFilled // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Quixotic1BuyOrderFilledIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Quixotic1BuyOrderFilled)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Quixotic1BuyOrderFilled)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Quixotic1BuyOrderFilledIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Quixotic1BuyOrderFilledIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Quixotic1BuyOrderFilled represents a BuyOrderFilled event raised by the Quixotic1 contract.
type Quixotic1BuyOrderFilled struct {
	Seller          common.Address
	Buyer           common.Address
	ContractAddress common.Address
	TokenId         *big.Int
	Price           *big.Int
	Raw             types.Log // Blockchain specific contextual infos
}

// FilterBuyOrderFilled is a free log retrieval operation binding the contract event 0xcb3cd529428badade171c8b0ef6c2f25d13bc69785143c43ec869d386ae34141.
//
// Solidity: event BuyOrderFilled(address indexed seller, address buyer, address indexed contractAddress, uint256 indexed tokenId, uint256 price)
func (_Quixotic1 *Quixotic1Filterer) FilterBuyOrderFilled(opts *bind.FilterOpts, seller []common.Address, contractAddress []common.Address, tokenId []*big.Int) (*Quixotic1BuyOrderFilledIterator, error) {

	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	var contractAddressRule []interface{}
	for _, contractAddressItem := range contractAddress {
		contractAddressRule = append(contractAddressRule, contractAddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Quixotic1.contract.FilterLogs(opts, "BuyOrderFilled", sellerRule, contractAddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Quixotic1BuyOrderFilledIterator{contract: _Quixotic1.contract, event: "BuyOrderFilled", logs: logs, sub: sub}, nil
}

// WatchBuyOrderFilled is a free log subscription operation binding the contract event 0xcb3cd529428badade171c8b0ef6c2f25d13bc69785143c43ec869d386ae34141.
//
// Solidity: event BuyOrderFilled(address indexed seller, address buyer, address indexed contractAddress, uint256 indexed tokenId, uint256 price)
func (_Quixotic1 *Quixotic1Filterer) WatchBuyOrderFilled(opts *bind.WatchOpts, sink chan<- *Quixotic1BuyOrderFilled, seller []common.Address, contractAddress []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	var contractAddressRule []interface{}
	for _, contractAddressItem := range contractAddress {
		contractAddressRule = append(contractAddressRule, contractAddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Quixotic1.contract.WatchLogs(opts, "BuyOrderFilled", sellerRule, contractAddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Quixotic1BuyOrderFilled)
				if err := _Quixotic1.contract.UnpackLog(event, "BuyOrderFilled", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBuyOrderFilled is a log parse operation binding the contract event 0xcb3cd529428badade171c8b0ef6c2f25d13bc69785143c43ec869d386ae34141.
//
// Solidity: event BuyOrderFilled(address indexed seller, address buyer, address indexed contractAddress, uint256 indexed tokenId, uint256 price)
func (_Quixotic1 *Quixotic1Filterer) ParseBuyOrderFilled(log types.Log) (*Quixotic1BuyOrderFilled, error) {
	event := new(Quixotic1BuyOrderFilled)
	if err := _Quixotic1.contract.UnpackLog(event, "BuyOrderFilled", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Quixotic1DutchAuctionFilledIterator is returned from FilterDutchAuctionFilled and is used to iterate over the raw logs and unpacked data for DutchAuctionFilled events raised by the Quixotic1 contract.
type Quixotic1DutchAuctionFilledIterator struct {
	Event *Quixotic1DutchAuctionFilled // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Quixotic1DutchAuctionFilledIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Quixotic1DutchAuctionFilled)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Quixotic1DutchAuctionFilled)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Quixotic1DutchAuctionFilledIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Quixotic1DutchAuctionFilledIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Quixotic1DutchAuctionFilled represents a DutchAuctionFilled event raised by the Quixotic1 contract.
type Quixotic1DutchAuctionFilled struct {
	Seller          common.Address
	Buyer           common.Address
	ContractAddress common.Address
	TokenId         *big.Int
	Price           *big.Int
	Raw             types.Log // Blockchain specific contextual infos
}

// FilterDutchAuctionFilled is a free log retrieval operation binding the contract event 0xcd2ac775eba6d68eb3bdbba5c500751ed102e5d2b7bf9c459b3e7b808d132cbd.
//
// Solidity: event DutchAuctionFilled(address indexed seller, address buyer, address indexed contractAddress, uint256 indexed tokenId, uint256 price)
func (_Quixotic1 *Quixotic1Filterer) FilterDutchAuctionFilled(opts *bind.FilterOpts, seller []common.Address, contractAddress []common.Address, tokenId []*big.Int) (*Quixotic1DutchAuctionFilledIterator, error) {

	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	var contractAddressRule []interface{}
	for _, contractAddressItem := range contractAddress {
		contractAddressRule = append(contractAddressRule, contractAddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Quixotic1.contract.FilterLogs(opts, "DutchAuctionFilled", sellerRule, contractAddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Quixotic1DutchAuctionFilledIterator{contract: _Quixotic1.contract, event: "DutchAuctionFilled", logs: logs, sub: sub}, nil
}

// WatchDutchAuctionFilled is a free log subscription operation binding the contract event 0xcd2ac775eba6d68eb3bdbba5c500751ed102e5d2b7bf9c459b3e7b808d132cbd.
//
// Solidity: event DutchAuctionFilled(address indexed seller, address buyer, address indexed contractAddress, uint256 indexed tokenId, uint256 price)
func (_Quixotic1 *Quixotic1Filterer) WatchDutchAuctionFilled(opts *bind.WatchOpts, sink chan<- *Quixotic1DutchAuctionFilled, seller []common.Address, contractAddress []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	var contractAddressRule []interface{}
	for _, contractAddressItem := range contractAddress {
		contractAddressRule = append(contractAddressRule, contractAddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Quixotic1.contract.WatchLogs(opts, "DutchAuctionFilled", sellerRule, contractAddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Quixotic1DutchAuctionFilled)
				if err := _Quixotic1.contract.UnpackLog(event, "DutchAuctionFilled", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseDutchAuctionFilled is a log parse operation binding the contract event 0xcd2ac775eba6d68eb3bdbba5c500751ed102e5d2b7bf9c459b3e7b808d132cbd.
//
// Solidity: event DutchAuctionFilled(address indexed seller, address buyer, address indexed contractAddress, uint256 indexed tokenId, uint256 price)
func (_Quixotic1 *Quixotic1Filterer) ParseDutchAuctionFilled(log types.Log) (*Quixotic1DutchAuctionFilled, error) {
	event := new(Quixotic1DutchAuctionFilled)
	if err := _Quixotic1.contract.UnpackLog(event, "DutchAuctionFilled", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Quixotic1OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Quixotic1 contract.
type Quixotic1OwnershipTransferredIterator struct {
	Event *Quixotic1OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Quixotic1OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Quixotic1OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Quixotic1OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Quixotic1OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Quixotic1OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Quixotic1OwnershipTransferred represents a OwnershipTransferred event raised by the Quixotic1 contract.
type Quixotic1OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Quixotic1 *Quixotic1Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Quixotic1OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Quixotic1.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Quixotic1OwnershipTransferredIterator{contract: _Quixotic1.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Quixotic1 *Quixotic1Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Quixotic1OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Quixotic1.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Quixotic1OwnershipTransferred)
				if err := _Quixotic1.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Quixotic1 *Quixotic1Filterer) ParseOwnershipTransferred(log types.Log) (*Quixotic1OwnershipTransferred, error) {
	event := new(Quixotic1OwnershipTransferred)
	if err := _Quixotic1.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Quixotic1PausedIterator is returned from FilterPaused and is used to iterate over the raw logs and unpacked data for Paused events raised by the Quixotic1 contract.
type Quixotic1PausedIterator struct {
	Event *Quixotic1Paused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Quixotic1PausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Quixotic1Paused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Quixotic1Paused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Quixotic1PausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Quixotic1PausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Quixotic1Paused represents a Paused event raised by the Quixotic1 contract.
type Quixotic1Paused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterPaused is a free log retrieval operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_Quixotic1 *Quixotic1Filterer) FilterPaused(opts *bind.FilterOpts) (*Quixotic1PausedIterator, error) {

	logs, sub, err := _Quixotic1.contract.FilterLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return &Quixotic1PausedIterator{contract: _Quixotic1.contract, event: "Paused", logs: logs, sub: sub}, nil
}

// WatchPaused is a free log subscription operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_Quixotic1 *Quixotic1Filterer) WatchPaused(opts *bind.WatchOpts, sink chan<- *Quixotic1Paused) (event.Subscription, error) {

	logs, sub, err := _Quixotic1.contract.WatchLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Quixotic1Paused)
				if err := _Quixotic1.contract.UnpackLog(event, "Paused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePaused is a log parse operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_Quixotic1 *Quixotic1Filterer) ParsePaused(log types.Log) (*Quixotic1Paused, error) {
	event := new(Quixotic1Paused)
	if err := _Quixotic1.contract.UnpackLog(event, "Paused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Quixotic1SellOrderFilledIterator is returned from FilterSellOrderFilled and is used to iterate over the raw logs and unpacked data for SellOrderFilled events raised by the Quixotic1 contract.
type Quixotic1SellOrderFilledIterator struct {
	Event *Quixotic1SellOrderFilled // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Quixotic1SellOrderFilledIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Quixotic1SellOrderFilled)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Quixotic1SellOrderFilled)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Quixotic1SellOrderFilledIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Quixotic1SellOrderFilledIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Quixotic1SellOrderFilled represents a SellOrderFilled event raised by the Quixotic1 contract.
type Quixotic1SellOrderFilled struct {
	Seller          common.Address
	Buyer           common.Address
	ContractAddress common.Address
	TokenId         *big.Int
	Price           *big.Int
	Raw             types.Log // Blockchain specific contextual infos
}

// FilterSellOrderFilled is a free log retrieval operation binding the contract event 0x70ba0d31158674eea8365d0f7b9ac70e552cc28b8bb848664e4feb939c6578f8.
//
// Solidity: event SellOrderFilled(address indexed seller, address buyer, address indexed contractAddress, uint256 indexed tokenId, uint256 price)
func (_Quixotic1 *Quixotic1Filterer) FilterSellOrderFilled(opts *bind.FilterOpts, seller []common.Address, contractAddress []common.Address, tokenId []*big.Int) (*Quixotic1SellOrderFilledIterator, error) {

	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	var contractAddressRule []interface{}
	for _, contractAddressItem := range contractAddress {
		contractAddressRule = append(contractAddressRule, contractAddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Quixotic1.contract.FilterLogs(opts, "SellOrderFilled", sellerRule, contractAddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Quixotic1SellOrderFilledIterator{contract: _Quixotic1.contract, event: "SellOrderFilled", logs: logs, sub: sub}, nil
}

// WatchSellOrderFilled is a free log subscription operation binding the contract event 0x70ba0d31158674eea8365d0f7b9ac70e552cc28b8bb848664e4feb939c6578f8.
//
// Solidity: event SellOrderFilled(address indexed seller, address buyer, address indexed contractAddress, uint256 indexed tokenId, uint256 price)
func (_Quixotic1 *Quixotic1Filterer) WatchSellOrderFilled(opts *bind.WatchOpts, sink chan<- *Quixotic1SellOrderFilled, seller []common.Address, contractAddress []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	var contractAddressRule []interface{}
	for _, contractAddressItem := range contractAddress {
		contractAddressRule = append(contractAddressRule, contractAddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Quixotic1.contract.WatchLogs(opts, "SellOrderFilled", sellerRule, contractAddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Quixotic1SellOrderFilled)
				if err := _Quixotic1.contract.UnpackLog(event, "SellOrderFilled", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseSellOrderFilled is a log parse operation binding the contract event 0x70ba0d31158674eea8365d0f7b9ac70e552cc28b8bb848664e4feb939c6578f8.
//
// Solidity: event SellOrderFilled(address indexed seller, address buyer, address indexed contractAddress, uint256 indexed tokenId, uint256 price)
func (_Quixotic1 *Quixotic1Filterer) ParseSellOrderFilled(log types.Log) (*Quixotic1SellOrderFilled, error) {
	event := new(Quixotic1SellOrderFilled)
	if err := _Quixotic1.contract.UnpackLog(event, "SellOrderFilled", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Quixotic1UnpausedIterator is returned from FilterUnpaused and is used to iterate over the raw logs and unpacked data for Unpaused events raised by the Quixotic1 contract.
type Quixotic1UnpausedIterator struct {
	Event *Quixotic1Unpaused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Quixotic1UnpausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Quixotic1Unpaused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Quixotic1Unpaused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Quixotic1UnpausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Quixotic1UnpausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Quixotic1Unpaused represents a Unpaused event raised by the Quixotic1 contract.
type Quixotic1Unpaused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterUnpaused is a free log retrieval operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_Quixotic1 *Quixotic1Filterer) FilterUnpaused(opts *bind.FilterOpts) (*Quixotic1UnpausedIterator, error) {

	logs, sub, err := _Quixotic1.contract.FilterLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return &Quixotic1UnpausedIterator{contract: _Quixotic1.contract, event: "Unpaused", logs: logs, sub: sub}, nil
}

// WatchUnpaused is a free log subscription operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_Quixotic1 *Quixotic1Filterer) WatchUnpaused(opts *bind.WatchOpts, sink chan<- *Quixotic1Unpaused) (event.Subscription, error) {

	logs, sub, err := _Quixotic1.contract.WatchLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Quixotic1Unpaused)
				if err := _Quixotic1.contract.UnpackLog(event, "Unpaused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUnpaused is a log parse operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_Quixotic1 *Quixotic1Filterer) ParseUnpaused(log types.Log) (*Quixotic1Unpaused, error) {
	event := new(Quixotic1Unpaused)
	if err := _Quixotic1.contract.UnpackLog(event, "Unpaused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
