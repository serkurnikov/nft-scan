package marketplace

import (
	"context"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/powerman/structlog"

	opensea_0 "lab.aviproduction.org/aggregator/nft-scan/pkg/marketplace/restapi/opensea/type_0"
)

type Manager struct {
	logger *structlog.Logger
}

func New(logger *structlog.Logger) *Manager {
	return &Manager{logger: logger}
}

func (m *Manager) ProcessOpenSeaSaleOrder(_ context.Context, log types.Log) (*opensea_0.Opensea0OrderFulfilled, error) {
	filterer, err := opensea_0.NewOpensea0Filterer(log.Address, nil)
	if err != nil {
		return nil, err
	}
	return filterer.ParseOrderFulfilled(log)
}
