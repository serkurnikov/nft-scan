// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package trove_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Trove0MetaData contains all meta data concerning the Trove0 contract.
var Trove0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"initialLogic\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"initialAdmin\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"_data\",\"type\":\"bytes\"}],\"stateMutability\":\"payable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"previousAdmin\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"newAdmin\",\"type\":\"address\"}],\"name\":\"AdminChanged\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"implementation\",\"type\":\"address\"}],\"name\":\"Upgraded\",\"type\":\"event\"},{\"stateMutability\":\"payable\",\"type\":\"fallback\"},{\"inputs\":[],\"name\":\"admin\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newAdmin\",\"type\":\"address\"}],\"name\":\"changeAdmin\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"implementation\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newImplementation\",\"type\":\"address\"}],\"name\":\"upgradeTo\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newImplementation\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"data\",\"type\":\"bytes\"}],\"name\":\"upgradeToAndCall\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
}

// Trove0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Trove0MetaData.ABI instead.
var Trove0ABI = Trove0MetaData.ABI

// Trove0 is an auto generated Go binding around an Ethereum contract.
type Trove0 struct {
	Trove0Caller     // Read-only binding to the contract
	Trove0Transactor // Write-only binding to the contract
	Trove0Filterer   // Log filterer for contract events
}

// Trove0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Trove0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Trove0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Trove0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Trove0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Trove0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Trove0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Trove0Session struct {
	Contract     *Trove0           // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Trove0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Trove0CallerSession struct {
	Contract *Trove0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// Trove0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Trove0TransactorSession struct {
	Contract     *Trove0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Trove0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Trove0Raw struct {
	Contract *Trove0 // Generic contract binding to access the raw methods on
}

// Trove0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Trove0CallerRaw struct {
	Contract *Trove0Caller // Generic read-only contract binding to access the raw methods on
}

// Trove0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Trove0TransactorRaw struct {
	Contract *Trove0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewTrove0 creates a new instance of Trove0, bound to a specific deployed contract.
func NewTrove0(address common.Address, backend bind.ContractBackend) (*Trove0, error) {
	contract, err := bindTrove0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Trove0{Trove0Caller: Trove0Caller{contract: contract}, Trove0Transactor: Trove0Transactor{contract: contract}, Trove0Filterer: Trove0Filterer{contract: contract}}, nil
}

// NewTrove0Caller creates a new read-only instance of Trove0, bound to a specific deployed contract.
func NewTrove0Caller(address common.Address, caller bind.ContractCaller) (*Trove0Caller, error) {
	contract, err := bindTrove0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Trove0Caller{contract: contract}, nil
}

// NewTrove0Transactor creates a new write-only instance of Trove0, bound to a specific deployed contract.
func NewTrove0Transactor(address common.Address, transactor bind.ContractTransactor) (*Trove0Transactor, error) {
	contract, err := bindTrove0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Trove0Transactor{contract: contract}, nil
}

// NewTrove0Filterer creates a new log filterer instance of Trove0, bound to a specific deployed contract.
func NewTrove0Filterer(address common.Address, filterer bind.ContractFilterer) (*Trove0Filterer, error) {
	contract, err := bindTrove0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Trove0Filterer{contract: contract}, nil
}

// bindTrove0 binds a generic wrapper to an already deployed contract.
func bindTrove0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Trove0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Trove0 *Trove0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Trove0.Contract.Trove0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Trove0 *Trove0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Trove0.Contract.Trove0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Trove0 *Trove0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Trove0.Contract.Trove0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Trove0 *Trove0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Trove0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Trove0 *Trove0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Trove0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Trove0 *Trove0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Trove0.Contract.contract.Transact(opts, method, params...)
}

// Admin is a paid mutator transaction binding the contract method 0xf851a440.
//
// Solidity: function admin() returns(address)
func (_Trove0 *Trove0Transactor) Admin(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Trove0.contract.Transact(opts, "admin")
}

// Admin is a paid mutator transaction binding the contract method 0xf851a440.
//
// Solidity: function admin() returns(address)
func (_Trove0 *Trove0Session) Admin() (*types.Transaction, error) {
	return _Trove0.Contract.Admin(&_Trove0.TransactOpts)
}

// Admin is a paid mutator transaction binding the contract method 0xf851a440.
//
// Solidity: function admin() returns(address)
func (_Trove0 *Trove0TransactorSession) Admin() (*types.Transaction, error) {
	return _Trove0.Contract.Admin(&_Trove0.TransactOpts)
}

// ChangeAdmin is a paid mutator transaction binding the contract method 0x8f283970.
//
// Solidity: function changeAdmin(address newAdmin) returns()
func (_Trove0 *Trove0Transactor) ChangeAdmin(opts *bind.TransactOpts, newAdmin common.Address) (*types.Transaction, error) {
	return _Trove0.contract.Transact(opts, "changeAdmin", newAdmin)
}

// ChangeAdmin is a paid mutator transaction binding the contract method 0x8f283970.
//
// Solidity: function changeAdmin(address newAdmin) returns()
func (_Trove0 *Trove0Session) ChangeAdmin(newAdmin common.Address) (*types.Transaction, error) {
	return _Trove0.Contract.ChangeAdmin(&_Trove0.TransactOpts, newAdmin)
}

// ChangeAdmin is a paid mutator transaction binding the contract method 0x8f283970.
//
// Solidity: function changeAdmin(address newAdmin) returns()
func (_Trove0 *Trove0TransactorSession) ChangeAdmin(newAdmin common.Address) (*types.Transaction, error) {
	return _Trove0.Contract.ChangeAdmin(&_Trove0.TransactOpts, newAdmin)
}

// Implementation is a paid mutator transaction binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() returns(address)
func (_Trove0 *Trove0Transactor) Implementation(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Trove0.contract.Transact(opts, "implementation")
}

// Implementation is a paid mutator transaction binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() returns(address)
func (_Trove0 *Trove0Session) Implementation() (*types.Transaction, error) {
	return _Trove0.Contract.Implementation(&_Trove0.TransactOpts)
}

// Implementation is a paid mutator transaction binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() returns(address)
func (_Trove0 *Trove0TransactorSession) Implementation() (*types.Transaction, error) {
	return _Trove0.Contract.Implementation(&_Trove0.TransactOpts)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address newImplementation) returns()
func (_Trove0 *Trove0Transactor) UpgradeTo(opts *bind.TransactOpts, newImplementation common.Address) (*types.Transaction, error) {
	return _Trove0.contract.Transact(opts, "upgradeTo", newImplementation)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address newImplementation) returns()
func (_Trove0 *Trove0Session) UpgradeTo(newImplementation common.Address) (*types.Transaction, error) {
	return _Trove0.Contract.UpgradeTo(&_Trove0.TransactOpts, newImplementation)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address newImplementation) returns()
func (_Trove0 *Trove0TransactorSession) UpgradeTo(newImplementation common.Address) (*types.Transaction, error) {
	return _Trove0.Contract.UpgradeTo(&_Trove0.TransactOpts, newImplementation)
}

// UpgradeToAndCall is a paid mutator transaction binding the contract method 0x4f1ef286.
//
// Solidity: function upgradeToAndCall(address newImplementation, bytes data) payable returns()
func (_Trove0 *Trove0Transactor) UpgradeToAndCall(opts *bind.TransactOpts, newImplementation common.Address, data []byte) (*types.Transaction, error) {
	return _Trove0.contract.Transact(opts, "upgradeToAndCall", newImplementation, data)
}

// UpgradeToAndCall is a paid mutator transaction binding the contract method 0x4f1ef286.
//
// Solidity: function upgradeToAndCall(address newImplementation, bytes data) payable returns()
func (_Trove0 *Trove0Session) UpgradeToAndCall(newImplementation common.Address, data []byte) (*types.Transaction, error) {
	return _Trove0.Contract.UpgradeToAndCall(&_Trove0.TransactOpts, newImplementation, data)
}

// UpgradeToAndCall is a paid mutator transaction binding the contract method 0x4f1ef286.
//
// Solidity: function upgradeToAndCall(address newImplementation, bytes data) payable returns()
func (_Trove0 *Trove0TransactorSession) UpgradeToAndCall(newImplementation common.Address, data []byte) (*types.Transaction, error) {
	return _Trove0.Contract.UpgradeToAndCall(&_Trove0.TransactOpts, newImplementation, data)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Trove0 *Trove0Transactor) Fallback(opts *bind.TransactOpts, calldata []byte) (*types.Transaction, error) {
	return _Trove0.contract.RawTransact(opts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Trove0 *Trove0Session) Fallback(calldata []byte) (*types.Transaction, error) {
	return _Trove0.Contract.Fallback(&_Trove0.TransactOpts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Trove0 *Trove0TransactorSession) Fallback(calldata []byte) (*types.Transaction, error) {
	return _Trove0.Contract.Fallback(&_Trove0.TransactOpts, calldata)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Trove0 *Trove0Transactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Trove0.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Trove0 *Trove0Session) Receive() (*types.Transaction, error) {
	return _Trove0.Contract.Receive(&_Trove0.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Trove0 *Trove0TransactorSession) Receive() (*types.Transaction, error) {
	return _Trove0.Contract.Receive(&_Trove0.TransactOpts)
}

// Trove0AdminChangedIterator is returned from FilterAdminChanged and is used to iterate over the raw logs and unpacked data for AdminChanged events raised by the Trove0 contract.
type Trove0AdminChangedIterator struct {
	Event *Trove0AdminChanged // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Trove0AdminChangedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Trove0AdminChanged)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Trove0AdminChanged)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Trove0AdminChangedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Trove0AdminChangedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Trove0AdminChanged represents a AdminChanged event raised by the Trove0 contract.
type Trove0AdminChanged struct {
	PreviousAdmin common.Address
	NewAdmin      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterAdminChanged is a free log retrieval operation binding the contract event 0x7e644d79422f17c01e4894b5f4f588d331ebfa28653d42ae832dc59e38c9798f.
//
// Solidity: event AdminChanged(address previousAdmin, address newAdmin)
func (_Trove0 *Trove0Filterer) FilterAdminChanged(opts *bind.FilterOpts) (*Trove0AdminChangedIterator, error) {

	logs, sub, err := _Trove0.contract.FilterLogs(opts, "AdminChanged")
	if err != nil {
		return nil, err
	}
	return &Trove0AdminChangedIterator{contract: _Trove0.contract, event: "AdminChanged", logs: logs, sub: sub}, nil
}

// WatchAdminChanged is a free log subscription operation binding the contract event 0x7e644d79422f17c01e4894b5f4f588d331ebfa28653d42ae832dc59e38c9798f.
//
// Solidity: event AdminChanged(address previousAdmin, address newAdmin)
func (_Trove0 *Trove0Filterer) WatchAdminChanged(opts *bind.WatchOpts, sink chan<- *Trove0AdminChanged) (event.Subscription, error) {

	logs, sub, err := _Trove0.contract.WatchLogs(opts, "AdminChanged")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Trove0AdminChanged)
				if err := _Trove0.contract.UnpackLog(event, "AdminChanged", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminChanged is a log parse operation binding the contract event 0x7e644d79422f17c01e4894b5f4f588d331ebfa28653d42ae832dc59e38c9798f.
//
// Solidity: event AdminChanged(address previousAdmin, address newAdmin)
func (_Trove0 *Trove0Filterer) ParseAdminChanged(log types.Log) (*Trove0AdminChanged, error) {
	event := new(Trove0AdminChanged)
	if err := _Trove0.contract.UnpackLog(event, "AdminChanged", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Trove0UpgradedIterator is returned from FilterUpgraded and is used to iterate over the raw logs and unpacked data for Upgraded events raised by the Trove0 contract.
type Trove0UpgradedIterator struct {
	Event *Trove0Upgraded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Trove0UpgradedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Trove0Upgraded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Trove0Upgraded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Trove0UpgradedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Trove0UpgradedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Trove0Upgraded represents a Upgraded event raised by the Trove0 contract.
type Trove0Upgraded struct {
	Implementation common.Address
	Raw            types.Log // Blockchain specific contextual infos
}

// FilterUpgraded is a free log retrieval operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Trove0 *Trove0Filterer) FilterUpgraded(opts *bind.FilterOpts, implementation []common.Address) (*Trove0UpgradedIterator, error) {

	var implementationRule []interface{}
	for _, implementationItem := range implementation {
		implementationRule = append(implementationRule, implementationItem)
	}

	logs, sub, err := _Trove0.contract.FilterLogs(opts, "Upgraded", implementationRule)
	if err != nil {
		return nil, err
	}
	return &Trove0UpgradedIterator{contract: _Trove0.contract, event: "Upgraded", logs: logs, sub: sub}, nil
}

// WatchUpgraded is a free log subscription operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Trove0 *Trove0Filterer) WatchUpgraded(opts *bind.WatchOpts, sink chan<- *Trove0Upgraded, implementation []common.Address) (event.Subscription, error) {

	var implementationRule []interface{}
	for _, implementationItem := range implementation {
		implementationRule = append(implementationRule, implementationItem)
	}

	logs, sub, err := _Trove0.contract.WatchLogs(opts, "Upgraded", implementationRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Trove0Upgraded)
				if err := _Trove0.contract.UnpackLog(event, "Upgraded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUpgraded is a log parse operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Trove0 *Trove0Filterer) ParseUpgraded(log types.Log) (*Trove0Upgraded, error) {
	event := new(Trove0Upgraded)
	if err := _Trove0.contract.UnpackLog(event, "Upgraded", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
