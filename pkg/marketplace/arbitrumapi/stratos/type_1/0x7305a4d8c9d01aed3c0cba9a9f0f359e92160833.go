// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package stratos_1

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Stratos1MetaData contains all meta data concerning the Stratos1 contract.
var Stratos1MetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Paused\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"addresspayable\",\"name\":\"buyer\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"SellOrderFilled\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Unpaused\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startPrice\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endPrice\",\"type\":\"uint256\"}],\"name\":\"calculateCurrentPrice\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"addr\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"tokenAddr\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"cancelPreviousSellOrders\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"addresspayable\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"expiration\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"quantity\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"createdAtBlockNumber\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"paymentERC20\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"signature\",\"type\":\"bytes\"},{\"internalType\":\"addresspayable\",\"name\":\"buyer\",\"type\":\"address\"}],\"name\":\"fillSellOrder\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"}],\"name\":\"getRoyaltyPayoutAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"}],\"name\":\"getRoyaltyPayoutRate\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"pause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"paused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"addresspayable\",\"name\":\"_newMakerWallet\",\"type\":\"address\"}],\"name\":\"setMakerWallet\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_royaltyRegistry\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_cancellationRegistry\",\"type\":\"address\"}],\"name\":\"setRegistryContracts\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"contractAddress\",\"type\":\"address\"},{\"internalType\":\"addresspayable\",\"name\":\"_payoutAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_payoutPerMille\",\"type\":\"uint256\"}],\"name\":\"setRoyalty\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"unpause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// Stratos1ABI is the input ABI used to generate the binding from.
// Deprecated: Use Stratos1MetaData.ABI instead.
var Stratos1ABI = Stratos1MetaData.ABI

// Stratos1 is an auto generated Go binding around an Ethereum contract.
type Stratos1 struct {
	Stratos1Caller     // Read-only binding to the contract
	Stratos1Transactor // Write-only binding to the contract
	Stratos1Filterer   // Log filterer for contract events
}

// Stratos1Caller is an auto generated read-only Go binding around an Ethereum contract.
type Stratos1Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Stratos1Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Stratos1Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Stratos1Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Stratos1Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Stratos1Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Stratos1Session struct {
	Contract     *Stratos1         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Stratos1CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Stratos1CallerSession struct {
	Contract *Stratos1Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// Stratos1TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Stratos1TransactorSession struct {
	Contract     *Stratos1Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// Stratos1Raw is an auto generated low-level Go binding around an Ethereum contract.
type Stratos1Raw struct {
	Contract *Stratos1 // Generic contract binding to access the raw methods on
}

// Stratos1CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Stratos1CallerRaw struct {
	Contract *Stratos1Caller // Generic read-only contract binding to access the raw methods on
}

// Stratos1TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Stratos1TransactorRaw struct {
	Contract *Stratos1Transactor // Generic write-only contract binding to access the raw methods on
}

// NewStratos1 creates a new instance of Stratos1, bound to a specific deployed contract.
func NewStratos1(address common.Address, backend bind.ContractBackend) (*Stratos1, error) {
	contract, err := bindStratos1(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Stratos1{Stratos1Caller: Stratos1Caller{contract: contract}, Stratos1Transactor: Stratos1Transactor{contract: contract}, Stratos1Filterer: Stratos1Filterer{contract: contract}}, nil
}

// NewStratos1Caller creates a new read-only instance of Stratos1, bound to a specific deployed contract.
func NewStratos1Caller(address common.Address, caller bind.ContractCaller) (*Stratos1Caller, error) {
	contract, err := bindStratos1(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Stratos1Caller{contract: contract}, nil
}

// NewStratos1Transactor creates a new write-only instance of Stratos1, bound to a specific deployed contract.
func NewStratos1Transactor(address common.Address, transactor bind.ContractTransactor) (*Stratos1Transactor, error) {
	contract, err := bindStratos1(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Stratos1Transactor{contract: contract}, nil
}

// NewStratos1Filterer creates a new log filterer instance of Stratos1, bound to a specific deployed contract.
func NewStratos1Filterer(address common.Address, filterer bind.ContractFilterer) (*Stratos1Filterer, error) {
	contract, err := bindStratos1(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Stratos1Filterer{contract: contract}, nil
}

// bindStratos1 binds a generic wrapper to an already deployed contract.
func bindStratos1(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Stratos1ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Stratos1 *Stratos1Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Stratos1.Contract.Stratos1Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Stratos1 *Stratos1Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Stratos1.Contract.Stratos1Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Stratos1 *Stratos1Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Stratos1.Contract.Stratos1Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Stratos1 *Stratos1CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Stratos1.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Stratos1 *Stratos1TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Stratos1.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Stratos1 *Stratos1TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Stratos1.Contract.contract.Transact(opts, method, params...)
}

// CalculateCurrentPrice is a free data retrieval call binding the contract method 0xc3fbdb7c.
//
// Solidity: function calculateCurrentPrice(uint256 startTime, uint256 endTime, uint256 startPrice, uint256 endPrice) view returns(uint256)
func (_Stratos1 *Stratos1Caller) CalculateCurrentPrice(opts *bind.CallOpts, startTime *big.Int, endTime *big.Int, startPrice *big.Int, endPrice *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Stratos1.contract.Call(opts, &out, "calculateCurrentPrice", startTime, endTime, startPrice, endPrice)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CalculateCurrentPrice is a free data retrieval call binding the contract method 0xc3fbdb7c.
//
// Solidity: function calculateCurrentPrice(uint256 startTime, uint256 endTime, uint256 startPrice, uint256 endPrice) view returns(uint256)
func (_Stratos1 *Stratos1Session) CalculateCurrentPrice(startTime *big.Int, endTime *big.Int, startPrice *big.Int, endPrice *big.Int) (*big.Int, error) {
	return _Stratos1.Contract.CalculateCurrentPrice(&_Stratos1.CallOpts, startTime, endTime, startPrice, endPrice)
}

// CalculateCurrentPrice is a free data retrieval call binding the contract method 0xc3fbdb7c.
//
// Solidity: function calculateCurrentPrice(uint256 startTime, uint256 endTime, uint256 startPrice, uint256 endPrice) view returns(uint256)
func (_Stratos1 *Stratos1CallerSession) CalculateCurrentPrice(startTime *big.Int, endTime *big.Int, startPrice *big.Int, endPrice *big.Int) (*big.Int, error) {
	return _Stratos1.Contract.CalculateCurrentPrice(&_Stratos1.CallOpts, startTime, endTime, startPrice, endPrice)
}

// GetRoyaltyPayoutAddress is a free data retrieval call binding the contract method 0x2df5cb23.
//
// Solidity: function getRoyaltyPayoutAddress(address contractAddress) view returns(address)
func (_Stratos1 *Stratos1Caller) GetRoyaltyPayoutAddress(opts *bind.CallOpts, contractAddress common.Address) (common.Address, error) {
	var out []interface{}
	err := _Stratos1.contract.Call(opts, &out, "getRoyaltyPayoutAddress", contractAddress)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetRoyaltyPayoutAddress is a free data retrieval call binding the contract method 0x2df5cb23.
//
// Solidity: function getRoyaltyPayoutAddress(address contractAddress) view returns(address)
func (_Stratos1 *Stratos1Session) GetRoyaltyPayoutAddress(contractAddress common.Address) (common.Address, error) {
	return _Stratos1.Contract.GetRoyaltyPayoutAddress(&_Stratos1.CallOpts, contractAddress)
}

// GetRoyaltyPayoutAddress is a free data retrieval call binding the contract method 0x2df5cb23.
//
// Solidity: function getRoyaltyPayoutAddress(address contractAddress) view returns(address)
func (_Stratos1 *Stratos1CallerSession) GetRoyaltyPayoutAddress(contractAddress common.Address) (common.Address, error) {
	return _Stratos1.Contract.GetRoyaltyPayoutAddress(&_Stratos1.CallOpts, contractAddress)
}

// GetRoyaltyPayoutRate is a free data retrieval call binding the contract method 0xec4210ff.
//
// Solidity: function getRoyaltyPayoutRate(address contractAddress) view returns(uint256)
func (_Stratos1 *Stratos1Caller) GetRoyaltyPayoutRate(opts *bind.CallOpts, contractAddress common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Stratos1.contract.Call(opts, &out, "getRoyaltyPayoutRate", contractAddress)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetRoyaltyPayoutRate is a free data retrieval call binding the contract method 0xec4210ff.
//
// Solidity: function getRoyaltyPayoutRate(address contractAddress) view returns(uint256)
func (_Stratos1 *Stratos1Session) GetRoyaltyPayoutRate(contractAddress common.Address) (*big.Int, error) {
	return _Stratos1.Contract.GetRoyaltyPayoutRate(&_Stratos1.CallOpts, contractAddress)
}

// GetRoyaltyPayoutRate is a free data retrieval call binding the contract method 0xec4210ff.
//
// Solidity: function getRoyaltyPayoutRate(address contractAddress) view returns(uint256)
func (_Stratos1 *Stratos1CallerSession) GetRoyaltyPayoutRate(contractAddress common.Address) (*big.Int, error) {
	return _Stratos1.Contract.GetRoyaltyPayoutRate(&_Stratos1.CallOpts, contractAddress)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Stratos1 *Stratos1Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Stratos1.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Stratos1 *Stratos1Session) Owner() (common.Address, error) {
	return _Stratos1.Contract.Owner(&_Stratos1.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Stratos1 *Stratos1CallerSession) Owner() (common.Address, error) {
	return _Stratos1.Contract.Owner(&_Stratos1.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Stratos1 *Stratos1Caller) Paused(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Stratos1.contract.Call(opts, &out, "paused")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Stratos1 *Stratos1Session) Paused() (bool, error) {
	return _Stratos1.Contract.Paused(&_Stratos1.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Stratos1 *Stratos1CallerSession) Paused() (bool, error) {
	return _Stratos1.Contract.Paused(&_Stratos1.CallOpts)
}

// CancelPreviousSellOrders is a paid mutator transaction binding the contract method 0x083d089d.
//
// Solidity: function cancelPreviousSellOrders(address addr, address tokenAddr, uint256 tokenId) returns()
func (_Stratos1 *Stratos1Transactor) CancelPreviousSellOrders(opts *bind.TransactOpts, addr common.Address, tokenAddr common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Stratos1.contract.Transact(opts, "cancelPreviousSellOrders", addr, tokenAddr, tokenId)
}

// CancelPreviousSellOrders is a paid mutator transaction binding the contract method 0x083d089d.
//
// Solidity: function cancelPreviousSellOrders(address addr, address tokenAddr, uint256 tokenId) returns()
func (_Stratos1 *Stratos1Session) CancelPreviousSellOrders(addr common.Address, tokenAddr common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Stratos1.Contract.CancelPreviousSellOrders(&_Stratos1.TransactOpts, addr, tokenAddr, tokenId)
}

// CancelPreviousSellOrders is a paid mutator transaction binding the contract method 0x083d089d.
//
// Solidity: function cancelPreviousSellOrders(address addr, address tokenAddr, uint256 tokenId) returns()
func (_Stratos1 *Stratos1TransactorSession) CancelPreviousSellOrders(addr common.Address, tokenAddr common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Stratos1.Contract.CancelPreviousSellOrders(&_Stratos1.TransactOpts, addr, tokenAddr, tokenId)
}

// FillSellOrder is a paid mutator transaction binding the contract method 0x912d97fc.
//
// Solidity: function fillSellOrder(address seller, address contractAddress, uint256 tokenId, uint256 startTime, uint256 expiration, uint256 price, uint256 quantity, uint256 createdAtBlockNumber, address paymentERC20, bytes signature, address buyer) payable returns()
func (_Stratos1 *Stratos1Transactor) FillSellOrder(opts *bind.TransactOpts, seller common.Address, contractAddress common.Address, tokenId *big.Int, startTime *big.Int, expiration *big.Int, price *big.Int, quantity *big.Int, createdAtBlockNumber *big.Int, paymentERC20 common.Address, signature []byte, buyer common.Address) (*types.Transaction, error) {
	return _Stratos1.contract.Transact(opts, "fillSellOrder", seller, contractAddress, tokenId, startTime, expiration, price, quantity, createdAtBlockNumber, paymentERC20, signature, buyer)
}

// FillSellOrder is a paid mutator transaction binding the contract method 0x912d97fc.
//
// Solidity: function fillSellOrder(address seller, address contractAddress, uint256 tokenId, uint256 startTime, uint256 expiration, uint256 price, uint256 quantity, uint256 createdAtBlockNumber, address paymentERC20, bytes signature, address buyer) payable returns()
func (_Stratos1 *Stratos1Session) FillSellOrder(seller common.Address, contractAddress common.Address, tokenId *big.Int, startTime *big.Int, expiration *big.Int, price *big.Int, quantity *big.Int, createdAtBlockNumber *big.Int, paymentERC20 common.Address, signature []byte, buyer common.Address) (*types.Transaction, error) {
	return _Stratos1.Contract.FillSellOrder(&_Stratos1.TransactOpts, seller, contractAddress, tokenId, startTime, expiration, price, quantity, createdAtBlockNumber, paymentERC20, signature, buyer)
}

// FillSellOrder is a paid mutator transaction binding the contract method 0x912d97fc.
//
// Solidity: function fillSellOrder(address seller, address contractAddress, uint256 tokenId, uint256 startTime, uint256 expiration, uint256 price, uint256 quantity, uint256 createdAtBlockNumber, address paymentERC20, bytes signature, address buyer) payable returns()
func (_Stratos1 *Stratos1TransactorSession) FillSellOrder(seller common.Address, contractAddress common.Address, tokenId *big.Int, startTime *big.Int, expiration *big.Int, price *big.Int, quantity *big.Int, createdAtBlockNumber *big.Int, paymentERC20 common.Address, signature []byte, buyer common.Address) (*types.Transaction, error) {
	return _Stratos1.Contract.FillSellOrder(&_Stratos1.TransactOpts, seller, contractAddress, tokenId, startTime, expiration, price, quantity, createdAtBlockNumber, paymentERC20, signature, buyer)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_Stratos1 *Stratos1Transactor) Pause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Stratos1.contract.Transact(opts, "pause")
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_Stratos1 *Stratos1Session) Pause() (*types.Transaction, error) {
	return _Stratos1.Contract.Pause(&_Stratos1.TransactOpts)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_Stratos1 *Stratos1TransactorSession) Pause() (*types.Transaction, error) {
	return _Stratos1.Contract.Pause(&_Stratos1.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Stratos1 *Stratos1Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Stratos1.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Stratos1 *Stratos1Session) RenounceOwnership() (*types.Transaction, error) {
	return _Stratos1.Contract.RenounceOwnership(&_Stratos1.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Stratos1 *Stratos1TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Stratos1.Contract.RenounceOwnership(&_Stratos1.TransactOpts)
}

// SetMakerWallet is a paid mutator transaction binding the contract method 0x2b812a34.
//
// Solidity: function setMakerWallet(address _newMakerWallet) returns()
func (_Stratos1 *Stratos1Transactor) SetMakerWallet(opts *bind.TransactOpts, _newMakerWallet common.Address) (*types.Transaction, error) {
	return _Stratos1.contract.Transact(opts, "setMakerWallet", _newMakerWallet)
}

// SetMakerWallet is a paid mutator transaction binding the contract method 0x2b812a34.
//
// Solidity: function setMakerWallet(address _newMakerWallet) returns()
func (_Stratos1 *Stratos1Session) SetMakerWallet(_newMakerWallet common.Address) (*types.Transaction, error) {
	return _Stratos1.Contract.SetMakerWallet(&_Stratos1.TransactOpts, _newMakerWallet)
}

// SetMakerWallet is a paid mutator transaction binding the contract method 0x2b812a34.
//
// Solidity: function setMakerWallet(address _newMakerWallet) returns()
func (_Stratos1 *Stratos1TransactorSession) SetMakerWallet(_newMakerWallet common.Address) (*types.Transaction, error) {
	return _Stratos1.Contract.SetMakerWallet(&_Stratos1.TransactOpts, _newMakerWallet)
}

// SetRegistryContracts is a paid mutator transaction binding the contract method 0xb1216b79.
//
// Solidity: function setRegistryContracts(address _royaltyRegistry, address _cancellationRegistry) returns()
func (_Stratos1 *Stratos1Transactor) SetRegistryContracts(opts *bind.TransactOpts, _royaltyRegistry common.Address, _cancellationRegistry common.Address) (*types.Transaction, error) {
	return _Stratos1.contract.Transact(opts, "setRegistryContracts", _royaltyRegistry, _cancellationRegistry)
}

// SetRegistryContracts is a paid mutator transaction binding the contract method 0xb1216b79.
//
// Solidity: function setRegistryContracts(address _royaltyRegistry, address _cancellationRegistry) returns()
func (_Stratos1 *Stratos1Session) SetRegistryContracts(_royaltyRegistry common.Address, _cancellationRegistry common.Address) (*types.Transaction, error) {
	return _Stratos1.Contract.SetRegistryContracts(&_Stratos1.TransactOpts, _royaltyRegistry, _cancellationRegistry)
}

// SetRegistryContracts is a paid mutator transaction binding the contract method 0xb1216b79.
//
// Solidity: function setRegistryContracts(address _royaltyRegistry, address _cancellationRegistry) returns()
func (_Stratos1 *Stratos1TransactorSession) SetRegistryContracts(_royaltyRegistry common.Address, _cancellationRegistry common.Address) (*types.Transaction, error) {
	return _Stratos1.Contract.SetRegistryContracts(&_Stratos1.TransactOpts, _royaltyRegistry, _cancellationRegistry)
}

// SetRoyalty is a paid mutator transaction binding the contract method 0x55c338aa.
//
// Solidity: function setRoyalty(address contractAddress, address _payoutAddress, uint256 _payoutPerMille) returns()
func (_Stratos1 *Stratos1Transactor) SetRoyalty(opts *bind.TransactOpts, contractAddress common.Address, _payoutAddress common.Address, _payoutPerMille *big.Int) (*types.Transaction, error) {
	return _Stratos1.contract.Transact(opts, "setRoyalty", contractAddress, _payoutAddress, _payoutPerMille)
}

// SetRoyalty is a paid mutator transaction binding the contract method 0x55c338aa.
//
// Solidity: function setRoyalty(address contractAddress, address _payoutAddress, uint256 _payoutPerMille) returns()
func (_Stratos1 *Stratos1Session) SetRoyalty(contractAddress common.Address, _payoutAddress common.Address, _payoutPerMille *big.Int) (*types.Transaction, error) {
	return _Stratos1.Contract.SetRoyalty(&_Stratos1.TransactOpts, contractAddress, _payoutAddress, _payoutPerMille)
}

// SetRoyalty is a paid mutator transaction binding the contract method 0x55c338aa.
//
// Solidity: function setRoyalty(address contractAddress, address _payoutAddress, uint256 _payoutPerMille) returns()
func (_Stratos1 *Stratos1TransactorSession) SetRoyalty(contractAddress common.Address, _payoutAddress common.Address, _payoutPerMille *big.Int) (*types.Transaction, error) {
	return _Stratos1.Contract.SetRoyalty(&_Stratos1.TransactOpts, contractAddress, _payoutAddress, _payoutPerMille)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Stratos1 *Stratos1Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Stratos1.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Stratos1 *Stratos1Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Stratos1.Contract.TransferOwnership(&_Stratos1.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Stratos1 *Stratos1TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Stratos1.Contract.TransferOwnership(&_Stratos1.TransactOpts, newOwner)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_Stratos1 *Stratos1Transactor) Unpause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Stratos1.contract.Transact(opts, "unpause")
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_Stratos1 *Stratos1Session) Unpause() (*types.Transaction, error) {
	return _Stratos1.Contract.Unpause(&_Stratos1.TransactOpts)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_Stratos1 *Stratos1TransactorSession) Unpause() (*types.Transaction, error) {
	return _Stratos1.Contract.Unpause(&_Stratos1.TransactOpts)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_Stratos1 *Stratos1Transactor) Withdraw(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Stratos1.contract.Transact(opts, "withdraw")
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_Stratos1 *Stratos1Session) Withdraw() (*types.Transaction, error) {
	return _Stratos1.Contract.Withdraw(&_Stratos1.TransactOpts)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_Stratos1 *Stratos1TransactorSession) Withdraw() (*types.Transaction, error) {
	return _Stratos1.Contract.Withdraw(&_Stratos1.TransactOpts)
}

// Stratos1OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Stratos1 contract.
type Stratos1OwnershipTransferredIterator struct {
	Event *Stratos1OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Stratos1OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Stratos1OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Stratos1OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Stratos1OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Stratos1OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Stratos1OwnershipTransferred represents a OwnershipTransferred event raised by the Stratos1 contract.
type Stratos1OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Stratos1 *Stratos1Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Stratos1OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Stratos1.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Stratos1OwnershipTransferredIterator{contract: _Stratos1.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Stratos1 *Stratos1Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Stratos1OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Stratos1.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Stratos1OwnershipTransferred)
				if err := _Stratos1.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Stratos1 *Stratos1Filterer) ParseOwnershipTransferred(log types.Log) (*Stratos1OwnershipTransferred, error) {
	event := new(Stratos1OwnershipTransferred)
	if err := _Stratos1.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Stratos1PausedIterator is returned from FilterPaused and is used to iterate over the raw logs and unpacked data for Paused events raised by the Stratos1 contract.
type Stratos1PausedIterator struct {
	Event *Stratos1Paused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Stratos1PausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Stratos1Paused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Stratos1Paused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Stratos1PausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Stratos1PausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Stratos1Paused represents a Paused event raised by the Stratos1 contract.
type Stratos1Paused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterPaused is a free log retrieval operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_Stratos1 *Stratos1Filterer) FilterPaused(opts *bind.FilterOpts) (*Stratos1PausedIterator, error) {

	logs, sub, err := _Stratos1.contract.FilterLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return &Stratos1PausedIterator{contract: _Stratos1.contract, event: "Paused", logs: logs, sub: sub}, nil
}

// WatchPaused is a free log subscription operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_Stratos1 *Stratos1Filterer) WatchPaused(opts *bind.WatchOpts, sink chan<- *Stratos1Paused) (event.Subscription, error) {

	logs, sub, err := _Stratos1.contract.WatchLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Stratos1Paused)
				if err := _Stratos1.contract.UnpackLog(event, "Paused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePaused is a log parse operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_Stratos1 *Stratos1Filterer) ParsePaused(log types.Log) (*Stratos1Paused, error) {
	event := new(Stratos1Paused)
	if err := _Stratos1.contract.UnpackLog(event, "Paused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Stratos1SellOrderFilledIterator is returned from FilterSellOrderFilled and is used to iterate over the raw logs and unpacked data for SellOrderFilled events raised by the Stratos1 contract.
type Stratos1SellOrderFilledIterator struct {
	Event *Stratos1SellOrderFilled // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Stratos1SellOrderFilledIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Stratos1SellOrderFilled)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Stratos1SellOrderFilled)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Stratos1SellOrderFilledIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Stratos1SellOrderFilledIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Stratos1SellOrderFilled represents a SellOrderFilled event raised by the Stratos1 contract.
type Stratos1SellOrderFilled struct {
	Seller          common.Address
	Buyer           common.Address
	ContractAddress common.Address
	TokenId         *big.Int
	Price           *big.Int
	Raw             types.Log // Blockchain specific contextual infos
}

// FilterSellOrderFilled is a free log retrieval operation binding the contract event 0x70ba0d31158674eea8365d0f7b9ac70e552cc28b8bb848664e4feb939c6578f8.
//
// Solidity: event SellOrderFilled(address indexed seller, address buyer, address indexed contractAddress, uint256 indexed tokenId, uint256 price)
func (_Stratos1 *Stratos1Filterer) FilterSellOrderFilled(opts *bind.FilterOpts, seller []common.Address, contractAddress []common.Address, tokenId []*big.Int) (*Stratos1SellOrderFilledIterator, error) {

	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	var contractAddressRule []interface{}
	for _, contractAddressItem := range contractAddress {
		contractAddressRule = append(contractAddressRule, contractAddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Stratos1.contract.FilterLogs(opts, "SellOrderFilled", sellerRule, contractAddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Stratos1SellOrderFilledIterator{contract: _Stratos1.contract, event: "SellOrderFilled", logs: logs, sub: sub}, nil
}

// WatchSellOrderFilled is a free log subscription operation binding the contract event 0x70ba0d31158674eea8365d0f7b9ac70e552cc28b8bb848664e4feb939c6578f8.
//
// Solidity: event SellOrderFilled(address indexed seller, address buyer, address indexed contractAddress, uint256 indexed tokenId, uint256 price)
func (_Stratos1 *Stratos1Filterer) WatchSellOrderFilled(opts *bind.WatchOpts, sink chan<- *Stratos1SellOrderFilled, seller []common.Address, contractAddress []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	var contractAddressRule []interface{}
	for _, contractAddressItem := range contractAddress {
		contractAddressRule = append(contractAddressRule, contractAddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Stratos1.contract.WatchLogs(opts, "SellOrderFilled", sellerRule, contractAddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Stratos1SellOrderFilled)
				if err := _Stratos1.contract.UnpackLog(event, "SellOrderFilled", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseSellOrderFilled is a log parse operation binding the contract event 0x70ba0d31158674eea8365d0f7b9ac70e552cc28b8bb848664e4feb939c6578f8.
//
// Solidity: event SellOrderFilled(address indexed seller, address buyer, address indexed contractAddress, uint256 indexed tokenId, uint256 price)
func (_Stratos1 *Stratos1Filterer) ParseSellOrderFilled(log types.Log) (*Stratos1SellOrderFilled, error) {
	event := new(Stratos1SellOrderFilled)
	if err := _Stratos1.contract.UnpackLog(event, "SellOrderFilled", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Stratos1UnpausedIterator is returned from FilterUnpaused and is used to iterate over the raw logs and unpacked data for Unpaused events raised by the Stratos1 contract.
type Stratos1UnpausedIterator struct {
	Event *Stratos1Unpaused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Stratos1UnpausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Stratos1Unpaused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Stratos1Unpaused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Stratos1UnpausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Stratos1UnpausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Stratos1Unpaused represents a Unpaused event raised by the Stratos1 contract.
type Stratos1Unpaused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterUnpaused is a free log retrieval operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_Stratos1 *Stratos1Filterer) FilterUnpaused(opts *bind.FilterOpts) (*Stratos1UnpausedIterator, error) {

	logs, sub, err := _Stratos1.contract.FilterLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return &Stratos1UnpausedIterator{contract: _Stratos1.contract, event: "Unpaused", logs: logs, sub: sub}, nil
}

// WatchUnpaused is a free log subscription operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_Stratos1 *Stratos1Filterer) WatchUnpaused(opts *bind.WatchOpts, sink chan<- *Stratos1Unpaused) (event.Subscription, error) {

	logs, sub, err := _Stratos1.contract.WatchLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Stratos1Unpaused)
				if err := _Stratos1.contract.UnpackLog(event, "Unpaused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUnpaused is a log parse operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_Stratos1 *Stratos1Filterer) ParseUnpaused(log types.Log) (*Stratos1Unpaused, error) {
	event := new(Stratos1Unpaused)
	if err := _Stratos1.contract.UnpackLog(event, "Unpaused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
