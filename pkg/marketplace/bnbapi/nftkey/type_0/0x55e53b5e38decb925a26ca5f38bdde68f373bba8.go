// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package nftkey_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// INFTKEYMarketplaceRoyaltyERC721CollectionRoyalty is an auto generated low-level Go binding around an user-defined struct.
type INFTKEYMarketplaceRoyaltyERC721CollectionRoyalty struct {
	Recipient   common.Address
	FeeFraction *big.Int
	SetBy       common.Address
}

// INFTKEYMarketplaceV2Bid is an auto generated low-level Go binding around an user-defined struct.
type INFTKEYMarketplaceV2Bid struct {
	TokenId         *big.Int
	Value           *big.Int
	Bidder          common.Address
	ExpireTimestamp *big.Int
}

// INFTKEYMarketplaceV2Listing is an auto generated low-level Go binding around an user-defined struct.
type INFTKEYMarketplaceV2Listing struct {
	TokenId         *big.Int
	Value           *big.Int
	Seller          common.Address
	ExpireTimestamp *big.Int
}

// Nftkey0MetaData contains all meta data concerning the Nftkey0 contract.
var Nftkey0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_paymentTokenAddress\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"feeFraction\",\"type\":\"uint256\"}],\"name\":\"SetRoyalty\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"indexed\":false,\"internalType\":\"structINFTKEYMarketplaceV2.Bid\",\"name\":\"bid\",\"type\":\"tuple\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"serviceFee\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"royaltyFee\",\"type\":\"uint256\"}],\"name\":\"TokenBidAccepted\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"indexed\":false,\"internalType\":\"structINFTKEYMarketplaceV2.Bid\",\"name\":\"bid\",\"type\":\"tuple\"}],\"name\":\"TokenBidEntered\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"indexed\":false,\"internalType\":\"structINFTKEYMarketplaceV2.Bid\",\"name\":\"bid\",\"type\":\"tuple\"}],\"name\":\"TokenBidWithdrawn\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"indexed\":false,\"internalType\":\"structINFTKEYMarketplaceV2.Listing\",\"name\":\"listing\",\"type\":\"tuple\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"serviceFee\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"royaltyFee\",\"type\":\"uint256\"}],\"name\":\"TokenBought\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"indexed\":false,\"internalType\":\"structINFTKEYMarketplaceV2.Listing\",\"name\":\"listing\",\"type\":\"tuple\"}],\"name\":\"TokenDelisted\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"indexed\":false,\"internalType\":\"structINFTKEYMarketplaceV2.Listing\",\"name\":\"listing\",\"type\":\"tuple\"}],\"name\":\"TokenListed\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"acceptBidForToken\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"actionTimeOutRangeMax\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"actionTimeOutRangeMin\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"buyToken\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bool\",\"name\":\"enabled\",\"type\":\"bool\"}],\"name\":\"changeMarketplaceStatus\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"timeInSec\",\"type\":\"uint256\"}],\"name\":\"changeMaxActionTimeLimit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"timeInSec\",\"type\":\"uint256\"}],\"name\":\"changeMinActionTimeLimit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint8\",\"name\":\"serviceFeeFraction_\",\"type\":\"uint8\"}],\"name\":\"changeSeriveFee\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"defaultRoyaltyFraction\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"delistToken\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"name\":\"enterBidForToken\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"from\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"size\",\"type\":\"uint256\"}],\"name\":\"getBidderBids\",\"outputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"internalType\":\"structINFTKEYMarketplaceV2.Bid[]\",\"name\":\"bidderBids\",\"type\":\"tuple[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"}],\"name\":\"getBidderTokenBid\",\"outputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"internalType\":\"structINFTKEYMarketplaceV2.Bid\",\"name\":\"validBid\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"getTokenBids\",\"outputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"internalType\":\"structINFTKEYMarketplaceV2.Bid[]\",\"name\":\"bids\",\"type\":\"tuple[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"getTokenHighestBid\",\"outputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"internalType\":\"structINFTKEYMarketplaceV2.Bid\",\"name\":\"highestBid\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"from\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"size\",\"type\":\"uint256\"}],\"name\":\"getTokenHighestBids\",\"outputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"internalType\":\"structINFTKEYMarketplaceV2.Bid[]\",\"name\":\"highestBids\",\"type\":\"tuple[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"getTokenListing\",\"outputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"internalType\":\"structINFTKEYMarketplaceV2.Listing\",\"name\":\"validListing\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"from\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"size\",\"type\":\"uint256\"}],\"name\":\"getTokenListings\",\"outputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"internalType\":\"structINFTKEYMarketplaceV2.Listing[]\",\"name\":\"listings\",\"type\":\"tuple[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"isTradingEnabled\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"expireTimestamp\",\"type\":\"uint256\"}],\"name\":\"listToken\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"}],\"name\":\"numTokenListings\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"}],\"name\":\"numTokenWithBids\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"paymentToken\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"}],\"name\":\"royalty\",\"outputs\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"feeFraction\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"setBy\",\"type\":\"address\"}],\"internalType\":\"structINFTKEYMarketplaceRoyalty.ERC721CollectionRoyalty\",\"name\":\"\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"royaltyUpperLimit\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"serviceFee\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"newRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"feeFraction\",\"type\":\"uint256\"}],\"name\":\"setRoyalty\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"newRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"feeFraction\",\"type\":\"uint256\"}],\"name\":\"setRoyaltyForCollection\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_newUpperLimit\",\"type\":\"uint256\"}],\"name\":\"updateRoyaltyUpperLimit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc721Address\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"withdrawBidForToken\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// Nftkey0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Nftkey0MetaData.ABI instead.
var Nftkey0ABI = Nftkey0MetaData.ABI

// Nftkey0 is an auto generated Go binding around an Ethereum contract.
type Nftkey0 struct {
	Nftkey0Caller     // Read-only binding to the contract
	Nftkey0Transactor // Write-only binding to the contract
	Nftkey0Filterer   // Log filterer for contract events
}

// Nftkey0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Nftkey0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Nftkey0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Nftkey0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Nftkey0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Nftkey0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Nftkey0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Nftkey0Session struct {
	Contract     *Nftkey0          // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Nftkey0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Nftkey0CallerSession struct {
	Contract *Nftkey0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts  // Call options to use throughout this session
}

// Nftkey0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Nftkey0TransactorSession struct {
	Contract     *Nftkey0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts  // Transaction auth options to use throughout this session
}

// Nftkey0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Nftkey0Raw struct {
	Contract *Nftkey0 // Generic contract binding to access the raw methods on
}

// Nftkey0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Nftkey0CallerRaw struct {
	Contract *Nftkey0Caller // Generic read-only contract binding to access the raw methods on
}

// Nftkey0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Nftkey0TransactorRaw struct {
	Contract *Nftkey0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewNftkey0 creates a new instance of Nftkey0, bound to a specific deployed contract.
func NewNftkey0(address common.Address, backend bind.ContractBackend) (*Nftkey0, error) {
	contract, err := bindNftkey0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Nftkey0{Nftkey0Caller: Nftkey0Caller{contract: contract}, Nftkey0Transactor: Nftkey0Transactor{contract: contract}, Nftkey0Filterer: Nftkey0Filterer{contract: contract}}, nil
}

// NewNftkey0Caller creates a new read-only instance of Nftkey0, bound to a specific deployed contract.
func NewNftkey0Caller(address common.Address, caller bind.ContractCaller) (*Nftkey0Caller, error) {
	contract, err := bindNftkey0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Nftkey0Caller{contract: contract}, nil
}

// NewNftkey0Transactor creates a new write-only instance of Nftkey0, bound to a specific deployed contract.
func NewNftkey0Transactor(address common.Address, transactor bind.ContractTransactor) (*Nftkey0Transactor, error) {
	contract, err := bindNftkey0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Nftkey0Transactor{contract: contract}, nil
}

// NewNftkey0Filterer creates a new log filterer instance of Nftkey0, bound to a specific deployed contract.
func NewNftkey0Filterer(address common.Address, filterer bind.ContractFilterer) (*Nftkey0Filterer, error) {
	contract, err := bindNftkey0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Nftkey0Filterer{contract: contract}, nil
}

// bindNftkey0 binds a generic wrapper to an already deployed contract.
func bindNftkey0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Nftkey0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Nftkey0 *Nftkey0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Nftkey0.Contract.Nftkey0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Nftkey0 *Nftkey0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Nftkey0.Contract.Nftkey0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Nftkey0 *Nftkey0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Nftkey0.Contract.Nftkey0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Nftkey0 *Nftkey0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Nftkey0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Nftkey0 *Nftkey0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Nftkey0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Nftkey0 *Nftkey0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Nftkey0.Contract.contract.Transact(opts, method, params...)
}

// ActionTimeOutRangeMax is a free data retrieval call binding the contract method 0x453dfc50.
//
// Solidity: function actionTimeOutRangeMax() view returns(uint256)
func (_Nftkey0 *Nftkey0Caller) ActionTimeOutRangeMax(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "actionTimeOutRangeMax")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// ActionTimeOutRangeMax is a free data retrieval call binding the contract method 0x453dfc50.
//
// Solidity: function actionTimeOutRangeMax() view returns(uint256)
func (_Nftkey0 *Nftkey0Session) ActionTimeOutRangeMax() (*big.Int, error) {
	return _Nftkey0.Contract.ActionTimeOutRangeMax(&_Nftkey0.CallOpts)
}

// ActionTimeOutRangeMax is a free data retrieval call binding the contract method 0x453dfc50.
//
// Solidity: function actionTimeOutRangeMax() view returns(uint256)
func (_Nftkey0 *Nftkey0CallerSession) ActionTimeOutRangeMax() (*big.Int, error) {
	return _Nftkey0.Contract.ActionTimeOutRangeMax(&_Nftkey0.CallOpts)
}

// ActionTimeOutRangeMin is a free data retrieval call binding the contract method 0x33549d3d.
//
// Solidity: function actionTimeOutRangeMin() view returns(uint256)
func (_Nftkey0 *Nftkey0Caller) ActionTimeOutRangeMin(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "actionTimeOutRangeMin")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// ActionTimeOutRangeMin is a free data retrieval call binding the contract method 0x33549d3d.
//
// Solidity: function actionTimeOutRangeMin() view returns(uint256)
func (_Nftkey0 *Nftkey0Session) ActionTimeOutRangeMin() (*big.Int, error) {
	return _Nftkey0.Contract.ActionTimeOutRangeMin(&_Nftkey0.CallOpts)
}

// ActionTimeOutRangeMin is a free data retrieval call binding the contract method 0x33549d3d.
//
// Solidity: function actionTimeOutRangeMin() view returns(uint256)
func (_Nftkey0 *Nftkey0CallerSession) ActionTimeOutRangeMin() (*big.Int, error) {
	return _Nftkey0.Contract.ActionTimeOutRangeMin(&_Nftkey0.CallOpts)
}

// DefaultRoyaltyFraction is a free data retrieval call binding the contract method 0x6d0042b8.
//
// Solidity: function defaultRoyaltyFraction() view returns(uint256)
func (_Nftkey0 *Nftkey0Caller) DefaultRoyaltyFraction(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "defaultRoyaltyFraction")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// DefaultRoyaltyFraction is a free data retrieval call binding the contract method 0x6d0042b8.
//
// Solidity: function defaultRoyaltyFraction() view returns(uint256)
func (_Nftkey0 *Nftkey0Session) DefaultRoyaltyFraction() (*big.Int, error) {
	return _Nftkey0.Contract.DefaultRoyaltyFraction(&_Nftkey0.CallOpts)
}

// DefaultRoyaltyFraction is a free data retrieval call binding the contract method 0x6d0042b8.
//
// Solidity: function defaultRoyaltyFraction() view returns(uint256)
func (_Nftkey0 *Nftkey0CallerSession) DefaultRoyaltyFraction() (*big.Int, error) {
	return _Nftkey0.Contract.DefaultRoyaltyFraction(&_Nftkey0.CallOpts)
}

// GetBidderBids is a free data retrieval call binding the contract method 0xcf6ac953.
//
// Solidity: function getBidderBids(address erc721Address, address bidder, uint256 from, uint256 size) view returns((uint256,uint256,address,uint256)[] bidderBids)
func (_Nftkey0 *Nftkey0Caller) GetBidderBids(opts *bind.CallOpts, erc721Address common.Address, bidder common.Address, from *big.Int, size *big.Int) ([]INFTKEYMarketplaceV2Bid, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "getBidderBids", erc721Address, bidder, from, size)

	if err != nil {
		return *new([]INFTKEYMarketplaceV2Bid), err
	}

	out0 := *abi.ConvertType(out[0], new([]INFTKEYMarketplaceV2Bid)).(*[]INFTKEYMarketplaceV2Bid)

	return out0, err

}

// GetBidderBids is a free data retrieval call binding the contract method 0xcf6ac953.
//
// Solidity: function getBidderBids(address erc721Address, address bidder, uint256 from, uint256 size) view returns((uint256,uint256,address,uint256)[] bidderBids)
func (_Nftkey0 *Nftkey0Session) GetBidderBids(erc721Address common.Address, bidder common.Address, from *big.Int, size *big.Int) ([]INFTKEYMarketplaceV2Bid, error) {
	return _Nftkey0.Contract.GetBidderBids(&_Nftkey0.CallOpts, erc721Address, bidder, from, size)
}

// GetBidderBids is a free data retrieval call binding the contract method 0xcf6ac953.
//
// Solidity: function getBidderBids(address erc721Address, address bidder, uint256 from, uint256 size) view returns((uint256,uint256,address,uint256)[] bidderBids)
func (_Nftkey0 *Nftkey0CallerSession) GetBidderBids(erc721Address common.Address, bidder common.Address, from *big.Int, size *big.Int) ([]INFTKEYMarketplaceV2Bid, error) {
	return _Nftkey0.Contract.GetBidderBids(&_Nftkey0.CallOpts, erc721Address, bidder, from, size)
}

// GetBidderTokenBid is a free data retrieval call binding the contract method 0x90bc4e37.
//
// Solidity: function getBidderTokenBid(address erc721Address, uint256 tokenId, address bidder) view returns((uint256,uint256,address,uint256) validBid)
func (_Nftkey0 *Nftkey0Caller) GetBidderTokenBid(opts *bind.CallOpts, erc721Address common.Address, tokenId *big.Int, bidder common.Address) (INFTKEYMarketplaceV2Bid, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "getBidderTokenBid", erc721Address, tokenId, bidder)

	if err != nil {
		return *new(INFTKEYMarketplaceV2Bid), err
	}

	out0 := *abi.ConvertType(out[0], new(INFTKEYMarketplaceV2Bid)).(*INFTKEYMarketplaceV2Bid)

	return out0, err

}

// GetBidderTokenBid is a free data retrieval call binding the contract method 0x90bc4e37.
//
// Solidity: function getBidderTokenBid(address erc721Address, uint256 tokenId, address bidder) view returns((uint256,uint256,address,uint256) validBid)
func (_Nftkey0 *Nftkey0Session) GetBidderTokenBid(erc721Address common.Address, tokenId *big.Int, bidder common.Address) (INFTKEYMarketplaceV2Bid, error) {
	return _Nftkey0.Contract.GetBidderTokenBid(&_Nftkey0.CallOpts, erc721Address, tokenId, bidder)
}

// GetBidderTokenBid is a free data retrieval call binding the contract method 0x90bc4e37.
//
// Solidity: function getBidderTokenBid(address erc721Address, uint256 tokenId, address bidder) view returns((uint256,uint256,address,uint256) validBid)
func (_Nftkey0 *Nftkey0CallerSession) GetBidderTokenBid(erc721Address common.Address, tokenId *big.Int, bidder common.Address) (INFTKEYMarketplaceV2Bid, error) {
	return _Nftkey0.Contract.GetBidderTokenBid(&_Nftkey0.CallOpts, erc721Address, tokenId, bidder)
}

// GetTokenBids is a free data retrieval call binding the contract method 0x66c1e8bf.
//
// Solidity: function getTokenBids(address erc721Address, uint256 tokenId) view returns((uint256,uint256,address,uint256)[] bids)
func (_Nftkey0 *Nftkey0Caller) GetTokenBids(opts *bind.CallOpts, erc721Address common.Address, tokenId *big.Int) ([]INFTKEYMarketplaceV2Bid, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "getTokenBids", erc721Address, tokenId)

	if err != nil {
		return *new([]INFTKEYMarketplaceV2Bid), err
	}

	out0 := *abi.ConvertType(out[0], new([]INFTKEYMarketplaceV2Bid)).(*[]INFTKEYMarketplaceV2Bid)

	return out0, err

}

// GetTokenBids is a free data retrieval call binding the contract method 0x66c1e8bf.
//
// Solidity: function getTokenBids(address erc721Address, uint256 tokenId) view returns((uint256,uint256,address,uint256)[] bids)
func (_Nftkey0 *Nftkey0Session) GetTokenBids(erc721Address common.Address, tokenId *big.Int) ([]INFTKEYMarketplaceV2Bid, error) {
	return _Nftkey0.Contract.GetTokenBids(&_Nftkey0.CallOpts, erc721Address, tokenId)
}

// GetTokenBids is a free data retrieval call binding the contract method 0x66c1e8bf.
//
// Solidity: function getTokenBids(address erc721Address, uint256 tokenId) view returns((uint256,uint256,address,uint256)[] bids)
func (_Nftkey0 *Nftkey0CallerSession) GetTokenBids(erc721Address common.Address, tokenId *big.Int) ([]INFTKEYMarketplaceV2Bid, error) {
	return _Nftkey0.Contract.GetTokenBids(&_Nftkey0.CallOpts, erc721Address, tokenId)
}

// GetTokenHighestBid is a free data retrieval call binding the contract method 0x1f77a820.
//
// Solidity: function getTokenHighestBid(address erc721Address, uint256 tokenId) view returns((uint256,uint256,address,uint256) highestBid)
func (_Nftkey0 *Nftkey0Caller) GetTokenHighestBid(opts *bind.CallOpts, erc721Address common.Address, tokenId *big.Int) (INFTKEYMarketplaceV2Bid, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "getTokenHighestBid", erc721Address, tokenId)

	if err != nil {
		return *new(INFTKEYMarketplaceV2Bid), err
	}

	out0 := *abi.ConvertType(out[0], new(INFTKEYMarketplaceV2Bid)).(*INFTKEYMarketplaceV2Bid)

	return out0, err

}

// GetTokenHighestBid is a free data retrieval call binding the contract method 0x1f77a820.
//
// Solidity: function getTokenHighestBid(address erc721Address, uint256 tokenId) view returns((uint256,uint256,address,uint256) highestBid)
func (_Nftkey0 *Nftkey0Session) GetTokenHighestBid(erc721Address common.Address, tokenId *big.Int) (INFTKEYMarketplaceV2Bid, error) {
	return _Nftkey0.Contract.GetTokenHighestBid(&_Nftkey0.CallOpts, erc721Address, tokenId)
}

// GetTokenHighestBid is a free data retrieval call binding the contract method 0x1f77a820.
//
// Solidity: function getTokenHighestBid(address erc721Address, uint256 tokenId) view returns((uint256,uint256,address,uint256) highestBid)
func (_Nftkey0 *Nftkey0CallerSession) GetTokenHighestBid(erc721Address common.Address, tokenId *big.Int) (INFTKEYMarketplaceV2Bid, error) {
	return _Nftkey0.Contract.GetTokenHighestBid(&_Nftkey0.CallOpts, erc721Address, tokenId)
}

// GetTokenHighestBids is a free data retrieval call binding the contract method 0x2c20d1b4.
//
// Solidity: function getTokenHighestBids(address erc721Address, uint256 from, uint256 size) view returns((uint256,uint256,address,uint256)[] highestBids)
func (_Nftkey0 *Nftkey0Caller) GetTokenHighestBids(opts *bind.CallOpts, erc721Address common.Address, from *big.Int, size *big.Int) ([]INFTKEYMarketplaceV2Bid, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "getTokenHighestBids", erc721Address, from, size)

	if err != nil {
		return *new([]INFTKEYMarketplaceV2Bid), err
	}

	out0 := *abi.ConvertType(out[0], new([]INFTKEYMarketplaceV2Bid)).(*[]INFTKEYMarketplaceV2Bid)

	return out0, err

}

// GetTokenHighestBids is a free data retrieval call binding the contract method 0x2c20d1b4.
//
// Solidity: function getTokenHighestBids(address erc721Address, uint256 from, uint256 size) view returns((uint256,uint256,address,uint256)[] highestBids)
func (_Nftkey0 *Nftkey0Session) GetTokenHighestBids(erc721Address common.Address, from *big.Int, size *big.Int) ([]INFTKEYMarketplaceV2Bid, error) {
	return _Nftkey0.Contract.GetTokenHighestBids(&_Nftkey0.CallOpts, erc721Address, from, size)
}

// GetTokenHighestBids is a free data retrieval call binding the contract method 0x2c20d1b4.
//
// Solidity: function getTokenHighestBids(address erc721Address, uint256 from, uint256 size) view returns((uint256,uint256,address,uint256)[] highestBids)
func (_Nftkey0 *Nftkey0CallerSession) GetTokenHighestBids(erc721Address common.Address, from *big.Int, size *big.Int) ([]INFTKEYMarketplaceV2Bid, error) {
	return _Nftkey0.Contract.GetTokenHighestBids(&_Nftkey0.CallOpts, erc721Address, from, size)
}

// GetTokenListing is a free data retrieval call binding the contract method 0x96f97164.
//
// Solidity: function getTokenListing(address erc721Address, uint256 tokenId) view returns((uint256,uint256,address,uint256) validListing)
func (_Nftkey0 *Nftkey0Caller) GetTokenListing(opts *bind.CallOpts, erc721Address common.Address, tokenId *big.Int) (INFTKEYMarketplaceV2Listing, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "getTokenListing", erc721Address, tokenId)

	if err != nil {
		return *new(INFTKEYMarketplaceV2Listing), err
	}

	out0 := *abi.ConvertType(out[0], new(INFTKEYMarketplaceV2Listing)).(*INFTKEYMarketplaceV2Listing)

	return out0, err

}

// GetTokenListing is a free data retrieval call binding the contract method 0x96f97164.
//
// Solidity: function getTokenListing(address erc721Address, uint256 tokenId) view returns((uint256,uint256,address,uint256) validListing)
func (_Nftkey0 *Nftkey0Session) GetTokenListing(erc721Address common.Address, tokenId *big.Int) (INFTKEYMarketplaceV2Listing, error) {
	return _Nftkey0.Contract.GetTokenListing(&_Nftkey0.CallOpts, erc721Address, tokenId)
}

// GetTokenListing is a free data retrieval call binding the contract method 0x96f97164.
//
// Solidity: function getTokenListing(address erc721Address, uint256 tokenId) view returns((uint256,uint256,address,uint256) validListing)
func (_Nftkey0 *Nftkey0CallerSession) GetTokenListing(erc721Address common.Address, tokenId *big.Int) (INFTKEYMarketplaceV2Listing, error) {
	return _Nftkey0.Contract.GetTokenListing(&_Nftkey0.CallOpts, erc721Address, tokenId)
}

// GetTokenListings is a free data retrieval call binding the contract method 0x45105cb1.
//
// Solidity: function getTokenListings(address erc721Address, uint256 from, uint256 size) view returns((uint256,uint256,address,uint256)[] listings)
func (_Nftkey0 *Nftkey0Caller) GetTokenListings(opts *bind.CallOpts, erc721Address common.Address, from *big.Int, size *big.Int) ([]INFTKEYMarketplaceV2Listing, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "getTokenListings", erc721Address, from, size)

	if err != nil {
		return *new([]INFTKEYMarketplaceV2Listing), err
	}

	out0 := *abi.ConvertType(out[0], new([]INFTKEYMarketplaceV2Listing)).(*[]INFTKEYMarketplaceV2Listing)

	return out0, err

}

// GetTokenListings is a free data retrieval call binding the contract method 0x45105cb1.
//
// Solidity: function getTokenListings(address erc721Address, uint256 from, uint256 size) view returns((uint256,uint256,address,uint256)[] listings)
func (_Nftkey0 *Nftkey0Session) GetTokenListings(erc721Address common.Address, from *big.Int, size *big.Int) ([]INFTKEYMarketplaceV2Listing, error) {
	return _Nftkey0.Contract.GetTokenListings(&_Nftkey0.CallOpts, erc721Address, from, size)
}

// GetTokenListings is a free data retrieval call binding the contract method 0x45105cb1.
//
// Solidity: function getTokenListings(address erc721Address, uint256 from, uint256 size) view returns((uint256,uint256,address,uint256)[] listings)
func (_Nftkey0 *Nftkey0CallerSession) GetTokenListings(erc721Address common.Address, from *big.Int, size *big.Int) ([]INFTKEYMarketplaceV2Listing, error) {
	return _Nftkey0.Contract.GetTokenListings(&_Nftkey0.CallOpts, erc721Address, from, size)
}

// IsTradingEnabled is a free data retrieval call binding the contract method 0x064a59d0.
//
// Solidity: function isTradingEnabled() view returns(bool)
func (_Nftkey0 *Nftkey0Caller) IsTradingEnabled(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "isTradingEnabled")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsTradingEnabled is a free data retrieval call binding the contract method 0x064a59d0.
//
// Solidity: function isTradingEnabled() view returns(bool)
func (_Nftkey0 *Nftkey0Session) IsTradingEnabled() (bool, error) {
	return _Nftkey0.Contract.IsTradingEnabled(&_Nftkey0.CallOpts)
}

// IsTradingEnabled is a free data retrieval call binding the contract method 0x064a59d0.
//
// Solidity: function isTradingEnabled() view returns(bool)
func (_Nftkey0 *Nftkey0CallerSession) IsTradingEnabled() (bool, error) {
	return _Nftkey0.Contract.IsTradingEnabled(&_Nftkey0.CallOpts)
}

// NumTokenListings is a free data retrieval call binding the contract method 0xf8e7b936.
//
// Solidity: function numTokenListings(address erc721Address) view returns(uint256)
func (_Nftkey0 *Nftkey0Caller) NumTokenListings(opts *bind.CallOpts, erc721Address common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "numTokenListings", erc721Address)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// NumTokenListings is a free data retrieval call binding the contract method 0xf8e7b936.
//
// Solidity: function numTokenListings(address erc721Address) view returns(uint256)
func (_Nftkey0 *Nftkey0Session) NumTokenListings(erc721Address common.Address) (*big.Int, error) {
	return _Nftkey0.Contract.NumTokenListings(&_Nftkey0.CallOpts, erc721Address)
}

// NumTokenListings is a free data retrieval call binding the contract method 0xf8e7b936.
//
// Solidity: function numTokenListings(address erc721Address) view returns(uint256)
func (_Nftkey0 *Nftkey0CallerSession) NumTokenListings(erc721Address common.Address) (*big.Int, error) {
	return _Nftkey0.Contract.NumTokenListings(&_Nftkey0.CallOpts, erc721Address)
}

// NumTokenWithBids is a free data retrieval call binding the contract method 0xeb635ab8.
//
// Solidity: function numTokenWithBids(address erc721Address) view returns(uint256)
func (_Nftkey0 *Nftkey0Caller) NumTokenWithBids(opts *bind.CallOpts, erc721Address common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "numTokenWithBids", erc721Address)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// NumTokenWithBids is a free data retrieval call binding the contract method 0xeb635ab8.
//
// Solidity: function numTokenWithBids(address erc721Address) view returns(uint256)
func (_Nftkey0 *Nftkey0Session) NumTokenWithBids(erc721Address common.Address) (*big.Int, error) {
	return _Nftkey0.Contract.NumTokenWithBids(&_Nftkey0.CallOpts, erc721Address)
}

// NumTokenWithBids is a free data retrieval call binding the contract method 0xeb635ab8.
//
// Solidity: function numTokenWithBids(address erc721Address) view returns(uint256)
func (_Nftkey0 *Nftkey0CallerSession) NumTokenWithBids(erc721Address common.Address) (*big.Int, error) {
	return _Nftkey0.Contract.NumTokenWithBids(&_Nftkey0.CallOpts, erc721Address)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Nftkey0 *Nftkey0Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Nftkey0 *Nftkey0Session) Owner() (common.Address, error) {
	return _Nftkey0.Contract.Owner(&_Nftkey0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Nftkey0 *Nftkey0CallerSession) Owner() (common.Address, error) {
	return _Nftkey0.Contract.Owner(&_Nftkey0.CallOpts)
}

// PaymentToken is a free data retrieval call binding the contract method 0x3013ce29.
//
// Solidity: function paymentToken() view returns(address)
func (_Nftkey0 *Nftkey0Caller) PaymentToken(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "paymentToken")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// PaymentToken is a free data retrieval call binding the contract method 0x3013ce29.
//
// Solidity: function paymentToken() view returns(address)
func (_Nftkey0 *Nftkey0Session) PaymentToken() (common.Address, error) {
	return _Nftkey0.Contract.PaymentToken(&_Nftkey0.CallOpts)
}

// PaymentToken is a free data retrieval call binding the contract method 0x3013ce29.
//
// Solidity: function paymentToken() view returns(address)
func (_Nftkey0 *Nftkey0CallerSession) PaymentToken() (common.Address, error) {
	return _Nftkey0.Contract.PaymentToken(&_Nftkey0.CallOpts)
}

// Royalty is a free data retrieval call binding the contract method 0x861b69d6.
//
// Solidity: function royalty(address erc721Address) view returns((address,uint256,address))
func (_Nftkey0 *Nftkey0Caller) Royalty(opts *bind.CallOpts, erc721Address common.Address) (INFTKEYMarketplaceRoyaltyERC721CollectionRoyalty, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "royalty", erc721Address)

	if err != nil {
		return *new(INFTKEYMarketplaceRoyaltyERC721CollectionRoyalty), err
	}

	out0 := *abi.ConvertType(out[0], new(INFTKEYMarketplaceRoyaltyERC721CollectionRoyalty)).(*INFTKEYMarketplaceRoyaltyERC721CollectionRoyalty)

	return out0, err

}

// Royalty is a free data retrieval call binding the contract method 0x861b69d6.
//
// Solidity: function royalty(address erc721Address) view returns((address,uint256,address))
func (_Nftkey0 *Nftkey0Session) Royalty(erc721Address common.Address) (INFTKEYMarketplaceRoyaltyERC721CollectionRoyalty, error) {
	return _Nftkey0.Contract.Royalty(&_Nftkey0.CallOpts, erc721Address)
}

// Royalty is a free data retrieval call binding the contract method 0x861b69d6.
//
// Solidity: function royalty(address erc721Address) view returns((address,uint256,address))
func (_Nftkey0 *Nftkey0CallerSession) Royalty(erc721Address common.Address) (INFTKEYMarketplaceRoyaltyERC721CollectionRoyalty, error) {
	return _Nftkey0.Contract.Royalty(&_Nftkey0.CallOpts, erc721Address)
}

// RoyaltyUpperLimit is a free data retrieval call binding the contract method 0xee8ef34d.
//
// Solidity: function royaltyUpperLimit() view returns(uint256)
func (_Nftkey0 *Nftkey0Caller) RoyaltyUpperLimit(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "royaltyUpperLimit")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// RoyaltyUpperLimit is a free data retrieval call binding the contract method 0xee8ef34d.
//
// Solidity: function royaltyUpperLimit() view returns(uint256)
func (_Nftkey0 *Nftkey0Session) RoyaltyUpperLimit() (*big.Int, error) {
	return _Nftkey0.Contract.RoyaltyUpperLimit(&_Nftkey0.CallOpts)
}

// RoyaltyUpperLimit is a free data retrieval call binding the contract method 0xee8ef34d.
//
// Solidity: function royaltyUpperLimit() view returns(uint256)
func (_Nftkey0 *Nftkey0CallerSession) RoyaltyUpperLimit() (*big.Int, error) {
	return _Nftkey0.Contract.RoyaltyUpperLimit(&_Nftkey0.CallOpts)
}

// ServiceFee is a free data retrieval call binding the contract method 0x8abdf5aa.
//
// Solidity: function serviceFee() view returns(uint8)
func (_Nftkey0 *Nftkey0Caller) ServiceFee(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Nftkey0.contract.Call(opts, &out, "serviceFee")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// ServiceFee is a free data retrieval call binding the contract method 0x8abdf5aa.
//
// Solidity: function serviceFee() view returns(uint8)
func (_Nftkey0 *Nftkey0Session) ServiceFee() (uint8, error) {
	return _Nftkey0.Contract.ServiceFee(&_Nftkey0.CallOpts)
}

// ServiceFee is a free data retrieval call binding the contract method 0x8abdf5aa.
//
// Solidity: function serviceFee() view returns(uint8)
func (_Nftkey0 *Nftkey0CallerSession) ServiceFee() (uint8, error) {
	return _Nftkey0.Contract.ServiceFee(&_Nftkey0.CallOpts)
}

// AcceptBidForToken is a paid mutator transaction binding the contract method 0xeb3e87b9.
//
// Solidity: function acceptBidForToken(address erc721Address, uint256 tokenId, address bidder, uint256 value) returns()
func (_Nftkey0 *Nftkey0Transactor) AcceptBidForToken(opts *bind.TransactOpts, erc721Address common.Address, tokenId *big.Int, bidder common.Address, value *big.Int) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "acceptBidForToken", erc721Address, tokenId, bidder, value)
}

// AcceptBidForToken is a paid mutator transaction binding the contract method 0xeb3e87b9.
//
// Solidity: function acceptBidForToken(address erc721Address, uint256 tokenId, address bidder, uint256 value) returns()
func (_Nftkey0 *Nftkey0Session) AcceptBidForToken(erc721Address common.Address, tokenId *big.Int, bidder common.Address, value *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.AcceptBidForToken(&_Nftkey0.TransactOpts, erc721Address, tokenId, bidder, value)
}

// AcceptBidForToken is a paid mutator transaction binding the contract method 0xeb3e87b9.
//
// Solidity: function acceptBidForToken(address erc721Address, uint256 tokenId, address bidder, uint256 value) returns()
func (_Nftkey0 *Nftkey0TransactorSession) AcceptBidForToken(erc721Address common.Address, tokenId *big.Int, bidder common.Address, value *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.AcceptBidForToken(&_Nftkey0.TransactOpts, erc721Address, tokenId, bidder, value)
}

// BuyToken is a paid mutator transaction binding the contract method 0x68f8fc10.
//
// Solidity: function buyToken(address erc721Address, uint256 tokenId) payable returns()
func (_Nftkey0 *Nftkey0Transactor) BuyToken(opts *bind.TransactOpts, erc721Address common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "buyToken", erc721Address, tokenId)
}

// BuyToken is a paid mutator transaction binding the contract method 0x68f8fc10.
//
// Solidity: function buyToken(address erc721Address, uint256 tokenId) payable returns()
func (_Nftkey0 *Nftkey0Session) BuyToken(erc721Address common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.BuyToken(&_Nftkey0.TransactOpts, erc721Address, tokenId)
}

// BuyToken is a paid mutator transaction binding the contract method 0x68f8fc10.
//
// Solidity: function buyToken(address erc721Address, uint256 tokenId) payable returns()
func (_Nftkey0 *Nftkey0TransactorSession) BuyToken(erc721Address common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.BuyToken(&_Nftkey0.TransactOpts, erc721Address, tokenId)
}

// ChangeMarketplaceStatus is a paid mutator transaction binding the contract method 0xb6be53ba.
//
// Solidity: function changeMarketplaceStatus(bool enabled) returns()
func (_Nftkey0 *Nftkey0Transactor) ChangeMarketplaceStatus(opts *bind.TransactOpts, enabled bool) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "changeMarketplaceStatus", enabled)
}

// ChangeMarketplaceStatus is a paid mutator transaction binding the contract method 0xb6be53ba.
//
// Solidity: function changeMarketplaceStatus(bool enabled) returns()
func (_Nftkey0 *Nftkey0Session) ChangeMarketplaceStatus(enabled bool) (*types.Transaction, error) {
	return _Nftkey0.Contract.ChangeMarketplaceStatus(&_Nftkey0.TransactOpts, enabled)
}

// ChangeMarketplaceStatus is a paid mutator transaction binding the contract method 0xb6be53ba.
//
// Solidity: function changeMarketplaceStatus(bool enabled) returns()
func (_Nftkey0 *Nftkey0TransactorSession) ChangeMarketplaceStatus(enabled bool) (*types.Transaction, error) {
	return _Nftkey0.Contract.ChangeMarketplaceStatus(&_Nftkey0.TransactOpts, enabled)
}

// ChangeMaxActionTimeLimit is a paid mutator transaction binding the contract method 0xa3c0b5f0.
//
// Solidity: function changeMaxActionTimeLimit(uint256 timeInSec) returns()
func (_Nftkey0 *Nftkey0Transactor) ChangeMaxActionTimeLimit(opts *bind.TransactOpts, timeInSec *big.Int) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "changeMaxActionTimeLimit", timeInSec)
}

// ChangeMaxActionTimeLimit is a paid mutator transaction binding the contract method 0xa3c0b5f0.
//
// Solidity: function changeMaxActionTimeLimit(uint256 timeInSec) returns()
func (_Nftkey0 *Nftkey0Session) ChangeMaxActionTimeLimit(timeInSec *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.ChangeMaxActionTimeLimit(&_Nftkey0.TransactOpts, timeInSec)
}

// ChangeMaxActionTimeLimit is a paid mutator transaction binding the contract method 0xa3c0b5f0.
//
// Solidity: function changeMaxActionTimeLimit(uint256 timeInSec) returns()
func (_Nftkey0 *Nftkey0TransactorSession) ChangeMaxActionTimeLimit(timeInSec *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.ChangeMaxActionTimeLimit(&_Nftkey0.TransactOpts, timeInSec)
}

// ChangeMinActionTimeLimit is a paid mutator transaction binding the contract method 0x2426fc24.
//
// Solidity: function changeMinActionTimeLimit(uint256 timeInSec) returns()
func (_Nftkey0 *Nftkey0Transactor) ChangeMinActionTimeLimit(opts *bind.TransactOpts, timeInSec *big.Int) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "changeMinActionTimeLimit", timeInSec)
}

// ChangeMinActionTimeLimit is a paid mutator transaction binding the contract method 0x2426fc24.
//
// Solidity: function changeMinActionTimeLimit(uint256 timeInSec) returns()
func (_Nftkey0 *Nftkey0Session) ChangeMinActionTimeLimit(timeInSec *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.ChangeMinActionTimeLimit(&_Nftkey0.TransactOpts, timeInSec)
}

// ChangeMinActionTimeLimit is a paid mutator transaction binding the contract method 0x2426fc24.
//
// Solidity: function changeMinActionTimeLimit(uint256 timeInSec) returns()
func (_Nftkey0 *Nftkey0TransactorSession) ChangeMinActionTimeLimit(timeInSec *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.ChangeMinActionTimeLimit(&_Nftkey0.TransactOpts, timeInSec)
}

// ChangeSeriveFee is a paid mutator transaction binding the contract method 0xf8ad6f62.
//
// Solidity: function changeSeriveFee(uint8 serviceFeeFraction_) returns()
func (_Nftkey0 *Nftkey0Transactor) ChangeSeriveFee(opts *bind.TransactOpts, serviceFeeFraction_ uint8) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "changeSeriveFee", serviceFeeFraction_)
}

// ChangeSeriveFee is a paid mutator transaction binding the contract method 0xf8ad6f62.
//
// Solidity: function changeSeriveFee(uint8 serviceFeeFraction_) returns()
func (_Nftkey0 *Nftkey0Session) ChangeSeriveFee(serviceFeeFraction_ uint8) (*types.Transaction, error) {
	return _Nftkey0.Contract.ChangeSeriveFee(&_Nftkey0.TransactOpts, serviceFeeFraction_)
}

// ChangeSeriveFee is a paid mutator transaction binding the contract method 0xf8ad6f62.
//
// Solidity: function changeSeriveFee(uint8 serviceFeeFraction_) returns()
func (_Nftkey0 *Nftkey0TransactorSession) ChangeSeriveFee(serviceFeeFraction_ uint8) (*types.Transaction, error) {
	return _Nftkey0.Contract.ChangeSeriveFee(&_Nftkey0.TransactOpts, serviceFeeFraction_)
}

// DelistToken is a paid mutator transaction binding the contract method 0xfeb88406.
//
// Solidity: function delistToken(address erc721Address, uint256 tokenId) returns()
func (_Nftkey0 *Nftkey0Transactor) DelistToken(opts *bind.TransactOpts, erc721Address common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "delistToken", erc721Address, tokenId)
}

// DelistToken is a paid mutator transaction binding the contract method 0xfeb88406.
//
// Solidity: function delistToken(address erc721Address, uint256 tokenId) returns()
func (_Nftkey0 *Nftkey0Session) DelistToken(erc721Address common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.DelistToken(&_Nftkey0.TransactOpts, erc721Address, tokenId)
}

// DelistToken is a paid mutator transaction binding the contract method 0xfeb88406.
//
// Solidity: function delistToken(address erc721Address, uint256 tokenId) returns()
func (_Nftkey0 *Nftkey0TransactorSession) DelistToken(erc721Address common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.DelistToken(&_Nftkey0.TransactOpts, erc721Address, tokenId)
}

// EnterBidForToken is a paid mutator transaction binding the contract method 0x4313e9bd.
//
// Solidity: function enterBidForToken(address erc721Address, uint256 tokenId, uint256 value, uint256 expireTimestamp) returns()
func (_Nftkey0 *Nftkey0Transactor) EnterBidForToken(opts *bind.TransactOpts, erc721Address common.Address, tokenId *big.Int, value *big.Int, expireTimestamp *big.Int) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "enterBidForToken", erc721Address, tokenId, value, expireTimestamp)
}

// EnterBidForToken is a paid mutator transaction binding the contract method 0x4313e9bd.
//
// Solidity: function enterBidForToken(address erc721Address, uint256 tokenId, uint256 value, uint256 expireTimestamp) returns()
func (_Nftkey0 *Nftkey0Session) EnterBidForToken(erc721Address common.Address, tokenId *big.Int, value *big.Int, expireTimestamp *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.EnterBidForToken(&_Nftkey0.TransactOpts, erc721Address, tokenId, value, expireTimestamp)
}

// EnterBidForToken is a paid mutator transaction binding the contract method 0x4313e9bd.
//
// Solidity: function enterBidForToken(address erc721Address, uint256 tokenId, uint256 value, uint256 expireTimestamp) returns()
func (_Nftkey0 *Nftkey0TransactorSession) EnterBidForToken(erc721Address common.Address, tokenId *big.Int, value *big.Int, expireTimestamp *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.EnterBidForToken(&_Nftkey0.TransactOpts, erc721Address, tokenId, value, expireTimestamp)
}

// ListToken is a paid mutator transaction binding the contract method 0xb43d901d.
//
// Solidity: function listToken(address erc721Address, uint256 tokenId, uint256 value, uint256 expireTimestamp) returns()
func (_Nftkey0 *Nftkey0Transactor) ListToken(opts *bind.TransactOpts, erc721Address common.Address, tokenId *big.Int, value *big.Int, expireTimestamp *big.Int) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "listToken", erc721Address, tokenId, value, expireTimestamp)
}

// ListToken is a paid mutator transaction binding the contract method 0xb43d901d.
//
// Solidity: function listToken(address erc721Address, uint256 tokenId, uint256 value, uint256 expireTimestamp) returns()
func (_Nftkey0 *Nftkey0Session) ListToken(erc721Address common.Address, tokenId *big.Int, value *big.Int, expireTimestamp *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.ListToken(&_Nftkey0.TransactOpts, erc721Address, tokenId, value, expireTimestamp)
}

// ListToken is a paid mutator transaction binding the contract method 0xb43d901d.
//
// Solidity: function listToken(address erc721Address, uint256 tokenId, uint256 value, uint256 expireTimestamp) returns()
func (_Nftkey0 *Nftkey0TransactorSession) ListToken(erc721Address common.Address, tokenId *big.Int, value *big.Int, expireTimestamp *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.ListToken(&_Nftkey0.TransactOpts, erc721Address, tokenId, value, expireTimestamp)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Nftkey0 *Nftkey0Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Nftkey0 *Nftkey0Session) RenounceOwnership() (*types.Transaction, error) {
	return _Nftkey0.Contract.RenounceOwnership(&_Nftkey0.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Nftkey0 *Nftkey0TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Nftkey0.Contract.RenounceOwnership(&_Nftkey0.TransactOpts)
}

// SetRoyalty is a paid mutator transaction binding the contract method 0x55c338aa.
//
// Solidity: function setRoyalty(address erc721Address, address newRecipient, uint256 feeFraction) returns()
func (_Nftkey0 *Nftkey0Transactor) SetRoyalty(opts *bind.TransactOpts, erc721Address common.Address, newRecipient common.Address, feeFraction *big.Int) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "setRoyalty", erc721Address, newRecipient, feeFraction)
}

// SetRoyalty is a paid mutator transaction binding the contract method 0x55c338aa.
//
// Solidity: function setRoyalty(address erc721Address, address newRecipient, uint256 feeFraction) returns()
func (_Nftkey0 *Nftkey0Session) SetRoyalty(erc721Address common.Address, newRecipient common.Address, feeFraction *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.SetRoyalty(&_Nftkey0.TransactOpts, erc721Address, newRecipient, feeFraction)
}

// SetRoyalty is a paid mutator transaction binding the contract method 0x55c338aa.
//
// Solidity: function setRoyalty(address erc721Address, address newRecipient, uint256 feeFraction) returns()
func (_Nftkey0 *Nftkey0TransactorSession) SetRoyalty(erc721Address common.Address, newRecipient common.Address, feeFraction *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.SetRoyalty(&_Nftkey0.TransactOpts, erc721Address, newRecipient, feeFraction)
}

// SetRoyaltyForCollection is a paid mutator transaction binding the contract method 0x19d334cb.
//
// Solidity: function setRoyaltyForCollection(address erc721Address, address newRecipient, uint256 feeFraction) returns()
func (_Nftkey0 *Nftkey0Transactor) SetRoyaltyForCollection(opts *bind.TransactOpts, erc721Address common.Address, newRecipient common.Address, feeFraction *big.Int) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "setRoyaltyForCollection", erc721Address, newRecipient, feeFraction)
}

// SetRoyaltyForCollection is a paid mutator transaction binding the contract method 0x19d334cb.
//
// Solidity: function setRoyaltyForCollection(address erc721Address, address newRecipient, uint256 feeFraction) returns()
func (_Nftkey0 *Nftkey0Session) SetRoyaltyForCollection(erc721Address common.Address, newRecipient common.Address, feeFraction *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.SetRoyaltyForCollection(&_Nftkey0.TransactOpts, erc721Address, newRecipient, feeFraction)
}

// SetRoyaltyForCollection is a paid mutator transaction binding the contract method 0x19d334cb.
//
// Solidity: function setRoyaltyForCollection(address erc721Address, address newRecipient, uint256 feeFraction) returns()
func (_Nftkey0 *Nftkey0TransactorSession) SetRoyaltyForCollection(erc721Address common.Address, newRecipient common.Address, feeFraction *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.SetRoyaltyForCollection(&_Nftkey0.TransactOpts, erc721Address, newRecipient, feeFraction)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Nftkey0 *Nftkey0Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Nftkey0 *Nftkey0Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Nftkey0.Contract.TransferOwnership(&_Nftkey0.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Nftkey0 *Nftkey0TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Nftkey0.Contract.TransferOwnership(&_Nftkey0.TransactOpts, newOwner)
}

// UpdateRoyaltyUpperLimit is a paid mutator transaction binding the contract method 0xcdea490d.
//
// Solidity: function updateRoyaltyUpperLimit(uint256 _newUpperLimit) returns()
func (_Nftkey0 *Nftkey0Transactor) UpdateRoyaltyUpperLimit(opts *bind.TransactOpts, _newUpperLimit *big.Int) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "updateRoyaltyUpperLimit", _newUpperLimit)
}

// UpdateRoyaltyUpperLimit is a paid mutator transaction binding the contract method 0xcdea490d.
//
// Solidity: function updateRoyaltyUpperLimit(uint256 _newUpperLimit) returns()
func (_Nftkey0 *Nftkey0Session) UpdateRoyaltyUpperLimit(_newUpperLimit *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.UpdateRoyaltyUpperLimit(&_Nftkey0.TransactOpts, _newUpperLimit)
}

// UpdateRoyaltyUpperLimit is a paid mutator transaction binding the contract method 0xcdea490d.
//
// Solidity: function updateRoyaltyUpperLimit(uint256 _newUpperLimit) returns()
func (_Nftkey0 *Nftkey0TransactorSession) UpdateRoyaltyUpperLimit(_newUpperLimit *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.UpdateRoyaltyUpperLimit(&_Nftkey0.TransactOpts, _newUpperLimit)
}

// WithdrawBidForToken is a paid mutator transaction binding the contract method 0x0609d095.
//
// Solidity: function withdrawBidForToken(address erc721Address, uint256 tokenId) returns()
func (_Nftkey0 *Nftkey0Transactor) WithdrawBidForToken(opts *bind.TransactOpts, erc721Address common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Nftkey0.contract.Transact(opts, "withdrawBidForToken", erc721Address, tokenId)
}

// WithdrawBidForToken is a paid mutator transaction binding the contract method 0x0609d095.
//
// Solidity: function withdrawBidForToken(address erc721Address, uint256 tokenId) returns()
func (_Nftkey0 *Nftkey0Session) WithdrawBidForToken(erc721Address common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.WithdrawBidForToken(&_Nftkey0.TransactOpts, erc721Address, tokenId)
}

// WithdrawBidForToken is a paid mutator transaction binding the contract method 0x0609d095.
//
// Solidity: function withdrawBidForToken(address erc721Address, uint256 tokenId) returns()
func (_Nftkey0 *Nftkey0TransactorSession) WithdrawBidForToken(erc721Address common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Nftkey0.Contract.WithdrawBidForToken(&_Nftkey0.TransactOpts, erc721Address, tokenId)
}

// Nftkey0OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Nftkey0 contract.
type Nftkey0OwnershipTransferredIterator struct {
	Event *Nftkey0OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Nftkey0OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Nftkey0OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Nftkey0OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Nftkey0OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Nftkey0OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Nftkey0OwnershipTransferred represents a OwnershipTransferred event raised by the Nftkey0 contract.
type Nftkey0OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Nftkey0 *Nftkey0Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Nftkey0OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Nftkey0.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Nftkey0OwnershipTransferredIterator{contract: _Nftkey0.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Nftkey0 *Nftkey0Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Nftkey0OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Nftkey0.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Nftkey0OwnershipTransferred)
				if err := _Nftkey0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Nftkey0 *Nftkey0Filterer) ParseOwnershipTransferred(log types.Log) (*Nftkey0OwnershipTransferred, error) {
	event := new(Nftkey0OwnershipTransferred)
	if err := _Nftkey0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Nftkey0SetRoyaltyIterator is returned from FilterSetRoyalty and is used to iterate over the raw logs and unpacked data for SetRoyalty events raised by the Nftkey0 contract.
type Nftkey0SetRoyaltyIterator struct {
	Event *Nftkey0SetRoyalty // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Nftkey0SetRoyaltyIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Nftkey0SetRoyalty)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Nftkey0SetRoyalty)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Nftkey0SetRoyaltyIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Nftkey0SetRoyaltyIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Nftkey0SetRoyalty represents a SetRoyalty event raised by the Nftkey0 contract.
type Nftkey0SetRoyalty struct {
	Erc721Address common.Address
	Recipient     common.Address
	FeeFraction   *big.Int
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterSetRoyalty is a free log retrieval operation binding the contract event 0xb6b61d78ac5372b51940cf3b322ea21839c456cade69acdf1b9fb9a6f79d6ff7.
//
// Solidity: event SetRoyalty(address indexed erc721Address, address indexed recipient, uint256 feeFraction)
func (_Nftkey0 *Nftkey0Filterer) FilterSetRoyalty(opts *bind.FilterOpts, erc721Address []common.Address, recipient []common.Address) (*Nftkey0SetRoyaltyIterator, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var recipientRule []interface{}
	for _, recipientItem := range recipient {
		recipientRule = append(recipientRule, recipientItem)
	}

	logs, sub, err := _Nftkey0.contract.FilterLogs(opts, "SetRoyalty", erc721AddressRule, recipientRule)
	if err != nil {
		return nil, err
	}
	return &Nftkey0SetRoyaltyIterator{contract: _Nftkey0.contract, event: "SetRoyalty", logs: logs, sub: sub}, nil
}

// WatchSetRoyalty is a free log subscription operation binding the contract event 0xb6b61d78ac5372b51940cf3b322ea21839c456cade69acdf1b9fb9a6f79d6ff7.
//
// Solidity: event SetRoyalty(address indexed erc721Address, address indexed recipient, uint256 feeFraction)
func (_Nftkey0 *Nftkey0Filterer) WatchSetRoyalty(opts *bind.WatchOpts, sink chan<- *Nftkey0SetRoyalty, erc721Address []common.Address, recipient []common.Address) (event.Subscription, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var recipientRule []interface{}
	for _, recipientItem := range recipient {
		recipientRule = append(recipientRule, recipientItem)
	}

	logs, sub, err := _Nftkey0.contract.WatchLogs(opts, "SetRoyalty", erc721AddressRule, recipientRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Nftkey0SetRoyalty)
				if err := _Nftkey0.contract.UnpackLog(event, "SetRoyalty", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseSetRoyalty is a log parse operation binding the contract event 0xb6b61d78ac5372b51940cf3b322ea21839c456cade69acdf1b9fb9a6f79d6ff7.
//
// Solidity: event SetRoyalty(address indexed erc721Address, address indexed recipient, uint256 feeFraction)
func (_Nftkey0 *Nftkey0Filterer) ParseSetRoyalty(log types.Log) (*Nftkey0SetRoyalty, error) {
	event := new(Nftkey0SetRoyalty)
	if err := _Nftkey0.contract.UnpackLog(event, "SetRoyalty", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Nftkey0TokenBidAcceptedIterator is returned from FilterTokenBidAccepted and is used to iterate over the raw logs and unpacked data for TokenBidAccepted events raised by the Nftkey0 contract.
type Nftkey0TokenBidAcceptedIterator struct {
	Event *Nftkey0TokenBidAccepted // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Nftkey0TokenBidAcceptedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Nftkey0TokenBidAccepted)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Nftkey0TokenBidAccepted)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Nftkey0TokenBidAcceptedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Nftkey0TokenBidAcceptedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Nftkey0TokenBidAccepted represents a TokenBidAccepted event raised by the Nftkey0 contract.
type Nftkey0TokenBidAccepted struct {
	Erc721Address common.Address
	TokenId       *big.Int
	Seller        common.Address
	Bid           INFTKEYMarketplaceV2Bid
	ServiceFee    *big.Int
	RoyaltyFee    *big.Int
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterTokenBidAccepted is a free log retrieval operation binding the contract event 0xcc8a6de82515ca1ae870ff05651038b858e8bcd1b5143c342987b6dc3c343453.
//
// Solidity: event TokenBidAccepted(address indexed erc721Address, uint256 indexed tokenId, address indexed seller, (uint256,uint256,address,uint256) bid, uint256 serviceFee, uint256 royaltyFee)
func (_Nftkey0 *Nftkey0Filterer) FilterTokenBidAccepted(opts *bind.FilterOpts, erc721Address []common.Address, tokenId []*big.Int, seller []common.Address) (*Nftkey0TokenBidAcceptedIterator, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}
	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	logs, sub, err := _Nftkey0.contract.FilterLogs(opts, "TokenBidAccepted", erc721AddressRule, tokenIdRule, sellerRule)
	if err != nil {
		return nil, err
	}
	return &Nftkey0TokenBidAcceptedIterator{contract: _Nftkey0.contract, event: "TokenBidAccepted", logs: logs, sub: sub}, nil
}

// WatchTokenBidAccepted is a free log subscription operation binding the contract event 0xcc8a6de82515ca1ae870ff05651038b858e8bcd1b5143c342987b6dc3c343453.
//
// Solidity: event TokenBidAccepted(address indexed erc721Address, uint256 indexed tokenId, address indexed seller, (uint256,uint256,address,uint256) bid, uint256 serviceFee, uint256 royaltyFee)
func (_Nftkey0 *Nftkey0Filterer) WatchTokenBidAccepted(opts *bind.WatchOpts, sink chan<- *Nftkey0TokenBidAccepted, erc721Address []common.Address, tokenId []*big.Int, seller []common.Address) (event.Subscription, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}
	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	logs, sub, err := _Nftkey0.contract.WatchLogs(opts, "TokenBidAccepted", erc721AddressRule, tokenIdRule, sellerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Nftkey0TokenBidAccepted)
				if err := _Nftkey0.contract.UnpackLog(event, "TokenBidAccepted", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokenBidAccepted is a log parse operation binding the contract event 0xcc8a6de82515ca1ae870ff05651038b858e8bcd1b5143c342987b6dc3c343453.
//
// Solidity: event TokenBidAccepted(address indexed erc721Address, uint256 indexed tokenId, address indexed seller, (uint256,uint256,address,uint256) bid, uint256 serviceFee, uint256 royaltyFee)
func (_Nftkey0 *Nftkey0Filterer) ParseTokenBidAccepted(log types.Log) (*Nftkey0TokenBidAccepted, error) {
	event := new(Nftkey0TokenBidAccepted)
	if err := _Nftkey0.contract.UnpackLog(event, "TokenBidAccepted", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Nftkey0TokenBidEnteredIterator is returned from FilterTokenBidEntered and is used to iterate over the raw logs and unpacked data for TokenBidEntered events raised by the Nftkey0 contract.
type Nftkey0TokenBidEnteredIterator struct {
	Event *Nftkey0TokenBidEntered // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Nftkey0TokenBidEnteredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Nftkey0TokenBidEntered)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Nftkey0TokenBidEntered)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Nftkey0TokenBidEnteredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Nftkey0TokenBidEnteredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Nftkey0TokenBidEntered represents a TokenBidEntered event raised by the Nftkey0 contract.
type Nftkey0TokenBidEntered struct {
	Erc721Address common.Address
	TokenId       *big.Int
	Bid           INFTKEYMarketplaceV2Bid
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterTokenBidEntered is a free log retrieval operation binding the contract event 0xc547e24584f4dd2da70009d5141bf4344b59a0ce26cd8f7c5d5a28151a11f219.
//
// Solidity: event TokenBidEntered(address indexed erc721Address, uint256 indexed tokenId, (uint256,uint256,address,uint256) bid)
func (_Nftkey0 *Nftkey0Filterer) FilterTokenBidEntered(opts *bind.FilterOpts, erc721Address []common.Address, tokenId []*big.Int) (*Nftkey0TokenBidEnteredIterator, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Nftkey0.contract.FilterLogs(opts, "TokenBidEntered", erc721AddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Nftkey0TokenBidEnteredIterator{contract: _Nftkey0.contract, event: "TokenBidEntered", logs: logs, sub: sub}, nil
}

// WatchTokenBidEntered is a free log subscription operation binding the contract event 0xc547e24584f4dd2da70009d5141bf4344b59a0ce26cd8f7c5d5a28151a11f219.
//
// Solidity: event TokenBidEntered(address indexed erc721Address, uint256 indexed tokenId, (uint256,uint256,address,uint256) bid)
func (_Nftkey0 *Nftkey0Filterer) WatchTokenBidEntered(opts *bind.WatchOpts, sink chan<- *Nftkey0TokenBidEntered, erc721Address []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Nftkey0.contract.WatchLogs(opts, "TokenBidEntered", erc721AddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Nftkey0TokenBidEntered)
				if err := _Nftkey0.contract.UnpackLog(event, "TokenBidEntered", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokenBidEntered is a log parse operation binding the contract event 0xc547e24584f4dd2da70009d5141bf4344b59a0ce26cd8f7c5d5a28151a11f219.
//
// Solidity: event TokenBidEntered(address indexed erc721Address, uint256 indexed tokenId, (uint256,uint256,address,uint256) bid)
func (_Nftkey0 *Nftkey0Filterer) ParseTokenBidEntered(log types.Log) (*Nftkey0TokenBidEntered, error) {
	event := new(Nftkey0TokenBidEntered)
	if err := _Nftkey0.contract.UnpackLog(event, "TokenBidEntered", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Nftkey0TokenBidWithdrawnIterator is returned from FilterTokenBidWithdrawn and is used to iterate over the raw logs and unpacked data for TokenBidWithdrawn events raised by the Nftkey0 contract.
type Nftkey0TokenBidWithdrawnIterator struct {
	Event *Nftkey0TokenBidWithdrawn // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Nftkey0TokenBidWithdrawnIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Nftkey0TokenBidWithdrawn)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Nftkey0TokenBidWithdrawn)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Nftkey0TokenBidWithdrawnIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Nftkey0TokenBidWithdrawnIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Nftkey0TokenBidWithdrawn represents a TokenBidWithdrawn event raised by the Nftkey0 contract.
type Nftkey0TokenBidWithdrawn struct {
	Erc721Address common.Address
	TokenId       *big.Int
	Bid           INFTKEYMarketplaceV2Bid
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterTokenBidWithdrawn is a free log retrieval operation binding the contract event 0xef9d84bc41d1a54361c4bf46f5e11b7c90a3fcb4e604b1b24e0e35d0fa371604.
//
// Solidity: event TokenBidWithdrawn(address indexed erc721Address, uint256 indexed tokenId, (uint256,uint256,address,uint256) bid)
func (_Nftkey0 *Nftkey0Filterer) FilterTokenBidWithdrawn(opts *bind.FilterOpts, erc721Address []common.Address, tokenId []*big.Int) (*Nftkey0TokenBidWithdrawnIterator, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Nftkey0.contract.FilterLogs(opts, "TokenBidWithdrawn", erc721AddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Nftkey0TokenBidWithdrawnIterator{contract: _Nftkey0.contract, event: "TokenBidWithdrawn", logs: logs, sub: sub}, nil
}

// WatchTokenBidWithdrawn is a free log subscription operation binding the contract event 0xef9d84bc41d1a54361c4bf46f5e11b7c90a3fcb4e604b1b24e0e35d0fa371604.
//
// Solidity: event TokenBidWithdrawn(address indexed erc721Address, uint256 indexed tokenId, (uint256,uint256,address,uint256) bid)
func (_Nftkey0 *Nftkey0Filterer) WatchTokenBidWithdrawn(opts *bind.WatchOpts, sink chan<- *Nftkey0TokenBidWithdrawn, erc721Address []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Nftkey0.contract.WatchLogs(opts, "TokenBidWithdrawn", erc721AddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Nftkey0TokenBidWithdrawn)
				if err := _Nftkey0.contract.UnpackLog(event, "TokenBidWithdrawn", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokenBidWithdrawn is a log parse operation binding the contract event 0xef9d84bc41d1a54361c4bf46f5e11b7c90a3fcb4e604b1b24e0e35d0fa371604.
//
// Solidity: event TokenBidWithdrawn(address indexed erc721Address, uint256 indexed tokenId, (uint256,uint256,address,uint256) bid)
func (_Nftkey0 *Nftkey0Filterer) ParseTokenBidWithdrawn(log types.Log) (*Nftkey0TokenBidWithdrawn, error) {
	event := new(Nftkey0TokenBidWithdrawn)
	if err := _Nftkey0.contract.UnpackLog(event, "TokenBidWithdrawn", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Nftkey0TokenBoughtIterator is returned from FilterTokenBought and is used to iterate over the raw logs and unpacked data for TokenBought events raised by the Nftkey0 contract.
type Nftkey0TokenBoughtIterator struct {
	Event *Nftkey0TokenBought // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Nftkey0TokenBoughtIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Nftkey0TokenBought)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Nftkey0TokenBought)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Nftkey0TokenBoughtIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Nftkey0TokenBoughtIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Nftkey0TokenBought represents a TokenBought event raised by the Nftkey0 contract.
type Nftkey0TokenBought struct {
	Erc721Address common.Address
	TokenId       *big.Int
	Buyer         common.Address
	Listing       INFTKEYMarketplaceV2Listing
	ServiceFee    *big.Int
	RoyaltyFee    *big.Int
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterTokenBought is a free log retrieval operation binding the contract event 0x50a3cf2b1df7bd2994e752563ce6f85769fb50da66bbb9a9912d2d6066a6b4da.
//
// Solidity: event TokenBought(address indexed erc721Address, uint256 indexed tokenId, address indexed buyer, (uint256,uint256,address,uint256) listing, uint256 serviceFee, uint256 royaltyFee)
func (_Nftkey0 *Nftkey0Filterer) FilterTokenBought(opts *bind.FilterOpts, erc721Address []common.Address, tokenId []*big.Int, buyer []common.Address) (*Nftkey0TokenBoughtIterator, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}
	var buyerRule []interface{}
	for _, buyerItem := range buyer {
		buyerRule = append(buyerRule, buyerItem)
	}

	logs, sub, err := _Nftkey0.contract.FilterLogs(opts, "TokenBought", erc721AddressRule, tokenIdRule, buyerRule)
	if err != nil {
		return nil, err
	}
	return &Nftkey0TokenBoughtIterator{contract: _Nftkey0.contract, event: "TokenBought", logs: logs, sub: sub}, nil
}

// WatchTokenBought is a free log subscription operation binding the contract event 0x50a3cf2b1df7bd2994e752563ce6f85769fb50da66bbb9a9912d2d6066a6b4da.
//
// Solidity: event TokenBought(address indexed erc721Address, uint256 indexed tokenId, address indexed buyer, (uint256,uint256,address,uint256) listing, uint256 serviceFee, uint256 royaltyFee)
func (_Nftkey0 *Nftkey0Filterer) WatchTokenBought(opts *bind.WatchOpts, sink chan<- *Nftkey0TokenBought, erc721Address []common.Address, tokenId []*big.Int, buyer []common.Address) (event.Subscription, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}
	var buyerRule []interface{}
	for _, buyerItem := range buyer {
		buyerRule = append(buyerRule, buyerItem)
	}

	logs, sub, err := _Nftkey0.contract.WatchLogs(opts, "TokenBought", erc721AddressRule, tokenIdRule, buyerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Nftkey0TokenBought)
				if err := _Nftkey0.contract.UnpackLog(event, "TokenBought", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokenBought is a log parse operation binding the contract event 0x50a3cf2b1df7bd2994e752563ce6f85769fb50da66bbb9a9912d2d6066a6b4da.
//
// Solidity: event TokenBought(address indexed erc721Address, uint256 indexed tokenId, address indexed buyer, (uint256,uint256,address,uint256) listing, uint256 serviceFee, uint256 royaltyFee)
func (_Nftkey0 *Nftkey0Filterer) ParseTokenBought(log types.Log) (*Nftkey0TokenBought, error) {
	event := new(Nftkey0TokenBought)
	if err := _Nftkey0.contract.UnpackLog(event, "TokenBought", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Nftkey0TokenDelistedIterator is returned from FilterTokenDelisted and is used to iterate over the raw logs and unpacked data for TokenDelisted events raised by the Nftkey0 contract.
type Nftkey0TokenDelistedIterator struct {
	Event *Nftkey0TokenDelisted // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Nftkey0TokenDelistedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Nftkey0TokenDelisted)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Nftkey0TokenDelisted)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Nftkey0TokenDelistedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Nftkey0TokenDelistedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Nftkey0TokenDelisted represents a TokenDelisted event raised by the Nftkey0 contract.
type Nftkey0TokenDelisted struct {
	Erc721Address common.Address
	TokenId       *big.Int
	Listing       INFTKEYMarketplaceV2Listing
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterTokenDelisted is a free log retrieval operation binding the contract event 0x835a0a426c94e53ab00dd6cf617ba68d1fa6c9162ef7a036b80be930c9a32c53.
//
// Solidity: event TokenDelisted(address indexed erc721Address, uint256 indexed tokenId, (uint256,uint256,address,uint256) listing)
func (_Nftkey0 *Nftkey0Filterer) FilterTokenDelisted(opts *bind.FilterOpts, erc721Address []common.Address, tokenId []*big.Int) (*Nftkey0TokenDelistedIterator, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Nftkey0.contract.FilterLogs(opts, "TokenDelisted", erc721AddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Nftkey0TokenDelistedIterator{contract: _Nftkey0.contract, event: "TokenDelisted", logs: logs, sub: sub}, nil
}

// WatchTokenDelisted is a free log subscription operation binding the contract event 0x835a0a426c94e53ab00dd6cf617ba68d1fa6c9162ef7a036b80be930c9a32c53.
//
// Solidity: event TokenDelisted(address indexed erc721Address, uint256 indexed tokenId, (uint256,uint256,address,uint256) listing)
func (_Nftkey0 *Nftkey0Filterer) WatchTokenDelisted(opts *bind.WatchOpts, sink chan<- *Nftkey0TokenDelisted, erc721Address []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Nftkey0.contract.WatchLogs(opts, "TokenDelisted", erc721AddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Nftkey0TokenDelisted)
				if err := _Nftkey0.contract.UnpackLog(event, "TokenDelisted", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokenDelisted is a log parse operation binding the contract event 0x835a0a426c94e53ab00dd6cf617ba68d1fa6c9162ef7a036b80be930c9a32c53.
//
// Solidity: event TokenDelisted(address indexed erc721Address, uint256 indexed tokenId, (uint256,uint256,address,uint256) listing)
func (_Nftkey0 *Nftkey0Filterer) ParseTokenDelisted(log types.Log) (*Nftkey0TokenDelisted, error) {
	event := new(Nftkey0TokenDelisted)
	if err := _Nftkey0.contract.UnpackLog(event, "TokenDelisted", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Nftkey0TokenListedIterator is returned from FilterTokenListed and is used to iterate over the raw logs and unpacked data for TokenListed events raised by the Nftkey0 contract.
type Nftkey0TokenListedIterator struct {
	Event *Nftkey0TokenListed // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Nftkey0TokenListedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Nftkey0TokenListed)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Nftkey0TokenListed)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Nftkey0TokenListedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Nftkey0TokenListedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Nftkey0TokenListed represents a TokenListed event raised by the Nftkey0 contract.
type Nftkey0TokenListed struct {
	Erc721Address common.Address
	TokenId       *big.Int
	Listing       INFTKEYMarketplaceV2Listing
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterTokenListed is a free log retrieval operation binding the contract event 0xfc8bd63d1c4480fdf7501f95b3dd0ba53542a02abbab92c0f579468341741abd.
//
// Solidity: event TokenListed(address indexed erc721Address, uint256 indexed tokenId, (uint256,uint256,address,uint256) listing)
func (_Nftkey0 *Nftkey0Filterer) FilterTokenListed(opts *bind.FilterOpts, erc721Address []common.Address, tokenId []*big.Int) (*Nftkey0TokenListedIterator, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Nftkey0.contract.FilterLogs(opts, "TokenListed", erc721AddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Nftkey0TokenListedIterator{contract: _Nftkey0.contract, event: "TokenListed", logs: logs, sub: sub}, nil
}

// WatchTokenListed is a free log subscription operation binding the contract event 0xfc8bd63d1c4480fdf7501f95b3dd0ba53542a02abbab92c0f579468341741abd.
//
// Solidity: event TokenListed(address indexed erc721Address, uint256 indexed tokenId, (uint256,uint256,address,uint256) listing)
func (_Nftkey0 *Nftkey0Filterer) WatchTokenListed(opts *bind.WatchOpts, sink chan<- *Nftkey0TokenListed, erc721Address []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var erc721AddressRule []interface{}
	for _, erc721AddressItem := range erc721Address {
		erc721AddressRule = append(erc721AddressRule, erc721AddressItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Nftkey0.contract.WatchLogs(opts, "TokenListed", erc721AddressRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Nftkey0TokenListed)
				if err := _Nftkey0.contract.UnpackLog(event, "TokenListed", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokenListed is a log parse operation binding the contract event 0xfc8bd63d1c4480fdf7501f95b3dd0ba53542a02abbab92c0f579468341741abd.
//
// Solidity: event TokenListed(address indexed erc721Address, uint256 indexed tokenId, (uint256,uint256,address,uint256) listing)
func (_Nftkey0 *Nftkey0Filterer) ParseTokenListed(log types.Log) (*Nftkey0TokenListed, error) {
	event := new(Nftkey0TokenListed)
	if err := _Nftkey0.contract.UnpackLog(event, "TokenListed", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
