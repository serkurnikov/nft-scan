// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package okxnft_2

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Okxnft2MetaData contains all meta data concerning the Okxnft2 contract.
var Okxnft2MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_implementation\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"ProxyOwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"implementation\",\"type\":\"address\"}],\"name\":\"Upgraded\",\"type\":\"event\"},{\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"fallback\"},{\"constant\":true,\"inputs\":[],\"name\":\"implementation\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"impl\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"proxyOwner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"address\",\"name\":\"_newOwner\",\"type\":\"address\"}],\"name\":\"transferProxyOwnership\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"address\",\"name\":\"_implementation\",\"type\":\"address\"}],\"name\":\"upgradeTo\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// Okxnft2ABI is the input ABI used to generate the binding from.
// Deprecated: Use Okxnft2MetaData.ABI instead.
var Okxnft2ABI = Okxnft2MetaData.ABI

// Okxnft2 is an auto generated Go binding around an Ethereum contract.
type Okxnft2 struct {
	Okxnft2Caller     // Read-only binding to the contract
	Okxnft2Transactor // Write-only binding to the contract
	Okxnft2Filterer   // Log filterer for contract events
}

// Okxnft2Caller is an auto generated read-only Go binding around an Ethereum contract.
type Okxnft2Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Okxnft2Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Okxnft2Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Okxnft2Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Okxnft2Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Okxnft2Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Okxnft2Session struct {
	Contract     *Okxnft2          // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Okxnft2CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Okxnft2CallerSession struct {
	Contract *Okxnft2Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts  // Call options to use throughout this session
}

// Okxnft2TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Okxnft2TransactorSession struct {
	Contract     *Okxnft2Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts  // Transaction auth options to use throughout this session
}

// Okxnft2Raw is an auto generated low-level Go binding around an Ethereum contract.
type Okxnft2Raw struct {
	Contract *Okxnft2 // Generic contract binding to access the raw methods on
}

// Okxnft2CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Okxnft2CallerRaw struct {
	Contract *Okxnft2Caller // Generic read-only contract binding to access the raw methods on
}

// Okxnft2TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Okxnft2TransactorRaw struct {
	Contract *Okxnft2Transactor // Generic write-only contract binding to access the raw methods on
}

// NewOkxnft2 creates a new instance of Okxnft2, bound to a specific deployed contract.
func NewOkxnft2(address common.Address, backend bind.ContractBackend) (*Okxnft2, error) {
	contract, err := bindOkxnft2(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Okxnft2{Okxnft2Caller: Okxnft2Caller{contract: contract}, Okxnft2Transactor: Okxnft2Transactor{contract: contract}, Okxnft2Filterer: Okxnft2Filterer{contract: contract}}, nil
}

// NewOkxnft2Caller creates a new read-only instance of Okxnft2, bound to a specific deployed contract.
func NewOkxnft2Caller(address common.Address, caller bind.ContractCaller) (*Okxnft2Caller, error) {
	contract, err := bindOkxnft2(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Okxnft2Caller{contract: contract}, nil
}

// NewOkxnft2Transactor creates a new write-only instance of Okxnft2, bound to a specific deployed contract.
func NewOkxnft2Transactor(address common.Address, transactor bind.ContractTransactor) (*Okxnft2Transactor, error) {
	contract, err := bindOkxnft2(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Okxnft2Transactor{contract: contract}, nil
}

// NewOkxnft2Filterer creates a new log filterer instance of Okxnft2, bound to a specific deployed contract.
func NewOkxnft2Filterer(address common.Address, filterer bind.ContractFilterer) (*Okxnft2Filterer, error) {
	contract, err := bindOkxnft2(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Okxnft2Filterer{contract: contract}, nil
}

// bindOkxnft2 binds a generic wrapper to an already deployed contract.
func bindOkxnft2(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Okxnft2ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Okxnft2 *Okxnft2Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Okxnft2.Contract.Okxnft2Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Okxnft2 *Okxnft2Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Okxnft2.Contract.Okxnft2Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Okxnft2 *Okxnft2Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Okxnft2.Contract.Okxnft2Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Okxnft2 *Okxnft2CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Okxnft2.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Okxnft2 *Okxnft2TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Okxnft2.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Okxnft2 *Okxnft2TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Okxnft2.Contract.contract.Transact(opts, method, params...)
}

// Implementation is a free data retrieval call binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() view returns(address impl)
func (_Okxnft2 *Okxnft2Caller) Implementation(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Okxnft2.contract.Call(opts, &out, "implementation")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Implementation is a free data retrieval call binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() view returns(address impl)
func (_Okxnft2 *Okxnft2Session) Implementation() (common.Address, error) {
	return _Okxnft2.Contract.Implementation(&_Okxnft2.CallOpts)
}

// Implementation is a free data retrieval call binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() view returns(address impl)
func (_Okxnft2 *Okxnft2CallerSession) Implementation() (common.Address, error) {
	return _Okxnft2.Contract.Implementation(&_Okxnft2.CallOpts)
}

// ProxyOwner is a free data retrieval call binding the contract method 0x025313a2.
//
// Solidity: function proxyOwner() view returns(address owner)
func (_Okxnft2 *Okxnft2Caller) ProxyOwner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Okxnft2.contract.Call(opts, &out, "proxyOwner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// ProxyOwner is a free data retrieval call binding the contract method 0x025313a2.
//
// Solidity: function proxyOwner() view returns(address owner)
func (_Okxnft2 *Okxnft2Session) ProxyOwner() (common.Address, error) {
	return _Okxnft2.Contract.ProxyOwner(&_Okxnft2.CallOpts)
}

// ProxyOwner is a free data retrieval call binding the contract method 0x025313a2.
//
// Solidity: function proxyOwner() view returns(address owner)
func (_Okxnft2 *Okxnft2CallerSession) ProxyOwner() (common.Address, error) {
	return _Okxnft2.Contract.ProxyOwner(&_Okxnft2.CallOpts)
}

// TransferProxyOwnership is a paid mutator transaction binding the contract method 0xf1739cae.
//
// Solidity: function transferProxyOwnership(address _newOwner) returns()
func (_Okxnft2 *Okxnft2Transactor) TransferProxyOwnership(opts *bind.TransactOpts, _newOwner common.Address) (*types.Transaction, error) {
	return _Okxnft2.contract.Transact(opts, "transferProxyOwnership", _newOwner)
}

// TransferProxyOwnership is a paid mutator transaction binding the contract method 0xf1739cae.
//
// Solidity: function transferProxyOwnership(address _newOwner) returns()
func (_Okxnft2 *Okxnft2Session) TransferProxyOwnership(_newOwner common.Address) (*types.Transaction, error) {
	return _Okxnft2.Contract.TransferProxyOwnership(&_Okxnft2.TransactOpts, _newOwner)
}

// TransferProxyOwnership is a paid mutator transaction binding the contract method 0xf1739cae.
//
// Solidity: function transferProxyOwnership(address _newOwner) returns()
func (_Okxnft2 *Okxnft2TransactorSession) TransferProxyOwnership(_newOwner common.Address) (*types.Transaction, error) {
	return _Okxnft2.Contract.TransferProxyOwnership(&_Okxnft2.TransactOpts, _newOwner)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address _implementation) returns()
func (_Okxnft2 *Okxnft2Transactor) UpgradeTo(opts *bind.TransactOpts, _implementation common.Address) (*types.Transaction, error) {
	return _Okxnft2.contract.Transact(opts, "upgradeTo", _implementation)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address _implementation) returns()
func (_Okxnft2 *Okxnft2Session) UpgradeTo(_implementation common.Address) (*types.Transaction, error) {
	return _Okxnft2.Contract.UpgradeTo(&_Okxnft2.TransactOpts, _implementation)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address _implementation) returns()
func (_Okxnft2 *Okxnft2TransactorSession) UpgradeTo(_implementation common.Address) (*types.Transaction, error) {
	return _Okxnft2.Contract.UpgradeTo(&_Okxnft2.TransactOpts, _implementation)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Okxnft2 *Okxnft2Transactor) Fallback(opts *bind.TransactOpts, calldata []byte) (*types.Transaction, error) {
	return _Okxnft2.contract.RawTransact(opts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Okxnft2 *Okxnft2Session) Fallback(calldata []byte) (*types.Transaction, error) {
	return _Okxnft2.Contract.Fallback(&_Okxnft2.TransactOpts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Okxnft2 *Okxnft2TransactorSession) Fallback(calldata []byte) (*types.Transaction, error) {
	return _Okxnft2.Contract.Fallback(&_Okxnft2.TransactOpts, calldata)
}

// Okxnft2ProxyOwnershipTransferredIterator is returned from FilterProxyOwnershipTransferred and is used to iterate over the raw logs and unpacked data for ProxyOwnershipTransferred events raised by the Okxnft2 contract.
type Okxnft2ProxyOwnershipTransferredIterator struct {
	Event *Okxnft2ProxyOwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Okxnft2ProxyOwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Okxnft2ProxyOwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Okxnft2ProxyOwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Okxnft2ProxyOwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Okxnft2ProxyOwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Okxnft2ProxyOwnershipTransferred represents a ProxyOwnershipTransferred event raised by the Okxnft2 contract.
type Okxnft2ProxyOwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterProxyOwnershipTransferred is a free log retrieval operation binding the contract event 0x5a3e66efaa1e445ebd894728a69d6959842ea1e97bd79b892797106e270efcd9.
//
// Solidity: event ProxyOwnershipTransferred(address previousOwner, address newOwner)
func (_Okxnft2 *Okxnft2Filterer) FilterProxyOwnershipTransferred(opts *bind.FilterOpts) (*Okxnft2ProxyOwnershipTransferredIterator, error) {

	logs, sub, err := _Okxnft2.contract.FilterLogs(opts, "ProxyOwnershipTransferred")
	if err != nil {
		return nil, err
	}
	return &Okxnft2ProxyOwnershipTransferredIterator{contract: _Okxnft2.contract, event: "ProxyOwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchProxyOwnershipTransferred is a free log subscription operation binding the contract event 0x5a3e66efaa1e445ebd894728a69d6959842ea1e97bd79b892797106e270efcd9.
//
// Solidity: event ProxyOwnershipTransferred(address previousOwner, address newOwner)
func (_Okxnft2 *Okxnft2Filterer) WatchProxyOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Okxnft2ProxyOwnershipTransferred) (event.Subscription, error) {

	logs, sub, err := _Okxnft2.contract.WatchLogs(opts, "ProxyOwnershipTransferred")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Okxnft2ProxyOwnershipTransferred)
				if err := _Okxnft2.contract.UnpackLog(event, "ProxyOwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseProxyOwnershipTransferred is a log parse operation binding the contract event 0x5a3e66efaa1e445ebd894728a69d6959842ea1e97bd79b892797106e270efcd9.
//
// Solidity: event ProxyOwnershipTransferred(address previousOwner, address newOwner)
func (_Okxnft2 *Okxnft2Filterer) ParseProxyOwnershipTransferred(log types.Log) (*Okxnft2ProxyOwnershipTransferred, error) {
	event := new(Okxnft2ProxyOwnershipTransferred)
	if err := _Okxnft2.contract.UnpackLog(event, "ProxyOwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Okxnft2UpgradedIterator is returned from FilterUpgraded and is used to iterate over the raw logs and unpacked data for Upgraded events raised by the Okxnft2 contract.
type Okxnft2UpgradedIterator struct {
	Event *Okxnft2Upgraded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Okxnft2UpgradedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Okxnft2Upgraded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Okxnft2Upgraded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Okxnft2UpgradedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Okxnft2UpgradedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Okxnft2Upgraded represents a Upgraded event raised by the Okxnft2 contract.
type Okxnft2Upgraded struct {
	Implementation common.Address
	Raw            types.Log // Blockchain specific contextual infos
}

// FilterUpgraded is a free log retrieval operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Okxnft2 *Okxnft2Filterer) FilterUpgraded(opts *bind.FilterOpts, implementation []common.Address) (*Okxnft2UpgradedIterator, error) {

	var implementationRule []interface{}
	for _, implementationItem := range implementation {
		implementationRule = append(implementationRule, implementationItem)
	}

	logs, sub, err := _Okxnft2.contract.FilterLogs(opts, "Upgraded", implementationRule)
	if err != nil {
		return nil, err
	}
	return &Okxnft2UpgradedIterator{contract: _Okxnft2.contract, event: "Upgraded", logs: logs, sub: sub}, nil
}

// WatchUpgraded is a free log subscription operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Okxnft2 *Okxnft2Filterer) WatchUpgraded(opts *bind.WatchOpts, sink chan<- *Okxnft2Upgraded, implementation []common.Address) (event.Subscription, error) {

	var implementationRule []interface{}
	for _, implementationItem := range implementation {
		implementationRule = append(implementationRule, implementationItem)
	}

	logs, sub, err := _Okxnft2.contract.WatchLogs(opts, "Upgraded", implementationRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Okxnft2Upgraded)
				if err := _Okxnft2.contract.UnpackLog(event, "Upgraded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUpgraded is a log parse operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Okxnft2 *Okxnft2Filterer) ParseUpgraded(log types.Log) (*Okxnft2Upgraded, error) {
	event := new(Okxnft2Upgraded)
	if err := _Okxnft2.contract.UnpackLog(event, "Upgraded", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
