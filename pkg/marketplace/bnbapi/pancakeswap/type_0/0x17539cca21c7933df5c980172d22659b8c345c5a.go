// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package pancakeswap_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// ERC721NFTMarketV1Ask is an auto generated low-level Go binding around an user-defined struct.
type ERC721NFTMarketV1Ask struct {
	Seller common.Address
	Price  *big.Int
}

// ERC721NFTMarketV1Collection is an auto generated low-level Go binding around an user-defined struct.
type ERC721NFTMarketV1Collection struct {
	Status           uint8
	CreatorAddress   common.Address
	WhitelistChecker common.Address
	TradingFee       *big.Int
	CreatorFee       *big.Int
}

// Pancakeswap0MetaData contains all meta data concerning the Pancakeswap0 contract.
var Pancakeswap0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_adminAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_treasuryAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_WBNBAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_minimumAskPrice\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_maximumAskPrice\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"AskCancel\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"askPrice\",\"type\":\"uint256\"}],\"name\":\"AskNew\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"askPrice\",\"type\":\"uint256\"}],\"name\":\"AskUpdate\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"}],\"name\":\"CollectionClose\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"creator\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"whitelistChecker\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"tradingFee\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"creatorFee\",\"type\":\"uint256\"}],\"name\":\"CollectionNew\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"creator\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"whitelistChecker\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"tradingFee\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"creatorFee\",\"type\":\"uint256\"}],\"name\":\"CollectionUpdate\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"admin\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"treasury\",\"type\":\"address\"}],\"name\":\"NewAdminAndTreasuryAddresses\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"minimumAskPrice\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"maximumAskPrice\",\"type\":\"uint256\"}],\"name\":\"NewMinimumAndMaximumAskPrices\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"NonFungibleTokenRecovery\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"claimer\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"RevenueClaim\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"TokenRecovery\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"askPrice\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"netPrice\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"withBNB\",\"type\":\"bool\"}],\"name\":\"Trade\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"TOTAL_MAX_FEE\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"WBNB\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_collection\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_creator\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_whitelistChecker\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tradingFee\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_creatorFee\",\"type\":\"uint256\"}],\"name\":\"addCollection\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"adminAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_collection\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"buyTokenUsingBNB\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_collection\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_price\",\"type\":\"uint256\"}],\"name\":\"buyTokenUsingWBNB\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"calculatePriceAndFeesForCollection\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"netPrice\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tradingFee\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"creatorFee\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_collection\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"_tokenIds\",\"type\":\"uint256[]\"}],\"name\":\"canTokensBeListed\",\"outputs\":[{\"internalType\":\"bool[]\",\"name\":\"listingStatuses\",\"type\":\"bool[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_collection\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"cancelAskOrder\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"claimPendingRevenue\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_collection\",\"type\":\"address\"}],\"name\":\"closeCollectionForTradingAndListing\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_collection\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_askPrice\",\"type\":\"uint256\"}],\"name\":\"createAskOrder\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"maximumAskPrice\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"minimumAskPrice\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_collection\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_newPrice\",\"type\":\"uint256\"}],\"name\":\"modifyAskOrder\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_collection\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_creator\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_whitelistChecker\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tradingFee\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_creatorFee\",\"type\":\"uint256\"}],\"name\":\"modifyCollection\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC721Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"pendingRevenue\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_token\",\"type\":\"address\"}],\"name\":\"recoverFungibleTokens\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"recoverNonFungibleToken\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_adminAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_treasuryAddress\",\"type\":\"address\"}],\"name\":\"setAdminAndTreasuryAddresses\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"treasuryAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_minimumAskPrice\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_maximumAskPrice\",\"type\":\"uint256\"}],\"name\":\"updateMinimumAndMaximumPrices\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"cursor\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"size\",\"type\":\"uint256\"}],\"name\":\"viewAsksByCollection\",\"outputs\":[{\"internalType\":\"uint256[]\",\"name\":\"tokenIds\",\"type\":\"uint256[]\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"internalType\":\"structERC721NFTMarketV1.Ask[]\",\"name\":\"askInfo\",\"type\":\"tuple[]\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"cursor\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"size\",\"type\":\"uint256\"}],\"name\":\"viewAsksByCollectionAndSeller\",\"outputs\":[{\"internalType\":\"uint256[]\",\"name\":\"tokenIds\",\"type\":\"uint256[]\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"internalType\":\"structERC721NFTMarketV1.Ask[]\",\"name\":\"askInfo\",\"type\":\"tuple[]\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"tokenIds\",\"type\":\"uint256[]\"}],\"name\":\"viewAsksByCollectionAndTokenIds\",\"outputs\":[{\"internalType\":\"bool[]\",\"name\":\"statuses\",\"type\":\"bool[]\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"internalType\":\"structERC721NFTMarketV1.Ask[]\",\"name\":\"askInfo\",\"type\":\"tuple[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"cursor\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"size\",\"type\":\"uint256\"}],\"name\":\"viewCollections\",\"outputs\":[{\"internalType\":\"address[]\",\"name\":\"collectionAddresses\",\"type\":\"address[]\"},{\"components\":[{\"internalType\":\"enumERC721NFTMarketV1.CollectionStatus\",\"name\":\"status\",\"type\":\"uint8\"},{\"internalType\":\"address\",\"name\":\"creatorAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"whitelistChecker\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tradingFee\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"creatorFee\",\"type\":\"uint256\"}],\"internalType\":\"structERC721NFTMarketV1.Collection[]\",\"name\":\"collectionDetails\",\"type\":\"tuple[]\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
}

// Pancakeswap0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Pancakeswap0MetaData.ABI instead.
var Pancakeswap0ABI = Pancakeswap0MetaData.ABI

// Pancakeswap0 is an auto generated Go binding around an Ethereum contract.
type Pancakeswap0 struct {
	Pancakeswap0Caller     // Read-only binding to the contract
	Pancakeswap0Transactor // Write-only binding to the contract
	Pancakeswap0Filterer   // Log filterer for contract events
}

// Pancakeswap0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Pancakeswap0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Pancakeswap0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Pancakeswap0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Pancakeswap0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Pancakeswap0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Pancakeswap0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Pancakeswap0Session struct {
	Contract     *Pancakeswap0     // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Pancakeswap0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Pancakeswap0CallerSession struct {
	Contract *Pancakeswap0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts       // Call options to use throughout this session
}

// Pancakeswap0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Pancakeswap0TransactorSession struct {
	Contract     *Pancakeswap0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts       // Transaction auth options to use throughout this session
}

// Pancakeswap0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Pancakeswap0Raw struct {
	Contract *Pancakeswap0 // Generic contract binding to access the raw methods on
}

// Pancakeswap0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Pancakeswap0CallerRaw struct {
	Contract *Pancakeswap0Caller // Generic read-only contract binding to access the raw methods on
}

// Pancakeswap0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Pancakeswap0TransactorRaw struct {
	Contract *Pancakeswap0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewPancakeswap0 creates a new instance of Pancakeswap0, bound to a specific deployed contract.
func NewPancakeswap0(address common.Address, backend bind.ContractBackend) (*Pancakeswap0, error) {
	contract, err := bindPancakeswap0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0{Pancakeswap0Caller: Pancakeswap0Caller{contract: contract}, Pancakeswap0Transactor: Pancakeswap0Transactor{contract: contract}, Pancakeswap0Filterer: Pancakeswap0Filterer{contract: contract}}, nil
}

// NewPancakeswap0Caller creates a new read-only instance of Pancakeswap0, bound to a specific deployed contract.
func NewPancakeswap0Caller(address common.Address, caller bind.ContractCaller) (*Pancakeswap0Caller, error) {
	contract, err := bindPancakeswap0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0Caller{contract: contract}, nil
}

// NewPancakeswap0Transactor creates a new write-only instance of Pancakeswap0, bound to a specific deployed contract.
func NewPancakeswap0Transactor(address common.Address, transactor bind.ContractTransactor) (*Pancakeswap0Transactor, error) {
	contract, err := bindPancakeswap0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0Transactor{contract: contract}, nil
}

// NewPancakeswap0Filterer creates a new log filterer instance of Pancakeswap0, bound to a specific deployed contract.
func NewPancakeswap0Filterer(address common.Address, filterer bind.ContractFilterer) (*Pancakeswap0Filterer, error) {
	contract, err := bindPancakeswap0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0Filterer{contract: contract}, nil
}

// bindPancakeswap0 binds a generic wrapper to an already deployed contract.
func bindPancakeswap0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Pancakeswap0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Pancakeswap0 *Pancakeswap0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Pancakeswap0.Contract.Pancakeswap0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Pancakeswap0 *Pancakeswap0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.Pancakeswap0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Pancakeswap0 *Pancakeswap0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.Pancakeswap0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Pancakeswap0 *Pancakeswap0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Pancakeswap0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Pancakeswap0 *Pancakeswap0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Pancakeswap0 *Pancakeswap0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.contract.Transact(opts, method, params...)
}

// TOTALMAXFEE is a free data retrieval call binding the contract method 0x209cbe7e.
//
// Solidity: function TOTAL_MAX_FEE() view returns(uint256)
func (_Pancakeswap0 *Pancakeswap0Caller) TOTALMAXFEE(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "TOTAL_MAX_FEE")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TOTALMAXFEE is a free data retrieval call binding the contract method 0x209cbe7e.
//
// Solidity: function TOTAL_MAX_FEE() view returns(uint256)
func (_Pancakeswap0 *Pancakeswap0Session) TOTALMAXFEE() (*big.Int, error) {
	return _Pancakeswap0.Contract.TOTALMAXFEE(&_Pancakeswap0.CallOpts)
}

// TOTALMAXFEE is a free data retrieval call binding the contract method 0x209cbe7e.
//
// Solidity: function TOTAL_MAX_FEE() view returns(uint256)
func (_Pancakeswap0 *Pancakeswap0CallerSession) TOTALMAXFEE() (*big.Int, error) {
	return _Pancakeswap0.Contract.TOTALMAXFEE(&_Pancakeswap0.CallOpts)
}

// WBNB is a free data retrieval call binding the contract method 0x8dd95002.
//
// Solidity: function WBNB() view returns(address)
func (_Pancakeswap0 *Pancakeswap0Caller) WBNB(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "WBNB")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// WBNB is a free data retrieval call binding the contract method 0x8dd95002.
//
// Solidity: function WBNB() view returns(address)
func (_Pancakeswap0 *Pancakeswap0Session) WBNB() (common.Address, error) {
	return _Pancakeswap0.Contract.WBNB(&_Pancakeswap0.CallOpts)
}

// WBNB is a free data retrieval call binding the contract method 0x8dd95002.
//
// Solidity: function WBNB() view returns(address)
func (_Pancakeswap0 *Pancakeswap0CallerSession) WBNB() (common.Address, error) {
	return _Pancakeswap0.Contract.WBNB(&_Pancakeswap0.CallOpts)
}

// AdminAddress is a free data retrieval call binding the contract method 0xfc6f9468.
//
// Solidity: function adminAddress() view returns(address)
func (_Pancakeswap0 *Pancakeswap0Caller) AdminAddress(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "adminAddress")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// AdminAddress is a free data retrieval call binding the contract method 0xfc6f9468.
//
// Solidity: function adminAddress() view returns(address)
func (_Pancakeswap0 *Pancakeswap0Session) AdminAddress() (common.Address, error) {
	return _Pancakeswap0.Contract.AdminAddress(&_Pancakeswap0.CallOpts)
}

// AdminAddress is a free data retrieval call binding the contract method 0xfc6f9468.
//
// Solidity: function adminAddress() view returns(address)
func (_Pancakeswap0 *Pancakeswap0CallerSession) AdminAddress() (common.Address, error) {
	return _Pancakeswap0.Contract.AdminAddress(&_Pancakeswap0.CallOpts)
}

// CalculatePriceAndFeesForCollection is a free data retrieval call binding the contract method 0x9e0140c8.
//
// Solidity: function calculatePriceAndFeesForCollection(address collection, uint256 price) view returns(uint256 netPrice, uint256 tradingFee, uint256 creatorFee)
func (_Pancakeswap0 *Pancakeswap0Caller) CalculatePriceAndFeesForCollection(opts *bind.CallOpts, collection common.Address, price *big.Int) (struct {
	NetPrice   *big.Int
	TradingFee *big.Int
	CreatorFee *big.Int
}, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "calculatePriceAndFeesForCollection", collection, price)

	outstruct := new(struct {
		NetPrice   *big.Int
		TradingFee *big.Int
		CreatorFee *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.NetPrice = *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	outstruct.TradingFee = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.CreatorFee = *abi.ConvertType(out[2], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// CalculatePriceAndFeesForCollection is a free data retrieval call binding the contract method 0x9e0140c8.
//
// Solidity: function calculatePriceAndFeesForCollection(address collection, uint256 price) view returns(uint256 netPrice, uint256 tradingFee, uint256 creatorFee)
func (_Pancakeswap0 *Pancakeswap0Session) CalculatePriceAndFeesForCollection(collection common.Address, price *big.Int) (struct {
	NetPrice   *big.Int
	TradingFee *big.Int
	CreatorFee *big.Int
}, error) {
	return _Pancakeswap0.Contract.CalculatePriceAndFeesForCollection(&_Pancakeswap0.CallOpts, collection, price)
}

// CalculatePriceAndFeesForCollection is a free data retrieval call binding the contract method 0x9e0140c8.
//
// Solidity: function calculatePriceAndFeesForCollection(address collection, uint256 price) view returns(uint256 netPrice, uint256 tradingFee, uint256 creatorFee)
func (_Pancakeswap0 *Pancakeswap0CallerSession) CalculatePriceAndFeesForCollection(collection common.Address, price *big.Int) (struct {
	NetPrice   *big.Int
	TradingFee *big.Int
	CreatorFee *big.Int
}, error) {
	return _Pancakeswap0.Contract.CalculatePriceAndFeesForCollection(&_Pancakeswap0.CallOpts, collection, price)
}

// CanTokensBeListed is a free data retrieval call binding the contract method 0xc5d4618e.
//
// Solidity: function canTokensBeListed(address _collection, uint256[] _tokenIds) view returns(bool[] listingStatuses)
func (_Pancakeswap0 *Pancakeswap0Caller) CanTokensBeListed(opts *bind.CallOpts, _collection common.Address, _tokenIds []*big.Int) ([]bool, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "canTokensBeListed", _collection, _tokenIds)

	if err != nil {
		return *new([]bool), err
	}

	out0 := *abi.ConvertType(out[0], new([]bool)).(*[]bool)

	return out0, err

}

// CanTokensBeListed is a free data retrieval call binding the contract method 0xc5d4618e.
//
// Solidity: function canTokensBeListed(address _collection, uint256[] _tokenIds) view returns(bool[] listingStatuses)
func (_Pancakeswap0 *Pancakeswap0Session) CanTokensBeListed(_collection common.Address, _tokenIds []*big.Int) ([]bool, error) {
	return _Pancakeswap0.Contract.CanTokensBeListed(&_Pancakeswap0.CallOpts, _collection, _tokenIds)
}

// CanTokensBeListed is a free data retrieval call binding the contract method 0xc5d4618e.
//
// Solidity: function canTokensBeListed(address _collection, uint256[] _tokenIds) view returns(bool[] listingStatuses)
func (_Pancakeswap0 *Pancakeswap0CallerSession) CanTokensBeListed(_collection common.Address, _tokenIds []*big.Int) ([]bool, error) {
	return _Pancakeswap0.Contract.CanTokensBeListed(&_Pancakeswap0.CallOpts, _collection, _tokenIds)
}

// MaximumAskPrice is a free data retrieval call binding the contract method 0x98a5155a.
//
// Solidity: function maximumAskPrice() view returns(uint256)
func (_Pancakeswap0 *Pancakeswap0Caller) MaximumAskPrice(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "maximumAskPrice")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MaximumAskPrice is a free data retrieval call binding the contract method 0x98a5155a.
//
// Solidity: function maximumAskPrice() view returns(uint256)
func (_Pancakeswap0 *Pancakeswap0Session) MaximumAskPrice() (*big.Int, error) {
	return _Pancakeswap0.Contract.MaximumAskPrice(&_Pancakeswap0.CallOpts)
}

// MaximumAskPrice is a free data retrieval call binding the contract method 0x98a5155a.
//
// Solidity: function maximumAskPrice() view returns(uint256)
func (_Pancakeswap0 *Pancakeswap0CallerSession) MaximumAskPrice() (*big.Int, error) {
	return _Pancakeswap0.Contract.MaximumAskPrice(&_Pancakeswap0.CallOpts)
}

// MinimumAskPrice is a free data retrieval call binding the contract method 0x4f06d441.
//
// Solidity: function minimumAskPrice() view returns(uint256)
func (_Pancakeswap0 *Pancakeswap0Caller) MinimumAskPrice(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "minimumAskPrice")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MinimumAskPrice is a free data retrieval call binding the contract method 0x4f06d441.
//
// Solidity: function minimumAskPrice() view returns(uint256)
func (_Pancakeswap0 *Pancakeswap0Session) MinimumAskPrice() (*big.Int, error) {
	return _Pancakeswap0.Contract.MinimumAskPrice(&_Pancakeswap0.CallOpts)
}

// MinimumAskPrice is a free data retrieval call binding the contract method 0x4f06d441.
//
// Solidity: function minimumAskPrice() view returns(uint256)
func (_Pancakeswap0 *Pancakeswap0CallerSession) MinimumAskPrice() (*big.Int, error) {
	return _Pancakeswap0.Contract.MinimumAskPrice(&_Pancakeswap0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Pancakeswap0 *Pancakeswap0Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Pancakeswap0 *Pancakeswap0Session) Owner() (common.Address, error) {
	return _Pancakeswap0.Contract.Owner(&_Pancakeswap0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Pancakeswap0 *Pancakeswap0CallerSession) Owner() (common.Address, error) {
	return _Pancakeswap0.Contract.Owner(&_Pancakeswap0.CallOpts)
}

// PendingRevenue is a free data retrieval call binding the contract method 0x20b6b2fc.
//
// Solidity: function pendingRevenue(address ) view returns(uint256)
func (_Pancakeswap0 *Pancakeswap0Caller) PendingRevenue(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "pendingRevenue", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// PendingRevenue is a free data retrieval call binding the contract method 0x20b6b2fc.
//
// Solidity: function pendingRevenue(address ) view returns(uint256)
func (_Pancakeswap0 *Pancakeswap0Session) PendingRevenue(arg0 common.Address) (*big.Int, error) {
	return _Pancakeswap0.Contract.PendingRevenue(&_Pancakeswap0.CallOpts, arg0)
}

// PendingRevenue is a free data retrieval call binding the contract method 0x20b6b2fc.
//
// Solidity: function pendingRevenue(address ) view returns(uint256)
func (_Pancakeswap0 *Pancakeswap0CallerSession) PendingRevenue(arg0 common.Address) (*big.Int, error) {
	return _Pancakeswap0.Contract.PendingRevenue(&_Pancakeswap0.CallOpts, arg0)
}

// TreasuryAddress is a free data retrieval call binding the contract method 0xc5f956af.
//
// Solidity: function treasuryAddress() view returns(address)
func (_Pancakeswap0 *Pancakeswap0Caller) TreasuryAddress(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "treasuryAddress")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// TreasuryAddress is a free data retrieval call binding the contract method 0xc5f956af.
//
// Solidity: function treasuryAddress() view returns(address)
func (_Pancakeswap0 *Pancakeswap0Session) TreasuryAddress() (common.Address, error) {
	return _Pancakeswap0.Contract.TreasuryAddress(&_Pancakeswap0.CallOpts)
}

// TreasuryAddress is a free data retrieval call binding the contract method 0xc5f956af.
//
// Solidity: function treasuryAddress() view returns(address)
func (_Pancakeswap0 *Pancakeswap0CallerSession) TreasuryAddress() (common.Address, error) {
	return _Pancakeswap0.Contract.TreasuryAddress(&_Pancakeswap0.CallOpts)
}

// ViewAsksByCollection is a free data retrieval call binding the contract method 0x84a40864.
//
// Solidity: function viewAsksByCollection(address collection, uint256 cursor, uint256 size) view returns(uint256[] tokenIds, (address,uint256)[] askInfo, uint256)
func (_Pancakeswap0 *Pancakeswap0Caller) ViewAsksByCollection(opts *bind.CallOpts, collection common.Address, cursor *big.Int, size *big.Int) ([]*big.Int, []ERC721NFTMarketV1Ask, *big.Int, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "viewAsksByCollection", collection, cursor, size)

	if err != nil {
		return *new([]*big.Int), *new([]ERC721NFTMarketV1Ask), *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new([]*big.Int)).(*[]*big.Int)
	out1 := *abi.ConvertType(out[1], new([]ERC721NFTMarketV1Ask)).(*[]ERC721NFTMarketV1Ask)
	out2 := *abi.ConvertType(out[2], new(*big.Int)).(**big.Int)

	return out0, out1, out2, err

}

// ViewAsksByCollection is a free data retrieval call binding the contract method 0x84a40864.
//
// Solidity: function viewAsksByCollection(address collection, uint256 cursor, uint256 size) view returns(uint256[] tokenIds, (address,uint256)[] askInfo, uint256)
func (_Pancakeswap0 *Pancakeswap0Session) ViewAsksByCollection(collection common.Address, cursor *big.Int, size *big.Int) ([]*big.Int, []ERC721NFTMarketV1Ask, *big.Int, error) {
	return _Pancakeswap0.Contract.ViewAsksByCollection(&_Pancakeswap0.CallOpts, collection, cursor, size)
}

// ViewAsksByCollection is a free data retrieval call binding the contract method 0x84a40864.
//
// Solidity: function viewAsksByCollection(address collection, uint256 cursor, uint256 size) view returns(uint256[] tokenIds, (address,uint256)[] askInfo, uint256)
func (_Pancakeswap0 *Pancakeswap0CallerSession) ViewAsksByCollection(collection common.Address, cursor *big.Int, size *big.Int) ([]*big.Int, []ERC721NFTMarketV1Ask, *big.Int, error) {
	return _Pancakeswap0.Contract.ViewAsksByCollection(&_Pancakeswap0.CallOpts, collection, cursor, size)
}

// ViewAsksByCollectionAndSeller is a free data retrieval call binding the contract method 0xd286ef74.
//
// Solidity: function viewAsksByCollectionAndSeller(address collection, address seller, uint256 cursor, uint256 size) view returns(uint256[] tokenIds, (address,uint256)[] askInfo, uint256)
func (_Pancakeswap0 *Pancakeswap0Caller) ViewAsksByCollectionAndSeller(opts *bind.CallOpts, collection common.Address, seller common.Address, cursor *big.Int, size *big.Int) ([]*big.Int, []ERC721NFTMarketV1Ask, *big.Int, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "viewAsksByCollectionAndSeller", collection, seller, cursor, size)

	if err != nil {
		return *new([]*big.Int), *new([]ERC721NFTMarketV1Ask), *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new([]*big.Int)).(*[]*big.Int)
	out1 := *abi.ConvertType(out[1], new([]ERC721NFTMarketV1Ask)).(*[]ERC721NFTMarketV1Ask)
	out2 := *abi.ConvertType(out[2], new(*big.Int)).(**big.Int)

	return out0, out1, out2, err

}

// ViewAsksByCollectionAndSeller is a free data retrieval call binding the contract method 0xd286ef74.
//
// Solidity: function viewAsksByCollectionAndSeller(address collection, address seller, uint256 cursor, uint256 size) view returns(uint256[] tokenIds, (address,uint256)[] askInfo, uint256)
func (_Pancakeswap0 *Pancakeswap0Session) ViewAsksByCollectionAndSeller(collection common.Address, seller common.Address, cursor *big.Int, size *big.Int) ([]*big.Int, []ERC721NFTMarketV1Ask, *big.Int, error) {
	return _Pancakeswap0.Contract.ViewAsksByCollectionAndSeller(&_Pancakeswap0.CallOpts, collection, seller, cursor, size)
}

// ViewAsksByCollectionAndSeller is a free data retrieval call binding the contract method 0xd286ef74.
//
// Solidity: function viewAsksByCollectionAndSeller(address collection, address seller, uint256 cursor, uint256 size) view returns(uint256[] tokenIds, (address,uint256)[] askInfo, uint256)
func (_Pancakeswap0 *Pancakeswap0CallerSession) ViewAsksByCollectionAndSeller(collection common.Address, seller common.Address, cursor *big.Int, size *big.Int) ([]*big.Int, []ERC721NFTMarketV1Ask, *big.Int, error) {
	return _Pancakeswap0.Contract.ViewAsksByCollectionAndSeller(&_Pancakeswap0.CallOpts, collection, seller, cursor, size)
}

// ViewAsksByCollectionAndTokenIds is a free data retrieval call binding the contract method 0x22f67b6d.
//
// Solidity: function viewAsksByCollectionAndTokenIds(address collection, uint256[] tokenIds) view returns(bool[] statuses, (address,uint256)[] askInfo)
func (_Pancakeswap0 *Pancakeswap0Caller) ViewAsksByCollectionAndTokenIds(opts *bind.CallOpts, collection common.Address, tokenIds []*big.Int) (struct {
	Statuses []bool
	AskInfo  []ERC721NFTMarketV1Ask
}, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "viewAsksByCollectionAndTokenIds", collection, tokenIds)

	outstruct := new(struct {
		Statuses []bool
		AskInfo  []ERC721NFTMarketV1Ask
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Statuses = *abi.ConvertType(out[0], new([]bool)).(*[]bool)
	outstruct.AskInfo = *abi.ConvertType(out[1], new([]ERC721NFTMarketV1Ask)).(*[]ERC721NFTMarketV1Ask)

	return *outstruct, err

}

// ViewAsksByCollectionAndTokenIds is a free data retrieval call binding the contract method 0x22f67b6d.
//
// Solidity: function viewAsksByCollectionAndTokenIds(address collection, uint256[] tokenIds) view returns(bool[] statuses, (address,uint256)[] askInfo)
func (_Pancakeswap0 *Pancakeswap0Session) ViewAsksByCollectionAndTokenIds(collection common.Address, tokenIds []*big.Int) (struct {
	Statuses []bool
	AskInfo  []ERC721NFTMarketV1Ask
}, error) {
	return _Pancakeswap0.Contract.ViewAsksByCollectionAndTokenIds(&_Pancakeswap0.CallOpts, collection, tokenIds)
}

// ViewAsksByCollectionAndTokenIds is a free data retrieval call binding the contract method 0x22f67b6d.
//
// Solidity: function viewAsksByCollectionAndTokenIds(address collection, uint256[] tokenIds) view returns(bool[] statuses, (address,uint256)[] askInfo)
func (_Pancakeswap0 *Pancakeswap0CallerSession) ViewAsksByCollectionAndTokenIds(collection common.Address, tokenIds []*big.Int) (struct {
	Statuses []bool
	AskInfo  []ERC721NFTMarketV1Ask
}, error) {
	return _Pancakeswap0.Contract.ViewAsksByCollectionAndTokenIds(&_Pancakeswap0.CallOpts, collection, tokenIds)
}

// ViewCollections is a free data retrieval call binding the contract method 0x51c383ea.
//
// Solidity: function viewCollections(uint256 cursor, uint256 size) view returns(address[] collectionAddresses, (uint8,address,address,uint256,uint256)[] collectionDetails, uint256)
func (_Pancakeswap0 *Pancakeswap0Caller) ViewCollections(opts *bind.CallOpts, cursor *big.Int, size *big.Int) ([]common.Address, []ERC721NFTMarketV1Collection, *big.Int, error) {
	var out []interface{}
	err := _Pancakeswap0.contract.Call(opts, &out, "viewCollections", cursor, size)

	if err != nil {
		return *new([]common.Address), *new([]ERC721NFTMarketV1Collection), *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new([]common.Address)).(*[]common.Address)
	out1 := *abi.ConvertType(out[1], new([]ERC721NFTMarketV1Collection)).(*[]ERC721NFTMarketV1Collection)
	out2 := *abi.ConvertType(out[2], new(*big.Int)).(**big.Int)

	return out0, out1, out2, err

}

// ViewCollections is a free data retrieval call binding the contract method 0x51c383ea.
//
// Solidity: function viewCollections(uint256 cursor, uint256 size) view returns(address[] collectionAddresses, (uint8,address,address,uint256,uint256)[] collectionDetails, uint256)
func (_Pancakeswap0 *Pancakeswap0Session) ViewCollections(cursor *big.Int, size *big.Int) ([]common.Address, []ERC721NFTMarketV1Collection, *big.Int, error) {
	return _Pancakeswap0.Contract.ViewCollections(&_Pancakeswap0.CallOpts, cursor, size)
}

// ViewCollections is a free data retrieval call binding the contract method 0x51c383ea.
//
// Solidity: function viewCollections(uint256 cursor, uint256 size) view returns(address[] collectionAddresses, (uint8,address,address,uint256,uint256)[] collectionDetails, uint256)
func (_Pancakeswap0 *Pancakeswap0CallerSession) ViewCollections(cursor *big.Int, size *big.Int) ([]common.Address, []ERC721NFTMarketV1Collection, *big.Int, error) {
	return _Pancakeswap0.Contract.ViewCollections(&_Pancakeswap0.CallOpts, cursor, size)
}

// AddCollection is a paid mutator transaction binding the contract method 0xe17734a3.
//
// Solidity: function addCollection(address _collection, address _creator, address _whitelistChecker, uint256 _tradingFee, uint256 _creatorFee) returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) AddCollection(opts *bind.TransactOpts, _collection common.Address, _creator common.Address, _whitelistChecker common.Address, _tradingFee *big.Int, _creatorFee *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "addCollection", _collection, _creator, _whitelistChecker, _tradingFee, _creatorFee)
}

// AddCollection is a paid mutator transaction binding the contract method 0xe17734a3.
//
// Solidity: function addCollection(address _collection, address _creator, address _whitelistChecker, uint256 _tradingFee, uint256 _creatorFee) returns()
func (_Pancakeswap0 *Pancakeswap0Session) AddCollection(_collection common.Address, _creator common.Address, _whitelistChecker common.Address, _tradingFee *big.Int, _creatorFee *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.AddCollection(&_Pancakeswap0.TransactOpts, _collection, _creator, _whitelistChecker, _tradingFee, _creatorFee)
}

// AddCollection is a paid mutator transaction binding the contract method 0xe17734a3.
//
// Solidity: function addCollection(address _collection, address _creator, address _whitelistChecker, uint256 _tradingFee, uint256 _creatorFee) returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) AddCollection(_collection common.Address, _creator common.Address, _whitelistChecker common.Address, _tradingFee *big.Int, _creatorFee *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.AddCollection(&_Pancakeswap0.TransactOpts, _collection, _creator, _whitelistChecker, _tradingFee, _creatorFee)
}

// BuyTokenUsingBNB is a paid mutator transaction binding the contract method 0xfb9fe938.
//
// Solidity: function buyTokenUsingBNB(address _collection, uint256 _tokenId) payable returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) BuyTokenUsingBNB(opts *bind.TransactOpts, _collection common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "buyTokenUsingBNB", _collection, _tokenId)
}

// BuyTokenUsingBNB is a paid mutator transaction binding the contract method 0xfb9fe938.
//
// Solidity: function buyTokenUsingBNB(address _collection, uint256 _tokenId) payable returns()
func (_Pancakeswap0 *Pancakeswap0Session) BuyTokenUsingBNB(_collection common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.BuyTokenUsingBNB(&_Pancakeswap0.TransactOpts, _collection, _tokenId)
}

// BuyTokenUsingBNB is a paid mutator transaction binding the contract method 0xfb9fe938.
//
// Solidity: function buyTokenUsingBNB(address _collection, uint256 _tokenId) payable returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) BuyTokenUsingBNB(_collection common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.BuyTokenUsingBNB(&_Pancakeswap0.TransactOpts, _collection, _tokenId)
}

// BuyTokenUsingWBNB is a paid mutator transaction binding the contract method 0xed140e86.
//
// Solidity: function buyTokenUsingWBNB(address _collection, uint256 _tokenId, uint256 _price) returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) BuyTokenUsingWBNB(opts *bind.TransactOpts, _collection common.Address, _tokenId *big.Int, _price *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "buyTokenUsingWBNB", _collection, _tokenId, _price)
}

// BuyTokenUsingWBNB is a paid mutator transaction binding the contract method 0xed140e86.
//
// Solidity: function buyTokenUsingWBNB(address _collection, uint256 _tokenId, uint256 _price) returns()
func (_Pancakeswap0 *Pancakeswap0Session) BuyTokenUsingWBNB(_collection common.Address, _tokenId *big.Int, _price *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.BuyTokenUsingWBNB(&_Pancakeswap0.TransactOpts, _collection, _tokenId, _price)
}

// BuyTokenUsingWBNB is a paid mutator transaction binding the contract method 0xed140e86.
//
// Solidity: function buyTokenUsingWBNB(address _collection, uint256 _tokenId, uint256 _price) returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) BuyTokenUsingWBNB(_collection common.Address, _tokenId *big.Int, _price *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.BuyTokenUsingWBNB(&_Pancakeswap0.TransactOpts, _collection, _tokenId, _price)
}

// CancelAskOrder is a paid mutator transaction binding the contract method 0x95f42098.
//
// Solidity: function cancelAskOrder(address _collection, uint256 _tokenId) returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) CancelAskOrder(opts *bind.TransactOpts, _collection common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "cancelAskOrder", _collection, _tokenId)
}

// CancelAskOrder is a paid mutator transaction binding the contract method 0x95f42098.
//
// Solidity: function cancelAskOrder(address _collection, uint256 _tokenId) returns()
func (_Pancakeswap0 *Pancakeswap0Session) CancelAskOrder(_collection common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.CancelAskOrder(&_Pancakeswap0.TransactOpts, _collection, _tokenId)
}

// CancelAskOrder is a paid mutator transaction binding the contract method 0x95f42098.
//
// Solidity: function cancelAskOrder(address _collection, uint256 _tokenId) returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) CancelAskOrder(_collection common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.CancelAskOrder(&_Pancakeswap0.TransactOpts, _collection, _tokenId)
}

// ClaimPendingRevenue is a paid mutator transaction binding the contract method 0xb6c1d2cf.
//
// Solidity: function claimPendingRevenue() returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) ClaimPendingRevenue(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "claimPendingRevenue")
}

// ClaimPendingRevenue is a paid mutator transaction binding the contract method 0xb6c1d2cf.
//
// Solidity: function claimPendingRevenue() returns()
func (_Pancakeswap0 *Pancakeswap0Session) ClaimPendingRevenue() (*types.Transaction, error) {
	return _Pancakeswap0.Contract.ClaimPendingRevenue(&_Pancakeswap0.TransactOpts)
}

// ClaimPendingRevenue is a paid mutator transaction binding the contract method 0xb6c1d2cf.
//
// Solidity: function claimPendingRevenue() returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) ClaimPendingRevenue() (*types.Transaction, error) {
	return _Pancakeswap0.Contract.ClaimPendingRevenue(&_Pancakeswap0.TransactOpts)
}

// CloseCollectionForTradingAndListing is a paid mutator transaction binding the contract method 0xddf9ba6f.
//
// Solidity: function closeCollectionForTradingAndListing(address _collection) returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) CloseCollectionForTradingAndListing(opts *bind.TransactOpts, _collection common.Address) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "closeCollectionForTradingAndListing", _collection)
}

// CloseCollectionForTradingAndListing is a paid mutator transaction binding the contract method 0xddf9ba6f.
//
// Solidity: function closeCollectionForTradingAndListing(address _collection) returns()
func (_Pancakeswap0 *Pancakeswap0Session) CloseCollectionForTradingAndListing(_collection common.Address) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.CloseCollectionForTradingAndListing(&_Pancakeswap0.TransactOpts, _collection)
}

// CloseCollectionForTradingAndListing is a paid mutator transaction binding the contract method 0xddf9ba6f.
//
// Solidity: function closeCollectionForTradingAndListing(address _collection) returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) CloseCollectionForTradingAndListing(_collection common.Address) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.CloseCollectionForTradingAndListing(&_Pancakeswap0.TransactOpts, _collection)
}

// CreateAskOrder is a paid mutator transaction binding the contract method 0x803b64a1.
//
// Solidity: function createAskOrder(address _collection, uint256 _tokenId, uint256 _askPrice) returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) CreateAskOrder(opts *bind.TransactOpts, _collection common.Address, _tokenId *big.Int, _askPrice *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "createAskOrder", _collection, _tokenId, _askPrice)
}

// CreateAskOrder is a paid mutator transaction binding the contract method 0x803b64a1.
//
// Solidity: function createAskOrder(address _collection, uint256 _tokenId, uint256 _askPrice) returns()
func (_Pancakeswap0 *Pancakeswap0Session) CreateAskOrder(_collection common.Address, _tokenId *big.Int, _askPrice *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.CreateAskOrder(&_Pancakeswap0.TransactOpts, _collection, _tokenId, _askPrice)
}

// CreateAskOrder is a paid mutator transaction binding the contract method 0x803b64a1.
//
// Solidity: function createAskOrder(address _collection, uint256 _tokenId, uint256 _askPrice) returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) CreateAskOrder(_collection common.Address, _tokenId *big.Int, _askPrice *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.CreateAskOrder(&_Pancakeswap0.TransactOpts, _collection, _tokenId, _askPrice)
}

// ModifyAskOrder is a paid mutator transaction binding the contract method 0x9b2adcdc.
//
// Solidity: function modifyAskOrder(address _collection, uint256 _tokenId, uint256 _newPrice) returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) ModifyAskOrder(opts *bind.TransactOpts, _collection common.Address, _tokenId *big.Int, _newPrice *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "modifyAskOrder", _collection, _tokenId, _newPrice)
}

// ModifyAskOrder is a paid mutator transaction binding the contract method 0x9b2adcdc.
//
// Solidity: function modifyAskOrder(address _collection, uint256 _tokenId, uint256 _newPrice) returns()
func (_Pancakeswap0 *Pancakeswap0Session) ModifyAskOrder(_collection common.Address, _tokenId *big.Int, _newPrice *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.ModifyAskOrder(&_Pancakeswap0.TransactOpts, _collection, _tokenId, _newPrice)
}

// ModifyAskOrder is a paid mutator transaction binding the contract method 0x9b2adcdc.
//
// Solidity: function modifyAskOrder(address _collection, uint256 _tokenId, uint256 _newPrice) returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) ModifyAskOrder(_collection common.Address, _tokenId *big.Int, _newPrice *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.ModifyAskOrder(&_Pancakeswap0.TransactOpts, _collection, _tokenId, _newPrice)
}

// ModifyCollection is a paid mutator transaction binding the contract method 0x6b248032.
//
// Solidity: function modifyCollection(address _collection, address _creator, address _whitelistChecker, uint256 _tradingFee, uint256 _creatorFee) returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) ModifyCollection(opts *bind.TransactOpts, _collection common.Address, _creator common.Address, _whitelistChecker common.Address, _tradingFee *big.Int, _creatorFee *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "modifyCollection", _collection, _creator, _whitelistChecker, _tradingFee, _creatorFee)
}

// ModifyCollection is a paid mutator transaction binding the contract method 0x6b248032.
//
// Solidity: function modifyCollection(address _collection, address _creator, address _whitelistChecker, uint256 _tradingFee, uint256 _creatorFee) returns()
func (_Pancakeswap0 *Pancakeswap0Session) ModifyCollection(_collection common.Address, _creator common.Address, _whitelistChecker common.Address, _tradingFee *big.Int, _creatorFee *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.ModifyCollection(&_Pancakeswap0.TransactOpts, _collection, _creator, _whitelistChecker, _tradingFee, _creatorFee)
}

// ModifyCollection is a paid mutator transaction binding the contract method 0x6b248032.
//
// Solidity: function modifyCollection(address _collection, address _creator, address _whitelistChecker, uint256 _tradingFee, uint256 _creatorFee) returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) ModifyCollection(_collection common.Address, _creator common.Address, _whitelistChecker common.Address, _tradingFee *big.Int, _creatorFee *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.ModifyCollection(&_Pancakeswap0.TransactOpts, _collection, _creator, _whitelistChecker, _tradingFee, _creatorFee)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Pancakeswap0 *Pancakeswap0Transactor) OnERC721Received(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "onERC721Received", arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Pancakeswap0 *Pancakeswap0Session) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.OnERC721Received(&_Pancakeswap0.TransactOpts, arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Pancakeswap0 *Pancakeswap0TransactorSession) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.OnERC721Received(&_Pancakeswap0.TransactOpts, arg0, arg1, arg2, arg3)
}

// RecoverFungibleTokens is a paid mutator transaction binding the contract method 0x3ffd1781.
//
// Solidity: function recoverFungibleTokens(address _token) returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) RecoverFungibleTokens(opts *bind.TransactOpts, _token common.Address) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "recoverFungibleTokens", _token)
}

// RecoverFungibleTokens is a paid mutator transaction binding the contract method 0x3ffd1781.
//
// Solidity: function recoverFungibleTokens(address _token) returns()
func (_Pancakeswap0 *Pancakeswap0Session) RecoverFungibleTokens(_token common.Address) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.RecoverFungibleTokens(&_Pancakeswap0.TransactOpts, _token)
}

// RecoverFungibleTokens is a paid mutator transaction binding the contract method 0x3ffd1781.
//
// Solidity: function recoverFungibleTokens(address _token) returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) RecoverFungibleTokens(_token common.Address) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.RecoverFungibleTokens(&_Pancakeswap0.TransactOpts, _token)
}

// RecoverNonFungibleToken is a paid mutator transaction binding the contract method 0xbb0fd147.
//
// Solidity: function recoverNonFungibleToken(address _token, uint256 _tokenId) returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) RecoverNonFungibleToken(opts *bind.TransactOpts, _token common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "recoverNonFungibleToken", _token, _tokenId)
}

// RecoverNonFungibleToken is a paid mutator transaction binding the contract method 0xbb0fd147.
//
// Solidity: function recoverNonFungibleToken(address _token, uint256 _tokenId) returns()
func (_Pancakeswap0 *Pancakeswap0Session) RecoverNonFungibleToken(_token common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.RecoverNonFungibleToken(&_Pancakeswap0.TransactOpts, _token, _tokenId)
}

// RecoverNonFungibleToken is a paid mutator transaction binding the contract method 0xbb0fd147.
//
// Solidity: function recoverNonFungibleToken(address _token, uint256 _tokenId) returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) RecoverNonFungibleToken(_token common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.RecoverNonFungibleToken(&_Pancakeswap0.TransactOpts, _token, _tokenId)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Pancakeswap0 *Pancakeswap0Session) RenounceOwnership() (*types.Transaction, error) {
	return _Pancakeswap0.Contract.RenounceOwnership(&_Pancakeswap0.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Pancakeswap0.Contract.RenounceOwnership(&_Pancakeswap0.TransactOpts)
}

// SetAdminAndTreasuryAddresses is a paid mutator transaction binding the contract method 0x791e8018.
//
// Solidity: function setAdminAndTreasuryAddresses(address _adminAddress, address _treasuryAddress) returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) SetAdminAndTreasuryAddresses(opts *bind.TransactOpts, _adminAddress common.Address, _treasuryAddress common.Address) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "setAdminAndTreasuryAddresses", _adminAddress, _treasuryAddress)
}

// SetAdminAndTreasuryAddresses is a paid mutator transaction binding the contract method 0x791e8018.
//
// Solidity: function setAdminAndTreasuryAddresses(address _adminAddress, address _treasuryAddress) returns()
func (_Pancakeswap0 *Pancakeswap0Session) SetAdminAndTreasuryAddresses(_adminAddress common.Address, _treasuryAddress common.Address) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.SetAdminAndTreasuryAddresses(&_Pancakeswap0.TransactOpts, _adminAddress, _treasuryAddress)
}

// SetAdminAndTreasuryAddresses is a paid mutator transaction binding the contract method 0x791e8018.
//
// Solidity: function setAdminAndTreasuryAddresses(address _adminAddress, address _treasuryAddress) returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) SetAdminAndTreasuryAddresses(_adminAddress common.Address, _treasuryAddress common.Address) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.SetAdminAndTreasuryAddresses(&_Pancakeswap0.TransactOpts, _adminAddress, _treasuryAddress)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Pancakeswap0 *Pancakeswap0Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.TransferOwnership(&_Pancakeswap0.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.TransferOwnership(&_Pancakeswap0.TransactOpts, newOwner)
}

// UpdateMinimumAndMaximumPrices is a paid mutator transaction binding the contract method 0xb2ea3931.
//
// Solidity: function updateMinimumAndMaximumPrices(uint256 _minimumAskPrice, uint256 _maximumAskPrice) returns()
func (_Pancakeswap0 *Pancakeswap0Transactor) UpdateMinimumAndMaximumPrices(opts *bind.TransactOpts, _minimumAskPrice *big.Int, _maximumAskPrice *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.contract.Transact(opts, "updateMinimumAndMaximumPrices", _minimumAskPrice, _maximumAskPrice)
}

// UpdateMinimumAndMaximumPrices is a paid mutator transaction binding the contract method 0xb2ea3931.
//
// Solidity: function updateMinimumAndMaximumPrices(uint256 _minimumAskPrice, uint256 _maximumAskPrice) returns()
func (_Pancakeswap0 *Pancakeswap0Session) UpdateMinimumAndMaximumPrices(_minimumAskPrice *big.Int, _maximumAskPrice *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.UpdateMinimumAndMaximumPrices(&_Pancakeswap0.TransactOpts, _minimumAskPrice, _maximumAskPrice)
}

// UpdateMinimumAndMaximumPrices is a paid mutator transaction binding the contract method 0xb2ea3931.
//
// Solidity: function updateMinimumAndMaximumPrices(uint256 _minimumAskPrice, uint256 _maximumAskPrice) returns()
func (_Pancakeswap0 *Pancakeswap0TransactorSession) UpdateMinimumAndMaximumPrices(_minimumAskPrice *big.Int, _maximumAskPrice *big.Int) (*types.Transaction, error) {
	return _Pancakeswap0.Contract.UpdateMinimumAndMaximumPrices(&_Pancakeswap0.TransactOpts, _minimumAskPrice, _maximumAskPrice)
}

// Pancakeswap0AskCancelIterator is returned from FilterAskCancel and is used to iterate over the raw logs and unpacked data for AskCancel events raised by the Pancakeswap0 contract.
type Pancakeswap0AskCancelIterator struct {
	Event *Pancakeswap0AskCancel // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Pancakeswap0AskCancelIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Pancakeswap0AskCancel)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Pancakeswap0AskCancel)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Pancakeswap0AskCancelIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Pancakeswap0AskCancelIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Pancakeswap0AskCancel represents a AskCancel event raised by the Pancakeswap0 contract.
type Pancakeswap0AskCancel struct {
	Collection common.Address
	Seller     common.Address
	TokenId    *big.Int
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterAskCancel is a free log retrieval operation binding the contract event 0xa5764ea233325154a6388255c54b9f302ec2ed8586af0a9a606cd64e88298eb7.
//
// Solidity: event AskCancel(address indexed collection, address indexed seller, uint256 indexed tokenId)
func (_Pancakeswap0 *Pancakeswap0Filterer) FilterAskCancel(opts *bind.FilterOpts, collection []common.Address, seller []common.Address, tokenId []*big.Int) (*Pancakeswap0AskCancelIterator, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Pancakeswap0.contract.FilterLogs(opts, "AskCancel", collectionRule, sellerRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0AskCancelIterator{contract: _Pancakeswap0.contract, event: "AskCancel", logs: logs, sub: sub}, nil
}

// WatchAskCancel is a free log subscription operation binding the contract event 0xa5764ea233325154a6388255c54b9f302ec2ed8586af0a9a606cd64e88298eb7.
//
// Solidity: event AskCancel(address indexed collection, address indexed seller, uint256 indexed tokenId)
func (_Pancakeswap0 *Pancakeswap0Filterer) WatchAskCancel(opts *bind.WatchOpts, sink chan<- *Pancakeswap0AskCancel, collection []common.Address, seller []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Pancakeswap0.contract.WatchLogs(opts, "AskCancel", collectionRule, sellerRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Pancakeswap0AskCancel)
				if err := _Pancakeswap0.contract.UnpackLog(event, "AskCancel", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAskCancel is a log parse operation binding the contract event 0xa5764ea233325154a6388255c54b9f302ec2ed8586af0a9a606cd64e88298eb7.
//
// Solidity: event AskCancel(address indexed collection, address indexed seller, uint256 indexed tokenId)
func (_Pancakeswap0 *Pancakeswap0Filterer) ParseAskCancel(log types.Log) (*Pancakeswap0AskCancel, error) {
	event := new(Pancakeswap0AskCancel)
	if err := _Pancakeswap0.contract.UnpackLog(event, "AskCancel", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Pancakeswap0AskNewIterator is returned from FilterAskNew and is used to iterate over the raw logs and unpacked data for AskNew events raised by the Pancakeswap0 contract.
type Pancakeswap0AskNewIterator struct {
	Event *Pancakeswap0AskNew // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Pancakeswap0AskNewIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Pancakeswap0AskNew)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Pancakeswap0AskNew)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Pancakeswap0AskNewIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Pancakeswap0AskNewIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Pancakeswap0AskNew represents a AskNew event raised by the Pancakeswap0 contract.
type Pancakeswap0AskNew struct {
	Collection common.Address
	Seller     common.Address
	TokenId    *big.Int
	AskPrice   *big.Int
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterAskNew is a free log retrieval operation binding the contract event 0xaa0431463bb62c812c04a1e5310cf43dd9baedc7a8eab71e02f572c741467e8c.
//
// Solidity: event AskNew(address indexed collection, address indexed seller, uint256 indexed tokenId, uint256 askPrice)
func (_Pancakeswap0 *Pancakeswap0Filterer) FilterAskNew(opts *bind.FilterOpts, collection []common.Address, seller []common.Address, tokenId []*big.Int) (*Pancakeswap0AskNewIterator, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Pancakeswap0.contract.FilterLogs(opts, "AskNew", collectionRule, sellerRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0AskNewIterator{contract: _Pancakeswap0.contract, event: "AskNew", logs: logs, sub: sub}, nil
}

// WatchAskNew is a free log subscription operation binding the contract event 0xaa0431463bb62c812c04a1e5310cf43dd9baedc7a8eab71e02f572c741467e8c.
//
// Solidity: event AskNew(address indexed collection, address indexed seller, uint256 indexed tokenId, uint256 askPrice)
func (_Pancakeswap0 *Pancakeswap0Filterer) WatchAskNew(opts *bind.WatchOpts, sink chan<- *Pancakeswap0AskNew, collection []common.Address, seller []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Pancakeswap0.contract.WatchLogs(opts, "AskNew", collectionRule, sellerRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Pancakeswap0AskNew)
				if err := _Pancakeswap0.contract.UnpackLog(event, "AskNew", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAskNew is a log parse operation binding the contract event 0xaa0431463bb62c812c04a1e5310cf43dd9baedc7a8eab71e02f572c741467e8c.
//
// Solidity: event AskNew(address indexed collection, address indexed seller, uint256 indexed tokenId, uint256 askPrice)
func (_Pancakeswap0 *Pancakeswap0Filterer) ParseAskNew(log types.Log) (*Pancakeswap0AskNew, error) {
	event := new(Pancakeswap0AskNew)
	if err := _Pancakeswap0.contract.UnpackLog(event, "AskNew", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Pancakeswap0AskUpdateIterator is returned from FilterAskUpdate and is used to iterate over the raw logs and unpacked data for AskUpdate events raised by the Pancakeswap0 contract.
type Pancakeswap0AskUpdateIterator struct {
	Event *Pancakeswap0AskUpdate // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Pancakeswap0AskUpdateIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Pancakeswap0AskUpdate)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Pancakeswap0AskUpdate)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Pancakeswap0AskUpdateIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Pancakeswap0AskUpdateIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Pancakeswap0AskUpdate represents a AskUpdate event raised by the Pancakeswap0 contract.
type Pancakeswap0AskUpdate struct {
	Collection common.Address
	Seller     common.Address
	TokenId    *big.Int
	AskPrice   *big.Int
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterAskUpdate is a free log retrieval operation binding the contract event 0x492437c0ade072acdf12e63a19d8df68bb5e414e2098dc470239df87702343ea.
//
// Solidity: event AskUpdate(address indexed collection, address indexed seller, uint256 indexed tokenId, uint256 askPrice)
func (_Pancakeswap0 *Pancakeswap0Filterer) FilterAskUpdate(opts *bind.FilterOpts, collection []common.Address, seller []common.Address, tokenId []*big.Int) (*Pancakeswap0AskUpdateIterator, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Pancakeswap0.contract.FilterLogs(opts, "AskUpdate", collectionRule, sellerRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0AskUpdateIterator{contract: _Pancakeswap0.contract, event: "AskUpdate", logs: logs, sub: sub}, nil
}

// WatchAskUpdate is a free log subscription operation binding the contract event 0x492437c0ade072acdf12e63a19d8df68bb5e414e2098dc470239df87702343ea.
//
// Solidity: event AskUpdate(address indexed collection, address indexed seller, uint256 indexed tokenId, uint256 askPrice)
func (_Pancakeswap0 *Pancakeswap0Filterer) WatchAskUpdate(opts *bind.WatchOpts, sink chan<- *Pancakeswap0AskUpdate, collection []common.Address, seller []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Pancakeswap0.contract.WatchLogs(opts, "AskUpdate", collectionRule, sellerRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Pancakeswap0AskUpdate)
				if err := _Pancakeswap0.contract.UnpackLog(event, "AskUpdate", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAskUpdate is a log parse operation binding the contract event 0x492437c0ade072acdf12e63a19d8df68bb5e414e2098dc470239df87702343ea.
//
// Solidity: event AskUpdate(address indexed collection, address indexed seller, uint256 indexed tokenId, uint256 askPrice)
func (_Pancakeswap0 *Pancakeswap0Filterer) ParseAskUpdate(log types.Log) (*Pancakeswap0AskUpdate, error) {
	event := new(Pancakeswap0AskUpdate)
	if err := _Pancakeswap0.contract.UnpackLog(event, "AskUpdate", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Pancakeswap0CollectionCloseIterator is returned from FilterCollectionClose and is used to iterate over the raw logs and unpacked data for CollectionClose events raised by the Pancakeswap0 contract.
type Pancakeswap0CollectionCloseIterator struct {
	Event *Pancakeswap0CollectionClose // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Pancakeswap0CollectionCloseIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Pancakeswap0CollectionClose)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Pancakeswap0CollectionClose)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Pancakeswap0CollectionCloseIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Pancakeswap0CollectionCloseIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Pancakeswap0CollectionClose represents a CollectionClose event raised by the Pancakeswap0 contract.
type Pancakeswap0CollectionClose struct {
	Collection common.Address
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterCollectionClose is a free log retrieval operation binding the contract event 0xf6df65a3d7d931af345d93f0420a22e8910de36c0d027c1d1ac93d5be509f5a9.
//
// Solidity: event CollectionClose(address indexed collection)
func (_Pancakeswap0 *Pancakeswap0Filterer) FilterCollectionClose(opts *bind.FilterOpts, collection []common.Address) (*Pancakeswap0CollectionCloseIterator, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}

	logs, sub, err := _Pancakeswap0.contract.FilterLogs(opts, "CollectionClose", collectionRule)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0CollectionCloseIterator{contract: _Pancakeswap0.contract, event: "CollectionClose", logs: logs, sub: sub}, nil
}

// WatchCollectionClose is a free log subscription operation binding the contract event 0xf6df65a3d7d931af345d93f0420a22e8910de36c0d027c1d1ac93d5be509f5a9.
//
// Solidity: event CollectionClose(address indexed collection)
func (_Pancakeswap0 *Pancakeswap0Filterer) WatchCollectionClose(opts *bind.WatchOpts, sink chan<- *Pancakeswap0CollectionClose, collection []common.Address) (event.Subscription, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}

	logs, sub, err := _Pancakeswap0.contract.WatchLogs(opts, "CollectionClose", collectionRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Pancakeswap0CollectionClose)
				if err := _Pancakeswap0.contract.UnpackLog(event, "CollectionClose", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCollectionClose is a log parse operation binding the contract event 0xf6df65a3d7d931af345d93f0420a22e8910de36c0d027c1d1ac93d5be509f5a9.
//
// Solidity: event CollectionClose(address indexed collection)
func (_Pancakeswap0 *Pancakeswap0Filterer) ParseCollectionClose(log types.Log) (*Pancakeswap0CollectionClose, error) {
	event := new(Pancakeswap0CollectionClose)
	if err := _Pancakeswap0.contract.UnpackLog(event, "CollectionClose", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Pancakeswap0CollectionNewIterator is returned from FilterCollectionNew and is used to iterate over the raw logs and unpacked data for CollectionNew events raised by the Pancakeswap0 contract.
type Pancakeswap0CollectionNewIterator struct {
	Event *Pancakeswap0CollectionNew // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Pancakeswap0CollectionNewIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Pancakeswap0CollectionNew)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Pancakeswap0CollectionNew)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Pancakeswap0CollectionNewIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Pancakeswap0CollectionNewIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Pancakeswap0CollectionNew represents a CollectionNew event raised by the Pancakeswap0 contract.
type Pancakeswap0CollectionNew struct {
	Collection       common.Address
	Creator          common.Address
	WhitelistChecker common.Address
	TradingFee       *big.Int
	CreatorFee       *big.Int
	Raw              types.Log // Blockchain specific contextual infos
}

// FilterCollectionNew is a free log retrieval operation binding the contract event 0x4863c75720ebabd11e0cf1c60b58786b5572680a20fa1b568c203964ae56026a.
//
// Solidity: event CollectionNew(address indexed collection, address indexed creator, address indexed whitelistChecker, uint256 tradingFee, uint256 creatorFee)
func (_Pancakeswap0 *Pancakeswap0Filterer) FilterCollectionNew(opts *bind.FilterOpts, collection []common.Address, creator []common.Address, whitelistChecker []common.Address) (*Pancakeswap0CollectionNewIterator, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var creatorRule []interface{}
	for _, creatorItem := range creator {
		creatorRule = append(creatorRule, creatorItem)
	}
	var whitelistCheckerRule []interface{}
	for _, whitelistCheckerItem := range whitelistChecker {
		whitelistCheckerRule = append(whitelistCheckerRule, whitelistCheckerItem)
	}

	logs, sub, err := _Pancakeswap0.contract.FilterLogs(opts, "CollectionNew", collectionRule, creatorRule, whitelistCheckerRule)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0CollectionNewIterator{contract: _Pancakeswap0.contract, event: "CollectionNew", logs: logs, sub: sub}, nil
}

// WatchCollectionNew is a free log subscription operation binding the contract event 0x4863c75720ebabd11e0cf1c60b58786b5572680a20fa1b568c203964ae56026a.
//
// Solidity: event CollectionNew(address indexed collection, address indexed creator, address indexed whitelistChecker, uint256 tradingFee, uint256 creatorFee)
func (_Pancakeswap0 *Pancakeswap0Filterer) WatchCollectionNew(opts *bind.WatchOpts, sink chan<- *Pancakeswap0CollectionNew, collection []common.Address, creator []common.Address, whitelistChecker []common.Address) (event.Subscription, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var creatorRule []interface{}
	for _, creatorItem := range creator {
		creatorRule = append(creatorRule, creatorItem)
	}
	var whitelistCheckerRule []interface{}
	for _, whitelistCheckerItem := range whitelistChecker {
		whitelistCheckerRule = append(whitelistCheckerRule, whitelistCheckerItem)
	}

	logs, sub, err := _Pancakeswap0.contract.WatchLogs(opts, "CollectionNew", collectionRule, creatorRule, whitelistCheckerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Pancakeswap0CollectionNew)
				if err := _Pancakeswap0.contract.UnpackLog(event, "CollectionNew", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCollectionNew is a log parse operation binding the contract event 0x4863c75720ebabd11e0cf1c60b58786b5572680a20fa1b568c203964ae56026a.
//
// Solidity: event CollectionNew(address indexed collection, address indexed creator, address indexed whitelistChecker, uint256 tradingFee, uint256 creatorFee)
func (_Pancakeswap0 *Pancakeswap0Filterer) ParseCollectionNew(log types.Log) (*Pancakeswap0CollectionNew, error) {
	event := new(Pancakeswap0CollectionNew)
	if err := _Pancakeswap0.contract.UnpackLog(event, "CollectionNew", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Pancakeswap0CollectionUpdateIterator is returned from FilterCollectionUpdate and is used to iterate over the raw logs and unpacked data for CollectionUpdate events raised by the Pancakeswap0 contract.
type Pancakeswap0CollectionUpdateIterator struct {
	Event *Pancakeswap0CollectionUpdate // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Pancakeswap0CollectionUpdateIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Pancakeswap0CollectionUpdate)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Pancakeswap0CollectionUpdate)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Pancakeswap0CollectionUpdateIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Pancakeswap0CollectionUpdateIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Pancakeswap0CollectionUpdate represents a CollectionUpdate event raised by the Pancakeswap0 contract.
type Pancakeswap0CollectionUpdate struct {
	Collection       common.Address
	Creator          common.Address
	WhitelistChecker common.Address
	TradingFee       *big.Int
	CreatorFee       *big.Int
	Raw              types.Log // Blockchain specific contextual infos
}

// FilterCollectionUpdate is a free log retrieval operation binding the contract event 0x655bd8b9d972737b566ec5473b537d81d079c6e251d4986005bf3f36000eacef.
//
// Solidity: event CollectionUpdate(address indexed collection, address indexed creator, address indexed whitelistChecker, uint256 tradingFee, uint256 creatorFee)
func (_Pancakeswap0 *Pancakeswap0Filterer) FilterCollectionUpdate(opts *bind.FilterOpts, collection []common.Address, creator []common.Address, whitelistChecker []common.Address) (*Pancakeswap0CollectionUpdateIterator, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var creatorRule []interface{}
	for _, creatorItem := range creator {
		creatorRule = append(creatorRule, creatorItem)
	}
	var whitelistCheckerRule []interface{}
	for _, whitelistCheckerItem := range whitelistChecker {
		whitelistCheckerRule = append(whitelistCheckerRule, whitelistCheckerItem)
	}

	logs, sub, err := _Pancakeswap0.contract.FilterLogs(opts, "CollectionUpdate", collectionRule, creatorRule, whitelistCheckerRule)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0CollectionUpdateIterator{contract: _Pancakeswap0.contract, event: "CollectionUpdate", logs: logs, sub: sub}, nil
}

// WatchCollectionUpdate is a free log subscription operation binding the contract event 0x655bd8b9d972737b566ec5473b537d81d079c6e251d4986005bf3f36000eacef.
//
// Solidity: event CollectionUpdate(address indexed collection, address indexed creator, address indexed whitelistChecker, uint256 tradingFee, uint256 creatorFee)
func (_Pancakeswap0 *Pancakeswap0Filterer) WatchCollectionUpdate(opts *bind.WatchOpts, sink chan<- *Pancakeswap0CollectionUpdate, collection []common.Address, creator []common.Address, whitelistChecker []common.Address) (event.Subscription, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var creatorRule []interface{}
	for _, creatorItem := range creator {
		creatorRule = append(creatorRule, creatorItem)
	}
	var whitelistCheckerRule []interface{}
	for _, whitelistCheckerItem := range whitelistChecker {
		whitelistCheckerRule = append(whitelistCheckerRule, whitelistCheckerItem)
	}

	logs, sub, err := _Pancakeswap0.contract.WatchLogs(opts, "CollectionUpdate", collectionRule, creatorRule, whitelistCheckerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Pancakeswap0CollectionUpdate)
				if err := _Pancakeswap0.contract.UnpackLog(event, "CollectionUpdate", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCollectionUpdate is a log parse operation binding the contract event 0x655bd8b9d972737b566ec5473b537d81d079c6e251d4986005bf3f36000eacef.
//
// Solidity: event CollectionUpdate(address indexed collection, address indexed creator, address indexed whitelistChecker, uint256 tradingFee, uint256 creatorFee)
func (_Pancakeswap0 *Pancakeswap0Filterer) ParseCollectionUpdate(log types.Log) (*Pancakeswap0CollectionUpdate, error) {
	event := new(Pancakeswap0CollectionUpdate)
	if err := _Pancakeswap0.contract.UnpackLog(event, "CollectionUpdate", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Pancakeswap0NewAdminAndTreasuryAddressesIterator is returned from FilterNewAdminAndTreasuryAddresses and is used to iterate over the raw logs and unpacked data for NewAdminAndTreasuryAddresses events raised by the Pancakeswap0 contract.
type Pancakeswap0NewAdminAndTreasuryAddressesIterator struct {
	Event *Pancakeswap0NewAdminAndTreasuryAddresses // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Pancakeswap0NewAdminAndTreasuryAddressesIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Pancakeswap0NewAdminAndTreasuryAddresses)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Pancakeswap0NewAdminAndTreasuryAddresses)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Pancakeswap0NewAdminAndTreasuryAddressesIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Pancakeswap0NewAdminAndTreasuryAddressesIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Pancakeswap0NewAdminAndTreasuryAddresses represents a NewAdminAndTreasuryAddresses event raised by the Pancakeswap0 contract.
type Pancakeswap0NewAdminAndTreasuryAddresses struct {
	Admin    common.Address
	Treasury common.Address
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterNewAdminAndTreasuryAddresses is a free log retrieval operation binding the contract event 0x2a1b1d9b1cb634e511bbf940f66e95082249486cf37efdb4bccba1c5f4c00b9b.
//
// Solidity: event NewAdminAndTreasuryAddresses(address indexed admin, address indexed treasury)
func (_Pancakeswap0 *Pancakeswap0Filterer) FilterNewAdminAndTreasuryAddresses(opts *bind.FilterOpts, admin []common.Address, treasury []common.Address) (*Pancakeswap0NewAdminAndTreasuryAddressesIterator, error) {

	var adminRule []interface{}
	for _, adminItem := range admin {
		adminRule = append(adminRule, adminItem)
	}
	var treasuryRule []interface{}
	for _, treasuryItem := range treasury {
		treasuryRule = append(treasuryRule, treasuryItem)
	}

	logs, sub, err := _Pancakeswap0.contract.FilterLogs(opts, "NewAdminAndTreasuryAddresses", adminRule, treasuryRule)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0NewAdminAndTreasuryAddressesIterator{contract: _Pancakeswap0.contract, event: "NewAdminAndTreasuryAddresses", logs: logs, sub: sub}, nil
}

// WatchNewAdminAndTreasuryAddresses is a free log subscription operation binding the contract event 0x2a1b1d9b1cb634e511bbf940f66e95082249486cf37efdb4bccba1c5f4c00b9b.
//
// Solidity: event NewAdminAndTreasuryAddresses(address indexed admin, address indexed treasury)
func (_Pancakeswap0 *Pancakeswap0Filterer) WatchNewAdminAndTreasuryAddresses(opts *bind.WatchOpts, sink chan<- *Pancakeswap0NewAdminAndTreasuryAddresses, admin []common.Address, treasury []common.Address) (event.Subscription, error) {

	var adminRule []interface{}
	for _, adminItem := range admin {
		adminRule = append(adminRule, adminItem)
	}
	var treasuryRule []interface{}
	for _, treasuryItem := range treasury {
		treasuryRule = append(treasuryRule, treasuryItem)
	}

	logs, sub, err := _Pancakeswap0.contract.WatchLogs(opts, "NewAdminAndTreasuryAddresses", adminRule, treasuryRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Pancakeswap0NewAdminAndTreasuryAddresses)
				if err := _Pancakeswap0.contract.UnpackLog(event, "NewAdminAndTreasuryAddresses", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewAdminAndTreasuryAddresses is a log parse operation binding the contract event 0x2a1b1d9b1cb634e511bbf940f66e95082249486cf37efdb4bccba1c5f4c00b9b.
//
// Solidity: event NewAdminAndTreasuryAddresses(address indexed admin, address indexed treasury)
func (_Pancakeswap0 *Pancakeswap0Filterer) ParseNewAdminAndTreasuryAddresses(log types.Log) (*Pancakeswap0NewAdminAndTreasuryAddresses, error) {
	event := new(Pancakeswap0NewAdminAndTreasuryAddresses)
	if err := _Pancakeswap0.contract.UnpackLog(event, "NewAdminAndTreasuryAddresses", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Pancakeswap0NewMinimumAndMaximumAskPricesIterator is returned from FilterNewMinimumAndMaximumAskPrices and is used to iterate over the raw logs and unpacked data for NewMinimumAndMaximumAskPrices events raised by the Pancakeswap0 contract.
type Pancakeswap0NewMinimumAndMaximumAskPricesIterator struct {
	Event *Pancakeswap0NewMinimumAndMaximumAskPrices // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Pancakeswap0NewMinimumAndMaximumAskPricesIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Pancakeswap0NewMinimumAndMaximumAskPrices)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Pancakeswap0NewMinimumAndMaximumAskPrices)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Pancakeswap0NewMinimumAndMaximumAskPricesIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Pancakeswap0NewMinimumAndMaximumAskPricesIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Pancakeswap0NewMinimumAndMaximumAskPrices represents a NewMinimumAndMaximumAskPrices event raised by the Pancakeswap0 contract.
type Pancakeswap0NewMinimumAndMaximumAskPrices struct {
	MinimumAskPrice *big.Int
	MaximumAskPrice *big.Int
	Raw             types.Log // Blockchain specific contextual infos
}

// FilterNewMinimumAndMaximumAskPrices is a free log retrieval operation binding the contract event 0xb25b7c478b617cc7fb07f97a49dc7dcba200dae3abbcb0aa17216e2ae6deaef7.
//
// Solidity: event NewMinimumAndMaximumAskPrices(uint256 minimumAskPrice, uint256 maximumAskPrice)
func (_Pancakeswap0 *Pancakeswap0Filterer) FilterNewMinimumAndMaximumAskPrices(opts *bind.FilterOpts) (*Pancakeswap0NewMinimumAndMaximumAskPricesIterator, error) {

	logs, sub, err := _Pancakeswap0.contract.FilterLogs(opts, "NewMinimumAndMaximumAskPrices")
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0NewMinimumAndMaximumAskPricesIterator{contract: _Pancakeswap0.contract, event: "NewMinimumAndMaximumAskPrices", logs: logs, sub: sub}, nil
}

// WatchNewMinimumAndMaximumAskPrices is a free log subscription operation binding the contract event 0xb25b7c478b617cc7fb07f97a49dc7dcba200dae3abbcb0aa17216e2ae6deaef7.
//
// Solidity: event NewMinimumAndMaximumAskPrices(uint256 minimumAskPrice, uint256 maximumAskPrice)
func (_Pancakeswap0 *Pancakeswap0Filterer) WatchNewMinimumAndMaximumAskPrices(opts *bind.WatchOpts, sink chan<- *Pancakeswap0NewMinimumAndMaximumAskPrices) (event.Subscription, error) {

	logs, sub, err := _Pancakeswap0.contract.WatchLogs(opts, "NewMinimumAndMaximumAskPrices")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Pancakeswap0NewMinimumAndMaximumAskPrices)
				if err := _Pancakeswap0.contract.UnpackLog(event, "NewMinimumAndMaximumAskPrices", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewMinimumAndMaximumAskPrices is a log parse operation binding the contract event 0xb25b7c478b617cc7fb07f97a49dc7dcba200dae3abbcb0aa17216e2ae6deaef7.
//
// Solidity: event NewMinimumAndMaximumAskPrices(uint256 minimumAskPrice, uint256 maximumAskPrice)
func (_Pancakeswap0 *Pancakeswap0Filterer) ParseNewMinimumAndMaximumAskPrices(log types.Log) (*Pancakeswap0NewMinimumAndMaximumAskPrices, error) {
	event := new(Pancakeswap0NewMinimumAndMaximumAskPrices)
	if err := _Pancakeswap0.contract.UnpackLog(event, "NewMinimumAndMaximumAskPrices", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Pancakeswap0NonFungibleTokenRecoveryIterator is returned from FilterNonFungibleTokenRecovery and is used to iterate over the raw logs and unpacked data for NonFungibleTokenRecovery events raised by the Pancakeswap0 contract.
type Pancakeswap0NonFungibleTokenRecoveryIterator struct {
	Event *Pancakeswap0NonFungibleTokenRecovery // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Pancakeswap0NonFungibleTokenRecoveryIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Pancakeswap0NonFungibleTokenRecovery)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Pancakeswap0NonFungibleTokenRecovery)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Pancakeswap0NonFungibleTokenRecoveryIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Pancakeswap0NonFungibleTokenRecoveryIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Pancakeswap0NonFungibleTokenRecovery represents a NonFungibleTokenRecovery event raised by the Pancakeswap0 contract.
type Pancakeswap0NonFungibleTokenRecovery struct {
	Token   common.Address
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterNonFungibleTokenRecovery is a free log retrieval operation binding the contract event 0x861c3ea25dbda3af0bf5d258ba8582c0276c9446b1479e817be3f1b4a89acf91.
//
// Solidity: event NonFungibleTokenRecovery(address indexed token, uint256 indexed tokenId)
func (_Pancakeswap0 *Pancakeswap0Filterer) FilterNonFungibleTokenRecovery(opts *bind.FilterOpts, token []common.Address, tokenId []*big.Int) (*Pancakeswap0NonFungibleTokenRecoveryIterator, error) {

	var tokenRule []interface{}
	for _, tokenItem := range token {
		tokenRule = append(tokenRule, tokenItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Pancakeswap0.contract.FilterLogs(opts, "NonFungibleTokenRecovery", tokenRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0NonFungibleTokenRecoveryIterator{contract: _Pancakeswap0.contract, event: "NonFungibleTokenRecovery", logs: logs, sub: sub}, nil
}

// WatchNonFungibleTokenRecovery is a free log subscription operation binding the contract event 0x861c3ea25dbda3af0bf5d258ba8582c0276c9446b1479e817be3f1b4a89acf91.
//
// Solidity: event NonFungibleTokenRecovery(address indexed token, uint256 indexed tokenId)
func (_Pancakeswap0 *Pancakeswap0Filterer) WatchNonFungibleTokenRecovery(opts *bind.WatchOpts, sink chan<- *Pancakeswap0NonFungibleTokenRecovery, token []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var tokenRule []interface{}
	for _, tokenItem := range token {
		tokenRule = append(tokenRule, tokenItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Pancakeswap0.contract.WatchLogs(opts, "NonFungibleTokenRecovery", tokenRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Pancakeswap0NonFungibleTokenRecovery)
				if err := _Pancakeswap0.contract.UnpackLog(event, "NonFungibleTokenRecovery", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNonFungibleTokenRecovery is a log parse operation binding the contract event 0x861c3ea25dbda3af0bf5d258ba8582c0276c9446b1479e817be3f1b4a89acf91.
//
// Solidity: event NonFungibleTokenRecovery(address indexed token, uint256 indexed tokenId)
func (_Pancakeswap0 *Pancakeswap0Filterer) ParseNonFungibleTokenRecovery(log types.Log) (*Pancakeswap0NonFungibleTokenRecovery, error) {
	event := new(Pancakeswap0NonFungibleTokenRecovery)
	if err := _Pancakeswap0.contract.UnpackLog(event, "NonFungibleTokenRecovery", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Pancakeswap0OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Pancakeswap0 contract.
type Pancakeswap0OwnershipTransferredIterator struct {
	Event *Pancakeswap0OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Pancakeswap0OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Pancakeswap0OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Pancakeswap0OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Pancakeswap0OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Pancakeswap0OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Pancakeswap0OwnershipTransferred represents a OwnershipTransferred event raised by the Pancakeswap0 contract.
type Pancakeswap0OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Pancakeswap0 *Pancakeswap0Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Pancakeswap0OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Pancakeswap0.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0OwnershipTransferredIterator{contract: _Pancakeswap0.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Pancakeswap0 *Pancakeswap0Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Pancakeswap0OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Pancakeswap0.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Pancakeswap0OwnershipTransferred)
				if err := _Pancakeswap0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Pancakeswap0 *Pancakeswap0Filterer) ParseOwnershipTransferred(log types.Log) (*Pancakeswap0OwnershipTransferred, error) {
	event := new(Pancakeswap0OwnershipTransferred)
	if err := _Pancakeswap0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Pancakeswap0RevenueClaimIterator is returned from FilterRevenueClaim and is used to iterate over the raw logs and unpacked data for RevenueClaim events raised by the Pancakeswap0 contract.
type Pancakeswap0RevenueClaimIterator struct {
	Event *Pancakeswap0RevenueClaim // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Pancakeswap0RevenueClaimIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Pancakeswap0RevenueClaim)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Pancakeswap0RevenueClaim)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Pancakeswap0RevenueClaimIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Pancakeswap0RevenueClaimIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Pancakeswap0RevenueClaim represents a RevenueClaim event raised by the Pancakeswap0 contract.
type Pancakeswap0RevenueClaim struct {
	Claimer common.Address
	Amount  *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterRevenueClaim is a free log retrieval operation binding the contract event 0xc6cb6f1a9d5212d4ee3819d9b175a85e8333a7327d53dfbc1b610e1fd8ec3cb5.
//
// Solidity: event RevenueClaim(address indexed claimer, uint256 amount)
func (_Pancakeswap0 *Pancakeswap0Filterer) FilterRevenueClaim(opts *bind.FilterOpts, claimer []common.Address) (*Pancakeswap0RevenueClaimIterator, error) {

	var claimerRule []interface{}
	for _, claimerItem := range claimer {
		claimerRule = append(claimerRule, claimerItem)
	}

	logs, sub, err := _Pancakeswap0.contract.FilterLogs(opts, "RevenueClaim", claimerRule)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0RevenueClaimIterator{contract: _Pancakeswap0.contract, event: "RevenueClaim", logs: logs, sub: sub}, nil
}

// WatchRevenueClaim is a free log subscription operation binding the contract event 0xc6cb6f1a9d5212d4ee3819d9b175a85e8333a7327d53dfbc1b610e1fd8ec3cb5.
//
// Solidity: event RevenueClaim(address indexed claimer, uint256 amount)
func (_Pancakeswap0 *Pancakeswap0Filterer) WatchRevenueClaim(opts *bind.WatchOpts, sink chan<- *Pancakeswap0RevenueClaim, claimer []common.Address) (event.Subscription, error) {

	var claimerRule []interface{}
	for _, claimerItem := range claimer {
		claimerRule = append(claimerRule, claimerItem)
	}

	logs, sub, err := _Pancakeswap0.contract.WatchLogs(opts, "RevenueClaim", claimerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Pancakeswap0RevenueClaim)
				if err := _Pancakeswap0.contract.UnpackLog(event, "RevenueClaim", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseRevenueClaim is a log parse operation binding the contract event 0xc6cb6f1a9d5212d4ee3819d9b175a85e8333a7327d53dfbc1b610e1fd8ec3cb5.
//
// Solidity: event RevenueClaim(address indexed claimer, uint256 amount)
func (_Pancakeswap0 *Pancakeswap0Filterer) ParseRevenueClaim(log types.Log) (*Pancakeswap0RevenueClaim, error) {
	event := new(Pancakeswap0RevenueClaim)
	if err := _Pancakeswap0.contract.UnpackLog(event, "RevenueClaim", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Pancakeswap0TokenRecoveryIterator is returned from FilterTokenRecovery and is used to iterate over the raw logs and unpacked data for TokenRecovery events raised by the Pancakeswap0 contract.
type Pancakeswap0TokenRecoveryIterator struct {
	Event *Pancakeswap0TokenRecovery // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Pancakeswap0TokenRecoveryIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Pancakeswap0TokenRecovery)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Pancakeswap0TokenRecovery)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Pancakeswap0TokenRecoveryIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Pancakeswap0TokenRecoveryIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Pancakeswap0TokenRecovery represents a TokenRecovery event raised by the Pancakeswap0 contract.
type Pancakeswap0TokenRecovery struct {
	Token  common.Address
	Amount *big.Int
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterTokenRecovery is a free log retrieval operation binding the contract event 0x14f11966a996e0629572e51064726d2057a80fbd34efc066682c06a71dbb6e98.
//
// Solidity: event TokenRecovery(address indexed token, uint256 amount)
func (_Pancakeswap0 *Pancakeswap0Filterer) FilterTokenRecovery(opts *bind.FilterOpts, token []common.Address) (*Pancakeswap0TokenRecoveryIterator, error) {

	var tokenRule []interface{}
	for _, tokenItem := range token {
		tokenRule = append(tokenRule, tokenItem)
	}

	logs, sub, err := _Pancakeswap0.contract.FilterLogs(opts, "TokenRecovery", tokenRule)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0TokenRecoveryIterator{contract: _Pancakeswap0.contract, event: "TokenRecovery", logs: logs, sub: sub}, nil
}

// WatchTokenRecovery is a free log subscription operation binding the contract event 0x14f11966a996e0629572e51064726d2057a80fbd34efc066682c06a71dbb6e98.
//
// Solidity: event TokenRecovery(address indexed token, uint256 amount)
func (_Pancakeswap0 *Pancakeswap0Filterer) WatchTokenRecovery(opts *bind.WatchOpts, sink chan<- *Pancakeswap0TokenRecovery, token []common.Address) (event.Subscription, error) {

	var tokenRule []interface{}
	for _, tokenItem := range token {
		tokenRule = append(tokenRule, tokenItem)
	}

	logs, sub, err := _Pancakeswap0.contract.WatchLogs(opts, "TokenRecovery", tokenRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Pancakeswap0TokenRecovery)
				if err := _Pancakeswap0.contract.UnpackLog(event, "TokenRecovery", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokenRecovery is a log parse operation binding the contract event 0x14f11966a996e0629572e51064726d2057a80fbd34efc066682c06a71dbb6e98.
//
// Solidity: event TokenRecovery(address indexed token, uint256 amount)
func (_Pancakeswap0 *Pancakeswap0Filterer) ParseTokenRecovery(log types.Log) (*Pancakeswap0TokenRecovery, error) {
	event := new(Pancakeswap0TokenRecovery)
	if err := _Pancakeswap0.contract.UnpackLog(event, "TokenRecovery", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Pancakeswap0TradeIterator is returned from FilterTrade and is used to iterate over the raw logs and unpacked data for Trade events raised by the Pancakeswap0 contract.
type Pancakeswap0TradeIterator struct {
	Event *Pancakeswap0Trade // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Pancakeswap0TradeIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Pancakeswap0Trade)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Pancakeswap0Trade)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Pancakeswap0TradeIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Pancakeswap0TradeIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Pancakeswap0Trade represents a Trade event raised by the Pancakeswap0 contract.
type Pancakeswap0Trade struct {
	Collection common.Address
	TokenId    *big.Int
	Seller     common.Address
	Buyer      common.Address
	AskPrice   *big.Int
	NetPrice   *big.Int
	WithBNB    bool
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterTrade is a free log retrieval operation binding the contract event 0xdaac0e40b8f01e970d08d4e8ae57ac31a5845fffde104c43f05e19bbec78491e.
//
// Solidity: event Trade(address indexed collection, uint256 indexed tokenId, address indexed seller, address buyer, uint256 askPrice, uint256 netPrice, bool withBNB)
func (_Pancakeswap0 *Pancakeswap0Filterer) FilterTrade(opts *bind.FilterOpts, collection []common.Address, tokenId []*big.Int, seller []common.Address) (*Pancakeswap0TradeIterator, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}
	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	logs, sub, err := _Pancakeswap0.contract.FilterLogs(opts, "Trade", collectionRule, tokenIdRule, sellerRule)
	if err != nil {
		return nil, err
	}
	return &Pancakeswap0TradeIterator{contract: _Pancakeswap0.contract, event: "Trade", logs: logs, sub: sub}, nil
}

// WatchTrade is a free log subscription operation binding the contract event 0xdaac0e40b8f01e970d08d4e8ae57ac31a5845fffde104c43f05e19bbec78491e.
//
// Solidity: event Trade(address indexed collection, uint256 indexed tokenId, address indexed seller, address buyer, uint256 askPrice, uint256 netPrice, bool withBNB)
func (_Pancakeswap0 *Pancakeswap0Filterer) WatchTrade(opts *bind.WatchOpts, sink chan<- *Pancakeswap0Trade, collection []common.Address, tokenId []*big.Int, seller []common.Address) (event.Subscription, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}
	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	logs, sub, err := _Pancakeswap0.contract.WatchLogs(opts, "Trade", collectionRule, tokenIdRule, sellerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Pancakeswap0Trade)
				if err := _Pancakeswap0.contract.UnpackLog(event, "Trade", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTrade is a log parse operation binding the contract event 0xdaac0e40b8f01e970d08d4e8ae57ac31a5845fffde104c43f05e19bbec78491e.
//
// Solidity: event Trade(address indexed collection, uint256 indexed tokenId, address indexed seller, address buyer, uint256 askPrice, uint256 netPrice, bool withBNB)
func (_Pancakeswap0 *Pancakeswap0Filterer) ParseTrade(log types.Log) (*Pancakeswap0Trade, error) {
	event := new(Pancakeswap0Trade)
	if err := _Pancakeswap0.contract.UnpackLog(event, "Trade", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
