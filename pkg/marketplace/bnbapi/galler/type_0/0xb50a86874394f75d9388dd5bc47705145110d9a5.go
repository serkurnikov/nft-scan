// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package galler_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Galler0MetaData contains all meta data concerning the Galler0 contract.
var Galler0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"chainId\",\"type\":\"uint256\"},{\"internalType\":\"address[]\",\"name\":\"registryAddrs\",\"type\":\"address[]\"},{\"internalType\":\"bytes\",\"name\":\"customPersonalSignPrefix\",\"type\":\"bytes\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"bytes32\",\"name\":\"hash\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"registry\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"maker\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"staticTarget\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"bytes4\",\"name\":\"staticSelector\",\"type\":\"bytes4\"},{\"indexed\":false,\"internalType\":\"bytes\",\"name\":\"staticExtradata\",\"type\":\"bytes\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"maximumFill\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"listingTime\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"expirationTime\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"orderbookInclusionDesired\",\"type\":\"bool\"}],\"name\":\"OrderApproved\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"bytes32\",\"name\":\"hash\",\"type\":\"bytes32\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"maker\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"newFill\",\"type\":\"uint256\"}],\"name\":\"OrderFillChanged\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"firstHash\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"secondHash\",\"type\":\"bytes32\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"firstMaker\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"secondMaker\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"newFirstFill\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"newSecondFill\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"bytes32\",\"name\":\"metadata\",\"type\":\"bytes32\"}],\"name\":\"OrdersMatched\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"hash\",\"type\":\"bytes32\"}],\"name\":\"approveOrderHash_\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"registry\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"maker\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"staticTarget\",\"type\":\"address\"},{\"internalType\":\"bytes4\",\"name\":\"staticSelector\",\"type\":\"bytes4\"},{\"internalType\":\"bytes\",\"name\":\"staticExtradata\",\"type\":\"bytes\"},{\"internalType\":\"uint256\",\"name\":\"maximumFill\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"listingTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"expirationTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"orderbookInclusionDesired\",\"type\":\"bool\"}],\"name\":\"approveOrder_\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"name\":\"approved\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256[16]\",\"name\":\"uints\",\"type\":\"uint256[16]\"},{\"internalType\":\"bytes4[2]\",\"name\":\"staticSelectors\",\"type\":\"bytes4[2]\"},{\"internalType\":\"bytes\",\"name\":\"firstExtradata\",\"type\":\"bytes\"},{\"internalType\":\"bytes\",\"name\":\"firstCalldata\",\"type\":\"bytes\"},{\"internalType\":\"bytes\",\"name\":\"secondExtradata\",\"type\":\"bytes\"},{\"internalType\":\"bytes\",\"name\":\"secondCalldata\",\"type\":\"bytes\"},{\"internalType\":\"uint8[2]\",\"name\":\"howToCalls\",\"type\":\"uint8[2]\"},{\"internalType\":\"bytes32\",\"name\":\"metadata\",\"type\":\"bytes32\"},{\"internalType\":\"bytes\",\"name\":\"signatures\",\"type\":\"bytes\"}],\"name\":\"atomicMatch_\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"codename\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"name\":\"fills\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"registry\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"maker\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"staticTarget\",\"type\":\"address\"},{\"internalType\":\"bytes4\",\"name\":\"staticSelector\",\"type\":\"bytes4\"},{\"internalType\":\"bytes\",\"name\":\"staticExtradata\",\"type\":\"bytes\"},{\"internalType\":\"uint256\",\"name\":\"maximumFill\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"listingTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"expirationTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"}],\"name\":\"hashOrder_\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"hash\",\"type\":\"bytes32\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"orderHash\",\"type\":\"bytes32\"}],\"name\":\"hashToSign_\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"hash\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"registries\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"hash\",\"type\":\"bytes32\"},{\"internalType\":\"uint256\",\"name\":\"fill\",\"type\":\"uint256\"}],\"name\":\"setOrderFill_\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"hash\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"maker\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"signature\",\"type\":\"bytes\"}],\"name\":\"validateOrderAuthorization_\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"registry\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"maker\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"staticTarget\",\"type\":\"address\"},{\"internalType\":\"bytes4\",\"name\":\"staticSelector\",\"type\":\"bytes4\"},{\"internalType\":\"bytes\",\"name\":\"staticExtradata\",\"type\":\"bytes\"},{\"internalType\":\"uint256\",\"name\":\"maximumFill\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"listingTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"expirationTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"}],\"name\":\"validateOrderParameters_\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"version\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
}

// Galler0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Galler0MetaData.ABI instead.
var Galler0ABI = Galler0MetaData.ABI

// Galler0 is an auto generated Go binding around an Ethereum contract.
type Galler0 struct {
	Galler0Caller     // Read-only binding to the contract
	Galler0Transactor // Write-only binding to the contract
	Galler0Filterer   // Log filterer for contract events
}

// Galler0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Galler0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Galler0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Galler0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Galler0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Galler0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Galler0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Galler0Session struct {
	Contract     *Galler0          // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Galler0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Galler0CallerSession struct {
	Contract *Galler0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts  // Call options to use throughout this session
}

// Galler0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Galler0TransactorSession struct {
	Contract     *Galler0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts  // Transaction auth options to use throughout this session
}

// Galler0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Galler0Raw struct {
	Contract *Galler0 // Generic contract binding to access the raw methods on
}

// Galler0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Galler0CallerRaw struct {
	Contract *Galler0Caller // Generic read-only contract binding to access the raw methods on
}

// Galler0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Galler0TransactorRaw struct {
	Contract *Galler0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewGaller0 creates a new instance of Galler0, bound to a specific deployed contract.
func NewGaller0(address common.Address, backend bind.ContractBackend) (*Galler0, error) {
	contract, err := bindGaller0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Galler0{Galler0Caller: Galler0Caller{contract: contract}, Galler0Transactor: Galler0Transactor{contract: contract}, Galler0Filterer: Galler0Filterer{contract: contract}}, nil
}

// NewGaller0Caller creates a new read-only instance of Galler0, bound to a specific deployed contract.
func NewGaller0Caller(address common.Address, caller bind.ContractCaller) (*Galler0Caller, error) {
	contract, err := bindGaller0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Galler0Caller{contract: contract}, nil
}

// NewGaller0Transactor creates a new write-only instance of Galler0, bound to a specific deployed contract.
func NewGaller0Transactor(address common.Address, transactor bind.ContractTransactor) (*Galler0Transactor, error) {
	contract, err := bindGaller0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Galler0Transactor{contract: contract}, nil
}

// NewGaller0Filterer creates a new log filterer instance of Galler0, bound to a specific deployed contract.
func NewGaller0Filterer(address common.Address, filterer bind.ContractFilterer) (*Galler0Filterer, error) {
	contract, err := bindGaller0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Galler0Filterer{contract: contract}, nil
}

// bindGaller0 binds a generic wrapper to an already deployed contract.
func bindGaller0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Galler0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Galler0 *Galler0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Galler0.Contract.Galler0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Galler0 *Galler0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Galler0.Contract.Galler0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Galler0 *Galler0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Galler0.Contract.Galler0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Galler0 *Galler0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Galler0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Galler0 *Galler0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Galler0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Galler0 *Galler0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Galler0.Contract.contract.Transact(opts, method, params...)
}

// Approved is a free data retrieval call binding the contract method 0xfac7fc38.
//
// Solidity: function approved(address , bytes32 ) view returns(bool)
func (_Galler0 *Galler0Caller) Approved(opts *bind.CallOpts, arg0 common.Address, arg1 [32]byte) (bool, error) {
	var out []interface{}
	err := _Galler0.contract.Call(opts, &out, "approved", arg0, arg1)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Approved is a free data retrieval call binding the contract method 0xfac7fc38.
//
// Solidity: function approved(address , bytes32 ) view returns(bool)
func (_Galler0 *Galler0Session) Approved(arg0 common.Address, arg1 [32]byte) (bool, error) {
	return _Galler0.Contract.Approved(&_Galler0.CallOpts, arg0, arg1)
}

// Approved is a free data retrieval call binding the contract method 0xfac7fc38.
//
// Solidity: function approved(address , bytes32 ) view returns(bool)
func (_Galler0 *Galler0CallerSession) Approved(arg0 common.Address, arg1 [32]byte) (bool, error) {
	return _Galler0.Contract.Approved(&_Galler0.CallOpts, arg0, arg1)
}

// Codename is a free data retrieval call binding the contract method 0x31e63199.
//
// Solidity: function codename() view returns(string)
func (_Galler0 *Galler0Caller) Codename(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Galler0.contract.Call(opts, &out, "codename")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Codename is a free data retrieval call binding the contract method 0x31e63199.
//
// Solidity: function codename() view returns(string)
func (_Galler0 *Galler0Session) Codename() (string, error) {
	return _Galler0.Contract.Codename(&_Galler0.CallOpts)
}

// Codename is a free data retrieval call binding the contract method 0x31e63199.
//
// Solidity: function codename() view returns(string)
func (_Galler0 *Galler0CallerSession) Codename() (string, error) {
	return _Galler0.Contract.Codename(&_Galler0.CallOpts)
}

// Fills is a free data retrieval call binding the contract method 0xffc2ab14.
//
// Solidity: function fills(address , bytes32 ) view returns(uint256)
func (_Galler0 *Galler0Caller) Fills(opts *bind.CallOpts, arg0 common.Address, arg1 [32]byte) (*big.Int, error) {
	var out []interface{}
	err := _Galler0.contract.Call(opts, &out, "fills", arg0, arg1)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Fills is a free data retrieval call binding the contract method 0xffc2ab14.
//
// Solidity: function fills(address , bytes32 ) view returns(uint256)
func (_Galler0 *Galler0Session) Fills(arg0 common.Address, arg1 [32]byte) (*big.Int, error) {
	return _Galler0.Contract.Fills(&_Galler0.CallOpts, arg0, arg1)
}

// Fills is a free data retrieval call binding the contract method 0xffc2ab14.
//
// Solidity: function fills(address , bytes32 ) view returns(uint256)
func (_Galler0 *Galler0CallerSession) Fills(arg0 common.Address, arg1 [32]byte) (*big.Int, error) {
	return _Galler0.Contract.Fills(&_Galler0.CallOpts, arg0, arg1)
}

// HashOrder is a free data retrieval call binding the contract method 0xf861975b.
//
// Solidity: function hashOrder_(address registry, address maker, address staticTarget, bytes4 staticSelector, bytes staticExtradata, uint256 maximumFill, uint256 listingTime, uint256 expirationTime, uint256 salt) pure returns(bytes32 hash)
func (_Galler0 *Galler0Caller) HashOrder(opts *bind.CallOpts, registry common.Address, maker common.Address, staticTarget common.Address, staticSelector [4]byte, staticExtradata []byte, maximumFill *big.Int, listingTime *big.Int, expirationTime *big.Int, salt *big.Int) ([32]byte, error) {
	var out []interface{}
	err := _Galler0.contract.Call(opts, &out, "hashOrder_", registry, maker, staticTarget, staticSelector, staticExtradata, maximumFill, listingTime, expirationTime, salt)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// HashOrder is a free data retrieval call binding the contract method 0xf861975b.
//
// Solidity: function hashOrder_(address registry, address maker, address staticTarget, bytes4 staticSelector, bytes staticExtradata, uint256 maximumFill, uint256 listingTime, uint256 expirationTime, uint256 salt) pure returns(bytes32 hash)
func (_Galler0 *Galler0Session) HashOrder(registry common.Address, maker common.Address, staticTarget common.Address, staticSelector [4]byte, staticExtradata []byte, maximumFill *big.Int, listingTime *big.Int, expirationTime *big.Int, salt *big.Int) ([32]byte, error) {
	return _Galler0.Contract.HashOrder(&_Galler0.CallOpts, registry, maker, staticTarget, staticSelector, staticExtradata, maximumFill, listingTime, expirationTime, salt)
}

// HashOrder is a free data retrieval call binding the contract method 0xf861975b.
//
// Solidity: function hashOrder_(address registry, address maker, address staticTarget, bytes4 staticSelector, bytes staticExtradata, uint256 maximumFill, uint256 listingTime, uint256 expirationTime, uint256 salt) pure returns(bytes32 hash)
func (_Galler0 *Galler0CallerSession) HashOrder(registry common.Address, maker common.Address, staticTarget common.Address, staticSelector [4]byte, staticExtradata []byte, maximumFill *big.Int, listingTime *big.Int, expirationTime *big.Int, salt *big.Int) ([32]byte, error) {
	return _Galler0.Contract.HashOrder(&_Galler0.CallOpts, registry, maker, staticTarget, staticSelector, staticExtradata, maximumFill, listingTime, expirationTime, salt)
}

// HashToSign is a free data retrieval call binding the contract method 0xd57ad588.
//
// Solidity: function hashToSign_(bytes32 orderHash) view returns(bytes32 hash)
func (_Galler0 *Galler0Caller) HashToSign(opts *bind.CallOpts, orderHash [32]byte) ([32]byte, error) {
	var out []interface{}
	err := _Galler0.contract.Call(opts, &out, "hashToSign_", orderHash)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// HashToSign is a free data retrieval call binding the contract method 0xd57ad588.
//
// Solidity: function hashToSign_(bytes32 orderHash) view returns(bytes32 hash)
func (_Galler0 *Galler0Session) HashToSign(orderHash [32]byte) ([32]byte, error) {
	return _Galler0.Contract.HashToSign(&_Galler0.CallOpts, orderHash)
}

// HashToSign is a free data retrieval call binding the contract method 0xd57ad588.
//
// Solidity: function hashToSign_(bytes32 orderHash) view returns(bytes32 hash)
func (_Galler0 *Galler0CallerSession) HashToSign(orderHash [32]byte) ([32]byte, error) {
	return _Galler0.Contract.HashToSign(&_Galler0.CallOpts, orderHash)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Galler0 *Galler0Caller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Galler0.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Galler0 *Galler0Session) Name() (string, error) {
	return _Galler0.Contract.Name(&_Galler0.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Galler0 *Galler0CallerSession) Name() (string, error) {
	return _Galler0.Contract.Name(&_Galler0.CallOpts)
}

// Registries is a free data retrieval call binding the contract method 0xcaed80df.
//
// Solidity: function registries(address ) view returns(bool)
func (_Galler0 *Galler0Caller) Registries(opts *bind.CallOpts, arg0 common.Address) (bool, error) {
	var out []interface{}
	err := _Galler0.contract.Call(opts, &out, "registries", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Registries is a free data retrieval call binding the contract method 0xcaed80df.
//
// Solidity: function registries(address ) view returns(bool)
func (_Galler0 *Galler0Session) Registries(arg0 common.Address) (bool, error) {
	return _Galler0.Contract.Registries(&_Galler0.CallOpts, arg0)
}

// Registries is a free data retrieval call binding the contract method 0xcaed80df.
//
// Solidity: function registries(address ) view returns(bool)
func (_Galler0 *Galler0CallerSession) Registries(arg0 common.Address) (bool, error) {
	return _Galler0.Contract.Registries(&_Galler0.CallOpts, arg0)
}

// ValidateOrderAuthorization is a free data retrieval call binding the contract method 0x13b219e2.
//
// Solidity: function validateOrderAuthorization_(bytes32 hash, address maker, bytes signature) view returns(bool)
func (_Galler0 *Galler0Caller) ValidateOrderAuthorization(opts *bind.CallOpts, hash [32]byte, maker common.Address, signature []byte) (bool, error) {
	var out []interface{}
	err := _Galler0.contract.Call(opts, &out, "validateOrderAuthorization_", hash, maker, signature)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// ValidateOrderAuthorization is a free data retrieval call binding the contract method 0x13b219e2.
//
// Solidity: function validateOrderAuthorization_(bytes32 hash, address maker, bytes signature) view returns(bool)
func (_Galler0 *Galler0Session) ValidateOrderAuthorization(hash [32]byte, maker common.Address, signature []byte) (bool, error) {
	return _Galler0.Contract.ValidateOrderAuthorization(&_Galler0.CallOpts, hash, maker, signature)
}

// ValidateOrderAuthorization is a free data retrieval call binding the contract method 0x13b219e2.
//
// Solidity: function validateOrderAuthorization_(bytes32 hash, address maker, bytes signature) view returns(bool)
func (_Galler0 *Galler0CallerSession) ValidateOrderAuthorization(hash [32]byte, maker common.Address, signature []byte) (bool, error) {
	return _Galler0.Contract.ValidateOrderAuthorization(&_Galler0.CallOpts, hash, maker, signature)
}

// ValidateOrderParameters is a free data retrieval call binding the contract method 0xf9b0bc20.
//
// Solidity: function validateOrderParameters_(address registry, address maker, address staticTarget, bytes4 staticSelector, bytes staticExtradata, uint256 maximumFill, uint256 listingTime, uint256 expirationTime, uint256 salt) view returns(bool)
func (_Galler0 *Galler0Caller) ValidateOrderParameters(opts *bind.CallOpts, registry common.Address, maker common.Address, staticTarget common.Address, staticSelector [4]byte, staticExtradata []byte, maximumFill *big.Int, listingTime *big.Int, expirationTime *big.Int, salt *big.Int) (bool, error) {
	var out []interface{}
	err := _Galler0.contract.Call(opts, &out, "validateOrderParameters_", registry, maker, staticTarget, staticSelector, staticExtradata, maximumFill, listingTime, expirationTime, salt)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// ValidateOrderParameters is a free data retrieval call binding the contract method 0xf9b0bc20.
//
// Solidity: function validateOrderParameters_(address registry, address maker, address staticTarget, bytes4 staticSelector, bytes staticExtradata, uint256 maximumFill, uint256 listingTime, uint256 expirationTime, uint256 salt) view returns(bool)
func (_Galler0 *Galler0Session) ValidateOrderParameters(registry common.Address, maker common.Address, staticTarget common.Address, staticSelector [4]byte, staticExtradata []byte, maximumFill *big.Int, listingTime *big.Int, expirationTime *big.Int, salt *big.Int) (bool, error) {
	return _Galler0.Contract.ValidateOrderParameters(&_Galler0.CallOpts, registry, maker, staticTarget, staticSelector, staticExtradata, maximumFill, listingTime, expirationTime, salt)
}

// ValidateOrderParameters is a free data retrieval call binding the contract method 0xf9b0bc20.
//
// Solidity: function validateOrderParameters_(address registry, address maker, address staticTarget, bytes4 staticSelector, bytes staticExtradata, uint256 maximumFill, uint256 listingTime, uint256 expirationTime, uint256 salt) view returns(bool)
func (_Galler0 *Galler0CallerSession) ValidateOrderParameters(registry common.Address, maker common.Address, staticTarget common.Address, staticSelector [4]byte, staticExtradata []byte, maximumFill *big.Int, listingTime *big.Int, expirationTime *big.Int, salt *big.Int) (bool, error) {
	return _Galler0.Contract.ValidateOrderParameters(&_Galler0.CallOpts, registry, maker, staticTarget, staticSelector, staticExtradata, maximumFill, listingTime, expirationTime, salt)
}

// Version is a free data retrieval call binding the contract method 0x54fd4d50.
//
// Solidity: function version() view returns(string)
func (_Galler0 *Galler0Caller) Version(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Galler0.contract.Call(opts, &out, "version")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Version is a free data retrieval call binding the contract method 0x54fd4d50.
//
// Solidity: function version() view returns(string)
func (_Galler0 *Galler0Session) Version() (string, error) {
	return _Galler0.Contract.Version(&_Galler0.CallOpts)
}

// Version is a free data retrieval call binding the contract method 0x54fd4d50.
//
// Solidity: function version() view returns(string)
func (_Galler0 *Galler0CallerSession) Version() (string, error) {
	return _Galler0.Contract.Version(&_Galler0.CallOpts)
}

// ApproveOrderHash is a paid mutator transaction binding the contract method 0x0812226e.
//
// Solidity: function approveOrderHash_(bytes32 hash) returns()
func (_Galler0 *Galler0Transactor) ApproveOrderHash(opts *bind.TransactOpts, hash [32]byte) (*types.Transaction, error) {
	return _Galler0.contract.Transact(opts, "approveOrderHash_", hash)
}

// ApproveOrderHash is a paid mutator transaction binding the contract method 0x0812226e.
//
// Solidity: function approveOrderHash_(bytes32 hash) returns()
func (_Galler0 *Galler0Session) ApproveOrderHash(hash [32]byte) (*types.Transaction, error) {
	return _Galler0.Contract.ApproveOrderHash(&_Galler0.TransactOpts, hash)
}

// ApproveOrderHash is a paid mutator transaction binding the contract method 0x0812226e.
//
// Solidity: function approveOrderHash_(bytes32 hash) returns()
func (_Galler0 *Galler0TransactorSession) ApproveOrderHash(hash [32]byte) (*types.Transaction, error) {
	return _Galler0.Contract.ApproveOrderHash(&_Galler0.TransactOpts, hash)
}

// ApproveOrder is a paid mutator transaction binding the contract method 0x995e8195.
//
// Solidity: function approveOrder_(address registry, address maker, address staticTarget, bytes4 staticSelector, bytes staticExtradata, uint256 maximumFill, uint256 listingTime, uint256 expirationTime, uint256 salt, bool orderbookInclusionDesired) returns()
func (_Galler0 *Galler0Transactor) ApproveOrder(opts *bind.TransactOpts, registry common.Address, maker common.Address, staticTarget common.Address, staticSelector [4]byte, staticExtradata []byte, maximumFill *big.Int, listingTime *big.Int, expirationTime *big.Int, salt *big.Int, orderbookInclusionDesired bool) (*types.Transaction, error) {
	return _Galler0.contract.Transact(opts, "approveOrder_", registry, maker, staticTarget, staticSelector, staticExtradata, maximumFill, listingTime, expirationTime, salt, orderbookInclusionDesired)
}

// ApproveOrder is a paid mutator transaction binding the contract method 0x995e8195.
//
// Solidity: function approveOrder_(address registry, address maker, address staticTarget, bytes4 staticSelector, bytes staticExtradata, uint256 maximumFill, uint256 listingTime, uint256 expirationTime, uint256 salt, bool orderbookInclusionDesired) returns()
func (_Galler0 *Galler0Session) ApproveOrder(registry common.Address, maker common.Address, staticTarget common.Address, staticSelector [4]byte, staticExtradata []byte, maximumFill *big.Int, listingTime *big.Int, expirationTime *big.Int, salt *big.Int, orderbookInclusionDesired bool) (*types.Transaction, error) {
	return _Galler0.Contract.ApproveOrder(&_Galler0.TransactOpts, registry, maker, staticTarget, staticSelector, staticExtradata, maximumFill, listingTime, expirationTime, salt, orderbookInclusionDesired)
}

// ApproveOrder is a paid mutator transaction binding the contract method 0x995e8195.
//
// Solidity: function approveOrder_(address registry, address maker, address staticTarget, bytes4 staticSelector, bytes staticExtradata, uint256 maximumFill, uint256 listingTime, uint256 expirationTime, uint256 salt, bool orderbookInclusionDesired) returns()
func (_Galler0 *Galler0TransactorSession) ApproveOrder(registry common.Address, maker common.Address, staticTarget common.Address, staticSelector [4]byte, staticExtradata []byte, maximumFill *big.Int, listingTime *big.Int, expirationTime *big.Int, salt *big.Int, orderbookInclusionDesired bool) (*types.Transaction, error) {
	return _Galler0.Contract.ApproveOrder(&_Galler0.TransactOpts, registry, maker, staticTarget, staticSelector, staticExtradata, maximumFill, listingTime, expirationTime, salt, orderbookInclusionDesired)
}

// AtomicMatch is a paid mutator transaction binding the contract method 0x9a5168db.
//
// Solidity: function atomicMatch_(uint256[16] uints, bytes4[2] staticSelectors, bytes firstExtradata, bytes firstCalldata, bytes secondExtradata, bytes secondCalldata, uint8[2] howToCalls, bytes32 metadata, bytes signatures) payable returns()
func (_Galler0 *Galler0Transactor) AtomicMatch(opts *bind.TransactOpts, uints [16]*big.Int, staticSelectors [2][4]byte, firstExtradata []byte, firstCalldata []byte, secondExtradata []byte, secondCalldata []byte, howToCalls [2]uint8, metadata [32]byte, signatures []byte) (*types.Transaction, error) {
	return _Galler0.contract.Transact(opts, "atomicMatch_", uints, staticSelectors, firstExtradata, firstCalldata, secondExtradata, secondCalldata, howToCalls, metadata, signatures)
}

// AtomicMatch is a paid mutator transaction binding the contract method 0x9a5168db.
//
// Solidity: function atomicMatch_(uint256[16] uints, bytes4[2] staticSelectors, bytes firstExtradata, bytes firstCalldata, bytes secondExtradata, bytes secondCalldata, uint8[2] howToCalls, bytes32 metadata, bytes signatures) payable returns()
func (_Galler0 *Galler0Session) AtomicMatch(uints [16]*big.Int, staticSelectors [2][4]byte, firstExtradata []byte, firstCalldata []byte, secondExtradata []byte, secondCalldata []byte, howToCalls [2]uint8, metadata [32]byte, signatures []byte) (*types.Transaction, error) {
	return _Galler0.Contract.AtomicMatch(&_Galler0.TransactOpts, uints, staticSelectors, firstExtradata, firstCalldata, secondExtradata, secondCalldata, howToCalls, metadata, signatures)
}

// AtomicMatch is a paid mutator transaction binding the contract method 0x9a5168db.
//
// Solidity: function atomicMatch_(uint256[16] uints, bytes4[2] staticSelectors, bytes firstExtradata, bytes firstCalldata, bytes secondExtradata, bytes secondCalldata, uint8[2] howToCalls, bytes32 metadata, bytes signatures) payable returns()
func (_Galler0 *Galler0TransactorSession) AtomicMatch(uints [16]*big.Int, staticSelectors [2][4]byte, firstExtradata []byte, firstCalldata []byte, secondExtradata []byte, secondCalldata []byte, howToCalls [2]uint8, metadata [32]byte, signatures []byte) (*types.Transaction, error) {
	return _Galler0.Contract.AtomicMatch(&_Galler0.TransactOpts, uints, staticSelectors, firstExtradata, firstCalldata, secondExtradata, secondCalldata, howToCalls, metadata, signatures)
}

// SetOrderFill is a paid mutator transaction binding the contract method 0x14127f13.
//
// Solidity: function setOrderFill_(bytes32 hash, uint256 fill) returns()
func (_Galler0 *Galler0Transactor) SetOrderFill(opts *bind.TransactOpts, hash [32]byte, fill *big.Int) (*types.Transaction, error) {
	return _Galler0.contract.Transact(opts, "setOrderFill_", hash, fill)
}

// SetOrderFill is a paid mutator transaction binding the contract method 0x14127f13.
//
// Solidity: function setOrderFill_(bytes32 hash, uint256 fill) returns()
func (_Galler0 *Galler0Session) SetOrderFill(hash [32]byte, fill *big.Int) (*types.Transaction, error) {
	return _Galler0.Contract.SetOrderFill(&_Galler0.TransactOpts, hash, fill)
}

// SetOrderFill is a paid mutator transaction binding the contract method 0x14127f13.
//
// Solidity: function setOrderFill_(bytes32 hash, uint256 fill) returns()
func (_Galler0 *Galler0TransactorSession) SetOrderFill(hash [32]byte, fill *big.Int) (*types.Transaction, error) {
	return _Galler0.Contract.SetOrderFill(&_Galler0.TransactOpts, hash, fill)
}

// Galler0OrderApprovedIterator is returned from FilterOrderApproved and is used to iterate over the raw logs and unpacked data for OrderApproved events raised by the Galler0 contract.
type Galler0OrderApprovedIterator struct {
	Event *Galler0OrderApproved // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Galler0OrderApprovedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Galler0OrderApproved)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Galler0OrderApproved)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Galler0OrderApprovedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Galler0OrderApprovedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Galler0OrderApproved represents a OrderApproved event raised by the Galler0 contract.
type Galler0OrderApproved struct {
	Hash                      [32]byte
	Registry                  common.Address
	Maker                     common.Address
	StaticTarget              common.Address
	StaticSelector            [4]byte
	StaticExtradata           []byte
	MaximumFill               *big.Int
	ListingTime               *big.Int
	ExpirationTime            *big.Int
	Salt                      *big.Int
	OrderbookInclusionDesired bool
	Raw                       types.Log // Blockchain specific contextual infos
}

// FilterOrderApproved is a free log retrieval operation binding the contract event 0xbace5d282ba59cba3de1ac539b0ed46ae5a3d8ad434a3ac9f07ce45d16614818.
//
// Solidity: event OrderApproved(bytes32 indexed hash, address registry, address indexed maker, address staticTarget, bytes4 staticSelector, bytes staticExtradata, uint256 maximumFill, uint256 listingTime, uint256 expirationTime, uint256 salt, bool orderbookInclusionDesired)
func (_Galler0 *Galler0Filterer) FilterOrderApproved(opts *bind.FilterOpts, hash [][32]byte, maker []common.Address) (*Galler0OrderApprovedIterator, error) {

	var hashRule []interface{}
	for _, hashItem := range hash {
		hashRule = append(hashRule, hashItem)
	}

	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}

	logs, sub, err := _Galler0.contract.FilterLogs(opts, "OrderApproved", hashRule, makerRule)
	if err != nil {
		return nil, err
	}
	return &Galler0OrderApprovedIterator{contract: _Galler0.contract, event: "OrderApproved", logs: logs, sub: sub}, nil
}

// WatchOrderApproved is a free log subscription operation binding the contract event 0xbace5d282ba59cba3de1ac539b0ed46ae5a3d8ad434a3ac9f07ce45d16614818.
//
// Solidity: event OrderApproved(bytes32 indexed hash, address registry, address indexed maker, address staticTarget, bytes4 staticSelector, bytes staticExtradata, uint256 maximumFill, uint256 listingTime, uint256 expirationTime, uint256 salt, bool orderbookInclusionDesired)
func (_Galler0 *Galler0Filterer) WatchOrderApproved(opts *bind.WatchOpts, sink chan<- *Galler0OrderApproved, hash [][32]byte, maker []common.Address) (event.Subscription, error) {

	var hashRule []interface{}
	for _, hashItem := range hash {
		hashRule = append(hashRule, hashItem)
	}

	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}

	logs, sub, err := _Galler0.contract.WatchLogs(opts, "OrderApproved", hashRule, makerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Galler0OrderApproved)
				if err := _Galler0.contract.UnpackLog(event, "OrderApproved", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOrderApproved is a log parse operation binding the contract event 0xbace5d282ba59cba3de1ac539b0ed46ae5a3d8ad434a3ac9f07ce45d16614818.
//
// Solidity: event OrderApproved(bytes32 indexed hash, address registry, address indexed maker, address staticTarget, bytes4 staticSelector, bytes staticExtradata, uint256 maximumFill, uint256 listingTime, uint256 expirationTime, uint256 salt, bool orderbookInclusionDesired)
func (_Galler0 *Galler0Filterer) ParseOrderApproved(log types.Log) (*Galler0OrderApproved, error) {
	event := new(Galler0OrderApproved)
	if err := _Galler0.contract.UnpackLog(event, "OrderApproved", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Galler0OrderFillChangedIterator is returned from FilterOrderFillChanged and is used to iterate over the raw logs and unpacked data for OrderFillChanged events raised by the Galler0 contract.
type Galler0OrderFillChangedIterator struct {
	Event *Galler0OrderFillChanged // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Galler0OrderFillChangedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Galler0OrderFillChanged)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Galler0OrderFillChanged)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Galler0OrderFillChangedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Galler0OrderFillChangedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Galler0OrderFillChanged represents a OrderFillChanged event raised by the Galler0 contract.
type Galler0OrderFillChanged struct {
	Hash    [32]byte
	Maker   common.Address
	NewFill *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterOrderFillChanged is a free log retrieval operation binding the contract event 0x826cd05b52546b47590739469989cacd5336e0a9e6f9392de2dee5cfb19c8aab.
//
// Solidity: event OrderFillChanged(bytes32 indexed hash, address indexed maker, uint256 newFill)
func (_Galler0 *Galler0Filterer) FilterOrderFillChanged(opts *bind.FilterOpts, hash [][32]byte, maker []common.Address) (*Galler0OrderFillChangedIterator, error) {

	var hashRule []interface{}
	for _, hashItem := range hash {
		hashRule = append(hashRule, hashItem)
	}
	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}

	logs, sub, err := _Galler0.contract.FilterLogs(opts, "OrderFillChanged", hashRule, makerRule)
	if err != nil {
		return nil, err
	}
	return &Galler0OrderFillChangedIterator{contract: _Galler0.contract, event: "OrderFillChanged", logs: logs, sub: sub}, nil
}

// WatchOrderFillChanged is a free log subscription operation binding the contract event 0x826cd05b52546b47590739469989cacd5336e0a9e6f9392de2dee5cfb19c8aab.
//
// Solidity: event OrderFillChanged(bytes32 indexed hash, address indexed maker, uint256 newFill)
func (_Galler0 *Galler0Filterer) WatchOrderFillChanged(opts *bind.WatchOpts, sink chan<- *Galler0OrderFillChanged, hash [][32]byte, maker []common.Address) (event.Subscription, error) {

	var hashRule []interface{}
	for _, hashItem := range hash {
		hashRule = append(hashRule, hashItem)
	}
	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}

	logs, sub, err := _Galler0.contract.WatchLogs(opts, "OrderFillChanged", hashRule, makerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Galler0OrderFillChanged)
				if err := _Galler0.contract.UnpackLog(event, "OrderFillChanged", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOrderFillChanged is a log parse operation binding the contract event 0x826cd05b52546b47590739469989cacd5336e0a9e6f9392de2dee5cfb19c8aab.
//
// Solidity: event OrderFillChanged(bytes32 indexed hash, address indexed maker, uint256 newFill)
func (_Galler0 *Galler0Filterer) ParseOrderFillChanged(log types.Log) (*Galler0OrderFillChanged, error) {
	event := new(Galler0OrderFillChanged)
	if err := _Galler0.contract.UnpackLog(event, "OrderFillChanged", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Galler0OrdersMatchedIterator is returned from FilterOrdersMatched and is used to iterate over the raw logs and unpacked data for OrdersMatched events raised by the Galler0 contract.
type Galler0OrdersMatchedIterator struct {
	Event *Galler0OrdersMatched // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Galler0OrdersMatchedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Galler0OrdersMatched)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Galler0OrdersMatched)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Galler0OrdersMatchedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Galler0OrdersMatchedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Galler0OrdersMatched represents a OrdersMatched event raised by the Galler0 contract.
type Galler0OrdersMatched struct {
	FirstHash     [32]byte
	SecondHash    [32]byte
	FirstMaker    common.Address
	SecondMaker   common.Address
	NewFirstFill  *big.Int
	NewSecondFill *big.Int
	Metadata      [32]byte
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOrdersMatched is a free log retrieval operation binding the contract event 0xe8447df45ce371d4c2f6cfcb9342b06165f71b244e809b909977f0dab191a009.
//
// Solidity: event OrdersMatched(bytes32 firstHash, bytes32 secondHash, address indexed firstMaker, address indexed secondMaker, uint256 newFirstFill, uint256 newSecondFill, bytes32 indexed metadata)
func (_Galler0 *Galler0Filterer) FilterOrdersMatched(opts *bind.FilterOpts, firstMaker []common.Address, secondMaker []common.Address, metadata [][32]byte) (*Galler0OrdersMatchedIterator, error) {

	var firstMakerRule []interface{}
	for _, firstMakerItem := range firstMaker {
		firstMakerRule = append(firstMakerRule, firstMakerItem)
	}
	var secondMakerRule []interface{}
	for _, secondMakerItem := range secondMaker {
		secondMakerRule = append(secondMakerRule, secondMakerItem)
	}

	var metadataRule []interface{}
	for _, metadataItem := range metadata {
		metadataRule = append(metadataRule, metadataItem)
	}

	logs, sub, err := _Galler0.contract.FilterLogs(opts, "OrdersMatched", firstMakerRule, secondMakerRule, metadataRule)
	if err != nil {
		return nil, err
	}
	return &Galler0OrdersMatchedIterator{contract: _Galler0.contract, event: "OrdersMatched", logs: logs, sub: sub}, nil
}

// WatchOrdersMatched is a free log subscription operation binding the contract event 0xe8447df45ce371d4c2f6cfcb9342b06165f71b244e809b909977f0dab191a009.
//
// Solidity: event OrdersMatched(bytes32 firstHash, bytes32 secondHash, address indexed firstMaker, address indexed secondMaker, uint256 newFirstFill, uint256 newSecondFill, bytes32 indexed metadata)
func (_Galler0 *Galler0Filterer) WatchOrdersMatched(opts *bind.WatchOpts, sink chan<- *Galler0OrdersMatched, firstMaker []common.Address, secondMaker []common.Address, metadata [][32]byte) (event.Subscription, error) {

	var firstMakerRule []interface{}
	for _, firstMakerItem := range firstMaker {
		firstMakerRule = append(firstMakerRule, firstMakerItem)
	}
	var secondMakerRule []interface{}
	for _, secondMakerItem := range secondMaker {
		secondMakerRule = append(secondMakerRule, secondMakerItem)
	}

	var metadataRule []interface{}
	for _, metadataItem := range metadata {
		metadataRule = append(metadataRule, metadataItem)
	}

	logs, sub, err := _Galler0.contract.WatchLogs(opts, "OrdersMatched", firstMakerRule, secondMakerRule, metadataRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Galler0OrdersMatched)
				if err := _Galler0.contract.UnpackLog(event, "OrdersMatched", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOrdersMatched is a log parse operation binding the contract event 0xe8447df45ce371d4c2f6cfcb9342b06165f71b244e809b909977f0dab191a009.
//
// Solidity: event OrdersMatched(bytes32 firstHash, bytes32 secondHash, address indexed firstMaker, address indexed secondMaker, uint256 newFirstFill, uint256 newSecondFill, bytes32 indexed metadata)
func (_Galler0 *Galler0Filterer) ParseOrdersMatched(log types.Log) (*Galler0OrdersMatched, error) {
	event := new(Galler0OrdersMatched)
	if err := _Galler0.contract.UnpackLog(event, "OrdersMatched", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
