// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package airnfts_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Airnfts0MetaData contains all meta data concerning the Airnfts0 contract.
var Airnfts0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"approved\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"Approval\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"approved\",\"type\":\"bool\"}],\"name\":\"ApprovalForAll\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"minter\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"nftID\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"uri\",\"type\":\"string\"}],\"name\":\"Minted\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"nftID\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"isListed\",\"type\":\"bool\"}],\"name\":\"NftListStatus\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"oldPrice\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"newPrice\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"nftID\",\"type\":\"uint256\"}],\"name\":\"PriceUpdate\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"nftID\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"uri\",\"type\":\"string\"}],\"name\":\"Purchase\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"_contractOwner\",\"outputs\":[{\"internalType\":\"addresspayable\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"baseURI\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"}],\"name\":\"buy\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"getApproved\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"}],\"name\":\"isApprovedForAll\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"listedMap\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_tokenURI\",\"type\":\"string\"},{\"internalType\":\"address\",\"name\":\"_toAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_price\",\"type\":\"uint256\"}],\"name\":\"mint\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"ownerOf\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"price\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"safeTransferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"_data\",\"type\":\"bytes\"}],\"name\":\"safeTransferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"approved\",\"type\":\"bool\"}],\"name\":\"setApprovalForAll\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes4\",\"name\":\"interfaceId\",\"type\":\"bytes4\"}],\"name\":\"supportsInterface\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"tokenByIndex\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"tokenOfOwnerByIndex\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"tokenURI\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"transferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"shouldBeListed\",\"type\":\"bool\"}],\"name\":\"updateListingStatus\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_price\",\"type\":\"uint256\"}],\"name\":\"updatePrice\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// Airnfts0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Airnfts0MetaData.ABI instead.
var Airnfts0ABI = Airnfts0MetaData.ABI

// Airnfts0 is an auto generated Go binding around an Ethereum contract.
type Airnfts0 struct {
	Airnfts0Caller     // Read-only binding to the contract
	Airnfts0Transactor // Write-only binding to the contract
	Airnfts0Filterer   // Log filterer for contract events
}

// Airnfts0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Airnfts0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Airnfts0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Airnfts0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Airnfts0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Airnfts0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Airnfts0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Airnfts0Session struct {
	Contract     *Airnfts0         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Airnfts0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Airnfts0CallerSession struct {
	Contract *Airnfts0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// Airnfts0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Airnfts0TransactorSession struct {
	Contract     *Airnfts0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// Airnfts0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Airnfts0Raw struct {
	Contract *Airnfts0 // Generic contract binding to access the raw methods on
}

// Airnfts0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Airnfts0CallerRaw struct {
	Contract *Airnfts0Caller // Generic read-only contract binding to access the raw methods on
}

// Airnfts0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Airnfts0TransactorRaw struct {
	Contract *Airnfts0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewAirnfts0 creates a new instance of Airnfts0, bound to a specific deployed contract.
func NewAirnfts0(address common.Address, backend bind.ContractBackend) (*Airnfts0, error) {
	contract, err := bindAirnfts0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Airnfts0{Airnfts0Caller: Airnfts0Caller{contract: contract}, Airnfts0Transactor: Airnfts0Transactor{contract: contract}, Airnfts0Filterer: Airnfts0Filterer{contract: contract}}, nil
}

// NewAirnfts0Caller creates a new read-only instance of Airnfts0, bound to a specific deployed contract.
func NewAirnfts0Caller(address common.Address, caller bind.ContractCaller) (*Airnfts0Caller, error) {
	contract, err := bindAirnfts0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Airnfts0Caller{contract: contract}, nil
}

// NewAirnfts0Transactor creates a new write-only instance of Airnfts0, bound to a specific deployed contract.
func NewAirnfts0Transactor(address common.Address, transactor bind.ContractTransactor) (*Airnfts0Transactor, error) {
	contract, err := bindAirnfts0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Airnfts0Transactor{contract: contract}, nil
}

// NewAirnfts0Filterer creates a new log filterer instance of Airnfts0, bound to a specific deployed contract.
func NewAirnfts0Filterer(address common.Address, filterer bind.ContractFilterer) (*Airnfts0Filterer, error) {
	contract, err := bindAirnfts0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Airnfts0Filterer{contract: contract}, nil
}

// bindAirnfts0 binds a generic wrapper to an already deployed contract.
func bindAirnfts0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Airnfts0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Airnfts0 *Airnfts0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Airnfts0.Contract.Airnfts0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Airnfts0 *Airnfts0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Airnfts0.Contract.Airnfts0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Airnfts0 *Airnfts0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Airnfts0.Contract.Airnfts0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Airnfts0 *Airnfts0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Airnfts0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Airnfts0 *Airnfts0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Airnfts0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Airnfts0 *Airnfts0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Airnfts0.Contract.contract.Transact(opts, method, params...)
}

// ContractOwner is a free data retrieval call binding the contract method 0x2bb3b114.
//
// Solidity: function _contractOwner() view returns(address)
func (_Airnfts0 *Airnfts0Caller) ContractOwner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "_contractOwner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// ContractOwner is a free data retrieval call binding the contract method 0x2bb3b114.
//
// Solidity: function _contractOwner() view returns(address)
func (_Airnfts0 *Airnfts0Session) ContractOwner() (common.Address, error) {
	return _Airnfts0.Contract.ContractOwner(&_Airnfts0.CallOpts)
}

// ContractOwner is a free data retrieval call binding the contract method 0x2bb3b114.
//
// Solidity: function _contractOwner() view returns(address)
func (_Airnfts0 *Airnfts0CallerSession) ContractOwner() (common.Address, error) {
	return _Airnfts0.Contract.ContractOwner(&_Airnfts0.CallOpts)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address owner) view returns(uint256)
func (_Airnfts0 *Airnfts0Caller) BalanceOf(opts *bind.CallOpts, owner common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "balanceOf", owner)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address owner) view returns(uint256)
func (_Airnfts0 *Airnfts0Session) BalanceOf(owner common.Address) (*big.Int, error) {
	return _Airnfts0.Contract.BalanceOf(&_Airnfts0.CallOpts, owner)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address owner) view returns(uint256)
func (_Airnfts0 *Airnfts0CallerSession) BalanceOf(owner common.Address) (*big.Int, error) {
	return _Airnfts0.Contract.BalanceOf(&_Airnfts0.CallOpts, owner)
}

// BaseURI is a free data retrieval call binding the contract method 0x6c0360eb.
//
// Solidity: function baseURI() view returns(string)
func (_Airnfts0 *Airnfts0Caller) BaseURI(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "baseURI")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// BaseURI is a free data retrieval call binding the contract method 0x6c0360eb.
//
// Solidity: function baseURI() view returns(string)
func (_Airnfts0 *Airnfts0Session) BaseURI() (string, error) {
	return _Airnfts0.Contract.BaseURI(&_Airnfts0.CallOpts)
}

// BaseURI is a free data retrieval call binding the contract method 0x6c0360eb.
//
// Solidity: function baseURI() view returns(string)
func (_Airnfts0 *Airnfts0CallerSession) BaseURI() (string, error) {
	return _Airnfts0.Contract.BaseURI(&_Airnfts0.CallOpts)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(address)
func (_Airnfts0 *Airnfts0Caller) GetApproved(opts *bind.CallOpts, tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "getApproved", tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(address)
func (_Airnfts0 *Airnfts0Session) GetApproved(tokenId *big.Int) (common.Address, error) {
	return _Airnfts0.Contract.GetApproved(&_Airnfts0.CallOpts, tokenId)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(address)
func (_Airnfts0 *Airnfts0CallerSession) GetApproved(tokenId *big.Int) (common.Address, error) {
	return _Airnfts0.Contract.GetApproved(&_Airnfts0.CallOpts, tokenId)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address owner, address operator) view returns(bool)
func (_Airnfts0 *Airnfts0Caller) IsApprovedForAll(opts *bind.CallOpts, owner common.Address, operator common.Address) (bool, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "isApprovedForAll", owner, operator)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address owner, address operator) view returns(bool)
func (_Airnfts0 *Airnfts0Session) IsApprovedForAll(owner common.Address, operator common.Address) (bool, error) {
	return _Airnfts0.Contract.IsApprovedForAll(&_Airnfts0.CallOpts, owner, operator)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address owner, address operator) view returns(bool)
func (_Airnfts0 *Airnfts0CallerSession) IsApprovedForAll(owner common.Address, operator common.Address) (bool, error) {
	return _Airnfts0.Contract.IsApprovedForAll(&_Airnfts0.CallOpts, owner, operator)
}

// ListedMap is a free data retrieval call binding the contract method 0xbc8ba28f.
//
// Solidity: function listedMap(uint256 ) view returns(bool)
func (_Airnfts0 *Airnfts0Caller) ListedMap(opts *bind.CallOpts, arg0 *big.Int) (bool, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "listedMap", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// ListedMap is a free data retrieval call binding the contract method 0xbc8ba28f.
//
// Solidity: function listedMap(uint256 ) view returns(bool)
func (_Airnfts0 *Airnfts0Session) ListedMap(arg0 *big.Int) (bool, error) {
	return _Airnfts0.Contract.ListedMap(&_Airnfts0.CallOpts, arg0)
}

// ListedMap is a free data retrieval call binding the contract method 0xbc8ba28f.
//
// Solidity: function listedMap(uint256 ) view returns(bool)
func (_Airnfts0 *Airnfts0CallerSession) ListedMap(arg0 *big.Int) (bool, error) {
	return _Airnfts0.Contract.ListedMap(&_Airnfts0.CallOpts, arg0)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Airnfts0 *Airnfts0Caller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Airnfts0 *Airnfts0Session) Name() (string, error) {
	return _Airnfts0.Contract.Name(&_Airnfts0.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Airnfts0 *Airnfts0CallerSession) Name() (string, error) {
	return _Airnfts0.Contract.Name(&_Airnfts0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Airnfts0 *Airnfts0Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Airnfts0 *Airnfts0Session) Owner() (common.Address, error) {
	return _Airnfts0.Contract.Owner(&_Airnfts0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Airnfts0 *Airnfts0CallerSession) Owner() (common.Address, error) {
	return _Airnfts0.Contract.Owner(&_Airnfts0.CallOpts)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(address)
func (_Airnfts0 *Airnfts0Caller) OwnerOf(opts *bind.CallOpts, tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "ownerOf", tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(address)
func (_Airnfts0 *Airnfts0Session) OwnerOf(tokenId *big.Int) (common.Address, error) {
	return _Airnfts0.Contract.OwnerOf(&_Airnfts0.CallOpts, tokenId)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(address)
func (_Airnfts0 *Airnfts0CallerSession) OwnerOf(tokenId *big.Int) (common.Address, error) {
	return _Airnfts0.Contract.OwnerOf(&_Airnfts0.CallOpts, tokenId)
}

// Price is a free data retrieval call binding the contract method 0x26a49e37.
//
// Solidity: function price(uint256 ) view returns(uint256)
func (_Airnfts0 *Airnfts0Caller) Price(opts *bind.CallOpts, arg0 *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "price", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Price is a free data retrieval call binding the contract method 0x26a49e37.
//
// Solidity: function price(uint256 ) view returns(uint256)
func (_Airnfts0 *Airnfts0Session) Price(arg0 *big.Int) (*big.Int, error) {
	return _Airnfts0.Contract.Price(&_Airnfts0.CallOpts, arg0)
}

// Price is a free data retrieval call binding the contract method 0x26a49e37.
//
// Solidity: function price(uint256 ) view returns(uint256)
func (_Airnfts0 *Airnfts0CallerSession) Price(arg0 *big.Int) (*big.Int, error) {
	return _Airnfts0.Contract.Price(&_Airnfts0.CallOpts, arg0)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Airnfts0 *Airnfts0Caller) SupportsInterface(opts *bind.CallOpts, interfaceId [4]byte) (bool, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "supportsInterface", interfaceId)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Airnfts0 *Airnfts0Session) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _Airnfts0.Contract.SupportsInterface(&_Airnfts0.CallOpts, interfaceId)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Airnfts0 *Airnfts0CallerSession) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _Airnfts0.Contract.SupportsInterface(&_Airnfts0.CallOpts, interfaceId)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_Airnfts0 *Airnfts0Caller) Symbol(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_Airnfts0 *Airnfts0Session) Symbol() (string, error) {
	return _Airnfts0.Contract.Symbol(&_Airnfts0.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_Airnfts0 *Airnfts0CallerSession) Symbol() (string, error) {
	return _Airnfts0.Contract.Symbol(&_Airnfts0.CallOpts)
}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) view returns(uint256)
func (_Airnfts0 *Airnfts0Caller) TokenByIndex(opts *bind.CallOpts, index *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "tokenByIndex", index)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) view returns(uint256)
func (_Airnfts0 *Airnfts0Session) TokenByIndex(index *big.Int) (*big.Int, error) {
	return _Airnfts0.Contract.TokenByIndex(&_Airnfts0.CallOpts, index)
}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) view returns(uint256)
func (_Airnfts0 *Airnfts0CallerSession) TokenByIndex(index *big.Int) (*big.Int, error) {
	return _Airnfts0.Contract.TokenByIndex(&_Airnfts0.CallOpts, index)
}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address owner, uint256 index) view returns(uint256)
func (_Airnfts0 *Airnfts0Caller) TokenOfOwnerByIndex(opts *bind.CallOpts, owner common.Address, index *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "tokenOfOwnerByIndex", owner, index)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address owner, uint256 index) view returns(uint256)
func (_Airnfts0 *Airnfts0Session) TokenOfOwnerByIndex(owner common.Address, index *big.Int) (*big.Int, error) {
	return _Airnfts0.Contract.TokenOfOwnerByIndex(&_Airnfts0.CallOpts, owner, index)
}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address owner, uint256 index) view returns(uint256)
func (_Airnfts0 *Airnfts0CallerSession) TokenOfOwnerByIndex(owner common.Address, index *big.Int) (*big.Int, error) {
	return _Airnfts0.Contract.TokenOfOwnerByIndex(&_Airnfts0.CallOpts, owner, index)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(string)
func (_Airnfts0 *Airnfts0Caller) TokenURI(opts *bind.CallOpts, tokenId *big.Int) (string, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "tokenURI", tokenId)

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(string)
func (_Airnfts0 *Airnfts0Session) TokenURI(tokenId *big.Int) (string, error) {
	return _Airnfts0.Contract.TokenURI(&_Airnfts0.CallOpts, tokenId)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(string)
func (_Airnfts0 *Airnfts0CallerSession) TokenURI(tokenId *big.Int) (string, error) {
	return _Airnfts0.Contract.TokenURI(&_Airnfts0.CallOpts, tokenId)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_Airnfts0 *Airnfts0Caller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Airnfts0.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_Airnfts0 *Airnfts0Session) TotalSupply() (*big.Int, error) {
	return _Airnfts0.Contract.TotalSupply(&_Airnfts0.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_Airnfts0 *Airnfts0CallerSession) TotalSupply() (*big.Int, error) {
	return _Airnfts0.Contract.TotalSupply(&_Airnfts0.CallOpts)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address to, uint256 tokenId) returns()
func (_Airnfts0 *Airnfts0Transactor) Approve(opts *bind.TransactOpts, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Airnfts0.contract.Transact(opts, "approve", to, tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address to, uint256 tokenId) returns()
func (_Airnfts0 *Airnfts0Session) Approve(to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Airnfts0.Contract.Approve(&_Airnfts0.TransactOpts, to, tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address to, uint256 tokenId) returns()
func (_Airnfts0 *Airnfts0TransactorSession) Approve(to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Airnfts0.Contract.Approve(&_Airnfts0.TransactOpts, to, tokenId)
}

// Buy is a paid mutator transaction binding the contract method 0xd96a094a.
//
// Solidity: function buy(uint256 _id) payable returns()
func (_Airnfts0 *Airnfts0Transactor) Buy(opts *bind.TransactOpts, _id *big.Int) (*types.Transaction, error) {
	return _Airnfts0.contract.Transact(opts, "buy", _id)
}

// Buy is a paid mutator transaction binding the contract method 0xd96a094a.
//
// Solidity: function buy(uint256 _id) payable returns()
func (_Airnfts0 *Airnfts0Session) Buy(_id *big.Int) (*types.Transaction, error) {
	return _Airnfts0.Contract.Buy(&_Airnfts0.TransactOpts, _id)
}

// Buy is a paid mutator transaction binding the contract method 0xd96a094a.
//
// Solidity: function buy(uint256 _id) payable returns()
func (_Airnfts0 *Airnfts0TransactorSession) Buy(_id *big.Int) (*types.Transaction, error) {
	return _Airnfts0.Contract.Buy(&_Airnfts0.TransactOpts, _id)
}

// Mint is a paid mutator transaction binding the contract method 0x7e8816b9.
//
// Solidity: function mint(string _tokenURI, address _toAddress, uint256 _price) returns(uint256)
func (_Airnfts0 *Airnfts0Transactor) Mint(opts *bind.TransactOpts, _tokenURI string, _toAddress common.Address, _price *big.Int) (*types.Transaction, error) {
	return _Airnfts0.contract.Transact(opts, "mint", _tokenURI, _toAddress, _price)
}

// Mint is a paid mutator transaction binding the contract method 0x7e8816b9.
//
// Solidity: function mint(string _tokenURI, address _toAddress, uint256 _price) returns(uint256)
func (_Airnfts0 *Airnfts0Session) Mint(_tokenURI string, _toAddress common.Address, _price *big.Int) (*types.Transaction, error) {
	return _Airnfts0.Contract.Mint(&_Airnfts0.TransactOpts, _tokenURI, _toAddress, _price)
}

// Mint is a paid mutator transaction binding the contract method 0x7e8816b9.
//
// Solidity: function mint(string _tokenURI, address _toAddress, uint256 _price) returns(uint256)
func (_Airnfts0 *Airnfts0TransactorSession) Mint(_tokenURI string, _toAddress common.Address, _price *big.Int) (*types.Transaction, error) {
	return _Airnfts0.Contract.Mint(&_Airnfts0.TransactOpts, _tokenURI, _toAddress, _price)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Airnfts0 *Airnfts0Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Airnfts0.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Airnfts0 *Airnfts0Session) RenounceOwnership() (*types.Transaction, error) {
	return _Airnfts0.Contract.RenounceOwnership(&_Airnfts0.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Airnfts0 *Airnfts0TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Airnfts0.Contract.RenounceOwnership(&_Airnfts0.TransactOpts)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId) returns()
func (_Airnfts0 *Airnfts0Transactor) SafeTransferFrom(opts *bind.TransactOpts, from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Airnfts0.contract.Transact(opts, "safeTransferFrom", from, to, tokenId)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId) returns()
func (_Airnfts0 *Airnfts0Session) SafeTransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Airnfts0.Contract.SafeTransferFrom(&_Airnfts0.TransactOpts, from, to, tokenId)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId) returns()
func (_Airnfts0 *Airnfts0TransactorSession) SafeTransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Airnfts0.Contract.SafeTransferFrom(&_Airnfts0.TransactOpts, from, to, tokenId)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId, bytes _data) returns()
func (_Airnfts0 *Airnfts0Transactor) SafeTransferFrom0(opts *bind.TransactOpts, from common.Address, to common.Address, tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _Airnfts0.contract.Transact(opts, "safeTransferFrom0", from, to, tokenId, _data)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId, bytes _data) returns()
func (_Airnfts0 *Airnfts0Session) SafeTransferFrom0(from common.Address, to common.Address, tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _Airnfts0.Contract.SafeTransferFrom0(&_Airnfts0.TransactOpts, from, to, tokenId, _data)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId, bytes _data) returns()
func (_Airnfts0 *Airnfts0TransactorSession) SafeTransferFrom0(from common.Address, to common.Address, tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _Airnfts0.Contract.SafeTransferFrom0(&_Airnfts0.TransactOpts, from, to, tokenId, _data)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address operator, bool approved) returns()
func (_Airnfts0 *Airnfts0Transactor) SetApprovalForAll(opts *bind.TransactOpts, operator common.Address, approved bool) (*types.Transaction, error) {
	return _Airnfts0.contract.Transact(opts, "setApprovalForAll", operator, approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address operator, bool approved) returns()
func (_Airnfts0 *Airnfts0Session) SetApprovalForAll(operator common.Address, approved bool) (*types.Transaction, error) {
	return _Airnfts0.Contract.SetApprovalForAll(&_Airnfts0.TransactOpts, operator, approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address operator, bool approved) returns()
func (_Airnfts0 *Airnfts0TransactorSession) SetApprovalForAll(operator common.Address, approved bool) (*types.Transaction, error) {
	return _Airnfts0.Contract.SetApprovalForAll(&_Airnfts0.TransactOpts, operator, approved)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address from, address to, uint256 tokenId) returns()
func (_Airnfts0 *Airnfts0Transactor) TransferFrom(opts *bind.TransactOpts, from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Airnfts0.contract.Transact(opts, "transferFrom", from, to, tokenId)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address from, address to, uint256 tokenId) returns()
func (_Airnfts0 *Airnfts0Session) TransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Airnfts0.Contract.TransferFrom(&_Airnfts0.TransactOpts, from, to, tokenId)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address from, address to, uint256 tokenId) returns()
func (_Airnfts0 *Airnfts0TransactorSession) TransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Airnfts0.Contract.TransferFrom(&_Airnfts0.TransactOpts, from, to, tokenId)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Airnfts0 *Airnfts0Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Airnfts0.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Airnfts0 *Airnfts0Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Airnfts0.Contract.TransferOwnership(&_Airnfts0.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Airnfts0 *Airnfts0TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Airnfts0.Contract.TransferOwnership(&_Airnfts0.TransactOpts, newOwner)
}

// UpdateListingStatus is a paid mutator transaction binding the contract method 0x98214bcb.
//
// Solidity: function updateListingStatus(uint256 _tokenId, bool shouldBeListed) returns(bool)
func (_Airnfts0 *Airnfts0Transactor) UpdateListingStatus(opts *bind.TransactOpts, _tokenId *big.Int, shouldBeListed bool) (*types.Transaction, error) {
	return _Airnfts0.contract.Transact(opts, "updateListingStatus", _tokenId, shouldBeListed)
}

// UpdateListingStatus is a paid mutator transaction binding the contract method 0x98214bcb.
//
// Solidity: function updateListingStatus(uint256 _tokenId, bool shouldBeListed) returns(bool)
func (_Airnfts0 *Airnfts0Session) UpdateListingStatus(_tokenId *big.Int, shouldBeListed bool) (*types.Transaction, error) {
	return _Airnfts0.Contract.UpdateListingStatus(&_Airnfts0.TransactOpts, _tokenId, shouldBeListed)
}

// UpdateListingStatus is a paid mutator transaction binding the contract method 0x98214bcb.
//
// Solidity: function updateListingStatus(uint256 _tokenId, bool shouldBeListed) returns(bool)
func (_Airnfts0 *Airnfts0TransactorSession) UpdateListingStatus(_tokenId *big.Int, shouldBeListed bool) (*types.Transaction, error) {
	return _Airnfts0.Contract.UpdateListingStatus(&_Airnfts0.TransactOpts, _tokenId, shouldBeListed)
}

// UpdatePrice is a paid mutator transaction binding the contract method 0x82367b2d.
//
// Solidity: function updatePrice(uint256 _tokenId, uint256 _price) returns(bool)
func (_Airnfts0 *Airnfts0Transactor) UpdatePrice(opts *bind.TransactOpts, _tokenId *big.Int, _price *big.Int) (*types.Transaction, error) {
	return _Airnfts0.contract.Transact(opts, "updatePrice", _tokenId, _price)
}

// UpdatePrice is a paid mutator transaction binding the contract method 0x82367b2d.
//
// Solidity: function updatePrice(uint256 _tokenId, uint256 _price) returns(bool)
func (_Airnfts0 *Airnfts0Session) UpdatePrice(_tokenId *big.Int, _price *big.Int) (*types.Transaction, error) {
	return _Airnfts0.Contract.UpdatePrice(&_Airnfts0.TransactOpts, _tokenId, _price)
}

// UpdatePrice is a paid mutator transaction binding the contract method 0x82367b2d.
//
// Solidity: function updatePrice(uint256 _tokenId, uint256 _price) returns(bool)
func (_Airnfts0 *Airnfts0TransactorSession) UpdatePrice(_tokenId *big.Int, _price *big.Int) (*types.Transaction, error) {
	return _Airnfts0.Contract.UpdatePrice(&_Airnfts0.TransactOpts, _tokenId, _price)
}

// Airnfts0ApprovalIterator is returned from FilterApproval and is used to iterate over the raw logs and unpacked data for Approval events raised by the Airnfts0 contract.
type Airnfts0ApprovalIterator struct {
	Event *Airnfts0Approval // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Airnfts0ApprovalIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Airnfts0Approval)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Airnfts0Approval)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Airnfts0ApprovalIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Airnfts0ApprovalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Airnfts0Approval represents a Approval event raised by the Airnfts0 contract.
type Airnfts0Approval struct {
	Owner    common.Address
	Approved common.Address
	TokenId  *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApproval is a free log retrieval operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_Airnfts0 *Airnfts0Filterer) FilterApproval(opts *bind.FilterOpts, owner []common.Address, approved []common.Address, tokenId []*big.Int) (*Airnfts0ApprovalIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var approvedRule []interface{}
	for _, approvedItem := range approved {
		approvedRule = append(approvedRule, approvedItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Airnfts0.contract.FilterLogs(opts, "Approval", ownerRule, approvedRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Airnfts0ApprovalIterator{contract: _Airnfts0.contract, event: "Approval", logs: logs, sub: sub}, nil
}

// WatchApproval is a free log subscription operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_Airnfts0 *Airnfts0Filterer) WatchApproval(opts *bind.WatchOpts, sink chan<- *Airnfts0Approval, owner []common.Address, approved []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var approvedRule []interface{}
	for _, approvedItem := range approved {
		approvedRule = append(approvedRule, approvedItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Airnfts0.contract.WatchLogs(opts, "Approval", ownerRule, approvedRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Airnfts0Approval)
				if err := _Airnfts0.contract.UnpackLog(event, "Approval", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApproval is a log parse operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_Airnfts0 *Airnfts0Filterer) ParseApproval(log types.Log) (*Airnfts0Approval, error) {
	event := new(Airnfts0Approval)
	if err := _Airnfts0.contract.UnpackLog(event, "Approval", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Airnfts0ApprovalForAllIterator is returned from FilterApprovalForAll and is used to iterate over the raw logs and unpacked data for ApprovalForAll events raised by the Airnfts0 contract.
type Airnfts0ApprovalForAllIterator struct {
	Event *Airnfts0ApprovalForAll // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Airnfts0ApprovalForAllIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Airnfts0ApprovalForAll)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Airnfts0ApprovalForAll)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Airnfts0ApprovalForAllIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Airnfts0ApprovalForAllIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Airnfts0ApprovalForAll represents a ApprovalForAll event raised by the Airnfts0 contract.
type Airnfts0ApprovalForAll struct {
	Owner    common.Address
	Operator common.Address
	Approved bool
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApprovalForAll is a free log retrieval operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_Airnfts0 *Airnfts0Filterer) FilterApprovalForAll(opts *bind.FilterOpts, owner []common.Address, operator []common.Address) (*Airnfts0ApprovalForAllIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var operatorRule []interface{}
	for _, operatorItem := range operator {
		operatorRule = append(operatorRule, operatorItem)
	}

	logs, sub, err := _Airnfts0.contract.FilterLogs(opts, "ApprovalForAll", ownerRule, operatorRule)
	if err != nil {
		return nil, err
	}
	return &Airnfts0ApprovalForAllIterator{contract: _Airnfts0.contract, event: "ApprovalForAll", logs: logs, sub: sub}, nil
}

// WatchApprovalForAll is a free log subscription operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_Airnfts0 *Airnfts0Filterer) WatchApprovalForAll(opts *bind.WatchOpts, sink chan<- *Airnfts0ApprovalForAll, owner []common.Address, operator []common.Address) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var operatorRule []interface{}
	for _, operatorItem := range operator {
		operatorRule = append(operatorRule, operatorItem)
	}

	logs, sub, err := _Airnfts0.contract.WatchLogs(opts, "ApprovalForAll", ownerRule, operatorRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Airnfts0ApprovalForAll)
				if err := _Airnfts0.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApprovalForAll is a log parse operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_Airnfts0 *Airnfts0Filterer) ParseApprovalForAll(log types.Log) (*Airnfts0ApprovalForAll, error) {
	event := new(Airnfts0ApprovalForAll)
	if err := _Airnfts0.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Airnfts0MintedIterator is returned from FilterMinted and is used to iterate over the raw logs and unpacked data for Minted events raised by the Airnfts0 contract.
type Airnfts0MintedIterator struct {
	Event *Airnfts0Minted // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Airnfts0MintedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Airnfts0Minted)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Airnfts0Minted)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Airnfts0MintedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Airnfts0MintedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Airnfts0Minted represents a Minted event raised by the Airnfts0 contract.
type Airnfts0Minted struct {
	Minter common.Address
	Price  *big.Int
	NftID  *big.Int
	Uri    string
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterMinted is a free log retrieval operation binding the contract event 0xf2cb5e52049d127ad1c335f1cc25f2fdbc911bec1beb2611f4c1e8b1c274d4b4.
//
// Solidity: event Minted(address indexed minter, uint256 price, uint256 nftID, string uri)
func (_Airnfts0 *Airnfts0Filterer) FilterMinted(opts *bind.FilterOpts, minter []common.Address) (*Airnfts0MintedIterator, error) {

	var minterRule []interface{}
	for _, minterItem := range minter {
		minterRule = append(minterRule, minterItem)
	}

	logs, sub, err := _Airnfts0.contract.FilterLogs(opts, "Minted", minterRule)
	if err != nil {
		return nil, err
	}
	return &Airnfts0MintedIterator{contract: _Airnfts0.contract, event: "Minted", logs: logs, sub: sub}, nil
}

// WatchMinted is a free log subscription operation binding the contract event 0xf2cb5e52049d127ad1c335f1cc25f2fdbc911bec1beb2611f4c1e8b1c274d4b4.
//
// Solidity: event Minted(address indexed minter, uint256 price, uint256 nftID, string uri)
func (_Airnfts0 *Airnfts0Filterer) WatchMinted(opts *bind.WatchOpts, sink chan<- *Airnfts0Minted, minter []common.Address) (event.Subscription, error) {

	var minterRule []interface{}
	for _, minterItem := range minter {
		minterRule = append(minterRule, minterItem)
	}

	logs, sub, err := _Airnfts0.contract.WatchLogs(opts, "Minted", minterRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Airnfts0Minted)
				if err := _Airnfts0.contract.UnpackLog(event, "Minted", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseMinted is a log parse operation binding the contract event 0xf2cb5e52049d127ad1c335f1cc25f2fdbc911bec1beb2611f4c1e8b1c274d4b4.
//
// Solidity: event Minted(address indexed minter, uint256 price, uint256 nftID, string uri)
func (_Airnfts0 *Airnfts0Filterer) ParseMinted(log types.Log) (*Airnfts0Minted, error) {
	event := new(Airnfts0Minted)
	if err := _Airnfts0.contract.UnpackLog(event, "Minted", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Airnfts0NftListStatusIterator is returned from FilterNftListStatus and is used to iterate over the raw logs and unpacked data for NftListStatus events raised by the Airnfts0 contract.
type Airnfts0NftListStatusIterator struct {
	Event *Airnfts0NftListStatus // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Airnfts0NftListStatusIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Airnfts0NftListStatus)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Airnfts0NftListStatus)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Airnfts0NftListStatusIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Airnfts0NftListStatusIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Airnfts0NftListStatus represents a NftListStatus event raised by the Airnfts0 contract.
type Airnfts0NftListStatus struct {
	Owner    common.Address
	NftID    *big.Int
	IsListed bool
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterNftListStatus is a free log retrieval operation binding the contract event 0x3fd63d9ca8dc693a1b9911e664951294721009a4f6239c862d6719a160a1edfc.
//
// Solidity: event NftListStatus(address indexed owner, uint256 nftID, bool isListed)
func (_Airnfts0 *Airnfts0Filterer) FilterNftListStatus(opts *bind.FilterOpts, owner []common.Address) (*Airnfts0NftListStatusIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}

	logs, sub, err := _Airnfts0.contract.FilterLogs(opts, "NftListStatus", ownerRule)
	if err != nil {
		return nil, err
	}
	return &Airnfts0NftListStatusIterator{contract: _Airnfts0.contract, event: "NftListStatus", logs: logs, sub: sub}, nil
}

// WatchNftListStatus is a free log subscription operation binding the contract event 0x3fd63d9ca8dc693a1b9911e664951294721009a4f6239c862d6719a160a1edfc.
//
// Solidity: event NftListStatus(address indexed owner, uint256 nftID, bool isListed)
func (_Airnfts0 *Airnfts0Filterer) WatchNftListStatus(opts *bind.WatchOpts, sink chan<- *Airnfts0NftListStatus, owner []common.Address) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}

	logs, sub, err := _Airnfts0.contract.WatchLogs(opts, "NftListStatus", ownerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Airnfts0NftListStatus)
				if err := _Airnfts0.contract.UnpackLog(event, "NftListStatus", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNftListStatus is a log parse operation binding the contract event 0x3fd63d9ca8dc693a1b9911e664951294721009a4f6239c862d6719a160a1edfc.
//
// Solidity: event NftListStatus(address indexed owner, uint256 nftID, bool isListed)
func (_Airnfts0 *Airnfts0Filterer) ParseNftListStatus(log types.Log) (*Airnfts0NftListStatus, error) {
	event := new(Airnfts0NftListStatus)
	if err := _Airnfts0.contract.UnpackLog(event, "NftListStatus", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Airnfts0OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Airnfts0 contract.
type Airnfts0OwnershipTransferredIterator struct {
	Event *Airnfts0OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Airnfts0OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Airnfts0OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Airnfts0OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Airnfts0OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Airnfts0OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Airnfts0OwnershipTransferred represents a OwnershipTransferred event raised by the Airnfts0 contract.
type Airnfts0OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Airnfts0 *Airnfts0Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Airnfts0OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Airnfts0.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Airnfts0OwnershipTransferredIterator{contract: _Airnfts0.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Airnfts0 *Airnfts0Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Airnfts0OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Airnfts0.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Airnfts0OwnershipTransferred)
				if err := _Airnfts0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Airnfts0 *Airnfts0Filterer) ParseOwnershipTransferred(log types.Log) (*Airnfts0OwnershipTransferred, error) {
	event := new(Airnfts0OwnershipTransferred)
	if err := _Airnfts0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Airnfts0PriceUpdateIterator is returned from FilterPriceUpdate and is used to iterate over the raw logs and unpacked data for PriceUpdate events raised by the Airnfts0 contract.
type Airnfts0PriceUpdateIterator struct {
	Event *Airnfts0PriceUpdate // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Airnfts0PriceUpdateIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Airnfts0PriceUpdate)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Airnfts0PriceUpdate)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Airnfts0PriceUpdateIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Airnfts0PriceUpdateIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Airnfts0PriceUpdate represents a PriceUpdate event raised by the Airnfts0 contract.
type Airnfts0PriceUpdate struct {
	Owner    common.Address
	OldPrice *big.Int
	NewPrice *big.Int
	NftID    *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterPriceUpdate is a free log retrieval operation binding the contract event 0x8647dab5101cbe18afb171756e9753802f9d66725bf2346b079b8b1a275e0116.
//
// Solidity: event PriceUpdate(address indexed owner, uint256 oldPrice, uint256 newPrice, uint256 nftID)
func (_Airnfts0 *Airnfts0Filterer) FilterPriceUpdate(opts *bind.FilterOpts, owner []common.Address) (*Airnfts0PriceUpdateIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}

	logs, sub, err := _Airnfts0.contract.FilterLogs(opts, "PriceUpdate", ownerRule)
	if err != nil {
		return nil, err
	}
	return &Airnfts0PriceUpdateIterator{contract: _Airnfts0.contract, event: "PriceUpdate", logs: logs, sub: sub}, nil
}

// WatchPriceUpdate is a free log subscription operation binding the contract event 0x8647dab5101cbe18afb171756e9753802f9d66725bf2346b079b8b1a275e0116.
//
// Solidity: event PriceUpdate(address indexed owner, uint256 oldPrice, uint256 newPrice, uint256 nftID)
func (_Airnfts0 *Airnfts0Filterer) WatchPriceUpdate(opts *bind.WatchOpts, sink chan<- *Airnfts0PriceUpdate, owner []common.Address) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}

	logs, sub, err := _Airnfts0.contract.WatchLogs(opts, "PriceUpdate", ownerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Airnfts0PriceUpdate)
				if err := _Airnfts0.contract.UnpackLog(event, "PriceUpdate", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePriceUpdate is a log parse operation binding the contract event 0x8647dab5101cbe18afb171756e9753802f9d66725bf2346b079b8b1a275e0116.
//
// Solidity: event PriceUpdate(address indexed owner, uint256 oldPrice, uint256 newPrice, uint256 nftID)
func (_Airnfts0 *Airnfts0Filterer) ParsePriceUpdate(log types.Log) (*Airnfts0PriceUpdate, error) {
	event := new(Airnfts0PriceUpdate)
	if err := _Airnfts0.contract.UnpackLog(event, "PriceUpdate", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Airnfts0PurchaseIterator is returned from FilterPurchase and is used to iterate over the raw logs and unpacked data for Purchase events raised by the Airnfts0 contract.
type Airnfts0PurchaseIterator struct {
	Event *Airnfts0Purchase // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Airnfts0PurchaseIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Airnfts0Purchase)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Airnfts0Purchase)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Airnfts0PurchaseIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Airnfts0PurchaseIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Airnfts0Purchase represents a Purchase event raised by the Airnfts0 contract.
type Airnfts0Purchase struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Price         *big.Int
	NftID         *big.Int
	Uri           string
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterPurchase is a free log retrieval operation binding the contract event 0xef258f47a33a1cba99d81ea828f234ff5d6cb31034c0f79ecb5198f8c6d118f6.
//
// Solidity: event Purchase(address indexed previousOwner, address indexed newOwner, uint256 price, uint256 nftID, string uri)
func (_Airnfts0 *Airnfts0Filterer) FilterPurchase(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Airnfts0PurchaseIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Airnfts0.contract.FilterLogs(opts, "Purchase", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Airnfts0PurchaseIterator{contract: _Airnfts0.contract, event: "Purchase", logs: logs, sub: sub}, nil
}

// WatchPurchase is a free log subscription operation binding the contract event 0xef258f47a33a1cba99d81ea828f234ff5d6cb31034c0f79ecb5198f8c6d118f6.
//
// Solidity: event Purchase(address indexed previousOwner, address indexed newOwner, uint256 price, uint256 nftID, string uri)
func (_Airnfts0 *Airnfts0Filterer) WatchPurchase(opts *bind.WatchOpts, sink chan<- *Airnfts0Purchase, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Airnfts0.contract.WatchLogs(opts, "Purchase", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Airnfts0Purchase)
				if err := _Airnfts0.contract.UnpackLog(event, "Purchase", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePurchase is a log parse operation binding the contract event 0xef258f47a33a1cba99d81ea828f234ff5d6cb31034c0f79ecb5198f8c6d118f6.
//
// Solidity: event Purchase(address indexed previousOwner, address indexed newOwner, uint256 price, uint256 nftID, string uri)
func (_Airnfts0 *Airnfts0Filterer) ParsePurchase(log types.Log) (*Airnfts0Purchase, error) {
	event := new(Airnfts0Purchase)
	if err := _Airnfts0.contract.UnpackLog(event, "Purchase", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Airnfts0TransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the Airnfts0 contract.
type Airnfts0TransferIterator struct {
	Event *Airnfts0Transfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Airnfts0TransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Airnfts0Transfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Airnfts0Transfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Airnfts0TransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Airnfts0TransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Airnfts0Transfer represents a Transfer event raised by the Airnfts0 contract.
type Airnfts0Transfer struct {
	From    common.Address
	To      common.Address
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_Airnfts0 *Airnfts0Filterer) FilterTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address, tokenId []*big.Int) (*Airnfts0TransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Airnfts0.contract.FilterLogs(opts, "Transfer", fromRule, toRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Airnfts0TransferIterator{contract: _Airnfts0.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_Airnfts0 *Airnfts0Filterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *Airnfts0Transfer, from []common.Address, to []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Airnfts0.contract.WatchLogs(opts, "Transfer", fromRule, toRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Airnfts0Transfer)
				if err := _Airnfts0.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_Airnfts0 *Airnfts0Filterer) ParseTransfer(log types.Log) (*Airnfts0Transfer, error) {
	event := new(Airnfts0Transfer)
	if err := _Airnfts0.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
