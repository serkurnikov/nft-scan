// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package superrare_3

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Superrare3MetaData contains all meta data concerning the Superrare3 contract.
var Superrare3MetaData = &bind.MetaData{
	ABI: "[{\"constant\":false,\"inputs\":[{\"name\":\"_uri\",\"type\":\"string\"},{\"name\":\"_editions\",\"type\":\"uint256\"},{\"name\":\"_salePrice\",\"type\":\"uint256\"}],\"name\":\"addNewTokenWithEditions\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"name\":\"_salePrice\",\"type\":\"uint256\"}],\"name\":\"setSalePrice\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"name\":\"_name\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"pure\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_to\",\"type\":\"address\"},{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"currentBidDetailsOfToken\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"},{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"approvedFor\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"acceptBid\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"_creator\",\"type\":\"address\"}],\"name\":\"isWhitelisted\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"bid\",\"outputs\":[],\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"_owner\",\"type\":\"address\"}],\"name\":\"tokensOf\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256[]\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_percentage\",\"type\":\"uint256\"}],\"name\":\"setMaintainerPercentage\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_creator\",\"type\":\"address\"}],\"name\":\"whitelistCreator\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"ownerOf\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"_uri\",\"type\":\"string\"}],\"name\":\"originalTokenOfUri\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"_owner\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"name\":\"_symbol\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"pure\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"cancelBid\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"salePriceOfToken\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_to\",\"type\":\"address\"},{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"transfer\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"takeOwnership\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_percentage\",\"type\":\"uint256\"}],\"name\":\"setCreatorPercentage\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"tokenURI\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"creatorOfToken\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"buy\",\"outputs\":[],\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_uri\",\"type\":\"string\"}],\"name\":\"addNewToken\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"creatorPercentage\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"maintainerPercentage\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"_creator\",\"type\":\"address\"}],\"name\":\"WhitelistCreator\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"_bidder\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"_amount\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"Bid\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"_bidder\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"_seller\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"_amount\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"AcceptBid\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"_bidder\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"_amount\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"CancelBid\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"_buyer\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"_seller\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"_amount\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"Sold\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"_price\",\"type\":\"uint256\"}],\"name\":\"SalePriceSet\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"_from\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"_to\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"_owner\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"_approved\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"Approval\",\"type\":\"event\"}]",
}

// Superrare3ABI is the input ABI used to generate the binding from.
// Deprecated: Use Superrare3MetaData.ABI instead.
var Superrare3ABI = Superrare3MetaData.ABI

// Superrare3 is an auto generated Go binding around an Ethereum contract.
type Superrare3 struct {
	Superrare3Caller     // Read-only binding to the contract
	Superrare3Transactor // Write-only binding to the contract
	Superrare3Filterer   // Log filterer for contract events
}

// Superrare3Caller is an auto generated read-only Go binding around an Ethereum contract.
type Superrare3Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Superrare3Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Superrare3Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Superrare3Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Superrare3Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Superrare3Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Superrare3Session struct {
	Contract     *Superrare3       // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Superrare3CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Superrare3CallerSession struct {
	Contract *Superrare3Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts     // Call options to use throughout this session
}

// Superrare3TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Superrare3TransactorSession struct {
	Contract     *Superrare3Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts     // Transaction auth options to use throughout this session
}

// Superrare3Raw is an auto generated low-level Go binding around an Ethereum contract.
type Superrare3Raw struct {
	Contract *Superrare3 // Generic contract binding to access the raw methods on
}

// Superrare3CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Superrare3CallerRaw struct {
	Contract *Superrare3Caller // Generic read-only contract binding to access the raw methods on
}

// Superrare3TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Superrare3TransactorRaw struct {
	Contract *Superrare3Transactor // Generic write-only contract binding to access the raw methods on
}

// NewSuperrare3 creates a new instance of Superrare3, bound to a specific deployed contract.
func NewSuperrare3(address common.Address, backend bind.ContractBackend) (*Superrare3, error) {
	contract, err := bindSuperrare3(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Superrare3{Superrare3Caller: Superrare3Caller{contract: contract}, Superrare3Transactor: Superrare3Transactor{contract: contract}, Superrare3Filterer: Superrare3Filterer{contract: contract}}, nil
}

// NewSuperrare3Caller creates a new read-only instance of Superrare3, bound to a specific deployed contract.
func NewSuperrare3Caller(address common.Address, caller bind.ContractCaller) (*Superrare3Caller, error) {
	contract, err := bindSuperrare3(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Superrare3Caller{contract: contract}, nil
}

// NewSuperrare3Transactor creates a new write-only instance of Superrare3, bound to a specific deployed contract.
func NewSuperrare3Transactor(address common.Address, transactor bind.ContractTransactor) (*Superrare3Transactor, error) {
	contract, err := bindSuperrare3(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Superrare3Transactor{contract: contract}, nil
}

// NewSuperrare3Filterer creates a new log filterer instance of Superrare3, bound to a specific deployed contract.
func NewSuperrare3Filterer(address common.Address, filterer bind.ContractFilterer) (*Superrare3Filterer, error) {
	contract, err := bindSuperrare3(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Superrare3Filterer{contract: contract}, nil
}

// bindSuperrare3 binds a generic wrapper to an already deployed contract.
func bindSuperrare3(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Superrare3ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Superrare3 *Superrare3Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Superrare3.Contract.Superrare3Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Superrare3 *Superrare3Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Superrare3.Contract.Superrare3Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Superrare3 *Superrare3Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Superrare3.Contract.Superrare3Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Superrare3 *Superrare3CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Superrare3.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Superrare3 *Superrare3TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Superrare3.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Superrare3 *Superrare3TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Superrare3.Contract.contract.Transact(opts, method, params...)
}

// ApprovedFor is a free data retrieval call binding the contract method 0x2a6dd48f.
//
// Solidity: function approvedFor(uint256 _tokenId) view returns(address)
func (_Superrare3 *Superrare3Caller) ApprovedFor(opts *bind.CallOpts, _tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "approvedFor", _tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// ApprovedFor is a free data retrieval call binding the contract method 0x2a6dd48f.
//
// Solidity: function approvedFor(uint256 _tokenId) view returns(address)
func (_Superrare3 *Superrare3Session) ApprovedFor(_tokenId *big.Int) (common.Address, error) {
	return _Superrare3.Contract.ApprovedFor(&_Superrare3.CallOpts, _tokenId)
}

// ApprovedFor is a free data retrieval call binding the contract method 0x2a6dd48f.
//
// Solidity: function approvedFor(uint256 _tokenId) view returns(address)
func (_Superrare3 *Superrare3CallerSession) ApprovedFor(_tokenId *big.Int) (common.Address, error) {
	return _Superrare3.Contract.ApprovedFor(&_Superrare3.CallOpts, _tokenId)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address _owner) view returns(uint256)
func (_Superrare3 *Superrare3Caller) BalanceOf(opts *bind.CallOpts, _owner common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "balanceOf", _owner)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address _owner) view returns(uint256)
func (_Superrare3 *Superrare3Session) BalanceOf(_owner common.Address) (*big.Int, error) {
	return _Superrare3.Contract.BalanceOf(&_Superrare3.CallOpts, _owner)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address _owner) view returns(uint256)
func (_Superrare3 *Superrare3CallerSession) BalanceOf(_owner common.Address) (*big.Int, error) {
	return _Superrare3.Contract.BalanceOf(&_Superrare3.CallOpts, _owner)
}

// CreatorOfToken is a free data retrieval call binding the contract method 0xd5da8d44.
//
// Solidity: function creatorOfToken(uint256 _tokenId) view returns(address)
func (_Superrare3 *Superrare3Caller) CreatorOfToken(opts *bind.CallOpts, _tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "creatorOfToken", _tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// CreatorOfToken is a free data retrieval call binding the contract method 0xd5da8d44.
//
// Solidity: function creatorOfToken(uint256 _tokenId) view returns(address)
func (_Superrare3 *Superrare3Session) CreatorOfToken(_tokenId *big.Int) (common.Address, error) {
	return _Superrare3.Contract.CreatorOfToken(&_Superrare3.CallOpts, _tokenId)
}

// CreatorOfToken is a free data retrieval call binding the contract method 0xd5da8d44.
//
// Solidity: function creatorOfToken(uint256 _tokenId) view returns(address)
func (_Superrare3 *Superrare3CallerSession) CreatorOfToken(_tokenId *big.Int) (common.Address, error) {
	return _Superrare3.Contract.CreatorOfToken(&_Superrare3.CallOpts, _tokenId)
}

// CreatorPercentage is a free data retrieval call binding the contract method 0xf071bf4f.
//
// Solidity: function creatorPercentage() view returns(uint256)
func (_Superrare3 *Superrare3Caller) CreatorPercentage(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "creatorPercentage")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CreatorPercentage is a free data retrieval call binding the contract method 0xf071bf4f.
//
// Solidity: function creatorPercentage() view returns(uint256)
func (_Superrare3 *Superrare3Session) CreatorPercentage() (*big.Int, error) {
	return _Superrare3.Contract.CreatorPercentage(&_Superrare3.CallOpts)
}

// CreatorPercentage is a free data retrieval call binding the contract method 0xf071bf4f.
//
// Solidity: function creatorPercentage() view returns(uint256)
func (_Superrare3 *Superrare3CallerSession) CreatorPercentage() (*big.Int, error) {
	return _Superrare3.Contract.CreatorPercentage(&_Superrare3.CallOpts)
}

// CurrentBidDetailsOfToken is a free data retrieval call binding the contract method 0x1a289aea.
//
// Solidity: function currentBidDetailsOfToken(uint256 _tokenId) view returns(uint256, address)
func (_Superrare3 *Superrare3Caller) CurrentBidDetailsOfToken(opts *bind.CallOpts, _tokenId *big.Int) (*big.Int, common.Address, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "currentBidDetailsOfToken", _tokenId)

	if err != nil {
		return *new(*big.Int), *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	out1 := *abi.ConvertType(out[1], new(common.Address)).(*common.Address)

	return out0, out1, err

}

// CurrentBidDetailsOfToken is a free data retrieval call binding the contract method 0x1a289aea.
//
// Solidity: function currentBidDetailsOfToken(uint256 _tokenId) view returns(uint256, address)
func (_Superrare3 *Superrare3Session) CurrentBidDetailsOfToken(_tokenId *big.Int) (*big.Int, common.Address, error) {
	return _Superrare3.Contract.CurrentBidDetailsOfToken(&_Superrare3.CallOpts, _tokenId)
}

// CurrentBidDetailsOfToken is a free data retrieval call binding the contract method 0x1a289aea.
//
// Solidity: function currentBidDetailsOfToken(uint256 _tokenId) view returns(uint256, address)
func (_Superrare3 *Superrare3CallerSession) CurrentBidDetailsOfToken(_tokenId *big.Int) (*big.Int, common.Address, error) {
	return _Superrare3.Contract.CurrentBidDetailsOfToken(&_Superrare3.CallOpts, _tokenId)
}

// IsWhitelisted is a free data retrieval call binding the contract method 0x3af32abf.
//
// Solidity: function isWhitelisted(address _creator) view returns(bool)
func (_Superrare3 *Superrare3Caller) IsWhitelisted(opts *bind.CallOpts, _creator common.Address) (bool, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "isWhitelisted", _creator)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsWhitelisted is a free data retrieval call binding the contract method 0x3af32abf.
//
// Solidity: function isWhitelisted(address _creator) view returns(bool)
func (_Superrare3 *Superrare3Session) IsWhitelisted(_creator common.Address) (bool, error) {
	return _Superrare3.Contract.IsWhitelisted(&_Superrare3.CallOpts, _creator)
}

// IsWhitelisted is a free data retrieval call binding the contract method 0x3af32abf.
//
// Solidity: function isWhitelisted(address _creator) view returns(bool)
func (_Superrare3 *Superrare3CallerSession) IsWhitelisted(_creator common.Address) (bool, error) {
	return _Superrare3.Contract.IsWhitelisted(&_Superrare3.CallOpts, _creator)
}

// MaintainerPercentage is a free data retrieval call binding the contract method 0xf2bf6b8c.
//
// Solidity: function maintainerPercentage() view returns(uint256)
func (_Superrare3 *Superrare3Caller) MaintainerPercentage(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "maintainerPercentage")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MaintainerPercentage is a free data retrieval call binding the contract method 0xf2bf6b8c.
//
// Solidity: function maintainerPercentage() view returns(uint256)
func (_Superrare3 *Superrare3Session) MaintainerPercentage() (*big.Int, error) {
	return _Superrare3.Contract.MaintainerPercentage(&_Superrare3.CallOpts)
}

// MaintainerPercentage is a free data retrieval call binding the contract method 0xf2bf6b8c.
//
// Solidity: function maintainerPercentage() view returns(uint256)
func (_Superrare3 *Superrare3CallerSession) MaintainerPercentage() (*big.Int, error) {
	return _Superrare3.Contract.MaintainerPercentage(&_Superrare3.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() pure returns(string _name)
func (_Superrare3 *Superrare3Caller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() pure returns(string _name)
func (_Superrare3 *Superrare3Session) Name() (string, error) {
	return _Superrare3.Contract.Name(&_Superrare3.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() pure returns(string _name)
func (_Superrare3 *Superrare3CallerSession) Name() (string, error) {
	return _Superrare3.Contract.Name(&_Superrare3.CallOpts)
}

// OriginalTokenOfUri is a free data retrieval call binding the contract method 0x653436fd.
//
// Solidity: function originalTokenOfUri(string _uri) view returns(uint256)
func (_Superrare3 *Superrare3Caller) OriginalTokenOfUri(opts *bind.CallOpts, _uri string) (*big.Int, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "originalTokenOfUri", _uri)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// OriginalTokenOfUri is a free data retrieval call binding the contract method 0x653436fd.
//
// Solidity: function originalTokenOfUri(string _uri) view returns(uint256)
func (_Superrare3 *Superrare3Session) OriginalTokenOfUri(_uri string) (*big.Int, error) {
	return _Superrare3.Contract.OriginalTokenOfUri(&_Superrare3.CallOpts, _uri)
}

// OriginalTokenOfUri is a free data retrieval call binding the contract method 0x653436fd.
//
// Solidity: function originalTokenOfUri(string _uri) view returns(uint256)
func (_Superrare3 *Superrare3CallerSession) OriginalTokenOfUri(_uri string) (*big.Int, error) {
	return _Superrare3.Contract.OriginalTokenOfUri(&_Superrare3.CallOpts, _uri)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Superrare3 *Superrare3Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Superrare3 *Superrare3Session) Owner() (common.Address, error) {
	return _Superrare3.Contract.Owner(&_Superrare3.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Superrare3 *Superrare3CallerSession) Owner() (common.Address, error) {
	return _Superrare3.Contract.Owner(&_Superrare3.CallOpts)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 _tokenId) view returns(address)
func (_Superrare3 *Superrare3Caller) OwnerOf(opts *bind.CallOpts, _tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "ownerOf", _tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 _tokenId) view returns(address)
func (_Superrare3 *Superrare3Session) OwnerOf(_tokenId *big.Int) (common.Address, error) {
	return _Superrare3.Contract.OwnerOf(&_Superrare3.CallOpts, _tokenId)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 _tokenId) view returns(address)
func (_Superrare3 *Superrare3CallerSession) OwnerOf(_tokenId *big.Int) (common.Address, error) {
	return _Superrare3.Contract.OwnerOf(&_Superrare3.CallOpts, _tokenId)
}

// SalePriceOfToken is a free data retrieval call binding the contract method 0x9f2a3c32.
//
// Solidity: function salePriceOfToken(uint256 _tokenId) view returns(uint256)
func (_Superrare3 *Superrare3Caller) SalePriceOfToken(opts *bind.CallOpts, _tokenId *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "salePriceOfToken", _tokenId)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// SalePriceOfToken is a free data retrieval call binding the contract method 0x9f2a3c32.
//
// Solidity: function salePriceOfToken(uint256 _tokenId) view returns(uint256)
func (_Superrare3 *Superrare3Session) SalePriceOfToken(_tokenId *big.Int) (*big.Int, error) {
	return _Superrare3.Contract.SalePriceOfToken(&_Superrare3.CallOpts, _tokenId)
}

// SalePriceOfToken is a free data retrieval call binding the contract method 0x9f2a3c32.
//
// Solidity: function salePriceOfToken(uint256 _tokenId) view returns(uint256)
func (_Superrare3 *Superrare3CallerSession) SalePriceOfToken(_tokenId *big.Int) (*big.Int, error) {
	return _Superrare3.Contract.SalePriceOfToken(&_Superrare3.CallOpts, _tokenId)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() pure returns(string _symbol)
func (_Superrare3 *Superrare3Caller) Symbol(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() pure returns(string _symbol)
func (_Superrare3 *Superrare3Session) Symbol() (string, error) {
	return _Superrare3.Contract.Symbol(&_Superrare3.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() pure returns(string _symbol)
func (_Superrare3 *Superrare3CallerSession) Symbol() (string, error) {
	return _Superrare3.Contract.Symbol(&_Superrare3.CallOpts)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 _tokenId) view returns(string)
func (_Superrare3 *Superrare3Caller) TokenURI(opts *bind.CallOpts, _tokenId *big.Int) (string, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "tokenURI", _tokenId)

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 _tokenId) view returns(string)
func (_Superrare3 *Superrare3Session) TokenURI(_tokenId *big.Int) (string, error) {
	return _Superrare3.Contract.TokenURI(&_Superrare3.CallOpts, _tokenId)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 _tokenId) view returns(string)
func (_Superrare3 *Superrare3CallerSession) TokenURI(_tokenId *big.Int) (string, error) {
	return _Superrare3.Contract.TokenURI(&_Superrare3.CallOpts, _tokenId)
}

// TokensOf is a free data retrieval call binding the contract method 0x5a3f2672.
//
// Solidity: function tokensOf(address _owner) view returns(uint256[])
func (_Superrare3 *Superrare3Caller) TokensOf(opts *bind.CallOpts, _owner common.Address) ([]*big.Int, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "tokensOf", _owner)

	if err != nil {
		return *new([]*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new([]*big.Int)).(*[]*big.Int)

	return out0, err

}

// TokensOf is a free data retrieval call binding the contract method 0x5a3f2672.
//
// Solidity: function tokensOf(address _owner) view returns(uint256[])
func (_Superrare3 *Superrare3Session) TokensOf(_owner common.Address) ([]*big.Int, error) {
	return _Superrare3.Contract.TokensOf(&_Superrare3.CallOpts, _owner)
}

// TokensOf is a free data retrieval call binding the contract method 0x5a3f2672.
//
// Solidity: function tokensOf(address _owner) view returns(uint256[])
func (_Superrare3 *Superrare3CallerSession) TokensOf(_owner common.Address) ([]*big.Int, error) {
	return _Superrare3.Contract.TokensOf(&_Superrare3.CallOpts, _owner)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_Superrare3 *Superrare3Caller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Superrare3.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_Superrare3 *Superrare3Session) TotalSupply() (*big.Int, error) {
	return _Superrare3.Contract.TotalSupply(&_Superrare3.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_Superrare3 *Superrare3CallerSession) TotalSupply() (*big.Int, error) {
	return _Superrare3.Contract.TotalSupply(&_Superrare3.CallOpts)
}

// AcceptBid is a paid mutator transaction binding the contract method 0x2b1fd58a.
//
// Solidity: function acceptBid(uint256 _tokenId) returns()
func (_Superrare3 *Superrare3Transactor) AcceptBid(opts *bind.TransactOpts, _tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "acceptBid", _tokenId)
}

// AcceptBid is a paid mutator transaction binding the contract method 0x2b1fd58a.
//
// Solidity: function acceptBid(uint256 _tokenId) returns()
func (_Superrare3 *Superrare3Session) AcceptBid(_tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.AcceptBid(&_Superrare3.TransactOpts, _tokenId)
}

// AcceptBid is a paid mutator transaction binding the contract method 0x2b1fd58a.
//
// Solidity: function acceptBid(uint256 _tokenId) returns()
func (_Superrare3 *Superrare3TransactorSession) AcceptBid(_tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.AcceptBid(&_Superrare3.TransactOpts, _tokenId)
}

// AddNewToken is a paid mutator transaction binding the contract method 0xd9856c21.
//
// Solidity: function addNewToken(string _uri) returns()
func (_Superrare3 *Superrare3Transactor) AddNewToken(opts *bind.TransactOpts, _uri string) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "addNewToken", _uri)
}

// AddNewToken is a paid mutator transaction binding the contract method 0xd9856c21.
//
// Solidity: function addNewToken(string _uri) returns()
func (_Superrare3 *Superrare3Session) AddNewToken(_uri string) (*types.Transaction, error) {
	return _Superrare3.Contract.AddNewToken(&_Superrare3.TransactOpts, _uri)
}

// AddNewToken is a paid mutator transaction binding the contract method 0xd9856c21.
//
// Solidity: function addNewToken(string _uri) returns()
func (_Superrare3 *Superrare3TransactorSession) AddNewToken(_uri string) (*types.Transaction, error) {
	return _Superrare3.Contract.AddNewToken(&_Superrare3.TransactOpts, _uri)
}

// AddNewTokenWithEditions is a paid mutator transaction binding the contract method 0x019871e9.
//
// Solidity: function addNewTokenWithEditions(string _uri, uint256 _editions, uint256 _salePrice) returns()
func (_Superrare3 *Superrare3Transactor) AddNewTokenWithEditions(opts *bind.TransactOpts, _uri string, _editions *big.Int, _salePrice *big.Int) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "addNewTokenWithEditions", _uri, _editions, _salePrice)
}

// AddNewTokenWithEditions is a paid mutator transaction binding the contract method 0x019871e9.
//
// Solidity: function addNewTokenWithEditions(string _uri, uint256 _editions, uint256 _salePrice) returns()
func (_Superrare3 *Superrare3Session) AddNewTokenWithEditions(_uri string, _editions *big.Int, _salePrice *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.AddNewTokenWithEditions(&_Superrare3.TransactOpts, _uri, _editions, _salePrice)
}

// AddNewTokenWithEditions is a paid mutator transaction binding the contract method 0x019871e9.
//
// Solidity: function addNewTokenWithEditions(string _uri, uint256 _editions, uint256 _salePrice) returns()
func (_Superrare3 *Superrare3TransactorSession) AddNewTokenWithEditions(_uri string, _editions *big.Int, _salePrice *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.AddNewTokenWithEditions(&_Superrare3.TransactOpts, _uri, _editions, _salePrice)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address _to, uint256 _tokenId) returns()
func (_Superrare3 *Superrare3Transactor) Approve(opts *bind.TransactOpts, _to common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "approve", _to, _tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address _to, uint256 _tokenId) returns()
func (_Superrare3 *Superrare3Session) Approve(_to common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.Approve(&_Superrare3.TransactOpts, _to, _tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address _to, uint256 _tokenId) returns()
func (_Superrare3 *Superrare3TransactorSession) Approve(_to common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.Approve(&_Superrare3.TransactOpts, _to, _tokenId)
}

// Bid is a paid mutator transaction binding the contract method 0x454a2ab3.
//
// Solidity: function bid(uint256 _tokenId) payable returns()
func (_Superrare3 *Superrare3Transactor) Bid(opts *bind.TransactOpts, _tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "bid", _tokenId)
}

// Bid is a paid mutator transaction binding the contract method 0x454a2ab3.
//
// Solidity: function bid(uint256 _tokenId) payable returns()
func (_Superrare3 *Superrare3Session) Bid(_tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.Bid(&_Superrare3.TransactOpts, _tokenId)
}

// Bid is a paid mutator transaction binding the contract method 0x454a2ab3.
//
// Solidity: function bid(uint256 _tokenId) payable returns()
func (_Superrare3 *Superrare3TransactorSession) Bid(_tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.Bid(&_Superrare3.TransactOpts, _tokenId)
}

// Buy is a paid mutator transaction binding the contract method 0xd96a094a.
//
// Solidity: function buy(uint256 _tokenId) payable returns()
func (_Superrare3 *Superrare3Transactor) Buy(opts *bind.TransactOpts, _tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "buy", _tokenId)
}

// Buy is a paid mutator transaction binding the contract method 0xd96a094a.
//
// Solidity: function buy(uint256 _tokenId) payable returns()
func (_Superrare3 *Superrare3Session) Buy(_tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.Buy(&_Superrare3.TransactOpts, _tokenId)
}

// Buy is a paid mutator transaction binding the contract method 0xd96a094a.
//
// Solidity: function buy(uint256 _tokenId) payable returns()
func (_Superrare3 *Superrare3TransactorSession) Buy(_tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.Buy(&_Superrare3.TransactOpts, _tokenId)
}

// CancelBid is a paid mutator transaction binding the contract method 0x9703ef35.
//
// Solidity: function cancelBid(uint256 _tokenId) returns()
func (_Superrare3 *Superrare3Transactor) CancelBid(opts *bind.TransactOpts, _tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "cancelBid", _tokenId)
}

// CancelBid is a paid mutator transaction binding the contract method 0x9703ef35.
//
// Solidity: function cancelBid(uint256 _tokenId) returns()
func (_Superrare3 *Superrare3Session) CancelBid(_tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.CancelBid(&_Superrare3.TransactOpts, _tokenId)
}

// CancelBid is a paid mutator transaction binding the contract method 0x9703ef35.
//
// Solidity: function cancelBid(uint256 _tokenId) returns()
func (_Superrare3 *Superrare3TransactorSession) CancelBid(_tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.CancelBid(&_Superrare3.TransactOpts, _tokenId)
}

// SetCreatorPercentage is a paid mutator transaction binding the contract method 0xc0ec93e7.
//
// Solidity: function setCreatorPercentage(uint256 _percentage) returns()
func (_Superrare3 *Superrare3Transactor) SetCreatorPercentage(opts *bind.TransactOpts, _percentage *big.Int) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "setCreatorPercentage", _percentage)
}

// SetCreatorPercentage is a paid mutator transaction binding the contract method 0xc0ec93e7.
//
// Solidity: function setCreatorPercentage(uint256 _percentage) returns()
func (_Superrare3 *Superrare3Session) SetCreatorPercentage(_percentage *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.SetCreatorPercentage(&_Superrare3.TransactOpts, _percentage)
}

// SetCreatorPercentage is a paid mutator transaction binding the contract method 0xc0ec93e7.
//
// Solidity: function setCreatorPercentage(uint256 _percentage) returns()
func (_Superrare3 *Superrare3TransactorSession) SetCreatorPercentage(_percentage *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.SetCreatorPercentage(&_Superrare3.TransactOpts, _percentage)
}

// SetMaintainerPercentage is a paid mutator transaction binding the contract method 0x5c68a557.
//
// Solidity: function setMaintainerPercentage(uint256 _percentage) returns()
func (_Superrare3 *Superrare3Transactor) SetMaintainerPercentage(opts *bind.TransactOpts, _percentage *big.Int) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "setMaintainerPercentage", _percentage)
}

// SetMaintainerPercentage is a paid mutator transaction binding the contract method 0x5c68a557.
//
// Solidity: function setMaintainerPercentage(uint256 _percentage) returns()
func (_Superrare3 *Superrare3Session) SetMaintainerPercentage(_percentage *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.SetMaintainerPercentage(&_Superrare3.TransactOpts, _percentage)
}

// SetMaintainerPercentage is a paid mutator transaction binding the contract method 0x5c68a557.
//
// Solidity: function setMaintainerPercentage(uint256 _percentage) returns()
func (_Superrare3 *Superrare3TransactorSession) SetMaintainerPercentage(_percentage *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.SetMaintainerPercentage(&_Superrare3.TransactOpts, _percentage)
}

// SetSalePrice is a paid mutator transaction binding the contract method 0x053992c5.
//
// Solidity: function setSalePrice(uint256 _tokenId, uint256 _salePrice) returns()
func (_Superrare3 *Superrare3Transactor) SetSalePrice(opts *bind.TransactOpts, _tokenId *big.Int, _salePrice *big.Int) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "setSalePrice", _tokenId, _salePrice)
}

// SetSalePrice is a paid mutator transaction binding the contract method 0x053992c5.
//
// Solidity: function setSalePrice(uint256 _tokenId, uint256 _salePrice) returns()
func (_Superrare3 *Superrare3Session) SetSalePrice(_tokenId *big.Int, _salePrice *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.SetSalePrice(&_Superrare3.TransactOpts, _tokenId, _salePrice)
}

// SetSalePrice is a paid mutator transaction binding the contract method 0x053992c5.
//
// Solidity: function setSalePrice(uint256 _tokenId, uint256 _salePrice) returns()
func (_Superrare3 *Superrare3TransactorSession) SetSalePrice(_tokenId *big.Int, _salePrice *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.SetSalePrice(&_Superrare3.TransactOpts, _tokenId, _salePrice)
}

// TakeOwnership is a paid mutator transaction binding the contract method 0xb2e6ceeb.
//
// Solidity: function takeOwnership(uint256 _tokenId) returns()
func (_Superrare3 *Superrare3Transactor) TakeOwnership(opts *bind.TransactOpts, _tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "takeOwnership", _tokenId)
}

// TakeOwnership is a paid mutator transaction binding the contract method 0xb2e6ceeb.
//
// Solidity: function takeOwnership(uint256 _tokenId) returns()
func (_Superrare3 *Superrare3Session) TakeOwnership(_tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.TakeOwnership(&_Superrare3.TransactOpts, _tokenId)
}

// TakeOwnership is a paid mutator transaction binding the contract method 0xb2e6ceeb.
//
// Solidity: function takeOwnership(uint256 _tokenId) returns()
func (_Superrare3 *Superrare3TransactorSession) TakeOwnership(_tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.TakeOwnership(&_Superrare3.TransactOpts, _tokenId)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address _to, uint256 _tokenId) returns()
func (_Superrare3 *Superrare3Transactor) Transfer(opts *bind.TransactOpts, _to common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "transfer", _to, _tokenId)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address _to, uint256 _tokenId) returns()
func (_Superrare3 *Superrare3Session) Transfer(_to common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.Transfer(&_Superrare3.TransactOpts, _to, _tokenId)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address _to, uint256 _tokenId) returns()
func (_Superrare3 *Superrare3TransactorSession) Transfer(_to common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Superrare3.Contract.Transfer(&_Superrare3.TransactOpts, _to, _tokenId)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Superrare3 *Superrare3Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Superrare3 *Superrare3Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Superrare3.Contract.TransferOwnership(&_Superrare3.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Superrare3 *Superrare3TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Superrare3.Contract.TransferOwnership(&_Superrare3.TransactOpts, newOwner)
}

// WhitelistCreator is a paid mutator transaction binding the contract method 0x62f11dd2.
//
// Solidity: function whitelistCreator(address _creator) returns()
func (_Superrare3 *Superrare3Transactor) WhitelistCreator(opts *bind.TransactOpts, _creator common.Address) (*types.Transaction, error) {
	return _Superrare3.contract.Transact(opts, "whitelistCreator", _creator)
}

// WhitelistCreator is a paid mutator transaction binding the contract method 0x62f11dd2.
//
// Solidity: function whitelistCreator(address _creator) returns()
func (_Superrare3 *Superrare3Session) WhitelistCreator(_creator common.Address) (*types.Transaction, error) {
	return _Superrare3.Contract.WhitelistCreator(&_Superrare3.TransactOpts, _creator)
}

// WhitelistCreator is a paid mutator transaction binding the contract method 0x62f11dd2.
//
// Solidity: function whitelistCreator(address _creator) returns()
func (_Superrare3 *Superrare3TransactorSession) WhitelistCreator(_creator common.Address) (*types.Transaction, error) {
	return _Superrare3.Contract.WhitelistCreator(&_Superrare3.TransactOpts, _creator)
}

// Superrare3AcceptBidIterator is returned from FilterAcceptBid and is used to iterate over the raw logs and unpacked data for AcceptBid events raised by the Superrare3 contract.
type Superrare3AcceptBidIterator struct {
	Event *Superrare3AcceptBid // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Superrare3AcceptBidIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Superrare3AcceptBid)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Superrare3AcceptBid)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Superrare3AcceptBidIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Superrare3AcceptBidIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Superrare3AcceptBid represents a AcceptBid event raised by the Superrare3 contract.
type Superrare3AcceptBid struct {
	Bidder  common.Address
	Seller  common.Address
	Amount  *big.Int
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterAcceptBid is a free log retrieval operation binding the contract event 0xd6deddb2e105b46d4644d24aac8c58493a0f107e7973b2fe8d8fa7931a2912be.
//
// Solidity: event AcceptBid(address indexed _bidder, address indexed _seller, uint256 _amount, uint256 indexed _tokenId)
func (_Superrare3 *Superrare3Filterer) FilterAcceptBid(opts *bind.FilterOpts, _bidder []common.Address, _seller []common.Address, _tokenId []*big.Int) (*Superrare3AcceptBidIterator, error) {

	var _bidderRule []interface{}
	for _, _bidderItem := range _bidder {
		_bidderRule = append(_bidderRule, _bidderItem)
	}
	var _sellerRule []interface{}
	for _, _sellerItem := range _seller {
		_sellerRule = append(_sellerRule, _sellerItem)
	}

	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Superrare3.contract.FilterLogs(opts, "AcceptBid", _bidderRule, _sellerRule, _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Superrare3AcceptBidIterator{contract: _Superrare3.contract, event: "AcceptBid", logs: logs, sub: sub}, nil
}

// WatchAcceptBid is a free log subscription operation binding the contract event 0xd6deddb2e105b46d4644d24aac8c58493a0f107e7973b2fe8d8fa7931a2912be.
//
// Solidity: event AcceptBid(address indexed _bidder, address indexed _seller, uint256 _amount, uint256 indexed _tokenId)
func (_Superrare3 *Superrare3Filterer) WatchAcceptBid(opts *bind.WatchOpts, sink chan<- *Superrare3AcceptBid, _bidder []common.Address, _seller []common.Address, _tokenId []*big.Int) (event.Subscription, error) {

	var _bidderRule []interface{}
	for _, _bidderItem := range _bidder {
		_bidderRule = append(_bidderRule, _bidderItem)
	}
	var _sellerRule []interface{}
	for _, _sellerItem := range _seller {
		_sellerRule = append(_sellerRule, _sellerItem)
	}

	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Superrare3.contract.WatchLogs(opts, "AcceptBid", _bidderRule, _sellerRule, _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Superrare3AcceptBid)
				if err := _Superrare3.contract.UnpackLog(event, "AcceptBid", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAcceptBid is a log parse operation binding the contract event 0xd6deddb2e105b46d4644d24aac8c58493a0f107e7973b2fe8d8fa7931a2912be.
//
// Solidity: event AcceptBid(address indexed _bidder, address indexed _seller, uint256 _amount, uint256 indexed _tokenId)
func (_Superrare3 *Superrare3Filterer) ParseAcceptBid(log types.Log) (*Superrare3AcceptBid, error) {
	event := new(Superrare3AcceptBid)
	if err := _Superrare3.contract.UnpackLog(event, "AcceptBid", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Superrare3ApprovalIterator is returned from FilterApproval and is used to iterate over the raw logs and unpacked data for Approval events raised by the Superrare3 contract.
type Superrare3ApprovalIterator struct {
	Event *Superrare3Approval // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Superrare3ApprovalIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Superrare3Approval)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Superrare3Approval)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Superrare3ApprovalIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Superrare3ApprovalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Superrare3Approval represents a Approval event raised by the Superrare3 contract.
type Superrare3Approval struct {
	Owner    common.Address
	Approved common.Address
	TokenId  *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApproval is a free log retrieval operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed _owner, address indexed _approved, uint256 _tokenId)
func (_Superrare3 *Superrare3Filterer) FilterApproval(opts *bind.FilterOpts, _owner []common.Address, _approved []common.Address) (*Superrare3ApprovalIterator, error) {

	var _ownerRule []interface{}
	for _, _ownerItem := range _owner {
		_ownerRule = append(_ownerRule, _ownerItem)
	}
	var _approvedRule []interface{}
	for _, _approvedItem := range _approved {
		_approvedRule = append(_approvedRule, _approvedItem)
	}

	logs, sub, err := _Superrare3.contract.FilterLogs(opts, "Approval", _ownerRule, _approvedRule)
	if err != nil {
		return nil, err
	}
	return &Superrare3ApprovalIterator{contract: _Superrare3.contract, event: "Approval", logs: logs, sub: sub}, nil
}

// WatchApproval is a free log subscription operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed _owner, address indexed _approved, uint256 _tokenId)
func (_Superrare3 *Superrare3Filterer) WatchApproval(opts *bind.WatchOpts, sink chan<- *Superrare3Approval, _owner []common.Address, _approved []common.Address) (event.Subscription, error) {

	var _ownerRule []interface{}
	for _, _ownerItem := range _owner {
		_ownerRule = append(_ownerRule, _ownerItem)
	}
	var _approvedRule []interface{}
	for _, _approvedItem := range _approved {
		_approvedRule = append(_approvedRule, _approvedItem)
	}

	logs, sub, err := _Superrare3.contract.WatchLogs(opts, "Approval", _ownerRule, _approvedRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Superrare3Approval)
				if err := _Superrare3.contract.UnpackLog(event, "Approval", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApproval is a log parse operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed _owner, address indexed _approved, uint256 _tokenId)
func (_Superrare3 *Superrare3Filterer) ParseApproval(log types.Log) (*Superrare3Approval, error) {
	event := new(Superrare3Approval)
	if err := _Superrare3.contract.UnpackLog(event, "Approval", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Superrare3BidIterator is returned from FilterBid and is used to iterate over the raw logs and unpacked data for Bid events raised by the Superrare3 contract.
type Superrare3BidIterator struct {
	Event *Superrare3Bid // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Superrare3BidIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Superrare3Bid)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Superrare3Bid)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Superrare3BidIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Superrare3BidIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Superrare3Bid represents a Bid event raised by the Superrare3 contract.
type Superrare3Bid struct {
	Bidder  common.Address
	Amount  *big.Int
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterBid is a free log retrieval operation binding the contract event 0x19421268847f42dd61705778018ddfc43bcdce8517e7a630acb12f122c709481.
//
// Solidity: event Bid(address indexed _bidder, uint256 indexed _amount, uint256 indexed _tokenId)
func (_Superrare3 *Superrare3Filterer) FilterBid(opts *bind.FilterOpts, _bidder []common.Address, _amount []*big.Int, _tokenId []*big.Int) (*Superrare3BidIterator, error) {

	var _bidderRule []interface{}
	for _, _bidderItem := range _bidder {
		_bidderRule = append(_bidderRule, _bidderItem)
	}
	var _amountRule []interface{}
	for _, _amountItem := range _amount {
		_amountRule = append(_amountRule, _amountItem)
	}
	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Superrare3.contract.FilterLogs(opts, "Bid", _bidderRule, _amountRule, _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Superrare3BidIterator{contract: _Superrare3.contract, event: "Bid", logs: logs, sub: sub}, nil
}

// WatchBid is a free log subscription operation binding the contract event 0x19421268847f42dd61705778018ddfc43bcdce8517e7a630acb12f122c709481.
//
// Solidity: event Bid(address indexed _bidder, uint256 indexed _amount, uint256 indexed _tokenId)
func (_Superrare3 *Superrare3Filterer) WatchBid(opts *bind.WatchOpts, sink chan<- *Superrare3Bid, _bidder []common.Address, _amount []*big.Int, _tokenId []*big.Int) (event.Subscription, error) {

	var _bidderRule []interface{}
	for _, _bidderItem := range _bidder {
		_bidderRule = append(_bidderRule, _bidderItem)
	}
	var _amountRule []interface{}
	for _, _amountItem := range _amount {
		_amountRule = append(_amountRule, _amountItem)
	}
	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Superrare3.contract.WatchLogs(opts, "Bid", _bidderRule, _amountRule, _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Superrare3Bid)
				if err := _Superrare3.contract.UnpackLog(event, "Bid", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBid is a log parse operation binding the contract event 0x19421268847f42dd61705778018ddfc43bcdce8517e7a630acb12f122c709481.
//
// Solidity: event Bid(address indexed _bidder, uint256 indexed _amount, uint256 indexed _tokenId)
func (_Superrare3 *Superrare3Filterer) ParseBid(log types.Log) (*Superrare3Bid, error) {
	event := new(Superrare3Bid)
	if err := _Superrare3.contract.UnpackLog(event, "Bid", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Superrare3CancelBidIterator is returned from FilterCancelBid and is used to iterate over the raw logs and unpacked data for CancelBid events raised by the Superrare3 contract.
type Superrare3CancelBidIterator struct {
	Event *Superrare3CancelBid // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Superrare3CancelBidIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Superrare3CancelBid)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Superrare3CancelBid)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Superrare3CancelBidIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Superrare3CancelBidIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Superrare3CancelBid represents a CancelBid event raised by the Superrare3 contract.
type Superrare3CancelBid struct {
	Bidder  common.Address
	Amount  *big.Int
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterCancelBid is a free log retrieval operation binding the contract event 0x09dcebe16a733e22cc47e4959c50d4f21624d9f1815db32c2e439fbbd7b3eda0.
//
// Solidity: event CancelBid(address indexed _bidder, uint256 indexed _amount, uint256 indexed _tokenId)
func (_Superrare3 *Superrare3Filterer) FilterCancelBid(opts *bind.FilterOpts, _bidder []common.Address, _amount []*big.Int, _tokenId []*big.Int) (*Superrare3CancelBidIterator, error) {

	var _bidderRule []interface{}
	for _, _bidderItem := range _bidder {
		_bidderRule = append(_bidderRule, _bidderItem)
	}
	var _amountRule []interface{}
	for _, _amountItem := range _amount {
		_amountRule = append(_amountRule, _amountItem)
	}
	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Superrare3.contract.FilterLogs(opts, "CancelBid", _bidderRule, _amountRule, _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Superrare3CancelBidIterator{contract: _Superrare3.contract, event: "CancelBid", logs: logs, sub: sub}, nil
}

// WatchCancelBid is a free log subscription operation binding the contract event 0x09dcebe16a733e22cc47e4959c50d4f21624d9f1815db32c2e439fbbd7b3eda0.
//
// Solidity: event CancelBid(address indexed _bidder, uint256 indexed _amount, uint256 indexed _tokenId)
func (_Superrare3 *Superrare3Filterer) WatchCancelBid(opts *bind.WatchOpts, sink chan<- *Superrare3CancelBid, _bidder []common.Address, _amount []*big.Int, _tokenId []*big.Int) (event.Subscription, error) {

	var _bidderRule []interface{}
	for _, _bidderItem := range _bidder {
		_bidderRule = append(_bidderRule, _bidderItem)
	}
	var _amountRule []interface{}
	for _, _amountItem := range _amount {
		_amountRule = append(_amountRule, _amountItem)
	}
	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Superrare3.contract.WatchLogs(opts, "CancelBid", _bidderRule, _amountRule, _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Superrare3CancelBid)
				if err := _Superrare3.contract.UnpackLog(event, "CancelBid", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCancelBid is a log parse operation binding the contract event 0x09dcebe16a733e22cc47e4959c50d4f21624d9f1815db32c2e439fbbd7b3eda0.
//
// Solidity: event CancelBid(address indexed _bidder, uint256 indexed _amount, uint256 indexed _tokenId)
func (_Superrare3 *Superrare3Filterer) ParseCancelBid(log types.Log) (*Superrare3CancelBid, error) {
	event := new(Superrare3CancelBid)
	if err := _Superrare3.contract.UnpackLog(event, "CancelBid", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Superrare3OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Superrare3 contract.
type Superrare3OwnershipTransferredIterator struct {
	Event *Superrare3OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Superrare3OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Superrare3OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Superrare3OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Superrare3OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Superrare3OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Superrare3OwnershipTransferred represents a OwnershipTransferred event raised by the Superrare3 contract.
type Superrare3OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Superrare3 *Superrare3Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Superrare3OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Superrare3.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Superrare3OwnershipTransferredIterator{contract: _Superrare3.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Superrare3 *Superrare3Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Superrare3OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Superrare3.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Superrare3OwnershipTransferred)
				if err := _Superrare3.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Superrare3 *Superrare3Filterer) ParseOwnershipTransferred(log types.Log) (*Superrare3OwnershipTransferred, error) {
	event := new(Superrare3OwnershipTransferred)
	if err := _Superrare3.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Superrare3SalePriceSetIterator is returned from FilterSalePriceSet and is used to iterate over the raw logs and unpacked data for SalePriceSet events raised by the Superrare3 contract.
type Superrare3SalePriceSetIterator struct {
	Event *Superrare3SalePriceSet // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Superrare3SalePriceSetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Superrare3SalePriceSet)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Superrare3SalePriceSet)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Superrare3SalePriceSetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Superrare3SalePriceSetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Superrare3SalePriceSet represents a SalePriceSet event raised by the Superrare3 contract.
type Superrare3SalePriceSet struct {
	TokenId *big.Int
	Price   *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterSalePriceSet is a free log retrieval operation binding the contract event 0xe23ea816dce6d7f5c0b85cbd597e7c3b97b2453791152c0b94e5e5c5f314d2f0.
//
// Solidity: event SalePriceSet(uint256 indexed _tokenId, uint256 indexed _price)
func (_Superrare3 *Superrare3Filterer) FilterSalePriceSet(opts *bind.FilterOpts, _tokenId []*big.Int, _price []*big.Int) (*Superrare3SalePriceSetIterator, error) {

	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}
	var _priceRule []interface{}
	for _, _priceItem := range _price {
		_priceRule = append(_priceRule, _priceItem)
	}

	logs, sub, err := _Superrare3.contract.FilterLogs(opts, "SalePriceSet", _tokenIdRule, _priceRule)
	if err != nil {
		return nil, err
	}
	return &Superrare3SalePriceSetIterator{contract: _Superrare3.contract, event: "SalePriceSet", logs: logs, sub: sub}, nil
}

// WatchSalePriceSet is a free log subscription operation binding the contract event 0xe23ea816dce6d7f5c0b85cbd597e7c3b97b2453791152c0b94e5e5c5f314d2f0.
//
// Solidity: event SalePriceSet(uint256 indexed _tokenId, uint256 indexed _price)
func (_Superrare3 *Superrare3Filterer) WatchSalePriceSet(opts *bind.WatchOpts, sink chan<- *Superrare3SalePriceSet, _tokenId []*big.Int, _price []*big.Int) (event.Subscription, error) {

	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}
	var _priceRule []interface{}
	for _, _priceItem := range _price {
		_priceRule = append(_priceRule, _priceItem)
	}

	logs, sub, err := _Superrare3.contract.WatchLogs(opts, "SalePriceSet", _tokenIdRule, _priceRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Superrare3SalePriceSet)
				if err := _Superrare3.contract.UnpackLog(event, "SalePriceSet", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseSalePriceSet is a log parse operation binding the contract event 0xe23ea816dce6d7f5c0b85cbd597e7c3b97b2453791152c0b94e5e5c5f314d2f0.
//
// Solidity: event SalePriceSet(uint256 indexed _tokenId, uint256 indexed _price)
func (_Superrare3 *Superrare3Filterer) ParseSalePriceSet(log types.Log) (*Superrare3SalePriceSet, error) {
	event := new(Superrare3SalePriceSet)
	if err := _Superrare3.contract.UnpackLog(event, "SalePriceSet", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Superrare3SoldIterator is returned from FilterSold and is used to iterate over the raw logs and unpacked data for Sold events raised by the Superrare3 contract.
type Superrare3SoldIterator struct {
	Event *Superrare3Sold // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Superrare3SoldIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Superrare3Sold)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Superrare3Sold)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Superrare3SoldIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Superrare3SoldIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Superrare3Sold represents a Sold event raised by the Superrare3 contract.
type Superrare3Sold struct {
	Buyer   common.Address
	Seller  common.Address
	Amount  *big.Int
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterSold is a free log retrieval operation binding the contract event 0x16dd16959a056953a63cf14bf427881e762e54f03d86b864efea8238dd3b822f.
//
// Solidity: event Sold(address indexed _buyer, address indexed _seller, uint256 _amount, uint256 indexed _tokenId)
func (_Superrare3 *Superrare3Filterer) FilterSold(opts *bind.FilterOpts, _buyer []common.Address, _seller []common.Address, _tokenId []*big.Int) (*Superrare3SoldIterator, error) {

	var _buyerRule []interface{}
	for _, _buyerItem := range _buyer {
		_buyerRule = append(_buyerRule, _buyerItem)
	}
	var _sellerRule []interface{}
	for _, _sellerItem := range _seller {
		_sellerRule = append(_sellerRule, _sellerItem)
	}

	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Superrare3.contract.FilterLogs(opts, "Sold", _buyerRule, _sellerRule, _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Superrare3SoldIterator{contract: _Superrare3.contract, event: "Sold", logs: logs, sub: sub}, nil
}

// WatchSold is a free log subscription operation binding the contract event 0x16dd16959a056953a63cf14bf427881e762e54f03d86b864efea8238dd3b822f.
//
// Solidity: event Sold(address indexed _buyer, address indexed _seller, uint256 _amount, uint256 indexed _tokenId)
func (_Superrare3 *Superrare3Filterer) WatchSold(opts *bind.WatchOpts, sink chan<- *Superrare3Sold, _buyer []common.Address, _seller []common.Address, _tokenId []*big.Int) (event.Subscription, error) {

	var _buyerRule []interface{}
	for _, _buyerItem := range _buyer {
		_buyerRule = append(_buyerRule, _buyerItem)
	}
	var _sellerRule []interface{}
	for _, _sellerItem := range _seller {
		_sellerRule = append(_sellerRule, _sellerItem)
	}

	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Superrare3.contract.WatchLogs(opts, "Sold", _buyerRule, _sellerRule, _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Superrare3Sold)
				if err := _Superrare3.contract.UnpackLog(event, "Sold", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseSold is a log parse operation binding the contract event 0x16dd16959a056953a63cf14bf427881e762e54f03d86b864efea8238dd3b822f.
//
// Solidity: event Sold(address indexed _buyer, address indexed _seller, uint256 _amount, uint256 indexed _tokenId)
func (_Superrare3 *Superrare3Filterer) ParseSold(log types.Log) (*Superrare3Sold, error) {
	event := new(Superrare3Sold)
	if err := _Superrare3.contract.UnpackLog(event, "Sold", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Superrare3TransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the Superrare3 contract.
type Superrare3TransferIterator struct {
	Event *Superrare3Transfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Superrare3TransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Superrare3Transfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Superrare3Transfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Superrare3TransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Superrare3TransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Superrare3Transfer represents a Transfer event raised by the Superrare3 contract.
type Superrare3Transfer struct {
	From    common.Address
	To      common.Address
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed _from, address indexed _to, uint256 _tokenId)
func (_Superrare3 *Superrare3Filterer) FilterTransfer(opts *bind.FilterOpts, _from []common.Address, _to []common.Address) (*Superrare3TransferIterator, error) {

	var _fromRule []interface{}
	for _, _fromItem := range _from {
		_fromRule = append(_fromRule, _fromItem)
	}
	var _toRule []interface{}
	for _, _toItem := range _to {
		_toRule = append(_toRule, _toItem)
	}

	logs, sub, err := _Superrare3.contract.FilterLogs(opts, "Transfer", _fromRule, _toRule)
	if err != nil {
		return nil, err
	}
	return &Superrare3TransferIterator{contract: _Superrare3.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed _from, address indexed _to, uint256 _tokenId)
func (_Superrare3 *Superrare3Filterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *Superrare3Transfer, _from []common.Address, _to []common.Address) (event.Subscription, error) {

	var _fromRule []interface{}
	for _, _fromItem := range _from {
		_fromRule = append(_fromRule, _fromItem)
	}
	var _toRule []interface{}
	for _, _toItem := range _to {
		_toRule = append(_toRule, _toItem)
	}

	logs, sub, err := _Superrare3.contract.WatchLogs(opts, "Transfer", _fromRule, _toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Superrare3Transfer)
				if err := _Superrare3.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed _from, address indexed _to, uint256 _tokenId)
func (_Superrare3 *Superrare3Filterer) ParseTransfer(log types.Log) (*Superrare3Transfer, error) {
	event := new(Superrare3Transfer)
	if err := _Superrare3.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Superrare3WhitelistCreatorIterator is returned from FilterWhitelistCreator and is used to iterate over the raw logs and unpacked data for WhitelistCreator events raised by the Superrare3 contract.
type Superrare3WhitelistCreatorIterator struct {
	Event *Superrare3WhitelistCreator // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Superrare3WhitelistCreatorIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Superrare3WhitelistCreator)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Superrare3WhitelistCreator)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Superrare3WhitelistCreatorIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Superrare3WhitelistCreatorIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Superrare3WhitelistCreator represents a WhitelistCreator event raised by the Superrare3 contract.
type Superrare3WhitelistCreator struct {
	Creator common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterWhitelistCreator is a free log retrieval operation binding the contract event 0x55eed0aed3ec6e015b9ad5e984675fe36c0ce3aebdcb70f467670773f19f7f8d.
//
// Solidity: event WhitelistCreator(address indexed _creator)
func (_Superrare3 *Superrare3Filterer) FilterWhitelistCreator(opts *bind.FilterOpts, _creator []common.Address) (*Superrare3WhitelistCreatorIterator, error) {

	var _creatorRule []interface{}
	for _, _creatorItem := range _creator {
		_creatorRule = append(_creatorRule, _creatorItem)
	}

	logs, sub, err := _Superrare3.contract.FilterLogs(opts, "WhitelistCreator", _creatorRule)
	if err != nil {
		return nil, err
	}
	return &Superrare3WhitelistCreatorIterator{contract: _Superrare3.contract, event: "WhitelistCreator", logs: logs, sub: sub}, nil
}

// WatchWhitelistCreator is a free log subscription operation binding the contract event 0x55eed0aed3ec6e015b9ad5e984675fe36c0ce3aebdcb70f467670773f19f7f8d.
//
// Solidity: event WhitelistCreator(address indexed _creator)
func (_Superrare3 *Superrare3Filterer) WatchWhitelistCreator(opts *bind.WatchOpts, sink chan<- *Superrare3WhitelistCreator, _creator []common.Address) (event.Subscription, error) {

	var _creatorRule []interface{}
	for _, _creatorItem := range _creator {
		_creatorRule = append(_creatorRule, _creatorItem)
	}

	logs, sub, err := _Superrare3.contract.WatchLogs(opts, "WhitelistCreator", _creatorRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Superrare3WhitelistCreator)
				if err := _Superrare3.contract.UnpackLog(event, "WhitelistCreator", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseWhitelistCreator is a log parse operation binding the contract event 0x55eed0aed3ec6e015b9ad5e984675fe36c0ce3aebdcb70f467670773f19f7f8d.
//
// Solidity: event WhitelistCreator(address indexed _creator)
func (_Superrare3 *Superrare3Filterer) ParseWhitelistCreator(log types.Log) (*Superrare3WhitelistCreator, error) {
	event := new(Superrare3WhitelistCreator)
	if err := _Superrare3.contract.UnpackLog(event, "WhitelistCreator", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
