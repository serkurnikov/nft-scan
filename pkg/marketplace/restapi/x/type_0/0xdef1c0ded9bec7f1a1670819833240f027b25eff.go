// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package x_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// X0MetaData contains all meta data concerning the X0 contract.
var X0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"stateMutability\":\"payable\",\"type\":\"fallback\"},{\"inputs\":[{\"internalType\":\"bytes4\",\"name\":\"selector\",\"type\":\"bytes4\"}],\"name\":\"getFunctionImplementation\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"impl\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
}

// X0ABI is the input ABI used to generate the binding from.
// Deprecated: Use X0MetaData.ABI instead.
var X0ABI = X0MetaData.ABI

// X0 is an auto generated Go binding around an Ethereum contract.
type X0 struct {
	X0Caller     // Read-only binding to the contract
	X0Transactor // Write-only binding to the contract
	X0Filterer   // Log filterer for contract events
}

// X0Caller is an auto generated read-only Go binding around an Ethereum contract.
type X0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// X0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type X0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// X0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type X0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// X0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type X0Session struct {
	Contract     *X0               // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// X0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type X0CallerSession struct {
	Contract *X0Caller     // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// X0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type X0TransactorSession struct {
	Contract     *X0Transactor     // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// X0Raw is an auto generated low-level Go binding around an Ethereum contract.
type X0Raw struct {
	Contract *X0 // Generic contract binding to access the raw methods on
}

// X0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type X0CallerRaw struct {
	Contract *X0Caller // Generic read-only contract binding to access the raw methods on
}

// X0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type X0TransactorRaw struct {
	Contract *X0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewX0 creates a new instance of X0, bound to a specific deployed contract.
func NewX0(address common.Address, backend bind.ContractBackend) (*X0, error) {
	contract, err := bindX0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &X0{X0Caller: X0Caller{contract: contract}, X0Transactor: X0Transactor{contract: contract}, X0Filterer: X0Filterer{contract: contract}}, nil
}

// NewX0Caller creates a new read-only instance of X0, bound to a specific deployed contract.
func NewX0Caller(address common.Address, caller bind.ContractCaller) (*X0Caller, error) {
	contract, err := bindX0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &X0Caller{contract: contract}, nil
}

// NewX0Transactor creates a new write-only instance of X0, bound to a specific deployed contract.
func NewX0Transactor(address common.Address, transactor bind.ContractTransactor) (*X0Transactor, error) {
	contract, err := bindX0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &X0Transactor{contract: contract}, nil
}

// NewX0Filterer creates a new log filterer instance of X0, bound to a specific deployed contract.
func NewX0Filterer(address common.Address, filterer bind.ContractFilterer) (*X0Filterer, error) {
	contract, err := bindX0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &X0Filterer{contract: contract}, nil
}

// bindX0 binds a generic wrapper to an already deployed contract.
func bindX0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(X0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_X0 *X0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _X0.Contract.X0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_X0 *X0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _X0.Contract.X0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_X0 *X0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _X0.Contract.X0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_X0 *X0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _X0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_X0 *X0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _X0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_X0 *X0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _X0.Contract.contract.Transact(opts, method, params...)
}

// GetFunctionImplementation is a free data retrieval call binding the contract method 0x972fdd26.
//
// Solidity: function getFunctionImplementation(bytes4 selector) view returns(address impl)
func (_X0 *X0Caller) GetFunctionImplementation(opts *bind.CallOpts, selector [4]byte) (common.Address, error) {
	var out []interface{}
	err := _X0.contract.Call(opts, &out, "getFunctionImplementation", selector)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetFunctionImplementation is a free data retrieval call binding the contract method 0x972fdd26.
//
// Solidity: function getFunctionImplementation(bytes4 selector) view returns(address impl)
func (_X0 *X0Session) GetFunctionImplementation(selector [4]byte) (common.Address, error) {
	return _X0.Contract.GetFunctionImplementation(&_X0.CallOpts, selector)
}

// GetFunctionImplementation is a free data retrieval call binding the contract method 0x972fdd26.
//
// Solidity: function getFunctionImplementation(bytes4 selector) view returns(address impl)
func (_X0 *X0CallerSession) GetFunctionImplementation(selector [4]byte) (common.Address, error) {
	return _X0.Contract.GetFunctionImplementation(&_X0.CallOpts, selector)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_X0 *X0Transactor) Fallback(opts *bind.TransactOpts, calldata []byte) (*types.Transaction, error) {
	return _X0.contract.RawTransact(opts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_X0 *X0Session) Fallback(calldata []byte) (*types.Transaction, error) {
	return _X0.Contract.Fallback(&_X0.TransactOpts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_X0 *X0TransactorSession) Fallback(calldata []byte) (*types.Transaction, error) {
	return _X0.Contract.Fallback(&_X0.TransactOpts, calldata)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_X0 *X0Transactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _X0.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_X0 *X0Session) Receive() (*types.Transaction, error) {
	return _X0.Contract.Receive(&_X0.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_X0 *X0TransactorSession) Receive() (*types.Transaction, error) {
	return _X0.Contract.Receive(&_X0.TransactOpts)
}
