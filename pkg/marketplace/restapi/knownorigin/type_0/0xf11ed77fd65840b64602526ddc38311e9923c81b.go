// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package knownorigin_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Knownorigin0MetaData contains all meta data concerning the Knownorigin0 contract.
var Knownorigin0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"contractIKOAccessControlsLookup\",\"name\":\"_accessControls\",\"type\":\"address\"},{\"internalType\":\"contractIKODAV3\",\"name\":\"_koda\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_platformAccount\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"contractIERC20\",\"name\":\"_token\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"_recipient\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"AdminRecoverERC20\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"addresspayable\",\"name\":\"_recipient\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"AdminRecoverETH\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"_creator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_koCommission\",\"type\":\"uint256\"}],\"name\":\"AdminSetKoCommissionOverrideForCreator\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_koCommission\",\"type\":\"uint256\"}],\"name\":\"AdminSetKoCommissionOverrideForEdition\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"contractIKOAccessControlsLookup\",\"name\":\"_oldAddress\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"contractIKOAccessControlsLookup\",\"name\":\"_newAddress\",\"type\":\"address\"}],\"name\":\"AdminUpdateAccessControls\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_bidLockupPeriod\",\"type\":\"uint256\"}],\"name\":\"AdminUpdateBidLockupPeriod\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_minBidAmount\",\"type\":\"uint256\"}],\"name\":\"AdminUpdateMinBidAmount\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_modulo\",\"type\":\"uint256\"}],\"name\":\"AdminUpdateModulo\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"_oldAddress\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"_newAddress\",\"type\":\"address\"}],\"name\":\"AdminUpdatePlatformAccount\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_platformPrimarySaleCommission\",\"type\":\"uint256\"}],\"name\":\"AdminUpdatePlatformPrimarySaleCommission\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_reserveAuctionBidExtensionWindow\",\"type\":\"uint128\"}],\"name\":\"AdminUpdateReserveAuctionBidExtensionWindow\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_reserveAuctionLengthOnceReserveMet\",\"type\":\"uint128\"}],\"name\":\"AdminUpdateReserveAuctionLengthOnceReserveMet\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_currentOwner\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_bidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_originalBiddingEnd\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_currentBiddingEnd\",\"type\":\"uint256\"}],\"name\":\"BidPlacedOnReserveAuction\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_bidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_bid\",\"type\":\"uint128\"}],\"name\":\"BidWithdrawnFromReserveAuction\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_bidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_bid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_newBidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_newOffer\",\"type\":\"uint256\"}],\"name\":\"BidderRefunded\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_bidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_bid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_newBidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_newOffer\",\"type\":\"uint256\"}],\"name\":\"BidderRefundedFailed\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"}],\"name\":\"BuyNowDeListed\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_price\",\"type\":\"uint256\"}],\"name\":\"BuyNowPriceChanged\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_buyer\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_currentOwner\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_price\",\"type\":\"uint256\"}],\"name\":\"BuyNowPurchased\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"ConvertFromBuyNowToOffers\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_listingPrice\",\"type\":\"uint128\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"ConvertSteppedAuctionToBuyNow\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"EditionAcceptingOffer\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_bidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"EditionBidAccepted\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_bidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"EditionBidPlaced\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_bidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"EditionBidRejected\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_bidder\",\"type\":\"address\"}],\"name\":\"EditionBidWithdrawn\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_price\",\"type\":\"uint128\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"EditionConvertedFromOffersToBuyItNow\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_basePrice\",\"type\":\"uint128\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_stepPrice\",\"type\":\"uint128\"}],\"name\":\"EditionSteppedAuctionUpdated\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_buyer\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_price\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint16\",\"name\":\"_currentStep\",\"type\":\"uint16\"}],\"name\":\"EditionSteppedSaleBuy\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_basePrice\",\"type\":\"uint128\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_stepPrice\",\"type\":\"uint128\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"EditionSteppedSaleListed\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_bidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_bid\",\"type\":\"uint128\"}],\"name\":\"EmergencyBidWithdrawFromReserveAuction\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_price\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_currentOwner\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_startDate\",\"type\":\"uint256\"}],\"name\":\"ListedForBuyNow\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_reservePrice\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"ListedForReserveAuction\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Paused\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[],\"name\":\"PrimaryMarketplaceDeployed\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_listingPrice\",\"type\":\"uint128\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"ReserveAuctionConvertedToBuyItNow\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"ReserveAuctionConvertedToOffers\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_finalPrice\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_currentOwner\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_winner\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_resulter\",\"type\":\"address\"}],\"name\":\"ReserveAuctionResulted\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_reservePrice\",\"type\":\"uint256\"}],\"name\":\"ReservePriceUpdated\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Unpaused\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_offerPrice\",\"type\":\"uint256\"}],\"name\":\"acceptEditionBid\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"accessControls\",\"outputs\":[{\"internalType\":\"contractIKOAccessControlsLookup\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"}],\"name\":\"adminRejectEditionBid\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"bidLockupPeriod\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"}],\"name\":\"buyEditionToken\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_recipient\",\"type\":\"address\"}],\"name\":\"buyEditionTokenFor\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"}],\"name\":\"buyNextStep\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_buyer\",\"type\":\"address\"}],\"name\":\"buyNextStepFor\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"convertFromBuyNowToOffers\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"internalType\":\"uint128\",\"name\":\"_listingPrice\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"convertOffersToBuyItNow\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"internalType\":\"uint128\",\"name\":\"_listingPrice\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"convertReserveAuctionToBuyItNow\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"convertReserveAuctionToOffers\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"internalType\":\"uint128\",\"name\":\"_listingPrice\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"convertSteppedAuctionToListing\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"convertSteppedAuctionToOffers\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"editionOffers\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"offer\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"lockupUntil\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"editionOffersStartDate\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"editionOrTokenListings\",\"outputs\":[{\"internalType\":\"uint128\",\"name\":\"price\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"startDate\",\"type\":\"uint128\"},{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"editionOrTokenWithReserveAuctions\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint128\",\"name\":\"reservePrice\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"bid\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"startDate\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"biddingEnd\",\"type\":\"uint128\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"editionStep\",\"outputs\":[{\"internalType\":\"uint128\",\"name\":\"basePrice\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"stepPrice\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"startDate\",\"type\":\"uint128\"},{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"uint16\",\"name\":\"currentStep\",\"type\":\"uint16\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"}],\"name\":\"emergencyExitBidFromReserveAuction\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"enableEditionOffers\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"}],\"name\":\"getNextEditionSteppedPrice\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"koCommissionOverrideForCreators\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"active\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"koCommission\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"koCommissionOverrideForEditions\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"active\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"koCommission\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"koda\",\"outputs\":[{\"internalType\":\"contractIKODAV3\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_seller\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"internalType\":\"uint128\",\"name\":\"_listingPrice\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"listForBuyNow\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_creator\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"internalType\":\"uint128\",\"name\":\"_reservePrice\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"listForReserveAuction\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_creator\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"internalType\":\"uint128\",\"name\":\"_basePrice\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"_stepPrice\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"_startDate\",\"type\":\"uint128\"}],\"name\":\"listSteppedEditionAuction\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"minBidAmount\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"modulo\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"pause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"paused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"}],\"name\":\"placeBidOnReserveAuction\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_bidder\",\"type\":\"address\"}],\"name\":\"placeBidOnReserveAuctionFor\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"}],\"name\":\"placeEditionBid\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_bidder\",\"type\":\"address\"}],\"name\":\"placeEditionBidFor\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"platformAccount\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"platformPrimarySaleCommission\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contractIERC20\",\"name\":\"_token\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_recipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"recoverERC20\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"addresspayable\",\"name\":\"_recipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"recoverStuckETH\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"}],\"name\":\"rejectEditionBid\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"reserveAuctionBidExtensionWindow\",\"outputs\":[{\"internalType\":\"uint128\",\"name\":\"\",\"type\":\"uint128\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"reserveAuctionLengthOnceReserveMet\",\"outputs\":[{\"internalType\":\"uint128\",\"name\":\"\",\"type\":\"uint128\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"}],\"name\":\"resultReserveAuction\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"internalType\":\"uint128\",\"name\":\"_listingPrice\",\"type\":\"uint128\"}],\"name\":\"setBuyNowPriceListing\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_creator\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_active\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"_koCommission\",\"type\":\"uint256\"}],\"name\":\"setKoCommissionOverrideForCreator\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"_active\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"_koCommission\",\"type\":\"uint256\"}],\"name\":\"setKoCommissionOverrideForEdition\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"unpause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contractIKOAccessControlsLookup\",\"name\":\"_accessControls\",\"type\":\"address\"}],\"name\":\"updateAccessControls\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_bidLockupPeriod\",\"type\":\"uint256\"}],\"name\":\"updateBidLockupPeriod\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_minBidAmount\",\"type\":\"uint256\"}],\"name\":\"updateMinBidAmount\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_modulo\",\"type\":\"uint256\"}],\"name\":\"updateModulo\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_newPlatformAccount\",\"type\":\"address\"}],\"name\":\"updatePlatformAccount\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_platformPrimarySaleCommission\",\"type\":\"uint256\"}],\"name\":\"updatePlatformPrimarySaleCommission\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint128\",\"name\":\"_reserveAuctionBidExtensionWindow\",\"type\":\"uint128\"}],\"name\":\"updateReserveAuctionBidExtensionWindow\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint128\",\"name\":\"_reserveAuctionLengthOnceReserveMet\",\"type\":\"uint128\"}],\"name\":\"updateReserveAuctionLengthOnceReserveMet\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"},{\"internalType\":\"uint128\",\"name\":\"_reservePrice\",\"type\":\"uint128\"}],\"name\":\"updateReservePriceForReserveAuction\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"},{\"internalType\":\"uint128\",\"name\":\"_basePrice\",\"type\":\"uint128\"},{\"internalType\":\"uint128\",\"name\":\"_stepPrice\",\"type\":\"uint128\"}],\"name\":\"updateSteppedAuction\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"}],\"name\":\"withdrawBidFromReserveAuction\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_editionId\",\"type\":\"uint256\"}],\"name\":\"withdrawEditionBid\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// Knownorigin0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Knownorigin0MetaData.ABI instead.
var Knownorigin0ABI = Knownorigin0MetaData.ABI

// Knownorigin0 is an auto generated Go binding around an Ethereum contract.
type Knownorigin0 struct {
	Knownorigin0Caller     // Read-only binding to the contract
	Knownorigin0Transactor // Write-only binding to the contract
	Knownorigin0Filterer   // Log filterer for contract events
}

// Knownorigin0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Knownorigin0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Knownorigin0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Knownorigin0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Knownorigin0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Knownorigin0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Knownorigin0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Knownorigin0Session struct {
	Contract     *Knownorigin0     // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Knownorigin0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Knownorigin0CallerSession struct {
	Contract *Knownorigin0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts       // Call options to use throughout this session
}

// Knownorigin0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Knownorigin0TransactorSession struct {
	Contract     *Knownorigin0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts       // Transaction auth options to use throughout this session
}

// Knownorigin0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Knownorigin0Raw struct {
	Contract *Knownorigin0 // Generic contract binding to access the raw methods on
}

// Knownorigin0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Knownorigin0CallerRaw struct {
	Contract *Knownorigin0Caller // Generic read-only contract binding to access the raw methods on
}

// Knownorigin0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Knownorigin0TransactorRaw struct {
	Contract *Knownorigin0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewKnownorigin0 creates a new instance of Knownorigin0, bound to a specific deployed contract.
func NewKnownorigin0(address common.Address, backend bind.ContractBackend) (*Knownorigin0, error) {
	contract, err := bindKnownorigin0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0{Knownorigin0Caller: Knownorigin0Caller{contract: contract}, Knownorigin0Transactor: Knownorigin0Transactor{contract: contract}, Knownorigin0Filterer: Knownorigin0Filterer{contract: contract}}, nil
}

// NewKnownorigin0Caller creates a new read-only instance of Knownorigin0, bound to a specific deployed contract.
func NewKnownorigin0Caller(address common.Address, caller bind.ContractCaller) (*Knownorigin0Caller, error) {
	contract, err := bindKnownorigin0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0Caller{contract: contract}, nil
}

// NewKnownorigin0Transactor creates a new write-only instance of Knownorigin0, bound to a specific deployed contract.
func NewKnownorigin0Transactor(address common.Address, transactor bind.ContractTransactor) (*Knownorigin0Transactor, error) {
	contract, err := bindKnownorigin0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0Transactor{contract: contract}, nil
}

// NewKnownorigin0Filterer creates a new log filterer instance of Knownorigin0, bound to a specific deployed contract.
func NewKnownorigin0Filterer(address common.Address, filterer bind.ContractFilterer) (*Knownorigin0Filterer, error) {
	contract, err := bindKnownorigin0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0Filterer{contract: contract}, nil
}

// bindKnownorigin0 binds a generic wrapper to an already deployed contract.
func bindKnownorigin0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Knownorigin0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Knownorigin0 *Knownorigin0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Knownorigin0.Contract.Knownorigin0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Knownorigin0 *Knownorigin0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Knownorigin0.Contract.Knownorigin0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Knownorigin0 *Knownorigin0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Knownorigin0.Contract.Knownorigin0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Knownorigin0 *Knownorigin0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Knownorigin0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Knownorigin0 *Knownorigin0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Knownorigin0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Knownorigin0 *Knownorigin0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Knownorigin0.Contract.contract.Transact(opts, method, params...)
}

// AccessControls is a free data retrieval call binding the contract method 0x748365ef.
//
// Solidity: function accessControls() view returns(address)
func (_Knownorigin0 *Knownorigin0Caller) AccessControls(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "accessControls")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// AccessControls is a free data retrieval call binding the contract method 0x748365ef.
//
// Solidity: function accessControls() view returns(address)
func (_Knownorigin0 *Knownorigin0Session) AccessControls() (common.Address, error) {
	return _Knownorigin0.Contract.AccessControls(&_Knownorigin0.CallOpts)
}

// AccessControls is a free data retrieval call binding the contract method 0x748365ef.
//
// Solidity: function accessControls() view returns(address)
func (_Knownorigin0 *Knownorigin0CallerSession) AccessControls() (common.Address, error) {
	return _Knownorigin0.Contract.AccessControls(&_Knownorigin0.CallOpts)
}

// BidLockupPeriod is a free data retrieval call binding the contract method 0x95880bfe.
//
// Solidity: function bidLockupPeriod() view returns(uint256)
func (_Knownorigin0 *Knownorigin0Caller) BidLockupPeriod(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "bidLockupPeriod")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BidLockupPeriod is a free data retrieval call binding the contract method 0x95880bfe.
//
// Solidity: function bidLockupPeriod() view returns(uint256)
func (_Knownorigin0 *Knownorigin0Session) BidLockupPeriod() (*big.Int, error) {
	return _Knownorigin0.Contract.BidLockupPeriod(&_Knownorigin0.CallOpts)
}

// BidLockupPeriod is a free data retrieval call binding the contract method 0x95880bfe.
//
// Solidity: function bidLockupPeriod() view returns(uint256)
func (_Knownorigin0 *Knownorigin0CallerSession) BidLockupPeriod() (*big.Int, error) {
	return _Knownorigin0.Contract.BidLockupPeriod(&_Knownorigin0.CallOpts)
}

// EditionOffers is a free data retrieval call binding the contract method 0x47437970.
//
// Solidity: function editionOffers(uint256 ) view returns(uint256 offer, address bidder, uint256 lockupUntil)
func (_Knownorigin0 *Knownorigin0Caller) EditionOffers(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Offer       *big.Int
	Bidder      common.Address
	LockupUntil *big.Int
}, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "editionOffers", arg0)

	outstruct := new(struct {
		Offer       *big.Int
		Bidder      common.Address
		LockupUntil *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Offer = *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	outstruct.Bidder = *abi.ConvertType(out[1], new(common.Address)).(*common.Address)
	outstruct.LockupUntil = *abi.ConvertType(out[2], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// EditionOffers is a free data retrieval call binding the contract method 0x47437970.
//
// Solidity: function editionOffers(uint256 ) view returns(uint256 offer, address bidder, uint256 lockupUntil)
func (_Knownorigin0 *Knownorigin0Session) EditionOffers(arg0 *big.Int) (struct {
	Offer       *big.Int
	Bidder      common.Address
	LockupUntil *big.Int
}, error) {
	return _Knownorigin0.Contract.EditionOffers(&_Knownorigin0.CallOpts, arg0)
}

// EditionOffers is a free data retrieval call binding the contract method 0x47437970.
//
// Solidity: function editionOffers(uint256 ) view returns(uint256 offer, address bidder, uint256 lockupUntil)
func (_Knownorigin0 *Knownorigin0CallerSession) EditionOffers(arg0 *big.Int) (struct {
	Offer       *big.Int
	Bidder      common.Address
	LockupUntil *big.Int
}, error) {
	return _Knownorigin0.Contract.EditionOffers(&_Knownorigin0.CallOpts, arg0)
}

// EditionOffersStartDate is a free data retrieval call binding the contract method 0x87311d8f.
//
// Solidity: function editionOffersStartDate(uint256 ) view returns(uint256)
func (_Knownorigin0 *Knownorigin0Caller) EditionOffersStartDate(opts *bind.CallOpts, arg0 *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "editionOffersStartDate", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// EditionOffersStartDate is a free data retrieval call binding the contract method 0x87311d8f.
//
// Solidity: function editionOffersStartDate(uint256 ) view returns(uint256)
func (_Knownorigin0 *Knownorigin0Session) EditionOffersStartDate(arg0 *big.Int) (*big.Int, error) {
	return _Knownorigin0.Contract.EditionOffersStartDate(&_Knownorigin0.CallOpts, arg0)
}

// EditionOffersStartDate is a free data retrieval call binding the contract method 0x87311d8f.
//
// Solidity: function editionOffersStartDate(uint256 ) view returns(uint256)
func (_Knownorigin0 *Knownorigin0CallerSession) EditionOffersStartDate(arg0 *big.Int) (*big.Int, error) {
	return _Knownorigin0.Contract.EditionOffersStartDate(&_Knownorigin0.CallOpts, arg0)
}

// EditionOrTokenListings is a free data retrieval call binding the contract method 0x7fb38ab0.
//
// Solidity: function editionOrTokenListings(uint256 ) view returns(uint128 price, uint128 startDate, address seller)
func (_Knownorigin0 *Knownorigin0Caller) EditionOrTokenListings(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Price     *big.Int
	StartDate *big.Int
	Seller    common.Address
}, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "editionOrTokenListings", arg0)

	outstruct := new(struct {
		Price     *big.Int
		StartDate *big.Int
		Seller    common.Address
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Price = *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	outstruct.StartDate = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.Seller = *abi.ConvertType(out[2], new(common.Address)).(*common.Address)

	return *outstruct, err

}

// EditionOrTokenListings is a free data retrieval call binding the contract method 0x7fb38ab0.
//
// Solidity: function editionOrTokenListings(uint256 ) view returns(uint128 price, uint128 startDate, address seller)
func (_Knownorigin0 *Knownorigin0Session) EditionOrTokenListings(arg0 *big.Int) (struct {
	Price     *big.Int
	StartDate *big.Int
	Seller    common.Address
}, error) {
	return _Knownorigin0.Contract.EditionOrTokenListings(&_Knownorigin0.CallOpts, arg0)
}

// EditionOrTokenListings is a free data retrieval call binding the contract method 0x7fb38ab0.
//
// Solidity: function editionOrTokenListings(uint256 ) view returns(uint128 price, uint128 startDate, address seller)
func (_Knownorigin0 *Knownorigin0CallerSession) EditionOrTokenListings(arg0 *big.Int) (struct {
	Price     *big.Int
	StartDate *big.Int
	Seller    common.Address
}, error) {
	return _Knownorigin0.Contract.EditionOrTokenListings(&_Knownorigin0.CallOpts, arg0)
}

// EditionOrTokenWithReserveAuctions is a free data retrieval call binding the contract method 0x8f084d7c.
//
// Solidity: function editionOrTokenWithReserveAuctions(uint256 ) view returns(address seller, address bidder, uint128 reservePrice, uint128 bid, uint128 startDate, uint128 biddingEnd)
func (_Knownorigin0 *Knownorigin0Caller) EditionOrTokenWithReserveAuctions(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Seller       common.Address
	Bidder       common.Address
	ReservePrice *big.Int
	Bid          *big.Int
	StartDate    *big.Int
	BiddingEnd   *big.Int
}, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "editionOrTokenWithReserveAuctions", arg0)

	outstruct := new(struct {
		Seller       common.Address
		Bidder       common.Address
		ReservePrice *big.Int
		Bid          *big.Int
		StartDate    *big.Int
		BiddingEnd   *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Seller = *abi.ConvertType(out[0], new(common.Address)).(*common.Address)
	outstruct.Bidder = *abi.ConvertType(out[1], new(common.Address)).(*common.Address)
	outstruct.ReservePrice = *abi.ConvertType(out[2], new(*big.Int)).(**big.Int)
	outstruct.Bid = *abi.ConvertType(out[3], new(*big.Int)).(**big.Int)
	outstruct.StartDate = *abi.ConvertType(out[4], new(*big.Int)).(**big.Int)
	outstruct.BiddingEnd = *abi.ConvertType(out[5], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// EditionOrTokenWithReserveAuctions is a free data retrieval call binding the contract method 0x8f084d7c.
//
// Solidity: function editionOrTokenWithReserveAuctions(uint256 ) view returns(address seller, address bidder, uint128 reservePrice, uint128 bid, uint128 startDate, uint128 biddingEnd)
func (_Knownorigin0 *Knownorigin0Session) EditionOrTokenWithReserveAuctions(arg0 *big.Int) (struct {
	Seller       common.Address
	Bidder       common.Address
	ReservePrice *big.Int
	Bid          *big.Int
	StartDate    *big.Int
	BiddingEnd   *big.Int
}, error) {
	return _Knownorigin0.Contract.EditionOrTokenWithReserveAuctions(&_Knownorigin0.CallOpts, arg0)
}

// EditionOrTokenWithReserveAuctions is a free data retrieval call binding the contract method 0x8f084d7c.
//
// Solidity: function editionOrTokenWithReserveAuctions(uint256 ) view returns(address seller, address bidder, uint128 reservePrice, uint128 bid, uint128 startDate, uint128 biddingEnd)
func (_Knownorigin0 *Knownorigin0CallerSession) EditionOrTokenWithReserveAuctions(arg0 *big.Int) (struct {
	Seller       common.Address
	Bidder       common.Address
	ReservePrice *big.Int
	Bid          *big.Int
	StartDate    *big.Int
	BiddingEnd   *big.Int
}, error) {
	return _Knownorigin0.Contract.EditionOrTokenWithReserveAuctions(&_Knownorigin0.CallOpts, arg0)
}

// EditionStep is a free data retrieval call binding the contract method 0xb0a00851.
//
// Solidity: function editionStep(uint256 ) view returns(uint128 basePrice, uint128 stepPrice, uint128 startDate, address seller, uint16 currentStep)
func (_Knownorigin0 *Knownorigin0Caller) EditionStep(opts *bind.CallOpts, arg0 *big.Int) (struct {
	BasePrice   *big.Int
	StepPrice   *big.Int
	StartDate   *big.Int
	Seller      common.Address
	CurrentStep uint16
}, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "editionStep", arg0)

	outstruct := new(struct {
		BasePrice   *big.Int
		StepPrice   *big.Int
		StartDate   *big.Int
		Seller      common.Address
		CurrentStep uint16
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.BasePrice = *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	outstruct.StepPrice = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.StartDate = *abi.ConvertType(out[2], new(*big.Int)).(**big.Int)
	outstruct.Seller = *abi.ConvertType(out[3], new(common.Address)).(*common.Address)
	outstruct.CurrentStep = *abi.ConvertType(out[4], new(uint16)).(*uint16)

	return *outstruct, err

}

// EditionStep is a free data retrieval call binding the contract method 0xb0a00851.
//
// Solidity: function editionStep(uint256 ) view returns(uint128 basePrice, uint128 stepPrice, uint128 startDate, address seller, uint16 currentStep)
func (_Knownorigin0 *Knownorigin0Session) EditionStep(arg0 *big.Int) (struct {
	BasePrice   *big.Int
	StepPrice   *big.Int
	StartDate   *big.Int
	Seller      common.Address
	CurrentStep uint16
}, error) {
	return _Knownorigin0.Contract.EditionStep(&_Knownorigin0.CallOpts, arg0)
}

// EditionStep is a free data retrieval call binding the contract method 0xb0a00851.
//
// Solidity: function editionStep(uint256 ) view returns(uint128 basePrice, uint128 stepPrice, uint128 startDate, address seller, uint16 currentStep)
func (_Knownorigin0 *Knownorigin0CallerSession) EditionStep(arg0 *big.Int) (struct {
	BasePrice   *big.Int
	StepPrice   *big.Int
	StartDate   *big.Int
	Seller      common.Address
	CurrentStep uint16
}, error) {
	return _Knownorigin0.Contract.EditionStep(&_Knownorigin0.CallOpts, arg0)
}

// GetNextEditionSteppedPrice is a free data retrieval call binding the contract method 0x7d87bc80.
//
// Solidity: function getNextEditionSteppedPrice(uint256 _editionId) view returns(uint256 price)
func (_Knownorigin0 *Knownorigin0Caller) GetNextEditionSteppedPrice(opts *bind.CallOpts, _editionId *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "getNextEditionSteppedPrice", _editionId)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetNextEditionSteppedPrice is a free data retrieval call binding the contract method 0x7d87bc80.
//
// Solidity: function getNextEditionSteppedPrice(uint256 _editionId) view returns(uint256 price)
func (_Knownorigin0 *Knownorigin0Session) GetNextEditionSteppedPrice(_editionId *big.Int) (*big.Int, error) {
	return _Knownorigin0.Contract.GetNextEditionSteppedPrice(&_Knownorigin0.CallOpts, _editionId)
}

// GetNextEditionSteppedPrice is a free data retrieval call binding the contract method 0x7d87bc80.
//
// Solidity: function getNextEditionSteppedPrice(uint256 _editionId) view returns(uint256 price)
func (_Knownorigin0 *Knownorigin0CallerSession) GetNextEditionSteppedPrice(_editionId *big.Int) (*big.Int, error) {
	return _Knownorigin0.Contract.GetNextEditionSteppedPrice(&_Knownorigin0.CallOpts, _editionId)
}

// KoCommissionOverrideForCreators is a free data retrieval call binding the contract method 0x4f17e281.
//
// Solidity: function koCommissionOverrideForCreators(address ) view returns(bool active, uint256 koCommission)
func (_Knownorigin0 *Knownorigin0Caller) KoCommissionOverrideForCreators(opts *bind.CallOpts, arg0 common.Address) (struct {
	Active       bool
	KoCommission *big.Int
}, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "koCommissionOverrideForCreators", arg0)

	outstruct := new(struct {
		Active       bool
		KoCommission *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Active = *abi.ConvertType(out[0], new(bool)).(*bool)
	outstruct.KoCommission = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// KoCommissionOverrideForCreators is a free data retrieval call binding the contract method 0x4f17e281.
//
// Solidity: function koCommissionOverrideForCreators(address ) view returns(bool active, uint256 koCommission)
func (_Knownorigin0 *Knownorigin0Session) KoCommissionOverrideForCreators(arg0 common.Address) (struct {
	Active       bool
	KoCommission *big.Int
}, error) {
	return _Knownorigin0.Contract.KoCommissionOverrideForCreators(&_Knownorigin0.CallOpts, arg0)
}

// KoCommissionOverrideForCreators is a free data retrieval call binding the contract method 0x4f17e281.
//
// Solidity: function koCommissionOverrideForCreators(address ) view returns(bool active, uint256 koCommission)
func (_Knownorigin0 *Knownorigin0CallerSession) KoCommissionOverrideForCreators(arg0 common.Address) (struct {
	Active       bool
	KoCommission *big.Int
}, error) {
	return _Knownorigin0.Contract.KoCommissionOverrideForCreators(&_Knownorigin0.CallOpts, arg0)
}

// KoCommissionOverrideForEditions is a free data retrieval call binding the contract method 0x4f484119.
//
// Solidity: function koCommissionOverrideForEditions(uint256 ) view returns(bool active, uint256 koCommission)
func (_Knownorigin0 *Knownorigin0Caller) KoCommissionOverrideForEditions(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Active       bool
	KoCommission *big.Int
}, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "koCommissionOverrideForEditions", arg0)

	outstruct := new(struct {
		Active       bool
		KoCommission *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Active = *abi.ConvertType(out[0], new(bool)).(*bool)
	outstruct.KoCommission = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// KoCommissionOverrideForEditions is a free data retrieval call binding the contract method 0x4f484119.
//
// Solidity: function koCommissionOverrideForEditions(uint256 ) view returns(bool active, uint256 koCommission)
func (_Knownorigin0 *Knownorigin0Session) KoCommissionOverrideForEditions(arg0 *big.Int) (struct {
	Active       bool
	KoCommission *big.Int
}, error) {
	return _Knownorigin0.Contract.KoCommissionOverrideForEditions(&_Knownorigin0.CallOpts, arg0)
}

// KoCommissionOverrideForEditions is a free data retrieval call binding the contract method 0x4f484119.
//
// Solidity: function koCommissionOverrideForEditions(uint256 ) view returns(bool active, uint256 koCommission)
func (_Knownorigin0 *Knownorigin0CallerSession) KoCommissionOverrideForEditions(arg0 *big.Int) (struct {
	Active       bool
	KoCommission *big.Int
}, error) {
	return _Knownorigin0.Contract.KoCommissionOverrideForEditions(&_Knownorigin0.CallOpts, arg0)
}

// Koda is a free data retrieval call binding the contract method 0x84c95d2a.
//
// Solidity: function koda() view returns(address)
func (_Knownorigin0 *Knownorigin0Caller) Koda(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "koda")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Koda is a free data retrieval call binding the contract method 0x84c95d2a.
//
// Solidity: function koda() view returns(address)
func (_Knownorigin0 *Knownorigin0Session) Koda() (common.Address, error) {
	return _Knownorigin0.Contract.Koda(&_Knownorigin0.CallOpts)
}

// Koda is a free data retrieval call binding the contract method 0x84c95d2a.
//
// Solidity: function koda() view returns(address)
func (_Knownorigin0 *Knownorigin0CallerSession) Koda() (common.Address, error) {
	return _Knownorigin0.Contract.Koda(&_Knownorigin0.CallOpts)
}

// MinBidAmount is a free data retrieval call binding the contract method 0x49751788.
//
// Solidity: function minBidAmount() view returns(uint256)
func (_Knownorigin0 *Knownorigin0Caller) MinBidAmount(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "minBidAmount")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MinBidAmount is a free data retrieval call binding the contract method 0x49751788.
//
// Solidity: function minBidAmount() view returns(uint256)
func (_Knownorigin0 *Knownorigin0Session) MinBidAmount() (*big.Int, error) {
	return _Knownorigin0.Contract.MinBidAmount(&_Knownorigin0.CallOpts)
}

// MinBidAmount is a free data retrieval call binding the contract method 0x49751788.
//
// Solidity: function minBidAmount() view returns(uint256)
func (_Knownorigin0 *Knownorigin0CallerSession) MinBidAmount() (*big.Int, error) {
	return _Knownorigin0.Contract.MinBidAmount(&_Knownorigin0.CallOpts)
}

// Modulo is a free data retrieval call binding the contract method 0x29745262.
//
// Solidity: function modulo() view returns(uint256)
func (_Knownorigin0 *Knownorigin0Caller) Modulo(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "modulo")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Modulo is a free data retrieval call binding the contract method 0x29745262.
//
// Solidity: function modulo() view returns(uint256)
func (_Knownorigin0 *Knownorigin0Session) Modulo() (*big.Int, error) {
	return _Knownorigin0.Contract.Modulo(&_Knownorigin0.CallOpts)
}

// Modulo is a free data retrieval call binding the contract method 0x29745262.
//
// Solidity: function modulo() view returns(uint256)
func (_Knownorigin0 *Knownorigin0CallerSession) Modulo() (*big.Int, error) {
	return _Knownorigin0.Contract.Modulo(&_Knownorigin0.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Knownorigin0 *Knownorigin0Caller) Paused(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "paused")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Knownorigin0 *Knownorigin0Session) Paused() (bool, error) {
	return _Knownorigin0.Contract.Paused(&_Knownorigin0.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Knownorigin0 *Knownorigin0CallerSession) Paused() (bool, error) {
	return _Knownorigin0.Contract.Paused(&_Knownorigin0.CallOpts)
}

// PlatformAccount is a free data retrieval call binding the contract method 0x97849eb3.
//
// Solidity: function platformAccount() view returns(address)
func (_Knownorigin0 *Knownorigin0Caller) PlatformAccount(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "platformAccount")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// PlatformAccount is a free data retrieval call binding the contract method 0x97849eb3.
//
// Solidity: function platformAccount() view returns(address)
func (_Knownorigin0 *Knownorigin0Session) PlatformAccount() (common.Address, error) {
	return _Knownorigin0.Contract.PlatformAccount(&_Knownorigin0.CallOpts)
}

// PlatformAccount is a free data retrieval call binding the contract method 0x97849eb3.
//
// Solidity: function platformAccount() view returns(address)
func (_Knownorigin0 *Knownorigin0CallerSession) PlatformAccount() (common.Address, error) {
	return _Knownorigin0.Contract.PlatformAccount(&_Knownorigin0.CallOpts)
}

// PlatformPrimarySaleCommission is a free data retrieval call binding the contract method 0xce9e1ce5.
//
// Solidity: function platformPrimarySaleCommission() view returns(uint256)
func (_Knownorigin0 *Knownorigin0Caller) PlatformPrimarySaleCommission(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "platformPrimarySaleCommission")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// PlatformPrimarySaleCommission is a free data retrieval call binding the contract method 0xce9e1ce5.
//
// Solidity: function platformPrimarySaleCommission() view returns(uint256)
func (_Knownorigin0 *Knownorigin0Session) PlatformPrimarySaleCommission() (*big.Int, error) {
	return _Knownorigin0.Contract.PlatformPrimarySaleCommission(&_Knownorigin0.CallOpts)
}

// PlatformPrimarySaleCommission is a free data retrieval call binding the contract method 0xce9e1ce5.
//
// Solidity: function platformPrimarySaleCommission() view returns(uint256)
func (_Knownorigin0 *Knownorigin0CallerSession) PlatformPrimarySaleCommission() (*big.Int, error) {
	return _Knownorigin0.Contract.PlatformPrimarySaleCommission(&_Knownorigin0.CallOpts)
}

// ReserveAuctionBidExtensionWindow is a free data retrieval call binding the contract method 0xff89a80b.
//
// Solidity: function reserveAuctionBidExtensionWindow() view returns(uint128)
func (_Knownorigin0 *Knownorigin0Caller) ReserveAuctionBidExtensionWindow(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "reserveAuctionBidExtensionWindow")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// ReserveAuctionBidExtensionWindow is a free data retrieval call binding the contract method 0xff89a80b.
//
// Solidity: function reserveAuctionBidExtensionWindow() view returns(uint128)
func (_Knownorigin0 *Knownorigin0Session) ReserveAuctionBidExtensionWindow() (*big.Int, error) {
	return _Knownorigin0.Contract.ReserveAuctionBidExtensionWindow(&_Knownorigin0.CallOpts)
}

// ReserveAuctionBidExtensionWindow is a free data retrieval call binding the contract method 0xff89a80b.
//
// Solidity: function reserveAuctionBidExtensionWindow() view returns(uint128)
func (_Knownorigin0 *Knownorigin0CallerSession) ReserveAuctionBidExtensionWindow() (*big.Int, error) {
	return _Knownorigin0.Contract.ReserveAuctionBidExtensionWindow(&_Knownorigin0.CallOpts)
}

// ReserveAuctionLengthOnceReserveMet is a free data retrieval call binding the contract method 0x2cd13c00.
//
// Solidity: function reserveAuctionLengthOnceReserveMet() view returns(uint128)
func (_Knownorigin0 *Knownorigin0Caller) ReserveAuctionLengthOnceReserveMet(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Knownorigin0.contract.Call(opts, &out, "reserveAuctionLengthOnceReserveMet")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// ReserveAuctionLengthOnceReserveMet is a free data retrieval call binding the contract method 0x2cd13c00.
//
// Solidity: function reserveAuctionLengthOnceReserveMet() view returns(uint128)
func (_Knownorigin0 *Knownorigin0Session) ReserveAuctionLengthOnceReserveMet() (*big.Int, error) {
	return _Knownorigin0.Contract.ReserveAuctionLengthOnceReserveMet(&_Knownorigin0.CallOpts)
}

// ReserveAuctionLengthOnceReserveMet is a free data retrieval call binding the contract method 0x2cd13c00.
//
// Solidity: function reserveAuctionLengthOnceReserveMet() view returns(uint128)
func (_Knownorigin0 *Knownorigin0CallerSession) ReserveAuctionLengthOnceReserveMet() (*big.Int, error) {
	return _Knownorigin0.Contract.ReserveAuctionLengthOnceReserveMet(&_Knownorigin0.CallOpts)
}

// AcceptEditionBid is a paid mutator transaction binding the contract method 0xa8b3ed71.
//
// Solidity: function acceptEditionBid(uint256 _editionId, uint256 _offerPrice) returns()
func (_Knownorigin0 *Knownorigin0Transactor) AcceptEditionBid(opts *bind.TransactOpts, _editionId *big.Int, _offerPrice *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "acceptEditionBid", _editionId, _offerPrice)
}

// AcceptEditionBid is a paid mutator transaction binding the contract method 0xa8b3ed71.
//
// Solidity: function acceptEditionBid(uint256 _editionId, uint256 _offerPrice) returns()
func (_Knownorigin0 *Knownorigin0Session) AcceptEditionBid(_editionId *big.Int, _offerPrice *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.AcceptEditionBid(&_Knownorigin0.TransactOpts, _editionId, _offerPrice)
}

// AcceptEditionBid is a paid mutator transaction binding the contract method 0xa8b3ed71.
//
// Solidity: function acceptEditionBid(uint256 _editionId, uint256 _offerPrice) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) AcceptEditionBid(_editionId *big.Int, _offerPrice *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.AcceptEditionBid(&_Knownorigin0.TransactOpts, _editionId, _offerPrice)
}

// AdminRejectEditionBid is a paid mutator transaction binding the contract method 0xa340f39e.
//
// Solidity: function adminRejectEditionBid(uint256 _editionId) returns()
func (_Knownorigin0 *Knownorigin0Transactor) AdminRejectEditionBid(opts *bind.TransactOpts, _editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "adminRejectEditionBid", _editionId)
}

// AdminRejectEditionBid is a paid mutator transaction binding the contract method 0xa340f39e.
//
// Solidity: function adminRejectEditionBid(uint256 _editionId) returns()
func (_Knownorigin0 *Knownorigin0Session) AdminRejectEditionBid(_editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.AdminRejectEditionBid(&_Knownorigin0.TransactOpts, _editionId)
}

// AdminRejectEditionBid is a paid mutator transaction binding the contract method 0xa340f39e.
//
// Solidity: function adminRejectEditionBid(uint256 _editionId) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) AdminRejectEditionBid(_editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.AdminRejectEditionBid(&_Knownorigin0.TransactOpts, _editionId)
}

// BuyEditionToken is a paid mutator transaction binding the contract method 0x9c1d9907.
//
// Solidity: function buyEditionToken(uint256 _id) payable returns()
func (_Knownorigin0 *Knownorigin0Transactor) BuyEditionToken(opts *bind.TransactOpts, _id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "buyEditionToken", _id)
}

// BuyEditionToken is a paid mutator transaction binding the contract method 0x9c1d9907.
//
// Solidity: function buyEditionToken(uint256 _id) payable returns()
func (_Knownorigin0 *Knownorigin0Session) BuyEditionToken(_id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.BuyEditionToken(&_Knownorigin0.TransactOpts, _id)
}

// BuyEditionToken is a paid mutator transaction binding the contract method 0x9c1d9907.
//
// Solidity: function buyEditionToken(uint256 _id) payable returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) BuyEditionToken(_id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.BuyEditionToken(&_Knownorigin0.TransactOpts, _id)
}

// BuyEditionTokenFor is a paid mutator transaction binding the contract method 0x6710c3f2.
//
// Solidity: function buyEditionTokenFor(uint256 _id, address _recipient) payable returns()
func (_Knownorigin0 *Knownorigin0Transactor) BuyEditionTokenFor(opts *bind.TransactOpts, _id *big.Int, _recipient common.Address) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "buyEditionTokenFor", _id, _recipient)
}

// BuyEditionTokenFor is a paid mutator transaction binding the contract method 0x6710c3f2.
//
// Solidity: function buyEditionTokenFor(uint256 _id, address _recipient) payable returns()
func (_Knownorigin0 *Knownorigin0Session) BuyEditionTokenFor(_id *big.Int, _recipient common.Address) (*types.Transaction, error) {
	return _Knownorigin0.Contract.BuyEditionTokenFor(&_Knownorigin0.TransactOpts, _id, _recipient)
}

// BuyEditionTokenFor is a paid mutator transaction binding the contract method 0x6710c3f2.
//
// Solidity: function buyEditionTokenFor(uint256 _id, address _recipient) payable returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) BuyEditionTokenFor(_id *big.Int, _recipient common.Address) (*types.Transaction, error) {
	return _Knownorigin0.Contract.BuyEditionTokenFor(&_Knownorigin0.TransactOpts, _id, _recipient)
}

// BuyNextStep is a paid mutator transaction binding the contract method 0x55d93d49.
//
// Solidity: function buyNextStep(uint256 _editionId) payable returns()
func (_Knownorigin0 *Knownorigin0Transactor) BuyNextStep(opts *bind.TransactOpts, _editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "buyNextStep", _editionId)
}

// BuyNextStep is a paid mutator transaction binding the contract method 0x55d93d49.
//
// Solidity: function buyNextStep(uint256 _editionId) payable returns()
func (_Knownorigin0 *Knownorigin0Session) BuyNextStep(_editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.BuyNextStep(&_Knownorigin0.TransactOpts, _editionId)
}

// BuyNextStep is a paid mutator transaction binding the contract method 0x55d93d49.
//
// Solidity: function buyNextStep(uint256 _editionId) payable returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) BuyNextStep(_editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.BuyNextStep(&_Knownorigin0.TransactOpts, _editionId)
}

// BuyNextStepFor is a paid mutator transaction binding the contract method 0x4cb97c18.
//
// Solidity: function buyNextStepFor(uint256 _editionId, address _buyer) payable returns()
func (_Knownorigin0 *Knownorigin0Transactor) BuyNextStepFor(opts *bind.TransactOpts, _editionId *big.Int, _buyer common.Address) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "buyNextStepFor", _editionId, _buyer)
}

// BuyNextStepFor is a paid mutator transaction binding the contract method 0x4cb97c18.
//
// Solidity: function buyNextStepFor(uint256 _editionId, address _buyer) payable returns()
func (_Knownorigin0 *Knownorigin0Session) BuyNextStepFor(_editionId *big.Int, _buyer common.Address) (*types.Transaction, error) {
	return _Knownorigin0.Contract.BuyNextStepFor(&_Knownorigin0.TransactOpts, _editionId, _buyer)
}

// BuyNextStepFor is a paid mutator transaction binding the contract method 0x4cb97c18.
//
// Solidity: function buyNextStepFor(uint256 _editionId, address _buyer) payable returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) BuyNextStepFor(_editionId *big.Int, _buyer common.Address) (*types.Transaction, error) {
	return _Knownorigin0.Contract.BuyNextStepFor(&_Knownorigin0.TransactOpts, _editionId, _buyer)
}

// ConvertFromBuyNowToOffers is a paid mutator transaction binding the contract method 0x2ade1bf3.
//
// Solidity: function convertFromBuyNowToOffers(uint256 _editionId, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Transactor) ConvertFromBuyNowToOffers(opts *bind.TransactOpts, _editionId *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "convertFromBuyNowToOffers", _editionId, _startDate)
}

// ConvertFromBuyNowToOffers is a paid mutator transaction binding the contract method 0x2ade1bf3.
//
// Solidity: function convertFromBuyNowToOffers(uint256 _editionId, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Session) ConvertFromBuyNowToOffers(_editionId *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ConvertFromBuyNowToOffers(&_Knownorigin0.TransactOpts, _editionId, _startDate)
}

// ConvertFromBuyNowToOffers is a paid mutator transaction binding the contract method 0x2ade1bf3.
//
// Solidity: function convertFromBuyNowToOffers(uint256 _editionId, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) ConvertFromBuyNowToOffers(_editionId *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ConvertFromBuyNowToOffers(&_Knownorigin0.TransactOpts, _editionId, _startDate)
}

// ConvertOffersToBuyItNow is a paid mutator transaction binding the contract method 0xa81b5747.
//
// Solidity: function convertOffersToBuyItNow(uint256 _editionId, uint128 _listingPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Transactor) ConvertOffersToBuyItNow(opts *bind.TransactOpts, _editionId *big.Int, _listingPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "convertOffersToBuyItNow", _editionId, _listingPrice, _startDate)
}

// ConvertOffersToBuyItNow is a paid mutator transaction binding the contract method 0xa81b5747.
//
// Solidity: function convertOffersToBuyItNow(uint256 _editionId, uint128 _listingPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Session) ConvertOffersToBuyItNow(_editionId *big.Int, _listingPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ConvertOffersToBuyItNow(&_Knownorigin0.TransactOpts, _editionId, _listingPrice, _startDate)
}

// ConvertOffersToBuyItNow is a paid mutator transaction binding the contract method 0xa81b5747.
//
// Solidity: function convertOffersToBuyItNow(uint256 _editionId, uint128 _listingPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) ConvertOffersToBuyItNow(_editionId *big.Int, _listingPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ConvertOffersToBuyItNow(&_Knownorigin0.TransactOpts, _editionId, _listingPrice, _startDate)
}

// ConvertReserveAuctionToBuyItNow is a paid mutator transaction binding the contract method 0xdee294ab.
//
// Solidity: function convertReserveAuctionToBuyItNow(uint256 _editionId, uint128 _listingPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Transactor) ConvertReserveAuctionToBuyItNow(opts *bind.TransactOpts, _editionId *big.Int, _listingPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "convertReserveAuctionToBuyItNow", _editionId, _listingPrice, _startDate)
}

// ConvertReserveAuctionToBuyItNow is a paid mutator transaction binding the contract method 0xdee294ab.
//
// Solidity: function convertReserveAuctionToBuyItNow(uint256 _editionId, uint128 _listingPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Session) ConvertReserveAuctionToBuyItNow(_editionId *big.Int, _listingPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ConvertReserveAuctionToBuyItNow(&_Knownorigin0.TransactOpts, _editionId, _listingPrice, _startDate)
}

// ConvertReserveAuctionToBuyItNow is a paid mutator transaction binding the contract method 0xdee294ab.
//
// Solidity: function convertReserveAuctionToBuyItNow(uint256 _editionId, uint128 _listingPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) ConvertReserveAuctionToBuyItNow(_editionId *big.Int, _listingPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ConvertReserveAuctionToBuyItNow(&_Knownorigin0.TransactOpts, _editionId, _listingPrice, _startDate)
}

// ConvertReserveAuctionToOffers is a paid mutator transaction binding the contract method 0x502942cd.
//
// Solidity: function convertReserveAuctionToOffers(uint256 _editionId, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Transactor) ConvertReserveAuctionToOffers(opts *bind.TransactOpts, _editionId *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "convertReserveAuctionToOffers", _editionId, _startDate)
}

// ConvertReserveAuctionToOffers is a paid mutator transaction binding the contract method 0x502942cd.
//
// Solidity: function convertReserveAuctionToOffers(uint256 _editionId, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Session) ConvertReserveAuctionToOffers(_editionId *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ConvertReserveAuctionToOffers(&_Knownorigin0.TransactOpts, _editionId, _startDate)
}

// ConvertReserveAuctionToOffers is a paid mutator transaction binding the contract method 0x502942cd.
//
// Solidity: function convertReserveAuctionToOffers(uint256 _editionId, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) ConvertReserveAuctionToOffers(_editionId *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ConvertReserveAuctionToOffers(&_Knownorigin0.TransactOpts, _editionId, _startDate)
}

// ConvertSteppedAuctionToListing is a paid mutator transaction binding the contract method 0x4860ee39.
//
// Solidity: function convertSteppedAuctionToListing(uint256 _editionId, uint128 _listingPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Transactor) ConvertSteppedAuctionToListing(opts *bind.TransactOpts, _editionId *big.Int, _listingPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "convertSteppedAuctionToListing", _editionId, _listingPrice, _startDate)
}

// ConvertSteppedAuctionToListing is a paid mutator transaction binding the contract method 0x4860ee39.
//
// Solidity: function convertSteppedAuctionToListing(uint256 _editionId, uint128 _listingPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Session) ConvertSteppedAuctionToListing(_editionId *big.Int, _listingPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ConvertSteppedAuctionToListing(&_Knownorigin0.TransactOpts, _editionId, _listingPrice, _startDate)
}

// ConvertSteppedAuctionToListing is a paid mutator transaction binding the contract method 0x4860ee39.
//
// Solidity: function convertSteppedAuctionToListing(uint256 _editionId, uint128 _listingPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) ConvertSteppedAuctionToListing(_editionId *big.Int, _listingPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ConvertSteppedAuctionToListing(&_Knownorigin0.TransactOpts, _editionId, _listingPrice, _startDate)
}

// ConvertSteppedAuctionToOffers is a paid mutator transaction binding the contract method 0xc21f86f4.
//
// Solidity: function convertSteppedAuctionToOffers(uint256 _editionId, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Transactor) ConvertSteppedAuctionToOffers(opts *bind.TransactOpts, _editionId *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "convertSteppedAuctionToOffers", _editionId, _startDate)
}

// ConvertSteppedAuctionToOffers is a paid mutator transaction binding the contract method 0xc21f86f4.
//
// Solidity: function convertSteppedAuctionToOffers(uint256 _editionId, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Session) ConvertSteppedAuctionToOffers(_editionId *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ConvertSteppedAuctionToOffers(&_Knownorigin0.TransactOpts, _editionId, _startDate)
}

// ConvertSteppedAuctionToOffers is a paid mutator transaction binding the contract method 0xc21f86f4.
//
// Solidity: function convertSteppedAuctionToOffers(uint256 _editionId, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) ConvertSteppedAuctionToOffers(_editionId *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ConvertSteppedAuctionToOffers(&_Knownorigin0.TransactOpts, _editionId, _startDate)
}

// EmergencyExitBidFromReserveAuction is a paid mutator transaction binding the contract method 0xa9d9985e.
//
// Solidity: function emergencyExitBidFromReserveAuction(uint256 _id) returns()
func (_Knownorigin0 *Knownorigin0Transactor) EmergencyExitBidFromReserveAuction(opts *bind.TransactOpts, _id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "emergencyExitBidFromReserveAuction", _id)
}

// EmergencyExitBidFromReserveAuction is a paid mutator transaction binding the contract method 0xa9d9985e.
//
// Solidity: function emergencyExitBidFromReserveAuction(uint256 _id) returns()
func (_Knownorigin0 *Knownorigin0Session) EmergencyExitBidFromReserveAuction(_id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.EmergencyExitBidFromReserveAuction(&_Knownorigin0.TransactOpts, _id)
}

// EmergencyExitBidFromReserveAuction is a paid mutator transaction binding the contract method 0xa9d9985e.
//
// Solidity: function emergencyExitBidFromReserveAuction(uint256 _id) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) EmergencyExitBidFromReserveAuction(_id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.EmergencyExitBidFromReserveAuction(&_Knownorigin0.TransactOpts, _id)
}

// EnableEditionOffers is a paid mutator transaction binding the contract method 0x8d91997e.
//
// Solidity: function enableEditionOffers(uint256 _editionId, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Transactor) EnableEditionOffers(opts *bind.TransactOpts, _editionId *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "enableEditionOffers", _editionId, _startDate)
}

// EnableEditionOffers is a paid mutator transaction binding the contract method 0x8d91997e.
//
// Solidity: function enableEditionOffers(uint256 _editionId, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Session) EnableEditionOffers(_editionId *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.EnableEditionOffers(&_Knownorigin0.TransactOpts, _editionId, _startDate)
}

// EnableEditionOffers is a paid mutator transaction binding the contract method 0x8d91997e.
//
// Solidity: function enableEditionOffers(uint256 _editionId, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) EnableEditionOffers(_editionId *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.EnableEditionOffers(&_Knownorigin0.TransactOpts, _editionId, _startDate)
}

// ListForBuyNow is a paid mutator transaction binding the contract method 0x47176275.
//
// Solidity: function listForBuyNow(address _seller, uint256 _id, uint128 _listingPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Transactor) ListForBuyNow(opts *bind.TransactOpts, _seller common.Address, _id *big.Int, _listingPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "listForBuyNow", _seller, _id, _listingPrice, _startDate)
}

// ListForBuyNow is a paid mutator transaction binding the contract method 0x47176275.
//
// Solidity: function listForBuyNow(address _seller, uint256 _id, uint128 _listingPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Session) ListForBuyNow(_seller common.Address, _id *big.Int, _listingPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ListForBuyNow(&_Knownorigin0.TransactOpts, _seller, _id, _listingPrice, _startDate)
}

// ListForBuyNow is a paid mutator transaction binding the contract method 0x47176275.
//
// Solidity: function listForBuyNow(address _seller, uint256 _id, uint128 _listingPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) ListForBuyNow(_seller common.Address, _id *big.Int, _listingPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ListForBuyNow(&_Knownorigin0.TransactOpts, _seller, _id, _listingPrice, _startDate)
}

// ListForReserveAuction is a paid mutator transaction binding the contract method 0x4940ecf0.
//
// Solidity: function listForReserveAuction(address _creator, uint256 _id, uint128 _reservePrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Transactor) ListForReserveAuction(opts *bind.TransactOpts, _creator common.Address, _id *big.Int, _reservePrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "listForReserveAuction", _creator, _id, _reservePrice, _startDate)
}

// ListForReserveAuction is a paid mutator transaction binding the contract method 0x4940ecf0.
//
// Solidity: function listForReserveAuction(address _creator, uint256 _id, uint128 _reservePrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Session) ListForReserveAuction(_creator common.Address, _id *big.Int, _reservePrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ListForReserveAuction(&_Knownorigin0.TransactOpts, _creator, _id, _reservePrice, _startDate)
}

// ListForReserveAuction is a paid mutator transaction binding the contract method 0x4940ecf0.
//
// Solidity: function listForReserveAuction(address _creator, uint256 _id, uint128 _reservePrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) ListForReserveAuction(_creator common.Address, _id *big.Int, _reservePrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ListForReserveAuction(&_Knownorigin0.TransactOpts, _creator, _id, _reservePrice, _startDate)
}

// ListSteppedEditionAuction is a paid mutator transaction binding the contract method 0x5e636b6f.
//
// Solidity: function listSteppedEditionAuction(address _creator, uint256 _editionId, uint128 _basePrice, uint128 _stepPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Transactor) ListSteppedEditionAuction(opts *bind.TransactOpts, _creator common.Address, _editionId *big.Int, _basePrice *big.Int, _stepPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "listSteppedEditionAuction", _creator, _editionId, _basePrice, _stepPrice, _startDate)
}

// ListSteppedEditionAuction is a paid mutator transaction binding the contract method 0x5e636b6f.
//
// Solidity: function listSteppedEditionAuction(address _creator, uint256 _editionId, uint128 _basePrice, uint128 _stepPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0Session) ListSteppedEditionAuction(_creator common.Address, _editionId *big.Int, _basePrice *big.Int, _stepPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ListSteppedEditionAuction(&_Knownorigin0.TransactOpts, _creator, _editionId, _basePrice, _stepPrice, _startDate)
}

// ListSteppedEditionAuction is a paid mutator transaction binding the contract method 0x5e636b6f.
//
// Solidity: function listSteppedEditionAuction(address _creator, uint256 _editionId, uint128 _basePrice, uint128 _stepPrice, uint128 _startDate) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) ListSteppedEditionAuction(_creator common.Address, _editionId *big.Int, _basePrice *big.Int, _stepPrice *big.Int, _startDate *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ListSteppedEditionAuction(&_Knownorigin0.TransactOpts, _creator, _editionId, _basePrice, _stepPrice, _startDate)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_Knownorigin0 *Knownorigin0Transactor) Pause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "pause")
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_Knownorigin0 *Knownorigin0Session) Pause() (*types.Transaction, error) {
	return _Knownorigin0.Contract.Pause(&_Knownorigin0.TransactOpts)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) Pause() (*types.Transaction, error) {
	return _Knownorigin0.Contract.Pause(&_Knownorigin0.TransactOpts)
}

// PlaceBidOnReserveAuction is a paid mutator transaction binding the contract method 0x7012663e.
//
// Solidity: function placeBidOnReserveAuction(uint256 _id) payable returns()
func (_Knownorigin0 *Knownorigin0Transactor) PlaceBidOnReserveAuction(opts *bind.TransactOpts, _id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "placeBidOnReserveAuction", _id)
}

// PlaceBidOnReserveAuction is a paid mutator transaction binding the contract method 0x7012663e.
//
// Solidity: function placeBidOnReserveAuction(uint256 _id) payable returns()
func (_Knownorigin0 *Knownorigin0Session) PlaceBidOnReserveAuction(_id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.PlaceBidOnReserveAuction(&_Knownorigin0.TransactOpts, _id)
}

// PlaceBidOnReserveAuction is a paid mutator transaction binding the contract method 0x7012663e.
//
// Solidity: function placeBidOnReserveAuction(uint256 _id) payable returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) PlaceBidOnReserveAuction(_id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.PlaceBidOnReserveAuction(&_Knownorigin0.TransactOpts, _id)
}

// PlaceBidOnReserveAuctionFor is a paid mutator transaction binding the contract method 0x7f27c67c.
//
// Solidity: function placeBidOnReserveAuctionFor(uint256 _id, address _bidder) payable returns()
func (_Knownorigin0 *Knownorigin0Transactor) PlaceBidOnReserveAuctionFor(opts *bind.TransactOpts, _id *big.Int, _bidder common.Address) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "placeBidOnReserveAuctionFor", _id, _bidder)
}

// PlaceBidOnReserveAuctionFor is a paid mutator transaction binding the contract method 0x7f27c67c.
//
// Solidity: function placeBidOnReserveAuctionFor(uint256 _id, address _bidder) payable returns()
func (_Knownorigin0 *Knownorigin0Session) PlaceBidOnReserveAuctionFor(_id *big.Int, _bidder common.Address) (*types.Transaction, error) {
	return _Knownorigin0.Contract.PlaceBidOnReserveAuctionFor(&_Knownorigin0.TransactOpts, _id, _bidder)
}

// PlaceBidOnReserveAuctionFor is a paid mutator transaction binding the contract method 0x7f27c67c.
//
// Solidity: function placeBidOnReserveAuctionFor(uint256 _id, address _bidder) payable returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) PlaceBidOnReserveAuctionFor(_id *big.Int, _bidder common.Address) (*types.Transaction, error) {
	return _Knownorigin0.Contract.PlaceBidOnReserveAuctionFor(&_Knownorigin0.TransactOpts, _id, _bidder)
}

// PlaceEditionBid is a paid mutator transaction binding the contract method 0x02ece38a.
//
// Solidity: function placeEditionBid(uint256 _editionId) payable returns()
func (_Knownorigin0 *Knownorigin0Transactor) PlaceEditionBid(opts *bind.TransactOpts, _editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "placeEditionBid", _editionId)
}

// PlaceEditionBid is a paid mutator transaction binding the contract method 0x02ece38a.
//
// Solidity: function placeEditionBid(uint256 _editionId) payable returns()
func (_Knownorigin0 *Knownorigin0Session) PlaceEditionBid(_editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.PlaceEditionBid(&_Knownorigin0.TransactOpts, _editionId)
}

// PlaceEditionBid is a paid mutator transaction binding the contract method 0x02ece38a.
//
// Solidity: function placeEditionBid(uint256 _editionId) payable returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) PlaceEditionBid(_editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.PlaceEditionBid(&_Knownorigin0.TransactOpts, _editionId)
}

// PlaceEditionBidFor is a paid mutator transaction binding the contract method 0x20cd6695.
//
// Solidity: function placeEditionBidFor(uint256 _editionId, address _bidder) payable returns()
func (_Knownorigin0 *Knownorigin0Transactor) PlaceEditionBidFor(opts *bind.TransactOpts, _editionId *big.Int, _bidder common.Address) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "placeEditionBidFor", _editionId, _bidder)
}

// PlaceEditionBidFor is a paid mutator transaction binding the contract method 0x20cd6695.
//
// Solidity: function placeEditionBidFor(uint256 _editionId, address _bidder) payable returns()
func (_Knownorigin0 *Knownorigin0Session) PlaceEditionBidFor(_editionId *big.Int, _bidder common.Address) (*types.Transaction, error) {
	return _Knownorigin0.Contract.PlaceEditionBidFor(&_Knownorigin0.TransactOpts, _editionId, _bidder)
}

// PlaceEditionBidFor is a paid mutator transaction binding the contract method 0x20cd6695.
//
// Solidity: function placeEditionBidFor(uint256 _editionId, address _bidder) payable returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) PlaceEditionBidFor(_editionId *big.Int, _bidder common.Address) (*types.Transaction, error) {
	return _Knownorigin0.Contract.PlaceEditionBidFor(&_Knownorigin0.TransactOpts, _editionId, _bidder)
}

// RecoverERC20 is a paid mutator transaction binding the contract method 0x1171bda9.
//
// Solidity: function recoverERC20(address _token, address _recipient, uint256 _amount) returns()
func (_Knownorigin0 *Knownorigin0Transactor) RecoverERC20(opts *bind.TransactOpts, _token common.Address, _recipient common.Address, _amount *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "recoverERC20", _token, _recipient, _amount)
}

// RecoverERC20 is a paid mutator transaction binding the contract method 0x1171bda9.
//
// Solidity: function recoverERC20(address _token, address _recipient, uint256 _amount) returns()
func (_Knownorigin0 *Knownorigin0Session) RecoverERC20(_token common.Address, _recipient common.Address, _amount *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.RecoverERC20(&_Knownorigin0.TransactOpts, _token, _recipient, _amount)
}

// RecoverERC20 is a paid mutator transaction binding the contract method 0x1171bda9.
//
// Solidity: function recoverERC20(address _token, address _recipient, uint256 _amount) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) RecoverERC20(_token common.Address, _recipient common.Address, _amount *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.RecoverERC20(&_Knownorigin0.TransactOpts, _token, _recipient, _amount)
}

// RecoverStuckETH is a paid mutator transaction binding the contract method 0x61200f94.
//
// Solidity: function recoverStuckETH(address _recipient, uint256 _amount) returns()
func (_Knownorigin0 *Knownorigin0Transactor) RecoverStuckETH(opts *bind.TransactOpts, _recipient common.Address, _amount *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "recoverStuckETH", _recipient, _amount)
}

// RecoverStuckETH is a paid mutator transaction binding the contract method 0x61200f94.
//
// Solidity: function recoverStuckETH(address _recipient, uint256 _amount) returns()
func (_Knownorigin0 *Knownorigin0Session) RecoverStuckETH(_recipient common.Address, _amount *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.RecoverStuckETH(&_Knownorigin0.TransactOpts, _recipient, _amount)
}

// RecoverStuckETH is a paid mutator transaction binding the contract method 0x61200f94.
//
// Solidity: function recoverStuckETH(address _recipient, uint256 _amount) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) RecoverStuckETH(_recipient common.Address, _amount *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.RecoverStuckETH(&_Knownorigin0.TransactOpts, _recipient, _amount)
}

// RejectEditionBid is a paid mutator transaction binding the contract method 0xd5df687c.
//
// Solidity: function rejectEditionBid(uint256 _editionId) returns()
func (_Knownorigin0 *Knownorigin0Transactor) RejectEditionBid(opts *bind.TransactOpts, _editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "rejectEditionBid", _editionId)
}

// RejectEditionBid is a paid mutator transaction binding the contract method 0xd5df687c.
//
// Solidity: function rejectEditionBid(uint256 _editionId) returns()
func (_Knownorigin0 *Knownorigin0Session) RejectEditionBid(_editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.RejectEditionBid(&_Knownorigin0.TransactOpts, _editionId)
}

// RejectEditionBid is a paid mutator transaction binding the contract method 0xd5df687c.
//
// Solidity: function rejectEditionBid(uint256 _editionId) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) RejectEditionBid(_editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.RejectEditionBid(&_Knownorigin0.TransactOpts, _editionId)
}

// ResultReserveAuction is a paid mutator transaction binding the contract method 0x02b43373.
//
// Solidity: function resultReserveAuction(uint256 _id) returns()
func (_Knownorigin0 *Knownorigin0Transactor) ResultReserveAuction(opts *bind.TransactOpts, _id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "resultReserveAuction", _id)
}

// ResultReserveAuction is a paid mutator transaction binding the contract method 0x02b43373.
//
// Solidity: function resultReserveAuction(uint256 _id) returns()
func (_Knownorigin0 *Knownorigin0Session) ResultReserveAuction(_id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ResultReserveAuction(&_Knownorigin0.TransactOpts, _id)
}

// ResultReserveAuction is a paid mutator transaction binding the contract method 0x02b43373.
//
// Solidity: function resultReserveAuction(uint256 _id) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) ResultReserveAuction(_id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.ResultReserveAuction(&_Knownorigin0.TransactOpts, _id)
}

// SetBuyNowPriceListing is a paid mutator transaction binding the contract method 0x4d1f0a8e.
//
// Solidity: function setBuyNowPriceListing(uint256 _id, uint128 _listingPrice) returns()
func (_Knownorigin0 *Knownorigin0Transactor) SetBuyNowPriceListing(opts *bind.TransactOpts, _id *big.Int, _listingPrice *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "setBuyNowPriceListing", _id, _listingPrice)
}

// SetBuyNowPriceListing is a paid mutator transaction binding the contract method 0x4d1f0a8e.
//
// Solidity: function setBuyNowPriceListing(uint256 _id, uint128 _listingPrice) returns()
func (_Knownorigin0 *Knownorigin0Session) SetBuyNowPriceListing(_id *big.Int, _listingPrice *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.SetBuyNowPriceListing(&_Knownorigin0.TransactOpts, _id, _listingPrice)
}

// SetBuyNowPriceListing is a paid mutator transaction binding the contract method 0x4d1f0a8e.
//
// Solidity: function setBuyNowPriceListing(uint256 _id, uint128 _listingPrice) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) SetBuyNowPriceListing(_id *big.Int, _listingPrice *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.SetBuyNowPriceListing(&_Knownorigin0.TransactOpts, _id, _listingPrice)
}

// SetKoCommissionOverrideForCreator is a paid mutator transaction binding the contract method 0x7de3766f.
//
// Solidity: function setKoCommissionOverrideForCreator(address _creator, bool _active, uint256 _koCommission) returns()
func (_Knownorigin0 *Knownorigin0Transactor) SetKoCommissionOverrideForCreator(opts *bind.TransactOpts, _creator common.Address, _active bool, _koCommission *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "setKoCommissionOverrideForCreator", _creator, _active, _koCommission)
}

// SetKoCommissionOverrideForCreator is a paid mutator transaction binding the contract method 0x7de3766f.
//
// Solidity: function setKoCommissionOverrideForCreator(address _creator, bool _active, uint256 _koCommission) returns()
func (_Knownorigin0 *Knownorigin0Session) SetKoCommissionOverrideForCreator(_creator common.Address, _active bool, _koCommission *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.SetKoCommissionOverrideForCreator(&_Knownorigin0.TransactOpts, _creator, _active, _koCommission)
}

// SetKoCommissionOverrideForCreator is a paid mutator transaction binding the contract method 0x7de3766f.
//
// Solidity: function setKoCommissionOverrideForCreator(address _creator, bool _active, uint256 _koCommission) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) SetKoCommissionOverrideForCreator(_creator common.Address, _active bool, _koCommission *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.SetKoCommissionOverrideForCreator(&_Knownorigin0.TransactOpts, _creator, _active, _koCommission)
}

// SetKoCommissionOverrideForEdition is a paid mutator transaction binding the contract method 0x0cf274c6.
//
// Solidity: function setKoCommissionOverrideForEdition(uint256 _editionId, bool _active, uint256 _koCommission) returns()
func (_Knownorigin0 *Knownorigin0Transactor) SetKoCommissionOverrideForEdition(opts *bind.TransactOpts, _editionId *big.Int, _active bool, _koCommission *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "setKoCommissionOverrideForEdition", _editionId, _active, _koCommission)
}

// SetKoCommissionOverrideForEdition is a paid mutator transaction binding the contract method 0x0cf274c6.
//
// Solidity: function setKoCommissionOverrideForEdition(uint256 _editionId, bool _active, uint256 _koCommission) returns()
func (_Knownorigin0 *Knownorigin0Session) SetKoCommissionOverrideForEdition(_editionId *big.Int, _active bool, _koCommission *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.SetKoCommissionOverrideForEdition(&_Knownorigin0.TransactOpts, _editionId, _active, _koCommission)
}

// SetKoCommissionOverrideForEdition is a paid mutator transaction binding the contract method 0x0cf274c6.
//
// Solidity: function setKoCommissionOverrideForEdition(uint256 _editionId, bool _active, uint256 _koCommission) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) SetKoCommissionOverrideForEdition(_editionId *big.Int, _active bool, _koCommission *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.SetKoCommissionOverrideForEdition(&_Knownorigin0.TransactOpts, _editionId, _active, _koCommission)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_Knownorigin0 *Knownorigin0Transactor) Unpause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "unpause")
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_Knownorigin0 *Knownorigin0Session) Unpause() (*types.Transaction, error) {
	return _Knownorigin0.Contract.Unpause(&_Knownorigin0.TransactOpts)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) Unpause() (*types.Transaction, error) {
	return _Knownorigin0.Contract.Unpause(&_Knownorigin0.TransactOpts)
}

// UpdateAccessControls is a paid mutator transaction binding the contract method 0x6ef3da94.
//
// Solidity: function updateAccessControls(address _accessControls) returns()
func (_Knownorigin0 *Knownorigin0Transactor) UpdateAccessControls(opts *bind.TransactOpts, _accessControls common.Address) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "updateAccessControls", _accessControls)
}

// UpdateAccessControls is a paid mutator transaction binding the contract method 0x6ef3da94.
//
// Solidity: function updateAccessControls(address _accessControls) returns()
func (_Knownorigin0 *Knownorigin0Session) UpdateAccessControls(_accessControls common.Address) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateAccessControls(&_Knownorigin0.TransactOpts, _accessControls)
}

// UpdateAccessControls is a paid mutator transaction binding the contract method 0x6ef3da94.
//
// Solidity: function updateAccessControls(address _accessControls) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) UpdateAccessControls(_accessControls common.Address) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateAccessControls(&_Knownorigin0.TransactOpts, _accessControls)
}

// UpdateBidLockupPeriod is a paid mutator transaction binding the contract method 0x91186443.
//
// Solidity: function updateBidLockupPeriod(uint256 _bidLockupPeriod) returns()
func (_Knownorigin0 *Knownorigin0Transactor) UpdateBidLockupPeriod(opts *bind.TransactOpts, _bidLockupPeriod *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "updateBidLockupPeriod", _bidLockupPeriod)
}

// UpdateBidLockupPeriod is a paid mutator transaction binding the contract method 0x91186443.
//
// Solidity: function updateBidLockupPeriod(uint256 _bidLockupPeriod) returns()
func (_Knownorigin0 *Knownorigin0Session) UpdateBidLockupPeriod(_bidLockupPeriod *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateBidLockupPeriod(&_Knownorigin0.TransactOpts, _bidLockupPeriod)
}

// UpdateBidLockupPeriod is a paid mutator transaction binding the contract method 0x91186443.
//
// Solidity: function updateBidLockupPeriod(uint256 _bidLockupPeriod) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) UpdateBidLockupPeriod(_bidLockupPeriod *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateBidLockupPeriod(&_Knownorigin0.TransactOpts, _bidLockupPeriod)
}

// UpdateMinBidAmount is a paid mutator transaction binding the contract method 0x6305246e.
//
// Solidity: function updateMinBidAmount(uint256 _minBidAmount) returns()
func (_Knownorigin0 *Knownorigin0Transactor) UpdateMinBidAmount(opts *bind.TransactOpts, _minBidAmount *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "updateMinBidAmount", _minBidAmount)
}

// UpdateMinBidAmount is a paid mutator transaction binding the contract method 0x6305246e.
//
// Solidity: function updateMinBidAmount(uint256 _minBidAmount) returns()
func (_Knownorigin0 *Knownorigin0Session) UpdateMinBidAmount(_minBidAmount *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateMinBidAmount(&_Knownorigin0.TransactOpts, _minBidAmount)
}

// UpdateMinBidAmount is a paid mutator transaction binding the contract method 0x6305246e.
//
// Solidity: function updateMinBidAmount(uint256 _minBidAmount) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) UpdateMinBidAmount(_minBidAmount *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateMinBidAmount(&_Knownorigin0.TransactOpts, _minBidAmount)
}

// UpdateModulo is a paid mutator transaction binding the contract method 0x72ec7eec.
//
// Solidity: function updateModulo(uint256 _modulo) returns()
func (_Knownorigin0 *Knownorigin0Transactor) UpdateModulo(opts *bind.TransactOpts, _modulo *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "updateModulo", _modulo)
}

// UpdateModulo is a paid mutator transaction binding the contract method 0x72ec7eec.
//
// Solidity: function updateModulo(uint256 _modulo) returns()
func (_Knownorigin0 *Knownorigin0Session) UpdateModulo(_modulo *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateModulo(&_Knownorigin0.TransactOpts, _modulo)
}

// UpdateModulo is a paid mutator transaction binding the contract method 0x72ec7eec.
//
// Solidity: function updateModulo(uint256 _modulo) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) UpdateModulo(_modulo *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateModulo(&_Knownorigin0.TransactOpts, _modulo)
}

// UpdatePlatformAccount is a paid mutator transaction binding the contract method 0xa1d1e564.
//
// Solidity: function updatePlatformAccount(address _newPlatformAccount) returns()
func (_Knownorigin0 *Knownorigin0Transactor) UpdatePlatformAccount(opts *bind.TransactOpts, _newPlatformAccount common.Address) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "updatePlatformAccount", _newPlatformAccount)
}

// UpdatePlatformAccount is a paid mutator transaction binding the contract method 0xa1d1e564.
//
// Solidity: function updatePlatformAccount(address _newPlatformAccount) returns()
func (_Knownorigin0 *Knownorigin0Session) UpdatePlatformAccount(_newPlatformAccount common.Address) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdatePlatformAccount(&_Knownorigin0.TransactOpts, _newPlatformAccount)
}

// UpdatePlatformAccount is a paid mutator transaction binding the contract method 0xa1d1e564.
//
// Solidity: function updatePlatformAccount(address _newPlatformAccount) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) UpdatePlatformAccount(_newPlatformAccount common.Address) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdatePlatformAccount(&_Knownorigin0.TransactOpts, _newPlatformAccount)
}

// UpdatePlatformPrimarySaleCommission is a paid mutator transaction binding the contract method 0xb4f931b1.
//
// Solidity: function updatePlatformPrimarySaleCommission(uint256 _platformPrimarySaleCommission) returns()
func (_Knownorigin0 *Knownorigin0Transactor) UpdatePlatformPrimarySaleCommission(opts *bind.TransactOpts, _platformPrimarySaleCommission *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "updatePlatformPrimarySaleCommission", _platformPrimarySaleCommission)
}

// UpdatePlatformPrimarySaleCommission is a paid mutator transaction binding the contract method 0xb4f931b1.
//
// Solidity: function updatePlatformPrimarySaleCommission(uint256 _platformPrimarySaleCommission) returns()
func (_Knownorigin0 *Knownorigin0Session) UpdatePlatformPrimarySaleCommission(_platformPrimarySaleCommission *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdatePlatformPrimarySaleCommission(&_Knownorigin0.TransactOpts, _platformPrimarySaleCommission)
}

// UpdatePlatformPrimarySaleCommission is a paid mutator transaction binding the contract method 0xb4f931b1.
//
// Solidity: function updatePlatformPrimarySaleCommission(uint256 _platformPrimarySaleCommission) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) UpdatePlatformPrimarySaleCommission(_platformPrimarySaleCommission *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdatePlatformPrimarySaleCommission(&_Knownorigin0.TransactOpts, _platformPrimarySaleCommission)
}

// UpdateReserveAuctionBidExtensionWindow is a paid mutator transaction binding the contract method 0xcf500bec.
//
// Solidity: function updateReserveAuctionBidExtensionWindow(uint128 _reserveAuctionBidExtensionWindow) returns()
func (_Knownorigin0 *Knownorigin0Transactor) UpdateReserveAuctionBidExtensionWindow(opts *bind.TransactOpts, _reserveAuctionBidExtensionWindow *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "updateReserveAuctionBidExtensionWindow", _reserveAuctionBidExtensionWindow)
}

// UpdateReserveAuctionBidExtensionWindow is a paid mutator transaction binding the contract method 0xcf500bec.
//
// Solidity: function updateReserveAuctionBidExtensionWindow(uint128 _reserveAuctionBidExtensionWindow) returns()
func (_Knownorigin0 *Knownorigin0Session) UpdateReserveAuctionBidExtensionWindow(_reserveAuctionBidExtensionWindow *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateReserveAuctionBidExtensionWindow(&_Knownorigin0.TransactOpts, _reserveAuctionBidExtensionWindow)
}

// UpdateReserveAuctionBidExtensionWindow is a paid mutator transaction binding the contract method 0xcf500bec.
//
// Solidity: function updateReserveAuctionBidExtensionWindow(uint128 _reserveAuctionBidExtensionWindow) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) UpdateReserveAuctionBidExtensionWindow(_reserveAuctionBidExtensionWindow *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateReserveAuctionBidExtensionWindow(&_Knownorigin0.TransactOpts, _reserveAuctionBidExtensionWindow)
}

// UpdateReserveAuctionLengthOnceReserveMet is a paid mutator transaction binding the contract method 0x7480911b.
//
// Solidity: function updateReserveAuctionLengthOnceReserveMet(uint128 _reserveAuctionLengthOnceReserveMet) returns()
func (_Knownorigin0 *Knownorigin0Transactor) UpdateReserveAuctionLengthOnceReserveMet(opts *bind.TransactOpts, _reserveAuctionLengthOnceReserveMet *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "updateReserveAuctionLengthOnceReserveMet", _reserveAuctionLengthOnceReserveMet)
}

// UpdateReserveAuctionLengthOnceReserveMet is a paid mutator transaction binding the contract method 0x7480911b.
//
// Solidity: function updateReserveAuctionLengthOnceReserveMet(uint128 _reserveAuctionLengthOnceReserveMet) returns()
func (_Knownorigin0 *Knownorigin0Session) UpdateReserveAuctionLengthOnceReserveMet(_reserveAuctionLengthOnceReserveMet *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateReserveAuctionLengthOnceReserveMet(&_Knownorigin0.TransactOpts, _reserveAuctionLengthOnceReserveMet)
}

// UpdateReserveAuctionLengthOnceReserveMet is a paid mutator transaction binding the contract method 0x7480911b.
//
// Solidity: function updateReserveAuctionLengthOnceReserveMet(uint128 _reserveAuctionLengthOnceReserveMet) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) UpdateReserveAuctionLengthOnceReserveMet(_reserveAuctionLengthOnceReserveMet *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateReserveAuctionLengthOnceReserveMet(&_Knownorigin0.TransactOpts, _reserveAuctionLengthOnceReserveMet)
}

// UpdateReservePriceForReserveAuction is a paid mutator transaction binding the contract method 0xd599ee08.
//
// Solidity: function updateReservePriceForReserveAuction(uint256 _id, uint128 _reservePrice) returns()
func (_Knownorigin0 *Knownorigin0Transactor) UpdateReservePriceForReserveAuction(opts *bind.TransactOpts, _id *big.Int, _reservePrice *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "updateReservePriceForReserveAuction", _id, _reservePrice)
}

// UpdateReservePriceForReserveAuction is a paid mutator transaction binding the contract method 0xd599ee08.
//
// Solidity: function updateReservePriceForReserveAuction(uint256 _id, uint128 _reservePrice) returns()
func (_Knownorigin0 *Knownorigin0Session) UpdateReservePriceForReserveAuction(_id *big.Int, _reservePrice *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateReservePriceForReserveAuction(&_Knownorigin0.TransactOpts, _id, _reservePrice)
}

// UpdateReservePriceForReserveAuction is a paid mutator transaction binding the contract method 0xd599ee08.
//
// Solidity: function updateReservePriceForReserveAuction(uint256 _id, uint128 _reservePrice) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) UpdateReservePriceForReserveAuction(_id *big.Int, _reservePrice *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateReservePriceForReserveAuction(&_Knownorigin0.TransactOpts, _id, _reservePrice)
}

// UpdateSteppedAuction is a paid mutator transaction binding the contract method 0x068bce8d.
//
// Solidity: function updateSteppedAuction(uint256 _editionId, uint128 _basePrice, uint128 _stepPrice) returns()
func (_Knownorigin0 *Knownorigin0Transactor) UpdateSteppedAuction(opts *bind.TransactOpts, _editionId *big.Int, _basePrice *big.Int, _stepPrice *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "updateSteppedAuction", _editionId, _basePrice, _stepPrice)
}

// UpdateSteppedAuction is a paid mutator transaction binding the contract method 0x068bce8d.
//
// Solidity: function updateSteppedAuction(uint256 _editionId, uint128 _basePrice, uint128 _stepPrice) returns()
func (_Knownorigin0 *Knownorigin0Session) UpdateSteppedAuction(_editionId *big.Int, _basePrice *big.Int, _stepPrice *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateSteppedAuction(&_Knownorigin0.TransactOpts, _editionId, _basePrice, _stepPrice)
}

// UpdateSteppedAuction is a paid mutator transaction binding the contract method 0x068bce8d.
//
// Solidity: function updateSteppedAuction(uint256 _editionId, uint128 _basePrice, uint128 _stepPrice) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) UpdateSteppedAuction(_editionId *big.Int, _basePrice *big.Int, _stepPrice *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.UpdateSteppedAuction(&_Knownorigin0.TransactOpts, _editionId, _basePrice, _stepPrice)
}

// WithdrawBidFromReserveAuction is a paid mutator transaction binding the contract method 0x95ac51ce.
//
// Solidity: function withdrawBidFromReserveAuction(uint256 _id) returns()
func (_Knownorigin0 *Knownorigin0Transactor) WithdrawBidFromReserveAuction(opts *bind.TransactOpts, _id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "withdrawBidFromReserveAuction", _id)
}

// WithdrawBidFromReserveAuction is a paid mutator transaction binding the contract method 0x95ac51ce.
//
// Solidity: function withdrawBidFromReserveAuction(uint256 _id) returns()
func (_Knownorigin0 *Knownorigin0Session) WithdrawBidFromReserveAuction(_id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.WithdrawBidFromReserveAuction(&_Knownorigin0.TransactOpts, _id)
}

// WithdrawBidFromReserveAuction is a paid mutator transaction binding the contract method 0x95ac51ce.
//
// Solidity: function withdrawBidFromReserveAuction(uint256 _id) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) WithdrawBidFromReserveAuction(_id *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.WithdrawBidFromReserveAuction(&_Knownorigin0.TransactOpts, _id)
}

// WithdrawEditionBid is a paid mutator transaction binding the contract method 0xc30f0db2.
//
// Solidity: function withdrawEditionBid(uint256 _editionId) returns()
func (_Knownorigin0 *Knownorigin0Transactor) WithdrawEditionBid(opts *bind.TransactOpts, _editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.contract.Transact(opts, "withdrawEditionBid", _editionId)
}

// WithdrawEditionBid is a paid mutator transaction binding the contract method 0xc30f0db2.
//
// Solidity: function withdrawEditionBid(uint256 _editionId) returns()
func (_Knownorigin0 *Knownorigin0Session) WithdrawEditionBid(_editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.WithdrawEditionBid(&_Knownorigin0.TransactOpts, _editionId)
}

// WithdrawEditionBid is a paid mutator transaction binding the contract method 0xc30f0db2.
//
// Solidity: function withdrawEditionBid(uint256 _editionId) returns()
func (_Knownorigin0 *Knownorigin0TransactorSession) WithdrawEditionBid(_editionId *big.Int) (*types.Transaction, error) {
	return _Knownorigin0.Contract.WithdrawEditionBid(&_Knownorigin0.TransactOpts, _editionId)
}

// Knownorigin0AdminRecoverERC20Iterator is returned from FilterAdminRecoverERC20 and is used to iterate over the raw logs and unpacked data for AdminRecoverERC20 events raised by the Knownorigin0 contract.
type Knownorigin0AdminRecoverERC20Iterator struct {
	Event *Knownorigin0AdminRecoverERC20 // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0AdminRecoverERC20Iterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0AdminRecoverERC20)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0AdminRecoverERC20)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0AdminRecoverERC20Iterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0AdminRecoverERC20Iterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0AdminRecoverERC20 represents a AdminRecoverERC20 event raised by the Knownorigin0 contract.
type Knownorigin0AdminRecoverERC20 struct {
	Token     common.Address
	Recipient common.Address
	Amount    *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterAdminRecoverERC20 is a free log retrieval operation binding the contract event 0x305fd4b7f973d3d7ee0074e0c7b4db74db3bbf0c21b17bf10e003985e4fc9f7b.
//
// Solidity: event AdminRecoverERC20(address indexed _token, address indexed _recipient, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) FilterAdminRecoverERC20(opts *bind.FilterOpts, _token []common.Address, _recipient []common.Address) (*Knownorigin0AdminRecoverERC20Iterator, error) {

	var _tokenRule []interface{}
	for _, _tokenItem := range _token {
		_tokenRule = append(_tokenRule, _tokenItem)
	}
	var _recipientRule []interface{}
	for _, _recipientItem := range _recipient {
		_recipientRule = append(_recipientRule, _recipientItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "AdminRecoverERC20", _tokenRule, _recipientRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0AdminRecoverERC20Iterator{contract: _Knownorigin0.contract, event: "AdminRecoverERC20", logs: logs, sub: sub}, nil
}

// WatchAdminRecoverERC20 is a free log subscription operation binding the contract event 0x305fd4b7f973d3d7ee0074e0c7b4db74db3bbf0c21b17bf10e003985e4fc9f7b.
//
// Solidity: event AdminRecoverERC20(address indexed _token, address indexed _recipient, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) WatchAdminRecoverERC20(opts *bind.WatchOpts, sink chan<- *Knownorigin0AdminRecoverERC20, _token []common.Address, _recipient []common.Address) (event.Subscription, error) {

	var _tokenRule []interface{}
	for _, _tokenItem := range _token {
		_tokenRule = append(_tokenRule, _tokenItem)
	}
	var _recipientRule []interface{}
	for _, _recipientItem := range _recipient {
		_recipientRule = append(_recipientRule, _recipientItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "AdminRecoverERC20", _tokenRule, _recipientRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0AdminRecoverERC20)
				if err := _Knownorigin0.contract.UnpackLog(event, "AdminRecoverERC20", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminRecoverERC20 is a log parse operation binding the contract event 0x305fd4b7f973d3d7ee0074e0c7b4db74db3bbf0c21b17bf10e003985e4fc9f7b.
//
// Solidity: event AdminRecoverERC20(address indexed _token, address indexed _recipient, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) ParseAdminRecoverERC20(log types.Log) (*Knownorigin0AdminRecoverERC20, error) {
	event := new(Knownorigin0AdminRecoverERC20)
	if err := _Knownorigin0.contract.UnpackLog(event, "AdminRecoverERC20", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0AdminRecoverETHIterator is returned from FilterAdminRecoverETH and is used to iterate over the raw logs and unpacked data for AdminRecoverETH events raised by the Knownorigin0 contract.
type Knownorigin0AdminRecoverETHIterator struct {
	Event *Knownorigin0AdminRecoverETH // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0AdminRecoverETHIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0AdminRecoverETH)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0AdminRecoverETH)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0AdminRecoverETHIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0AdminRecoverETHIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0AdminRecoverETH represents a AdminRecoverETH event raised by the Knownorigin0 contract.
type Knownorigin0AdminRecoverETH struct {
	Recipient common.Address
	Amount    *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterAdminRecoverETH is a free log retrieval operation binding the contract event 0x4d688afe1abc567d30e199aa99174c72a9653aec3e1e87dd2b5e60ed469fad55.
//
// Solidity: event AdminRecoverETH(address indexed _recipient, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) FilterAdminRecoverETH(opts *bind.FilterOpts, _recipient []common.Address) (*Knownorigin0AdminRecoverETHIterator, error) {

	var _recipientRule []interface{}
	for _, _recipientItem := range _recipient {
		_recipientRule = append(_recipientRule, _recipientItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "AdminRecoverETH", _recipientRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0AdminRecoverETHIterator{contract: _Knownorigin0.contract, event: "AdminRecoverETH", logs: logs, sub: sub}, nil
}

// WatchAdminRecoverETH is a free log subscription operation binding the contract event 0x4d688afe1abc567d30e199aa99174c72a9653aec3e1e87dd2b5e60ed469fad55.
//
// Solidity: event AdminRecoverETH(address indexed _recipient, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) WatchAdminRecoverETH(opts *bind.WatchOpts, sink chan<- *Knownorigin0AdminRecoverETH, _recipient []common.Address) (event.Subscription, error) {

	var _recipientRule []interface{}
	for _, _recipientItem := range _recipient {
		_recipientRule = append(_recipientRule, _recipientItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "AdminRecoverETH", _recipientRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0AdminRecoverETH)
				if err := _Knownorigin0.contract.UnpackLog(event, "AdminRecoverETH", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminRecoverETH is a log parse operation binding the contract event 0x4d688afe1abc567d30e199aa99174c72a9653aec3e1e87dd2b5e60ed469fad55.
//
// Solidity: event AdminRecoverETH(address indexed _recipient, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) ParseAdminRecoverETH(log types.Log) (*Knownorigin0AdminRecoverETH, error) {
	event := new(Knownorigin0AdminRecoverETH)
	if err := _Knownorigin0.contract.UnpackLog(event, "AdminRecoverETH", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0AdminSetKoCommissionOverrideForCreatorIterator is returned from FilterAdminSetKoCommissionOverrideForCreator and is used to iterate over the raw logs and unpacked data for AdminSetKoCommissionOverrideForCreator events raised by the Knownorigin0 contract.
type Knownorigin0AdminSetKoCommissionOverrideForCreatorIterator struct {
	Event *Knownorigin0AdminSetKoCommissionOverrideForCreator // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0AdminSetKoCommissionOverrideForCreatorIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0AdminSetKoCommissionOverrideForCreator)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0AdminSetKoCommissionOverrideForCreator)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0AdminSetKoCommissionOverrideForCreatorIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0AdminSetKoCommissionOverrideForCreatorIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0AdminSetKoCommissionOverrideForCreator represents a AdminSetKoCommissionOverrideForCreator event raised by the Knownorigin0 contract.
type Knownorigin0AdminSetKoCommissionOverrideForCreator struct {
	Creator      common.Address
	KoCommission *big.Int
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterAdminSetKoCommissionOverrideForCreator is a free log retrieval operation binding the contract event 0x036d5b8a890813b20be995ed13873cba592f6933b7f9c5d342b258f72ede6e78.
//
// Solidity: event AdminSetKoCommissionOverrideForCreator(address indexed _creator, uint256 _koCommission)
func (_Knownorigin0 *Knownorigin0Filterer) FilterAdminSetKoCommissionOverrideForCreator(opts *bind.FilterOpts, _creator []common.Address) (*Knownorigin0AdminSetKoCommissionOverrideForCreatorIterator, error) {

	var _creatorRule []interface{}
	for _, _creatorItem := range _creator {
		_creatorRule = append(_creatorRule, _creatorItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "AdminSetKoCommissionOverrideForCreator", _creatorRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0AdminSetKoCommissionOverrideForCreatorIterator{contract: _Knownorigin0.contract, event: "AdminSetKoCommissionOverrideForCreator", logs: logs, sub: sub}, nil
}

// WatchAdminSetKoCommissionOverrideForCreator is a free log subscription operation binding the contract event 0x036d5b8a890813b20be995ed13873cba592f6933b7f9c5d342b258f72ede6e78.
//
// Solidity: event AdminSetKoCommissionOverrideForCreator(address indexed _creator, uint256 _koCommission)
func (_Knownorigin0 *Knownorigin0Filterer) WatchAdminSetKoCommissionOverrideForCreator(opts *bind.WatchOpts, sink chan<- *Knownorigin0AdminSetKoCommissionOverrideForCreator, _creator []common.Address) (event.Subscription, error) {

	var _creatorRule []interface{}
	for _, _creatorItem := range _creator {
		_creatorRule = append(_creatorRule, _creatorItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "AdminSetKoCommissionOverrideForCreator", _creatorRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0AdminSetKoCommissionOverrideForCreator)
				if err := _Knownorigin0.contract.UnpackLog(event, "AdminSetKoCommissionOverrideForCreator", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminSetKoCommissionOverrideForCreator is a log parse operation binding the contract event 0x036d5b8a890813b20be995ed13873cba592f6933b7f9c5d342b258f72ede6e78.
//
// Solidity: event AdminSetKoCommissionOverrideForCreator(address indexed _creator, uint256 _koCommission)
func (_Knownorigin0 *Knownorigin0Filterer) ParseAdminSetKoCommissionOverrideForCreator(log types.Log) (*Knownorigin0AdminSetKoCommissionOverrideForCreator, error) {
	event := new(Knownorigin0AdminSetKoCommissionOverrideForCreator)
	if err := _Knownorigin0.contract.UnpackLog(event, "AdminSetKoCommissionOverrideForCreator", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0AdminSetKoCommissionOverrideForEditionIterator is returned from FilterAdminSetKoCommissionOverrideForEdition and is used to iterate over the raw logs and unpacked data for AdminSetKoCommissionOverrideForEdition events raised by the Knownorigin0 contract.
type Knownorigin0AdminSetKoCommissionOverrideForEditionIterator struct {
	Event *Knownorigin0AdminSetKoCommissionOverrideForEdition // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0AdminSetKoCommissionOverrideForEditionIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0AdminSetKoCommissionOverrideForEdition)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0AdminSetKoCommissionOverrideForEdition)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0AdminSetKoCommissionOverrideForEditionIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0AdminSetKoCommissionOverrideForEditionIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0AdminSetKoCommissionOverrideForEdition represents a AdminSetKoCommissionOverrideForEdition event raised by the Knownorigin0 contract.
type Knownorigin0AdminSetKoCommissionOverrideForEdition struct {
	EditionId    *big.Int
	KoCommission *big.Int
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterAdminSetKoCommissionOverrideForEdition is a free log retrieval operation binding the contract event 0x3435d39114497a2c6d70dbcc09a4026fdaca4c8eb9661f55f026fd79c0a28508.
//
// Solidity: event AdminSetKoCommissionOverrideForEdition(uint256 indexed _editionId, uint256 _koCommission)
func (_Knownorigin0 *Knownorigin0Filterer) FilterAdminSetKoCommissionOverrideForEdition(opts *bind.FilterOpts, _editionId []*big.Int) (*Knownorigin0AdminSetKoCommissionOverrideForEditionIterator, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "AdminSetKoCommissionOverrideForEdition", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0AdminSetKoCommissionOverrideForEditionIterator{contract: _Knownorigin0.contract, event: "AdminSetKoCommissionOverrideForEdition", logs: logs, sub: sub}, nil
}

// WatchAdminSetKoCommissionOverrideForEdition is a free log subscription operation binding the contract event 0x3435d39114497a2c6d70dbcc09a4026fdaca4c8eb9661f55f026fd79c0a28508.
//
// Solidity: event AdminSetKoCommissionOverrideForEdition(uint256 indexed _editionId, uint256 _koCommission)
func (_Knownorigin0 *Knownorigin0Filterer) WatchAdminSetKoCommissionOverrideForEdition(opts *bind.WatchOpts, sink chan<- *Knownorigin0AdminSetKoCommissionOverrideForEdition, _editionId []*big.Int) (event.Subscription, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "AdminSetKoCommissionOverrideForEdition", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0AdminSetKoCommissionOverrideForEdition)
				if err := _Knownorigin0.contract.UnpackLog(event, "AdminSetKoCommissionOverrideForEdition", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminSetKoCommissionOverrideForEdition is a log parse operation binding the contract event 0x3435d39114497a2c6d70dbcc09a4026fdaca4c8eb9661f55f026fd79c0a28508.
//
// Solidity: event AdminSetKoCommissionOverrideForEdition(uint256 indexed _editionId, uint256 _koCommission)
func (_Knownorigin0 *Knownorigin0Filterer) ParseAdminSetKoCommissionOverrideForEdition(log types.Log) (*Knownorigin0AdminSetKoCommissionOverrideForEdition, error) {
	event := new(Knownorigin0AdminSetKoCommissionOverrideForEdition)
	if err := _Knownorigin0.contract.UnpackLog(event, "AdminSetKoCommissionOverrideForEdition", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0AdminUpdateAccessControlsIterator is returned from FilterAdminUpdateAccessControls and is used to iterate over the raw logs and unpacked data for AdminUpdateAccessControls events raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdateAccessControlsIterator struct {
	Event *Knownorigin0AdminUpdateAccessControls // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0AdminUpdateAccessControlsIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0AdminUpdateAccessControls)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0AdminUpdateAccessControls)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0AdminUpdateAccessControlsIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0AdminUpdateAccessControlsIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0AdminUpdateAccessControls represents a AdminUpdateAccessControls event raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdateAccessControls struct {
	OldAddress common.Address
	NewAddress common.Address
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterAdminUpdateAccessControls is a free log retrieval operation binding the contract event 0xacd428448d30f3fadee1e7d643a28cfee0ec86aef546e1cfc717d844b99eedf2.
//
// Solidity: event AdminUpdateAccessControls(address indexed _oldAddress, address indexed _newAddress)
func (_Knownorigin0 *Knownorigin0Filterer) FilterAdminUpdateAccessControls(opts *bind.FilterOpts, _oldAddress []common.Address, _newAddress []common.Address) (*Knownorigin0AdminUpdateAccessControlsIterator, error) {

	var _oldAddressRule []interface{}
	for _, _oldAddressItem := range _oldAddress {
		_oldAddressRule = append(_oldAddressRule, _oldAddressItem)
	}
	var _newAddressRule []interface{}
	for _, _newAddressItem := range _newAddress {
		_newAddressRule = append(_newAddressRule, _newAddressItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "AdminUpdateAccessControls", _oldAddressRule, _newAddressRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0AdminUpdateAccessControlsIterator{contract: _Knownorigin0.contract, event: "AdminUpdateAccessControls", logs: logs, sub: sub}, nil
}

// WatchAdminUpdateAccessControls is a free log subscription operation binding the contract event 0xacd428448d30f3fadee1e7d643a28cfee0ec86aef546e1cfc717d844b99eedf2.
//
// Solidity: event AdminUpdateAccessControls(address indexed _oldAddress, address indexed _newAddress)
func (_Knownorigin0 *Knownorigin0Filterer) WatchAdminUpdateAccessControls(opts *bind.WatchOpts, sink chan<- *Knownorigin0AdminUpdateAccessControls, _oldAddress []common.Address, _newAddress []common.Address) (event.Subscription, error) {

	var _oldAddressRule []interface{}
	for _, _oldAddressItem := range _oldAddress {
		_oldAddressRule = append(_oldAddressRule, _oldAddressItem)
	}
	var _newAddressRule []interface{}
	for _, _newAddressItem := range _newAddress {
		_newAddressRule = append(_newAddressRule, _newAddressItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "AdminUpdateAccessControls", _oldAddressRule, _newAddressRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0AdminUpdateAccessControls)
				if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdateAccessControls", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminUpdateAccessControls is a log parse operation binding the contract event 0xacd428448d30f3fadee1e7d643a28cfee0ec86aef546e1cfc717d844b99eedf2.
//
// Solidity: event AdminUpdateAccessControls(address indexed _oldAddress, address indexed _newAddress)
func (_Knownorigin0 *Knownorigin0Filterer) ParseAdminUpdateAccessControls(log types.Log) (*Knownorigin0AdminUpdateAccessControls, error) {
	event := new(Knownorigin0AdminUpdateAccessControls)
	if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdateAccessControls", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0AdminUpdateBidLockupPeriodIterator is returned from FilterAdminUpdateBidLockupPeriod and is used to iterate over the raw logs and unpacked data for AdminUpdateBidLockupPeriod events raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdateBidLockupPeriodIterator struct {
	Event *Knownorigin0AdminUpdateBidLockupPeriod // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0AdminUpdateBidLockupPeriodIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0AdminUpdateBidLockupPeriod)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0AdminUpdateBidLockupPeriod)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0AdminUpdateBidLockupPeriodIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0AdminUpdateBidLockupPeriodIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0AdminUpdateBidLockupPeriod represents a AdminUpdateBidLockupPeriod event raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdateBidLockupPeriod struct {
	BidLockupPeriod *big.Int
	Raw             types.Log // Blockchain specific contextual infos
}

// FilterAdminUpdateBidLockupPeriod is a free log retrieval operation binding the contract event 0x46b8c32fce68a5f2dff628d3b2cf9374788f42ea33b343728a64a3b090b4479e.
//
// Solidity: event AdminUpdateBidLockupPeriod(uint256 _bidLockupPeriod)
func (_Knownorigin0 *Knownorigin0Filterer) FilterAdminUpdateBidLockupPeriod(opts *bind.FilterOpts) (*Knownorigin0AdminUpdateBidLockupPeriodIterator, error) {

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "AdminUpdateBidLockupPeriod")
	if err != nil {
		return nil, err
	}
	return &Knownorigin0AdminUpdateBidLockupPeriodIterator{contract: _Knownorigin0.contract, event: "AdminUpdateBidLockupPeriod", logs: logs, sub: sub}, nil
}

// WatchAdminUpdateBidLockupPeriod is a free log subscription operation binding the contract event 0x46b8c32fce68a5f2dff628d3b2cf9374788f42ea33b343728a64a3b090b4479e.
//
// Solidity: event AdminUpdateBidLockupPeriod(uint256 _bidLockupPeriod)
func (_Knownorigin0 *Knownorigin0Filterer) WatchAdminUpdateBidLockupPeriod(opts *bind.WatchOpts, sink chan<- *Knownorigin0AdminUpdateBidLockupPeriod) (event.Subscription, error) {

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "AdminUpdateBidLockupPeriod")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0AdminUpdateBidLockupPeriod)
				if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdateBidLockupPeriod", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminUpdateBidLockupPeriod is a log parse operation binding the contract event 0x46b8c32fce68a5f2dff628d3b2cf9374788f42ea33b343728a64a3b090b4479e.
//
// Solidity: event AdminUpdateBidLockupPeriod(uint256 _bidLockupPeriod)
func (_Knownorigin0 *Knownorigin0Filterer) ParseAdminUpdateBidLockupPeriod(log types.Log) (*Knownorigin0AdminUpdateBidLockupPeriod, error) {
	event := new(Knownorigin0AdminUpdateBidLockupPeriod)
	if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdateBidLockupPeriod", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0AdminUpdateMinBidAmountIterator is returned from FilterAdminUpdateMinBidAmount and is used to iterate over the raw logs and unpacked data for AdminUpdateMinBidAmount events raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdateMinBidAmountIterator struct {
	Event *Knownorigin0AdminUpdateMinBidAmount // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0AdminUpdateMinBidAmountIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0AdminUpdateMinBidAmount)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0AdminUpdateMinBidAmount)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0AdminUpdateMinBidAmountIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0AdminUpdateMinBidAmountIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0AdminUpdateMinBidAmount represents a AdminUpdateMinBidAmount event raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdateMinBidAmount struct {
	MinBidAmount *big.Int
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterAdminUpdateMinBidAmount is a free log retrieval operation binding the contract event 0x0e782e841a04fe2a18adf67ea33f1becfdca41420dee80c4c28d966524f2e6e7.
//
// Solidity: event AdminUpdateMinBidAmount(uint256 _minBidAmount)
func (_Knownorigin0 *Knownorigin0Filterer) FilterAdminUpdateMinBidAmount(opts *bind.FilterOpts) (*Knownorigin0AdminUpdateMinBidAmountIterator, error) {

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "AdminUpdateMinBidAmount")
	if err != nil {
		return nil, err
	}
	return &Knownorigin0AdminUpdateMinBidAmountIterator{contract: _Knownorigin0.contract, event: "AdminUpdateMinBidAmount", logs: logs, sub: sub}, nil
}

// WatchAdminUpdateMinBidAmount is a free log subscription operation binding the contract event 0x0e782e841a04fe2a18adf67ea33f1becfdca41420dee80c4c28d966524f2e6e7.
//
// Solidity: event AdminUpdateMinBidAmount(uint256 _minBidAmount)
func (_Knownorigin0 *Knownorigin0Filterer) WatchAdminUpdateMinBidAmount(opts *bind.WatchOpts, sink chan<- *Knownorigin0AdminUpdateMinBidAmount) (event.Subscription, error) {

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "AdminUpdateMinBidAmount")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0AdminUpdateMinBidAmount)
				if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdateMinBidAmount", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminUpdateMinBidAmount is a log parse operation binding the contract event 0x0e782e841a04fe2a18adf67ea33f1becfdca41420dee80c4c28d966524f2e6e7.
//
// Solidity: event AdminUpdateMinBidAmount(uint256 _minBidAmount)
func (_Knownorigin0 *Knownorigin0Filterer) ParseAdminUpdateMinBidAmount(log types.Log) (*Knownorigin0AdminUpdateMinBidAmount, error) {
	event := new(Knownorigin0AdminUpdateMinBidAmount)
	if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdateMinBidAmount", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0AdminUpdateModuloIterator is returned from FilterAdminUpdateModulo and is used to iterate over the raw logs and unpacked data for AdminUpdateModulo events raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdateModuloIterator struct {
	Event *Knownorigin0AdminUpdateModulo // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0AdminUpdateModuloIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0AdminUpdateModulo)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0AdminUpdateModulo)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0AdminUpdateModuloIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0AdminUpdateModuloIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0AdminUpdateModulo represents a AdminUpdateModulo event raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdateModulo struct {
	Modulo *big.Int
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterAdminUpdateModulo is a free log retrieval operation binding the contract event 0x0974c11e488a74018e3da44e0750b7d0ec99a431c45f79a6110d722b823d0af0.
//
// Solidity: event AdminUpdateModulo(uint256 _modulo)
func (_Knownorigin0 *Knownorigin0Filterer) FilterAdminUpdateModulo(opts *bind.FilterOpts) (*Knownorigin0AdminUpdateModuloIterator, error) {

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "AdminUpdateModulo")
	if err != nil {
		return nil, err
	}
	return &Knownorigin0AdminUpdateModuloIterator{contract: _Knownorigin0.contract, event: "AdminUpdateModulo", logs: logs, sub: sub}, nil
}

// WatchAdminUpdateModulo is a free log subscription operation binding the contract event 0x0974c11e488a74018e3da44e0750b7d0ec99a431c45f79a6110d722b823d0af0.
//
// Solidity: event AdminUpdateModulo(uint256 _modulo)
func (_Knownorigin0 *Knownorigin0Filterer) WatchAdminUpdateModulo(opts *bind.WatchOpts, sink chan<- *Knownorigin0AdminUpdateModulo) (event.Subscription, error) {

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "AdminUpdateModulo")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0AdminUpdateModulo)
				if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdateModulo", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminUpdateModulo is a log parse operation binding the contract event 0x0974c11e488a74018e3da44e0750b7d0ec99a431c45f79a6110d722b823d0af0.
//
// Solidity: event AdminUpdateModulo(uint256 _modulo)
func (_Knownorigin0 *Knownorigin0Filterer) ParseAdminUpdateModulo(log types.Log) (*Knownorigin0AdminUpdateModulo, error) {
	event := new(Knownorigin0AdminUpdateModulo)
	if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdateModulo", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0AdminUpdatePlatformAccountIterator is returned from FilterAdminUpdatePlatformAccount and is used to iterate over the raw logs and unpacked data for AdminUpdatePlatformAccount events raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdatePlatformAccountIterator struct {
	Event *Knownorigin0AdminUpdatePlatformAccount // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0AdminUpdatePlatformAccountIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0AdminUpdatePlatformAccount)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0AdminUpdatePlatformAccount)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0AdminUpdatePlatformAccountIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0AdminUpdatePlatformAccountIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0AdminUpdatePlatformAccount represents a AdminUpdatePlatformAccount event raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdatePlatformAccount struct {
	OldAddress common.Address
	NewAddress common.Address
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterAdminUpdatePlatformAccount is a free log retrieval operation binding the contract event 0x0dcf4ed468afaf8b36d6f5982788e2b86c18a544e7fb05803a79f67cd097d591.
//
// Solidity: event AdminUpdatePlatformAccount(address indexed _oldAddress, address indexed _newAddress)
func (_Knownorigin0 *Knownorigin0Filterer) FilterAdminUpdatePlatformAccount(opts *bind.FilterOpts, _oldAddress []common.Address, _newAddress []common.Address) (*Knownorigin0AdminUpdatePlatformAccountIterator, error) {

	var _oldAddressRule []interface{}
	for _, _oldAddressItem := range _oldAddress {
		_oldAddressRule = append(_oldAddressRule, _oldAddressItem)
	}
	var _newAddressRule []interface{}
	for _, _newAddressItem := range _newAddress {
		_newAddressRule = append(_newAddressRule, _newAddressItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "AdminUpdatePlatformAccount", _oldAddressRule, _newAddressRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0AdminUpdatePlatformAccountIterator{contract: _Knownorigin0.contract, event: "AdminUpdatePlatformAccount", logs: logs, sub: sub}, nil
}

// WatchAdminUpdatePlatformAccount is a free log subscription operation binding the contract event 0x0dcf4ed468afaf8b36d6f5982788e2b86c18a544e7fb05803a79f67cd097d591.
//
// Solidity: event AdminUpdatePlatformAccount(address indexed _oldAddress, address indexed _newAddress)
func (_Knownorigin0 *Knownorigin0Filterer) WatchAdminUpdatePlatformAccount(opts *bind.WatchOpts, sink chan<- *Knownorigin0AdminUpdatePlatformAccount, _oldAddress []common.Address, _newAddress []common.Address) (event.Subscription, error) {

	var _oldAddressRule []interface{}
	for _, _oldAddressItem := range _oldAddress {
		_oldAddressRule = append(_oldAddressRule, _oldAddressItem)
	}
	var _newAddressRule []interface{}
	for _, _newAddressItem := range _newAddress {
		_newAddressRule = append(_newAddressRule, _newAddressItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "AdminUpdatePlatformAccount", _oldAddressRule, _newAddressRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0AdminUpdatePlatformAccount)
				if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdatePlatformAccount", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminUpdatePlatformAccount is a log parse operation binding the contract event 0x0dcf4ed468afaf8b36d6f5982788e2b86c18a544e7fb05803a79f67cd097d591.
//
// Solidity: event AdminUpdatePlatformAccount(address indexed _oldAddress, address indexed _newAddress)
func (_Knownorigin0 *Knownorigin0Filterer) ParseAdminUpdatePlatformAccount(log types.Log) (*Knownorigin0AdminUpdatePlatformAccount, error) {
	event := new(Knownorigin0AdminUpdatePlatformAccount)
	if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdatePlatformAccount", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0AdminUpdatePlatformPrimarySaleCommissionIterator is returned from FilterAdminUpdatePlatformPrimarySaleCommission and is used to iterate over the raw logs and unpacked data for AdminUpdatePlatformPrimarySaleCommission events raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdatePlatformPrimarySaleCommissionIterator struct {
	Event *Knownorigin0AdminUpdatePlatformPrimarySaleCommission // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0AdminUpdatePlatformPrimarySaleCommissionIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0AdminUpdatePlatformPrimarySaleCommission)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0AdminUpdatePlatformPrimarySaleCommission)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0AdminUpdatePlatformPrimarySaleCommissionIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0AdminUpdatePlatformPrimarySaleCommissionIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0AdminUpdatePlatformPrimarySaleCommission represents a AdminUpdatePlatformPrimarySaleCommission event raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdatePlatformPrimarySaleCommission struct {
	PlatformPrimarySaleCommission *big.Int
	Raw                           types.Log // Blockchain specific contextual infos
}

// FilterAdminUpdatePlatformPrimarySaleCommission is a free log retrieval operation binding the contract event 0xddddb61577530b164743cafe83935aeddb8888d7de39faeea75fd888bd1dcc75.
//
// Solidity: event AdminUpdatePlatformPrimarySaleCommission(uint256 _platformPrimarySaleCommission)
func (_Knownorigin0 *Knownorigin0Filterer) FilterAdminUpdatePlatformPrimarySaleCommission(opts *bind.FilterOpts) (*Knownorigin0AdminUpdatePlatformPrimarySaleCommissionIterator, error) {

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "AdminUpdatePlatformPrimarySaleCommission")
	if err != nil {
		return nil, err
	}
	return &Knownorigin0AdminUpdatePlatformPrimarySaleCommissionIterator{contract: _Knownorigin0.contract, event: "AdminUpdatePlatformPrimarySaleCommission", logs: logs, sub: sub}, nil
}

// WatchAdminUpdatePlatformPrimarySaleCommission is a free log subscription operation binding the contract event 0xddddb61577530b164743cafe83935aeddb8888d7de39faeea75fd888bd1dcc75.
//
// Solidity: event AdminUpdatePlatformPrimarySaleCommission(uint256 _platformPrimarySaleCommission)
func (_Knownorigin0 *Knownorigin0Filterer) WatchAdminUpdatePlatformPrimarySaleCommission(opts *bind.WatchOpts, sink chan<- *Knownorigin0AdminUpdatePlatformPrimarySaleCommission) (event.Subscription, error) {

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "AdminUpdatePlatformPrimarySaleCommission")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0AdminUpdatePlatformPrimarySaleCommission)
				if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdatePlatformPrimarySaleCommission", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminUpdatePlatformPrimarySaleCommission is a log parse operation binding the contract event 0xddddb61577530b164743cafe83935aeddb8888d7de39faeea75fd888bd1dcc75.
//
// Solidity: event AdminUpdatePlatformPrimarySaleCommission(uint256 _platformPrimarySaleCommission)
func (_Knownorigin0 *Knownorigin0Filterer) ParseAdminUpdatePlatformPrimarySaleCommission(log types.Log) (*Knownorigin0AdminUpdatePlatformPrimarySaleCommission, error) {
	event := new(Knownorigin0AdminUpdatePlatformPrimarySaleCommission)
	if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdatePlatformPrimarySaleCommission", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0AdminUpdateReserveAuctionBidExtensionWindowIterator is returned from FilterAdminUpdateReserveAuctionBidExtensionWindow and is used to iterate over the raw logs and unpacked data for AdminUpdateReserveAuctionBidExtensionWindow events raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdateReserveAuctionBidExtensionWindowIterator struct {
	Event *Knownorigin0AdminUpdateReserveAuctionBidExtensionWindow // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0AdminUpdateReserveAuctionBidExtensionWindowIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0AdminUpdateReserveAuctionBidExtensionWindow)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0AdminUpdateReserveAuctionBidExtensionWindow)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0AdminUpdateReserveAuctionBidExtensionWindowIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0AdminUpdateReserveAuctionBidExtensionWindowIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0AdminUpdateReserveAuctionBidExtensionWindow represents a AdminUpdateReserveAuctionBidExtensionWindow event raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdateReserveAuctionBidExtensionWindow struct {
	ReserveAuctionBidExtensionWindow *big.Int
	Raw                              types.Log // Blockchain specific contextual infos
}

// FilterAdminUpdateReserveAuctionBidExtensionWindow is a free log retrieval operation binding the contract event 0x0db06cc6a30966b8f617c70443f80200492161af831f75a335f3c3820e71bacd.
//
// Solidity: event AdminUpdateReserveAuctionBidExtensionWindow(uint128 _reserveAuctionBidExtensionWindow)
func (_Knownorigin0 *Knownorigin0Filterer) FilterAdminUpdateReserveAuctionBidExtensionWindow(opts *bind.FilterOpts) (*Knownorigin0AdminUpdateReserveAuctionBidExtensionWindowIterator, error) {

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "AdminUpdateReserveAuctionBidExtensionWindow")
	if err != nil {
		return nil, err
	}
	return &Knownorigin0AdminUpdateReserveAuctionBidExtensionWindowIterator{contract: _Knownorigin0.contract, event: "AdminUpdateReserveAuctionBidExtensionWindow", logs: logs, sub: sub}, nil
}

// WatchAdminUpdateReserveAuctionBidExtensionWindow is a free log subscription operation binding the contract event 0x0db06cc6a30966b8f617c70443f80200492161af831f75a335f3c3820e71bacd.
//
// Solidity: event AdminUpdateReserveAuctionBidExtensionWindow(uint128 _reserveAuctionBidExtensionWindow)
func (_Knownorigin0 *Knownorigin0Filterer) WatchAdminUpdateReserveAuctionBidExtensionWindow(opts *bind.WatchOpts, sink chan<- *Knownorigin0AdminUpdateReserveAuctionBidExtensionWindow) (event.Subscription, error) {

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "AdminUpdateReserveAuctionBidExtensionWindow")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0AdminUpdateReserveAuctionBidExtensionWindow)
				if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdateReserveAuctionBidExtensionWindow", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminUpdateReserveAuctionBidExtensionWindow is a log parse operation binding the contract event 0x0db06cc6a30966b8f617c70443f80200492161af831f75a335f3c3820e71bacd.
//
// Solidity: event AdminUpdateReserveAuctionBidExtensionWindow(uint128 _reserveAuctionBidExtensionWindow)
func (_Knownorigin0 *Knownorigin0Filterer) ParseAdminUpdateReserveAuctionBidExtensionWindow(log types.Log) (*Knownorigin0AdminUpdateReserveAuctionBidExtensionWindow, error) {
	event := new(Knownorigin0AdminUpdateReserveAuctionBidExtensionWindow)
	if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdateReserveAuctionBidExtensionWindow", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMetIterator is returned from FilterAdminUpdateReserveAuctionLengthOnceReserveMet and is used to iterate over the raw logs and unpacked data for AdminUpdateReserveAuctionLengthOnceReserveMet events raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMetIterator struct {
	Event *Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMet // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMet)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMet)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMet represents a AdminUpdateReserveAuctionLengthOnceReserveMet event raised by the Knownorigin0 contract.
type Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMet struct {
	ReserveAuctionLengthOnceReserveMet *big.Int
	Raw                                types.Log // Blockchain specific contextual infos
}

// FilterAdminUpdateReserveAuctionLengthOnceReserveMet is a free log retrieval operation binding the contract event 0xa076ed7cc197f8c245d58d44bfdc92180b22f8e3ddd247c45ee0e84f79f7b2bd.
//
// Solidity: event AdminUpdateReserveAuctionLengthOnceReserveMet(uint128 _reserveAuctionLengthOnceReserveMet)
func (_Knownorigin0 *Knownorigin0Filterer) FilterAdminUpdateReserveAuctionLengthOnceReserveMet(opts *bind.FilterOpts) (*Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMetIterator, error) {

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "AdminUpdateReserveAuctionLengthOnceReserveMet")
	if err != nil {
		return nil, err
	}
	return &Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMetIterator{contract: _Knownorigin0.contract, event: "AdminUpdateReserveAuctionLengthOnceReserveMet", logs: logs, sub: sub}, nil
}

// WatchAdminUpdateReserveAuctionLengthOnceReserveMet is a free log subscription operation binding the contract event 0xa076ed7cc197f8c245d58d44bfdc92180b22f8e3ddd247c45ee0e84f79f7b2bd.
//
// Solidity: event AdminUpdateReserveAuctionLengthOnceReserveMet(uint128 _reserveAuctionLengthOnceReserveMet)
func (_Knownorigin0 *Knownorigin0Filterer) WatchAdminUpdateReserveAuctionLengthOnceReserveMet(opts *bind.WatchOpts, sink chan<- *Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMet) (event.Subscription, error) {

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "AdminUpdateReserveAuctionLengthOnceReserveMet")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMet)
				if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdateReserveAuctionLengthOnceReserveMet", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAdminUpdateReserveAuctionLengthOnceReserveMet is a log parse operation binding the contract event 0xa076ed7cc197f8c245d58d44bfdc92180b22f8e3ddd247c45ee0e84f79f7b2bd.
//
// Solidity: event AdminUpdateReserveAuctionLengthOnceReserveMet(uint128 _reserveAuctionLengthOnceReserveMet)
func (_Knownorigin0 *Knownorigin0Filterer) ParseAdminUpdateReserveAuctionLengthOnceReserveMet(log types.Log) (*Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMet, error) {
	event := new(Knownorigin0AdminUpdateReserveAuctionLengthOnceReserveMet)
	if err := _Knownorigin0.contract.UnpackLog(event, "AdminUpdateReserveAuctionLengthOnceReserveMet", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0BidPlacedOnReserveAuctionIterator is returned from FilterBidPlacedOnReserveAuction and is used to iterate over the raw logs and unpacked data for BidPlacedOnReserveAuction events raised by the Knownorigin0 contract.
type Knownorigin0BidPlacedOnReserveAuctionIterator struct {
	Event *Knownorigin0BidPlacedOnReserveAuction // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0BidPlacedOnReserveAuctionIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0BidPlacedOnReserveAuction)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0BidPlacedOnReserveAuction)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0BidPlacedOnReserveAuctionIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0BidPlacedOnReserveAuctionIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0BidPlacedOnReserveAuction represents a BidPlacedOnReserveAuction event raised by the Knownorigin0 contract.
type Knownorigin0BidPlacedOnReserveAuction struct {
	Id                 *big.Int
	CurrentOwner       common.Address
	Bidder             common.Address
	Amount             *big.Int
	OriginalBiddingEnd *big.Int
	CurrentBiddingEnd  *big.Int
	Raw                types.Log // Blockchain specific contextual infos
}

// FilterBidPlacedOnReserveAuction is a free log retrieval operation binding the contract event 0x0dacabc07ffe733bf314aba914422a6efa538ba8f6885bbd1ee3275c3b3f389d.
//
// Solidity: event BidPlacedOnReserveAuction(uint256 indexed _id, address _currentOwner, address _bidder, uint256 _amount, uint256 _originalBiddingEnd, uint256 _currentBiddingEnd)
func (_Knownorigin0 *Knownorigin0Filterer) FilterBidPlacedOnReserveAuction(opts *bind.FilterOpts, _id []*big.Int) (*Knownorigin0BidPlacedOnReserveAuctionIterator, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "BidPlacedOnReserveAuction", _idRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0BidPlacedOnReserveAuctionIterator{contract: _Knownorigin0.contract, event: "BidPlacedOnReserveAuction", logs: logs, sub: sub}, nil
}

// WatchBidPlacedOnReserveAuction is a free log subscription operation binding the contract event 0x0dacabc07ffe733bf314aba914422a6efa538ba8f6885bbd1ee3275c3b3f389d.
//
// Solidity: event BidPlacedOnReserveAuction(uint256 indexed _id, address _currentOwner, address _bidder, uint256 _amount, uint256 _originalBiddingEnd, uint256 _currentBiddingEnd)
func (_Knownorigin0 *Knownorigin0Filterer) WatchBidPlacedOnReserveAuction(opts *bind.WatchOpts, sink chan<- *Knownorigin0BidPlacedOnReserveAuction, _id []*big.Int) (event.Subscription, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "BidPlacedOnReserveAuction", _idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0BidPlacedOnReserveAuction)
				if err := _Knownorigin0.contract.UnpackLog(event, "BidPlacedOnReserveAuction", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBidPlacedOnReserveAuction is a log parse operation binding the contract event 0x0dacabc07ffe733bf314aba914422a6efa538ba8f6885bbd1ee3275c3b3f389d.
//
// Solidity: event BidPlacedOnReserveAuction(uint256 indexed _id, address _currentOwner, address _bidder, uint256 _amount, uint256 _originalBiddingEnd, uint256 _currentBiddingEnd)
func (_Knownorigin0 *Knownorigin0Filterer) ParseBidPlacedOnReserveAuction(log types.Log) (*Knownorigin0BidPlacedOnReserveAuction, error) {
	event := new(Knownorigin0BidPlacedOnReserveAuction)
	if err := _Knownorigin0.contract.UnpackLog(event, "BidPlacedOnReserveAuction", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0BidWithdrawnFromReserveAuctionIterator is returned from FilterBidWithdrawnFromReserveAuction and is used to iterate over the raw logs and unpacked data for BidWithdrawnFromReserveAuction events raised by the Knownorigin0 contract.
type Knownorigin0BidWithdrawnFromReserveAuctionIterator struct {
	Event *Knownorigin0BidWithdrawnFromReserveAuction // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0BidWithdrawnFromReserveAuctionIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0BidWithdrawnFromReserveAuction)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0BidWithdrawnFromReserveAuction)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0BidWithdrawnFromReserveAuctionIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0BidWithdrawnFromReserveAuctionIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0BidWithdrawnFromReserveAuction represents a BidWithdrawnFromReserveAuction event raised by the Knownorigin0 contract.
type Knownorigin0BidWithdrawnFromReserveAuction struct {
	Id     *big.Int
	Bidder common.Address
	Bid    *big.Int
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterBidWithdrawnFromReserveAuction is a free log retrieval operation binding the contract event 0xd1d72c9a0832cf3726713b61a67a8f1656cc01385640d04f1155b000d1b7b751.
//
// Solidity: event BidWithdrawnFromReserveAuction(uint256 _id, address _bidder, uint128 _bid)
func (_Knownorigin0 *Knownorigin0Filterer) FilterBidWithdrawnFromReserveAuction(opts *bind.FilterOpts) (*Knownorigin0BidWithdrawnFromReserveAuctionIterator, error) {

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "BidWithdrawnFromReserveAuction")
	if err != nil {
		return nil, err
	}
	return &Knownorigin0BidWithdrawnFromReserveAuctionIterator{contract: _Knownorigin0.contract, event: "BidWithdrawnFromReserveAuction", logs: logs, sub: sub}, nil
}

// WatchBidWithdrawnFromReserveAuction is a free log subscription operation binding the contract event 0xd1d72c9a0832cf3726713b61a67a8f1656cc01385640d04f1155b000d1b7b751.
//
// Solidity: event BidWithdrawnFromReserveAuction(uint256 _id, address _bidder, uint128 _bid)
func (_Knownorigin0 *Knownorigin0Filterer) WatchBidWithdrawnFromReserveAuction(opts *bind.WatchOpts, sink chan<- *Knownorigin0BidWithdrawnFromReserveAuction) (event.Subscription, error) {

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "BidWithdrawnFromReserveAuction")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0BidWithdrawnFromReserveAuction)
				if err := _Knownorigin0.contract.UnpackLog(event, "BidWithdrawnFromReserveAuction", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBidWithdrawnFromReserveAuction is a log parse operation binding the contract event 0xd1d72c9a0832cf3726713b61a67a8f1656cc01385640d04f1155b000d1b7b751.
//
// Solidity: event BidWithdrawnFromReserveAuction(uint256 _id, address _bidder, uint128 _bid)
func (_Knownorigin0 *Knownorigin0Filterer) ParseBidWithdrawnFromReserveAuction(log types.Log) (*Knownorigin0BidWithdrawnFromReserveAuction, error) {
	event := new(Knownorigin0BidWithdrawnFromReserveAuction)
	if err := _Knownorigin0.contract.UnpackLog(event, "BidWithdrawnFromReserveAuction", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0BidderRefundedIterator is returned from FilterBidderRefunded and is used to iterate over the raw logs and unpacked data for BidderRefunded events raised by the Knownorigin0 contract.
type Knownorigin0BidderRefundedIterator struct {
	Event *Knownorigin0BidderRefunded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0BidderRefundedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0BidderRefunded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0BidderRefunded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0BidderRefundedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0BidderRefundedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0BidderRefunded represents a BidderRefunded event raised by the Knownorigin0 contract.
type Knownorigin0BidderRefunded struct {
	Id        *big.Int
	Bidder    common.Address
	Bid       *big.Int
	NewBidder common.Address
	NewOffer  *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterBidderRefunded is a free log retrieval operation binding the contract event 0x0769c15e91f18239b38add21faad4a6ba30cd8f93aed73984c02d766af4970f4.
//
// Solidity: event BidderRefunded(uint256 indexed _id, address _bidder, uint256 _bid, address _newBidder, uint256 _newOffer)
func (_Knownorigin0 *Knownorigin0Filterer) FilterBidderRefunded(opts *bind.FilterOpts, _id []*big.Int) (*Knownorigin0BidderRefundedIterator, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "BidderRefunded", _idRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0BidderRefundedIterator{contract: _Knownorigin0.contract, event: "BidderRefunded", logs: logs, sub: sub}, nil
}

// WatchBidderRefunded is a free log subscription operation binding the contract event 0x0769c15e91f18239b38add21faad4a6ba30cd8f93aed73984c02d766af4970f4.
//
// Solidity: event BidderRefunded(uint256 indexed _id, address _bidder, uint256 _bid, address _newBidder, uint256 _newOffer)
func (_Knownorigin0 *Knownorigin0Filterer) WatchBidderRefunded(opts *bind.WatchOpts, sink chan<- *Knownorigin0BidderRefunded, _id []*big.Int) (event.Subscription, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "BidderRefunded", _idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0BidderRefunded)
				if err := _Knownorigin0.contract.UnpackLog(event, "BidderRefunded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBidderRefunded is a log parse operation binding the contract event 0x0769c15e91f18239b38add21faad4a6ba30cd8f93aed73984c02d766af4970f4.
//
// Solidity: event BidderRefunded(uint256 indexed _id, address _bidder, uint256 _bid, address _newBidder, uint256 _newOffer)
func (_Knownorigin0 *Knownorigin0Filterer) ParseBidderRefunded(log types.Log) (*Knownorigin0BidderRefunded, error) {
	event := new(Knownorigin0BidderRefunded)
	if err := _Knownorigin0.contract.UnpackLog(event, "BidderRefunded", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0BidderRefundedFailedIterator is returned from FilterBidderRefundedFailed and is used to iterate over the raw logs and unpacked data for BidderRefundedFailed events raised by the Knownorigin0 contract.
type Knownorigin0BidderRefundedFailedIterator struct {
	Event *Knownorigin0BidderRefundedFailed // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0BidderRefundedFailedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0BidderRefundedFailed)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0BidderRefundedFailed)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0BidderRefundedFailedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0BidderRefundedFailedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0BidderRefundedFailed represents a BidderRefundedFailed event raised by the Knownorigin0 contract.
type Knownorigin0BidderRefundedFailed struct {
	Id        *big.Int
	Bidder    common.Address
	Bid       *big.Int
	NewBidder common.Address
	NewOffer  *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterBidderRefundedFailed is a free log retrieval operation binding the contract event 0x437414bccea896dfde9665f76dd6cbde1ec21a65c6e8bcd2afb4e9b0fb0ea196.
//
// Solidity: event BidderRefundedFailed(uint256 indexed _id, address _bidder, uint256 _bid, address _newBidder, uint256 _newOffer)
func (_Knownorigin0 *Knownorigin0Filterer) FilterBidderRefundedFailed(opts *bind.FilterOpts, _id []*big.Int) (*Knownorigin0BidderRefundedFailedIterator, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "BidderRefundedFailed", _idRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0BidderRefundedFailedIterator{contract: _Knownorigin0.contract, event: "BidderRefundedFailed", logs: logs, sub: sub}, nil
}

// WatchBidderRefundedFailed is a free log subscription operation binding the contract event 0x437414bccea896dfde9665f76dd6cbde1ec21a65c6e8bcd2afb4e9b0fb0ea196.
//
// Solidity: event BidderRefundedFailed(uint256 indexed _id, address _bidder, uint256 _bid, address _newBidder, uint256 _newOffer)
func (_Knownorigin0 *Knownorigin0Filterer) WatchBidderRefundedFailed(opts *bind.WatchOpts, sink chan<- *Knownorigin0BidderRefundedFailed, _id []*big.Int) (event.Subscription, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "BidderRefundedFailed", _idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0BidderRefundedFailed)
				if err := _Knownorigin0.contract.UnpackLog(event, "BidderRefundedFailed", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBidderRefundedFailed is a log parse operation binding the contract event 0x437414bccea896dfde9665f76dd6cbde1ec21a65c6e8bcd2afb4e9b0fb0ea196.
//
// Solidity: event BidderRefundedFailed(uint256 indexed _id, address _bidder, uint256 _bid, address _newBidder, uint256 _newOffer)
func (_Knownorigin0 *Knownorigin0Filterer) ParseBidderRefundedFailed(log types.Log) (*Knownorigin0BidderRefundedFailed, error) {
	event := new(Knownorigin0BidderRefundedFailed)
	if err := _Knownorigin0.contract.UnpackLog(event, "BidderRefundedFailed", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0BuyNowDeListedIterator is returned from FilterBuyNowDeListed and is used to iterate over the raw logs and unpacked data for BuyNowDeListed events raised by the Knownorigin0 contract.
type Knownorigin0BuyNowDeListedIterator struct {
	Event *Knownorigin0BuyNowDeListed // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0BuyNowDeListedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0BuyNowDeListed)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0BuyNowDeListed)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0BuyNowDeListedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0BuyNowDeListedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0BuyNowDeListed represents a BuyNowDeListed event raised by the Knownorigin0 contract.
type Knownorigin0BuyNowDeListed struct {
	Id  *big.Int
	Raw types.Log // Blockchain specific contextual infos
}

// FilterBuyNowDeListed is a free log retrieval operation binding the contract event 0x782c030e02c4ea624a3fb427d8dd4f1023dd5f0d944aa3d14a715628e1e41c7b.
//
// Solidity: event BuyNowDeListed(uint256 indexed _id)
func (_Knownorigin0 *Knownorigin0Filterer) FilterBuyNowDeListed(opts *bind.FilterOpts, _id []*big.Int) (*Knownorigin0BuyNowDeListedIterator, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "BuyNowDeListed", _idRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0BuyNowDeListedIterator{contract: _Knownorigin0.contract, event: "BuyNowDeListed", logs: logs, sub: sub}, nil
}

// WatchBuyNowDeListed is a free log subscription operation binding the contract event 0x782c030e02c4ea624a3fb427d8dd4f1023dd5f0d944aa3d14a715628e1e41c7b.
//
// Solidity: event BuyNowDeListed(uint256 indexed _id)
func (_Knownorigin0 *Knownorigin0Filterer) WatchBuyNowDeListed(opts *bind.WatchOpts, sink chan<- *Knownorigin0BuyNowDeListed, _id []*big.Int) (event.Subscription, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "BuyNowDeListed", _idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0BuyNowDeListed)
				if err := _Knownorigin0.contract.UnpackLog(event, "BuyNowDeListed", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBuyNowDeListed is a log parse operation binding the contract event 0x782c030e02c4ea624a3fb427d8dd4f1023dd5f0d944aa3d14a715628e1e41c7b.
//
// Solidity: event BuyNowDeListed(uint256 indexed _id)
func (_Knownorigin0 *Knownorigin0Filterer) ParseBuyNowDeListed(log types.Log) (*Knownorigin0BuyNowDeListed, error) {
	event := new(Knownorigin0BuyNowDeListed)
	if err := _Knownorigin0.contract.UnpackLog(event, "BuyNowDeListed", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0BuyNowPriceChangedIterator is returned from FilterBuyNowPriceChanged and is used to iterate over the raw logs and unpacked data for BuyNowPriceChanged events raised by the Knownorigin0 contract.
type Knownorigin0BuyNowPriceChangedIterator struct {
	Event *Knownorigin0BuyNowPriceChanged // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0BuyNowPriceChangedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0BuyNowPriceChanged)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0BuyNowPriceChanged)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0BuyNowPriceChangedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0BuyNowPriceChangedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0BuyNowPriceChanged represents a BuyNowPriceChanged event raised by the Knownorigin0 contract.
type Knownorigin0BuyNowPriceChanged struct {
	Id    *big.Int
	Price *big.Int
	Raw   types.Log // Blockchain specific contextual infos
}

// FilterBuyNowPriceChanged is a free log retrieval operation binding the contract event 0x2acf0b64eca25e79ecb9831cce1c9045dce27614bb88deb1cc5b435e87278cdb.
//
// Solidity: event BuyNowPriceChanged(uint256 indexed _id, uint256 _price)
func (_Knownorigin0 *Knownorigin0Filterer) FilterBuyNowPriceChanged(opts *bind.FilterOpts, _id []*big.Int) (*Knownorigin0BuyNowPriceChangedIterator, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "BuyNowPriceChanged", _idRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0BuyNowPriceChangedIterator{contract: _Knownorigin0.contract, event: "BuyNowPriceChanged", logs: logs, sub: sub}, nil
}

// WatchBuyNowPriceChanged is a free log subscription operation binding the contract event 0x2acf0b64eca25e79ecb9831cce1c9045dce27614bb88deb1cc5b435e87278cdb.
//
// Solidity: event BuyNowPriceChanged(uint256 indexed _id, uint256 _price)
func (_Knownorigin0 *Knownorigin0Filterer) WatchBuyNowPriceChanged(opts *bind.WatchOpts, sink chan<- *Knownorigin0BuyNowPriceChanged, _id []*big.Int) (event.Subscription, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "BuyNowPriceChanged", _idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0BuyNowPriceChanged)
				if err := _Knownorigin0.contract.UnpackLog(event, "BuyNowPriceChanged", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBuyNowPriceChanged is a log parse operation binding the contract event 0x2acf0b64eca25e79ecb9831cce1c9045dce27614bb88deb1cc5b435e87278cdb.
//
// Solidity: event BuyNowPriceChanged(uint256 indexed _id, uint256 _price)
func (_Knownorigin0 *Knownorigin0Filterer) ParseBuyNowPriceChanged(log types.Log) (*Knownorigin0BuyNowPriceChanged, error) {
	event := new(Knownorigin0BuyNowPriceChanged)
	if err := _Knownorigin0.contract.UnpackLog(event, "BuyNowPriceChanged", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0BuyNowPurchasedIterator is returned from FilterBuyNowPurchased and is used to iterate over the raw logs and unpacked data for BuyNowPurchased events raised by the Knownorigin0 contract.
type Knownorigin0BuyNowPurchasedIterator struct {
	Event *Knownorigin0BuyNowPurchased // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0BuyNowPurchasedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0BuyNowPurchased)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0BuyNowPurchased)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0BuyNowPurchasedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0BuyNowPurchasedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0BuyNowPurchased represents a BuyNowPurchased event raised by the Knownorigin0 contract.
type Knownorigin0BuyNowPurchased struct {
	TokenId      *big.Int
	Buyer        common.Address
	CurrentOwner common.Address
	Price        *big.Int
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterBuyNowPurchased is a free log retrieval operation binding the contract event 0x25a395eca5bca1a1ca0c1543a3e87384fabcf63100e03535276da15f838805b2.
//
// Solidity: event BuyNowPurchased(uint256 indexed _tokenId, address _buyer, address _currentOwner, uint256 _price)
func (_Knownorigin0 *Knownorigin0Filterer) FilterBuyNowPurchased(opts *bind.FilterOpts, _tokenId []*big.Int) (*Knownorigin0BuyNowPurchasedIterator, error) {

	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "BuyNowPurchased", _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0BuyNowPurchasedIterator{contract: _Knownorigin0.contract, event: "BuyNowPurchased", logs: logs, sub: sub}, nil
}

// WatchBuyNowPurchased is a free log subscription operation binding the contract event 0x25a395eca5bca1a1ca0c1543a3e87384fabcf63100e03535276da15f838805b2.
//
// Solidity: event BuyNowPurchased(uint256 indexed _tokenId, address _buyer, address _currentOwner, uint256 _price)
func (_Knownorigin0 *Knownorigin0Filterer) WatchBuyNowPurchased(opts *bind.WatchOpts, sink chan<- *Knownorigin0BuyNowPurchased, _tokenId []*big.Int) (event.Subscription, error) {

	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "BuyNowPurchased", _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0BuyNowPurchased)
				if err := _Knownorigin0.contract.UnpackLog(event, "BuyNowPurchased", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBuyNowPurchased is a log parse operation binding the contract event 0x25a395eca5bca1a1ca0c1543a3e87384fabcf63100e03535276da15f838805b2.
//
// Solidity: event BuyNowPurchased(uint256 indexed _tokenId, address _buyer, address _currentOwner, uint256 _price)
func (_Knownorigin0 *Knownorigin0Filterer) ParseBuyNowPurchased(log types.Log) (*Knownorigin0BuyNowPurchased, error) {
	event := new(Knownorigin0BuyNowPurchased)
	if err := _Knownorigin0.contract.UnpackLog(event, "BuyNowPurchased", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0ConvertFromBuyNowToOffersIterator is returned from FilterConvertFromBuyNowToOffers and is used to iterate over the raw logs and unpacked data for ConvertFromBuyNowToOffers events raised by the Knownorigin0 contract.
type Knownorigin0ConvertFromBuyNowToOffersIterator struct {
	Event *Knownorigin0ConvertFromBuyNowToOffers // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0ConvertFromBuyNowToOffersIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0ConvertFromBuyNowToOffers)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0ConvertFromBuyNowToOffers)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0ConvertFromBuyNowToOffersIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0ConvertFromBuyNowToOffersIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0ConvertFromBuyNowToOffers represents a ConvertFromBuyNowToOffers event raised by the Knownorigin0 contract.
type Knownorigin0ConvertFromBuyNowToOffers struct {
	EditionId *big.Int
	StartDate *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterConvertFromBuyNowToOffers is a free log retrieval operation binding the contract event 0xc5cede647bcf791d83ff0ee63bb8ae1434f9795878081f8ad4b36ee1cb582aad.
//
// Solidity: event ConvertFromBuyNowToOffers(uint256 indexed _editionId, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) FilterConvertFromBuyNowToOffers(opts *bind.FilterOpts, _editionId []*big.Int) (*Knownorigin0ConvertFromBuyNowToOffersIterator, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "ConvertFromBuyNowToOffers", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0ConvertFromBuyNowToOffersIterator{contract: _Knownorigin0.contract, event: "ConvertFromBuyNowToOffers", logs: logs, sub: sub}, nil
}

// WatchConvertFromBuyNowToOffers is a free log subscription operation binding the contract event 0xc5cede647bcf791d83ff0ee63bb8ae1434f9795878081f8ad4b36ee1cb582aad.
//
// Solidity: event ConvertFromBuyNowToOffers(uint256 indexed _editionId, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) WatchConvertFromBuyNowToOffers(opts *bind.WatchOpts, sink chan<- *Knownorigin0ConvertFromBuyNowToOffers, _editionId []*big.Int) (event.Subscription, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "ConvertFromBuyNowToOffers", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0ConvertFromBuyNowToOffers)
				if err := _Knownorigin0.contract.UnpackLog(event, "ConvertFromBuyNowToOffers", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseConvertFromBuyNowToOffers is a log parse operation binding the contract event 0xc5cede647bcf791d83ff0ee63bb8ae1434f9795878081f8ad4b36ee1cb582aad.
//
// Solidity: event ConvertFromBuyNowToOffers(uint256 indexed _editionId, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) ParseConvertFromBuyNowToOffers(log types.Log) (*Knownorigin0ConvertFromBuyNowToOffers, error) {
	event := new(Knownorigin0ConvertFromBuyNowToOffers)
	if err := _Knownorigin0.contract.UnpackLog(event, "ConvertFromBuyNowToOffers", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0ConvertSteppedAuctionToBuyNowIterator is returned from FilterConvertSteppedAuctionToBuyNow and is used to iterate over the raw logs and unpacked data for ConvertSteppedAuctionToBuyNow events raised by the Knownorigin0 contract.
type Knownorigin0ConvertSteppedAuctionToBuyNowIterator struct {
	Event *Knownorigin0ConvertSteppedAuctionToBuyNow // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0ConvertSteppedAuctionToBuyNowIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0ConvertSteppedAuctionToBuyNow)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0ConvertSteppedAuctionToBuyNow)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0ConvertSteppedAuctionToBuyNowIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0ConvertSteppedAuctionToBuyNowIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0ConvertSteppedAuctionToBuyNow represents a ConvertSteppedAuctionToBuyNow event raised by the Knownorigin0 contract.
type Knownorigin0ConvertSteppedAuctionToBuyNow struct {
	EditionId    *big.Int
	ListingPrice *big.Int
	StartDate    *big.Int
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterConvertSteppedAuctionToBuyNow is a free log retrieval operation binding the contract event 0x3ed14e51a13ebed8bcb36a692319d51c90603eef8a9b49835ed0f77909e6bdc6.
//
// Solidity: event ConvertSteppedAuctionToBuyNow(uint256 indexed _editionId, uint128 _listingPrice, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) FilterConvertSteppedAuctionToBuyNow(opts *bind.FilterOpts, _editionId []*big.Int) (*Knownorigin0ConvertSteppedAuctionToBuyNowIterator, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "ConvertSteppedAuctionToBuyNow", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0ConvertSteppedAuctionToBuyNowIterator{contract: _Knownorigin0.contract, event: "ConvertSteppedAuctionToBuyNow", logs: logs, sub: sub}, nil
}

// WatchConvertSteppedAuctionToBuyNow is a free log subscription operation binding the contract event 0x3ed14e51a13ebed8bcb36a692319d51c90603eef8a9b49835ed0f77909e6bdc6.
//
// Solidity: event ConvertSteppedAuctionToBuyNow(uint256 indexed _editionId, uint128 _listingPrice, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) WatchConvertSteppedAuctionToBuyNow(opts *bind.WatchOpts, sink chan<- *Knownorigin0ConvertSteppedAuctionToBuyNow, _editionId []*big.Int) (event.Subscription, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "ConvertSteppedAuctionToBuyNow", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0ConvertSteppedAuctionToBuyNow)
				if err := _Knownorigin0.contract.UnpackLog(event, "ConvertSteppedAuctionToBuyNow", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseConvertSteppedAuctionToBuyNow is a log parse operation binding the contract event 0x3ed14e51a13ebed8bcb36a692319d51c90603eef8a9b49835ed0f77909e6bdc6.
//
// Solidity: event ConvertSteppedAuctionToBuyNow(uint256 indexed _editionId, uint128 _listingPrice, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) ParseConvertSteppedAuctionToBuyNow(log types.Log) (*Knownorigin0ConvertSteppedAuctionToBuyNow, error) {
	event := new(Knownorigin0ConvertSteppedAuctionToBuyNow)
	if err := _Knownorigin0.contract.UnpackLog(event, "ConvertSteppedAuctionToBuyNow", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0EditionAcceptingOfferIterator is returned from FilterEditionAcceptingOffer and is used to iterate over the raw logs and unpacked data for EditionAcceptingOffer events raised by the Knownorigin0 contract.
type Knownorigin0EditionAcceptingOfferIterator struct {
	Event *Knownorigin0EditionAcceptingOffer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0EditionAcceptingOfferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0EditionAcceptingOffer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0EditionAcceptingOffer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0EditionAcceptingOfferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0EditionAcceptingOfferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0EditionAcceptingOffer represents a EditionAcceptingOffer event raised by the Knownorigin0 contract.
type Knownorigin0EditionAcceptingOffer struct {
	EditionId *big.Int
	StartDate *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterEditionAcceptingOffer is a free log retrieval operation binding the contract event 0x252bfd97a791429cb820c48af9674adf004431519d80a5d86fafdfb06863492c.
//
// Solidity: event EditionAcceptingOffer(uint256 indexed _editionId, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) FilterEditionAcceptingOffer(opts *bind.FilterOpts, _editionId []*big.Int) (*Knownorigin0EditionAcceptingOfferIterator, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "EditionAcceptingOffer", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0EditionAcceptingOfferIterator{contract: _Knownorigin0.contract, event: "EditionAcceptingOffer", logs: logs, sub: sub}, nil
}

// WatchEditionAcceptingOffer is a free log subscription operation binding the contract event 0x252bfd97a791429cb820c48af9674adf004431519d80a5d86fafdfb06863492c.
//
// Solidity: event EditionAcceptingOffer(uint256 indexed _editionId, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) WatchEditionAcceptingOffer(opts *bind.WatchOpts, sink chan<- *Knownorigin0EditionAcceptingOffer, _editionId []*big.Int) (event.Subscription, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "EditionAcceptingOffer", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0EditionAcceptingOffer)
				if err := _Knownorigin0.contract.UnpackLog(event, "EditionAcceptingOffer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEditionAcceptingOffer is a log parse operation binding the contract event 0x252bfd97a791429cb820c48af9674adf004431519d80a5d86fafdfb06863492c.
//
// Solidity: event EditionAcceptingOffer(uint256 indexed _editionId, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) ParseEditionAcceptingOffer(log types.Log) (*Knownorigin0EditionAcceptingOffer, error) {
	event := new(Knownorigin0EditionAcceptingOffer)
	if err := _Knownorigin0.contract.UnpackLog(event, "EditionAcceptingOffer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0EditionBidAcceptedIterator is returned from FilterEditionBidAccepted and is used to iterate over the raw logs and unpacked data for EditionBidAccepted events raised by the Knownorigin0 contract.
type Knownorigin0EditionBidAcceptedIterator struct {
	Event *Knownorigin0EditionBidAccepted // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0EditionBidAcceptedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0EditionBidAccepted)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0EditionBidAccepted)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0EditionBidAcceptedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0EditionBidAcceptedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0EditionBidAccepted represents a EditionBidAccepted event raised by the Knownorigin0 contract.
type Knownorigin0EditionBidAccepted struct {
	EditionId *big.Int
	TokenId   *big.Int
	Bidder    common.Address
	Amount    *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterEditionBidAccepted is a free log retrieval operation binding the contract event 0xbb5456559b0c90450f12de9d601e1470b0edb240c3425838360bd25412e6b8be.
//
// Solidity: event EditionBidAccepted(uint256 indexed _editionId, uint256 indexed _tokenId, address _bidder, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) FilterEditionBidAccepted(opts *bind.FilterOpts, _editionId []*big.Int, _tokenId []*big.Int) (*Knownorigin0EditionBidAcceptedIterator, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}
	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "EditionBidAccepted", _editionIdRule, _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0EditionBidAcceptedIterator{contract: _Knownorigin0.contract, event: "EditionBidAccepted", logs: logs, sub: sub}, nil
}

// WatchEditionBidAccepted is a free log subscription operation binding the contract event 0xbb5456559b0c90450f12de9d601e1470b0edb240c3425838360bd25412e6b8be.
//
// Solidity: event EditionBidAccepted(uint256 indexed _editionId, uint256 indexed _tokenId, address _bidder, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) WatchEditionBidAccepted(opts *bind.WatchOpts, sink chan<- *Knownorigin0EditionBidAccepted, _editionId []*big.Int, _tokenId []*big.Int) (event.Subscription, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}
	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "EditionBidAccepted", _editionIdRule, _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0EditionBidAccepted)
				if err := _Knownorigin0.contract.UnpackLog(event, "EditionBidAccepted", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEditionBidAccepted is a log parse operation binding the contract event 0xbb5456559b0c90450f12de9d601e1470b0edb240c3425838360bd25412e6b8be.
//
// Solidity: event EditionBidAccepted(uint256 indexed _editionId, uint256 indexed _tokenId, address _bidder, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) ParseEditionBidAccepted(log types.Log) (*Knownorigin0EditionBidAccepted, error) {
	event := new(Knownorigin0EditionBidAccepted)
	if err := _Knownorigin0.contract.UnpackLog(event, "EditionBidAccepted", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0EditionBidPlacedIterator is returned from FilterEditionBidPlaced and is used to iterate over the raw logs and unpacked data for EditionBidPlaced events raised by the Knownorigin0 contract.
type Knownorigin0EditionBidPlacedIterator struct {
	Event *Knownorigin0EditionBidPlaced // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0EditionBidPlacedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0EditionBidPlaced)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0EditionBidPlaced)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0EditionBidPlacedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0EditionBidPlacedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0EditionBidPlaced represents a EditionBidPlaced event raised by the Knownorigin0 contract.
type Knownorigin0EditionBidPlaced struct {
	EditionId *big.Int
	Bidder    common.Address
	Amount    *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterEditionBidPlaced is a free log retrieval operation binding the contract event 0x823a6e5c949aae9db807fd853796d0cf551fda5395f20faa534a3a2ef7149b9b.
//
// Solidity: event EditionBidPlaced(uint256 indexed _editionId, address _bidder, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) FilterEditionBidPlaced(opts *bind.FilterOpts, _editionId []*big.Int) (*Knownorigin0EditionBidPlacedIterator, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "EditionBidPlaced", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0EditionBidPlacedIterator{contract: _Knownorigin0.contract, event: "EditionBidPlaced", logs: logs, sub: sub}, nil
}

// WatchEditionBidPlaced is a free log subscription operation binding the contract event 0x823a6e5c949aae9db807fd853796d0cf551fda5395f20faa534a3a2ef7149b9b.
//
// Solidity: event EditionBidPlaced(uint256 indexed _editionId, address _bidder, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) WatchEditionBidPlaced(opts *bind.WatchOpts, sink chan<- *Knownorigin0EditionBidPlaced, _editionId []*big.Int) (event.Subscription, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "EditionBidPlaced", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0EditionBidPlaced)
				if err := _Knownorigin0.contract.UnpackLog(event, "EditionBidPlaced", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEditionBidPlaced is a log parse operation binding the contract event 0x823a6e5c949aae9db807fd853796d0cf551fda5395f20faa534a3a2ef7149b9b.
//
// Solidity: event EditionBidPlaced(uint256 indexed _editionId, address _bidder, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) ParseEditionBidPlaced(log types.Log) (*Knownorigin0EditionBidPlaced, error) {
	event := new(Knownorigin0EditionBidPlaced)
	if err := _Knownorigin0.contract.UnpackLog(event, "EditionBidPlaced", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0EditionBidRejectedIterator is returned from FilterEditionBidRejected and is used to iterate over the raw logs and unpacked data for EditionBidRejected events raised by the Knownorigin0 contract.
type Knownorigin0EditionBidRejectedIterator struct {
	Event *Knownorigin0EditionBidRejected // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0EditionBidRejectedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0EditionBidRejected)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0EditionBidRejected)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0EditionBidRejectedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0EditionBidRejectedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0EditionBidRejected represents a EditionBidRejected event raised by the Knownorigin0 contract.
type Knownorigin0EditionBidRejected struct {
	EditionId *big.Int
	Bidder    common.Address
	Amount    *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterEditionBidRejected is a free log retrieval operation binding the contract event 0xdd21daffb84ed4431b50ed9dedcd5e0cf1f96a4cc5976fefb4be54d43be37281.
//
// Solidity: event EditionBidRejected(uint256 indexed _editionId, address _bidder, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) FilterEditionBidRejected(opts *bind.FilterOpts, _editionId []*big.Int) (*Knownorigin0EditionBidRejectedIterator, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "EditionBidRejected", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0EditionBidRejectedIterator{contract: _Knownorigin0.contract, event: "EditionBidRejected", logs: logs, sub: sub}, nil
}

// WatchEditionBidRejected is a free log subscription operation binding the contract event 0xdd21daffb84ed4431b50ed9dedcd5e0cf1f96a4cc5976fefb4be54d43be37281.
//
// Solidity: event EditionBidRejected(uint256 indexed _editionId, address _bidder, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) WatchEditionBidRejected(opts *bind.WatchOpts, sink chan<- *Knownorigin0EditionBidRejected, _editionId []*big.Int) (event.Subscription, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "EditionBidRejected", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0EditionBidRejected)
				if err := _Knownorigin0.contract.UnpackLog(event, "EditionBidRejected", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEditionBidRejected is a log parse operation binding the contract event 0xdd21daffb84ed4431b50ed9dedcd5e0cf1f96a4cc5976fefb4be54d43be37281.
//
// Solidity: event EditionBidRejected(uint256 indexed _editionId, address _bidder, uint256 _amount)
func (_Knownorigin0 *Knownorigin0Filterer) ParseEditionBidRejected(log types.Log) (*Knownorigin0EditionBidRejected, error) {
	event := new(Knownorigin0EditionBidRejected)
	if err := _Knownorigin0.contract.UnpackLog(event, "EditionBidRejected", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0EditionBidWithdrawnIterator is returned from FilterEditionBidWithdrawn and is used to iterate over the raw logs and unpacked data for EditionBidWithdrawn events raised by the Knownorigin0 contract.
type Knownorigin0EditionBidWithdrawnIterator struct {
	Event *Knownorigin0EditionBidWithdrawn // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0EditionBidWithdrawnIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0EditionBidWithdrawn)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0EditionBidWithdrawn)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0EditionBidWithdrawnIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0EditionBidWithdrawnIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0EditionBidWithdrawn represents a EditionBidWithdrawn event raised by the Knownorigin0 contract.
type Knownorigin0EditionBidWithdrawn struct {
	EditionId *big.Int
	Bidder    common.Address
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterEditionBidWithdrawn is a free log retrieval operation binding the contract event 0x2ed9db6d233a11ddbebd6f8b9e9ee561a991b8ab9a839ad4eb724d8397b3cff6.
//
// Solidity: event EditionBidWithdrawn(uint256 indexed _editionId, address _bidder)
func (_Knownorigin0 *Knownorigin0Filterer) FilterEditionBidWithdrawn(opts *bind.FilterOpts, _editionId []*big.Int) (*Knownorigin0EditionBidWithdrawnIterator, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "EditionBidWithdrawn", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0EditionBidWithdrawnIterator{contract: _Knownorigin0.contract, event: "EditionBidWithdrawn", logs: logs, sub: sub}, nil
}

// WatchEditionBidWithdrawn is a free log subscription operation binding the contract event 0x2ed9db6d233a11ddbebd6f8b9e9ee561a991b8ab9a839ad4eb724d8397b3cff6.
//
// Solidity: event EditionBidWithdrawn(uint256 indexed _editionId, address _bidder)
func (_Knownorigin0 *Knownorigin0Filterer) WatchEditionBidWithdrawn(opts *bind.WatchOpts, sink chan<- *Knownorigin0EditionBidWithdrawn, _editionId []*big.Int) (event.Subscription, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "EditionBidWithdrawn", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0EditionBidWithdrawn)
				if err := _Knownorigin0.contract.UnpackLog(event, "EditionBidWithdrawn", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEditionBidWithdrawn is a log parse operation binding the contract event 0x2ed9db6d233a11ddbebd6f8b9e9ee561a991b8ab9a839ad4eb724d8397b3cff6.
//
// Solidity: event EditionBidWithdrawn(uint256 indexed _editionId, address _bidder)
func (_Knownorigin0 *Knownorigin0Filterer) ParseEditionBidWithdrawn(log types.Log) (*Knownorigin0EditionBidWithdrawn, error) {
	event := new(Knownorigin0EditionBidWithdrawn)
	if err := _Knownorigin0.contract.UnpackLog(event, "EditionBidWithdrawn", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0EditionConvertedFromOffersToBuyItNowIterator is returned from FilterEditionConvertedFromOffersToBuyItNow and is used to iterate over the raw logs and unpacked data for EditionConvertedFromOffersToBuyItNow events raised by the Knownorigin0 contract.
type Knownorigin0EditionConvertedFromOffersToBuyItNowIterator struct {
	Event *Knownorigin0EditionConvertedFromOffersToBuyItNow // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0EditionConvertedFromOffersToBuyItNowIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0EditionConvertedFromOffersToBuyItNow)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0EditionConvertedFromOffersToBuyItNow)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0EditionConvertedFromOffersToBuyItNowIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0EditionConvertedFromOffersToBuyItNowIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0EditionConvertedFromOffersToBuyItNow represents a EditionConvertedFromOffersToBuyItNow event raised by the Knownorigin0 contract.
type Knownorigin0EditionConvertedFromOffersToBuyItNow struct {
	EditionId *big.Int
	Price     *big.Int
	StartDate *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterEditionConvertedFromOffersToBuyItNow is a free log retrieval operation binding the contract event 0x4ee58f1b139dbfa3b12ee1fc57ad8d513b35c670477df6dfde35944346b1d194.
//
// Solidity: event EditionConvertedFromOffersToBuyItNow(uint256 _editionId, uint128 _price, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) FilterEditionConvertedFromOffersToBuyItNow(opts *bind.FilterOpts) (*Knownorigin0EditionConvertedFromOffersToBuyItNowIterator, error) {

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "EditionConvertedFromOffersToBuyItNow")
	if err != nil {
		return nil, err
	}
	return &Knownorigin0EditionConvertedFromOffersToBuyItNowIterator{contract: _Knownorigin0.contract, event: "EditionConvertedFromOffersToBuyItNow", logs: logs, sub: sub}, nil
}

// WatchEditionConvertedFromOffersToBuyItNow is a free log subscription operation binding the contract event 0x4ee58f1b139dbfa3b12ee1fc57ad8d513b35c670477df6dfde35944346b1d194.
//
// Solidity: event EditionConvertedFromOffersToBuyItNow(uint256 _editionId, uint128 _price, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) WatchEditionConvertedFromOffersToBuyItNow(opts *bind.WatchOpts, sink chan<- *Knownorigin0EditionConvertedFromOffersToBuyItNow) (event.Subscription, error) {

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "EditionConvertedFromOffersToBuyItNow")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0EditionConvertedFromOffersToBuyItNow)
				if err := _Knownorigin0.contract.UnpackLog(event, "EditionConvertedFromOffersToBuyItNow", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEditionConvertedFromOffersToBuyItNow is a log parse operation binding the contract event 0x4ee58f1b139dbfa3b12ee1fc57ad8d513b35c670477df6dfde35944346b1d194.
//
// Solidity: event EditionConvertedFromOffersToBuyItNow(uint256 _editionId, uint128 _price, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) ParseEditionConvertedFromOffersToBuyItNow(log types.Log) (*Knownorigin0EditionConvertedFromOffersToBuyItNow, error) {
	event := new(Knownorigin0EditionConvertedFromOffersToBuyItNow)
	if err := _Knownorigin0.contract.UnpackLog(event, "EditionConvertedFromOffersToBuyItNow", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0EditionSteppedAuctionUpdatedIterator is returned from FilterEditionSteppedAuctionUpdated and is used to iterate over the raw logs and unpacked data for EditionSteppedAuctionUpdated events raised by the Knownorigin0 contract.
type Knownorigin0EditionSteppedAuctionUpdatedIterator struct {
	Event *Knownorigin0EditionSteppedAuctionUpdated // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0EditionSteppedAuctionUpdatedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0EditionSteppedAuctionUpdated)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0EditionSteppedAuctionUpdated)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0EditionSteppedAuctionUpdatedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0EditionSteppedAuctionUpdatedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0EditionSteppedAuctionUpdated represents a EditionSteppedAuctionUpdated event raised by the Knownorigin0 contract.
type Knownorigin0EditionSteppedAuctionUpdated struct {
	EditionId *big.Int
	BasePrice *big.Int
	StepPrice *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterEditionSteppedAuctionUpdated is a free log retrieval operation binding the contract event 0xca6f94d6cf7150ed49f5d1ccd677efd457310ccf201845261d9098a6877dcf3c.
//
// Solidity: event EditionSteppedAuctionUpdated(uint256 indexed _editionId, uint128 _basePrice, uint128 _stepPrice)
func (_Knownorigin0 *Knownorigin0Filterer) FilterEditionSteppedAuctionUpdated(opts *bind.FilterOpts, _editionId []*big.Int) (*Knownorigin0EditionSteppedAuctionUpdatedIterator, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "EditionSteppedAuctionUpdated", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0EditionSteppedAuctionUpdatedIterator{contract: _Knownorigin0.contract, event: "EditionSteppedAuctionUpdated", logs: logs, sub: sub}, nil
}

// WatchEditionSteppedAuctionUpdated is a free log subscription operation binding the contract event 0xca6f94d6cf7150ed49f5d1ccd677efd457310ccf201845261d9098a6877dcf3c.
//
// Solidity: event EditionSteppedAuctionUpdated(uint256 indexed _editionId, uint128 _basePrice, uint128 _stepPrice)
func (_Knownorigin0 *Knownorigin0Filterer) WatchEditionSteppedAuctionUpdated(opts *bind.WatchOpts, sink chan<- *Knownorigin0EditionSteppedAuctionUpdated, _editionId []*big.Int) (event.Subscription, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "EditionSteppedAuctionUpdated", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0EditionSteppedAuctionUpdated)
				if err := _Knownorigin0.contract.UnpackLog(event, "EditionSteppedAuctionUpdated", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEditionSteppedAuctionUpdated is a log parse operation binding the contract event 0xca6f94d6cf7150ed49f5d1ccd677efd457310ccf201845261d9098a6877dcf3c.
//
// Solidity: event EditionSteppedAuctionUpdated(uint256 indexed _editionId, uint128 _basePrice, uint128 _stepPrice)
func (_Knownorigin0 *Knownorigin0Filterer) ParseEditionSteppedAuctionUpdated(log types.Log) (*Knownorigin0EditionSteppedAuctionUpdated, error) {
	event := new(Knownorigin0EditionSteppedAuctionUpdated)
	if err := _Knownorigin0.contract.UnpackLog(event, "EditionSteppedAuctionUpdated", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0EditionSteppedSaleBuyIterator is returned from FilterEditionSteppedSaleBuy and is used to iterate over the raw logs and unpacked data for EditionSteppedSaleBuy events raised by the Knownorigin0 contract.
type Knownorigin0EditionSteppedSaleBuyIterator struct {
	Event *Knownorigin0EditionSteppedSaleBuy // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0EditionSteppedSaleBuyIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0EditionSteppedSaleBuy)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0EditionSteppedSaleBuy)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0EditionSteppedSaleBuyIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0EditionSteppedSaleBuyIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0EditionSteppedSaleBuy represents a EditionSteppedSaleBuy event raised by the Knownorigin0 contract.
type Knownorigin0EditionSteppedSaleBuy struct {
	EditionId   *big.Int
	TokenId     *big.Int
	Buyer       common.Address
	Price       *big.Int
	CurrentStep uint16
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterEditionSteppedSaleBuy is a free log retrieval operation binding the contract event 0x41820cf4351ec738da5f8ada46c5a38d78c6aeaa3039cb1dddcf1ea6853dd343.
//
// Solidity: event EditionSteppedSaleBuy(uint256 indexed _editionId, uint256 indexed _tokenId, address _buyer, uint256 _price, uint16 _currentStep)
func (_Knownorigin0 *Knownorigin0Filterer) FilterEditionSteppedSaleBuy(opts *bind.FilterOpts, _editionId []*big.Int, _tokenId []*big.Int) (*Knownorigin0EditionSteppedSaleBuyIterator, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}
	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "EditionSteppedSaleBuy", _editionIdRule, _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0EditionSteppedSaleBuyIterator{contract: _Knownorigin0.contract, event: "EditionSteppedSaleBuy", logs: logs, sub: sub}, nil
}

// WatchEditionSteppedSaleBuy is a free log subscription operation binding the contract event 0x41820cf4351ec738da5f8ada46c5a38d78c6aeaa3039cb1dddcf1ea6853dd343.
//
// Solidity: event EditionSteppedSaleBuy(uint256 indexed _editionId, uint256 indexed _tokenId, address _buyer, uint256 _price, uint16 _currentStep)
func (_Knownorigin0 *Knownorigin0Filterer) WatchEditionSteppedSaleBuy(opts *bind.WatchOpts, sink chan<- *Knownorigin0EditionSteppedSaleBuy, _editionId []*big.Int, _tokenId []*big.Int) (event.Subscription, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}
	var _tokenIdRule []interface{}
	for _, _tokenIdItem := range _tokenId {
		_tokenIdRule = append(_tokenIdRule, _tokenIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "EditionSteppedSaleBuy", _editionIdRule, _tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0EditionSteppedSaleBuy)
				if err := _Knownorigin0.contract.UnpackLog(event, "EditionSteppedSaleBuy", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEditionSteppedSaleBuy is a log parse operation binding the contract event 0x41820cf4351ec738da5f8ada46c5a38d78c6aeaa3039cb1dddcf1ea6853dd343.
//
// Solidity: event EditionSteppedSaleBuy(uint256 indexed _editionId, uint256 indexed _tokenId, address _buyer, uint256 _price, uint16 _currentStep)
func (_Knownorigin0 *Knownorigin0Filterer) ParseEditionSteppedSaleBuy(log types.Log) (*Knownorigin0EditionSteppedSaleBuy, error) {
	event := new(Knownorigin0EditionSteppedSaleBuy)
	if err := _Knownorigin0.contract.UnpackLog(event, "EditionSteppedSaleBuy", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0EditionSteppedSaleListedIterator is returned from FilterEditionSteppedSaleListed and is used to iterate over the raw logs and unpacked data for EditionSteppedSaleListed events raised by the Knownorigin0 contract.
type Knownorigin0EditionSteppedSaleListedIterator struct {
	Event *Knownorigin0EditionSteppedSaleListed // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0EditionSteppedSaleListedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0EditionSteppedSaleListed)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0EditionSteppedSaleListed)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0EditionSteppedSaleListedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0EditionSteppedSaleListedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0EditionSteppedSaleListed represents a EditionSteppedSaleListed event raised by the Knownorigin0 contract.
type Knownorigin0EditionSteppedSaleListed struct {
	EditionId *big.Int
	BasePrice *big.Int
	StepPrice *big.Int
	StartDate *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterEditionSteppedSaleListed is a free log retrieval operation binding the contract event 0xde833f743c6e9e4ce17e0e6662f66d3af9ecba83780a7c10cd54ffa7b049562e.
//
// Solidity: event EditionSteppedSaleListed(uint256 indexed _editionId, uint128 _basePrice, uint128 _stepPrice, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) FilterEditionSteppedSaleListed(opts *bind.FilterOpts, _editionId []*big.Int) (*Knownorigin0EditionSteppedSaleListedIterator, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "EditionSteppedSaleListed", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0EditionSteppedSaleListedIterator{contract: _Knownorigin0.contract, event: "EditionSteppedSaleListed", logs: logs, sub: sub}, nil
}

// WatchEditionSteppedSaleListed is a free log subscription operation binding the contract event 0xde833f743c6e9e4ce17e0e6662f66d3af9ecba83780a7c10cd54ffa7b049562e.
//
// Solidity: event EditionSteppedSaleListed(uint256 indexed _editionId, uint128 _basePrice, uint128 _stepPrice, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) WatchEditionSteppedSaleListed(opts *bind.WatchOpts, sink chan<- *Knownorigin0EditionSteppedSaleListed, _editionId []*big.Int) (event.Subscription, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "EditionSteppedSaleListed", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0EditionSteppedSaleListed)
				if err := _Knownorigin0.contract.UnpackLog(event, "EditionSteppedSaleListed", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEditionSteppedSaleListed is a log parse operation binding the contract event 0xde833f743c6e9e4ce17e0e6662f66d3af9ecba83780a7c10cd54ffa7b049562e.
//
// Solidity: event EditionSteppedSaleListed(uint256 indexed _editionId, uint128 _basePrice, uint128 _stepPrice, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) ParseEditionSteppedSaleListed(log types.Log) (*Knownorigin0EditionSteppedSaleListed, error) {
	event := new(Knownorigin0EditionSteppedSaleListed)
	if err := _Knownorigin0.contract.UnpackLog(event, "EditionSteppedSaleListed", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0EmergencyBidWithdrawFromReserveAuctionIterator is returned from FilterEmergencyBidWithdrawFromReserveAuction and is used to iterate over the raw logs and unpacked data for EmergencyBidWithdrawFromReserveAuction events raised by the Knownorigin0 contract.
type Knownorigin0EmergencyBidWithdrawFromReserveAuctionIterator struct {
	Event *Knownorigin0EmergencyBidWithdrawFromReserveAuction // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0EmergencyBidWithdrawFromReserveAuctionIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0EmergencyBidWithdrawFromReserveAuction)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0EmergencyBidWithdrawFromReserveAuction)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0EmergencyBidWithdrawFromReserveAuctionIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0EmergencyBidWithdrawFromReserveAuctionIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0EmergencyBidWithdrawFromReserveAuction represents a EmergencyBidWithdrawFromReserveAuction event raised by the Knownorigin0 contract.
type Knownorigin0EmergencyBidWithdrawFromReserveAuction struct {
	Id     *big.Int
	Bidder common.Address
	Bid    *big.Int
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterEmergencyBidWithdrawFromReserveAuction is a free log retrieval operation binding the contract event 0x5254bc5658a9d8a2b41347762f2b035d708f4be1013ab8fb33922576e24570fe.
//
// Solidity: event EmergencyBidWithdrawFromReserveAuction(uint256 indexed _id, address _bidder, uint128 _bid)
func (_Knownorigin0 *Knownorigin0Filterer) FilterEmergencyBidWithdrawFromReserveAuction(opts *bind.FilterOpts, _id []*big.Int) (*Knownorigin0EmergencyBidWithdrawFromReserveAuctionIterator, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "EmergencyBidWithdrawFromReserveAuction", _idRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0EmergencyBidWithdrawFromReserveAuctionIterator{contract: _Knownorigin0.contract, event: "EmergencyBidWithdrawFromReserveAuction", logs: logs, sub: sub}, nil
}

// WatchEmergencyBidWithdrawFromReserveAuction is a free log subscription operation binding the contract event 0x5254bc5658a9d8a2b41347762f2b035d708f4be1013ab8fb33922576e24570fe.
//
// Solidity: event EmergencyBidWithdrawFromReserveAuction(uint256 indexed _id, address _bidder, uint128 _bid)
func (_Knownorigin0 *Knownorigin0Filterer) WatchEmergencyBidWithdrawFromReserveAuction(opts *bind.WatchOpts, sink chan<- *Knownorigin0EmergencyBidWithdrawFromReserveAuction, _id []*big.Int) (event.Subscription, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "EmergencyBidWithdrawFromReserveAuction", _idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0EmergencyBidWithdrawFromReserveAuction)
				if err := _Knownorigin0.contract.UnpackLog(event, "EmergencyBidWithdrawFromReserveAuction", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEmergencyBidWithdrawFromReserveAuction is a log parse operation binding the contract event 0x5254bc5658a9d8a2b41347762f2b035d708f4be1013ab8fb33922576e24570fe.
//
// Solidity: event EmergencyBidWithdrawFromReserveAuction(uint256 indexed _id, address _bidder, uint128 _bid)
func (_Knownorigin0 *Knownorigin0Filterer) ParseEmergencyBidWithdrawFromReserveAuction(log types.Log) (*Knownorigin0EmergencyBidWithdrawFromReserveAuction, error) {
	event := new(Knownorigin0EmergencyBidWithdrawFromReserveAuction)
	if err := _Knownorigin0.contract.UnpackLog(event, "EmergencyBidWithdrawFromReserveAuction", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0ListedForBuyNowIterator is returned from FilterListedForBuyNow and is used to iterate over the raw logs and unpacked data for ListedForBuyNow events raised by the Knownorigin0 contract.
type Knownorigin0ListedForBuyNowIterator struct {
	Event *Knownorigin0ListedForBuyNow // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0ListedForBuyNowIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0ListedForBuyNow)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0ListedForBuyNow)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0ListedForBuyNowIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0ListedForBuyNowIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0ListedForBuyNow represents a ListedForBuyNow event raised by the Knownorigin0 contract.
type Knownorigin0ListedForBuyNow struct {
	Id           *big.Int
	Price        *big.Int
	CurrentOwner common.Address
	StartDate    *big.Int
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterListedForBuyNow is a free log retrieval operation binding the contract event 0x7fd7de54b3a4cc7952845fd3fa85208e0ba980067ead6d59721bebc8bd7ec414.
//
// Solidity: event ListedForBuyNow(uint256 indexed _id, uint256 _price, address _currentOwner, uint256 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) FilterListedForBuyNow(opts *bind.FilterOpts, _id []*big.Int) (*Knownorigin0ListedForBuyNowIterator, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "ListedForBuyNow", _idRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0ListedForBuyNowIterator{contract: _Knownorigin0.contract, event: "ListedForBuyNow", logs: logs, sub: sub}, nil
}

// WatchListedForBuyNow is a free log subscription operation binding the contract event 0x7fd7de54b3a4cc7952845fd3fa85208e0ba980067ead6d59721bebc8bd7ec414.
//
// Solidity: event ListedForBuyNow(uint256 indexed _id, uint256 _price, address _currentOwner, uint256 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) WatchListedForBuyNow(opts *bind.WatchOpts, sink chan<- *Knownorigin0ListedForBuyNow, _id []*big.Int) (event.Subscription, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "ListedForBuyNow", _idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0ListedForBuyNow)
				if err := _Knownorigin0.contract.UnpackLog(event, "ListedForBuyNow", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseListedForBuyNow is a log parse operation binding the contract event 0x7fd7de54b3a4cc7952845fd3fa85208e0ba980067ead6d59721bebc8bd7ec414.
//
// Solidity: event ListedForBuyNow(uint256 indexed _id, uint256 _price, address _currentOwner, uint256 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) ParseListedForBuyNow(log types.Log) (*Knownorigin0ListedForBuyNow, error) {
	event := new(Knownorigin0ListedForBuyNow)
	if err := _Knownorigin0.contract.UnpackLog(event, "ListedForBuyNow", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0ListedForReserveAuctionIterator is returned from FilterListedForReserveAuction and is used to iterate over the raw logs and unpacked data for ListedForReserveAuction events raised by the Knownorigin0 contract.
type Knownorigin0ListedForReserveAuctionIterator struct {
	Event *Knownorigin0ListedForReserveAuction // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0ListedForReserveAuctionIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0ListedForReserveAuction)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0ListedForReserveAuction)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0ListedForReserveAuctionIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0ListedForReserveAuctionIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0ListedForReserveAuction represents a ListedForReserveAuction event raised by the Knownorigin0 contract.
type Knownorigin0ListedForReserveAuction struct {
	Id           *big.Int
	ReservePrice *big.Int
	StartDate    *big.Int
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterListedForReserveAuction is a free log retrieval operation binding the contract event 0xcd712ccc5669f36ad4dfb9acf905087c4b438eabe53e24c858c72bfa0515cb98.
//
// Solidity: event ListedForReserveAuction(uint256 indexed _id, uint256 _reservePrice, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) FilterListedForReserveAuction(opts *bind.FilterOpts, _id []*big.Int) (*Knownorigin0ListedForReserveAuctionIterator, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "ListedForReserveAuction", _idRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0ListedForReserveAuctionIterator{contract: _Knownorigin0.contract, event: "ListedForReserveAuction", logs: logs, sub: sub}, nil
}

// WatchListedForReserveAuction is a free log subscription operation binding the contract event 0xcd712ccc5669f36ad4dfb9acf905087c4b438eabe53e24c858c72bfa0515cb98.
//
// Solidity: event ListedForReserveAuction(uint256 indexed _id, uint256 _reservePrice, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) WatchListedForReserveAuction(opts *bind.WatchOpts, sink chan<- *Knownorigin0ListedForReserveAuction, _id []*big.Int) (event.Subscription, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "ListedForReserveAuction", _idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0ListedForReserveAuction)
				if err := _Knownorigin0.contract.UnpackLog(event, "ListedForReserveAuction", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseListedForReserveAuction is a log parse operation binding the contract event 0xcd712ccc5669f36ad4dfb9acf905087c4b438eabe53e24c858c72bfa0515cb98.
//
// Solidity: event ListedForReserveAuction(uint256 indexed _id, uint256 _reservePrice, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) ParseListedForReserveAuction(log types.Log) (*Knownorigin0ListedForReserveAuction, error) {
	event := new(Knownorigin0ListedForReserveAuction)
	if err := _Knownorigin0.contract.UnpackLog(event, "ListedForReserveAuction", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0PausedIterator is returned from FilterPaused and is used to iterate over the raw logs and unpacked data for Paused events raised by the Knownorigin0 contract.
type Knownorigin0PausedIterator struct {
	Event *Knownorigin0Paused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0PausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0Paused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0Paused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0PausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0PausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0Paused represents a Paused event raised by the Knownorigin0 contract.
type Knownorigin0Paused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterPaused is a free log retrieval operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_Knownorigin0 *Knownorigin0Filterer) FilterPaused(opts *bind.FilterOpts) (*Knownorigin0PausedIterator, error) {

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return &Knownorigin0PausedIterator{contract: _Knownorigin0.contract, event: "Paused", logs: logs, sub: sub}, nil
}

// WatchPaused is a free log subscription operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_Knownorigin0 *Knownorigin0Filterer) WatchPaused(opts *bind.WatchOpts, sink chan<- *Knownorigin0Paused) (event.Subscription, error) {

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0Paused)
				if err := _Knownorigin0.contract.UnpackLog(event, "Paused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePaused is a log parse operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_Knownorigin0 *Knownorigin0Filterer) ParsePaused(log types.Log) (*Knownorigin0Paused, error) {
	event := new(Knownorigin0Paused)
	if err := _Knownorigin0.contract.UnpackLog(event, "Paused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0PrimaryMarketplaceDeployedIterator is returned from FilterPrimaryMarketplaceDeployed and is used to iterate over the raw logs and unpacked data for PrimaryMarketplaceDeployed events raised by the Knownorigin0 contract.
type Knownorigin0PrimaryMarketplaceDeployedIterator struct {
	Event *Knownorigin0PrimaryMarketplaceDeployed // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0PrimaryMarketplaceDeployedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0PrimaryMarketplaceDeployed)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0PrimaryMarketplaceDeployed)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0PrimaryMarketplaceDeployedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0PrimaryMarketplaceDeployedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0PrimaryMarketplaceDeployed represents a PrimaryMarketplaceDeployed event raised by the Knownorigin0 contract.
type Knownorigin0PrimaryMarketplaceDeployed struct {
	Raw types.Log // Blockchain specific contextual infos
}

// FilterPrimaryMarketplaceDeployed is a free log retrieval operation binding the contract event 0x2c09563128f33976972b5f859a25a031d534b6895710d898ef01875708dc8ec5.
//
// Solidity: event PrimaryMarketplaceDeployed()
func (_Knownorigin0 *Knownorigin0Filterer) FilterPrimaryMarketplaceDeployed(opts *bind.FilterOpts) (*Knownorigin0PrimaryMarketplaceDeployedIterator, error) {

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "PrimaryMarketplaceDeployed")
	if err != nil {
		return nil, err
	}
	return &Knownorigin0PrimaryMarketplaceDeployedIterator{contract: _Knownorigin0.contract, event: "PrimaryMarketplaceDeployed", logs: logs, sub: sub}, nil
}

// WatchPrimaryMarketplaceDeployed is a free log subscription operation binding the contract event 0x2c09563128f33976972b5f859a25a031d534b6895710d898ef01875708dc8ec5.
//
// Solidity: event PrimaryMarketplaceDeployed()
func (_Knownorigin0 *Knownorigin0Filterer) WatchPrimaryMarketplaceDeployed(opts *bind.WatchOpts, sink chan<- *Knownorigin0PrimaryMarketplaceDeployed) (event.Subscription, error) {

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "PrimaryMarketplaceDeployed")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0PrimaryMarketplaceDeployed)
				if err := _Knownorigin0.contract.UnpackLog(event, "PrimaryMarketplaceDeployed", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePrimaryMarketplaceDeployed is a log parse operation binding the contract event 0x2c09563128f33976972b5f859a25a031d534b6895710d898ef01875708dc8ec5.
//
// Solidity: event PrimaryMarketplaceDeployed()
func (_Knownorigin0 *Knownorigin0Filterer) ParsePrimaryMarketplaceDeployed(log types.Log) (*Knownorigin0PrimaryMarketplaceDeployed, error) {
	event := new(Knownorigin0PrimaryMarketplaceDeployed)
	if err := _Knownorigin0.contract.UnpackLog(event, "PrimaryMarketplaceDeployed", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0ReserveAuctionConvertedToBuyItNowIterator is returned from FilterReserveAuctionConvertedToBuyItNow and is used to iterate over the raw logs and unpacked data for ReserveAuctionConvertedToBuyItNow events raised by the Knownorigin0 contract.
type Knownorigin0ReserveAuctionConvertedToBuyItNowIterator struct {
	Event *Knownorigin0ReserveAuctionConvertedToBuyItNow // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0ReserveAuctionConvertedToBuyItNowIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0ReserveAuctionConvertedToBuyItNow)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0ReserveAuctionConvertedToBuyItNow)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0ReserveAuctionConvertedToBuyItNowIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0ReserveAuctionConvertedToBuyItNowIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0ReserveAuctionConvertedToBuyItNow represents a ReserveAuctionConvertedToBuyItNow event raised by the Knownorigin0 contract.
type Knownorigin0ReserveAuctionConvertedToBuyItNow struct {
	Id           *big.Int
	ListingPrice *big.Int
	StartDate    *big.Int
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterReserveAuctionConvertedToBuyItNow is a free log retrieval operation binding the contract event 0x926bdcbf39866f134eede353f606580c48749e851283a077fde2912543b659df.
//
// Solidity: event ReserveAuctionConvertedToBuyItNow(uint256 indexed _id, uint128 _listingPrice, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) FilterReserveAuctionConvertedToBuyItNow(opts *bind.FilterOpts, _id []*big.Int) (*Knownorigin0ReserveAuctionConvertedToBuyItNowIterator, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "ReserveAuctionConvertedToBuyItNow", _idRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0ReserveAuctionConvertedToBuyItNowIterator{contract: _Knownorigin0.contract, event: "ReserveAuctionConvertedToBuyItNow", logs: logs, sub: sub}, nil
}

// WatchReserveAuctionConvertedToBuyItNow is a free log subscription operation binding the contract event 0x926bdcbf39866f134eede353f606580c48749e851283a077fde2912543b659df.
//
// Solidity: event ReserveAuctionConvertedToBuyItNow(uint256 indexed _id, uint128 _listingPrice, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) WatchReserveAuctionConvertedToBuyItNow(opts *bind.WatchOpts, sink chan<- *Knownorigin0ReserveAuctionConvertedToBuyItNow, _id []*big.Int) (event.Subscription, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "ReserveAuctionConvertedToBuyItNow", _idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0ReserveAuctionConvertedToBuyItNow)
				if err := _Knownorigin0.contract.UnpackLog(event, "ReserveAuctionConvertedToBuyItNow", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseReserveAuctionConvertedToBuyItNow is a log parse operation binding the contract event 0x926bdcbf39866f134eede353f606580c48749e851283a077fde2912543b659df.
//
// Solidity: event ReserveAuctionConvertedToBuyItNow(uint256 indexed _id, uint128 _listingPrice, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) ParseReserveAuctionConvertedToBuyItNow(log types.Log) (*Knownorigin0ReserveAuctionConvertedToBuyItNow, error) {
	event := new(Knownorigin0ReserveAuctionConvertedToBuyItNow)
	if err := _Knownorigin0.contract.UnpackLog(event, "ReserveAuctionConvertedToBuyItNow", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0ReserveAuctionConvertedToOffersIterator is returned from FilterReserveAuctionConvertedToOffers and is used to iterate over the raw logs and unpacked data for ReserveAuctionConvertedToOffers events raised by the Knownorigin0 contract.
type Knownorigin0ReserveAuctionConvertedToOffersIterator struct {
	Event *Knownorigin0ReserveAuctionConvertedToOffers // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0ReserveAuctionConvertedToOffersIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0ReserveAuctionConvertedToOffers)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0ReserveAuctionConvertedToOffers)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0ReserveAuctionConvertedToOffersIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0ReserveAuctionConvertedToOffersIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0ReserveAuctionConvertedToOffers represents a ReserveAuctionConvertedToOffers event raised by the Knownorigin0 contract.
type Knownorigin0ReserveAuctionConvertedToOffers struct {
	EditionId *big.Int
	StartDate *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterReserveAuctionConvertedToOffers is a free log retrieval operation binding the contract event 0x5ad253cc2bfa6797b2f1b2a45aa1b0e961d3c6ff8090cc72122c1745d3e84101.
//
// Solidity: event ReserveAuctionConvertedToOffers(uint256 indexed _editionId, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) FilterReserveAuctionConvertedToOffers(opts *bind.FilterOpts, _editionId []*big.Int) (*Knownorigin0ReserveAuctionConvertedToOffersIterator, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "ReserveAuctionConvertedToOffers", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0ReserveAuctionConvertedToOffersIterator{contract: _Knownorigin0.contract, event: "ReserveAuctionConvertedToOffers", logs: logs, sub: sub}, nil
}

// WatchReserveAuctionConvertedToOffers is a free log subscription operation binding the contract event 0x5ad253cc2bfa6797b2f1b2a45aa1b0e961d3c6ff8090cc72122c1745d3e84101.
//
// Solidity: event ReserveAuctionConvertedToOffers(uint256 indexed _editionId, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) WatchReserveAuctionConvertedToOffers(opts *bind.WatchOpts, sink chan<- *Knownorigin0ReserveAuctionConvertedToOffers, _editionId []*big.Int) (event.Subscription, error) {

	var _editionIdRule []interface{}
	for _, _editionIdItem := range _editionId {
		_editionIdRule = append(_editionIdRule, _editionIdItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "ReserveAuctionConvertedToOffers", _editionIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0ReserveAuctionConvertedToOffers)
				if err := _Knownorigin0.contract.UnpackLog(event, "ReserveAuctionConvertedToOffers", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseReserveAuctionConvertedToOffers is a log parse operation binding the contract event 0x5ad253cc2bfa6797b2f1b2a45aa1b0e961d3c6ff8090cc72122c1745d3e84101.
//
// Solidity: event ReserveAuctionConvertedToOffers(uint256 indexed _editionId, uint128 _startDate)
func (_Knownorigin0 *Knownorigin0Filterer) ParseReserveAuctionConvertedToOffers(log types.Log) (*Knownorigin0ReserveAuctionConvertedToOffers, error) {
	event := new(Knownorigin0ReserveAuctionConvertedToOffers)
	if err := _Knownorigin0.contract.UnpackLog(event, "ReserveAuctionConvertedToOffers", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0ReserveAuctionResultedIterator is returned from FilterReserveAuctionResulted and is used to iterate over the raw logs and unpacked data for ReserveAuctionResulted events raised by the Knownorigin0 contract.
type Knownorigin0ReserveAuctionResultedIterator struct {
	Event *Knownorigin0ReserveAuctionResulted // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0ReserveAuctionResultedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0ReserveAuctionResulted)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0ReserveAuctionResulted)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0ReserveAuctionResultedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0ReserveAuctionResultedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0ReserveAuctionResulted represents a ReserveAuctionResulted event raised by the Knownorigin0 contract.
type Knownorigin0ReserveAuctionResulted struct {
	Id           *big.Int
	FinalPrice   *big.Int
	CurrentOwner common.Address
	Winner       common.Address
	Resulter     common.Address
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterReserveAuctionResulted is a free log retrieval operation binding the contract event 0xa0a548bb147d15d7de045c333ab8b0ec3d0cc790775d009185aa0e009437507e.
//
// Solidity: event ReserveAuctionResulted(uint256 indexed _id, uint256 _finalPrice, address _currentOwner, address _winner, address _resulter)
func (_Knownorigin0 *Knownorigin0Filterer) FilterReserveAuctionResulted(opts *bind.FilterOpts, _id []*big.Int) (*Knownorigin0ReserveAuctionResultedIterator, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "ReserveAuctionResulted", _idRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0ReserveAuctionResultedIterator{contract: _Knownorigin0.contract, event: "ReserveAuctionResulted", logs: logs, sub: sub}, nil
}

// WatchReserveAuctionResulted is a free log subscription operation binding the contract event 0xa0a548bb147d15d7de045c333ab8b0ec3d0cc790775d009185aa0e009437507e.
//
// Solidity: event ReserveAuctionResulted(uint256 indexed _id, uint256 _finalPrice, address _currentOwner, address _winner, address _resulter)
func (_Knownorigin0 *Knownorigin0Filterer) WatchReserveAuctionResulted(opts *bind.WatchOpts, sink chan<- *Knownorigin0ReserveAuctionResulted, _id []*big.Int) (event.Subscription, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "ReserveAuctionResulted", _idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0ReserveAuctionResulted)
				if err := _Knownorigin0.contract.UnpackLog(event, "ReserveAuctionResulted", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseReserveAuctionResulted is a log parse operation binding the contract event 0xa0a548bb147d15d7de045c333ab8b0ec3d0cc790775d009185aa0e009437507e.
//
// Solidity: event ReserveAuctionResulted(uint256 indexed _id, uint256 _finalPrice, address _currentOwner, address _winner, address _resulter)
func (_Knownorigin0 *Knownorigin0Filterer) ParseReserveAuctionResulted(log types.Log) (*Knownorigin0ReserveAuctionResulted, error) {
	event := new(Knownorigin0ReserveAuctionResulted)
	if err := _Knownorigin0.contract.UnpackLog(event, "ReserveAuctionResulted", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0ReservePriceUpdatedIterator is returned from FilterReservePriceUpdated and is used to iterate over the raw logs and unpacked data for ReservePriceUpdated events raised by the Knownorigin0 contract.
type Knownorigin0ReservePriceUpdatedIterator struct {
	Event *Knownorigin0ReservePriceUpdated // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0ReservePriceUpdatedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0ReservePriceUpdated)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0ReservePriceUpdated)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0ReservePriceUpdatedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0ReservePriceUpdatedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0ReservePriceUpdated represents a ReservePriceUpdated event raised by the Knownorigin0 contract.
type Knownorigin0ReservePriceUpdated struct {
	Id           *big.Int
	ReservePrice *big.Int
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterReservePriceUpdated is a free log retrieval operation binding the contract event 0x87c77544fe96aeb9a3a6398d27ed891f9532d66ef82444a0a56aab18a58bbece.
//
// Solidity: event ReservePriceUpdated(uint256 indexed _id, uint256 _reservePrice)
func (_Knownorigin0 *Knownorigin0Filterer) FilterReservePriceUpdated(opts *bind.FilterOpts, _id []*big.Int) (*Knownorigin0ReservePriceUpdatedIterator, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "ReservePriceUpdated", _idRule)
	if err != nil {
		return nil, err
	}
	return &Knownorigin0ReservePriceUpdatedIterator{contract: _Knownorigin0.contract, event: "ReservePriceUpdated", logs: logs, sub: sub}, nil
}

// WatchReservePriceUpdated is a free log subscription operation binding the contract event 0x87c77544fe96aeb9a3a6398d27ed891f9532d66ef82444a0a56aab18a58bbece.
//
// Solidity: event ReservePriceUpdated(uint256 indexed _id, uint256 _reservePrice)
func (_Knownorigin0 *Knownorigin0Filterer) WatchReservePriceUpdated(opts *bind.WatchOpts, sink chan<- *Knownorigin0ReservePriceUpdated, _id []*big.Int) (event.Subscription, error) {

	var _idRule []interface{}
	for _, _idItem := range _id {
		_idRule = append(_idRule, _idItem)
	}

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "ReservePriceUpdated", _idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0ReservePriceUpdated)
				if err := _Knownorigin0.contract.UnpackLog(event, "ReservePriceUpdated", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseReservePriceUpdated is a log parse operation binding the contract event 0x87c77544fe96aeb9a3a6398d27ed891f9532d66ef82444a0a56aab18a58bbece.
//
// Solidity: event ReservePriceUpdated(uint256 indexed _id, uint256 _reservePrice)
func (_Knownorigin0 *Knownorigin0Filterer) ParseReservePriceUpdated(log types.Log) (*Knownorigin0ReservePriceUpdated, error) {
	event := new(Knownorigin0ReservePriceUpdated)
	if err := _Knownorigin0.contract.UnpackLog(event, "ReservePriceUpdated", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Knownorigin0UnpausedIterator is returned from FilterUnpaused and is used to iterate over the raw logs and unpacked data for Unpaused events raised by the Knownorigin0 contract.
type Knownorigin0UnpausedIterator struct {
	Event *Knownorigin0Unpaused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Knownorigin0UnpausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Knownorigin0Unpaused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Knownorigin0Unpaused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Knownorigin0UnpausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Knownorigin0UnpausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Knownorigin0Unpaused represents a Unpaused event raised by the Knownorigin0 contract.
type Knownorigin0Unpaused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterUnpaused is a free log retrieval operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_Knownorigin0 *Knownorigin0Filterer) FilterUnpaused(opts *bind.FilterOpts) (*Knownorigin0UnpausedIterator, error) {

	logs, sub, err := _Knownorigin0.contract.FilterLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return &Knownorigin0UnpausedIterator{contract: _Knownorigin0.contract, event: "Unpaused", logs: logs, sub: sub}, nil
}

// WatchUnpaused is a free log subscription operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_Knownorigin0 *Knownorigin0Filterer) WatchUnpaused(opts *bind.WatchOpts, sink chan<- *Knownorigin0Unpaused) (event.Subscription, error) {

	logs, sub, err := _Knownorigin0.contract.WatchLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Knownorigin0Unpaused)
				if err := _Knownorigin0.contract.UnpackLog(event, "Unpaused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUnpaused is a log parse operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_Knownorigin0 *Knownorigin0Filterer) ParseUnpaused(log types.Log) (*Knownorigin0Unpaused, error) {
	event := new(Knownorigin0Unpaused)
	if err := _Knownorigin0.contract.UnpackLog(event, "Unpaused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
