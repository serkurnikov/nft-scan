// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package looksrare_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// OrderTypesMakerOrder is an auto generated low-level Go binding around an user-defined struct.
type OrderTypesMakerOrder struct {
	IsOrderAsk         bool
	Signer             common.Address
	Collection         common.Address
	Price              *big.Int
	TokenId            *big.Int
	Amount             *big.Int
	Strategy           common.Address
	Currency           common.Address
	Nonce              *big.Int
	StartTime          *big.Int
	EndTime            *big.Int
	MinPercentageToAsk *big.Int
	Params             []byte
	V                  uint8
	R                  [32]byte
	S                  [32]byte
}

// OrderTypesTakerOrder is an auto generated low-level Go binding around an user-defined struct.
type OrderTypesTakerOrder struct {
	IsOrderAsk         bool
	Taker              common.Address
	Price              *big.Int
	TokenId            *big.Int
	MinPercentageToAsk *big.Int
	Params             []byte
}

// Looksrare0MetaData contains all meta data concerning the Looksrare0 contract.
var Looksrare0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_currencyManager\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_executionManager\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_royaltyFeeManager\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_WETH\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_protocolFeeRecipient\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"newMinNonce\",\"type\":\"uint256\"}],\"name\":\"CancelAllOrders\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256[]\",\"name\":\"orderNonces\",\"type\":\"uint256[]\"}],\"name\":\"CancelMultipleOrders\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"currencyManager\",\"type\":\"address\"}],\"name\":\"NewCurrencyManager\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"executionManager\",\"type\":\"address\"}],\"name\":\"NewExecutionManager\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"protocolFeeRecipient\",\"type\":\"address\"}],\"name\":\"NewProtocolFeeRecipient\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"royaltyFeeManager\",\"type\":\"address\"}],\"name\":\"NewRoyaltyFeeManager\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"transferSelectorNFT\",\"type\":\"address\"}],\"name\":\"NewTransferSelectorNFT\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"royaltyRecipient\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"currency\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"RoyaltyPayment\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"orderHash\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"orderNonce\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"taker\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"maker\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"strategy\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"currency\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"TakerAsk\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"orderHash\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"orderNonce\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"taker\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"maker\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"strategy\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"currency\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"TakerBid\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"DOMAIN_SEPARATOR\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"WETH\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"minNonce\",\"type\":\"uint256\"}],\"name\":\"cancelAllOrdersForSender\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256[]\",\"name\":\"orderNonces\",\"type\":\"uint256[]\"}],\"name\":\"cancelMultipleMakerOrders\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"currencyManager\",\"outputs\":[{\"internalType\":\"contractICurrencyManager\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"executionManager\",\"outputs\":[{\"internalType\":\"contractIExecutionManager\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"orderNonce\",\"type\":\"uint256\"}],\"name\":\"isUserOrderNonceExecutedOrCancelled\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"bool\",\"name\":\"isOrderAsk\",\"type\":\"bool\"},{\"internalType\":\"address\",\"name\":\"taker\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minPercentageToAsk\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"params\",\"type\":\"bytes\"}],\"internalType\":\"structOrderTypes.TakerOrder\",\"name\":\"takerBid\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"bool\",\"name\":\"isOrderAsk\",\"type\":\"bool\"},{\"internalType\":\"address\",\"name\":\"signer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"strategy\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"currency\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"nonce\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minPercentageToAsk\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"params\",\"type\":\"bytes\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"internalType\":\"structOrderTypes.MakerOrder\",\"name\":\"makerAsk\",\"type\":\"tuple\"}],\"name\":\"matchAskWithTakerBid\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"bool\",\"name\":\"isOrderAsk\",\"type\":\"bool\"},{\"internalType\":\"address\",\"name\":\"taker\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minPercentageToAsk\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"params\",\"type\":\"bytes\"}],\"internalType\":\"structOrderTypes.TakerOrder\",\"name\":\"takerBid\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"bool\",\"name\":\"isOrderAsk\",\"type\":\"bool\"},{\"internalType\":\"address\",\"name\":\"signer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"strategy\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"currency\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"nonce\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minPercentageToAsk\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"params\",\"type\":\"bytes\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"internalType\":\"structOrderTypes.MakerOrder\",\"name\":\"makerAsk\",\"type\":\"tuple\"}],\"name\":\"matchAskWithTakerBidUsingETHAndWETH\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"bool\",\"name\":\"isOrderAsk\",\"type\":\"bool\"},{\"internalType\":\"address\",\"name\":\"taker\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minPercentageToAsk\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"params\",\"type\":\"bytes\"}],\"internalType\":\"structOrderTypes.TakerOrder\",\"name\":\"takerAsk\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"bool\",\"name\":\"isOrderAsk\",\"type\":\"bool\"},{\"internalType\":\"address\",\"name\":\"signer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"strategy\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"currency\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"nonce\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minPercentageToAsk\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"params\",\"type\":\"bytes\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"internalType\":\"structOrderTypes.MakerOrder\",\"name\":\"makerBid\",\"type\":\"tuple\"}],\"name\":\"matchBidWithTakerAsk\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"protocolFeeRecipient\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"royaltyFeeManager\",\"outputs\":[{\"internalType\":\"contractIRoyaltyFeeManager\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"transferSelectorNFT\",\"outputs\":[{\"internalType\":\"contractITransferSelectorNFT\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_currencyManager\",\"type\":\"address\"}],\"name\":\"updateCurrencyManager\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_executionManager\",\"type\":\"address\"}],\"name\":\"updateExecutionManager\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_protocolFeeRecipient\",\"type\":\"address\"}],\"name\":\"updateProtocolFeeRecipient\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_royaltyFeeManager\",\"type\":\"address\"}],\"name\":\"updateRoyaltyFeeManager\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_transferSelectorNFT\",\"type\":\"address\"}],\"name\":\"updateTransferSelectorNFT\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"userMinOrderNonce\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
}

// Looksrare0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Looksrare0MetaData.ABI instead.
var Looksrare0ABI = Looksrare0MetaData.ABI

// Looksrare0 is an auto generated Go binding around an Ethereum contract.
type Looksrare0 struct {
	Looksrare0Caller     // Read-only binding to the contract
	Looksrare0Transactor // Write-only binding to the contract
	Looksrare0Filterer   // Log filterer for contract events
}

// Looksrare0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Looksrare0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Looksrare0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Looksrare0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Looksrare0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Looksrare0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Looksrare0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Looksrare0Session struct {
	Contract     *Looksrare0       // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Looksrare0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Looksrare0CallerSession struct {
	Contract *Looksrare0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts     // Call options to use throughout this session
}

// Looksrare0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Looksrare0TransactorSession struct {
	Contract     *Looksrare0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts     // Transaction auth options to use throughout this session
}

// Looksrare0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Looksrare0Raw struct {
	Contract *Looksrare0 // Generic contract binding to access the raw methods on
}

// Looksrare0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Looksrare0CallerRaw struct {
	Contract *Looksrare0Caller // Generic read-only contract binding to access the raw methods on
}

// Looksrare0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Looksrare0TransactorRaw struct {
	Contract *Looksrare0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewLooksrare0 creates a new instance of Looksrare0, bound to a specific deployed contract.
func NewLooksrare0(address common.Address, backend bind.ContractBackend) (*Looksrare0, error) {
	contract, err := bindLooksrare0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Looksrare0{Looksrare0Caller: Looksrare0Caller{contract: contract}, Looksrare0Transactor: Looksrare0Transactor{contract: contract}, Looksrare0Filterer: Looksrare0Filterer{contract: contract}}, nil
}

// NewLooksrare0Caller creates a new read-only instance of Looksrare0, bound to a specific deployed contract.
func NewLooksrare0Caller(address common.Address, caller bind.ContractCaller) (*Looksrare0Caller, error) {
	contract, err := bindLooksrare0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Looksrare0Caller{contract: contract}, nil
}

// NewLooksrare0Transactor creates a new write-only instance of Looksrare0, bound to a specific deployed contract.
func NewLooksrare0Transactor(address common.Address, transactor bind.ContractTransactor) (*Looksrare0Transactor, error) {
	contract, err := bindLooksrare0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Looksrare0Transactor{contract: contract}, nil
}

// NewLooksrare0Filterer creates a new log filterer instance of Looksrare0, bound to a specific deployed contract.
func NewLooksrare0Filterer(address common.Address, filterer bind.ContractFilterer) (*Looksrare0Filterer, error) {
	contract, err := bindLooksrare0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Looksrare0Filterer{contract: contract}, nil
}

// bindLooksrare0 binds a generic wrapper to an already deployed contract.
func bindLooksrare0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Looksrare0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Looksrare0 *Looksrare0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Looksrare0.Contract.Looksrare0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Looksrare0 *Looksrare0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Looksrare0.Contract.Looksrare0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Looksrare0 *Looksrare0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Looksrare0.Contract.Looksrare0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Looksrare0 *Looksrare0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Looksrare0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Looksrare0 *Looksrare0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Looksrare0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Looksrare0 *Looksrare0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Looksrare0.Contract.contract.Transact(opts, method, params...)
}

// DOMAINSEPARATOR is a free data retrieval call binding the contract method 0x3644e515.
//
// Solidity: function DOMAIN_SEPARATOR() view returns(bytes32)
func (_Looksrare0 *Looksrare0Caller) DOMAINSEPARATOR(opts *bind.CallOpts) ([32]byte, error) {
	var out []interface{}
	err := _Looksrare0.contract.Call(opts, &out, "DOMAIN_SEPARATOR")

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// DOMAINSEPARATOR is a free data retrieval call binding the contract method 0x3644e515.
//
// Solidity: function DOMAIN_SEPARATOR() view returns(bytes32)
func (_Looksrare0 *Looksrare0Session) DOMAINSEPARATOR() ([32]byte, error) {
	return _Looksrare0.Contract.DOMAINSEPARATOR(&_Looksrare0.CallOpts)
}

// DOMAINSEPARATOR is a free data retrieval call binding the contract method 0x3644e515.
//
// Solidity: function DOMAIN_SEPARATOR() view returns(bytes32)
func (_Looksrare0 *Looksrare0CallerSession) DOMAINSEPARATOR() ([32]byte, error) {
	return _Looksrare0.Contract.DOMAINSEPARATOR(&_Looksrare0.CallOpts)
}

// WETH is a free data retrieval call binding the contract method 0xad5c4648.
//
// Solidity: function WETH() view returns(address)
func (_Looksrare0 *Looksrare0Caller) WETH(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Looksrare0.contract.Call(opts, &out, "WETH")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// WETH is a free data retrieval call binding the contract method 0xad5c4648.
//
// Solidity: function WETH() view returns(address)
func (_Looksrare0 *Looksrare0Session) WETH() (common.Address, error) {
	return _Looksrare0.Contract.WETH(&_Looksrare0.CallOpts)
}

// WETH is a free data retrieval call binding the contract method 0xad5c4648.
//
// Solidity: function WETH() view returns(address)
func (_Looksrare0 *Looksrare0CallerSession) WETH() (common.Address, error) {
	return _Looksrare0.Contract.WETH(&_Looksrare0.CallOpts)
}

// CurrencyManager is a free data retrieval call binding the contract method 0x0f747d74.
//
// Solidity: function currencyManager() view returns(address)
func (_Looksrare0 *Looksrare0Caller) CurrencyManager(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Looksrare0.contract.Call(opts, &out, "currencyManager")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// CurrencyManager is a free data retrieval call binding the contract method 0x0f747d74.
//
// Solidity: function currencyManager() view returns(address)
func (_Looksrare0 *Looksrare0Session) CurrencyManager() (common.Address, error) {
	return _Looksrare0.Contract.CurrencyManager(&_Looksrare0.CallOpts)
}

// CurrencyManager is a free data retrieval call binding the contract method 0x0f747d74.
//
// Solidity: function currencyManager() view returns(address)
func (_Looksrare0 *Looksrare0CallerSession) CurrencyManager() (common.Address, error) {
	return _Looksrare0.Contract.CurrencyManager(&_Looksrare0.CallOpts)
}

// ExecutionManager is a free data retrieval call binding the contract method 0x483abb9f.
//
// Solidity: function executionManager() view returns(address)
func (_Looksrare0 *Looksrare0Caller) ExecutionManager(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Looksrare0.contract.Call(opts, &out, "executionManager")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// ExecutionManager is a free data retrieval call binding the contract method 0x483abb9f.
//
// Solidity: function executionManager() view returns(address)
func (_Looksrare0 *Looksrare0Session) ExecutionManager() (common.Address, error) {
	return _Looksrare0.Contract.ExecutionManager(&_Looksrare0.CallOpts)
}

// ExecutionManager is a free data retrieval call binding the contract method 0x483abb9f.
//
// Solidity: function executionManager() view returns(address)
func (_Looksrare0 *Looksrare0CallerSession) ExecutionManager() (common.Address, error) {
	return _Looksrare0.Contract.ExecutionManager(&_Looksrare0.CallOpts)
}

// IsUserOrderNonceExecutedOrCancelled is a free data retrieval call binding the contract method 0x31e27e27.
//
// Solidity: function isUserOrderNonceExecutedOrCancelled(address user, uint256 orderNonce) view returns(bool)
func (_Looksrare0 *Looksrare0Caller) IsUserOrderNonceExecutedOrCancelled(opts *bind.CallOpts, user common.Address, orderNonce *big.Int) (bool, error) {
	var out []interface{}
	err := _Looksrare0.contract.Call(opts, &out, "isUserOrderNonceExecutedOrCancelled", user, orderNonce)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsUserOrderNonceExecutedOrCancelled is a free data retrieval call binding the contract method 0x31e27e27.
//
// Solidity: function isUserOrderNonceExecutedOrCancelled(address user, uint256 orderNonce) view returns(bool)
func (_Looksrare0 *Looksrare0Session) IsUserOrderNonceExecutedOrCancelled(user common.Address, orderNonce *big.Int) (bool, error) {
	return _Looksrare0.Contract.IsUserOrderNonceExecutedOrCancelled(&_Looksrare0.CallOpts, user, orderNonce)
}

// IsUserOrderNonceExecutedOrCancelled is a free data retrieval call binding the contract method 0x31e27e27.
//
// Solidity: function isUserOrderNonceExecutedOrCancelled(address user, uint256 orderNonce) view returns(bool)
func (_Looksrare0 *Looksrare0CallerSession) IsUserOrderNonceExecutedOrCancelled(user common.Address, orderNonce *big.Int) (bool, error) {
	return _Looksrare0.Contract.IsUserOrderNonceExecutedOrCancelled(&_Looksrare0.CallOpts, user, orderNonce)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Looksrare0 *Looksrare0Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Looksrare0.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Looksrare0 *Looksrare0Session) Owner() (common.Address, error) {
	return _Looksrare0.Contract.Owner(&_Looksrare0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Looksrare0 *Looksrare0CallerSession) Owner() (common.Address, error) {
	return _Looksrare0.Contract.Owner(&_Looksrare0.CallOpts)
}

// ProtocolFeeRecipient is a free data retrieval call binding the contract method 0x64df049e.
//
// Solidity: function protocolFeeRecipient() view returns(address)
func (_Looksrare0 *Looksrare0Caller) ProtocolFeeRecipient(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Looksrare0.contract.Call(opts, &out, "protocolFeeRecipient")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// ProtocolFeeRecipient is a free data retrieval call binding the contract method 0x64df049e.
//
// Solidity: function protocolFeeRecipient() view returns(address)
func (_Looksrare0 *Looksrare0Session) ProtocolFeeRecipient() (common.Address, error) {
	return _Looksrare0.Contract.ProtocolFeeRecipient(&_Looksrare0.CallOpts)
}

// ProtocolFeeRecipient is a free data retrieval call binding the contract method 0x64df049e.
//
// Solidity: function protocolFeeRecipient() view returns(address)
func (_Looksrare0 *Looksrare0CallerSession) ProtocolFeeRecipient() (common.Address, error) {
	return _Looksrare0.Contract.ProtocolFeeRecipient(&_Looksrare0.CallOpts)
}

// RoyaltyFeeManager is a free data retrieval call binding the contract method 0x87e4401f.
//
// Solidity: function royaltyFeeManager() view returns(address)
func (_Looksrare0 *Looksrare0Caller) RoyaltyFeeManager(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Looksrare0.contract.Call(opts, &out, "royaltyFeeManager")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// RoyaltyFeeManager is a free data retrieval call binding the contract method 0x87e4401f.
//
// Solidity: function royaltyFeeManager() view returns(address)
func (_Looksrare0 *Looksrare0Session) RoyaltyFeeManager() (common.Address, error) {
	return _Looksrare0.Contract.RoyaltyFeeManager(&_Looksrare0.CallOpts)
}

// RoyaltyFeeManager is a free data retrieval call binding the contract method 0x87e4401f.
//
// Solidity: function royaltyFeeManager() view returns(address)
func (_Looksrare0 *Looksrare0CallerSession) RoyaltyFeeManager() (common.Address, error) {
	return _Looksrare0.Contract.RoyaltyFeeManager(&_Looksrare0.CallOpts)
}

// TransferSelectorNFT is a free data retrieval call binding the contract method 0x5e14f68e.
//
// Solidity: function transferSelectorNFT() view returns(address)
func (_Looksrare0 *Looksrare0Caller) TransferSelectorNFT(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Looksrare0.contract.Call(opts, &out, "transferSelectorNFT")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// TransferSelectorNFT is a free data retrieval call binding the contract method 0x5e14f68e.
//
// Solidity: function transferSelectorNFT() view returns(address)
func (_Looksrare0 *Looksrare0Session) TransferSelectorNFT() (common.Address, error) {
	return _Looksrare0.Contract.TransferSelectorNFT(&_Looksrare0.CallOpts)
}

// TransferSelectorNFT is a free data retrieval call binding the contract method 0x5e14f68e.
//
// Solidity: function transferSelectorNFT() view returns(address)
func (_Looksrare0 *Looksrare0CallerSession) TransferSelectorNFT() (common.Address, error) {
	return _Looksrare0.Contract.TransferSelectorNFT(&_Looksrare0.CallOpts)
}

// UserMinOrderNonce is a free data retrieval call binding the contract method 0x4266581e.
//
// Solidity: function userMinOrderNonce(address ) view returns(uint256)
func (_Looksrare0 *Looksrare0Caller) UserMinOrderNonce(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Looksrare0.contract.Call(opts, &out, "userMinOrderNonce", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// UserMinOrderNonce is a free data retrieval call binding the contract method 0x4266581e.
//
// Solidity: function userMinOrderNonce(address ) view returns(uint256)
func (_Looksrare0 *Looksrare0Session) UserMinOrderNonce(arg0 common.Address) (*big.Int, error) {
	return _Looksrare0.Contract.UserMinOrderNonce(&_Looksrare0.CallOpts, arg0)
}

// UserMinOrderNonce is a free data retrieval call binding the contract method 0x4266581e.
//
// Solidity: function userMinOrderNonce(address ) view returns(uint256)
func (_Looksrare0 *Looksrare0CallerSession) UserMinOrderNonce(arg0 common.Address) (*big.Int, error) {
	return _Looksrare0.Contract.UserMinOrderNonce(&_Looksrare0.CallOpts, arg0)
}

// CancelAllOrdersForSender is a paid mutator transaction binding the contract method 0xcbd2ec65.
//
// Solidity: function cancelAllOrdersForSender(uint256 minNonce) returns()
func (_Looksrare0 *Looksrare0Transactor) CancelAllOrdersForSender(opts *bind.TransactOpts, minNonce *big.Int) (*types.Transaction, error) {
	return _Looksrare0.contract.Transact(opts, "cancelAllOrdersForSender", minNonce)
}

// CancelAllOrdersForSender is a paid mutator transaction binding the contract method 0xcbd2ec65.
//
// Solidity: function cancelAllOrdersForSender(uint256 minNonce) returns()
func (_Looksrare0 *Looksrare0Session) CancelAllOrdersForSender(minNonce *big.Int) (*types.Transaction, error) {
	return _Looksrare0.Contract.CancelAllOrdersForSender(&_Looksrare0.TransactOpts, minNonce)
}

// CancelAllOrdersForSender is a paid mutator transaction binding the contract method 0xcbd2ec65.
//
// Solidity: function cancelAllOrdersForSender(uint256 minNonce) returns()
func (_Looksrare0 *Looksrare0TransactorSession) CancelAllOrdersForSender(minNonce *big.Int) (*types.Transaction, error) {
	return _Looksrare0.Contract.CancelAllOrdersForSender(&_Looksrare0.TransactOpts, minNonce)
}

// CancelMultipleMakerOrders is a paid mutator transaction binding the contract method 0x9e53a69a.
//
// Solidity: function cancelMultipleMakerOrders(uint256[] orderNonces) returns()
func (_Looksrare0 *Looksrare0Transactor) CancelMultipleMakerOrders(opts *bind.TransactOpts, orderNonces []*big.Int) (*types.Transaction, error) {
	return _Looksrare0.contract.Transact(opts, "cancelMultipleMakerOrders", orderNonces)
}

// CancelMultipleMakerOrders is a paid mutator transaction binding the contract method 0x9e53a69a.
//
// Solidity: function cancelMultipleMakerOrders(uint256[] orderNonces) returns()
func (_Looksrare0 *Looksrare0Session) CancelMultipleMakerOrders(orderNonces []*big.Int) (*types.Transaction, error) {
	return _Looksrare0.Contract.CancelMultipleMakerOrders(&_Looksrare0.TransactOpts, orderNonces)
}

// CancelMultipleMakerOrders is a paid mutator transaction binding the contract method 0x9e53a69a.
//
// Solidity: function cancelMultipleMakerOrders(uint256[] orderNonces) returns()
func (_Looksrare0 *Looksrare0TransactorSession) CancelMultipleMakerOrders(orderNonces []*big.Int) (*types.Transaction, error) {
	return _Looksrare0.Contract.CancelMultipleMakerOrders(&_Looksrare0.TransactOpts, orderNonces)
}

// MatchAskWithTakerBid is a paid mutator transaction binding the contract method 0x38e29209.
//
// Solidity: function matchAskWithTakerBid((bool,address,uint256,uint256,uint256,bytes) takerBid, (bool,address,address,uint256,uint256,uint256,address,address,uint256,uint256,uint256,uint256,bytes,uint8,bytes32,bytes32) makerAsk) returns()
func (_Looksrare0 *Looksrare0Transactor) MatchAskWithTakerBid(opts *bind.TransactOpts, takerBid OrderTypesTakerOrder, makerAsk OrderTypesMakerOrder) (*types.Transaction, error) {
	return _Looksrare0.contract.Transact(opts, "matchAskWithTakerBid", takerBid, makerAsk)
}

// MatchAskWithTakerBid is a paid mutator transaction binding the contract method 0x38e29209.
//
// Solidity: function matchAskWithTakerBid((bool,address,uint256,uint256,uint256,bytes) takerBid, (bool,address,address,uint256,uint256,uint256,address,address,uint256,uint256,uint256,uint256,bytes,uint8,bytes32,bytes32) makerAsk) returns()
func (_Looksrare0 *Looksrare0Session) MatchAskWithTakerBid(takerBid OrderTypesTakerOrder, makerAsk OrderTypesMakerOrder) (*types.Transaction, error) {
	return _Looksrare0.Contract.MatchAskWithTakerBid(&_Looksrare0.TransactOpts, takerBid, makerAsk)
}

// MatchAskWithTakerBid is a paid mutator transaction binding the contract method 0x38e29209.
//
// Solidity: function matchAskWithTakerBid((bool,address,uint256,uint256,uint256,bytes) takerBid, (bool,address,address,uint256,uint256,uint256,address,address,uint256,uint256,uint256,uint256,bytes,uint8,bytes32,bytes32) makerAsk) returns()
func (_Looksrare0 *Looksrare0TransactorSession) MatchAskWithTakerBid(takerBid OrderTypesTakerOrder, makerAsk OrderTypesMakerOrder) (*types.Transaction, error) {
	return _Looksrare0.Contract.MatchAskWithTakerBid(&_Looksrare0.TransactOpts, takerBid, makerAsk)
}

// MatchAskWithTakerBidUsingETHAndWETH is a paid mutator transaction binding the contract method 0xb4e4b296.
//
// Solidity: function matchAskWithTakerBidUsingETHAndWETH((bool,address,uint256,uint256,uint256,bytes) takerBid, (bool,address,address,uint256,uint256,uint256,address,address,uint256,uint256,uint256,uint256,bytes,uint8,bytes32,bytes32) makerAsk) payable returns()
func (_Looksrare0 *Looksrare0Transactor) MatchAskWithTakerBidUsingETHAndWETH(opts *bind.TransactOpts, takerBid OrderTypesTakerOrder, makerAsk OrderTypesMakerOrder) (*types.Transaction, error) {
	return _Looksrare0.contract.Transact(opts, "matchAskWithTakerBidUsingETHAndWETH", takerBid, makerAsk)
}

// MatchAskWithTakerBidUsingETHAndWETH is a paid mutator transaction binding the contract method 0xb4e4b296.
//
// Solidity: function matchAskWithTakerBidUsingETHAndWETH((bool,address,uint256,uint256,uint256,bytes) takerBid, (bool,address,address,uint256,uint256,uint256,address,address,uint256,uint256,uint256,uint256,bytes,uint8,bytes32,bytes32) makerAsk) payable returns()
func (_Looksrare0 *Looksrare0Session) MatchAskWithTakerBidUsingETHAndWETH(takerBid OrderTypesTakerOrder, makerAsk OrderTypesMakerOrder) (*types.Transaction, error) {
	return _Looksrare0.Contract.MatchAskWithTakerBidUsingETHAndWETH(&_Looksrare0.TransactOpts, takerBid, makerAsk)
}

// MatchAskWithTakerBidUsingETHAndWETH is a paid mutator transaction binding the contract method 0xb4e4b296.
//
// Solidity: function matchAskWithTakerBidUsingETHAndWETH((bool,address,uint256,uint256,uint256,bytes) takerBid, (bool,address,address,uint256,uint256,uint256,address,address,uint256,uint256,uint256,uint256,bytes,uint8,bytes32,bytes32) makerAsk) payable returns()
func (_Looksrare0 *Looksrare0TransactorSession) MatchAskWithTakerBidUsingETHAndWETH(takerBid OrderTypesTakerOrder, makerAsk OrderTypesMakerOrder) (*types.Transaction, error) {
	return _Looksrare0.Contract.MatchAskWithTakerBidUsingETHAndWETH(&_Looksrare0.TransactOpts, takerBid, makerAsk)
}

// MatchBidWithTakerAsk is a paid mutator transaction binding the contract method 0x3b6d032e.
//
// Solidity: function matchBidWithTakerAsk((bool,address,uint256,uint256,uint256,bytes) takerAsk, (bool,address,address,uint256,uint256,uint256,address,address,uint256,uint256,uint256,uint256,bytes,uint8,bytes32,bytes32) makerBid) returns()
func (_Looksrare0 *Looksrare0Transactor) MatchBidWithTakerAsk(opts *bind.TransactOpts, takerAsk OrderTypesTakerOrder, makerBid OrderTypesMakerOrder) (*types.Transaction, error) {
	return _Looksrare0.contract.Transact(opts, "matchBidWithTakerAsk", takerAsk, makerBid)
}

// MatchBidWithTakerAsk is a paid mutator transaction binding the contract method 0x3b6d032e.
//
// Solidity: function matchBidWithTakerAsk((bool,address,uint256,uint256,uint256,bytes) takerAsk, (bool,address,address,uint256,uint256,uint256,address,address,uint256,uint256,uint256,uint256,bytes,uint8,bytes32,bytes32) makerBid) returns()
func (_Looksrare0 *Looksrare0Session) MatchBidWithTakerAsk(takerAsk OrderTypesTakerOrder, makerBid OrderTypesMakerOrder) (*types.Transaction, error) {
	return _Looksrare0.Contract.MatchBidWithTakerAsk(&_Looksrare0.TransactOpts, takerAsk, makerBid)
}

// MatchBidWithTakerAsk is a paid mutator transaction binding the contract method 0x3b6d032e.
//
// Solidity: function matchBidWithTakerAsk((bool,address,uint256,uint256,uint256,bytes) takerAsk, (bool,address,address,uint256,uint256,uint256,address,address,uint256,uint256,uint256,uint256,bytes,uint8,bytes32,bytes32) makerBid) returns()
func (_Looksrare0 *Looksrare0TransactorSession) MatchBidWithTakerAsk(takerAsk OrderTypesTakerOrder, makerBid OrderTypesMakerOrder) (*types.Transaction, error) {
	return _Looksrare0.Contract.MatchBidWithTakerAsk(&_Looksrare0.TransactOpts, takerAsk, makerBid)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Looksrare0 *Looksrare0Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Looksrare0.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Looksrare0 *Looksrare0Session) RenounceOwnership() (*types.Transaction, error) {
	return _Looksrare0.Contract.RenounceOwnership(&_Looksrare0.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Looksrare0 *Looksrare0TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Looksrare0.Contract.RenounceOwnership(&_Looksrare0.TransactOpts)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Looksrare0 *Looksrare0Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Looksrare0.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Looksrare0 *Looksrare0Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Looksrare0.Contract.TransferOwnership(&_Looksrare0.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Looksrare0 *Looksrare0TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Looksrare0.Contract.TransferOwnership(&_Looksrare0.TransactOpts, newOwner)
}

// UpdateCurrencyManager is a paid mutator transaction binding the contract method 0x5ce052d7.
//
// Solidity: function updateCurrencyManager(address _currencyManager) returns()
func (_Looksrare0 *Looksrare0Transactor) UpdateCurrencyManager(opts *bind.TransactOpts, _currencyManager common.Address) (*types.Transaction, error) {
	return _Looksrare0.contract.Transact(opts, "updateCurrencyManager", _currencyManager)
}

// UpdateCurrencyManager is a paid mutator transaction binding the contract method 0x5ce052d7.
//
// Solidity: function updateCurrencyManager(address _currencyManager) returns()
func (_Looksrare0 *Looksrare0Session) UpdateCurrencyManager(_currencyManager common.Address) (*types.Transaction, error) {
	return _Looksrare0.Contract.UpdateCurrencyManager(&_Looksrare0.TransactOpts, _currencyManager)
}

// UpdateCurrencyManager is a paid mutator transaction binding the contract method 0x5ce052d7.
//
// Solidity: function updateCurrencyManager(address _currencyManager) returns()
func (_Looksrare0 *Looksrare0TransactorSession) UpdateCurrencyManager(_currencyManager common.Address) (*types.Transaction, error) {
	return _Looksrare0.Contract.UpdateCurrencyManager(&_Looksrare0.TransactOpts, _currencyManager)
}

// UpdateExecutionManager is a paid mutator transaction binding the contract method 0xd4ff41dc.
//
// Solidity: function updateExecutionManager(address _executionManager) returns()
func (_Looksrare0 *Looksrare0Transactor) UpdateExecutionManager(opts *bind.TransactOpts, _executionManager common.Address) (*types.Transaction, error) {
	return _Looksrare0.contract.Transact(opts, "updateExecutionManager", _executionManager)
}

// UpdateExecutionManager is a paid mutator transaction binding the contract method 0xd4ff41dc.
//
// Solidity: function updateExecutionManager(address _executionManager) returns()
func (_Looksrare0 *Looksrare0Session) UpdateExecutionManager(_executionManager common.Address) (*types.Transaction, error) {
	return _Looksrare0.Contract.UpdateExecutionManager(&_Looksrare0.TransactOpts, _executionManager)
}

// UpdateExecutionManager is a paid mutator transaction binding the contract method 0xd4ff41dc.
//
// Solidity: function updateExecutionManager(address _executionManager) returns()
func (_Looksrare0 *Looksrare0TransactorSession) UpdateExecutionManager(_executionManager common.Address) (*types.Transaction, error) {
	return _Looksrare0.Contract.UpdateExecutionManager(&_Looksrare0.TransactOpts, _executionManager)
}

// UpdateProtocolFeeRecipient is a paid mutator transaction binding the contract method 0x1df47f80.
//
// Solidity: function updateProtocolFeeRecipient(address _protocolFeeRecipient) returns()
func (_Looksrare0 *Looksrare0Transactor) UpdateProtocolFeeRecipient(opts *bind.TransactOpts, _protocolFeeRecipient common.Address) (*types.Transaction, error) {
	return _Looksrare0.contract.Transact(opts, "updateProtocolFeeRecipient", _protocolFeeRecipient)
}

// UpdateProtocolFeeRecipient is a paid mutator transaction binding the contract method 0x1df47f80.
//
// Solidity: function updateProtocolFeeRecipient(address _protocolFeeRecipient) returns()
func (_Looksrare0 *Looksrare0Session) UpdateProtocolFeeRecipient(_protocolFeeRecipient common.Address) (*types.Transaction, error) {
	return _Looksrare0.Contract.UpdateProtocolFeeRecipient(&_Looksrare0.TransactOpts, _protocolFeeRecipient)
}

// UpdateProtocolFeeRecipient is a paid mutator transaction binding the contract method 0x1df47f80.
//
// Solidity: function updateProtocolFeeRecipient(address _protocolFeeRecipient) returns()
func (_Looksrare0 *Looksrare0TransactorSession) UpdateProtocolFeeRecipient(_protocolFeeRecipient common.Address) (*types.Transaction, error) {
	return _Looksrare0.Contract.UpdateProtocolFeeRecipient(&_Looksrare0.TransactOpts, _protocolFeeRecipient)
}

// UpdateRoyaltyFeeManager is a paid mutator transaction binding the contract method 0xc5498769.
//
// Solidity: function updateRoyaltyFeeManager(address _royaltyFeeManager) returns()
func (_Looksrare0 *Looksrare0Transactor) UpdateRoyaltyFeeManager(opts *bind.TransactOpts, _royaltyFeeManager common.Address) (*types.Transaction, error) {
	return _Looksrare0.contract.Transact(opts, "updateRoyaltyFeeManager", _royaltyFeeManager)
}

// UpdateRoyaltyFeeManager is a paid mutator transaction binding the contract method 0xc5498769.
//
// Solidity: function updateRoyaltyFeeManager(address _royaltyFeeManager) returns()
func (_Looksrare0 *Looksrare0Session) UpdateRoyaltyFeeManager(_royaltyFeeManager common.Address) (*types.Transaction, error) {
	return _Looksrare0.Contract.UpdateRoyaltyFeeManager(&_Looksrare0.TransactOpts, _royaltyFeeManager)
}

// UpdateRoyaltyFeeManager is a paid mutator transaction binding the contract method 0xc5498769.
//
// Solidity: function updateRoyaltyFeeManager(address _royaltyFeeManager) returns()
func (_Looksrare0 *Looksrare0TransactorSession) UpdateRoyaltyFeeManager(_royaltyFeeManager common.Address) (*types.Transaction, error) {
	return _Looksrare0.Contract.UpdateRoyaltyFeeManager(&_Looksrare0.TransactOpts, _royaltyFeeManager)
}

// UpdateTransferSelectorNFT is a paid mutator transaction binding the contract method 0xf75ff53f.
//
// Solidity: function updateTransferSelectorNFT(address _transferSelectorNFT) returns()
func (_Looksrare0 *Looksrare0Transactor) UpdateTransferSelectorNFT(opts *bind.TransactOpts, _transferSelectorNFT common.Address) (*types.Transaction, error) {
	return _Looksrare0.contract.Transact(opts, "updateTransferSelectorNFT", _transferSelectorNFT)
}

// UpdateTransferSelectorNFT is a paid mutator transaction binding the contract method 0xf75ff53f.
//
// Solidity: function updateTransferSelectorNFT(address _transferSelectorNFT) returns()
func (_Looksrare0 *Looksrare0Session) UpdateTransferSelectorNFT(_transferSelectorNFT common.Address) (*types.Transaction, error) {
	return _Looksrare0.Contract.UpdateTransferSelectorNFT(&_Looksrare0.TransactOpts, _transferSelectorNFT)
}

// UpdateTransferSelectorNFT is a paid mutator transaction binding the contract method 0xf75ff53f.
//
// Solidity: function updateTransferSelectorNFT(address _transferSelectorNFT) returns()
func (_Looksrare0 *Looksrare0TransactorSession) UpdateTransferSelectorNFT(_transferSelectorNFT common.Address) (*types.Transaction, error) {
	return _Looksrare0.Contract.UpdateTransferSelectorNFT(&_Looksrare0.TransactOpts, _transferSelectorNFT)
}

// Looksrare0CancelAllOrdersIterator is returned from FilterCancelAllOrders and is used to iterate over the raw logs and unpacked data for CancelAllOrders events raised by the Looksrare0 contract.
type Looksrare0CancelAllOrdersIterator struct {
	Event *Looksrare0CancelAllOrders // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Looksrare0CancelAllOrdersIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Looksrare0CancelAllOrders)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Looksrare0CancelAllOrders)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Looksrare0CancelAllOrdersIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Looksrare0CancelAllOrdersIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Looksrare0CancelAllOrders represents a CancelAllOrders event raised by the Looksrare0 contract.
type Looksrare0CancelAllOrders struct {
	User        common.Address
	NewMinNonce *big.Int
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterCancelAllOrders is a free log retrieval operation binding the contract event 0x1e7178d84f0b0825c65795cd62e7972809ad3aac6917843aaec596161b2c0a97.
//
// Solidity: event CancelAllOrders(address indexed user, uint256 newMinNonce)
func (_Looksrare0 *Looksrare0Filterer) FilterCancelAllOrders(opts *bind.FilterOpts, user []common.Address) (*Looksrare0CancelAllOrdersIterator, error) {

	var userRule []interface{}
	for _, userItem := range user {
		userRule = append(userRule, userItem)
	}

	logs, sub, err := _Looksrare0.contract.FilterLogs(opts, "CancelAllOrders", userRule)
	if err != nil {
		return nil, err
	}
	return &Looksrare0CancelAllOrdersIterator{contract: _Looksrare0.contract, event: "CancelAllOrders", logs: logs, sub: sub}, nil
}

// WatchCancelAllOrders is a free log subscription operation binding the contract event 0x1e7178d84f0b0825c65795cd62e7972809ad3aac6917843aaec596161b2c0a97.
//
// Solidity: event CancelAllOrders(address indexed user, uint256 newMinNonce)
func (_Looksrare0 *Looksrare0Filterer) WatchCancelAllOrders(opts *bind.WatchOpts, sink chan<- *Looksrare0CancelAllOrders, user []common.Address) (event.Subscription, error) {

	var userRule []interface{}
	for _, userItem := range user {
		userRule = append(userRule, userItem)
	}

	logs, sub, err := _Looksrare0.contract.WatchLogs(opts, "CancelAllOrders", userRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Looksrare0CancelAllOrders)
				if err := _Looksrare0.contract.UnpackLog(event, "CancelAllOrders", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCancelAllOrders is a log parse operation binding the contract event 0x1e7178d84f0b0825c65795cd62e7972809ad3aac6917843aaec596161b2c0a97.
//
// Solidity: event CancelAllOrders(address indexed user, uint256 newMinNonce)
func (_Looksrare0 *Looksrare0Filterer) ParseCancelAllOrders(log types.Log) (*Looksrare0CancelAllOrders, error) {
	event := new(Looksrare0CancelAllOrders)
	if err := _Looksrare0.contract.UnpackLog(event, "CancelAllOrders", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Looksrare0CancelMultipleOrdersIterator is returned from FilterCancelMultipleOrders and is used to iterate over the raw logs and unpacked data for CancelMultipleOrders events raised by the Looksrare0 contract.
type Looksrare0CancelMultipleOrdersIterator struct {
	Event *Looksrare0CancelMultipleOrders // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Looksrare0CancelMultipleOrdersIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Looksrare0CancelMultipleOrders)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Looksrare0CancelMultipleOrders)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Looksrare0CancelMultipleOrdersIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Looksrare0CancelMultipleOrdersIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Looksrare0CancelMultipleOrders represents a CancelMultipleOrders event raised by the Looksrare0 contract.
type Looksrare0CancelMultipleOrders struct {
	User        common.Address
	OrderNonces []*big.Int
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterCancelMultipleOrders is a free log retrieval operation binding the contract event 0xfa0ae5d80fe3763c880a3839fab0294171a6f730d1f82c4cd5392c6f67b41732.
//
// Solidity: event CancelMultipleOrders(address indexed user, uint256[] orderNonces)
func (_Looksrare0 *Looksrare0Filterer) FilterCancelMultipleOrders(opts *bind.FilterOpts, user []common.Address) (*Looksrare0CancelMultipleOrdersIterator, error) {

	var userRule []interface{}
	for _, userItem := range user {
		userRule = append(userRule, userItem)
	}

	logs, sub, err := _Looksrare0.contract.FilterLogs(opts, "CancelMultipleOrders", userRule)
	if err != nil {
		return nil, err
	}
	return &Looksrare0CancelMultipleOrdersIterator{contract: _Looksrare0.contract, event: "CancelMultipleOrders", logs: logs, sub: sub}, nil
}

// WatchCancelMultipleOrders is a free log subscription operation binding the contract event 0xfa0ae5d80fe3763c880a3839fab0294171a6f730d1f82c4cd5392c6f67b41732.
//
// Solidity: event CancelMultipleOrders(address indexed user, uint256[] orderNonces)
func (_Looksrare0 *Looksrare0Filterer) WatchCancelMultipleOrders(opts *bind.WatchOpts, sink chan<- *Looksrare0CancelMultipleOrders, user []common.Address) (event.Subscription, error) {

	var userRule []interface{}
	for _, userItem := range user {
		userRule = append(userRule, userItem)
	}

	logs, sub, err := _Looksrare0.contract.WatchLogs(opts, "CancelMultipleOrders", userRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Looksrare0CancelMultipleOrders)
				if err := _Looksrare0.contract.UnpackLog(event, "CancelMultipleOrders", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCancelMultipleOrders is a log parse operation binding the contract event 0xfa0ae5d80fe3763c880a3839fab0294171a6f730d1f82c4cd5392c6f67b41732.
//
// Solidity: event CancelMultipleOrders(address indexed user, uint256[] orderNonces)
func (_Looksrare0 *Looksrare0Filterer) ParseCancelMultipleOrders(log types.Log) (*Looksrare0CancelMultipleOrders, error) {
	event := new(Looksrare0CancelMultipleOrders)
	if err := _Looksrare0.contract.UnpackLog(event, "CancelMultipleOrders", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Looksrare0NewCurrencyManagerIterator is returned from FilterNewCurrencyManager and is used to iterate over the raw logs and unpacked data for NewCurrencyManager events raised by the Looksrare0 contract.
type Looksrare0NewCurrencyManagerIterator struct {
	Event *Looksrare0NewCurrencyManager // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Looksrare0NewCurrencyManagerIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Looksrare0NewCurrencyManager)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Looksrare0NewCurrencyManager)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Looksrare0NewCurrencyManagerIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Looksrare0NewCurrencyManagerIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Looksrare0NewCurrencyManager represents a NewCurrencyManager event raised by the Looksrare0 contract.
type Looksrare0NewCurrencyManager struct {
	CurrencyManager common.Address
	Raw             types.Log // Blockchain specific contextual infos
}

// FilterNewCurrencyManager is a free log retrieval operation binding the contract event 0xb4f5db40df3aced29e88a4babbc3b46e305e07d34098525d18b1497056e63838.
//
// Solidity: event NewCurrencyManager(address indexed currencyManager)
func (_Looksrare0 *Looksrare0Filterer) FilterNewCurrencyManager(opts *bind.FilterOpts, currencyManager []common.Address) (*Looksrare0NewCurrencyManagerIterator, error) {

	var currencyManagerRule []interface{}
	for _, currencyManagerItem := range currencyManager {
		currencyManagerRule = append(currencyManagerRule, currencyManagerItem)
	}

	logs, sub, err := _Looksrare0.contract.FilterLogs(opts, "NewCurrencyManager", currencyManagerRule)
	if err != nil {
		return nil, err
	}
	return &Looksrare0NewCurrencyManagerIterator{contract: _Looksrare0.contract, event: "NewCurrencyManager", logs: logs, sub: sub}, nil
}

// WatchNewCurrencyManager is a free log subscription operation binding the contract event 0xb4f5db40df3aced29e88a4babbc3b46e305e07d34098525d18b1497056e63838.
//
// Solidity: event NewCurrencyManager(address indexed currencyManager)
func (_Looksrare0 *Looksrare0Filterer) WatchNewCurrencyManager(opts *bind.WatchOpts, sink chan<- *Looksrare0NewCurrencyManager, currencyManager []common.Address) (event.Subscription, error) {

	var currencyManagerRule []interface{}
	for _, currencyManagerItem := range currencyManager {
		currencyManagerRule = append(currencyManagerRule, currencyManagerItem)
	}

	logs, sub, err := _Looksrare0.contract.WatchLogs(opts, "NewCurrencyManager", currencyManagerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Looksrare0NewCurrencyManager)
				if err := _Looksrare0.contract.UnpackLog(event, "NewCurrencyManager", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewCurrencyManager is a log parse operation binding the contract event 0xb4f5db40df3aced29e88a4babbc3b46e305e07d34098525d18b1497056e63838.
//
// Solidity: event NewCurrencyManager(address indexed currencyManager)
func (_Looksrare0 *Looksrare0Filterer) ParseNewCurrencyManager(log types.Log) (*Looksrare0NewCurrencyManager, error) {
	event := new(Looksrare0NewCurrencyManager)
	if err := _Looksrare0.contract.UnpackLog(event, "NewCurrencyManager", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Looksrare0NewExecutionManagerIterator is returned from FilterNewExecutionManager and is used to iterate over the raw logs and unpacked data for NewExecutionManager events raised by the Looksrare0 contract.
type Looksrare0NewExecutionManagerIterator struct {
	Event *Looksrare0NewExecutionManager // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Looksrare0NewExecutionManagerIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Looksrare0NewExecutionManager)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Looksrare0NewExecutionManager)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Looksrare0NewExecutionManagerIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Looksrare0NewExecutionManagerIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Looksrare0NewExecutionManager represents a NewExecutionManager event raised by the Looksrare0 contract.
type Looksrare0NewExecutionManager struct {
	ExecutionManager common.Address
	Raw              types.Log // Blockchain specific contextual infos
}

// FilterNewExecutionManager is a free log retrieval operation binding the contract event 0x36e2a376eabc3bc60cb88f29c288f53e36874a95a7f407330ab4f166b0905698.
//
// Solidity: event NewExecutionManager(address indexed executionManager)
func (_Looksrare0 *Looksrare0Filterer) FilterNewExecutionManager(opts *bind.FilterOpts, executionManager []common.Address) (*Looksrare0NewExecutionManagerIterator, error) {

	var executionManagerRule []interface{}
	for _, executionManagerItem := range executionManager {
		executionManagerRule = append(executionManagerRule, executionManagerItem)
	}

	logs, sub, err := _Looksrare0.contract.FilterLogs(opts, "NewExecutionManager", executionManagerRule)
	if err != nil {
		return nil, err
	}
	return &Looksrare0NewExecutionManagerIterator{contract: _Looksrare0.contract, event: "NewExecutionManager", logs: logs, sub: sub}, nil
}

// WatchNewExecutionManager is a free log subscription operation binding the contract event 0x36e2a376eabc3bc60cb88f29c288f53e36874a95a7f407330ab4f166b0905698.
//
// Solidity: event NewExecutionManager(address indexed executionManager)
func (_Looksrare0 *Looksrare0Filterer) WatchNewExecutionManager(opts *bind.WatchOpts, sink chan<- *Looksrare0NewExecutionManager, executionManager []common.Address) (event.Subscription, error) {

	var executionManagerRule []interface{}
	for _, executionManagerItem := range executionManager {
		executionManagerRule = append(executionManagerRule, executionManagerItem)
	}

	logs, sub, err := _Looksrare0.contract.WatchLogs(opts, "NewExecutionManager", executionManagerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Looksrare0NewExecutionManager)
				if err := _Looksrare0.contract.UnpackLog(event, "NewExecutionManager", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewExecutionManager is a log parse operation binding the contract event 0x36e2a376eabc3bc60cb88f29c288f53e36874a95a7f407330ab4f166b0905698.
//
// Solidity: event NewExecutionManager(address indexed executionManager)
func (_Looksrare0 *Looksrare0Filterer) ParseNewExecutionManager(log types.Log) (*Looksrare0NewExecutionManager, error) {
	event := new(Looksrare0NewExecutionManager)
	if err := _Looksrare0.contract.UnpackLog(event, "NewExecutionManager", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Looksrare0NewProtocolFeeRecipientIterator is returned from FilterNewProtocolFeeRecipient and is used to iterate over the raw logs and unpacked data for NewProtocolFeeRecipient events raised by the Looksrare0 contract.
type Looksrare0NewProtocolFeeRecipientIterator struct {
	Event *Looksrare0NewProtocolFeeRecipient // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Looksrare0NewProtocolFeeRecipientIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Looksrare0NewProtocolFeeRecipient)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Looksrare0NewProtocolFeeRecipient)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Looksrare0NewProtocolFeeRecipientIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Looksrare0NewProtocolFeeRecipientIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Looksrare0NewProtocolFeeRecipient represents a NewProtocolFeeRecipient event raised by the Looksrare0 contract.
type Looksrare0NewProtocolFeeRecipient struct {
	ProtocolFeeRecipient common.Address
	Raw                  types.Log // Blockchain specific contextual infos
}

// FilterNewProtocolFeeRecipient is a free log retrieval operation binding the contract event 0x8cffb07faa2874440346743bdc0a86b06c3335cc47dc49b327d10e77b73ceb10.
//
// Solidity: event NewProtocolFeeRecipient(address indexed protocolFeeRecipient)
func (_Looksrare0 *Looksrare0Filterer) FilterNewProtocolFeeRecipient(opts *bind.FilterOpts, protocolFeeRecipient []common.Address) (*Looksrare0NewProtocolFeeRecipientIterator, error) {

	var protocolFeeRecipientRule []interface{}
	for _, protocolFeeRecipientItem := range protocolFeeRecipient {
		protocolFeeRecipientRule = append(protocolFeeRecipientRule, protocolFeeRecipientItem)
	}

	logs, sub, err := _Looksrare0.contract.FilterLogs(opts, "NewProtocolFeeRecipient", protocolFeeRecipientRule)
	if err != nil {
		return nil, err
	}
	return &Looksrare0NewProtocolFeeRecipientIterator{contract: _Looksrare0.contract, event: "NewProtocolFeeRecipient", logs: logs, sub: sub}, nil
}

// WatchNewProtocolFeeRecipient is a free log subscription operation binding the contract event 0x8cffb07faa2874440346743bdc0a86b06c3335cc47dc49b327d10e77b73ceb10.
//
// Solidity: event NewProtocolFeeRecipient(address indexed protocolFeeRecipient)
func (_Looksrare0 *Looksrare0Filterer) WatchNewProtocolFeeRecipient(opts *bind.WatchOpts, sink chan<- *Looksrare0NewProtocolFeeRecipient, protocolFeeRecipient []common.Address) (event.Subscription, error) {

	var protocolFeeRecipientRule []interface{}
	for _, protocolFeeRecipientItem := range protocolFeeRecipient {
		protocolFeeRecipientRule = append(protocolFeeRecipientRule, protocolFeeRecipientItem)
	}

	logs, sub, err := _Looksrare0.contract.WatchLogs(opts, "NewProtocolFeeRecipient", protocolFeeRecipientRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Looksrare0NewProtocolFeeRecipient)
				if err := _Looksrare0.contract.UnpackLog(event, "NewProtocolFeeRecipient", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewProtocolFeeRecipient is a log parse operation binding the contract event 0x8cffb07faa2874440346743bdc0a86b06c3335cc47dc49b327d10e77b73ceb10.
//
// Solidity: event NewProtocolFeeRecipient(address indexed protocolFeeRecipient)
func (_Looksrare0 *Looksrare0Filterer) ParseNewProtocolFeeRecipient(log types.Log) (*Looksrare0NewProtocolFeeRecipient, error) {
	event := new(Looksrare0NewProtocolFeeRecipient)
	if err := _Looksrare0.contract.UnpackLog(event, "NewProtocolFeeRecipient", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Looksrare0NewRoyaltyFeeManagerIterator is returned from FilterNewRoyaltyFeeManager and is used to iterate over the raw logs and unpacked data for NewRoyaltyFeeManager events raised by the Looksrare0 contract.
type Looksrare0NewRoyaltyFeeManagerIterator struct {
	Event *Looksrare0NewRoyaltyFeeManager // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Looksrare0NewRoyaltyFeeManagerIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Looksrare0NewRoyaltyFeeManager)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Looksrare0NewRoyaltyFeeManager)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Looksrare0NewRoyaltyFeeManagerIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Looksrare0NewRoyaltyFeeManagerIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Looksrare0NewRoyaltyFeeManager represents a NewRoyaltyFeeManager event raised by the Looksrare0 contract.
type Looksrare0NewRoyaltyFeeManager struct {
	RoyaltyFeeManager common.Address
	Raw               types.Log // Blockchain specific contextual infos
}

// FilterNewRoyaltyFeeManager is a free log retrieval operation binding the contract event 0x80e3874461ebbd918ac3e81da0a92e5e51387d70f337237c9123e48d20e5a508.
//
// Solidity: event NewRoyaltyFeeManager(address indexed royaltyFeeManager)
func (_Looksrare0 *Looksrare0Filterer) FilterNewRoyaltyFeeManager(opts *bind.FilterOpts, royaltyFeeManager []common.Address) (*Looksrare0NewRoyaltyFeeManagerIterator, error) {

	var royaltyFeeManagerRule []interface{}
	for _, royaltyFeeManagerItem := range royaltyFeeManager {
		royaltyFeeManagerRule = append(royaltyFeeManagerRule, royaltyFeeManagerItem)
	}

	logs, sub, err := _Looksrare0.contract.FilterLogs(opts, "NewRoyaltyFeeManager", royaltyFeeManagerRule)
	if err != nil {
		return nil, err
	}
	return &Looksrare0NewRoyaltyFeeManagerIterator{contract: _Looksrare0.contract, event: "NewRoyaltyFeeManager", logs: logs, sub: sub}, nil
}

// WatchNewRoyaltyFeeManager is a free log subscription operation binding the contract event 0x80e3874461ebbd918ac3e81da0a92e5e51387d70f337237c9123e48d20e5a508.
//
// Solidity: event NewRoyaltyFeeManager(address indexed royaltyFeeManager)
func (_Looksrare0 *Looksrare0Filterer) WatchNewRoyaltyFeeManager(opts *bind.WatchOpts, sink chan<- *Looksrare0NewRoyaltyFeeManager, royaltyFeeManager []common.Address) (event.Subscription, error) {

	var royaltyFeeManagerRule []interface{}
	for _, royaltyFeeManagerItem := range royaltyFeeManager {
		royaltyFeeManagerRule = append(royaltyFeeManagerRule, royaltyFeeManagerItem)
	}

	logs, sub, err := _Looksrare0.contract.WatchLogs(opts, "NewRoyaltyFeeManager", royaltyFeeManagerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Looksrare0NewRoyaltyFeeManager)
				if err := _Looksrare0.contract.UnpackLog(event, "NewRoyaltyFeeManager", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewRoyaltyFeeManager is a log parse operation binding the contract event 0x80e3874461ebbd918ac3e81da0a92e5e51387d70f337237c9123e48d20e5a508.
//
// Solidity: event NewRoyaltyFeeManager(address indexed royaltyFeeManager)
func (_Looksrare0 *Looksrare0Filterer) ParseNewRoyaltyFeeManager(log types.Log) (*Looksrare0NewRoyaltyFeeManager, error) {
	event := new(Looksrare0NewRoyaltyFeeManager)
	if err := _Looksrare0.contract.UnpackLog(event, "NewRoyaltyFeeManager", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Looksrare0NewTransferSelectorNFTIterator is returned from FilterNewTransferSelectorNFT and is used to iterate over the raw logs and unpacked data for NewTransferSelectorNFT events raised by the Looksrare0 contract.
type Looksrare0NewTransferSelectorNFTIterator struct {
	Event *Looksrare0NewTransferSelectorNFT // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Looksrare0NewTransferSelectorNFTIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Looksrare0NewTransferSelectorNFT)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Looksrare0NewTransferSelectorNFT)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Looksrare0NewTransferSelectorNFTIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Looksrare0NewTransferSelectorNFTIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Looksrare0NewTransferSelectorNFT represents a NewTransferSelectorNFT event raised by the Looksrare0 contract.
type Looksrare0NewTransferSelectorNFT struct {
	TransferSelectorNFT common.Address
	Raw                 types.Log // Blockchain specific contextual infos
}

// FilterNewTransferSelectorNFT is a free log retrieval operation binding the contract event 0x205d78ab41afe80bd6b6aaa5d7599d5300ff8690da3ab1302c1b552f7baf7d8c.
//
// Solidity: event NewTransferSelectorNFT(address indexed transferSelectorNFT)
func (_Looksrare0 *Looksrare0Filterer) FilterNewTransferSelectorNFT(opts *bind.FilterOpts, transferSelectorNFT []common.Address) (*Looksrare0NewTransferSelectorNFTIterator, error) {

	var transferSelectorNFTRule []interface{}
	for _, transferSelectorNFTItem := range transferSelectorNFT {
		transferSelectorNFTRule = append(transferSelectorNFTRule, transferSelectorNFTItem)
	}

	logs, sub, err := _Looksrare0.contract.FilterLogs(opts, "NewTransferSelectorNFT", transferSelectorNFTRule)
	if err != nil {
		return nil, err
	}
	return &Looksrare0NewTransferSelectorNFTIterator{contract: _Looksrare0.contract, event: "NewTransferSelectorNFT", logs: logs, sub: sub}, nil
}

// WatchNewTransferSelectorNFT is a free log subscription operation binding the contract event 0x205d78ab41afe80bd6b6aaa5d7599d5300ff8690da3ab1302c1b552f7baf7d8c.
//
// Solidity: event NewTransferSelectorNFT(address indexed transferSelectorNFT)
func (_Looksrare0 *Looksrare0Filterer) WatchNewTransferSelectorNFT(opts *bind.WatchOpts, sink chan<- *Looksrare0NewTransferSelectorNFT, transferSelectorNFT []common.Address) (event.Subscription, error) {

	var transferSelectorNFTRule []interface{}
	for _, transferSelectorNFTItem := range transferSelectorNFT {
		transferSelectorNFTRule = append(transferSelectorNFTRule, transferSelectorNFTItem)
	}

	logs, sub, err := _Looksrare0.contract.WatchLogs(opts, "NewTransferSelectorNFT", transferSelectorNFTRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Looksrare0NewTransferSelectorNFT)
				if err := _Looksrare0.contract.UnpackLog(event, "NewTransferSelectorNFT", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewTransferSelectorNFT is a log parse operation binding the contract event 0x205d78ab41afe80bd6b6aaa5d7599d5300ff8690da3ab1302c1b552f7baf7d8c.
//
// Solidity: event NewTransferSelectorNFT(address indexed transferSelectorNFT)
func (_Looksrare0 *Looksrare0Filterer) ParseNewTransferSelectorNFT(log types.Log) (*Looksrare0NewTransferSelectorNFT, error) {
	event := new(Looksrare0NewTransferSelectorNFT)
	if err := _Looksrare0.contract.UnpackLog(event, "NewTransferSelectorNFT", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Looksrare0OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Looksrare0 contract.
type Looksrare0OwnershipTransferredIterator struct {
	Event *Looksrare0OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Looksrare0OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Looksrare0OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Looksrare0OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Looksrare0OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Looksrare0OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Looksrare0OwnershipTransferred represents a OwnershipTransferred event raised by the Looksrare0 contract.
type Looksrare0OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Looksrare0 *Looksrare0Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Looksrare0OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Looksrare0.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Looksrare0OwnershipTransferredIterator{contract: _Looksrare0.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Looksrare0 *Looksrare0Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Looksrare0OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Looksrare0.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Looksrare0OwnershipTransferred)
				if err := _Looksrare0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Looksrare0 *Looksrare0Filterer) ParseOwnershipTransferred(log types.Log) (*Looksrare0OwnershipTransferred, error) {
	event := new(Looksrare0OwnershipTransferred)
	if err := _Looksrare0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Looksrare0RoyaltyPaymentIterator is returned from FilterRoyaltyPayment and is used to iterate over the raw logs and unpacked data for RoyaltyPayment events raised by the Looksrare0 contract.
type Looksrare0RoyaltyPaymentIterator struct {
	Event *Looksrare0RoyaltyPayment // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Looksrare0RoyaltyPaymentIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Looksrare0RoyaltyPayment)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Looksrare0RoyaltyPayment)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Looksrare0RoyaltyPaymentIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Looksrare0RoyaltyPaymentIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Looksrare0RoyaltyPayment represents a RoyaltyPayment event raised by the Looksrare0 contract.
type Looksrare0RoyaltyPayment struct {
	Collection       common.Address
	TokenId          *big.Int
	RoyaltyRecipient common.Address
	Currency         common.Address
	Amount           *big.Int
	Raw              types.Log // Blockchain specific contextual infos
}

// FilterRoyaltyPayment is a free log retrieval operation binding the contract event 0x27c4f0403323142b599832f26acd21c74a9e5b809f2215726e244a4ac588cd7d.
//
// Solidity: event RoyaltyPayment(address indexed collection, uint256 indexed tokenId, address indexed royaltyRecipient, address currency, uint256 amount)
func (_Looksrare0 *Looksrare0Filterer) FilterRoyaltyPayment(opts *bind.FilterOpts, collection []common.Address, tokenId []*big.Int, royaltyRecipient []common.Address) (*Looksrare0RoyaltyPaymentIterator, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}
	var royaltyRecipientRule []interface{}
	for _, royaltyRecipientItem := range royaltyRecipient {
		royaltyRecipientRule = append(royaltyRecipientRule, royaltyRecipientItem)
	}

	logs, sub, err := _Looksrare0.contract.FilterLogs(opts, "RoyaltyPayment", collectionRule, tokenIdRule, royaltyRecipientRule)
	if err != nil {
		return nil, err
	}
	return &Looksrare0RoyaltyPaymentIterator{contract: _Looksrare0.contract, event: "RoyaltyPayment", logs: logs, sub: sub}, nil
}

// WatchRoyaltyPayment is a free log subscription operation binding the contract event 0x27c4f0403323142b599832f26acd21c74a9e5b809f2215726e244a4ac588cd7d.
//
// Solidity: event RoyaltyPayment(address indexed collection, uint256 indexed tokenId, address indexed royaltyRecipient, address currency, uint256 amount)
func (_Looksrare0 *Looksrare0Filterer) WatchRoyaltyPayment(opts *bind.WatchOpts, sink chan<- *Looksrare0RoyaltyPayment, collection []common.Address, tokenId []*big.Int, royaltyRecipient []common.Address) (event.Subscription, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}
	var royaltyRecipientRule []interface{}
	for _, royaltyRecipientItem := range royaltyRecipient {
		royaltyRecipientRule = append(royaltyRecipientRule, royaltyRecipientItem)
	}

	logs, sub, err := _Looksrare0.contract.WatchLogs(opts, "RoyaltyPayment", collectionRule, tokenIdRule, royaltyRecipientRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Looksrare0RoyaltyPayment)
				if err := _Looksrare0.contract.UnpackLog(event, "RoyaltyPayment", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseRoyaltyPayment is a log parse operation binding the contract event 0x27c4f0403323142b599832f26acd21c74a9e5b809f2215726e244a4ac588cd7d.
//
// Solidity: event RoyaltyPayment(address indexed collection, uint256 indexed tokenId, address indexed royaltyRecipient, address currency, uint256 amount)
func (_Looksrare0 *Looksrare0Filterer) ParseRoyaltyPayment(log types.Log) (*Looksrare0RoyaltyPayment, error) {
	event := new(Looksrare0RoyaltyPayment)
	if err := _Looksrare0.contract.UnpackLog(event, "RoyaltyPayment", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Looksrare0TakerAskIterator is returned from FilterTakerAsk and is used to iterate over the raw logs and unpacked data for TakerAsk events raised by the Looksrare0 contract.
type Looksrare0TakerAskIterator struct {
	Event *Looksrare0TakerAsk // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Looksrare0TakerAskIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Looksrare0TakerAsk)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Looksrare0TakerAsk)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Looksrare0TakerAskIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Looksrare0TakerAskIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Looksrare0TakerAsk represents a TakerAsk event raised by the Looksrare0 contract.
type Looksrare0TakerAsk struct {
	OrderHash  [32]byte
	OrderNonce *big.Int
	Taker      common.Address
	Maker      common.Address
	Strategy   common.Address
	Currency   common.Address
	Collection common.Address
	TokenId    *big.Int
	Amount     *big.Int
	Price      *big.Int
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterTakerAsk is a free log retrieval operation binding the contract event 0x68cd251d4d267c6e2034ff0088b990352b97b2002c0476587d0c4da889c11330.
//
// Solidity: event TakerAsk(bytes32 orderHash, uint256 orderNonce, address indexed taker, address indexed maker, address indexed strategy, address currency, address collection, uint256 tokenId, uint256 amount, uint256 price)
func (_Looksrare0 *Looksrare0Filterer) FilterTakerAsk(opts *bind.FilterOpts, taker []common.Address, maker []common.Address, strategy []common.Address) (*Looksrare0TakerAskIterator, error) {

	var takerRule []interface{}
	for _, takerItem := range taker {
		takerRule = append(takerRule, takerItem)
	}
	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}
	var strategyRule []interface{}
	for _, strategyItem := range strategy {
		strategyRule = append(strategyRule, strategyItem)
	}

	logs, sub, err := _Looksrare0.contract.FilterLogs(opts, "TakerAsk", takerRule, makerRule, strategyRule)
	if err != nil {
		return nil, err
	}
	return &Looksrare0TakerAskIterator{contract: _Looksrare0.contract, event: "TakerAsk", logs: logs, sub: sub}, nil
}

// WatchTakerAsk is a free log subscription operation binding the contract event 0x68cd251d4d267c6e2034ff0088b990352b97b2002c0476587d0c4da889c11330.
//
// Solidity: event TakerAsk(bytes32 orderHash, uint256 orderNonce, address indexed taker, address indexed maker, address indexed strategy, address currency, address collection, uint256 tokenId, uint256 amount, uint256 price)
func (_Looksrare0 *Looksrare0Filterer) WatchTakerAsk(opts *bind.WatchOpts, sink chan<- *Looksrare0TakerAsk, taker []common.Address, maker []common.Address, strategy []common.Address) (event.Subscription, error) {

	var takerRule []interface{}
	for _, takerItem := range taker {
		takerRule = append(takerRule, takerItem)
	}
	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}
	var strategyRule []interface{}
	for _, strategyItem := range strategy {
		strategyRule = append(strategyRule, strategyItem)
	}

	logs, sub, err := _Looksrare0.contract.WatchLogs(opts, "TakerAsk", takerRule, makerRule, strategyRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Looksrare0TakerAsk)
				if err := _Looksrare0.contract.UnpackLog(event, "TakerAsk", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTakerAsk is a log parse operation binding the contract event 0x68cd251d4d267c6e2034ff0088b990352b97b2002c0476587d0c4da889c11330.
//
// Solidity: event TakerAsk(bytes32 orderHash, uint256 orderNonce, address indexed taker, address indexed maker, address indexed strategy, address currency, address collection, uint256 tokenId, uint256 amount, uint256 price)
func (_Looksrare0 *Looksrare0Filterer) ParseTakerAsk(log types.Log) (*Looksrare0TakerAsk, error) {
	event := new(Looksrare0TakerAsk)
	if err := _Looksrare0.contract.UnpackLog(event, "TakerAsk", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Looksrare0TakerBidIterator is returned from FilterTakerBid and is used to iterate over the raw logs and unpacked data for TakerBid events raised by the Looksrare0 contract.
type Looksrare0TakerBidIterator struct {
	Event *Looksrare0TakerBid // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Looksrare0TakerBidIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Looksrare0TakerBid)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Looksrare0TakerBid)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Looksrare0TakerBidIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Looksrare0TakerBidIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Looksrare0TakerBid represents a TakerBid event raised by the Looksrare0 contract.
type Looksrare0TakerBid struct {
	OrderHash  [32]byte
	OrderNonce *big.Int
	Taker      common.Address
	Maker      common.Address
	Strategy   common.Address
	Currency   common.Address
	Collection common.Address
	TokenId    *big.Int
	Amount     *big.Int
	Price      *big.Int
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterTakerBid is a free log retrieval operation binding the contract event 0x95fb6205e23ff6bda16a2d1dba56b9ad7c783f67c96fa149785052f47696f2be.
//
// Solidity: event TakerBid(bytes32 orderHash, uint256 orderNonce, address indexed taker, address indexed maker, address indexed strategy, address currency, address collection, uint256 tokenId, uint256 amount, uint256 price)
func (_Looksrare0 *Looksrare0Filterer) FilterTakerBid(opts *bind.FilterOpts, taker []common.Address, maker []common.Address, strategy []common.Address) (*Looksrare0TakerBidIterator, error) {

	var takerRule []interface{}
	for _, takerItem := range taker {
		takerRule = append(takerRule, takerItem)
	}
	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}
	var strategyRule []interface{}
	for _, strategyItem := range strategy {
		strategyRule = append(strategyRule, strategyItem)
	}

	logs, sub, err := _Looksrare0.contract.FilterLogs(opts, "TakerBid", takerRule, makerRule, strategyRule)
	if err != nil {
		return nil, err
	}
	return &Looksrare0TakerBidIterator{contract: _Looksrare0.contract, event: "TakerBid", logs: logs, sub: sub}, nil
}

// WatchTakerBid is a free log subscription operation binding the contract event 0x95fb6205e23ff6bda16a2d1dba56b9ad7c783f67c96fa149785052f47696f2be.
//
// Solidity: event TakerBid(bytes32 orderHash, uint256 orderNonce, address indexed taker, address indexed maker, address indexed strategy, address currency, address collection, uint256 tokenId, uint256 amount, uint256 price)
func (_Looksrare0 *Looksrare0Filterer) WatchTakerBid(opts *bind.WatchOpts, sink chan<- *Looksrare0TakerBid, taker []common.Address, maker []common.Address, strategy []common.Address) (event.Subscription, error) {

	var takerRule []interface{}
	for _, takerItem := range taker {
		takerRule = append(takerRule, takerItem)
	}
	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}
	var strategyRule []interface{}
	for _, strategyItem := range strategy {
		strategyRule = append(strategyRule, strategyItem)
	}

	logs, sub, err := _Looksrare0.contract.WatchLogs(opts, "TakerBid", takerRule, makerRule, strategyRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Looksrare0TakerBid)
				if err := _Looksrare0.contract.UnpackLog(event, "TakerBid", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTakerBid is a log parse operation binding the contract event 0x95fb6205e23ff6bda16a2d1dba56b9ad7c783f67c96fa149785052f47696f2be.
//
// Solidity: event TakerBid(bytes32 orderHash, uint256 orderNonce, address indexed taker, address indexed maker, address indexed strategy, address currency, address collection, uint256 tokenId, uint256 amount, uint256 price)
func (_Looksrare0 *Looksrare0Filterer) ParseTakerBid(log types.Log) (*Looksrare0TakerBid, error) {
	event := new(Looksrare0TakerBid)
	if err := _Looksrare0.contract.UnpackLog(event, "TakerBid", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
