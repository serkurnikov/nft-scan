// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package cryptopunks_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Cryptopunks0MetaData contains all meta data concerning the Cryptopunks0 contract.
var Cryptopunks0MetaData = &bind.MetaData{
	ABI: "[{\"constant\":true,\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"punksOfferedForSale\",\"outputs\":[{\"name\":\"isForSale\",\"type\":\"bool\"},{\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"name\":\"seller\",\"type\":\"address\"},{\"name\":\"minValue\",\"type\":\"uint256\"},{\"name\":\"onlySellTo\",\"type\":\"address\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"enterBidForPunk\",\"outputs\":[],\"payable\":true,\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"name\":\"minPrice\",\"type\":\"uint256\"}],\"name\":\"acceptBidForPunk\",\"outputs\":[],\"payable\":false,\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"decimals\",\"outputs\":[{\"name\":\"\",\"type\":\"uint8\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"addresses\",\"type\":\"address[]\"},{\"name\":\"indices\",\"type\":\"uint256[]\"}],\"name\":\"setInitialOwners\",\"outputs\":[],\"payable\":false,\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"withdraw\",\"outputs\":[],\"payable\":false,\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"imageHash\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"nextPunkIndexToAssign\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"punkIndexToAddress\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"standard\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"punkBids\",\"outputs\":[{\"name\":\"hasBid\",\"type\":\"bool\"},{\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"name\":\"bidder\",\"type\":\"address\"},{\"name\":\"value\",\"type\":\"uint256\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"allInitialOwnersAssigned\",\"outputs\":[],\"payable\":false,\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"allPunksAssigned\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"buyPunk\",\"outputs\":[],\"payable\":true,\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"to\",\"type\":\"address\"},{\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"transferPunk\",\"outputs\":[],\"payable\":false,\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"withdrawBidForPunk\",\"outputs\":[],\"payable\":false,\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"to\",\"type\":\"address\"},{\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"setInitialOwner\",\"outputs\":[],\"payable\":false,\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"name\":\"minSalePriceInWei\",\"type\":\"uint256\"},{\"name\":\"toAddress\",\"type\":\"address\"}],\"name\":\"offerPunkForSaleToAddress\",\"outputs\":[],\"payable\":false,\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"punksRemainingToAssign\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"name\":\"minSalePriceInWei\",\"type\":\"uint256\"}],\"name\":\"offerPunkForSale\",\"outputs\":[],\"payable\":false,\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"getPunk\",\"outputs\":[],\"payable\":false,\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"\",\"type\":\"address\"}],\"name\":\"pendingWithdrawals\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"punkNoLongerForSale\",\"outputs\":[],\"payable\":false,\"type\":\"function\"},{\"inputs\":[],\"payable\":true,\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"Assign\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"PunkTransfer\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"minValue\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"toAddress\",\"type\":\"address\"}],\"name\":\"PunkOffered\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"value\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"fromAddress\",\"type\":\"address\"}],\"name\":\"PunkBidEntered\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"value\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"fromAddress\",\"type\":\"address\"}],\"name\":\"PunkBidWithdrawn\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"value\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"fromAddress\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"toAddress\",\"type\":\"address\"}],\"name\":\"PunkBought\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"PunkNoLongerForSale\",\"type\":\"event\"}]",
}

// Cryptopunks0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Cryptopunks0MetaData.ABI instead.
var Cryptopunks0ABI = Cryptopunks0MetaData.ABI

// Cryptopunks0 is an auto generated Go binding around an Ethereum contract.
type Cryptopunks0 struct {
	Cryptopunks0Caller     // Read-only binding to the contract
	Cryptopunks0Transactor // Write-only binding to the contract
	Cryptopunks0Filterer   // Log filterer for contract events
}

// Cryptopunks0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Cryptopunks0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Cryptopunks0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Cryptopunks0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Cryptopunks0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Cryptopunks0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Cryptopunks0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Cryptopunks0Session struct {
	Contract     *Cryptopunks0     // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Cryptopunks0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Cryptopunks0CallerSession struct {
	Contract *Cryptopunks0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts       // Call options to use throughout this session
}

// Cryptopunks0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Cryptopunks0TransactorSession struct {
	Contract     *Cryptopunks0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts       // Transaction auth options to use throughout this session
}

// Cryptopunks0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Cryptopunks0Raw struct {
	Contract *Cryptopunks0 // Generic contract binding to access the raw methods on
}

// Cryptopunks0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Cryptopunks0CallerRaw struct {
	Contract *Cryptopunks0Caller // Generic read-only contract binding to access the raw methods on
}

// Cryptopunks0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Cryptopunks0TransactorRaw struct {
	Contract *Cryptopunks0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewCryptopunks0 creates a new instance of Cryptopunks0, bound to a specific deployed contract.
func NewCryptopunks0(address common.Address, backend bind.ContractBackend) (*Cryptopunks0, error) {
	contract, err := bindCryptopunks0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Cryptopunks0{Cryptopunks0Caller: Cryptopunks0Caller{contract: contract}, Cryptopunks0Transactor: Cryptopunks0Transactor{contract: contract}, Cryptopunks0Filterer: Cryptopunks0Filterer{contract: contract}}, nil
}

// NewCryptopunks0Caller creates a new read-only instance of Cryptopunks0, bound to a specific deployed contract.
func NewCryptopunks0Caller(address common.Address, caller bind.ContractCaller) (*Cryptopunks0Caller, error) {
	contract, err := bindCryptopunks0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Cryptopunks0Caller{contract: contract}, nil
}

// NewCryptopunks0Transactor creates a new write-only instance of Cryptopunks0, bound to a specific deployed contract.
func NewCryptopunks0Transactor(address common.Address, transactor bind.ContractTransactor) (*Cryptopunks0Transactor, error) {
	contract, err := bindCryptopunks0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Cryptopunks0Transactor{contract: contract}, nil
}

// NewCryptopunks0Filterer creates a new log filterer instance of Cryptopunks0, bound to a specific deployed contract.
func NewCryptopunks0Filterer(address common.Address, filterer bind.ContractFilterer) (*Cryptopunks0Filterer, error) {
	contract, err := bindCryptopunks0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Cryptopunks0Filterer{contract: contract}, nil
}

// bindCryptopunks0 binds a generic wrapper to an already deployed contract.
func bindCryptopunks0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Cryptopunks0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Cryptopunks0 *Cryptopunks0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Cryptopunks0.Contract.Cryptopunks0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Cryptopunks0 *Cryptopunks0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.Cryptopunks0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Cryptopunks0 *Cryptopunks0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.Cryptopunks0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Cryptopunks0 *Cryptopunks0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Cryptopunks0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Cryptopunks0 *Cryptopunks0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Cryptopunks0 *Cryptopunks0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.contract.Transact(opts, method, params...)
}

// AllPunksAssigned is a free data retrieval call binding the contract method 0x8126c38a.
//
// Solidity: function allPunksAssigned() returns(bool)
func (_Cryptopunks0 *Cryptopunks0Caller) AllPunksAssigned(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "allPunksAssigned")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// AllPunksAssigned is a free data retrieval call binding the contract method 0x8126c38a.
//
// Solidity: function allPunksAssigned() returns(bool)
func (_Cryptopunks0 *Cryptopunks0Session) AllPunksAssigned() (bool, error) {
	return _Cryptopunks0.Contract.AllPunksAssigned(&_Cryptopunks0.CallOpts)
}

// AllPunksAssigned is a free data retrieval call binding the contract method 0x8126c38a.
//
// Solidity: function allPunksAssigned() returns(bool)
func (_Cryptopunks0 *Cryptopunks0CallerSession) AllPunksAssigned() (bool, error) {
	return _Cryptopunks0.Contract.AllPunksAssigned(&_Cryptopunks0.CallOpts)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address ) returns(uint256)
func (_Cryptopunks0 *Cryptopunks0Caller) BalanceOf(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "balanceOf", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address ) returns(uint256)
func (_Cryptopunks0 *Cryptopunks0Session) BalanceOf(arg0 common.Address) (*big.Int, error) {
	return _Cryptopunks0.Contract.BalanceOf(&_Cryptopunks0.CallOpts, arg0)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address ) returns(uint256)
func (_Cryptopunks0 *Cryptopunks0CallerSession) BalanceOf(arg0 common.Address) (*big.Int, error) {
	return _Cryptopunks0.Contract.BalanceOf(&_Cryptopunks0.CallOpts, arg0)
}

// Decimals is a free data retrieval call binding the contract method 0x313ce567.
//
// Solidity: function decimals() returns(uint8)
func (_Cryptopunks0 *Cryptopunks0Caller) Decimals(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "decimals")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// Decimals is a free data retrieval call binding the contract method 0x313ce567.
//
// Solidity: function decimals() returns(uint8)
func (_Cryptopunks0 *Cryptopunks0Session) Decimals() (uint8, error) {
	return _Cryptopunks0.Contract.Decimals(&_Cryptopunks0.CallOpts)
}

// Decimals is a free data retrieval call binding the contract method 0x313ce567.
//
// Solidity: function decimals() returns(uint8)
func (_Cryptopunks0 *Cryptopunks0CallerSession) Decimals() (uint8, error) {
	return _Cryptopunks0.Contract.Decimals(&_Cryptopunks0.CallOpts)
}

// ImageHash is a free data retrieval call binding the contract method 0x51605d80.
//
// Solidity: function imageHash() returns(string)
func (_Cryptopunks0 *Cryptopunks0Caller) ImageHash(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "imageHash")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// ImageHash is a free data retrieval call binding the contract method 0x51605d80.
//
// Solidity: function imageHash() returns(string)
func (_Cryptopunks0 *Cryptopunks0Session) ImageHash() (string, error) {
	return _Cryptopunks0.Contract.ImageHash(&_Cryptopunks0.CallOpts)
}

// ImageHash is a free data retrieval call binding the contract method 0x51605d80.
//
// Solidity: function imageHash() returns(string)
func (_Cryptopunks0 *Cryptopunks0CallerSession) ImageHash() (string, error) {
	return _Cryptopunks0.Contract.ImageHash(&_Cryptopunks0.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() returns(string)
func (_Cryptopunks0 *Cryptopunks0Caller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() returns(string)
func (_Cryptopunks0 *Cryptopunks0Session) Name() (string, error) {
	return _Cryptopunks0.Contract.Name(&_Cryptopunks0.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() returns(string)
func (_Cryptopunks0 *Cryptopunks0CallerSession) Name() (string, error) {
	return _Cryptopunks0.Contract.Name(&_Cryptopunks0.CallOpts)
}

// NextPunkIndexToAssign is a free data retrieval call binding the contract method 0x52f29a25.
//
// Solidity: function nextPunkIndexToAssign() returns(uint256)
func (_Cryptopunks0 *Cryptopunks0Caller) NextPunkIndexToAssign(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "nextPunkIndexToAssign")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// NextPunkIndexToAssign is a free data retrieval call binding the contract method 0x52f29a25.
//
// Solidity: function nextPunkIndexToAssign() returns(uint256)
func (_Cryptopunks0 *Cryptopunks0Session) NextPunkIndexToAssign() (*big.Int, error) {
	return _Cryptopunks0.Contract.NextPunkIndexToAssign(&_Cryptopunks0.CallOpts)
}

// NextPunkIndexToAssign is a free data retrieval call binding the contract method 0x52f29a25.
//
// Solidity: function nextPunkIndexToAssign() returns(uint256)
func (_Cryptopunks0 *Cryptopunks0CallerSession) NextPunkIndexToAssign() (*big.Int, error) {
	return _Cryptopunks0.Contract.NextPunkIndexToAssign(&_Cryptopunks0.CallOpts)
}

// PendingWithdrawals is a free data retrieval call binding the contract method 0xf3f43703.
//
// Solidity: function pendingWithdrawals(address ) returns(uint256)
func (_Cryptopunks0 *Cryptopunks0Caller) PendingWithdrawals(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "pendingWithdrawals", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// PendingWithdrawals is a free data retrieval call binding the contract method 0xf3f43703.
//
// Solidity: function pendingWithdrawals(address ) returns(uint256)
func (_Cryptopunks0 *Cryptopunks0Session) PendingWithdrawals(arg0 common.Address) (*big.Int, error) {
	return _Cryptopunks0.Contract.PendingWithdrawals(&_Cryptopunks0.CallOpts, arg0)
}

// PendingWithdrawals is a free data retrieval call binding the contract method 0xf3f43703.
//
// Solidity: function pendingWithdrawals(address ) returns(uint256)
func (_Cryptopunks0 *Cryptopunks0CallerSession) PendingWithdrawals(arg0 common.Address) (*big.Int, error) {
	return _Cryptopunks0.Contract.PendingWithdrawals(&_Cryptopunks0.CallOpts, arg0)
}

// PunkBids is a free data retrieval call binding the contract method 0x6e743fa9.
//
// Solidity: function punkBids(uint256 ) returns(bool hasBid, uint256 punkIndex, address bidder, uint256 value)
func (_Cryptopunks0 *Cryptopunks0Caller) PunkBids(opts *bind.CallOpts, arg0 *big.Int) (struct {
	HasBid    bool
	PunkIndex *big.Int
	Bidder    common.Address
	Value     *big.Int
}, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "punkBids", arg0)

	outstruct := new(struct {
		HasBid    bool
		PunkIndex *big.Int
		Bidder    common.Address
		Value     *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.HasBid = *abi.ConvertType(out[0], new(bool)).(*bool)
	outstruct.PunkIndex = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.Bidder = *abi.ConvertType(out[2], new(common.Address)).(*common.Address)
	outstruct.Value = *abi.ConvertType(out[3], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// PunkBids is a free data retrieval call binding the contract method 0x6e743fa9.
//
// Solidity: function punkBids(uint256 ) returns(bool hasBid, uint256 punkIndex, address bidder, uint256 value)
func (_Cryptopunks0 *Cryptopunks0Session) PunkBids(arg0 *big.Int) (struct {
	HasBid    bool
	PunkIndex *big.Int
	Bidder    common.Address
	Value     *big.Int
}, error) {
	return _Cryptopunks0.Contract.PunkBids(&_Cryptopunks0.CallOpts, arg0)
}

// PunkBids is a free data retrieval call binding the contract method 0x6e743fa9.
//
// Solidity: function punkBids(uint256 ) returns(bool hasBid, uint256 punkIndex, address bidder, uint256 value)
func (_Cryptopunks0 *Cryptopunks0CallerSession) PunkBids(arg0 *big.Int) (struct {
	HasBid    bool
	PunkIndex *big.Int
	Bidder    common.Address
	Value     *big.Int
}, error) {
	return _Cryptopunks0.Contract.PunkBids(&_Cryptopunks0.CallOpts, arg0)
}

// PunkIndexToAddress is a free data retrieval call binding the contract method 0x58178168.
//
// Solidity: function punkIndexToAddress(uint256 ) returns(address)
func (_Cryptopunks0 *Cryptopunks0Caller) PunkIndexToAddress(opts *bind.CallOpts, arg0 *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "punkIndexToAddress", arg0)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// PunkIndexToAddress is a free data retrieval call binding the contract method 0x58178168.
//
// Solidity: function punkIndexToAddress(uint256 ) returns(address)
func (_Cryptopunks0 *Cryptopunks0Session) PunkIndexToAddress(arg0 *big.Int) (common.Address, error) {
	return _Cryptopunks0.Contract.PunkIndexToAddress(&_Cryptopunks0.CallOpts, arg0)
}

// PunkIndexToAddress is a free data retrieval call binding the contract method 0x58178168.
//
// Solidity: function punkIndexToAddress(uint256 ) returns(address)
func (_Cryptopunks0 *Cryptopunks0CallerSession) PunkIndexToAddress(arg0 *big.Int) (common.Address, error) {
	return _Cryptopunks0.Contract.PunkIndexToAddress(&_Cryptopunks0.CallOpts, arg0)
}

// PunksOfferedForSale is a free data retrieval call binding the contract method 0x088f11f3.
//
// Solidity: function punksOfferedForSale(uint256 ) returns(bool isForSale, uint256 punkIndex, address seller, uint256 minValue, address onlySellTo)
func (_Cryptopunks0 *Cryptopunks0Caller) PunksOfferedForSale(opts *bind.CallOpts, arg0 *big.Int) (struct {
	IsForSale  bool
	PunkIndex  *big.Int
	Seller     common.Address
	MinValue   *big.Int
	OnlySellTo common.Address
}, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "punksOfferedForSale", arg0)

	outstruct := new(struct {
		IsForSale  bool
		PunkIndex  *big.Int
		Seller     common.Address
		MinValue   *big.Int
		OnlySellTo common.Address
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.IsForSale = *abi.ConvertType(out[0], new(bool)).(*bool)
	outstruct.PunkIndex = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.Seller = *abi.ConvertType(out[2], new(common.Address)).(*common.Address)
	outstruct.MinValue = *abi.ConvertType(out[3], new(*big.Int)).(**big.Int)
	outstruct.OnlySellTo = *abi.ConvertType(out[4], new(common.Address)).(*common.Address)

	return *outstruct, err

}

// PunksOfferedForSale is a free data retrieval call binding the contract method 0x088f11f3.
//
// Solidity: function punksOfferedForSale(uint256 ) returns(bool isForSale, uint256 punkIndex, address seller, uint256 minValue, address onlySellTo)
func (_Cryptopunks0 *Cryptopunks0Session) PunksOfferedForSale(arg0 *big.Int) (struct {
	IsForSale  bool
	PunkIndex  *big.Int
	Seller     common.Address
	MinValue   *big.Int
	OnlySellTo common.Address
}, error) {
	return _Cryptopunks0.Contract.PunksOfferedForSale(&_Cryptopunks0.CallOpts, arg0)
}

// PunksOfferedForSale is a free data retrieval call binding the contract method 0x088f11f3.
//
// Solidity: function punksOfferedForSale(uint256 ) returns(bool isForSale, uint256 punkIndex, address seller, uint256 minValue, address onlySellTo)
func (_Cryptopunks0 *Cryptopunks0CallerSession) PunksOfferedForSale(arg0 *big.Int) (struct {
	IsForSale  bool
	PunkIndex  *big.Int
	Seller     common.Address
	MinValue   *big.Int
	OnlySellTo common.Address
}, error) {
	return _Cryptopunks0.Contract.PunksOfferedForSale(&_Cryptopunks0.CallOpts, arg0)
}

// PunksRemainingToAssign is a free data retrieval call binding the contract method 0xc0d6ce63.
//
// Solidity: function punksRemainingToAssign() returns(uint256)
func (_Cryptopunks0 *Cryptopunks0Caller) PunksRemainingToAssign(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "punksRemainingToAssign")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// PunksRemainingToAssign is a free data retrieval call binding the contract method 0xc0d6ce63.
//
// Solidity: function punksRemainingToAssign() returns(uint256)
func (_Cryptopunks0 *Cryptopunks0Session) PunksRemainingToAssign() (*big.Int, error) {
	return _Cryptopunks0.Contract.PunksRemainingToAssign(&_Cryptopunks0.CallOpts)
}

// PunksRemainingToAssign is a free data retrieval call binding the contract method 0xc0d6ce63.
//
// Solidity: function punksRemainingToAssign() returns(uint256)
func (_Cryptopunks0 *Cryptopunks0CallerSession) PunksRemainingToAssign() (*big.Int, error) {
	return _Cryptopunks0.Contract.PunksRemainingToAssign(&_Cryptopunks0.CallOpts)
}

// Standard is a free data retrieval call binding the contract method 0x5a3b7e42.
//
// Solidity: function standard() returns(string)
func (_Cryptopunks0 *Cryptopunks0Caller) Standard(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "standard")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Standard is a free data retrieval call binding the contract method 0x5a3b7e42.
//
// Solidity: function standard() returns(string)
func (_Cryptopunks0 *Cryptopunks0Session) Standard() (string, error) {
	return _Cryptopunks0.Contract.Standard(&_Cryptopunks0.CallOpts)
}

// Standard is a free data retrieval call binding the contract method 0x5a3b7e42.
//
// Solidity: function standard() returns(string)
func (_Cryptopunks0 *Cryptopunks0CallerSession) Standard() (string, error) {
	return _Cryptopunks0.Contract.Standard(&_Cryptopunks0.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() returns(string)
func (_Cryptopunks0 *Cryptopunks0Caller) Symbol(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() returns(string)
func (_Cryptopunks0 *Cryptopunks0Session) Symbol() (string, error) {
	return _Cryptopunks0.Contract.Symbol(&_Cryptopunks0.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() returns(string)
func (_Cryptopunks0 *Cryptopunks0CallerSession) Symbol() (string, error) {
	return _Cryptopunks0.Contract.Symbol(&_Cryptopunks0.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() returns(uint256)
func (_Cryptopunks0 *Cryptopunks0Caller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Cryptopunks0.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() returns(uint256)
func (_Cryptopunks0 *Cryptopunks0Session) TotalSupply() (*big.Int, error) {
	return _Cryptopunks0.Contract.TotalSupply(&_Cryptopunks0.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() returns(uint256)
func (_Cryptopunks0 *Cryptopunks0CallerSession) TotalSupply() (*big.Int, error) {
	return _Cryptopunks0.Contract.TotalSupply(&_Cryptopunks0.CallOpts)
}

// AcceptBidForPunk is a paid mutator transaction binding the contract method 0x23165b75.
//
// Solidity: function acceptBidForPunk(uint256 punkIndex, uint256 minPrice) returns()
func (_Cryptopunks0 *Cryptopunks0Transactor) AcceptBidForPunk(opts *bind.TransactOpts, punkIndex *big.Int, minPrice *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.contract.Transact(opts, "acceptBidForPunk", punkIndex, minPrice)
}

// AcceptBidForPunk is a paid mutator transaction binding the contract method 0x23165b75.
//
// Solidity: function acceptBidForPunk(uint256 punkIndex, uint256 minPrice) returns()
func (_Cryptopunks0 *Cryptopunks0Session) AcceptBidForPunk(punkIndex *big.Int, minPrice *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.AcceptBidForPunk(&_Cryptopunks0.TransactOpts, punkIndex, minPrice)
}

// AcceptBidForPunk is a paid mutator transaction binding the contract method 0x23165b75.
//
// Solidity: function acceptBidForPunk(uint256 punkIndex, uint256 minPrice) returns()
func (_Cryptopunks0 *Cryptopunks0TransactorSession) AcceptBidForPunk(punkIndex *big.Int, minPrice *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.AcceptBidForPunk(&_Cryptopunks0.TransactOpts, punkIndex, minPrice)
}

// AllInitialOwnersAssigned is a paid mutator transaction binding the contract method 0x7ecedac9.
//
// Solidity: function allInitialOwnersAssigned() returns()
func (_Cryptopunks0 *Cryptopunks0Transactor) AllInitialOwnersAssigned(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptopunks0.contract.Transact(opts, "allInitialOwnersAssigned")
}

// AllInitialOwnersAssigned is a paid mutator transaction binding the contract method 0x7ecedac9.
//
// Solidity: function allInitialOwnersAssigned() returns()
func (_Cryptopunks0 *Cryptopunks0Session) AllInitialOwnersAssigned() (*types.Transaction, error) {
	return _Cryptopunks0.Contract.AllInitialOwnersAssigned(&_Cryptopunks0.TransactOpts)
}

// AllInitialOwnersAssigned is a paid mutator transaction binding the contract method 0x7ecedac9.
//
// Solidity: function allInitialOwnersAssigned() returns()
func (_Cryptopunks0 *Cryptopunks0TransactorSession) AllInitialOwnersAssigned() (*types.Transaction, error) {
	return _Cryptopunks0.Contract.AllInitialOwnersAssigned(&_Cryptopunks0.TransactOpts)
}

// BuyPunk is a paid mutator transaction binding the contract method 0x8264fe98.
//
// Solidity: function buyPunk(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Transactor) BuyPunk(opts *bind.TransactOpts, punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.contract.Transact(opts, "buyPunk", punkIndex)
}

// BuyPunk is a paid mutator transaction binding the contract method 0x8264fe98.
//
// Solidity: function buyPunk(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Session) BuyPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.BuyPunk(&_Cryptopunks0.TransactOpts, punkIndex)
}

// BuyPunk is a paid mutator transaction binding the contract method 0x8264fe98.
//
// Solidity: function buyPunk(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0TransactorSession) BuyPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.BuyPunk(&_Cryptopunks0.TransactOpts, punkIndex)
}

// EnterBidForPunk is a paid mutator transaction binding the contract method 0x091dbfd2.
//
// Solidity: function enterBidForPunk(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Transactor) EnterBidForPunk(opts *bind.TransactOpts, punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.contract.Transact(opts, "enterBidForPunk", punkIndex)
}

// EnterBidForPunk is a paid mutator transaction binding the contract method 0x091dbfd2.
//
// Solidity: function enterBidForPunk(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Session) EnterBidForPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.EnterBidForPunk(&_Cryptopunks0.TransactOpts, punkIndex)
}

// EnterBidForPunk is a paid mutator transaction binding the contract method 0x091dbfd2.
//
// Solidity: function enterBidForPunk(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0TransactorSession) EnterBidForPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.EnterBidForPunk(&_Cryptopunks0.TransactOpts, punkIndex)
}

// GetPunk is a paid mutator transaction binding the contract method 0xc81d1d5b.
//
// Solidity: function getPunk(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Transactor) GetPunk(opts *bind.TransactOpts, punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.contract.Transact(opts, "getPunk", punkIndex)
}

// GetPunk is a paid mutator transaction binding the contract method 0xc81d1d5b.
//
// Solidity: function getPunk(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Session) GetPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.GetPunk(&_Cryptopunks0.TransactOpts, punkIndex)
}

// GetPunk is a paid mutator transaction binding the contract method 0xc81d1d5b.
//
// Solidity: function getPunk(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0TransactorSession) GetPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.GetPunk(&_Cryptopunks0.TransactOpts, punkIndex)
}

// OfferPunkForSale is a paid mutator transaction binding the contract method 0xc44193c3.
//
// Solidity: function offerPunkForSale(uint256 punkIndex, uint256 minSalePriceInWei) returns()
func (_Cryptopunks0 *Cryptopunks0Transactor) OfferPunkForSale(opts *bind.TransactOpts, punkIndex *big.Int, minSalePriceInWei *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.contract.Transact(opts, "offerPunkForSale", punkIndex, minSalePriceInWei)
}

// OfferPunkForSale is a paid mutator transaction binding the contract method 0xc44193c3.
//
// Solidity: function offerPunkForSale(uint256 punkIndex, uint256 minSalePriceInWei) returns()
func (_Cryptopunks0 *Cryptopunks0Session) OfferPunkForSale(punkIndex *big.Int, minSalePriceInWei *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.OfferPunkForSale(&_Cryptopunks0.TransactOpts, punkIndex, minSalePriceInWei)
}

// OfferPunkForSale is a paid mutator transaction binding the contract method 0xc44193c3.
//
// Solidity: function offerPunkForSale(uint256 punkIndex, uint256 minSalePriceInWei) returns()
func (_Cryptopunks0 *Cryptopunks0TransactorSession) OfferPunkForSale(punkIndex *big.Int, minSalePriceInWei *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.OfferPunkForSale(&_Cryptopunks0.TransactOpts, punkIndex, minSalePriceInWei)
}

// OfferPunkForSaleToAddress is a paid mutator transaction binding the contract method 0xbf31196f.
//
// Solidity: function offerPunkForSaleToAddress(uint256 punkIndex, uint256 minSalePriceInWei, address toAddress) returns()
func (_Cryptopunks0 *Cryptopunks0Transactor) OfferPunkForSaleToAddress(opts *bind.TransactOpts, punkIndex *big.Int, minSalePriceInWei *big.Int, toAddress common.Address) (*types.Transaction, error) {
	return _Cryptopunks0.contract.Transact(opts, "offerPunkForSaleToAddress", punkIndex, minSalePriceInWei, toAddress)
}

// OfferPunkForSaleToAddress is a paid mutator transaction binding the contract method 0xbf31196f.
//
// Solidity: function offerPunkForSaleToAddress(uint256 punkIndex, uint256 minSalePriceInWei, address toAddress) returns()
func (_Cryptopunks0 *Cryptopunks0Session) OfferPunkForSaleToAddress(punkIndex *big.Int, minSalePriceInWei *big.Int, toAddress common.Address) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.OfferPunkForSaleToAddress(&_Cryptopunks0.TransactOpts, punkIndex, minSalePriceInWei, toAddress)
}

// OfferPunkForSaleToAddress is a paid mutator transaction binding the contract method 0xbf31196f.
//
// Solidity: function offerPunkForSaleToAddress(uint256 punkIndex, uint256 minSalePriceInWei, address toAddress) returns()
func (_Cryptopunks0 *Cryptopunks0TransactorSession) OfferPunkForSaleToAddress(punkIndex *big.Int, minSalePriceInWei *big.Int, toAddress common.Address) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.OfferPunkForSaleToAddress(&_Cryptopunks0.TransactOpts, punkIndex, minSalePriceInWei, toAddress)
}

// PunkNoLongerForSale is a paid mutator transaction binding the contract method 0xf6eeff1e.
//
// Solidity: function punkNoLongerForSale(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Transactor) PunkNoLongerForSale(opts *bind.TransactOpts, punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.contract.Transact(opts, "punkNoLongerForSale", punkIndex)
}

// PunkNoLongerForSale is a paid mutator transaction binding the contract method 0xf6eeff1e.
//
// Solidity: function punkNoLongerForSale(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Session) PunkNoLongerForSale(punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.PunkNoLongerForSale(&_Cryptopunks0.TransactOpts, punkIndex)
}

// PunkNoLongerForSale is a paid mutator transaction binding the contract method 0xf6eeff1e.
//
// Solidity: function punkNoLongerForSale(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0TransactorSession) PunkNoLongerForSale(punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.PunkNoLongerForSale(&_Cryptopunks0.TransactOpts, punkIndex)
}

// SetInitialOwner is a paid mutator transaction binding the contract method 0xa75a9049.
//
// Solidity: function setInitialOwner(address to, uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Transactor) SetInitialOwner(opts *bind.TransactOpts, to common.Address, punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.contract.Transact(opts, "setInitialOwner", to, punkIndex)
}

// SetInitialOwner is a paid mutator transaction binding the contract method 0xa75a9049.
//
// Solidity: function setInitialOwner(address to, uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Session) SetInitialOwner(to common.Address, punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.SetInitialOwner(&_Cryptopunks0.TransactOpts, to, punkIndex)
}

// SetInitialOwner is a paid mutator transaction binding the contract method 0xa75a9049.
//
// Solidity: function setInitialOwner(address to, uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0TransactorSession) SetInitialOwner(to common.Address, punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.SetInitialOwner(&_Cryptopunks0.TransactOpts, to, punkIndex)
}

// SetInitialOwners is a paid mutator transaction binding the contract method 0x39c5dde6.
//
// Solidity: function setInitialOwners(address[] addresses, uint256[] indices) returns()
func (_Cryptopunks0 *Cryptopunks0Transactor) SetInitialOwners(opts *bind.TransactOpts, addresses []common.Address, indices []*big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.contract.Transact(opts, "setInitialOwners", addresses, indices)
}

// SetInitialOwners is a paid mutator transaction binding the contract method 0x39c5dde6.
//
// Solidity: function setInitialOwners(address[] addresses, uint256[] indices) returns()
func (_Cryptopunks0 *Cryptopunks0Session) SetInitialOwners(addresses []common.Address, indices []*big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.SetInitialOwners(&_Cryptopunks0.TransactOpts, addresses, indices)
}

// SetInitialOwners is a paid mutator transaction binding the contract method 0x39c5dde6.
//
// Solidity: function setInitialOwners(address[] addresses, uint256[] indices) returns()
func (_Cryptopunks0 *Cryptopunks0TransactorSession) SetInitialOwners(addresses []common.Address, indices []*big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.SetInitialOwners(&_Cryptopunks0.TransactOpts, addresses, indices)
}

// TransferPunk is a paid mutator transaction binding the contract method 0x8b72a2ec.
//
// Solidity: function transferPunk(address to, uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Transactor) TransferPunk(opts *bind.TransactOpts, to common.Address, punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.contract.Transact(opts, "transferPunk", to, punkIndex)
}

// TransferPunk is a paid mutator transaction binding the contract method 0x8b72a2ec.
//
// Solidity: function transferPunk(address to, uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Session) TransferPunk(to common.Address, punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.TransferPunk(&_Cryptopunks0.TransactOpts, to, punkIndex)
}

// TransferPunk is a paid mutator transaction binding the contract method 0x8b72a2ec.
//
// Solidity: function transferPunk(address to, uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0TransactorSession) TransferPunk(to common.Address, punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.TransferPunk(&_Cryptopunks0.TransactOpts, to, punkIndex)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_Cryptopunks0 *Cryptopunks0Transactor) Withdraw(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptopunks0.contract.Transact(opts, "withdraw")
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_Cryptopunks0 *Cryptopunks0Session) Withdraw() (*types.Transaction, error) {
	return _Cryptopunks0.Contract.Withdraw(&_Cryptopunks0.TransactOpts)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_Cryptopunks0 *Cryptopunks0TransactorSession) Withdraw() (*types.Transaction, error) {
	return _Cryptopunks0.Contract.Withdraw(&_Cryptopunks0.TransactOpts)
}

// WithdrawBidForPunk is a paid mutator transaction binding the contract method 0x979bc638.
//
// Solidity: function withdrawBidForPunk(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Transactor) WithdrawBidForPunk(opts *bind.TransactOpts, punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.contract.Transact(opts, "withdrawBidForPunk", punkIndex)
}

// WithdrawBidForPunk is a paid mutator transaction binding the contract method 0x979bc638.
//
// Solidity: function withdrawBidForPunk(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0Session) WithdrawBidForPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.WithdrawBidForPunk(&_Cryptopunks0.TransactOpts, punkIndex)
}

// WithdrawBidForPunk is a paid mutator transaction binding the contract method 0x979bc638.
//
// Solidity: function withdrawBidForPunk(uint256 punkIndex) returns()
func (_Cryptopunks0 *Cryptopunks0TransactorSession) WithdrawBidForPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _Cryptopunks0.Contract.WithdrawBidForPunk(&_Cryptopunks0.TransactOpts, punkIndex)
}

// Cryptopunks0AssignIterator is returned from FilterAssign and is used to iterate over the raw logs and unpacked data for Assign events raised by the Cryptopunks0 contract.
type Cryptopunks0AssignIterator struct {
	Event *Cryptopunks0Assign // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptopunks0AssignIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptopunks0Assign)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptopunks0Assign)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptopunks0AssignIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptopunks0AssignIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptopunks0Assign represents a Assign event raised by the Cryptopunks0 contract.
type Cryptopunks0Assign struct {
	To        common.Address
	PunkIndex *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterAssign is a free log retrieval operation binding the contract event 0x8a0e37b73a0d9c82e205d4d1a3ff3d0b57ce5f4d7bccf6bac03336dc101cb7ba.
//
// Solidity: event Assign(address indexed to, uint256 punkIndex)
func (_Cryptopunks0 *Cryptopunks0Filterer) FilterAssign(opts *bind.FilterOpts, to []common.Address) (*Cryptopunks0AssignIterator, error) {

	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _Cryptopunks0.contract.FilterLogs(opts, "Assign", toRule)
	if err != nil {
		return nil, err
	}
	return &Cryptopunks0AssignIterator{contract: _Cryptopunks0.contract, event: "Assign", logs: logs, sub: sub}, nil
}

// WatchAssign is a free log subscription operation binding the contract event 0x8a0e37b73a0d9c82e205d4d1a3ff3d0b57ce5f4d7bccf6bac03336dc101cb7ba.
//
// Solidity: event Assign(address indexed to, uint256 punkIndex)
func (_Cryptopunks0 *Cryptopunks0Filterer) WatchAssign(opts *bind.WatchOpts, sink chan<- *Cryptopunks0Assign, to []common.Address) (event.Subscription, error) {

	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _Cryptopunks0.contract.WatchLogs(opts, "Assign", toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptopunks0Assign)
				if err := _Cryptopunks0.contract.UnpackLog(event, "Assign", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAssign is a log parse operation binding the contract event 0x8a0e37b73a0d9c82e205d4d1a3ff3d0b57ce5f4d7bccf6bac03336dc101cb7ba.
//
// Solidity: event Assign(address indexed to, uint256 punkIndex)
func (_Cryptopunks0 *Cryptopunks0Filterer) ParseAssign(log types.Log) (*Cryptopunks0Assign, error) {
	event := new(Cryptopunks0Assign)
	if err := _Cryptopunks0.contract.UnpackLog(event, "Assign", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Cryptopunks0PunkBidEnteredIterator is returned from FilterPunkBidEntered and is used to iterate over the raw logs and unpacked data for PunkBidEntered events raised by the Cryptopunks0 contract.
type Cryptopunks0PunkBidEnteredIterator struct {
	Event *Cryptopunks0PunkBidEntered // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptopunks0PunkBidEnteredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptopunks0PunkBidEntered)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptopunks0PunkBidEntered)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptopunks0PunkBidEnteredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptopunks0PunkBidEnteredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptopunks0PunkBidEntered represents a PunkBidEntered event raised by the Cryptopunks0 contract.
type Cryptopunks0PunkBidEntered struct {
	PunkIndex   *big.Int
	Value       *big.Int
	FromAddress common.Address
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterPunkBidEntered is a free log retrieval operation binding the contract event 0x5b859394fabae0c1ba88baffe67e751ab5248d2e879028b8c8d6897b0519f56a.
//
// Solidity: event PunkBidEntered(uint256 indexed punkIndex, uint256 value, address indexed fromAddress)
func (_Cryptopunks0 *Cryptopunks0Filterer) FilterPunkBidEntered(opts *bind.FilterOpts, punkIndex []*big.Int, fromAddress []common.Address) (*Cryptopunks0PunkBidEnteredIterator, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var fromAddressRule []interface{}
	for _, fromAddressItem := range fromAddress {
		fromAddressRule = append(fromAddressRule, fromAddressItem)
	}

	logs, sub, err := _Cryptopunks0.contract.FilterLogs(opts, "PunkBidEntered", punkIndexRule, fromAddressRule)
	if err != nil {
		return nil, err
	}
	return &Cryptopunks0PunkBidEnteredIterator{contract: _Cryptopunks0.contract, event: "PunkBidEntered", logs: logs, sub: sub}, nil
}

// WatchPunkBidEntered is a free log subscription operation binding the contract event 0x5b859394fabae0c1ba88baffe67e751ab5248d2e879028b8c8d6897b0519f56a.
//
// Solidity: event PunkBidEntered(uint256 indexed punkIndex, uint256 value, address indexed fromAddress)
func (_Cryptopunks0 *Cryptopunks0Filterer) WatchPunkBidEntered(opts *bind.WatchOpts, sink chan<- *Cryptopunks0PunkBidEntered, punkIndex []*big.Int, fromAddress []common.Address) (event.Subscription, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var fromAddressRule []interface{}
	for _, fromAddressItem := range fromAddress {
		fromAddressRule = append(fromAddressRule, fromAddressItem)
	}

	logs, sub, err := _Cryptopunks0.contract.WatchLogs(opts, "PunkBidEntered", punkIndexRule, fromAddressRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptopunks0PunkBidEntered)
				if err := _Cryptopunks0.contract.UnpackLog(event, "PunkBidEntered", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePunkBidEntered is a log parse operation binding the contract event 0x5b859394fabae0c1ba88baffe67e751ab5248d2e879028b8c8d6897b0519f56a.
//
// Solidity: event PunkBidEntered(uint256 indexed punkIndex, uint256 value, address indexed fromAddress)
func (_Cryptopunks0 *Cryptopunks0Filterer) ParsePunkBidEntered(log types.Log) (*Cryptopunks0PunkBidEntered, error) {
	event := new(Cryptopunks0PunkBidEntered)
	if err := _Cryptopunks0.contract.UnpackLog(event, "PunkBidEntered", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Cryptopunks0PunkBidWithdrawnIterator is returned from FilterPunkBidWithdrawn and is used to iterate over the raw logs and unpacked data for PunkBidWithdrawn events raised by the Cryptopunks0 contract.
type Cryptopunks0PunkBidWithdrawnIterator struct {
	Event *Cryptopunks0PunkBidWithdrawn // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptopunks0PunkBidWithdrawnIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptopunks0PunkBidWithdrawn)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptopunks0PunkBidWithdrawn)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptopunks0PunkBidWithdrawnIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptopunks0PunkBidWithdrawnIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptopunks0PunkBidWithdrawn represents a PunkBidWithdrawn event raised by the Cryptopunks0 contract.
type Cryptopunks0PunkBidWithdrawn struct {
	PunkIndex   *big.Int
	Value       *big.Int
	FromAddress common.Address
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterPunkBidWithdrawn is a free log retrieval operation binding the contract event 0x6f30e1ee4d81dcc7a8a478577f65d2ed2edb120565960ac45fe7c50551c87932.
//
// Solidity: event PunkBidWithdrawn(uint256 indexed punkIndex, uint256 value, address indexed fromAddress)
func (_Cryptopunks0 *Cryptopunks0Filterer) FilterPunkBidWithdrawn(opts *bind.FilterOpts, punkIndex []*big.Int, fromAddress []common.Address) (*Cryptopunks0PunkBidWithdrawnIterator, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var fromAddressRule []interface{}
	for _, fromAddressItem := range fromAddress {
		fromAddressRule = append(fromAddressRule, fromAddressItem)
	}

	logs, sub, err := _Cryptopunks0.contract.FilterLogs(opts, "PunkBidWithdrawn", punkIndexRule, fromAddressRule)
	if err != nil {
		return nil, err
	}
	return &Cryptopunks0PunkBidWithdrawnIterator{contract: _Cryptopunks0.contract, event: "PunkBidWithdrawn", logs: logs, sub: sub}, nil
}

// WatchPunkBidWithdrawn is a free log subscription operation binding the contract event 0x6f30e1ee4d81dcc7a8a478577f65d2ed2edb120565960ac45fe7c50551c87932.
//
// Solidity: event PunkBidWithdrawn(uint256 indexed punkIndex, uint256 value, address indexed fromAddress)
func (_Cryptopunks0 *Cryptopunks0Filterer) WatchPunkBidWithdrawn(opts *bind.WatchOpts, sink chan<- *Cryptopunks0PunkBidWithdrawn, punkIndex []*big.Int, fromAddress []common.Address) (event.Subscription, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var fromAddressRule []interface{}
	for _, fromAddressItem := range fromAddress {
		fromAddressRule = append(fromAddressRule, fromAddressItem)
	}

	logs, sub, err := _Cryptopunks0.contract.WatchLogs(opts, "PunkBidWithdrawn", punkIndexRule, fromAddressRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptopunks0PunkBidWithdrawn)
				if err := _Cryptopunks0.contract.UnpackLog(event, "PunkBidWithdrawn", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePunkBidWithdrawn is a log parse operation binding the contract event 0x6f30e1ee4d81dcc7a8a478577f65d2ed2edb120565960ac45fe7c50551c87932.
//
// Solidity: event PunkBidWithdrawn(uint256 indexed punkIndex, uint256 value, address indexed fromAddress)
func (_Cryptopunks0 *Cryptopunks0Filterer) ParsePunkBidWithdrawn(log types.Log) (*Cryptopunks0PunkBidWithdrawn, error) {
	event := new(Cryptopunks0PunkBidWithdrawn)
	if err := _Cryptopunks0.contract.UnpackLog(event, "PunkBidWithdrawn", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Cryptopunks0PunkBoughtIterator is returned from FilterPunkBought and is used to iterate over the raw logs and unpacked data for PunkBought events raised by the Cryptopunks0 contract.
type Cryptopunks0PunkBoughtIterator struct {
	Event *Cryptopunks0PunkBought // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptopunks0PunkBoughtIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptopunks0PunkBought)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptopunks0PunkBought)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptopunks0PunkBoughtIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptopunks0PunkBoughtIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptopunks0PunkBought represents a PunkBought event raised by the Cryptopunks0 contract.
type Cryptopunks0PunkBought struct {
	PunkIndex   *big.Int
	Value       *big.Int
	FromAddress common.Address
	ToAddress   common.Address
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterPunkBought is a free log retrieval operation binding the contract event 0x58e5d5a525e3b40bc15abaa38b5882678db1ee68befd2f60bafe3a7fd06db9e3.
//
// Solidity: event PunkBought(uint256 indexed punkIndex, uint256 value, address indexed fromAddress, address indexed toAddress)
func (_Cryptopunks0 *Cryptopunks0Filterer) FilterPunkBought(opts *bind.FilterOpts, punkIndex []*big.Int, fromAddress []common.Address, toAddress []common.Address) (*Cryptopunks0PunkBoughtIterator, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var fromAddressRule []interface{}
	for _, fromAddressItem := range fromAddress {
		fromAddressRule = append(fromAddressRule, fromAddressItem)
	}
	var toAddressRule []interface{}
	for _, toAddressItem := range toAddress {
		toAddressRule = append(toAddressRule, toAddressItem)
	}

	logs, sub, err := _Cryptopunks0.contract.FilterLogs(opts, "PunkBought", punkIndexRule, fromAddressRule, toAddressRule)
	if err != nil {
		return nil, err
	}
	return &Cryptopunks0PunkBoughtIterator{contract: _Cryptopunks0.contract, event: "PunkBought", logs: logs, sub: sub}, nil
}

// WatchPunkBought is a free log subscription operation binding the contract event 0x58e5d5a525e3b40bc15abaa38b5882678db1ee68befd2f60bafe3a7fd06db9e3.
//
// Solidity: event PunkBought(uint256 indexed punkIndex, uint256 value, address indexed fromAddress, address indexed toAddress)
func (_Cryptopunks0 *Cryptopunks0Filterer) WatchPunkBought(opts *bind.WatchOpts, sink chan<- *Cryptopunks0PunkBought, punkIndex []*big.Int, fromAddress []common.Address, toAddress []common.Address) (event.Subscription, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var fromAddressRule []interface{}
	for _, fromAddressItem := range fromAddress {
		fromAddressRule = append(fromAddressRule, fromAddressItem)
	}
	var toAddressRule []interface{}
	for _, toAddressItem := range toAddress {
		toAddressRule = append(toAddressRule, toAddressItem)
	}

	logs, sub, err := _Cryptopunks0.contract.WatchLogs(opts, "PunkBought", punkIndexRule, fromAddressRule, toAddressRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptopunks0PunkBought)
				if err := _Cryptopunks0.contract.UnpackLog(event, "PunkBought", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePunkBought is a log parse operation binding the contract event 0x58e5d5a525e3b40bc15abaa38b5882678db1ee68befd2f60bafe3a7fd06db9e3.
//
// Solidity: event PunkBought(uint256 indexed punkIndex, uint256 value, address indexed fromAddress, address indexed toAddress)
func (_Cryptopunks0 *Cryptopunks0Filterer) ParsePunkBought(log types.Log) (*Cryptopunks0PunkBought, error) {
	event := new(Cryptopunks0PunkBought)
	if err := _Cryptopunks0.contract.UnpackLog(event, "PunkBought", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Cryptopunks0PunkNoLongerForSaleIterator is returned from FilterPunkNoLongerForSale and is used to iterate over the raw logs and unpacked data for PunkNoLongerForSale events raised by the Cryptopunks0 contract.
type Cryptopunks0PunkNoLongerForSaleIterator struct {
	Event *Cryptopunks0PunkNoLongerForSale // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptopunks0PunkNoLongerForSaleIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptopunks0PunkNoLongerForSale)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptopunks0PunkNoLongerForSale)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptopunks0PunkNoLongerForSaleIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptopunks0PunkNoLongerForSaleIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptopunks0PunkNoLongerForSale represents a PunkNoLongerForSale event raised by the Cryptopunks0 contract.
type Cryptopunks0PunkNoLongerForSale struct {
	PunkIndex *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterPunkNoLongerForSale is a free log retrieval operation binding the contract event 0xb0e0a660b4e50f26f0b7ce75c24655fc76cc66e3334a54ff410277229fa10bd4.
//
// Solidity: event PunkNoLongerForSale(uint256 indexed punkIndex)
func (_Cryptopunks0 *Cryptopunks0Filterer) FilterPunkNoLongerForSale(opts *bind.FilterOpts, punkIndex []*big.Int) (*Cryptopunks0PunkNoLongerForSaleIterator, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	logs, sub, err := _Cryptopunks0.contract.FilterLogs(opts, "PunkNoLongerForSale", punkIndexRule)
	if err != nil {
		return nil, err
	}
	return &Cryptopunks0PunkNoLongerForSaleIterator{contract: _Cryptopunks0.contract, event: "PunkNoLongerForSale", logs: logs, sub: sub}, nil
}

// WatchPunkNoLongerForSale is a free log subscription operation binding the contract event 0xb0e0a660b4e50f26f0b7ce75c24655fc76cc66e3334a54ff410277229fa10bd4.
//
// Solidity: event PunkNoLongerForSale(uint256 indexed punkIndex)
func (_Cryptopunks0 *Cryptopunks0Filterer) WatchPunkNoLongerForSale(opts *bind.WatchOpts, sink chan<- *Cryptopunks0PunkNoLongerForSale, punkIndex []*big.Int) (event.Subscription, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	logs, sub, err := _Cryptopunks0.contract.WatchLogs(opts, "PunkNoLongerForSale", punkIndexRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptopunks0PunkNoLongerForSale)
				if err := _Cryptopunks0.contract.UnpackLog(event, "PunkNoLongerForSale", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePunkNoLongerForSale is a log parse operation binding the contract event 0xb0e0a660b4e50f26f0b7ce75c24655fc76cc66e3334a54ff410277229fa10bd4.
//
// Solidity: event PunkNoLongerForSale(uint256 indexed punkIndex)
func (_Cryptopunks0 *Cryptopunks0Filterer) ParsePunkNoLongerForSale(log types.Log) (*Cryptopunks0PunkNoLongerForSale, error) {
	event := new(Cryptopunks0PunkNoLongerForSale)
	if err := _Cryptopunks0.contract.UnpackLog(event, "PunkNoLongerForSale", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Cryptopunks0PunkOfferedIterator is returned from FilterPunkOffered and is used to iterate over the raw logs and unpacked data for PunkOffered events raised by the Cryptopunks0 contract.
type Cryptopunks0PunkOfferedIterator struct {
	Event *Cryptopunks0PunkOffered // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptopunks0PunkOfferedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptopunks0PunkOffered)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptopunks0PunkOffered)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptopunks0PunkOfferedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptopunks0PunkOfferedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptopunks0PunkOffered represents a PunkOffered event raised by the Cryptopunks0 contract.
type Cryptopunks0PunkOffered struct {
	PunkIndex *big.Int
	MinValue  *big.Int
	ToAddress common.Address
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterPunkOffered is a free log retrieval operation binding the contract event 0x3c7b682d5da98001a9b8cbda6c647d2c63d698a4184fd1d55e2ce7b66f5d21eb.
//
// Solidity: event PunkOffered(uint256 indexed punkIndex, uint256 minValue, address indexed toAddress)
func (_Cryptopunks0 *Cryptopunks0Filterer) FilterPunkOffered(opts *bind.FilterOpts, punkIndex []*big.Int, toAddress []common.Address) (*Cryptopunks0PunkOfferedIterator, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var toAddressRule []interface{}
	for _, toAddressItem := range toAddress {
		toAddressRule = append(toAddressRule, toAddressItem)
	}

	logs, sub, err := _Cryptopunks0.contract.FilterLogs(opts, "PunkOffered", punkIndexRule, toAddressRule)
	if err != nil {
		return nil, err
	}
	return &Cryptopunks0PunkOfferedIterator{contract: _Cryptopunks0.contract, event: "PunkOffered", logs: logs, sub: sub}, nil
}

// WatchPunkOffered is a free log subscription operation binding the contract event 0x3c7b682d5da98001a9b8cbda6c647d2c63d698a4184fd1d55e2ce7b66f5d21eb.
//
// Solidity: event PunkOffered(uint256 indexed punkIndex, uint256 minValue, address indexed toAddress)
func (_Cryptopunks0 *Cryptopunks0Filterer) WatchPunkOffered(opts *bind.WatchOpts, sink chan<- *Cryptopunks0PunkOffered, punkIndex []*big.Int, toAddress []common.Address) (event.Subscription, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var toAddressRule []interface{}
	for _, toAddressItem := range toAddress {
		toAddressRule = append(toAddressRule, toAddressItem)
	}

	logs, sub, err := _Cryptopunks0.contract.WatchLogs(opts, "PunkOffered", punkIndexRule, toAddressRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptopunks0PunkOffered)
				if err := _Cryptopunks0.contract.UnpackLog(event, "PunkOffered", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePunkOffered is a log parse operation binding the contract event 0x3c7b682d5da98001a9b8cbda6c647d2c63d698a4184fd1d55e2ce7b66f5d21eb.
//
// Solidity: event PunkOffered(uint256 indexed punkIndex, uint256 minValue, address indexed toAddress)
func (_Cryptopunks0 *Cryptopunks0Filterer) ParsePunkOffered(log types.Log) (*Cryptopunks0PunkOffered, error) {
	event := new(Cryptopunks0PunkOffered)
	if err := _Cryptopunks0.contract.UnpackLog(event, "PunkOffered", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Cryptopunks0PunkTransferIterator is returned from FilterPunkTransfer and is used to iterate over the raw logs and unpacked data for PunkTransfer events raised by the Cryptopunks0 contract.
type Cryptopunks0PunkTransferIterator struct {
	Event *Cryptopunks0PunkTransfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptopunks0PunkTransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptopunks0PunkTransfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptopunks0PunkTransfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptopunks0PunkTransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptopunks0PunkTransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptopunks0PunkTransfer represents a PunkTransfer event raised by the Cryptopunks0 contract.
type Cryptopunks0PunkTransfer struct {
	From      common.Address
	To        common.Address
	PunkIndex *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterPunkTransfer is a free log retrieval operation binding the contract event 0x05af636b70da6819000c49f85b21fa82081c632069bb626f30932034099107d8.
//
// Solidity: event PunkTransfer(address indexed from, address indexed to, uint256 punkIndex)
func (_Cryptopunks0 *Cryptopunks0Filterer) FilterPunkTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*Cryptopunks0PunkTransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _Cryptopunks0.contract.FilterLogs(opts, "PunkTransfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return &Cryptopunks0PunkTransferIterator{contract: _Cryptopunks0.contract, event: "PunkTransfer", logs: logs, sub: sub}, nil
}

// WatchPunkTransfer is a free log subscription operation binding the contract event 0x05af636b70da6819000c49f85b21fa82081c632069bb626f30932034099107d8.
//
// Solidity: event PunkTransfer(address indexed from, address indexed to, uint256 punkIndex)
func (_Cryptopunks0 *Cryptopunks0Filterer) WatchPunkTransfer(opts *bind.WatchOpts, sink chan<- *Cryptopunks0PunkTransfer, from []common.Address, to []common.Address) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _Cryptopunks0.contract.WatchLogs(opts, "PunkTransfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptopunks0PunkTransfer)
				if err := _Cryptopunks0.contract.UnpackLog(event, "PunkTransfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePunkTransfer is a log parse operation binding the contract event 0x05af636b70da6819000c49f85b21fa82081c632069bb626f30932034099107d8.
//
// Solidity: event PunkTransfer(address indexed from, address indexed to, uint256 punkIndex)
func (_Cryptopunks0 *Cryptopunks0Filterer) ParsePunkTransfer(log types.Log) (*Cryptopunks0PunkTransfer, error) {
	event := new(Cryptopunks0PunkTransfer)
	if err := _Cryptopunks0.contract.UnpackLog(event, "PunkTransfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Cryptopunks0TransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the Cryptopunks0 contract.
type Cryptopunks0TransferIterator struct {
	Event *Cryptopunks0Transfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptopunks0TransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptopunks0Transfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptopunks0Transfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptopunks0TransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptopunks0TransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptopunks0Transfer represents a Transfer event raised by the Cryptopunks0 contract.
type Cryptopunks0Transfer struct {
	From  common.Address
	To    common.Address
	Value *big.Int
	Raw   types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_Cryptopunks0 *Cryptopunks0Filterer) FilterTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*Cryptopunks0TransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _Cryptopunks0.contract.FilterLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return &Cryptopunks0TransferIterator{contract: _Cryptopunks0.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_Cryptopunks0 *Cryptopunks0Filterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *Cryptopunks0Transfer, from []common.Address, to []common.Address) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _Cryptopunks0.contract.WatchLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptopunks0Transfer)
				if err := _Cryptopunks0.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_Cryptopunks0 *Cryptopunks0Filterer) ParseTransfer(log types.Log) (*Cryptopunks0Transfer, error) {
	event := new(Cryptopunks0Transfer)
	if err := _Cryptopunks0.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
