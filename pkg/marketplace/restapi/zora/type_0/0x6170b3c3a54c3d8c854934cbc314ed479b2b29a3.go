// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package zora_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// AsksV11Ask is an auto generated low-level Go binding around an user-defined struct.
type AsksV11Ask struct {
	Seller               common.Address
	SellerFundsRecipient common.Address
	AskCurrency          common.Address
	FindersFeeBps        uint16
	AskPrice             *big.Int
}

// UniversalExchangeEventV1ExchangeDetails is an auto generated low-level Go binding around an user-defined struct.
type UniversalExchangeEventV1ExchangeDetails struct {
	TokenContract common.Address
	TokenId       *big.Int
	Amount        *big.Int
}

// Zora0MetaData contains all meta data concerning the Zora0 contract.
var Zora0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_erc20TransferHelper\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_erc721TransferHelper\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_royaltyEngine\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_protocolFeeSettings\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_wethAddress\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"tokenContract\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"sellerFundsRecipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"askCurrency\",\"type\":\"address\"},{\"internalType\":\"uint16\",\"name\":\"findersFeeBps\",\"type\":\"uint16\"},{\"internalType\":\"uint256\",\"name\":\"askPrice\",\"type\":\"uint256\"}],\"indexed\":false,\"internalType\":\"structAsksV1_1.Ask\",\"name\":\"ask\",\"type\":\"tuple\"}],\"name\":\"AskCanceled\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"tokenContract\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"sellerFundsRecipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"askCurrency\",\"type\":\"address\"},{\"internalType\":\"uint16\",\"name\":\"findersFeeBps\",\"type\":\"uint16\"},{\"internalType\":\"uint256\",\"name\":\"askPrice\",\"type\":\"uint256\"}],\"indexed\":false,\"internalType\":\"structAsksV1_1.Ask\",\"name\":\"ask\",\"type\":\"tuple\"}],\"name\":\"AskCreated\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"tokenContract\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"finder\",\"type\":\"address\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"sellerFundsRecipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"askCurrency\",\"type\":\"address\"},{\"internalType\":\"uint16\",\"name\":\"findersFeeBps\",\"type\":\"uint16\"},{\"internalType\":\"uint256\",\"name\":\"askPrice\",\"type\":\"uint256\"}],\"indexed\":false,\"internalType\":\"structAsksV1_1.Ask\",\"name\":\"ask\",\"type\":\"tuple\"}],\"name\":\"AskFilled\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"tokenContract\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"sellerFundsRecipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"askCurrency\",\"type\":\"address\"},{\"internalType\":\"uint16\",\"name\":\"findersFeeBps\",\"type\":\"uint16\"},{\"internalType\":\"uint256\",\"name\":\"askPrice\",\"type\":\"uint256\"}],\"indexed\":false,\"internalType\":\"structAsksV1_1.Ask\",\"name\":\"ask\",\"type\":\"tuple\"}],\"name\":\"AskPriceUpdated\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"userA\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"userB\",\"type\":\"address\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"tokenContract\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"indexed\":false,\"internalType\":\"structUniversalExchangeEventV1.ExchangeDetails\",\"name\":\"a\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"tokenContract\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"indexed\":false,\"internalType\":\"structUniversalExchangeEventV1.ExchangeDetails\",\"name\":\"b\",\"type\":\"tuple\"}],\"name\":\"ExchangeExecuted\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"tokenContract\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"RoyaltyPayout\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_tokenContract\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_payoutCurrency\",\"type\":\"address\"}],\"name\":\"_handleRoyaltyEnginePayout\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"askForNFT\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"sellerFundsRecipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"askCurrency\",\"type\":\"address\"},{\"internalType\":\"uint16\",\"name\":\"findersFeeBps\",\"type\":\"uint16\"},{\"internalType\":\"uint256\",\"name\":\"askPrice\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_tokenContract\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"cancelAsk\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_tokenContract\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_askPrice\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_askCurrency\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_sellerFundsRecipient\",\"type\":\"address\"},{\"internalType\":\"uint16\",\"name\":\"_findersFeeBps\",\"type\":\"uint16\"}],\"name\":\"createAsk\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"erc20TransferHelper\",\"outputs\":[{\"internalType\":\"contractERC20TransferHelper\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"erc721TransferHelper\",\"outputs\":[{\"internalType\":\"contractERC721TransferHelper\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_tokenContract\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_fillCurrency\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_fillAmount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_finder\",\"type\":\"address\"}],\"name\":\"fillAsk\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"registrar\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_tokenContract\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_askPrice\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_askCurrency\",\"type\":\"address\"}],\"name\":\"setAskPrice\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_royaltyEngine\",\"type\":\"address\"}],\"name\":\"setRoyaltyEngineAddress\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// Zora0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Zora0MetaData.ABI instead.
var Zora0ABI = Zora0MetaData.ABI

// Zora0 is an auto generated Go binding around an Ethereum contract.
type Zora0 struct {
	Zora0Caller     // Read-only binding to the contract
	Zora0Transactor // Write-only binding to the contract
	Zora0Filterer   // Log filterer for contract events
}

// Zora0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Zora0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Zora0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Zora0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Zora0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Zora0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Zora0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Zora0Session struct {
	Contract     *Zora0            // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Zora0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Zora0CallerSession struct {
	Contract *Zora0Caller  // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// Zora0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Zora0TransactorSession struct {
	Contract     *Zora0Transactor  // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Zora0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Zora0Raw struct {
	Contract *Zora0 // Generic contract binding to access the raw methods on
}

// Zora0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Zora0CallerRaw struct {
	Contract *Zora0Caller // Generic read-only contract binding to access the raw methods on
}

// Zora0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Zora0TransactorRaw struct {
	Contract *Zora0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewZora0 creates a new instance of Zora0, bound to a specific deployed contract.
func NewZora0(address common.Address, backend bind.ContractBackend) (*Zora0, error) {
	contract, err := bindZora0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Zora0{Zora0Caller: Zora0Caller{contract: contract}, Zora0Transactor: Zora0Transactor{contract: contract}, Zora0Filterer: Zora0Filterer{contract: contract}}, nil
}

// NewZora0Caller creates a new read-only instance of Zora0, bound to a specific deployed contract.
func NewZora0Caller(address common.Address, caller bind.ContractCaller) (*Zora0Caller, error) {
	contract, err := bindZora0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Zora0Caller{contract: contract}, nil
}

// NewZora0Transactor creates a new write-only instance of Zora0, bound to a specific deployed contract.
func NewZora0Transactor(address common.Address, transactor bind.ContractTransactor) (*Zora0Transactor, error) {
	contract, err := bindZora0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Zora0Transactor{contract: contract}, nil
}

// NewZora0Filterer creates a new log filterer instance of Zora0, bound to a specific deployed contract.
func NewZora0Filterer(address common.Address, filterer bind.ContractFilterer) (*Zora0Filterer, error) {
	contract, err := bindZora0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Zora0Filterer{contract: contract}, nil
}

// bindZora0 binds a generic wrapper to an already deployed contract.
func bindZora0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Zora0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Zora0 *Zora0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Zora0.Contract.Zora0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Zora0 *Zora0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Zora0.Contract.Zora0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Zora0 *Zora0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Zora0.Contract.Zora0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Zora0 *Zora0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Zora0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Zora0 *Zora0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Zora0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Zora0 *Zora0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Zora0.Contract.contract.Transact(opts, method, params...)
}

// AskForNFT is a free data retrieval call binding the contract method 0x418f0656.
//
// Solidity: function askForNFT(address , uint256 ) view returns(address seller, address sellerFundsRecipient, address askCurrency, uint16 findersFeeBps, uint256 askPrice)
func (_Zora0 *Zora0Caller) AskForNFT(opts *bind.CallOpts, arg0 common.Address, arg1 *big.Int) (struct {
	Seller               common.Address
	SellerFundsRecipient common.Address
	AskCurrency          common.Address
	FindersFeeBps        uint16
	AskPrice             *big.Int
}, error) {
	var out []interface{}
	err := _Zora0.contract.Call(opts, &out, "askForNFT", arg0, arg1)

	outstruct := new(struct {
		Seller               common.Address
		SellerFundsRecipient common.Address
		AskCurrency          common.Address
		FindersFeeBps        uint16
		AskPrice             *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Seller = *abi.ConvertType(out[0], new(common.Address)).(*common.Address)
	outstruct.SellerFundsRecipient = *abi.ConvertType(out[1], new(common.Address)).(*common.Address)
	outstruct.AskCurrency = *abi.ConvertType(out[2], new(common.Address)).(*common.Address)
	outstruct.FindersFeeBps = *abi.ConvertType(out[3], new(uint16)).(*uint16)
	outstruct.AskPrice = *abi.ConvertType(out[4], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// AskForNFT is a free data retrieval call binding the contract method 0x418f0656.
//
// Solidity: function askForNFT(address , uint256 ) view returns(address seller, address sellerFundsRecipient, address askCurrency, uint16 findersFeeBps, uint256 askPrice)
func (_Zora0 *Zora0Session) AskForNFT(arg0 common.Address, arg1 *big.Int) (struct {
	Seller               common.Address
	SellerFundsRecipient common.Address
	AskCurrency          common.Address
	FindersFeeBps        uint16
	AskPrice             *big.Int
}, error) {
	return _Zora0.Contract.AskForNFT(&_Zora0.CallOpts, arg0, arg1)
}

// AskForNFT is a free data retrieval call binding the contract method 0x418f0656.
//
// Solidity: function askForNFT(address , uint256 ) view returns(address seller, address sellerFundsRecipient, address askCurrency, uint16 findersFeeBps, uint256 askPrice)
func (_Zora0 *Zora0CallerSession) AskForNFT(arg0 common.Address, arg1 *big.Int) (struct {
	Seller               common.Address
	SellerFundsRecipient common.Address
	AskCurrency          common.Address
	FindersFeeBps        uint16
	AskPrice             *big.Int
}, error) {
	return _Zora0.Contract.AskForNFT(&_Zora0.CallOpts, arg0, arg1)
}

// Erc20TransferHelper is a free data retrieval call binding the contract method 0x8f9d3251.
//
// Solidity: function erc20TransferHelper() view returns(address)
func (_Zora0 *Zora0Caller) Erc20TransferHelper(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Zora0.contract.Call(opts, &out, "erc20TransferHelper")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Erc20TransferHelper is a free data retrieval call binding the contract method 0x8f9d3251.
//
// Solidity: function erc20TransferHelper() view returns(address)
func (_Zora0 *Zora0Session) Erc20TransferHelper() (common.Address, error) {
	return _Zora0.Contract.Erc20TransferHelper(&_Zora0.CallOpts)
}

// Erc20TransferHelper is a free data retrieval call binding the contract method 0x8f9d3251.
//
// Solidity: function erc20TransferHelper() view returns(address)
func (_Zora0 *Zora0CallerSession) Erc20TransferHelper() (common.Address, error) {
	return _Zora0.Contract.Erc20TransferHelper(&_Zora0.CallOpts)
}

// Erc721TransferHelper is a free data retrieval call binding the contract method 0xf7cd1d9b.
//
// Solidity: function erc721TransferHelper() view returns(address)
func (_Zora0 *Zora0Caller) Erc721TransferHelper(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Zora0.contract.Call(opts, &out, "erc721TransferHelper")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Erc721TransferHelper is a free data retrieval call binding the contract method 0xf7cd1d9b.
//
// Solidity: function erc721TransferHelper() view returns(address)
func (_Zora0 *Zora0Session) Erc721TransferHelper() (common.Address, error) {
	return _Zora0.Contract.Erc721TransferHelper(&_Zora0.CallOpts)
}

// Erc721TransferHelper is a free data retrieval call binding the contract method 0xf7cd1d9b.
//
// Solidity: function erc721TransferHelper() view returns(address)
func (_Zora0 *Zora0CallerSession) Erc721TransferHelper() (common.Address, error) {
	return _Zora0.Contract.Erc721TransferHelper(&_Zora0.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Zora0 *Zora0Caller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Zora0.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Zora0 *Zora0Session) Name() (string, error) {
	return _Zora0.Contract.Name(&_Zora0.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Zora0 *Zora0CallerSession) Name() (string, error) {
	return _Zora0.Contract.Name(&_Zora0.CallOpts)
}

// Registrar is a free data retrieval call binding the contract method 0x2b20e397.
//
// Solidity: function registrar() view returns(address)
func (_Zora0 *Zora0Caller) Registrar(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Zora0.contract.Call(opts, &out, "registrar")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Registrar is a free data retrieval call binding the contract method 0x2b20e397.
//
// Solidity: function registrar() view returns(address)
func (_Zora0 *Zora0Session) Registrar() (common.Address, error) {
	return _Zora0.Contract.Registrar(&_Zora0.CallOpts)
}

// Registrar is a free data retrieval call binding the contract method 0x2b20e397.
//
// Solidity: function registrar() view returns(address)
func (_Zora0 *Zora0CallerSession) Registrar() (common.Address, error) {
	return _Zora0.Contract.Registrar(&_Zora0.CallOpts)
}

// HandleRoyaltyEnginePayout is a paid mutator transaction binding the contract method 0x9128c22c.
//
// Solidity: function _handleRoyaltyEnginePayout(address _tokenContract, uint256 _tokenId, uint256 _amount, address _payoutCurrency) payable returns(uint256)
func (_Zora0 *Zora0Transactor) HandleRoyaltyEnginePayout(opts *bind.TransactOpts, _tokenContract common.Address, _tokenId *big.Int, _amount *big.Int, _payoutCurrency common.Address) (*types.Transaction, error) {
	return _Zora0.contract.Transact(opts, "_handleRoyaltyEnginePayout", _tokenContract, _tokenId, _amount, _payoutCurrency)
}

// HandleRoyaltyEnginePayout is a paid mutator transaction binding the contract method 0x9128c22c.
//
// Solidity: function _handleRoyaltyEnginePayout(address _tokenContract, uint256 _tokenId, uint256 _amount, address _payoutCurrency) payable returns(uint256)
func (_Zora0 *Zora0Session) HandleRoyaltyEnginePayout(_tokenContract common.Address, _tokenId *big.Int, _amount *big.Int, _payoutCurrency common.Address) (*types.Transaction, error) {
	return _Zora0.Contract.HandleRoyaltyEnginePayout(&_Zora0.TransactOpts, _tokenContract, _tokenId, _amount, _payoutCurrency)
}

// HandleRoyaltyEnginePayout is a paid mutator transaction binding the contract method 0x9128c22c.
//
// Solidity: function _handleRoyaltyEnginePayout(address _tokenContract, uint256 _tokenId, uint256 _amount, address _payoutCurrency) payable returns(uint256)
func (_Zora0 *Zora0TransactorSession) HandleRoyaltyEnginePayout(_tokenContract common.Address, _tokenId *big.Int, _amount *big.Int, _payoutCurrency common.Address) (*types.Transaction, error) {
	return _Zora0.Contract.HandleRoyaltyEnginePayout(&_Zora0.TransactOpts, _tokenContract, _tokenId, _amount, _payoutCurrency)
}

// CancelAsk is a paid mutator transaction binding the contract method 0x40b80746.
//
// Solidity: function cancelAsk(address _tokenContract, uint256 _tokenId) returns()
func (_Zora0 *Zora0Transactor) CancelAsk(opts *bind.TransactOpts, _tokenContract common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Zora0.contract.Transact(opts, "cancelAsk", _tokenContract, _tokenId)
}

// CancelAsk is a paid mutator transaction binding the contract method 0x40b80746.
//
// Solidity: function cancelAsk(address _tokenContract, uint256 _tokenId) returns()
func (_Zora0 *Zora0Session) CancelAsk(_tokenContract common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Zora0.Contract.CancelAsk(&_Zora0.TransactOpts, _tokenContract, _tokenId)
}

// CancelAsk is a paid mutator transaction binding the contract method 0x40b80746.
//
// Solidity: function cancelAsk(address _tokenContract, uint256 _tokenId) returns()
func (_Zora0 *Zora0TransactorSession) CancelAsk(_tokenContract common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Zora0.Contract.CancelAsk(&_Zora0.TransactOpts, _tokenContract, _tokenId)
}

// CreateAsk is a paid mutator transaction binding the contract method 0x9e847108.
//
// Solidity: function createAsk(address _tokenContract, uint256 _tokenId, uint256 _askPrice, address _askCurrency, address _sellerFundsRecipient, uint16 _findersFeeBps) returns()
func (_Zora0 *Zora0Transactor) CreateAsk(opts *bind.TransactOpts, _tokenContract common.Address, _tokenId *big.Int, _askPrice *big.Int, _askCurrency common.Address, _sellerFundsRecipient common.Address, _findersFeeBps uint16) (*types.Transaction, error) {
	return _Zora0.contract.Transact(opts, "createAsk", _tokenContract, _tokenId, _askPrice, _askCurrency, _sellerFundsRecipient, _findersFeeBps)
}

// CreateAsk is a paid mutator transaction binding the contract method 0x9e847108.
//
// Solidity: function createAsk(address _tokenContract, uint256 _tokenId, uint256 _askPrice, address _askCurrency, address _sellerFundsRecipient, uint16 _findersFeeBps) returns()
func (_Zora0 *Zora0Session) CreateAsk(_tokenContract common.Address, _tokenId *big.Int, _askPrice *big.Int, _askCurrency common.Address, _sellerFundsRecipient common.Address, _findersFeeBps uint16) (*types.Transaction, error) {
	return _Zora0.Contract.CreateAsk(&_Zora0.TransactOpts, _tokenContract, _tokenId, _askPrice, _askCurrency, _sellerFundsRecipient, _findersFeeBps)
}

// CreateAsk is a paid mutator transaction binding the contract method 0x9e847108.
//
// Solidity: function createAsk(address _tokenContract, uint256 _tokenId, uint256 _askPrice, address _askCurrency, address _sellerFundsRecipient, uint16 _findersFeeBps) returns()
func (_Zora0 *Zora0TransactorSession) CreateAsk(_tokenContract common.Address, _tokenId *big.Int, _askPrice *big.Int, _askCurrency common.Address, _sellerFundsRecipient common.Address, _findersFeeBps uint16) (*types.Transaction, error) {
	return _Zora0.Contract.CreateAsk(&_Zora0.TransactOpts, _tokenContract, _tokenId, _askPrice, _askCurrency, _sellerFundsRecipient, _findersFeeBps)
}

// FillAsk is a paid mutator transaction binding the contract method 0x622dcbd7.
//
// Solidity: function fillAsk(address _tokenContract, uint256 _tokenId, address _fillCurrency, uint256 _fillAmount, address _finder) payable returns()
func (_Zora0 *Zora0Transactor) FillAsk(opts *bind.TransactOpts, _tokenContract common.Address, _tokenId *big.Int, _fillCurrency common.Address, _fillAmount *big.Int, _finder common.Address) (*types.Transaction, error) {
	return _Zora0.contract.Transact(opts, "fillAsk", _tokenContract, _tokenId, _fillCurrency, _fillAmount, _finder)
}

// FillAsk is a paid mutator transaction binding the contract method 0x622dcbd7.
//
// Solidity: function fillAsk(address _tokenContract, uint256 _tokenId, address _fillCurrency, uint256 _fillAmount, address _finder) payable returns()
func (_Zora0 *Zora0Session) FillAsk(_tokenContract common.Address, _tokenId *big.Int, _fillCurrency common.Address, _fillAmount *big.Int, _finder common.Address) (*types.Transaction, error) {
	return _Zora0.Contract.FillAsk(&_Zora0.TransactOpts, _tokenContract, _tokenId, _fillCurrency, _fillAmount, _finder)
}

// FillAsk is a paid mutator transaction binding the contract method 0x622dcbd7.
//
// Solidity: function fillAsk(address _tokenContract, uint256 _tokenId, address _fillCurrency, uint256 _fillAmount, address _finder) payable returns()
func (_Zora0 *Zora0TransactorSession) FillAsk(_tokenContract common.Address, _tokenId *big.Int, _fillCurrency common.Address, _fillAmount *big.Int, _finder common.Address) (*types.Transaction, error) {
	return _Zora0.Contract.FillAsk(&_Zora0.TransactOpts, _tokenContract, _tokenId, _fillCurrency, _fillAmount, _finder)
}

// SetAskPrice is a paid mutator transaction binding the contract method 0xb2007533.
//
// Solidity: function setAskPrice(address _tokenContract, uint256 _tokenId, uint256 _askPrice, address _askCurrency) returns()
func (_Zora0 *Zora0Transactor) SetAskPrice(opts *bind.TransactOpts, _tokenContract common.Address, _tokenId *big.Int, _askPrice *big.Int, _askCurrency common.Address) (*types.Transaction, error) {
	return _Zora0.contract.Transact(opts, "setAskPrice", _tokenContract, _tokenId, _askPrice, _askCurrency)
}

// SetAskPrice is a paid mutator transaction binding the contract method 0xb2007533.
//
// Solidity: function setAskPrice(address _tokenContract, uint256 _tokenId, uint256 _askPrice, address _askCurrency) returns()
func (_Zora0 *Zora0Session) SetAskPrice(_tokenContract common.Address, _tokenId *big.Int, _askPrice *big.Int, _askCurrency common.Address) (*types.Transaction, error) {
	return _Zora0.Contract.SetAskPrice(&_Zora0.TransactOpts, _tokenContract, _tokenId, _askPrice, _askCurrency)
}

// SetAskPrice is a paid mutator transaction binding the contract method 0xb2007533.
//
// Solidity: function setAskPrice(address _tokenContract, uint256 _tokenId, uint256 _askPrice, address _askCurrency) returns()
func (_Zora0 *Zora0TransactorSession) SetAskPrice(_tokenContract common.Address, _tokenId *big.Int, _askPrice *big.Int, _askCurrency common.Address) (*types.Transaction, error) {
	return _Zora0.Contract.SetAskPrice(&_Zora0.TransactOpts, _tokenContract, _tokenId, _askPrice, _askCurrency)
}

// SetRoyaltyEngineAddress is a paid mutator transaction binding the contract method 0xb249bb30.
//
// Solidity: function setRoyaltyEngineAddress(address _royaltyEngine) returns()
func (_Zora0 *Zora0Transactor) SetRoyaltyEngineAddress(opts *bind.TransactOpts, _royaltyEngine common.Address) (*types.Transaction, error) {
	return _Zora0.contract.Transact(opts, "setRoyaltyEngineAddress", _royaltyEngine)
}

// SetRoyaltyEngineAddress is a paid mutator transaction binding the contract method 0xb249bb30.
//
// Solidity: function setRoyaltyEngineAddress(address _royaltyEngine) returns()
func (_Zora0 *Zora0Session) SetRoyaltyEngineAddress(_royaltyEngine common.Address) (*types.Transaction, error) {
	return _Zora0.Contract.SetRoyaltyEngineAddress(&_Zora0.TransactOpts, _royaltyEngine)
}

// SetRoyaltyEngineAddress is a paid mutator transaction binding the contract method 0xb249bb30.
//
// Solidity: function setRoyaltyEngineAddress(address _royaltyEngine) returns()
func (_Zora0 *Zora0TransactorSession) SetRoyaltyEngineAddress(_royaltyEngine common.Address) (*types.Transaction, error) {
	return _Zora0.Contract.SetRoyaltyEngineAddress(&_Zora0.TransactOpts, _royaltyEngine)
}

// Zora0AskCanceledIterator is returned from FilterAskCanceled and is used to iterate over the raw logs and unpacked data for AskCanceled events raised by the Zora0 contract.
type Zora0AskCanceledIterator struct {
	Event *Zora0AskCanceled // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Zora0AskCanceledIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Zora0AskCanceled)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Zora0AskCanceled)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Zora0AskCanceledIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Zora0AskCanceledIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Zora0AskCanceled represents a AskCanceled event raised by the Zora0 contract.
type Zora0AskCanceled struct {
	TokenContract common.Address
	TokenId       *big.Int
	Ask           AsksV11Ask
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterAskCanceled is a free log retrieval operation binding the contract event 0x871956abf85befb7c955eacd40fcabe7e01b1702d75764bf7f54bf481933fd35.
//
// Solidity: event AskCanceled(address indexed tokenContract, uint256 indexed tokenId, (address,address,address,uint16,uint256) ask)
func (_Zora0 *Zora0Filterer) FilterAskCanceled(opts *bind.FilterOpts, tokenContract []common.Address, tokenId []*big.Int) (*Zora0AskCanceledIterator, error) {

	var tokenContractRule []interface{}
	for _, tokenContractItem := range tokenContract {
		tokenContractRule = append(tokenContractRule, tokenContractItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Zora0.contract.FilterLogs(opts, "AskCanceled", tokenContractRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Zora0AskCanceledIterator{contract: _Zora0.contract, event: "AskCanceled", logs: logs, sub: sub}, nil
}

// WatchAskCanceled is a free log subscription operation binding the contract event 0x871956abf85befb7c955eacd40fcabe7e01b1702d75764bf7f54bf481933fd35.
//
// Solidity: event AskCanceled(address indexed tokenContract, uint256 indexed tokenId, (address,address,address,uint16,uint256) ask)
func (_Zora0 *Zora0Filterer) WatchAskCanceled(opts *bind.WatchOpts, sink chan<- *Zora0AskCanceled, tokenContract []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var tokenContractRule []interface{}
	for _, tokenContractItem := range tokenContract {
		tokenContractRule = append(tokenContractRule, tokenContractItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Zora0.contract.WatchLogs(opts, "AskCanceled", tokenContractRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Zora0AskCanceled)
				if err := _Zora0.contract.UnpackLog(event, "AskCanceled", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAskCanceled is a log parse operation binding the contract event 0x871956abf85befb7c955eacd40fcabe7e01b1702d75764bf7f54bf481933fd35.
//
// Solidity: event AskCanceled(address indexed tokenContract, uint256 indexed tokenId, (address,address,address,uint16,uint256) ask)
func (_Zora0 *Zora0Filterer) ParseAskCanceled(log types.Log) (*Zora0AskCanceled, error) {
	event := new(Zora0AskCanceled)
	if err := _Zora0.contract.UnpackLog(event, "AskCanceled", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Zora0AskCreatedIterator is returned from FilterAskCreated and is used to iterate over the raw logs and unpacked data for AskCreated events raised by the Zora0 contract.
type Zora0AskCreatedIterator struct {
	Event *Zora0AskCreated // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Zora0AskCreatedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Zora0AskCreated)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Zora0AskCreated)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Zora0AskCreatedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Zora0AskCreatedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Zora0AskCreated represents a AskCreated event raised by the Zora0 contract.
type Zora0AskCreated struct {
	TokenContract common.Address
	TokenId       *big.Int
	Ask           AsksV11Ask
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterAskCreated is a free log retrieval operation binding the contract event 0x5b65b398e1d736436510f4da442eaec71466d2abee0816567088c892c4bcee70.
//
// Solidity: event AskCreated(address indexed tokenContract, uint256 indexed tokenId, (address,address,address,uint16,uint256) ask)
func (_Zora0 *Zora0Filterer) FilterAskCreated(opts *bind.FilterOpts, tokenContract []common.Address, tokenId []*big.Int) (*Zora0AskCreatedIterator, error) {

	var tokenContractRule []interface{}
	for _, tokenContractItem := range tokenContract {
		tokenContractRule = append(tokenContractRule, tokenContractItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Zora0.contract.FilterLogs(opts, "AskCreated", tokenContractRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Zora0AskCreatedIterator{contract: _Zora0.contract, event: "AskCreated", logs: logs, sub: sub}, nil
}

// WatchAskCreated is a free log subscription operation binding the contract event 0x5b65b398e1d736436510f4da442eaec71466d2abee0816567088c892c4bcee70.
//
// Solidity: event AskCreated(address indexed tokenContract, uint256 indexed tokenId, (address,address,address,uint16,uint256) ask)
func (_Zora0 *Zora0Filterer) WatchAskCreated(opts *bind.WatchOpts, sink chan<- *Zora0AskCreated, tokenContract []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var tokenContractRule []interface{}
	for _, tokenContractItem := range tokenContract {
		tokenContractRule = append(tokenContractRule, tokenContractItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Zora0.contract.WatchLogs(opts, "AskCreated", tokenContractRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Zora0AskCreated)
				if err := _Zora0.contract.UnpackLog(event, "AskCreated", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAskCreated is a log parse operation binding the contract event 0x5b65b398e1d736436510f4da442eaec71466d2abee0816567088c892c4bcee70.
//
// Solidity: event AskCreated(address indexed tokenContract, uint256 indexed tokenId, (address,address,address,uint16,uint256) ask)
func (_Zora0 *Zora0Filterer) ParseAskCreated(log types.Log) (*Zora0AskCreated, error) {
	event := new(Zora0AskCreated)
	if err := _Zora0.contract.UnpackLog(event, "AskCreated", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Zora0AskFilledIterator is returned from FilterAskFilled and is used to iterate over the raw logs and unpacked data for AskFilled events raised by the Zora0 contract.
type Zora0AskFilledIterator struct {
	Event *Zora0AskFilled // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Zora0AskFilledIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Zora0AskFilled)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Zora0AskFilled)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Zora0AskFilledIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Zora0AskFilledIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Zora0AskFilled represents a AskFilled event raised by the Zora0 contract.
type Zora0AskFilled struct {
	TokenContract common.Address
	TokenId       *big.Int
	Buyer         common.Address
	Finder        common.Address
	Ask           AsksV11Ask
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterAskFilled is a free log retrieval operation binding the contract event 0x21a9d8e221211780696258a05c6225b1a24f428e2fd4d51708f1ab2be4224d39.
//
// Solidity: event AskFilled(address indexed tokenContract, uint256 indexed tokenId, address indexed buyer, address finder, (address,address,address,uint16,uint256) ask)
func (_Zora0 *Zora0Filterer) FilterAskFilled(opts *bind.FilterOpts, tokenContract []common.Address, tokenId []*big.Int, buyer []common.Address) (*Zora0AskFilledIterator, error) {

	var tokenContractRule []interface{}
	for _, tokenContractItem := range tokenContract {
		tokenContractRule = append(tokenContractRule, tokenContractItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}
	var buyerRule []interface{}
	for _, buyerItem := range buyer {
		buyerRule = append(buyerRule, buyerItem)
	}

	logs, sub, err := _Zora0.contract.FilterLogs(opts, "AskFilled", tokenContractRule, tokenIdRule, buyerRule)
	if err != nil {
		return nil, err
	}
	return &Zora0AskFilledIterator{contract: _Zora0.contract, event: "AskFilled", logs: logs, sub: sub}, nil
}

// WatchAskFilled is a free log subscription operation binding the contract event 0x21a9d8e221211780696258a05c6225b1a24f428e2fd4d51708f1ab2be4224d39.
//
// Solidity: event AskFilled(address indexed tokenContract, uint256 indexed tokenId, address indexed buyer, address finder, (address,address,address,uint16,uint256) ask)
func (_Zora0 *Zora0Filterer) WatchAskFilled(opts *bind.WatchOpts, sink chan<- *Zora0AskFilled, tokenContract []common.Address, tokenId []*big.Int, buyer []common.Address) (event.Subscription, error) {

	var tokenContractRule []interface{}
	for _, tokenContractItem := range tokenContract {
		tokenContractRule = append(tokenContractRule, tokenContractItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}
	var buyerRule []interface{}
	for _, buyerItem := range buyer {
		buyerRule = append(buyerRule, buyerItem)
	}

	logs, sub, err := _Zora0.contract.WatchLogs(opts, "AskFilled", tokenContractRule, tokenIdRule, buyerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Zora0AskFilled)
				if err := _Zora0.contract.UnpackLog(event, "AskFilled", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAskFilled is a log parse operation binding the contract event 0x21a9d8e221211780696258a05c6225b1a24f428e2fd4d51708f1ab2be4224d39.
//
// Solidity: event AskFilled(address indexed tokenContract, uint256 indexed tokenId, address indexed buyer, address finder, (address,address,address,uint16,uint256) ask)
func (_Zora0 *Zora0Filterer) ParseAskFilled(log types.Log) (*Zora0AskFilled, error) {
	event := new(Zora0AskFilled)
	if err := _Zora0.contract.UnpackLog(event, "AskFilled", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Zora0AskPriceUpdatedIterator is returned from FilterAskPriceUpdated and is used to iterate over the raw logs and unpacked data for AskPriceUpdated events raised by the Zora0 contract.
type Zora0AskPriceUpdatedIterator struct {
	Event *Zora0AskPriceUpdated // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Zora0AskPriceUpdatedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Zora0AskPriceUpdated)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Zora0AskPriceUpdated)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Zora0AskPriceUpdatedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Zora0AskPriceUpdatedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Zora0AskPriceUpdated represents a AskPriceUpdated event raised by the Zora0 contract.
type Zora0AskPriceUpdated struct {
	TokenContract common.Address
	TokenId       *big.Int
	Ask           AsksV11Ask
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterAskPriceUpdated is a free log retrieval operation binding the contract event 0x1a24bcf5290feab70f35cfb4870c294ebf497e608d4262b0ec0debe045008140.
//
// Solidity: event AskPriceUpdated(address indexed tokenContract, uint256 indexed tokenId, (address,address,address,uint16,uint256) ask)
func (_Zora0 *Zora0Filterer) FilterAskPriceUpdated(opts *bind.FilterOpts, tokenContract []common.Address, tokenId []*big.Int) (*Zora0AskPriceUpdatedIterator, error) {

	var tokenContractRule []interface{}
	for _, tokenContractItem := range tokenContract {
		tokenContractRule = append(tokenContractRule, tokenContractItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Zora0.contract.FilterLogs(opts, "AskPriceUpdated", tokenContractRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Zora0AskPriceUpdatedIterator{contract: _Zora0.contract, event: "AskPriceUpdated", logs: logs, sub: sub}, nil
}

// WatchAskPriceUpdated is a free log subscription operation binding the contract event 0x1a24bcf5290feab70f35cfb4870c294ebf497e608d4262b0ec0debe045008140.
//
// Solidity: event AskPriceUpdated(address indexed tokenContract, uint256 indexed tokenId, (address,address,address,uint16,uint256) ask)
func (_Zora0 *Zora0Filterer) WatchAskPriceUpdated(opts *bind.WatchOpts, sink chan<- *Zora0AskPriceUpdated, tokenContract []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var tokenContractRule []interface{}
	for _, tokenContractItem := range tokenContract {
		tokenContractRule = append(tokenContractRule, tokenContractItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Zora0.contract.WatchLogs(opts, "AskPriceUpdated", tokenContractRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Zora0AskPriceUpdated)
				if err := _Zora0.contract.UnpackLog(event, "AskPriceUpdated", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAskPriceUpdated is a log parse operation binding the contract event 0x1a24bcf5290feab70f35cfb4870c294ebf497e608d4262b0ec0debe045008140.
//
// Solidity: event AskPriceUpdated(address indexed tokenContract, uint256 indexed tokenId, (address,address,address,uint16,uint256) ask)
func (_Zora0 *Zora0Filterer) ParseAskPriceUpdated(log types.Log) (*Zora0AskPriceUpdated, error) {
	event := new(Zora0AskPriceUpdated)
	if err := _Zora0.contract.UnpackLog(event, "AskPriceUpdated", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Zora0ExchangeExecutedIterator is returned from FilterExchangeExecuted and is used to iterate over the raw logs and unpacked data for ExchangeExecuted events raised by the Zora0 contract.
type Zora0ExchangeExecutedIterator struct {
	Event *Zora0ExchangeExecuted // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Zora0ExchangeExecutedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Zora0ExchangeExecuted)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Zora0ExchangeExecuted)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Zora0ExchangeExecutedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Zora0ExchangeExecutedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Zora0ExchangeExecuted represents a ExchangeExecuted event raised by the Zora0 contract.
type Zora0ExchangeExecuted struct {
	UserA common.Address
	UserB common.Address
	A     UniversalExchangeEventV1ExchangeDetails
	B     UniversalExchangeEventV1ExchangeDetails
	Raw   types.Log // Blockchain specific contextual infos
}

// FilterExchangeExecuted is a free log retrieval operation binding the contract event 0x1f432c9454edd444f55492be01e3fa82aa88bfa28e120a039be204253c10c36e.
//
// Solidity: event ExchangeExecuted(address indexed userA, address indexed userB, (address,uint256,uint256) a, (address,uint256,uint256) b)
func (_Zora0 *Zora0Filterer) FilterExchangeExecuted(opts *bind.FilterOpts, userA []common.Address, userB []common.Address) (*Zora0ExchangeExecutedIterator, error) {

	var userARule []interface{}
	for _, userAItem := range userA {
		userARule = append(userARule, userAItem)
	}
	var userBRule []interface{}
	for _, userBItem := range userB {
		userBRule = append(userBRule, userBItem)
	}

	logs, sub, err := _Zora0.contract.FilterLogs(opts, "ExchangeExecuted", userARule, userBRule)
	if err != nil {
		return nil, err
	}
	return &Zora0ExchangeExecutedIterator{contract: _Zora0.contract, event: "ExchangeExecuted", logs: logs, sub: sub}, nil
}

// WatchExchangeExecuted is a free log subscription operation binding the contract event 0x1f432c9454edd444f55492be01e3fa82aa88bfa28e120a039be204253c10c36e.
//
// Solidity: event ExchangeExecuted(address indexed userA, address indexed userB, (address,uint256,uint256) a, (address,uint256,uint256) b)
func (_Zora0 *Zora0Filterer) WatchExchangeExecuted(opts *bind.WatchOpts, sink chan<- *Zora0ExchangeExecuted, userA []common.Address, userB []common.Address) (event.Subscription, error) {

	var userARule []interface{}
	for _, userAItem := range userA {
		userARule = append(userARule, userAItem)
	}
	var userBRule []interface{}
	for _, userBItem := range userB {
		userBRule = append(userBRule, userBItem)
	}

	logs, sub, err := _Zora0.contract.WatchLogs(opts, "ExchangeExecuted", userARule, userBRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Zora0ExchangeExecuted)
				if err := _Zora0.contract.UnpackLog(event, "ExchangeExecuted", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseExchangeExecuted is a log parse operation binding the contract event 0x1f432c9454edd444f55492be01e3fa82aa88bfa28e120a039be204253c10c36e.
//
// Solidity: event ExchangeExecuted(address indexed userA, address indexed userB, (address,uint256,uint256) a, (address,uint256,uint256) b)
func (_Zora0 *Zora0Filterer) ParseExchangeExecuted(log types.Log) (*Zora0ExchangeExecuted, error) {
	event := new(Zora0ExchangeExecuted)
	if err := _Zora0.contract.UnpackLog(event, "ExchangeExecuted", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Zora0RoyaltyPayoutIterator is returned from FilterRoyaltyPayout and is used to iterate over the raw logs and unpacked data for RoyaltyPayout events raised by the Zora0 contract.
type Zora0RoyaltyPayoutIterator struct {
	Event *Zora0RoyaltyPayout // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Zora0RoyaltyPayoutIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Zora0RoyaltyPayout)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Zora0RoyaltyPayout)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Zora0RoyaltyPayoutIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Zora0RoyaltyPayoutIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Zora0RoyaltyPayout represents a RoyaltyPayout event raised by the Zora0 contract.
type Zora0RoyaltyPayout struct {
	TokenContract common.Address
	TokenId       *big.Int
	Recipient     common.Address
	Amount        *big.Int
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterRoyaltyPayout is a free log retrieval operation binding the contract event 0x866e6ef8682ddf5f1025e64dfdb45527077f7be70fa9ef680b7ffd8cf4ab9c50.
//
// Solidity: event RoyaltyPayout(address indexed tokenContract, uint256 indexed tokenId, address indexed recipient, uint256 amount)
func (_Zora0 *Zora0Filterer) FilterRoyaltyPayout(opts *bind.FilterOpts, tokenContract []common.Address, tokenId []*big.Int, recipient []common.Address) (*Zora0RoyaltyPayoutIterator, error) {

	var tokenContractRule []interface{}
	for _, tokenContractItem := range tokenContract {
		tokenContractRule = append(tokenContractRule, tokenContractItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}
	var recipientRule []interface{}
	for _, recipientItem := range recipient {
		recipientRule = append(recipientRule, recipientItem)
	}

	logs, sub, err := _Zora0.contract.FilterLogs(opts, "RoyaltyPayout", tokenContractRule, tokenIdRule, recipientRule)
	if err != nil {
		return nil, err
	}
	return &Zora0RoyaltyPayoutIterator{contract: _Zora0.contract, event: "RoyaltyPayout", logs: logs, sub: sub}, nil
}

// WatchRoyaltyPayout is a free log subscription operation binding the contract event 0x866e6ef8682ddf5f1025e64dfdb45527077f7be70fa9ef680b7ffd8cf4ab9c50.
//
// Solidity: event RoyaltyPayout(address indexed tokenContract, uint256 indexed tokenId, address indexed recipient, uint256 amount)
func (_Zora0 *Zora0Filterer) WatchRoyaltyPayout(opts *bind.WatchOpts, sink chan<- *Zora0RoyaltyPayout, tokenContract []common.Address, tokenId []*big.Int, recipient []common.Address) (event.Subscription, error) {

	var tokenContractRule []interface{}
	for _, tokenContractItem := range tokenContract {
		tokenContractRule = append(tokenContractRule, tokenContractItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}
	var recipientRule []interface{}
	for _, recipientItem := range recipient {
		recipientRule = append(recipientRule, recipientItem)
	}

	logs, sub, err := _Zora0.contract.WatchLogs(opts, "RoyaltyPayout", tokenContractRule, tokenIdRule, recipientRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Zora0RoyaltyPayout)
				if err := _Zora0.contract.UnpackLog(event, "RoyaltyPayout", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseRoyaltyPayout is a log parse operation binding the contract event 0x866e6ef8682ddf5f1025e64dfdb45527077f7be70fa9ef680b7ffd8cf4ab9c50.
//
// Solidity: event RoyaltyPayout(address indexed tokenContract, uint256 indexed tokenId, address indexed recipient, uint256 amount)
func (_Zora0 *Zora0Filterer) ParseRoyaltyPayout(log types.Log) (*Zora0RoyaltyPayout, error) {
	event := new(Zora0RoyaltyPayout)
	if err := _Zora0.contract.UnpackLog(event, "RoyaltyPayout", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
