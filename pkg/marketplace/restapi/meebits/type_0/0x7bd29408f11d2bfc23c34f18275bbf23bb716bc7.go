// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package meebits_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Meebits0MetaData contains all meta data concerning the Meebits0 contract.
var Meebits0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_punks\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_glyphs\",\"type\":\"address\"},{\"internalType\":\"addresspayable\",\"name\":\"_beneficiary\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"approved\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"Approval\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"approved\",\"type\":\"bool\"}],\"name\":\"ApprovalForAll\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[],\"name\":\"CommunityGrantEnds\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Deposit\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"minter\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"createdVia\",\"type\":\"uint256\"}],\"name\":\"Mint\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"hash\",\"type\":\"bytes32\"}],\"name\":\"OfferCancelled\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[],\"name\":\"SaleBegins\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"bytes32\",\"name\":\"hash\",\"type\":\"bytes32\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"maker\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"taker\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"makerWei\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256[]\",\"name\":\"makerIds\",\"type\":\"uint256[]\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"takerWei\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256[]\",\"name\":\"takerIds\",\"type\":\"uint256[]\"}],\"name\":\"Trade\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Withdraw\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"SALE_LIMIT\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"TOKEN_LIMIT\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"maker\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"taker\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"makerWei\",\"type\":\"uint256\"},{\"internalType\":\"uint256[]\",\"name\":\"makerIds\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256\",\"name\":\"takerWei\",\"type\":\"uint256\"},{\"internalType\":\"uint256[]\",\"name\":\"takerIds\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256\",\"name\":\"expiry\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"signature\",\"type\":\"bytes\"}],\"name\":\"acceptTrade\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_approved\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_owner\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"maker\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"taker\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"makerWei\",\"type\":\"uint256\"},{\"internalType\":\"uint256[]\",\"name\":\"makerIds\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256\",\"name\":\"takerWei\",\"type\":\"uint256\"},{\"internalType\":\"uint256[]\",\"name\":\"takerIds\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256\",\"name\":\"expiry\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"}],\"name\":\"cancelOffer\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"name\":\"cancelledOffers\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"communityGrant\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"contentHash\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"contractSealed\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"creatorNftMints\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"deposit\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"quantity\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"devMint\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"endCommunityGrant\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"ethBalance\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"getApproved\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getPrice\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"maker\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"taker\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"makerWei\",\"type\":\"uint256\"},{\"internalType\":\"uint256[]\",\"name\":\"makerIds\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256\",\"name\":\"takerWei\",\"type\":\"uint256\"},{\"internalType\":\"uint256[]\",\"name\":\"takerIds\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256\",\"name\":\"expiry\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"}],\"name\":\"hashToSign\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_owner\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_operator\",\"type\":\"address\"}],\"name\":\"isApprovedForAll\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"marketPaused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"mint\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_createVia\",\"type\":\"uint256\"}],\"name\":\"mintWithPunkOrGlyph\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"mintsRemaining\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"_name\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"ownerOf\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"_owner\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bool\",\"name\":\"_paused\",\"type\":\"bool\"}],\"name\":\"pauseMarket\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"publicSale\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"safeTransferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"_data\",\"type\":\"bytes\"}],\"name\":\"safeTransferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"saleDuration\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"saleStartTime\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"sealContract\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_operator\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_approved\",\"type\":\"bool\"}],\"name\":\"setApprovalForAll\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_saleDuration\",\"type\":\"uint256\"}],\"name\":\"startSale\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes4\",\"name\":\"_interfaceID\",\"type\":\"bytes4\"}],\"name\":\"supportsInterface\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"_symbol\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"tokenByIndex\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_owner\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_index\",\"type\":\"uint256\"}],\"name\":\"tokenOfOwnerByIndex\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"tokenURI\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"maker\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"taker\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"makerWei\",\"type\":\"uint256\"},{\"internalType\":\"uint256[]\",\"name\":\"makerIds\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256\",\"name\":\"takerWei\",\"type\":\"uint256\"},{\"internalType\":\"uint256[]\",\"name\":\"takerIds\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256\",\"name\":\"expiry\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"signature\",\"type\":\"bytes\"}],\"name\":\"tradeValid\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"transferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// Meebits0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Meebits0MetaData.ABI instead.
var Meebits0ABI = Meebits0MetaData.ABI

// Meebits0 is an auto generated Go binding around an Ethereum contract.
type Meebits0 struct {
	Meebits0Caller     // Read-only binding to the contract
	Meebits0Transactor // Write-only binding to the contract
	Meebits0Filterer   // Log filterer for contract events
}

// Meebits0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Meebits0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Meebits0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Meebits0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Meebits0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Meebits0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Meebits0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Meebits0Session struct {
	Contract     *Meebits0         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Meebits0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Meebits0CallerSession struct {
	Contract *Meebits0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// Meebits0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Meebits0TransactorSession struct {
	Contract     *Meebits0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// Meebits0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Meebits0Raw struct {
	Contract *Meebits0 // Generic contract binding to access the raw methods on
}

// Meebits0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Meebits0CallerRaw struct {
	Contract *Meebits0Caller // Generic read-only contract binding to access the raw methods on
}

// Meebits0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Meebits0TransactorRaw struct {
	Contract *Meebits0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewMeebits0 creates a new instance of Meebits0, bound to a specific deployed contract.
func NewMeebits0(address common.Address, backend bind.ContractBackend) (*Meebits0, error) {
	contract, err := bindMeebits0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Meebits0{Meebits0Caller: Meebits0Caller{contract: contract}, Meebits0Transactor: Meebits0Transactor{contract: contract}, Meebits0Filterer: Meebits0Filterer{contract: contract}}, nil
}

// NewMeebits0Caller creates a new read-only instance of Meebits0, bound to a specific deployed contract.
func NewMeebits0Caller(address common.Address, caller bind.ContractCaller) (*Meebits0Caller, error) {
	contract, err := bindMeebits0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Meebits0Caller{contract: contract}, nil
}

// NewMeebits0Transactor creates a new write-only instance of Meebits0, bound to a specific deployed contract.
func NewMeebits0Transactor(address common.Address, transactor bind.ContractTransactor) (*Meebits0Transactor, error) {
	contract, err := bindMeebits0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Meebits0Transactor{contract: contract}, nil
}

// NewMeebits0Filterer creates a new log filterer instance of Meebits0, bound to a specific deployed contract.
func NewMeebits0Filterer(address common.Address, filterer bind.ContractFilterer) (*Meebits0Filterer, error) {
	contract, err := bindMeebits0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Meebits0Filterer{contract: contract}, nil
}

// bindMeebits0 binds a generic wrapper to an already deployed contract.
func bindMeebits0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Meebits0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Meebits0 *Meebits0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Meebits0.Contract.Meebits0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Meebits0 *Meebits0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Meebits0.Contract.Meebits0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Meebits0 *Meebits0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Meebits0.Contract.Meebits0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Meebits0 *Meebits0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Meebits0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Meebits0 *Meebits0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Meebits0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Meebits0 *Meebits0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Meebits0.Contract.contract.Transact(opts, method, params...)
}

// SALELIMIT is a free data retrieval call binding the contract method 0x212e8f67.
//
// Solidity: function SALE_LIMIT() view returns(uint256)
func (_Meebits0 *Meebits0Caller) SALELIMIT(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "SALE_LIMIT")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// SALELIMIT is a free data retrieval call binding the contract method 0x212e8f67.
//
// Solidity: function SALE_LIMIT() view returns(uint256)
func (_Meebits0 *Meebits0Session) SALELIMIT() (*big.Int, error) {
	return _Meebits0.Contract.SALELIMIT(&_Meebits0.CallOpts)
}

// SALELIMIT is a free data retrieval call binding the contract method 0x212e8f67.
//
// Solidity: function SALE_LIMIT() view returns(uint256)
func (_Meebits0 *Meebits0CallerSession) SALELIMIT() (*big.Int, error) {
	return _Meebits0.Contract.SALELIMIT(&_Meebits0.CallOpts)
}

// TOKENLIMIT is a free data retrieval call binding the contract method 0x031bd4c4.
//
// Solidity: function TOKEN_LIMIT() view returns(uint256)
func (_Meebits0 *Meebits0Caller) TOKENLIMIT(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "TOKEN_LIMIT")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TOKENLIMIT is a free data retrieval call binding the contract method 0x031bd4c4.
//
// Solidity: function TOKEN_LIMIT() view returns(uint256)
func (_Meebits0 *Meebits0Session) TOKENLIMIT() (*big.Int, error) {
	return _Meebits0.Contract.TOKENLIMIT(&_Meebits0.CallOpts)
}

// TOKENLIMIT is a free data retrieval call binding the contract method 0x031bd4c4.
//
// Solidity: function TOKEN_LIMIT() view returns(uint256)
func (_Meebits0 *Meebits0CallerSession) TOKENLIMIT() (*big.Int, error) {
	return _Meebits0.Contract.TOKENLIMIT(&_Meebits0.CallOpts)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address _owner) view returns(uint256)
func (_Meebits0 *Meebits0Caller) BalanceOf(opts *bind.CallOpts, _owner common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "balanceOf", _owner)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address _owner) view returns(uint256)
func (_Meebits0 *Meebits0Session) BalanceOf(_owner common.Address) (*big.Int, error) {
	return _Meebits0.Contract.BalanceOf(&_Meebits0.CallOpts, _owner)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address _owner) view returns(uint256)
func (_Meebits0 *Meebits0CallerSession) BalanceOf(_owner common.Address) (*big.Int, error) {
	return _Meebits0.Contract.BalanceOf(&_Meebits0.CallOpts, _owner)
}

// CancelledOffers is a free data retrieval call binding the contract method 0x9f7bf762.
//
// Solidity: function cancelledOffers(bytes32 ) view returns(bool)
func (_Meebits0 *Meebits0Caller) CancelledOffers(opts *bind.CallOpts, arg0 [32]byte) (bool, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "cancelledOffers", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// CancelledOffers is a free data retrieval call binding the contract method 0x9f7bf762.
//
// Solidity: function cancelledOffers(bytes32 ) view returns(bool)
func (_Meebits0 *Meebits0Session) CancelledOffers(arg0 [32]byte) (bool, error) {
	return _Meebits0.Contract.CancelledOffers(&_Meebits0.CallOpts, arg0)
}

// CancelledOffers is a free data retrieval call binding the contract method 0x9f7bf762.
//
// Solidity: function cancelledOffers(bytes32 ) view returns(bool)
func (_Meebits0 *Meebits0CallerSession) CancelledOffers(arg0 [32]byte) (bool, error) {
	return _Meebits0.Contract.CancelledOffers(&_Meebits0.CallOpts, arg0)
}

// CommunityGrant is a free data retrieval call binding the contract method 0x0614a6ba.
//
// Solidity: function communityGrant() view returns(bool)
func (_Meebits0 *Meebits0Caller) CommunityGrant(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "communityGrant")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// CommunityGrant is a free data retrieval call binding the contract method 0x0614a6ba.
//
// Solidity: function communityGrant() view returns(bool)
func (_Meebits0 *Meebits0Session) CommunityGrant() (bool, error) {
	return _Meebits0.Contract.CommunityGrant(&_Meebits0.CallOpts)
}

// CommunityGrant is a free data retrieval call binding the contract method 0x0614a6ba.
//
// Solidity: function communityGrant() view returns(bool)
func (_Meebits0 *Meebits0CallerSession) CommunityGrant() (bool, error) {
	return _Meebits0.Contract.CommunityGrant(&_Meebits0.CallOpts)
}

// ContentHash is a free data retrieval call binding the contract method 0x646c2e33.
//
// Solidity: function contentHash() view returns(string)
func (_Meebits0 *Meebits0Caller) ContentHash(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "contentHash")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// ContentHash is a free data retrieval call binding the contract method 0x646c2e33.
//
// Solidity: function contentHash() view returns(string)
func (_Meebits0 *Meebits0Session) ContentHash() (string, error) {
	return _Meebits0.Contract.ContentHash(&_Meebits0.CallOpts)
}

// ContentHash is a free data retrieval call binding the contract method 0x646c2e33.
//
// Solidity: function contentHash() view returns(string)
func (_Meebits0 *Meebits0CallerSession) ContentHash() (string, error) {
	return _Meebits0.Contract.ContentHash(&_Meebits0.CallOpts)
}

// ContractSealed is a free data retrieval call binding the contract method 0xb6501637.
//
// Solidity: function contractSealed() view returns(bool)
func (_Meebits0 *Meebits0Caller) ContractSealed(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "contractSealed")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// ContractSealed is a free data retrieval call binding the contract method 0xb6501637.
//
// Solidity: function contractSealed() view returns(bool)
func (_Meebits0 *Meebits0Session) ContractSealed() (bool, error) {
	return _Meebits0.Contract.ContractSealed(&_Meebits0.CallOpts)
}

// ContractSealed is a free data retrieval call binding the contract method 0xb6501637.
//
// Solidity: function contractSealed() view returns(bool)
func (_Meebits0 *Meebits0CallerSession) ContractSealed() (bool, error) {
	return _Meebits0.Contract.ContractSealed(&_Meebits0.CallOpts)
}

// CreatorNftMints is a free data retrieval call binding the contract method 0xb9f7e218.
//
// Solidity: function creatorNftMints(uint256 ) view returns(uint256)
func (_Meebits0 *Meebits0Caller) CreatorNftMints(opts *bind.CallOpts, arg0 *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "creatorNftMints", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CreatorNftMints is a free data retrieval call binding the contract method 0xb9f7e218.
//
// Solidity: function creatorNftMints(uint256 ) view returns(uint256)
func (_Meebits0 *Meebits0Session) CreatorNftMints(arg0 *big.Int) (*big.Int, error) {
	return _Meebits0.Contract.CreatorNftMints(&_Meebits0.CallOpts, arg0)
}

// CreatorNftMints is a free data retrieval call binding the contract method 0xb9f7e218.
//
// Solidity: function creatorNftMints(uint256 ) view returns(uint256)
func (_Meebits0 *Meebits0CallerSession) CreatorNftMints(arg0 *big.Int) (*big.Int, error) {
	return _Meebits0.Contract.CreatorNftMints(&_Meebits0.CallOpts, arg0)
}

// EthBalance is a free data retrieval call binding the contract method 0xd8f3790f.
//
// Solidity: function ethBalance(address ) view returns(uint256)
func (_Meebits0 *Meebits0Caller) EthBalance(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "ethBalance", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// EthBalance is a free data retrieval call binding the contract method 0xd8f3790f.
//
// Solidity: function ethBalance(address ) view returns(uint256)
func (_Meebits0 *Meebits0Session) EthBalance(arg0 common.Address) (*big.Int, error) {
	return _Meebits0.Contract.EthBalance(&_Meebits0.CallOpts, arg0)
}

// EthBalance is a free data retrieval call binding the contract method 0xd8f3790f.
//
// Solidity: function ethBalance(address ) view returns(uint256)
func (_Meebits0 *Meebits0CallerSession) EthBalance(arg0 common.Address) (*big.Int, error) {
	return _Meebits0.Contract.EthBalance(&_Meebits0.CallOpts, arg0)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 _tokenId) view returns(address)
func (_Meebits0 *Meebits0Caller) GetApproved(opts *bind.CallOpts, _tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "getApproved", _tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 _tokenId) view returns(address)
func (_Meebits0 *Meebits0Session) GetApproved(_tokenId *big.Int) (common.Address, error) {
	return _Meebits0.Contract.GetApproved(&_Meebits0.CallOpts, _tokenId)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 _tokenId) view returns(address)
func (_Meebits0 *Meebits0CallerSession) GetApproved(_tokenId *big.Int) (common.Address, error) {
	return _Meebits0.Contract.GetApproved(&_Meebits0.CallOpts, _tokenId)
}

// GetPrice is a free data retrieval call binding the contract method 0x98d5fdca.
//
// Solidity: function getPrice() view returns(uint256)
func (_Meebits0 *Meebits0Caller) GetPrice(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "getPrice")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetPrice is a free data retrieval call binding the contract method 0x98d5fdca.
//
// Solidity: function getPrice() view returns(uint256)
func (_Meebits0 *Meebits0Session) GetPrice() (*big.Int, error) {
	return _Meebits0.Contract.GetPrice(&_Meebits0.CallOpts)
}

// GetPrice is a free data retrieval call binding the contract method 0x98d5fdca.
//
// Solidity: function getPrice() view returns(uint256)
func (_Meebits0 *Meebits0CallerSession) GetPrice() (*big.Int, error) {
	return _Meebits0.Contract.GetPrice(&_Meebits0.CallOpts)
}

// HashToSign is a free data retrieval call binding the contract method 0xf592934c.
//
// Solidity: function hashToSign(address maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds, uint256 expiry, uint256 salt) pure returns(bytes32)
func (_Meebits0 *Meebits0Caller) HashToSign(opts *bind.CallOpts, maker common.Address, taker common.Address, makerWei *big.Int, makerIds []*big.Int, takerWei *big.Int, takerIds []*big.Int, expiry *big.Int, salt *big.Int) ([32]byte, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "hashToSign", maker, taker, makerWei, makerIds, takerWei, takerIds, expiry, salt)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// HashToSign is a free data retrieval call binding the contract method 0xf592934c.
//
// Solidity: function hashToSign(address maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds, uint256 expiry, uint256 salt) pure returns(bytes32)
func (_Meebits0 *Meebits0Session) HashToSign(maker common.Address, taker common.Address, makerWei *big.Int, makerIds []*big.Int, takerWei *big.Int, takerIds []*big.Int, expiry *big.Int, salt *big.Int) ([32]byte, error) {
	return _Meebits0.Contract.HashToSign(&_Meebits0.CallOpts, maker, taker, makerWei, makerIds, takerWei, takerIds, expiry, salt)
}

// HashToSign is a free data retrieval call binding the contract method 0xf592934c.
//
// Solidity: function hashToSign(address maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds, uint256 expiry, uint256 salt) pure returns(bytes32)
func (_Meebits0 *Meebits0CallerSession) HashToSign(maker common.Address, taker common.Address, makerWei *big.Int, makerIds []*big.Int, takerWei *big.Int, takerIds []*big.Int, expiry *big.Int, salt *big.Int) ([32]byte, error) {
	return _Meebits0.Contract.HashToSign(&_Meebits0.CallOpts, maker, taker, makerWei, makerIds, takerWei, takerIds, expiry, salt)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address _owner, address _operator) view returns(bool)
func (_Meebits0 *Meebits0Caller) IsApprovedForAll(opts *bind.CallOpts, _owner common.Address, _operator common.Address) (bool, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "isApprovedForAll", _owner, _operator)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address _owner, address _operator) view returns(bool)
func (_Meebits0 *Meebits0Session) IsApprovedForAll(_owner common.Address, _operator common.Address) (bool, error) {
	return _Meebits0.Contract.IsApprovedForAll(&_Meebits0.CallOpts, _owner, _operator)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address _owner, address _operator) view returns(bool)
func (_Meebits0 *Meebits0CallerSession) IsApprovedForAll(_owner common.Address, _operator common.Address) (bool, error) {
	return _Meebits0.Contract.IsApprovedForAll(&_Meebits0.CallOpts, _owner, _operator)
}

// MarketPaused is a free data retrieval call binding the contract method 0x3a283bd2.
//
// Solidity: function marketPaused() view returns(bool)
func (_Meebits0 *Meebits0Caller) MarketPaused(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "marketPaused")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// MarketPaused is a free data retrieval call binding the contract method 0x3a283bd2.
//
// Solidity: function marketPaused() view returns(bool)
func (_Meebits0 *Meebits0Session) MarketPaused() (bool, error) {
	return _Meebits0.Contract.MarketPaused(&_Meebits0.CallOpts)
}

// MarketPaused is a free data retrieval call binding the contract method 0x3a283bd2.
//
// Solidity: function marketPaused() view returns(bool)
func (_Meebits0 *Meebits0CallerSession) MarketPaused() (bool, error) {
	return _Meebits0.Contract.MarketPaused(&_Meebits0.CallOpts)
}

// MintsRemaining is a free data retrieval call binding the contract method 0x44c66be7.
//
// Solidity: function mintsRemaining() view returns(uint256)
func (_Meebits0 *Meebits0Caller) MintsRemaining(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "mintsRemaining")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MintsRemaining is a free data retrieval call binding the contract method 0x44c66be7.
//
// Solidity: function mintsRemaining() view returns(uint256)
func (_Meebits0 *Meebits0Session) MintsRemaining() (*big.Int, error) {
	return _Meebits0.Contract.MintsRemaining(&_Meebits0.CallOpts)
}

// MintsRemaining is a free data retrieval call binding the contract method 0x44c66be7.
//
// Solidity: function mintsRemaining() view returns(uint256)
func (_Meebits0 *Meebits0CallerSession) MintsRemaining() (*big.Int, error) {
	return _Meebits0.Contract.MintsRemaining(&_Meebits0.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string _name)
func (_Meebits0 *Meebits0Caller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string _name)
func (_Meebits0 *Meebits0Session) Name() (string, error) {
	return _Meebits0.Contract.Name(&_Meebits0.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string _name)
func (_Meebits0 *Meebits0CallerSession) Name() (string, error) {
	return _Meebits0.Contract.Name(&_Meebits0.CallOpts)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 _tokenId) view returns(address _owner)
func (_Meebits0 *Meebits0Caller) OwnerOf(opts *bind.CallOpts, _tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "ownerOf", _tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 _tokenId) view returns(address _owner)
func (_Meebits0 *Meebits0Session) OwnerOf(_tokenId *big.Int) (common.Address, error) {
	return _Meebits0.Contract.OwnerOf(&_Meebits0.CallOpts, _tokenId)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 _tokenId) view returns(address _owner)
func (_Meebits0 *Meebits0CallerSession) OwnerOf(_tokenId *big.Int) (common.Address, error) {
	return _Meebits0.Contract.OwnerOf(&_Meebits0.CallOpts, _tokenId)
}

// PublicSale is a free data retrieval call binding the contract method 0x33bc1c5c.
//
// Solidity: function publicSale() view returns(bool)
func (_Meebits0 *Meebits0Caller) PublicSale(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "publicSale")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// PublicSale is a free data retrieval call binding the contract method 0x33bc1c5c.
//
// Solidity: function publicSale() view returns(bool)
func (_Meebits0 *Meebits0Session) PublicSale() (bool, error) {
	return _Meebits0.Contract.PublicSale(&_Meebits0.CallOpts)
}

// PublicSale is a free data retrieval call binding the contract method 0x33bc1c5c.
//
// Solidity: function publicSale() view returns(bool)
func (_Meebits0 *Meebits0CallerSession) PublicSale() (bool, error) {
	return _Meebits0.Contract.PublicSale(&_Meebits0.CallOpts)
}

// SaleDuration is a free data retrieval call binding the contract method 0x3711d9fb.
//
// Solidity: function saleDuration() view returns(uint256)
func (_Meebits0 *Meebits0Caller) SaleDuration(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "saleDuration")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// SaleDuration is a free data retrieval call binding the contract method 0x3711d9fb.
//
// Solidity: function saleDuration() view returns(uint256)
func (_Meebits0 *Meebits0Session) SaleDuration() (*big.Int, error) {
	return _Meebits0.Contract.SaleDuration(&_Meebits0.CallOpts)
}

// SaleDuration is a free data retrieval call binding the contract method 0x3711d9fb.
//
// Solidity: function saleDuration() view returns(uint256)
func (_Meebits0 *Meebits0CallerSession) SaleDuration() (*big.Int, error) {
	return _Meebits0.Contract.SaleDuration(&_Meebits0.CallOpts)
}

// SaleStartTime is a free data retrieval call binding the contract method 0x1cbaee2d.
//
// Solidity: function saleStartTime() view returns(uint256)
func (_Meebits0 *Meebits0Caller) SaleStartTime(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "saleStartTime")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// SaleStartTime is a free data retrieval call binding the contract method 0x1cbaee2d.
//
// Solidity: function saleStartTime() view returns(uint256)
func (_Meebits0 *Meebits0Session) SaleStartTime() (*big.Int, error) {
	return _Meebits0.Contract.SaleStartTime(&_Meebits0.CallOpts)
}

// SaleStartTime is a free data retrieval call binding the contract method 0x1cbaee2d.
//
// Solidity: function saleStartTime() view returns(uint256)
func (_Meebits0 *Meebits0CallerSession) SaleStartTime() (*big.Int, error) {
	return _Meebits0.Contract.SaleStartTime(&_Meebits0.CallOpts)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 _interfaceID) view returns(bool)
func (_Meebits0 *Meebits0Caller) SupportsInterface(opts *bind.CallOpts, _interfaceID [4]byte) (bool, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "supportsInterface", _interfaceID)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 _interfaceID) view returns(bool)
func (_Meebits0 *Meebits0Session) SupportsInterface(_interfaceID [4]byte) (bool, error) {
	return _Meebits0.Contract.SupportsInterface(&_Meebits0.CallOpts, _interfaceID)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 _interfaceID) view returns(bool)
func (_Meebits0 *Meebits0CallerSession) SupportsInterface(_interfaceID [4]byte) (bool, error) {
	return _Meebits0.Contract.SupportsInterface(&_Meebits0.CallOpts, _interfaceID)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string _symbol)
func (_Meebits0 *Meebits0Caller) Symbol(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string _symbol)
func (_Meebits0 *Meebits0Session) Symbol() (string, error) {
	return _Meebits0.Contract.Symbol(&_Meebits0.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string _symbol)
func (_Meebits0 *Meebits0CallerSession) Symbol() (string, error) {
	return _Meebits0.Contract.Symbol(&_Meebits0.CallOpts)
}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) pure returns(uint256)
func (_Meebits0 *Meebits0Caller) TokenByIndex(opts *bind.CallOpts, index *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "tokenByIndex", index)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) pure returns(uint256)
func (_Meebits0 *Meebits0Session) TokenByIndex(index *big.Int) (*big.Int, error) {
	return _Meebits0.Contract.TokenByIndex(&_Meebits0.CallOpts, index)
}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) pure returns(uint256)
func (_Meebits0 *Meebits0CallerSession) TokenByIndex(index *big.Int) (*big.Int, error) {
	return _Meebits0.Contract.TokenByIndex(&_Meebits0.CallOpts, index)
}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address _owner, uint256 _index) view returns(uint256)
func (_Meebits0 *Meebits0Caller) TokenOfOwnerByIndex(opts *bind.CallOpts, _owner common.Address, _index *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "tokenOfOwnerByIndex", _owner, _index)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address _owner, uint256 _index) view returns(uint256)
func (_Meebits0 *Meebits0Session) TokenOfOwnerByIndex(_owner common.Address, _index *big.Int) (*big.Int, error) {
	return _Meebits0.Contract.TokenOfOwnerByIndex(&_Meebits0.CallOpts, _owner, _index)
}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address _owner, uint256 _index) view returns(uint256)
func (_Meebits0 *Meebits0CallerSession) TokenOfOwnerByIndex(_owner common.Address, _index *big.Int) (*big.Int, error) {
	return _Meebits0.Contract.TokenOfOwnerByIndex(&_Meebits0.CallOpts, _owner, _index)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 _tokenId) view returns(string)
func (_Meebits0 *Meebits0Caller) TokenURI(opts *bind.CallOpts, _tokenId *big.Int) (string, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "tokenURI", _tokenId)

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 _tokenId) view returns(string)
func (_Meebits0 *Meebits0Session) TokenURI(_tokenId *big.Int) (string, error) {
	return _Meebits0.Contract.TokenURI(&_Meebits0.CallOpts, _tokenId)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 _tokenId) view returns(string)
func (_Meebits0 *Meebits0CallerSession) TokenURI(_tokenId *big.Int) (string, error) {
	return _Meebits0.Contract.TokenURI(&_Meebits0.CallOpts, _tokenId)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_Meebits0 *Meebits0Caller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_Meebits0 *Meebits0Session) TotalSupply() (*big.Int, error) {
	return _Meebits0.Contract.TotalSupply(&_Meebits0.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_Meebits0 *Meebits0CallerSession) TotalSupply() (*big.Int, error) {
	return _Meebits0.Contract.TotalSupply(&_Meebits0.CallOpts)
}

// TradeValid is a free data retrieval call binding the contract method 0xd3d6c767.
//
// Solidity: function tradeValid(address maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds, uint256 expiry, uint256 salt, bytes signature) view returns(bool)
func (_Meebits0 *Meebits0Caller) TradeValid(opts *bind.CallOpts, maker common.Address, taker common.Address, makerWei *big.Int, makerIds []*big.Int, takerWei *big.Int, takerIds []*big.Int, expiry *big.Int, salt *big.Int, signature []byte) (bool, error) {
	var out []interface{}
	err := _Meebits0.contract.Call(opts, &out, "tradeValid", maker, taker, makerWei, makerIds, takerWei, takerIds, expiry, salt, signature)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// TradeValid is a free data retrieval call binding the contract method 0xd3d6c767.
//
// Solidity: function tradeValid(address maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds, uint256 expiry, uint256 salt, bytes signature) view returns(bool)
func (_Meebits0 *Meebits0Session) TradeValid(maker common.Address, taker common.Address, makerWei *big.Int, makerIds []*big.Int, takerWei *big.Int, takerIds []*big.Int, expiry *big.Int, salt *big.Int, signature []byte) (bool, error) {
	return _Meebits0.Contract.TradeValid(&_Meebits0.CallOpts, maker, taker, makerWei, makerIds, takerWei, takerIds, expiry, salt, signature)
}

// TradeValid is a free data retrieval call binding the contract method 0xd3d6c767.
//
// Solidity: function tradeValid(address maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds, uint256 expiry, uint256 salt, bytes signature) view returns(bool)
func (_Meebits0 *Meebits0CallerSession) TradeValid(maker common.Address, taker common.Address, makerWei *big.Int, makerIds []*big.Int, takerWei *big.Int, takerIds []*big.Int, expiry *big.Int, salt *big.Int, signature []byte) (bool, error) {
	return _Meebits0.Contract.TradeValid(&_Meebits0.CallOpts, maker, taker, makerWei, makerIds, takerWei, takerIds, expiry, salt, signature)
}

// AcceptTrade is a paid mutator transaction binding the contract method 0x0faadcf6.
//
// Solidity: function acceptTrade(address maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds, uint256 expiry, uint256 salt, bytes signature) payable returns()
func (_Meebits0 *Meebits0Transactor) AcceptTrade(opts *bind.TransactOpts, maker common.Address, taker common.Address, makerWei *big.Int, makerIds []*big.Int, takerWei *big.Int, takerIds []*big.Int, expiry *big.Int, salt *big.Int, signature []byte) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "acceptTrade", maker, taker, makerWei, makerIds, takerWei, takerIds, expiry, salt, signature)
}

// AcceptTrade is a paid mutator transaction binding the contract method 0x0faadcf6.
//
// Solidity: function acceptTrade(address maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds, uint256 expiry, uint256 salt, bytes signature) payable returns()
func (_Meebits0 *Meebits0Session) AcceptTrade(maker common.Address, taker common.Address, makerWei *big.Int, makerIds []*big.Int, takerWei *big.Int, takerIds []*big.Int, expiry *big.Int, salt *big.Int, signature []byte) (*types.Transaction, error) {
	return _Meebits0.Contract.AcceptTrade(&_Meebits0.TransactOpts, maker, taker, makerWei, makerIds, takerWei, takerIds, expiry, salt, signature)
}

// AcceptTrade is a paid mutator transaction binding the contract method 0x0faadcf6.
//
// Solidity: function acceptTrade(address maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds, uint256 expiry, uint256 salt, bytes signature) payable returns()
func (_Meebits0 *Meebits0TransactorSession) AcceptTrade(maker common.Address, taker common.Address, makerWei *big.Int, makerIds []*big.Int, takerWei *big.Int, takerIds []*big.Int, expiry *big.Int, salt *big.Int, signature []byte) (*types.Transaction, error) {
	return _Meebits0.Contract.AcceptTrade(&_Meebits0.TransactOpts, maker, taker, makerWei, makerIds, takerWei, takerIds, expiry, salt, signature)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address _approved, uint256 _tokenId) returns()
func (_Meebits0 *Meebits0Transactor) Approve(opts *bind.TransactOpts, _approved common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "approve", _approved, _tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address _approved, uint256 _tokenId) returns()
func (_Meebits0 *Meebits0Session) Approve(_approved common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.Approve(&_Meebits0.TransactOpts, _approved, _tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address _approved, uint256 _tokenId) returns()
func (_Meebits0 *Meebits0TransactorSession) Approve(_approved common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.Approve(&_Meebits0.TransactOpts, _approved, _tokenId)
}

// CancelOffer is a paid mutator transaction binding the contract method 0x8a84fe00.
//
// Solidity: function cancelOffer(address maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds, uint256 expiry, uint256 salt) returns()
func (_Meebits0 *Meebits0Transactor) CancelOffer(opts *bind.TransactOpts, maker common.Address, taker common.Address, makerWei *big.Int, makerIds []*big.Int, takerWei *big.Int, takerIds []*big.Int, expiry *big.Int, salt *big.Int) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "cancelOffer", maker, taker, makerWei, makerIds, takerWei, takerIds, expiry, salt)
}

// CancelOffer is a paid mutator transaction binding the contract method 0x8a84fe00.
//
// Solidity: function cancelOffer(address maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds, uint256 expiry, uint256 salt) returns()
func (_Meebits0 *Meebits0Session) CancelOffer(maker common.Address, taker common.Address, makerWei *big.Int, makerIds []*big.Int, takerWei *big.Int, takerIds []*big.Int, expiry *big.Int, salt *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.CancelOffer(&_Meebits0.TransactOpts, maker, taker, makerWei, makerIds, takerWei, takerIds, expiry, salt)
}

// CancelOffer is a paid mutator transaction binding the contract method 0x8a84fe00.
//
// Solidity: function cancelOffer(address maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds, uint256 expiry, uint256 salt) returns()
func (_Meebits0 *Meebits0TransactorSession) CancelOffer(maker common.Address, taker common.Address, makerWei *big.Int, makerIds []*big.Int, takerWei *big.Int, takerIds []*big.Int, expiry *big.Int, salt *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.CancelOffer(&_Meebits0.TransactOpts, maker, taker, makerWei, makerIds, takerWei, takerIds, expiry, salt)
}

// Deposit is a paid mutator transaction binding the contract method 0xd0e30db0.
//
// Solidity: function deposit() payable returns()
func (_Meebits0 *Meebits0Transactor) Deposit(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "deposit")
}

// Deposit is a paid mutator transaction binding the contract method 0xd0e30db0.
//
// Solidity: function deposit() payable returns()
func (_Meebits0 *Meebits0Session) Deposit() (*types.Transaction, error) {
	return _Meebits0.Contract.Deposit(&_Meebits0.TransactOpts)
}

// Deposit is a paid mutator transaction binding the contract method 0xd0e30db0.
//
// Solidity: function deposit() payable returns()
func (_Meebits0 *Meebits0TransactorSession) Deposit() (*types.Transaction, error) {
	return _Meebits0.Contract.Deposit(&_Meebits0.TransactOpts)
}

// DevMint is a paid mutator transaction binding the contract method 0x2d1a12f6.
//
// Solidity: function devMint(uint256 quantity, address recipient) returns()
func (_Meebits0 *Meebits0Transactor) DevMint(opts *bind.TransactOpts, quantity *big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "devMint", quantity, recipient)
}

// DevMint is a paid mutator transaction binding the contract method 0x2d1a12f6.
//
// Solidity: function devMint(uint256 quantity, address recipient) returns()
func (_Meebits0 *Meebits0Session) DevMint(quantity *big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Meebits0.Contract.DevMint(&_Meebits0.TransactOpts, quantity, recipient)
}

// DevMint is a paid mutator transaction binding the contract method 0x2d1a12f6.
//
// Solidity: function devMint(uint256 quantity, address recipient) returns()
func (_Meebits0 *Meebits0TransactorSession) DevMint(quantity *big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Meebits0.Contract.DevMint(&_Meebits0.TransactOpts, quantity, recipient)
}

// EndCommunityGrant is a paid mutator transaction binding the contract method 0xd357b01e.
//
// Solidity: function endCommunityGrant() returns()
func (_Meebits0 *Meebits0Transactor) EndCommunityGrant(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "endCommunityGrant")
}

// EndCommunityGrant is a paid mutator transaction binding the contract method 0xd357b01e.
//
// Solidity: function endCommunityGrant() returns()
func (_Meebits0 *Meebits0Session) EndCommunityGrant() (*types.Transaction, error) {
	return _Meebits0.Contract.EndCommunityGrant(&_Meebits0.TransactOpts)
}

// EndCommunityGrant is a paid mutator transaction binding the contract method 0xd357b01e.
//
// Solidity: function endCommunityGrant() returns()
func (_Meebits0 *Meebits0TransactorSession) EndCommunityGrant() (*types.Transaction, error) {
	return _Meebits0.Contract.EndCommunityGrant(&_Meebits0.TransactOpts)
}

// Mint is a paid mutator transaction binding the contract method 0x1249c58b.
//
// Solidity: function mint() payable returns(uint256)
func (_Meebits0 *Meebits0Transactor) Mint(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "mint")
}

// Mint is a paid mutator transaction binding the contract method 0x1249c58b.
//
// Solidity: function mint() payable returns(uint256)
func (_Meebits0 *Meebits0Session) Mint() (*types.Transaction, error) {
	return _Meebits0.Contract.Mint(&_Meebits0.TransactOpts)
}

// Mint is a paid mutator transaction binding the contract method 0x1249c58b.
//
// Solidity: function mint() payable returns(uint256)
func (_Meebits0 *Meebits0TransactorSession) Mint() (*types.Transaction, error) {
	return _Meebits0.Contract.Mint(&_Meebits0.TransactOpts)
}

// MintWithPunkOrGlyph is a paid mutator transaction binding the contract method 0xd09229a8.
//
// Solidity: function mintWithPunkOrGlyph(uint256 _createVia) returns(uint256)
func (_Meebits0 *Meebits0Transactor) MintWithPunkOrGlyph(opts *bind.TransactOpts, _createVia *big.Int) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "mintWithPunkOrGlyph", _createVia)
}

// MintWithPunkOrGlyph is a paid mutator transaction binding the contract method 0xd09229a8.
//
// Solidity: function mintWithPunkOrGlyph(uint256 _createVia) returns(uint256)
func (_Meebits0 *Meebits0Session) MintWithPunkOrGlyph(_createVia *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.MintWithPunkOrGlyph(&_Meebits0.TransactOpts, _createVia)
}

// MintWithPunkOrGlyph is a paid mutator transaction binding the contract method 0xd09229a8.
//
// Solidity: function mintWithPunkOrGlyph(uint256 _createVia) returns(uint256)
func (_Meebits0 *Meebits0TransactorSession) MintWithPunkOrGlyph(_createVia *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.MintWithPunkOrGlyph(&_Meebits0.TransactOpts, _createVia)
}

// PauseMarket is a paid mutator transaction binding the contract method 0x5ec390d8.
//
// Solidity: function pauseMarket(bool _paused) returns()
func (_Meebits0 *Meebits0Transactor) PauseMarket(opts *bind.TransactOpts, _paused bool) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "pauseMarket", _paused)
}

// PauseMarket is a paid mutator transaction binding the contract method 0x5ec390d8.
//
// Solidity: function pauseMarket(bool _paused) returns()
func (_Meebits0 *Meebits0Session) PauseMarket(_paused bool) (*types.Transaction, error) {
	return _Meebits0.Contract.PauseMarket(&_Meebits0.TransactOpts, _paused)
}

// PauseMarket is a paid mutator transaction binding the contract method 0x5ec390d8.
//
// Solidity: function pauseMarket(bool _paused) returns()
func (_Meebits0 *Meebits0TransactorSession) PauseMarket(_paused bool) (*types.Transaction, error) {
	return _Meebits0.Contract.PauseMarket(&_Meebits0.TransactOpts, _paused)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address _from, address _to, uint256 _tokenId) returns()
func (_Meebits0 *Meebits0Transactor) SafeTransferFrom(opts *bind.TransactOpts, _from common.Address, _to common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "safeTransferFrom", _from, _to, _tokenId)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address _from, address _to, uint256 _tokenId) returns()
func (_Meebits0 *Meebits0Session) SafeTransferFrom(_from common.Address, _to common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.SafeTransferFrom(&_Meebits0.TransactOpts, _from, _to, _tokenId)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address _from, address _to, uint256 _tokenId) returns()
func (_Meebits0 *Meebits0TransactorSession) SafeTransferFrom(_from common.Address, _to common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.SafeTransferFrom(&_Meebits0.TransactOpts, _from, _to, _tokenId)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address _from, address _to, uint256 _tokenId, bytes _data) returns()
func (_Meebits0 *Meebits0Transactor) SafeTransferFrom0(opts *bind.TransactOpts, _from common.Address, _to common.Address, _tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "safeTransferFrom0", _from, _to, _tokenId, _data)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address _from, address _to, uint256 _tokenId, bytes _data) returns()
func (_Meebits0 *Meebits0Session) SafeTransferFrom0(_from common.Address, _to common.Address, _tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _Meebits0.Contract.SafeTransferFrom0(&_Meebits0.TransactOpts, _from, _to, _tokenId, _data)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address _from, address _to, uint256 _tokenId, bytes _data) returns()
func (_Meebits0 *Meebits0TransactorSession) SafeTransferFrom0(_from common.Address, _to common.Address, _tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _Meebits0.Contract.SafeTransferFrom0(&_Meebits0.TransactOpts, _from, _to, _tokenId, _data)
}

// SealContract is a paid mutator transaction binding the contract method 0x68bd580e.
//
// Solidity: function sealContract() returns()
func (_Meebits0 *Meebits0Transactor) SealContract(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "sealContract")
}

// SealContract is a paid mutator transaction binding the contract method 0x68bd580e.
//
// Solidity: function sealContract() returns()
func (_Meebits0 *Meebits0Session) SealContract() (*types.Transaction, error) {
	return _Meebits0.Contract.SealContract(&_Meebits0.TransactOpts)
}

// SealContract is a paid mutator transaction binding the contract method 0x68bd580e.
//
// Solidity: function sealContract() returns()
func (_Meebits0 *Meebits0TransactorSession) SealContract() (*types.Transaction, error) {
	return _Meebits0.Contract.SealContract(&_Meebits0.TransactOpts)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address _operator, bool _approved) returns()
func (_Meebits0 *Meebits0Transactor) SetApprovalForAll(opts *bind.TransactOpts, _operator common.Address, _approved bool) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "setApprovalForAll", _operator, _approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address _operator, bool _approved) returns()
func (_Meebits0 *Meebits0Session) SetApprovalForAll(_operator common.Address, _approved bool) (*types.Transaction, error) {
	return _Meebits0.Contract.SetApprovalForAll(&_Meebits0.TransactOpts, _operator, _approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address _operator, bool _approved) returns()
func (_Meebits0 *Meebits0TransactorSession) SetApprovalForAll(_operator common.Address, _approved bool) (*types.Transaction, error) {
	return _Meebits0.Contract.SetApprovalForAll(&_Meebits0.TransactOpts, _operator, _approved)
}

// StartSale is a paid mutator transaction binding the contract method 0xf4f3122e.
//
// Solidity: function startSale(uint256 _price, uint256 _saleDuration) returns()
func (_Meebits0 *Meebits0Transactor) StartSale(opts *bind.TransactOpts, _price *big.Int, _saleDuration *big.Int) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "startSale", _price, _saleDuration)
}

// StartSale is a paid mutator transaction binding the contract method 0xf4f3122e.
//
// Solidity: function startSale(uint256 _price, uint256 _saleDuration) returns()
func (_Meebits0 *Meebits0Session) StartSale(_price *big.Int, _saleDuration *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.StartSale(&_Meebits0.TransactOpts, _price, _saleDuration)
}

// StartSale is a paid mutator transaction binding the contract method 0xf4f3122e.
//
// Solidity: function startSale(uint256 _price, uint256 _saleDuration) returns()
func (_Meebits0 *Meebits0TransactorSession) StartSale(_price *big.Int, _saleDuration *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.StartSale(&_Meebits0.TransactOpts, _price, _saleDuration)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address _from, address _to, uint256 _tokenId) returns()
func (_Meebits0 *Meebits0Transactor) TransferFrom(opts *bind.TransactOpts, _from common.Address, _to common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "transferFrom", _from, _to, _tokenId)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address _from, address _to, uint256 _tokenId) returns()
func (_Meebits0 *Meebits0Session) TransferFrom(_from common.Address, _to common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.TransferFrom(&_Meebits0.TransactOpts, _from, _to, _tokenId)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address _from, address _to, uint256 _tokenId) returns()
func (_Meebits0 *Meebits0TransactorSession) TransferFrom(_from common.Address, _to common.Address, _tokenId *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.TransferFrom(&_Meebits0.TransactOpts, _from, _to, _tokenId)
}

// Withdraw is a paid mutator transaction binding the contract method 0x2e1a7d4d.
//
// Solidity: function withdraw(uint256 amount) returns()
func (_Meebits0 *Meebits0Transactor) Withdraw(opts *bind.TransactOpts, amount *big.Int) (*types.Transaction, error) {
	return _Meebits0.contract.Transact(opts, "withdraw", amount)
}

// Withdraw is a paid mutator transaction binding the contract method 0x2e1a7d4d.
//
// Solidity: function withdraw(uint256 amount) returns()
func (_Meebits0 *Meebits0Session) Withdraw(amount *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.Withdraw(&_Meebits0.TransactOpts, amount)
}

// Withdraw is a paid mutator transaction binding the contract method 0x2e1a7d4d.
//
// Solidity: function withdraw(uint256 amount) returns()
func (_Meebits0 *Meebits0TransactorSession) Withdraw(amount *big.Int) (*types.Transaction, error) {
	return _Meebits0.Contract.Withdraw(&_Meebits0.TransactOpts, amount)
}

// Meebits0ApprovalIterator is returned from FilterApproval and is used to iterate over the raw logs and unpacked data for Approval events raised by the Meebits0 contract.
type Meebits0ApprovalIterator struct {
	Event *Meebits0Approval // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Meebits0ApprovalIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Meebits0Approval)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Meebits0Approval)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Meebits0ApprovalIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Meebits0ApprovalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Meebits0Approval represents a Approval event raised by the Meebits0 contract.
type Meebits0Approval struct {
	Owner    common.Address
	Approved common.Address
	TokenId  *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApproval is a free log retrieval operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_Meebits0 *Meebits0Filterer) FilterApproval(opts *bind.FilterOpts, owner []common.Address, approved []common.Address, tokenId []*big.Int) (*Meebits0ApprovalIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var approvedRule []interface{}
	for _, approvedItem := range approved {
		approvedRule = append(approvedRule, approvedItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Meebits0.contract.FilterLogs(opts, "Approval", ownerRule, approvedRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Meebits0ApprovalIterator{contract: _Meebits0.contract, event: "Approval", logs: logs, sub: sub}, nil
}

// WatchApproval is a free log subscription operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_Meebits0 *Meebits0Filterer) WatchApproval(opts *bind.WatchOpts, sink chan<- *Meebits0Approval, owner []common.Address, approved []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var approvedRule []interface{}
	for _, approvedItem := range approved {
		approvedRule = append(approvedRule, approvedItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Meebits0.contract.WatchLogs(opts, "Approval", ownerRule, approvedRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Meebits0Approval)
				if err := _Meebits0.contract.UnpackLog(event, "Approval", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApproval is a log parse operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_Meebits0 *Meebits0Filterer) ParseApproval(log types.Log) (*Meebits0Approval, error) {
	event := new(Meebits0Approval)
	if err := _Meebits0.contract.UnpackLog(event, "Approval", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Meebits0ApprovalForAllIterator is returned from FilterApprovalForAll and is used to iterate over the raw logs and unpacked data for ApprovalForAll events raised by the Meebits0 contract.
type Meebits0ApprovalForAllIterator struct {
	Event *Meebits0ApprovalForAll // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Meebits0ApprovalForAllIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Meebits0ApprovalForAll)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Meebits0ApprovalForAll)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Meebits0ApprovalForAllIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Meebits0ApprovalForAllIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Meebits0ApprovalForAll represents a ApprovalForAll event raised by the Meebits0 contract.
type Meebits0ApprovalForAll struct {
	Owner    common.Address
	Operator common.Address
	Approved bool
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApprovalForAll is a free log retrieval operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_Meebits0 *Meebits0Filterer) FilterApprovalForAll(opts *bind.FilterOpts, owner []common.Address, operator []common.Address) (*Meebits0ApprovalForAllIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var operatorRule []interface{}
	for _, operatorItem := range operator {
		operatorRule = append(operatorRule, operatorItem)
	}

	logs, sub, err := _Meebits0.contract.FilterLogs(opts, "ApprovalForAll", ownerRule, operatorRule)
	if err != nil {
		return nil, err
	}
	return &Meebits0ApprovalForAllIterator{contract: _Meebits0.contract, event: "ApprovalForAll", logs: logs, sub: sub}, nil
}

// WatchApprovalForAll is a free log subscription operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_Meebits0 *Meebits0Filterer) WatchApprovalForAll(opts *bind.WatchOpts, sink chan<- *Meebits0ApprovalForAll, owner []common.Address, operator []common.Address) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var operatorRule []interface{}
	for _, operatorItem := range operator {
		operatorRule = append(operatorRule, operatorItem)
	}

	logs, sub, err := _Meebits0.contract.WatchLogs(opts, "ApprovalForAll", ownerRule, operatorRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Meebits0ApprovalForAll)
				if err := _Meebits0.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApprovalForAll is a log parse operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_Meebits0 *Meebits0Filterer) ParseApprovalForAll(log types.Log) (*Meebits0ApprovalForAll, error) {
	event := new(Meebits0ApprovalForAll)
	if err := _Meebits0.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Meebits0CommunityGrantEndsIterator is returned from FilterCommunityGrantEnds and is used to iterate over the raw logs and unpacked data for CommunityGrantEnds events raised by the Meebits0 contract.
type Meebits0CommunityGrantEndsIterator struct {
	Event *Meebits0CommunityGrantEnds // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Meebits0CommunityGrantEndsIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Meebits0CommunityGrantEnds)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Meebits0CommunityGrantEnds)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Meebits0CommunityGrantEndsIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Meebits0CommunityGrantEndsIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Meebits0CommunityGrantEnds represents a CommunityGrantEnds event raised by the Meebits0 contract.
type Meebits0CommunityGrantEnds struct {
	Raw types.Log // Blockchain specific contextual infos
}

// FilterCommunityGrantEnds is a free log retrieval operation binding the contract event 0x5f00e567de9d29d9d36405c34b1164390da767fb97a1fb7d6bf3680ff90a3b26.
//
// Solidity: event CommunityGrantEnds()
func (_Meebits0 *Meebits0Filterer) FilterCommunityGrantEnds(opts *bind.FilterOpts) (*Meebits0CommunityGrantEndsIterator, error) {

	logs, sub, err := _Meebits0.contract.FilterLogs(opts, "CommunityGrantEnds")
	if err != nil {
		return nil, err
	}
	return &Meebits0CommunityGrantEndsIterator{contract: _Meebits0.contract, event: "CommunityGrantEnds", logs: logs, sub: sub}, nil
}

// WatchCommunityGrantEnds is a free log subscription operation binding the contract event 0x5f00e567de9d29d9d36405c34b1164390da767fb97a1fb7d6bf3680ff90a3b26.
//
// Solidity: event CommunityGrantEnds()
func (_Meebits0 *Meebits0Filterer) WatchCommunityGrantEnds(opts *bind.WatchOpts, sink chan<- *Meebits0CommunityGrantEnds) (event.Subscription, error) {

	logs, sub, err := _Meebits0.contract.WatchLogs(opts, "CommunityGrantEnds")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Meebits0CommunityGrantEnds)
				if err := _Meebits0.contract.UnpackLog(event, "CommunityGrantEnds", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCommunityGrantEnds is a log parse operation binding the contract event 0x5f00e567de9d29d9d36405c34b1164390da767fb97a1fb7d6bf3680ff90a3b26.
//
// Solidity: event CommunityGrantEnds()
func (_Meebits0 *Meebits0Filterer) ParseCommunityGrantEnds(log types.Log) (*Meebits0CommunityGrantEnds, error) {
	event := new(Meebits0CommunityGrantEnds)
	if err := _Meebits0.contract.UnpackLog(event, "CommunityGrantEnds", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Meebits0DepositIterator is returned from FilterDeposit and is used to iterate over the raw logs and unpacked data for Deposit events raised by the Meebits0 contract.
type Meebits0DepositIterator struct {
	Event *Meebits0Deposit // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Meebits0DepositIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Meebits0Deposit)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Meebits0Deposit)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Meebits0DepositIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Meebits0DepositIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Meebits0Deposit represents a Deposit event raised by the Meebits0 contract.
type Meebits0Deposit struct {
	Account common.Address
	Amount  *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterDeposit is a free log retrieval operation binding the contract event 0xe1fffcc4923d04b559f4d29a8bfc6cda04eb5b0d3c460751c2402c5c5cc9109c.
//
// Solidity: event Deposit(address indexed account, uint256 amount)
func (_Meebits0 *Meebits0Filterer) FilterDeposit(opts *bind.FilterOpts, account []common.Address) (*Meebits0DepositIterator, error) {

	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}

	logs, sub, err := _Meebits0.contract.FilterLogs(opts, "Deposit", accountRule)
	if err != nil {
		return nil, err
	}
	return &Meebits0DepositIterator{contract: _Meebits0.contract, event: "Deposit", logs: logs, sub: sub}, nil
}

// WatchDeposit is a free log subscription operation binding the contract event 0xe1fffcc4923d04b559f4d29a8bfc6cda04eb5b0d3c460751c2402c5c5cc9109c.
//
// Solidity: event Deposit(address indexed account, uint256 amount)
func (_Meebits0 *Meebits0Filterer) WatchDeposit(opts *bind.WatchOpts, sink chan<- *Meebits0Deposit, account []common.Address) (event.Subscription, error) {

	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}

	logs, sub, err := _Meebits0.contract.WatchLogs(opts, "Deposit", accountRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Meebits0Deposit)
				if err := _Meebits0.contract.UnpackLog(event, "Deposit", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseDeposit is a log parse operation binding the contract event 0xe1fffcc4923d04b559f4d29a8bfc6cda04eb5b0d3c460751c2402c5c5cc9109c.
//
// Solidity: event Deposit(address indexed account, uint256 amount)
func (_Meebits0 *Meebits0Filterer) ParseDeposit(log types.Log) (*Meebits0Deposit, error) {
	event := new(Meebits0Deposit)
	if err := _Meebits0.contract.UnpackLog(event, "Deposit", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Meebits0MintIterator is returned from FilterMint and is used to iterate over the raw logs and unpacked data for Mint events raised by the Meebits0 contract.
type Meebits0MintIterator struct {
	Event *Meebits0Mint // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Meebits0MintIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Meebits0Mint)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Meebits0Mint)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Meebits0MintIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Meebits0MintIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Meebits0Mint represents a Mint event raised by the Meebits0 contract.
type Meebits0Mint struct {
	Index      *big.Int
	Minter     common.Address
	CreatedVia *big.Int
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterMint is a free log retrieval operation binding the contract event 0x4e3883c75cc9c752bb1db2e406a822e4a75067ae77ad9a0a4d179f2709b9e1f6.
//
// Solidity: event Mint(uint256 indexed index, address indexed minter, uint256 createdVia)
func (_Meebits0 *Meebits0Filterer) FilterMint(opts *bind.FilterOpts, index []*big.Int, minter []common.Address) (*Meebits0MintIterator, error) {

	var indexRule []interface{}
	for _, indexItem := range index {
		indexRule = append(indexRule, indexItem)
	}
	var minterRule []interface{}
	for _, minterItem := range minter {
		minterRule = append(minterRule, minterItem)
	}

	logs, sub, err := _Meebits0.contract.FilterLogs(opts, "Mint", indexRule, minterRule)
	if err != nil {
		return nil, err
	}
	return &Meebits0MintIterator{contract: _Meebits0.contract, event: "Mint", logs: logs, sub: sub}, nil
}

// WatchMint is a free log subscription operation binding the contract event 0x4e3883c75cc9c752bb1db2e406a822e4a75067ae77ad9a0a4d179f2709b9e1f6.
//
// Solidity: event Mint(uint256 indexed index, address indexed minter, uint256 createdVia)
func (_Meebits0 *Meebits0Filterer) WatchMint(opts *bind.WatchOpts, sink chan<- *Meebits0Mint, index []*big.Int, minter []common.Address) (event.Subscription, error) {

	var indexRule []interface{}
	for _, indexItem := range index {
		indexRule = append(indexRule, indexItem)
	}
	var minterRule []interface{}
	for _, minterItem := range minter {
		minterRule = append(minterRule, minterItem)
	}

	logs, sub, err := _Meebits0.contract.WatchLogs(opts, "Mint", indexRule, minterRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Meebits0Mint)
				if err := _Meebits0.contract.UnpackLog(event, "Mint", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseMint is a log parse operation binding the contract event 0x4e3883c75cc9c752bb1db2e406a822e4a75067ae77ad9a0a4d179f2709b9e1f6.
//
// Solidity: event Mint(uint256 indexed index, address indexed minter, uint256 createdVia)
func (_Meebits0 *Meebits0Filterer) ParseMint(log types.Log) (*Meebits0Mint, error) {
	event := new(Meebits0Mint)
	if err := _Meebits0.contract.UnpackLog(event, "Mint", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Meebits0OfferCancelledIterator is returned from FilterOfferCancelled and is used to iterate over the raw logs and unpacked data for OfferCancelled events raised by the Meebits0 contract.
type Meebits0OfferCancelledIterator struct {
	Event *Meebits0OfferCancelled // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Meebits0OfferCancelledIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Meebits0OfferCancelled)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Meebits0OfferCancelled)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Meebits0OfferCancelledIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Meebits0OfferCancelledIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Meebits0OfferCancelled represents a OfferCancelled event raised by the Meebits0 contract.
type Meebits0OfferCancelled struct {
	Hash [32]byte
	Raw  types.Log // Blockchain specific contextual infos
}

// FilterOfferCancelled is a free log retrieval operation binding the contract event 0x3f9cb69d022b6ec319f86f2df848bcce01f2fc51c9f86396779a8081cf6ca2ea.
//
// Solidity: event OfferCancelled(bytes32 hash)
func (_Meebits0 *Meebits0Filterer) FilterOfferCancelled(opts *bind.FilterOpts) (*Meebits0OfferCancelledIterator, error) {

	logs, sub, err := _Meebits0.contract.FilterLogs(opts, "OfferCancelled")
	if err != nil {
		return nil, err
	}
	return &Meebits0OfferCancelledIterator{contract: _Meebits0.contract, event: "OfferCancelled", logs: logs, sub: sub}, nil
}

// WatchOfferCancelled is a free log subscription operation binding the contract event 0x3f9cb69d022b6ec319f86f2df848bcce01f2fc51c9f86396779a8081cf6ca2ea.
//
// Solidity: event OfferCancelled(bytes32 hash)
func (_Meebits0 *Meebits0Filterer) WatchOfferCancelled(opts *bind.WatchOpts, sink chan<- *Meebits0OfferCancelled) (event.Subscription, error) {

	logs, sub, err := _Meebits0.contract.WatchLogs(opts, "OfferCancelled")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Meebits0OfferCancelled)
				if err := _Meebits0.contract.UnpackLog(event, "OfferCancelled", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOfferCancelled is a log parse operation binding the contract event 0x3f9cb69d022b6ec319f86f2df848bcce01f2fc51c9f86396779a8081cf6ca2ea.
//
// Solidity: event OfferCancelled(bytes32 hash)
func (_Meebits0 *Meebits0Filterer) ParseOfferCancelled(log types.Log) (*Meebits0OfferCancelled, error) {
	event := new(Meebits0OfferCancelled)
	if err := _Meebits0.contract.UnpackLog(event, "OfferCancelled", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Meebits0SaleBeginsIterator is returned from FilterSaleBegins and is used to iterate over the raw logs and unpacked data for SaleBegins events raised by the Meebits0 contract.
type Meebits0SaleBeginsIterator struct {
	Event *Meebits0SaleBegins // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Meebits0SaleBeginsIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Meebits0SaleBegins)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Meebits0SaleBegins)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Meebits0SaleBeginsIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Meebits0SaleBeginsIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Meebits0SaleBegins represents a SaleBegins event raised by the Meebits0 contract.
type Meebits0SaleBegins struct {
	Raw types.Log // Blockchain specific contextual infos
}

// FilterSaleBegins is a free log retrieval operation binding the contract event 0x771cfe172460b7d64cc46cca57a1e1f40f52b47cf1d16fe30c78a2935b3dd580.
//
// Solidity: event SaleBegins()
func (_Meebits0 *Meebits0Filterer) FilterSaleBegins(opts *bind.FilterOpts) (*Meebits0SaleBeginsIterator, error) {

	logs, sub, err := _Meebits0.contract.FilterLogs(opts, "SaleBegins")
	if err != nil {
		return nil, err
	}
	return &Meebits0SaleBeginsIterator{contract: _Meebits0.contract, event: "SaleBegins", logs: logs, sub: sub}, nil
}

// WatchSaleBegins is a free log subscription operation binding the contract event 0x771cfe172460b7d64cc46cca57a1e1f40f52b47cf1d16fe30c78a2935b3dd580.
//
// Solidity: event SaleBegins()
func (_Meebits0 *Meebits0Filterer) WatchSaleBegins(opts *bind.WatchOpts, sink chan<- *Meebits0SaleBegins) (event.Subscription, error) {

	logs, sub, err := _Meebits0.contract.WatchLogs(opts, "SaleBegins")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Meebits0SaleBegins)
				if err := _Meebits0.contract.UnpackLog(event, "SaleBegins", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseSaleBegins is a log parse operation binding the contract event 0x771cfe172460b7d64cc46cca57a1e1f40f52b47cf1d16fe30c78a2935b3dd580.
//
// Solidity: event SaleBegins()
func (_Meebits0 *Meebits0Filterer) ParseSaleBegins(log types.Log) (*Meebits0SaleBegins, error) {
	event := new(Meebits0SaleBegins)
	if err := _Meebits0.contract.UnpackLog(event, "SaleBegins", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Meebits0TradeIterator is returned from FilterTrade and is used to iterate over the raw logs and unpacked data for Trade events raised by the Meebits0 contract.
type Meebits0TradeIterator struct {
	Event *Meebits0Trade // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Meebits0TradeIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Meebits0Trade)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Meebits0Trade)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Meebits0TradeIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Meebits0TradeIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Meebits0Trade represents a Trade event raised by the Meebits0 contract.
type Meebits0Trade struct {
	Hash     [32]byte
	Maker    common.Address
	Taker    common.Address
	MakerWei *big.Int
	MakerIds []*big.Int
	TakerWei *big.Int
	TakerIds []*big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterTrade is a free log retrieval operation binding the contract event 0x753d4db383bcac616c2e2651156bc71c1ec81c0d5509071a85e3f565568765a3.
//
// Solidity: event Trade(bytes32 indexed hash, address indexed maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds)
func (_Meebits0 *Meebits0Filterer) FilterTrade(opts *bind.FilterOpts, hash [][32]byte, maker []common.Address) (*Meebits0TradeIterator, error) {

	var hashRule []interface{}
	for _, hashItem := range hash {
		hashRule = append(hashRule, hashItem)
	}
	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}

	logs, sub, err := _Meebits0.contract.FilterLogs(opts, "Trade", hashRule, makerRule)
	if err != nil {
		return nil, err
	}
	return &Meebits0TradeIterator{contract: _Meebits0.contract, event: "Trade", logs: logs, sub: sub}, nil
}

// WatchTrade is a free log subscription operation binding the contract event 0x753d4db383bcac616c2e2651156bc71c1ec81c0d5509071a85e3f565568765a3.
//
// Solidity: event Trade(bytes32 indexed hash, address indexed maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds)
func (_Meebits0 *Meebits0Filterer) WatchTrade(opts *bind.WatchOpts, sink chan<- *Meebits0Trade, hash [][32]byte, maker []common.Address) (event.Subscription, error) {

	var hashRule []interface{}
	for _, hashItem := range hash {
		hashRule = append(hashRule, hashItem)
	}
	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}

	logs, sub, err := _Meebits0.contract.WatchLogs(opts, "Trade", hashRule, makerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Meebits0Trade)
				if err := _Meebits0.contract.UnpackLog(event, "Trade", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTrade is a log parse operation binding the contract event 0x753d4db383bcac616c2e2651156bc71c1ec81c0d5509071a85e3f565568765a3.
//
// Solidity: event Trade(bytes32 indexed hash, address indexed maker, address taker, uint256 makerWei, uint256[] makerIds, uint256 takerWei, uint256[] takerIds)
func (_Meebits0 *Meebits0Filterer) ParseTrade(log types.Log) (*Meebits0Trade, error) {
	event := new(Meebits0Trade)
	if err := _Meebits0.contract.UnpackLog(event, "Trade", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Meebits0TransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the Meebits0 contract.
type Meebits0TransferIterator struct {
	Event *Meebits0Transfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Meebits0TransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Meebits0Transfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Meebits0Transfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Meebits0TransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Meebits0TransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Meebits0Transfer represents a Transfer event raised by the Meebits0 contract.
type Meebits0Transfer struct {
	From    common.Address
	To      common.Address
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_Meebits0 *Meebits0Filterer) FilterTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address, tokenId []*big.Int) (*Meebits0TransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Meebits0.contract.FilterLogs(opts, "Transfer", fromRule, toRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Meebits0TransferIterator{contract: _Meebits0.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_Meebits0 *Meebits0Filterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *Meebits0Transfer, from []common.Address, to []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Meebits0.contract.WatchLogs(opts, "Transfer", fromRule, toRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Meebits0Transfer)
				if err := _Meebits0.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_Meebits0 *Meebits0Filterer) ParseTransfer(log types.Log) (*Meebits0Transfer, error) {
	event := new(Meebits0Transfer)
	if err := _Meebits0.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Meebits0WithdrawIterator is returned from FilterWithdraw and is used to iterate over the raw logs and unpacked data for Withdraw events raised by the Meebits0 contract.
type Meebits0WithdrawIterator struct {
	Event *Meebits0Withdraw // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Meebits0WithdrawIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Meebits0Withdraw)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Meebits0Withdraw)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Meebits0WithdrawIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Meebits0WithdrawIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Meebits0Withdraw represents a Withdraw event raised by the Meebits0 contract.
type Meebits0Withdraw struct {
	Account common.Address
	Amount  *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterWithdraw is a free log retrieval operation binding the contract event 0x884edad9ce6fa2440d8a54cc123490eb96d2768479d49ff9c7366125a9424364.
//
// Solidity: event Withdraw(address indexed account, uint256 amount)
func (_Meebits0 *Meebits0Filterer) FilterWithdraw(opts *bind.FilterOpts, account []common.Address) (*Meebits0WithdrawIterator, error) {

	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}

	logs, sub, err := _Meebits0.contract.FilterLogs(opts, "Withdraw", accountRule)
	if err != nil {
		return nil, err
	}
	return &Meebits0WithdrawIterator{contract: _Meebits0.contract, event: "Withdraw", logs: logs, sub: sub}, nil
}

// WatchWithdraw is a free log subscription operation binding the contract event 0x884edad9ce6fa2440d8a54cc123490eb96d2768479d49ff9c7366125a9424364.
//
// Solidity: event Withdraw(address indexed account, uint256 amount)
func (_Meebits0 *Meebits0Filterer) WatchWithdraw(opts *bind.WatchOpts, sink chan<- *Meebits0Withdraw, account []common.Address) (event.Subscription, error) {

	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}

	logs, sub, err := _Meebits0.contract.WatchLogs(opts, "Withdraw", accountRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Meebits0Withdraw)
				if err := _Meebits0.contract.UnpackLog(event, "Withdraw", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseWithdraw is a log parse operation binding the contract event 0x884edad9ce6fa2440d8a54cc123490eb96d2768479d49ff9c7366125a9424364.
//
// Solidity: event Withdraw(address indexed account, uint256 amount)
func (_Meebits0 *Meebits0Filterer) ParseWithdraw(log types.Log) (*Meebits0Withdraw, error) {
	event := new(Meebits0Withdraw)
	if err := _Meebits0.contract.UnpackLog(event, "Withdraw", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
