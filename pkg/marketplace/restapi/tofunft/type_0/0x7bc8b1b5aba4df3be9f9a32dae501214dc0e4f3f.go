// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package tofunft_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// MarketNGDetail is an auto generated low-level Go binding around an user-defined struct.
type MarketNGDetail struct {
	IntentionHash [32]byte
	Signer        common.Address
	TxDeadline    *big.Int
	Salt          [32]byte
	Id            *big.Int
	Opcode        uint8
	Caller        common.Address
	Currency      common.Address
	Price         *big.Int
	IncentiveRate *big.Int
	Settlement    MarketNGSettlement
	Bundle        []MarketNGTokenPair
	Deadline      *big.Int
}

// MarketNGIntention is an auto generated low-level Go binding around an user-defined struct.
type MarketNGIntention struct {
	User     common.Address
	Bundle   []MarketNGTokenPair
	Currency common.Address
	Price    *big.Int
	Deadline *big.Int
	Salt     [32]byte
	Kind     uint8
}

// MarketNGInventory is an auto generated low-level Go binding around an user-defined struct.
type MarketNGInventory struct {
	Seller   common.Address
	Buyer    common.Address
	Currency common.Address
	Price    *big.Int
	NetPrice *big.Int
	Deadline *big.Int
	Kind     uint8
	Status   uint8
}

// MarketNGPair721 is an auto generated low-level Go binding around an user-defined struct.
type MarketNGPair721 struct {
	Token   common.Address
	TokenId *big.Int
}

// MarketNGSettlement is an auto generated low-level Go binding around an user-defined struct.
type MarketNGSettlement struct {
	Coupons           []*big.Int
	FeeRate           *big.Int
	RoyaltyRate       *big.Int
	BuyerCashbackRate *big.Int
	FeeAddress        common.Address
	RoyaltyAddress    common.Address
}

// MarketNGSwap is an auto generated low-level Go binding around an user-defined struct.
type MarketNGSwap struct {
	Salt     [32]byte
	Creator  common.Address
	Deadline *big.Int
	Has      []MarketNGPair721
	Wants    []MarketNGPair721
}

// MarketNGTokenPair is an auto generated low-level Go binding around an user-defined struct.
type MarketNGTokenPair struct {
	Token    common.Address
	TokenId  *big.Int
	Amount   *big.Int
	Kind     uint8
	MintData []byte
}

// Tofunft0MetaData contains all meta data concerning the Tofunft0 contract.
var Tofunft0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"contractIWETH\",\"name\":\"weth_\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"refund\",\"type\":\"uint256\"}],\"name\":\"EvAuctionRefund\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"couponId\",\"type\":\"uint256\"}],\"name\":\"EvCouponSpent\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"},{\"internalType\":\"contractIERC20\",\"name\":\"currency\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"netPrice\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"},{\"internalType\":\"uint8\",\"name\":\"kind\",\"type\":\"uint8\"},{\"internalType\":\"uint8\",\"name\":\"status\",\"type\":\"uint8\"}],\"indexed\":false,\"internalType\":\"structMarketNG.Inventory\",\"name\":\"inventory\",\"type\":\"tuple\"}],\"name\":\"EvInventoryUpdate\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"addr\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"isRemoval\",\"type\":\"bool\"}],\"name\":\"EvMarketSignerUpdate\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[],\"name\":\"EvSettingsUpdated\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"components\":[{\"internalType\":\"bytes32\",\"name\":\"salt\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"creator\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"contractIERC721\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"internalType\":\"structMarketNG.Pair721[]\",\"name\":\"has\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"contractIERC721\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"internalType\":\"structMarketNG.Pair721[]\",\"name\":\"wants\",\"type\":\"tuple[]\"}],\"indexed\":false,\"internalType\":\"structMarketNG.Swap\",\"name\":\"req\",\"type\":\"tuple\"},{\"indexed\":false,\"internalType\":\"bytes\",\"name\":\"signature\",\"type\":\"bytes\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"swapper\",\"type\":\"address\"}],\"name\":\"EvSwapped\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Paused\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Unpaused\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"KIND_AUCTION\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"KIND_BUY\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"KIND_SELL\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"OP_ACCEPT_AUCTION\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"OP_ACCEPT_BUY\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"OP_BID\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"OP_BUY\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"OP_CANCEL_BUY\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"OP_COMPLETE_AUCTION\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"OP_COMPLETE_BUY\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"OP_COMPLETE_SELL\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"OP_MAX\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"OP_MIN\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"OP_REJECT_BUY\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"RATE_BASE\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"STATUS_CANCELLED\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"STATUS_DONE\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"STATUS_OPEN\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"TOKEN_1155\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"TOKEN_721\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"TOKEN_MINT\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256[]\",\"name\":\"ids\",\"type\":\"uint256[]\"}],\"name\":\"cancelBuys\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"couponSpent\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"noBundle\",\"type\":\"bool\"}],\"name\":\"emergencyCancelAuction\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"}],\"name\":\"hasInv\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint8\",\"name\":\"op\",\"type\":\"uint8\"}],\"name\":\"hasSignedIntention\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"contractIERC20\",\"name\":\"currency\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"inCaseMoneyGetsStuck\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"inventories\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"},{\"internalType\":\"contractIERC20\",\"name\":\"currency\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"netPrice\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"},{\"internalType\":\"uint8\",\"name\":\"kind\",\"type\":\"uint8\"},{\"internalType\":\"uint8\",\"name\":\"status\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"inventoryTokenCounts\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"inventoryTokens\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint8\",\"name\":\"kind\",\"type\":\"uint8\"},{\"internalType\":\"bytes\",\"name\":\"mintData\",\"type\":\"bytes\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"}],\"name\":\"isAuction\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"}],\"name\":\"isAuctionOpen\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"invId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"}],\"name\":\"isBundleApproved\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"}],\"name\":\"isBuy\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"}],\"name\":\"isBuyOpen\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"}],\"name\":\"isExpired\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"}],\"name\":\"isSell\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes\",\"name\":\"signature\",\"type\":\"bytes\"},{\"internalType\":\"bytes32\",\"name\":\"hash\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"signer\",\"type\":\"address\"}],\"name\":\"isSignatureValid\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"}],\"name\":\"isStatusOpen\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"marketSigners\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"minAuctionDuration\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"minAuctionIncrement\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"ids\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256[]\",\"name\":\"values\",\"type\":\"uint256[]\"},{\"internalType\":\"bytes\",\"name\":\"data\",\"type\":\"bytes\"}],\"name\":\"onERC1155BatchReceived\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"data\",\"type\":\"bytes\"}],\"name\":\"onERC1155Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"data\",\"type\":\"bytes\"}],\"name\":\"onERC721Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"pause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"paused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint8\",\"name\":\"kind\",\"type\":\"uint8\"},{\"internalType\":\"bytes\",\"name\":\"mintData\",\"type\":\"bytes\"}],\"internalType\":\"structMarketNG.TokenPair[]\",\"name\":\"bundle\",\"type\":\"tuple[]\"},{\"internalType\":\"contractIERC20\",\"name\":\"currency\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"},{\"internalType\":\"bytes32\",\"name\":\"salt\",\"type\":\"bytes32\"},{\"internalType\":\"uint8\",\"name\":\"kind\",\"type\":\"uint8\"}],\"internalType\":\"structMarketNG.Intention\",\"name\":\"intent\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"bytes32\",\"name\":\"intentionHash\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"signer\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"txDeadline\",\"type\":\"uint256\"},{\"internalType\":\"bytes32\",\"name\":\"salt\",\"type\":\"bytes32\"},{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint8\",\"name\":\"opcode\",\"type\":\"uint8\"},{\"internalType\":\"address\",\"name\":\"caller\",\"type\":\"address\"},{\"internalType\":\"contractIERC20\",\"name\":\"currency\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"incentiveRate\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"uint256[]\",\"name\":\"coupons\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256\",\"name\":\"feeRate\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"royaltyRate\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"buyerCashbackRate\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"feeAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"royaltyAddress\",\"type\":\"address\"}],\"internalType\":\"structMarketNG.Settlement\",\"name\":\"settlement\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint8\",\"name\":\"kind\",\"type\":\"uint8\"},{\"internalType\":\"bytes\",\"name\":\"mintData\",\"type\":\"bytes\"}],\"internalType\":\"structMarketNG.TokenPair[]\",\"name\":\"bundle\",\"type\":\"tuple[]\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"internalType\":\"structMarketNG.Detail\",\"name\":\"detail\",\"type\":\"tuple\"},{\"internalType\":\"bytes\",\"name\":\"sigIntent\",\"type\":\"bytes\"},{\"internalType\":\"bytes\",\"name\":\"sigDetail\",\"type\":\"bytes\"}],\"name\":\"run\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"components\":[{\"internalType\":\"contractIERC721\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"internalType\":\"structMarketNG.Pair721[]\",\"name\":\"tokens\",\"type\":\"tuple[]\"}],\"name\":\"send\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes4\",\"name\":\"interfaceId\",\"type\":\"bytes4\"}],\"name\":\"supportsInterface\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"bytes32\",\"name\":\"salt\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"creator\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"contractIERC721\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"internalType\":\"structMarketNG.Pair721[]\",\"name\":\"has\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"contractIERC721\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"internalType\":\"structMarketNG.Pair721[]\",\"name\":\"wants\",\"type\":\"tuple[]\"}],\"internalType\":\"structMarketNG.Swap\",\"name\":\"req\",\"type\":\"tuple\"},{\"internalType\":\"bytes\",\"name\":\"signature\",\"type\":\"bytes\"}],\"name\":\"swap\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"unpause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"minAuctionIncrement_\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minAuctionDuration_\",\"type\":\"uint256\"}],\"name\":\"updateSettings\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"addr\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"remove\",\"type\":\"bool\"}],\"name\":\"updateSigner\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"weth\",\"outputs\":[{\"internalType\":\"contractIWETH\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
}

// Tofunft0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Tofunft0MetaData.ABI instead.
var Tofunft0ABI = Tofunft0MetaData.ABI

// Tofunft0 is an auto generated Go binding around an Ethereum contract.
type Tofunft0 struct {
	Tofunft0Caller     // Read-only binding to the contract
	Tofunft0Transactor // Write-only binding to the contract
	Tofunft0Filterer   // Log filterer for contract events
}

// Tofunft0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Tofunft0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Tofunft0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Tofunft0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Tofunft0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Tofunft0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Tofunft0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Tofunft0Session struct {
	Contract     *Tofunft0         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Tofunft0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Tofunft0CallerSession struct {
	Contract *Tofunft0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// Tofunft0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Tofunft0TransactorSession struct {
	Contract     *Tofunft0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// Tofunft0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Tofunft0Raw struct {
	Contract *Tofunft0 // Generic contract binding to access the raw methods on
}

// Tofunft0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Tofunft0CallerRaw struct {
	Contract *Tofunft0Caller // Generic read-only contract binding to access the raw methods on
}

// Tofunft0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Tofunft0TransactorRaw struct {
	Contract *Tofunft0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewTofunft0 creates a new instance of Tofunft0, bound to a specific deployed contract.
func NewTofunft0(address common.Address, backend bind.ContractBackend) (*Tofunft0, error) {
	contract, err := bindTofunft0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Tofunft0{Tofunft0Caller: Tofunft0Caller{contract: contract}, Tofunft0Transactor: Tofunft0Transactor{contract: contract}, Tofunft0Filterer: Tofunft0Filterer{contract: contract}}, nil
}

// NewTofunft0Caller creates a new read-only instance of Tofunft0, bound to a specific deployed contract.
func NewTofunft0Caller(address common.Address, caller bind.ContractCaller) (*Tofunft0Caller, error) {
	contract, err := bindTofunft0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Tofunft0Caller{contract: contract}, nil
}

// NewTofunft0Transactor creates a new write-only instance of Tofunft0, bound to a specific deployed contract.
func NewTofunft0Transactor(address common.Address, transactor bind.ContractTransactor) (*Tofunft0Transactor, error) {
	contract, err := bindTofunft0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Tofunft0Transactor{contract: contract}, nil
}

// NewTofunft0Filterer creates a new log filterer instance of Tofunft0, bound to a specific deployed contract.
func NewTofunft0Filterer(address common.Address, filterer bind.ContractFilterer) (*Tofunft0Filterer, error) {
	contract, err := bindTofunft0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Tofunft0Filterer{contract: contract}, nil
}

// bindTofunft0 binds a generic wrapper to an already deployed contract.
func bindTofunft0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Tofunft0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Tofunft0 *Tofunft0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Tofunft0.Contract.Tofunft0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Tofunft0 *Tofunft0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Tofunft0.Contract.Tofunft0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Tofunft0 *Tofunft0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Tofunft0.Contract.Tofunft0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Tofunft0 *Tofunft0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Tofunft0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Tofunft0 *Tofunft0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Tofunft0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Tofunft0 *Tofunft0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Tofunft0.Contract.contract.Transact(opts, method, params...)
}

// KINDAUCTION is a free data retrieval call binding the contract method 0x7234d8f2.
//
// Solidity: function KIND_AUCTION() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) KINDAUCTION(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "KIND_AUCTION")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// KINDAUCTION is a free data retrieval call binding the contract method 0x7234d8f2.
//
// Solidity: function KIND_AUCTION() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) KINDAUCTION() (uint8, error) {
	return _Tofunft0.Contract.KINDAUCTION(&_Tofunft0.CallOpts)
}

// KINDAUCTION is a free data retrieval call binding the contract method 0x7234d8f2.
//
// Solidity: function KIND_AUCTION() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) KINDAUCTION() (uint8, error) {
	return _Tofunft0.Contract.KINDAUCTION(&_Tofunft0.CallOpts)
}

// KINDBUY is a free data retrieval call binding the contract method 0xe1784a02.
//
// Solidity: function KIND_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) KINDBUY(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "KIND_BUY")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// KINDBUY is a free data retrieval call binding the contract method 0xe1784a02.
//
// Solidity: function KIND_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) KINDBUY() (uint8, error) {
	return _Tofunft0.Contract.KINDBUY(&_Tofunft0.CallOpts)
}

// KINDBUY is a free data retrieval call binding the contract method 0xe1784a02.
//
// Solidity: function KIND_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) KINDBUY() (uint8, error) {
	return _Tofunft0.Contract.KINDBUY(&_Tofunft0.CallOpts)
}

// KINDSELL is a free data retrieval call binding the contract method 0x25593ac2.
//
// Solidity: function KIND_SELL() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) KINDSELL(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "KIND_SELL")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// KINDSELL is a free data retrieval call binding the contract method 0x25593ac2.
//
// Solidity: function KIND_SELL() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) KINDSELL() (uint8, error) {
	return _Tofunft0.Contract.KINDSELL(&_Tofunft0.CallOpts)
}

// KINDSELL is a free data retrieval call binding the contract method 0x25593ac2.
//
// Solidity: function KIND_SELL() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) KINDSELL() (uint8, error) {
	return _Tofunft0.Contract.KINDSELL(&_Tofunft0.CallOpts)
}

// OPACCEPTAUCTION is a free data retrieval call binding the contract method 0x7ae1ace0.
//
// Solidity: function OP_ACCEPT_AUCTION() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) OPACCEPTAUCTION(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "OP_ACCEPT_AUCTION")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// OPACCEPTAUCTION is a free data retrieval call binding the contract method 0x7ae1ace0.
//
// Solidity: function OP_ACCEPT_AUCTION() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) OPACCEPTAUCTION() (uint8, error) {
	return _Tofunft0.Contract.OPACCEPTAUCTION(&_Tofunft0.CallOpts)
}

// OPACCEPTAUCTION is a free data retrieval call binding the contract method 0x7ae1ace0.
//
// Solidity: function OP_ACCEPT_AUCTION() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) OPACCEPTAUCTION() (uint8, error) {
	return _Tofunft0.Contract.OPACCEPTAUCTION(&_Tofunft0.CallOpts)
}

// OPACCEPTBUY is a free data retrieval call binding the contract method 0x11f0794c.
//
// Solidity: function OP_ACCEPT_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) OPACCEPTBUY(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "OP_ACCEPT_BUY")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// OPACCEPTBUY is a free data retrieval call binding the contract method 0x11f0794c.
//
// Solidity: function OP_ACCEPT_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) OPACCEPTBUY() (uint8, error) {
	return _Tofunft0.Contract.OPACCEPTBUY(&_Tofunft0.CallOpts)
}

// OPACCEPTBUY is a free data retrieval call binding the contract method 0x11f0794c.
//
// Solidity: function OP_ACCEPT_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) OPACCEPTBUY() (uint8, error) {
	return _Tofunft0.Contract.OPACCEPTBUY(&_Tofunft0.CallOpts)
}

// OPBID is a free data retrieval call binding the contract method 0x81787a85.
//
// Solidity: function OP_BID() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) OPBID(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "OP_BID")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// OPBID is a free data retrieval call binding the contract method 0x81787a85.
//
// Solidity: function OP_BID() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) OPBID() (uint8, error) {
	return _Tofunft0.Contract.OPBID(&_Tofunft0.CallOpts)
}

// OPBID is a free data retrieval call binding the contract method 0x81787a85.
//
// Solidity: function OP_BID() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) OPBID() (uint8, error) {
	return _Tofunft0.Contract.OPBID(&_Tofunft0.CallOpts)
}

// OPBUY is a free data retrieval call binding the contract method 0xeb374261.
//
// Solidity: function OP_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) OPBUY(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "OP_BUY")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// OPBUY is a free data retrieval call binding the contract method 0xeb374261.
//
// Solidity: function OP_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) OPBUY() (uint8, error) {
	return _Tofunft0.Contract.OPBUY(&_Tofunft0.CallOpts)
}

// OPBUY is a free data retrieval call binding the contract method 0xeb374261.
//
// Solidity: function OP_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) OPBUY() (uint8, error) {
	return _Tofunft0.Contract.OPBUY(&_Tofunft0.CallOpts)
}

// OPCANCELBUY is a free data retrieval call binding the contract method 0x9e57feb5.
//
// Solidity: function OP_CANCEL_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) OPCANCELBUY(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "OP_CANCEL_BUY")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// OPCANCELBUY is a free data retrieval call binding the contract method 0x9e57feb5.
//
// Solidity: function OP_CANCEL_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) OPCANCELBUY() (uint8, error) {
	return _Tofunft0.Contract.OPCANCELBUY(&_Tofunft0.CallOpts)
}

// OPCANCELBUY is a free data retrieval call binding the contract method 0x9e57feb5.
//
// Solidity: function OP_CANCEL_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) OPCANCELBUY() (uint8, error) {
	return _Tofunft0.Contract.OPCANCELBUY(&_Tofunft0.CallOpts)
}

// OPCOMPLETEAUCTION is a free data retrieval call binding the contract method 0x6acc65db.
//
// Solidity: function OP_COMPLETE_AUCTION() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) OPCOMPLETEAUCTION(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "OP_COMPLETE_AUCTION")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// OPCOMPLETEAUCTION is a free data retrieval call binding the contract method 0x6acc65db.
//
// Solidity: function OP_COMPLETE_AUCTION() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) OPCOMPLETEAUCTION() (uint8, error) {
	return _Tofunft0.Contract.OPCOMPLETEAUCTION(&_Tofunft0.CallOpts)
}

// OPCOMPLETEAUCTION is a free data retrieval call binding the contract method 0x6acc65db.
//
// Solidity: function OP_COMPLETE_AUCTION() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) OPCOMPLETEAUCTION() (uint8, error) {
	return _Tofunft0.Contract.OPCOMPLETEAUCTION(&_Tofunft0.CallOpts)
}

// OPCOMPLETEBUY is a free data retrieval call binding the contract method 0xb50a2a55.
//
// Solidity: function OP_COMPLETE_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) OPCOMPLETEBUY(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "OP_COMPLETE_BUY")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// OPCOMPLETEBUY is a free data retrieval call binding the contract method 0xb50a2a55.
//
// Solidity: function OP_COMPLETE_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) OPCOMPLETEBUY() (uint8, error) {
	return _Tofunft0.Contract.OPCOMPLETEBUY(&_Tofunft0.CallOpts)
}

// OPCOMPLETEBUY is a free data retrieval call binding the contract method 0xb50a2a55.
//
// Solidity: function OP_COMPLETE_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) OPCOMPLETEBUY() (uint8, error) {
	return _Tofunft0.Contract.OPCOMPLETEBUY(&_Tofunft0.CallOpts)
}

// OPCOMPLETESELL is a free data retrieval call binding the contract method 0x8f18439e.
//
// Solidity: function OP_COMPLETE_SELL() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) OPCOMPLETESELL(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "OP_COMPLETE_SELL")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// OPCOMPLETESELL is a free data retrieval call binding the contract method 0x8f18439e.
//
// Solidity: function OP_COMPLETE_SELL() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) OPCOMPLETESELL() (uint8, error) {
	return _Tofunft0.Contract.OPCOMPLETESELL(&_Tofunft0.CallOpts)
}

// OPCOMPLETESELL is a free data retrieval call binding the contract method 0x8f18439e.
//
// Solidity: function OP_COMPLETE_SELL() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) OPCOMPLETESELL() (uint8, error) {
	return _Tofunft0.Contract.OPCOMPLETESELL(&_Tofunft0.CallOpts)
}

// OPMAX is a free data retrieval call binding the contract method 0xf0954160.
//
// Solidity: function OP_MAX() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) OPMAX(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "OP_MAX")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// OPMAX is a free data retrieval call binding the contract method 0xf0954160.
//
// Solidity: function OP_MAX() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) OPMAX() (uint8, error) {
	return _Tofunft0.Contract.OPMAX(&_Tofunft0.CallOpts)
}

// OPMAX is a free data retrieval call binding the contract method 0xf0954160.
//
// Solidity: function OP_MAX() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) OPMAX() (uint8, error) {
	return _Tofunft0.Contract.OPMAX(&_Tofunft0.CallOpts)
}

// OPMIN is a free data retrieval call binding the contract method 0x90c2b10e.
//
// Solidity: function OP_MIN() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) OPMIN(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "OP_MIN")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// OPMIN is a free data retrieval call binding the contract method 0x90c2b10e.
//
// Solidity: function OP_MIN() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) OPMIN() (uint8, error) {
	return _Tofunft0.Contract.OPMIN(&_Tofunft0.CallOpts)
}

// OPMIN is a free data retrieval call binding the contract method 0x90c2b10e.
//
// Solidity: function OP_MIN() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) OPMIN() (uint8, error) {
	return _Tofunft0.Contract.OPMIN(&_Tofunft0.CallOpts)
}

// OPREJECTBUY is a free data retrieval call binding the contract method 0x1bb03ca9.
//
// Solidity: function OP_REJECT_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) OPREJECTBUY(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "OP_REJECT_BUY")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// OPREJECTBUY is a free data retrieval call binding the contract method 0x1bb03ca9.
//
// Solidity: function OP_REJECT_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) OPREJECTBUY() (uint8, error) {
	return _Tofunft0.Contract.OPREJECTBUY(&_Tofunft0.CallOpts)
}

// OPREJECTBUY is a free data retrieval call binding the contract method 0x1bb03ca9.
//
// Solidity: function OP_REJECT_BUY() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) OPREJECTBUY() (uint8, error) {
	return _Tofunft0.Contract.OPREJECTBUY(&_Tofunft0.CallOpts)
}

// RATEBASE is a free data retrieval call binding the contract method 0x0873c6ec.
//
// Solidity: function RATE_BASE() view returns(uint256)
func (_Tofunft0 *Tofunft0Caller) RATEBASE(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "RATE_BASE")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// RATEBASE is a free data retrieval call binding the contract method 0x0873c6ec.
//
// Solidity: function RATE_BASE() view returns(uint256)
func (_Tofunft0 *Tofunft0Session) RATEBASE() (*big.Int, error) {
	return _Tofunft0.Contract.RATEBASE(&_Tofunft0.CallOpts)
}

// RATEBASE is a free data retrieval call binding the contract method 0x0873c6ec.
//
// Solidity: function RATE_BASE() view returns(uint256)
func (_Tofunft0 *Tofunft0CallerSession) RATEBASE() (*big.Int, error) {
	return _Tofunft0.Contract.RATEBASE(&_Tofunft0.CallOpts)
}

// STATUSCANCELLED is a free data retrieval call binding the contract method 0x5a4e5a15.
//
// Solidity: function STATUS_CANCELLED() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) STATUSCANCELLED(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "STATUS_CANCELLED")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// STATUSCANCELLED is a free data retrieval call binding the contract method 0x5a4e5a15.
//
// Solidity: function STATUS_CANCELLED() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) STATUSCANCELLED() (uint8, error) {
	return _Tofunft0.Contract.STATUSCANCELLED(&_Tofunft0.CallOpts)
}

// STATUSCANCELLED is a free data retrieval call binding the contract method 0x5a4e5a15.
//
// Solidity: function STATUS_CANCELLED() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) STATUSCANCELLED() (uint8, error) {
	return _Tofunft0.Contract.STATUSCANCELLED(&_Tofunft0.CallOpts)
}

// STATUSDONE is a free data retrieval call binding the contract method 0x740db280.
//
// Solidity: function STATUS_DONE() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) STATUSDONE(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "STATUS_DONE")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// STATUSDONE is a free data retrieval call binding the contract method 0x740db280.
//
// Solidity: function STATUS_DONE() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) STATUSDONE() (uint8, error) {
	return _Tofunft0.Contract.STATUSDONE(&_Tofunft0.CallOpts)
}

// STATUSDONE is a free data retrieval call binding the contract method 0x740db280.
//
// Solidity: function STATUS_DONE() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) STATUSDONE() (uint8, error) {
	return _Tofunft0.Contract.STATUSDONE(&_Tofunft0.CallOpts)
}

// STATUSOPEN is a free data retrieval call binding the contract method 0x24f8515b.
//
// Solidity: function STATUS_OPEN() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) STATUSOPEN(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "STATUS_OPEN")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// STATUSOPEN is a free data retrieval call binding the contract method 0x24f8515b.
//
// Solidity: function STATUS_OPEN() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) STATUSOPEN() (uint8, error) {
	return _Tofunft0.Contract.STATUSOPEN(&_Tofunft0.CallOpts)
}

// STATUSOPEN is a free data retrieval call binding the contract method 0x24f8515b.
//
// Solidity: function STATUS_OPEN() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) STATUSOPEN() (uint8, error) {
	return _Tofunft0.Contract.STATUSOPEN(&_Tofunft0.CallOpts)
}

// TOKEN1155 is a free data retrieval call binding the contract method 0xf0d250ba.
//
// Solidity: function TOKEN_1155() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) TOKEN1155(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "TOKEN_1155")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// TOKEN1155 is a free data retrieval call binding the contract method 0xf0d250ba.
//
// Solidity: function TOKEN_1155() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) TOKEN1155() (uint8, error) {
	return _Tofunft0.Contract.TOKEN1155(&_Tofunft0.CallOpts)
}

// TOKEN1155 is a free data retrieval call binding the contract method 0xf0d250ba.
//
// Solidity: function TOKEN_1155() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) TOKEN1155() (uint8, error) {
	return _Tofunft0.Contract.TOKEN1155(&_Tofunft0.CallOpts)
}

// TOKEN721 is a free data retrieval call binding the contract method 0xc477be20.
//
// Solidity: function TOKEN_721() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) TOKEN721(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "TOKEN_721")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// TOKEN721 is a free data retrieval call binding the contract method 0xc477be20.
//
// Solidity: function TOKEN_721() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) TOKEN721() (uint8, error) {
	return _Tofunft0.Contract.TOKEN721(&_Tofunft0.CallOpts)
}

// TOKEN721 is a free data retrieval call binding the contract method 0xc477be20.
//
// Solidity: function TOKEN_721() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) TOKEN721() (uint8, error) {
	return _Tofunft0.Contract.TOKEN721(&_Tofunft0.CallOpts)
}

// TOKENMINT is a free data retrieval call binding the contract method 0x853ca41a.
//
// Solidity: function TOKEN_MINT() view returns(uint8)
func (_Tofunft0 *Tofunft0Caller) TOKENMINT(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "TOKEN_MINT")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// TOKENMINT is a free data retrieval call binding the contract method 0x853ca41a.
//
// Solidity: function TOKEN_MINT() view returns(uint8)
func (_Tofunft0 *Tofunft0Session) TOKENMINT() (uint8, error) {
	return _Tofunft0.Contract.TOKENMINT(&_Tofunft0.CallOpts)
}

// TOKENMINT is a free data retrieval call binding the contract method 0x853ca41a.
//
// Solidity: function TOKEN_MINT() view returns(uint8)
func (_Tofunft0 *Tofunft0CallerSession) TOKENMINT() (uint8, error) {
	return _Tofunft0.Contract.TOKENMINT(&_Tofunft0.CallOpts)
}

// CouponSpent is a free data retrieval call binding the contract method 0x3ed9ffb7.
//
// Solidity: function couponSpent(uint256 ) view returns(bool)
func (_Tofunft0 *Tofunft0Caller) CouponSpent(opts *bind.CallOpts, arg0 *big.Int) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "couponSpent", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// CouponSpent is a free data retrieval call binding the contract method 0x3ed9ffb7.
//
// Solidity: function couponSpent(uint256 ) view returns(bool)
func (_Tofunft0 *Tofunft0Session) CouponSpent(arg0 *big.Int) (bool, error) {
	return _Tofunft0.Contract.CouponSpent(&_Tofunft0.CallOpts, arg0)
}

// CouponSpent is a free data retrieval call binding the contract method 0x3ed9ffb7.
//
// Solidity: function couponSpent(uint256 ) view returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) CouponSpent(arg0 *big.Int) (bool, error) {
	return _Tofunft0.Contract.CouponSpent(&_Tofunft0.CallOpts, arg0)
}

// HasInv is a free data retrieval call binding the contract method 0xf5116bc9.
//
// Solidity: function hasInv(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Caller) HasInv(opts *bind.CallOpts, id *big.Int) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "hasInv", id)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// HasInv is a free data retrieval call binding the contract method 0xf5116bc9.
//
// Solidity: function hasInv(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Session) HasInv(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.HasInv(&_Tofunft0.CallOpts, id)
}

// HasInv is a free data retrieval call binding the contract method 0xf5116bc9.
//
// Solidity: function hasInv(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) HasInv(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.HasInv(&_Tofunft0.CallOpts, id)
}

// HasSignedIntention is a free data retrieval call binding the contract method 0xac5e2cb1.
//
// Solidity: function hasSignedIntention(uint8 op) pure returns(bool)
func (_Tofunft0 *Tofunft0Caller) HasSignedIntention(opts *bind.CallOpts, op uint8) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "hasSignedIntention", op)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// HasSignedIntention is a free data retrieval call binding the contract method 0xac5e2cb1.
//
// Solidity: function hasSignedIntention(uint8 op) pure returns(bool)
func (_Tofunft0 *Tofunft0Session) HasSignedIntention(op uint8) (bool, error) {
	return _Tofunft0.Contract.HasSignedIntention(&_Tofunft0.CallOpts, op)
}

// HasSignedIntention is a free data retrieval call binding the contract method 0xac5e2cb1.
//
// Solidity: function hasSignedIntention(uint8 op) pure returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) HasSignedIntention(op uint8) (bool, error) {
	return _Tofunft0.Contract.HasSignedIntention(&_Tofunft0.CallOpts, op)
}

// Inventories is a free data retrieval call binding the contract method 0xcd78ba01.
//
// Solidity: function inventories(uint256 ) view returns(address seller, address buyer, address currency, uint256 price, uint256 netPrice, uint256 deadline, uint8 kind, uint8 status)
func (_Tofunft0 *Tofunft0Caller) Inventories(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Seller   common.Address
	Buyer    common.Address
	Currency common.Address
	Price    *big.Int
	NetPrice *big.Int
	Deadline *big.Int
	Kind     uint8
	Status   uint8
}, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "inventories", arg0)

	outstruct := new(struct {
		Seller   common.Address
		Buyer    common.Address
		Currency common.Address
		Price    *big.Int
		NetPrice *big.Int
		Deadline *big.Int
		Kind     uint8
		Status   uint8
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Seller = *abi.ConvertType(out[0], new(common.Address)).(*common.Address)
	outstruct.Buyer = *abi.ConvertType(out[1], new(common.Address)).(*common.Address)
	outstruct.Currency = *abi.ConvertType(out[2], new(common.Address)).(*common.Address)
	outstruct.Price = *abi.ConvertType(out[3], new(*big.Int)).(**big.Int)
	outstruct.NetPrice = *abi.ConvertType(out[4], new(*big.Int)).(**big.Int)
	outstruct.Deadline = *abi.ConvertType(out[5], new(*big.Int)).(**big.Int)
	outstruct.Kind = *abi.ConvertType(out[6], new(uint8)).(*uint8)
	outstruct.Status = *abi.ConvertType(out[7], new(uint8)).(*uint8)

	return *outstruct, err

}

// Inventories is a free data retrieval call binding the contract method 0xcd78ba01.
//
// Solidity: function inventories(uint256 ) view returns(address seller, address buyer, address currency, uint256 price, uint256 netPrice, uint256 deadline, uint8 kind, uint8 status)
func (_Tofunft0 *Tofunft0Session) Inventories(arg0 *big.Int) (struct {
	Seller   common.Address
	Buyer    common.Address
	Currency common.Address
	Price    *big.Int
	NetPrice *big.Int
	Deadline *big.Int
	Kind     uint8
	Status   uint8
}, error) {
	return _Tofunft0.Contract.Inventories(&_Tofunft0.CallOpts, arg0)
}

// Inventories is a free data retrieval call binding the contract method 0xcd78ba01.
//
// Solidity: function inventories(uint256 ) view returns(address seller, address buyer, address currency, uint256 price, uint256 netPrice, uint256 deadline, uint8 kind, uint8 status)
func (_Tofunft0 *Tofunft0CallerSession) Inventories(arg0 *big.Int) (struct {
	Seller   common.Address
	Buyer    common.Address
	Currency common.Address
	Price    *big.Int
	NetPrice *big.Int
	Deadline *big.Int
	Kind     uint8
	Status   uint8
}, error) {
	return _Tofunft0.Contract.Inventories(&_Tofunft0.CallOpts, arg0)
}

// InventoryTokenCounts is a free data retrieval call binding the contract method 0x5fd34298.
//
// Solidity: function inventoryTokenCounts(uint256 ) view returns(uint256)
func (_Tofunft0 *Tofunft0Caller) InventoryTokenCounts(opts *bind.CallOpts, arg0 *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "inventoryTokenCounts", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// InventoryTokenCounts is a free data retrieval call binding the contract method 0x5fd34298.
//
// Solidity: function inventoryTokenCounts(uint256 ) view returns(uint256)
func (_Tofunft0 *Tofunft0Session) InventoryTokenCounts(arg0 *big.Int) (*big.Int, error) {
	return _Tofunft0.Contract.InventoryTokenCounts(&_Tofunft0.CallOpts, arg0)
}

// InventoryTokenCounts is a free data retrieval call binding the contract method 0x5fd34298.
//
// Solidity: function inventoryTokenCounts(uint256 ) view returns(uint256)
func (_Tofunft0 *Tofunft0CallerSession) InventoryTokenCounts(arg0 *big.Int) (*big.Int, error) {
	return _Tofunft0.Contract.InventoryTokenCounts(&_Tofunft0.CallOpts, arg0)
}

// InventoryTokens is a free data retrieval call binding the contract method 0xb4533aad.
//
// Solidity: function inventoryTokens(uint256 , uint256 ) view returns(address token, uint256 tokenId, uint256 amount, uint8 kind, bytes mintData)
func (_Tofunft0 *Tofunft0Caller) InventoryTokens(opts *bind.CallOpts, arg0 *big.Int, arg1 *big.Int) (struct {
	Token    common.Address
	TokenId  *big.Int
	Amount   *big.Int
	Kind     uint8
	MintData []byte
}, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "inventoryTokens", arg0, arg1)

	outstruct := new(struct {
		Token    common.Address
		TokenId  *big.Int
		Amount   *big.Int
		Kind     uint8
		MintData []byte
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Token = *abi.ConvertType(out[0], new(common.Address)).(*common.Address)
	outstruct.TokenId = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.Amount = *abi.ConvertType(out[2], new(*big.Int)).(**big.Int)
	outstruct.Kind = *abi.ConvertType(out[3], new(uint8)).(*uint8)
	outstruct.MintData = *abi.ConvertType(out[4], new([]byte)).(*[]byte)

	return *outstruct, err

}

// InventoryTokens is a free data retrieval call binding the contract method 0xb4533aad.
//
// Solidity: function inventoryTokens(uint256 , uint256 ) view returns(address token, uint256 tokenId, uint256 amount, uint8 kind, bytes mintData)
func (_Tofunft0 *Tofunft0Session) InventoryTokens(arg0 *big.Int, arg1 *big.Int) (struct {
	Token    common.Address
	TokenId  *big.Int
	Amount   *big.Int
	Kind     uint8
	MintData []byte
}, error) {
	return _Tofunft0.Contract.InventoryTokens(&_Tofunft0.CallOpts, arg0, arg1)
}

// InventoryTokens is a free data retrieval call binding the contract method 0xb4533aad.
//
// Solidity: function inventoryTokens(uint256 , uint256 ) view returns(address token, uint256 tokenId, uint256 amount, uint8 kind, bytes mintData)
func (_Tofunft0 *Tofunft0CallerSession) InventoryTokens(arg0 *big.Int, arg1 *big.Int) (struct {
	Token    common.Address
	TokenId  *big.Int
	Amount   *big.Int
	Kind     uint8
	MintData []byte
}, error) {
	return _Tofunft0.Contract.InventoryTokens(&_Tofunft0.CallOpts, arg0, arg1)
}

// IsAuction is a free data retrieval call binding the contract method 0x8704f2a3.
//
// Solidity: function isAuction(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Caller) IsAuction(opts *bind.CallOpts, id *big.Int) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "isAuction", id)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsAuction is a free data retrieval call binding the contract method 0x8704f2a3.
//
// Solidity: function isAuction(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Session) IsAuction(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsAuction(&_Tofunft0.CallOpts, id)
}

// IsAuction is a free data retrieval call binding the contract method 0x8704f2a3.
//
// Solidity: function isAuction(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) IsAuction(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsAuction(&_Tofunft0.CallOpts, id)
}

// IsAuctionOpen is a free data retrieval call binding the contract method 0x0ad48628.
//
// Solidity: function isAuctionOpen(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Caller) IsAuctionOpen(opts *bind.CallOpts, id *big.Int) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "isAuctionOpen", id)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsAuctionOpen is a free data retrieval call binding the contract method 0x0ad48628.
//
// Solidity: function isAuctionOpen(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Session) IsAuctionOpen(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsAuctionOpen(&_Tofunft0.CallOpts, id)
}

// IsAuctionOpen is a free data retrieval call binding the contract method 0x0ad48628.
//
// Solidity: function isAuctionOpen(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) IsAuctionOpen(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsAuctionOpen(&_Tofunft0.CallOpts, id)
}

// IsBundleApproved is a free data retrieval call binding the contract method 0xf4a33e0d.
//
// Solidity: function isBundleApproved(uint256 invId, address owner) view returns(bool)
func (_Tofunft0 *Tofunft0Caller) IsBundleApproved(opts *bind.CallOpts, invId *big.Int, owner common.Address) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "isBundleApproved", invId, owner)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsBundleApproved is a free data retrieval call binding the contract method 0xf4a33e0d.
//
// Solidity: function isBundleApproved(uint256 invId, address owner) view returns(bool)
func (_Tofunft0 *Tofunft0Session) IsBundleApproved(invId *big.Int, owner common.Address) (bool, error) {
	return _Tofunft0.Contract.IsBundleApproved(&_Tofunft0.CallOpts, invId, owner)
}

// IsBundleApproved is a free data retrieval call binding the contract method 0xf4a33e0d.
//
// Solidity: function isBundleApproved(uint256 invId, address owner) view returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) IsBundleApproved(invId *big.Int, owner common.Address) (bool, error) {
	return _Tofunft0.Contract.IsBundleApproved(&_Tofunft0.CallOpts, invId, owner)
}

// IsBuy is a free data retrieval call binding the contract method 0xa80d33fb.
//
// Solidity: function isBuy(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Caller) IsBuy(opts *bind.CallOpts, id *big.Int) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "isBuy", id)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsBuy is a free data retrieval call binding the contract method 0xa80d33fb.
//
// Solidity: function isBuy(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Session) IsBuy(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsBuy(&_Tofunft0.CallOpts, id)
}

// IsBuy is a free data retrieval call binding the contract method 0xa80d33fb.
//
// Solidity: function isBuy(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) IsBuy(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsBuy(&_Tofunft0.CallOpts, id)
}

// IsBuyOpen is a free data retrieval call binding the contract method 0xbdf52b45.
//
// Solidity: function isBuyOpen(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Caller) IsBuyOpen(opts *bind.CallOpts, id *big.Int) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "isBuyOpen", id)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsBuyOpen is a free data retrieval call binding the contract method 0xbdf52b45.
//
// Solidity: function isBuyOpen(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Session) IsBuyOpen(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsBuyOpen(&_Tofunft0.CallOpts, id)
}

// IsBuyOpen is a free data retrieval call binding the contract method 0xbdf52b45.
//
// Solidity: function isBuyOpen(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) IsBuyOpen(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsBuyOpen(&_Tofunft0.CallOpts, id)
}

// IsExpired is a free data retrieval call binding the contract method 0xd9548e53.
//
// Solidity: function isExpired(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Caller) IsExpired(opts *bind.CallOpts, id *big.Int) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "isExpired", id)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsExpired is a free data retrieval call binding the contract method 0xd9548e53.
//
// Solidity: function isExpired(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Session) IsExpired(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsExpired(&_Tofunft0.CallOpts, id)
}

// IsExpired is a free data retrieval call binding the contract method 0xd9548e53.
//
// Solidity: function isExpired(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) IsExpired(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsExpired(&_Tofunft0.CallOpts, id)
}

// IsSell is a free data retrieval call binding the contract method 0x1b01e72c.
//
// Solidity: function isSell(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Caller) IsSell(opts *bind.CallOpts, id *big.Int) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "isSell", id)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsSell is a free data retrieval call binding the contract method 0x1b01e72c.
//
// Solidity: function isSell(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Session) IsSell(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsSell(&_Tofunft0.CallOpts, id)
}

// IsSell is a free data retrieval call binding the contract method 0x1b01e72c.
//
// Solidity: function isSell(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) IsSell(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsSell(&_Tofunft0.CallOpts, id)
}

// IsSignatureValid is a free data retrieval call binding the contract method 0x781dc70a.
//
// Solidity: function isSignatureValid(bytes signature, bytes32 hash, address signer) pure returns(bool)
func (_Tofunft0 *Tofunft0Caller) IsSignatureValid(opts *bind.CallOpts, signature []byte, hash [32]byte, signer common.Address) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "isSignatureValid", signature, hash, signer)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsSignatureValid is a free data retrieval call binding the contract method 0x781dc70a.
//
// Solidity: function isSignatureValid(bytes signature, bytes32 hash, address signer) pure returns(bool)
func (_Tofunft0 *Tofunft0Session) IsSignatureValid(signature []byte, hash [32]byte, signer common.Address) (bool, error) {
	return _Tofunft0.Contract.IsSignatureValid(&_Tofunft0.CallOpts, signature, hash, signer)
}

// IsSignatureValid is a free data retrieval call binding the contract method 0x781dc70a.
//
// Solidity: function isSignatureValid(bytes signature, bytes32 hash, address signer) pure returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) IsSignatureValid(signature []byte, hash [32]byte, signer common.Address) (bool, error) {
	return _Tofunft0.Contract.IsSignatureValid(&_Tofunft0.CallOpts, signature, hash, signer)
}

// IsStatusOpen is a free data retrieval call binding the contract method 0xee98ce91.
//
// Solidity: function isStatusOpen(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Caller) IsStatusOpen(opts *bind.CallOpts, id *big.Int) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "isStatusOpen", id)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsStatusOpen is a free data retrieval call binding the contract method 0xee98ce91.
//
// Solidity: function isStatusOpen(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0Session) IsStatusOpen(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsStatusOpen(&_Tofunft0.CallOpts, id)
}

// IsStatusOpen is a free data retrieval call binding the contract method 0xee98ce91.
//
// Solidity: function isStatusOpen(uint256 id) view returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) IsStatusOpen(id *big.Int) (bool, error) {
	return _Tofunft0.Contract.IsStatusOpen(&_Tofunft0.CallOpts, id)
}

// MarketSigners is a free data retrieval call binding the contract method 0x2bcd27df.
//
// Solidity: function marketSigners(address ) view returns(bool)
func (_Tofunft0 *Tofunft0Caller) MarketSigners(opts *bind.CallOpts, arg0 common.Address) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "marketSigners", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// MarketSigners is a free data retrieval call binding the contract method 0x2bcd27df.
//
// Solidity: function marketSigners(address ) view returns(bool)
func (_Tofunft0 *Tofunft0Session) MarketSigners(arg0 common.Address) (bool, error) {
	return _Tofunft0.Contract.MarketSigners(&_Tofunft0.CallOpts, arg0)
}

// MarketSigners is a free data retrieval call binding the contract method 0x2bcd27df.
//
// Solidity: function marketSigners(address ) view returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) MarketSigners(arg0 common.Address) (bool, error) {
	return _Tofunft0.Contract.MarketSigners(&_Tofunft0.CallOpts, arg0)
}

// MinAuctionDuration is a free data retrieval call binding the contract method 0x54134876.
//
// Solidity: function minAuctionDuration() view returns(uint256)
func (_Tofunft0 *Tofunft0Caller) MinAuctionDuration(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "minAuctionDuration")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MinAuctionDuration is a free data retrieval call binding the contract method 0x54134876.
//
// Solidity: function minAuctionDuration() view returns(uint256)
func (_Tofunft0 *Tofunft0Session) MinAuctionDuration() (*big.Int, error) {
	return _Tofunft0.Contract.MinAuctionDuration(&_Tofunft0.CallOpts)
}

// MinAuctionDuration is a free data retrieval call binding the contract method 0x54134876.
//
// Solidity: function minAuctionDuration() view returns(uint256)
func (_Tofunft0 *Tofunft0CallerSession) MinAuctionDuration() (*big.Int, error) {
	return _Tofunft0.Contract.MinAuctionDuration(&_Tofunft0.CallOpts)
}

// MinAuctionIncrement is a free data retrieval call binding the contract method 0x708d4d35.
//
// Solidity: function minAuctionIncrement() view returns(uint256)
func (_Tofunft0 *Tofunft0Caller) MinAuctionIncrement(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "minAuctionIncrement")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MinAuctionIncrement is a free data retrieval call binding the contract method 0x708d4d35.
//
// Solidity: function minAuctionIncrement() view returns(uint256)
func (_Tofunft0 *Tofunft0Session) MinAuctionIncrement() (*big.Int, error) {
	return _Tofunft0.Contract.MinAuctionIncrement(&_Tofunft0.CallOpts)
}

// MinAuctionIncrement is a free data retrieval call binding the contract method 0x708d4d35.
//
// Solidity: function minAuctionIncrement() view returns(uint256)
func (_Tofunft0 *Tofunft0CallerSession) MinAuctionIncrement() (*big.Int, error) {
	return _Tofunft0.Contract.MinAuctionIncrement(&_Tofunft0.CallOpts)
}

// OnERC1155BatchReceived is a free data retrieval call binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address operator, address from, uint256[] ids, uint256[] values, bytes data) view returns(bytes4)
func (_Tofunft0 *Tofunft0Caller) OnERC1155BatchReceived(opts *bind.CallOpts, operator common.Address, from common.Address, ids []*big.Int, values []*big.Int, data []byte) ([4]byte, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "onERC1155BatchReceived", operator, from, ids, values, data)

	if err != nil {
		return *new([4]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([4]byte)).(*[4]byte)

	return out0, err

}

// OnERC1155BatchReceived is a free data retrieval call binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address operator, address from, uint256[] ids, uint256[] values, bytes data) view returns(bytes4)
func (_Tofunft0 *Tofunft0Session) OnERC1155BatchReceived(operator common.Address, from common.Address, ids []*big.Int, values []*big.Int, data []byte) ([4]byte, error) {
	return _Tofunft0.Contract.OnERC1155BatchReceived(&_Tofunft0.CallOpts, operator, from, ids, values, data)
}

// OnERC1155BatchReceived is a free data retrieval call binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address operator, address from, uint256[] ids, uint256[] values, bytes data) view returns(bytes4)
func (_Tofunft0 *Tofunft0CallerSession) OnERC1155BatchReceived(operator common.Address, from common.Address, ids []*big.Int, values []*big.Int, data []byte) ([4]byte, error) {
	return _Tofunft0.Contract.OnERC1155BatchReceived(&_Tofunft0.CallOpts, operator, from, ids, values, data)
}

// OnERC1155Received is a free data retrieval call binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address operator, address from, uint256 id, uint256 value, bytes data) view returns(bytes4)
func (_Tofunft0 *Tofunft0Caller) OnERC1155Received(opts *bind.CallOpts, operator common.Address, from common.Address, id *big.Int, value *big.Int, data []byte) ([4]byte, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "onERC1155Received", operator, from, id, value, data)

	if err != nil {
		return *new([4]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([4]byte)).(*[4]byte)

	return out0, err

}

// OnERC1155Received is a free data retrieval call binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address operator, address from, uint256 id, uint256 value, bytes data) view returns(bytes4)
func (_Tofunft0 *Tofunft0Session) OnERC1155Received(operator common.Address, from common.Address, id *big.Int, value *big.Int, data []byte) ([4]byte, error) {
	return _Tofunft0.Contract.OnERC1155Received(&_Tofunft0.CallOpts, operator, from, id, value, data)
}

// OnERC1155Received is a free data retrieval call binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address operator, address from, uint256 id, uint256 value, bytes data) view returns(bytes4)
func (_Tofunft0 *Tofunft0CallerSession) OnERC1155Received(operator common.Address, from common.Address, id *big.Int, value *big.Int, data []byte) ([4]byte, error) {
	return _Tofunft0.Contract.OnERC1155Received(&_Tofunft0.CallOpts, operator, from, id, value, data)
}

// OnERC721Received is a free data retrieval call binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address operator, address from, uint256 tokenId, bytes data) view returns(bytes4)
func (_Tofunft0 *Tofunft0Caller) OnERC721Received(opts *bind.CallOpts, operator common.Address, from common.Address, tokenId *big.Int, data []byte) ([4]byte, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "onERC721Received", operator, from, tokenId, data)

	if err != nil {
		return *new([4]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([4]byte)).(*[4]byte)

	return out0, err

}

// OnERC721Received is a free data retrieval call binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address operator, address from, uint256 tokenId, bytes data) view returns(bytes4)
func (_Tofunft0 *Tofunft0Session) OnERC721Received(operator common.Address, from common.Address, tokenId *big.Int, data []byte) ([4]byte, error) {
	return _Tofunft0.Contract.OnERC721Received(&_Tofunft0.CallOpts, operator, from, tokenId, data)
}

// OnERC721Received is a free data retrieval call binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address operator, address from, uint256 tokenId, bytes data) view returns(bytes4)
func (_Tofunft0 *Tofunft0CallerSession) OnERC721Received(operator common.Address, from common.Address, tokenId *big.Int, data []byte) ([4]byte, error) {
	return _Tofunft0.Contract.OnERC721Received(&_Tofunft0.CallOpts, operator, from, tokenId, data)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Tofunft0 *Tofunft0Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Tofunft0 *Tofunft0Session) Owner() (common.Address, error) {
	return _Tofunft0.Contract.Owner(&_Tofunft0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Tofunft0 *Tofunft0CallerSession) Owner() (common.Address, error) {
	return _Tofunft0.Contract.Owner(&_Tofunft0.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Tofunft0 *Tofunft0Caller) Paused(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "paused")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Tofunft0 *Tofunft0Session) Paused() (bool, error) {
	return _Tofunft0.Contract.Paused(&_Tofunft0.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) Paused() (bool, error) {
	return _Tofunft0.Contract.Paused(&_Tofunft0.CallOpts)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) pure returns(bool)
func (_Tofunft0 *Tofunft0Caller) SupportsInterface(opts *bind.CallOpts, interfaceId [4]byte) (bool, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "supportsInterface", interfaceId)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) pure returns(bool)
func (_Tofunft0 *Tofunft0Session) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _Tofunft0.Contract.SupportsInterface(&_Tofunft0.CallOpts, interfaceId)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) pure returns(bool)
func (_Tofunft0 *Tofunft0CallerSession) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _Tofunft0.Contract.SupportsInterface(&_Tofunft0.CallOpts, interfaceId)
}

// Weth is a free data retrieval call binding the contract method 0x3fc8cef3.
//
// Solidity: function weth() view returns(address)
func (_Tofunft0 *Tofunft0Caller) Weth(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Tofunft0.contract.Call(opts, &out, "weth")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Weth is a free data retrieval call binding the contract method 0x3fc8cef3.
//
// Solidity: function weth() view returns(address)
func (_Tofunft0 *Tofunft0Session) Weth() (common.Address, error) {
	return _Tofunft0.Contract.Weth(&_Tofunft0.CallOpts)
}

// Weth is a free data retrieval call binding the contract method 0x3fc8cef3.
//
// Solidity: function weth() view returns(address)
func (_Tofunft0 *Tofunft0CallerSession) Weth() (common.Address, error) {
	return _Tofunft0.Contract.Weth(&_Tofunft0.CallOpts)
}

// CancelBuys is a paid mutator transaction binding the contract method 0xc1c30e80.
//
// Solidity: function cancelBuys(uint256[] ids) returns()
func (_Tofunft0 *Tofunft0Transactor) CancelBuys(opts *bind.TransactOpts, ids []*big.Int) (*types.Transaction, error) {
	return _Tofunft0.contract.Transact(opts, "cancelBuys", ids)
}

// CancelBuys is a paid mutator transaction binding the contract method 0xc1c30e80.
//
// Solidity: function cancelBuys(uint256[] ids) returns()
func (_Tofunft0 *Tofunft0Session) CancelBuys(ids []*big.Int) (*types.Transaction, error) {
	return _Tofunft0.Contract.CancelBuys(&_Tofunft0.TransactOpts, ids)
}

// CancelBuys is a paid mutator transaction binding the contract method 0xc1c30e80.
//
// Solidity: function cancelBuys(uint256[] ids) returns()
func (_Tofunft0 *Tofunft0TransactorSession) CancelBuys(ids []*big.Int) (*types.Transaction, error) {
	return _Tofunft0.Contract.CancelBuys(&_Tofunft0.TransactOpts, ids)
}

// EmergencyCancelAuction is a paid mutator transaction binding the contract method 0xe7d4a999.
//
// Solidity: function emergencyCancelAuction(uint256 id, bool noBundle) returns()
func (_Tofunft0 *Tofunft0Transactor) EmergencyCancelAuction(opts *bind.TransactOpts, id *big.Int, noBundle bool) (*types.Transaction, error) {
	return _Tofunft0.contract.Transact(opts, "emergencyCancelAuction", id, noBundle)
}

// EmergencyCancelAuction is a paid mutator transaction binding the contract method 0xe7d4a999.
//
// Solidity: function emergencyCancelAuction(uint256 id, bool noBundle) returns()
func (_Tofunft0 *Tofunft0Session) EmergencyCancelAuction(id *big.Int, noBundle bool) (*types.Transaction, error) {
	return _Tofunft0.Contract.EmergencyCancelAuction(&_Tofunft0.TransactOpts, id, noBundle)
}

// EmergencyCancelAuction is a paid mutator transaction binding the contract method 0xe7d4a999.
//
// Solidity: function emergencyCancelAuction(uint256 id, bool noBundle) returns()
func (_Tofunft0 *Tofunft0TransactorSession) EmergencyCancelAuction(id *big.Int, noBundle bool) (*types.Transaction, error) {
	return _Tofunft0.Contract.EmergencyCancelAuction(&_Tofunft0.TransactOpts, id, noBundle)
}

// InCaseMoneyGetsStuck is a paid mutator transaction binding the contract method 0x80bc688f.
//
// Solidity: function inCaseMoneyGetsStuck(address to, address currency, uint256 amount) returns()
func (_Tofunft0 *Tofunft0Transactor) InCaseMoneyGetsStuck(opts *bind.TransactOpts, to common.Address, currency common.Address, amount *big.Int) (*types.Transaction, error) {
	return _Tofunft0.contract.Transact(opts, "inCaseMoneyGetsStuck", to, currency, amount)
}

// InCaseMoneyGetsStuck is a paid mutator transaction binding the contract method 0x80bc688f.
//
// Solidity: function inCaseMoneyGetsStuck(address to, address currency, uint256 amount) returns()
func (_Tofunft0 *Tofunft0Session) InCaseMoneyGetsStuck(to common.Address, currency common.Address, amount *big.Int) (*types.Transaction, error) {
	return _Tofunft0.Contract.InCaseMoneyGetsStuck(&_Tofunft0.TransactOpts, to, currency, amount)
}

// InCaseMoneyGetsStuck is a paid mutator transaction binding the contract method 0x80bc688f.
//
// Solidity: function inCaseMoneyGetsStuck(address to, address currency, uint256 amount) returns()
func (_Tofunft0 *Tofunft0TransactorSession) InCaseMoneyGetsStuck(to common.Address, currency common.Address, amount *big.Int) (*types.Transaction, error) {
	return _Tofunft0.Contract.InCaseMoneyGetsStuck(&_Tofunft0.TransactOpts, to, currency, amount)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_Tofunft0 *Tofunft0Transactor) Pause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Tofunft0.contract.Transact(opts, "pause")
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_Tofunft0 *Tofunft0Session) Pause() (*types.Transaction, error) {
	return _Tofunft0.Contract.Pause(&_Tofunft0.TransactOpts)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_Tofunft0 *Tofunft0TransactorSession) Pause() (*types.Transaction, error) {
	return _Tofunft0.Contract.Pause(&_Tofunft0.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Tofunft0 *Tofunft0Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Tofunft0.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Tofunft0 *Tofunft0Session) RenounceOwnership() (*types.Transaction, error) {
	return _Tofunft0.Contract.RenounceOwnership(&_Tofunft0.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Tofunft0 *Tofunft0TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Tofunft0.Contract.RenounceOwnership(&_Tofunft0.TransactOpts)
}

// Run is a paid mutator transaction binding the contract method 0xba847759.
//
// Solidity: function run((address,(address,uint256,uint256,uint8,bytes)[],address,uint256,uint256,bytes32,uint8) intent, (bytes32,address,uint256,bytes32,uint256,uint8,address,address,uint256,uint256,(uint256[],uint256,uint256,uint256,address,address),(address,uint256,uint256,uint8,bytes)[],uint256) detail, bytes sigIntent, bytes sigDetail) payable returns()
func (_Tofunft0 *Tofunft0Transactor) Run(opts *bind.TransactOpts, intent MarketNGIntention, detail MarketNGDetail, sigIntent []byte, sigDetail []byte) (*types.Transaction, error) {
	return _Tofunft0.contract.Transact(opts, "run", intent, detail, sigIntent, sigDetail)
}

// Run is a paid mutator transaction binding the contract method 0xba847759.
//
// Solidity: function run((address,(address,uint256,uint256,uint8,bytes)[],address,uint256,uint256,bytes32,uint8) intent, (bytes32,address,uint256,bytes32,uint256,uint8,address,address,uint256,uint256,(uint256[],uint256,uint256,uint256,address,address),(address,uint256,uint256,uint8,bytes)[],uint256) detail, bytes sigIntent, bytes sigDetail) payable returns()
func (_Tofunft0 *Tofunft0Session) Run(intent MarketNGIntention, detail MarketNGDetail, sigIntent []byte, sigDetail []byte) (*types.Transaction, error) {
	return _Tofunft0.Contract.Run(&_Tofunft0.TransactOpts, intent, detail, sigIntent, sigDetail)
}

// Run is a paid mutator transaction binding the contract method 0xba847759.
//
// Solidity: function run((address,(address,uint256,uint256,uint8,bytes)[],address,uint256,uint256,bytes32,uint8) intent, (bytes32,address,uint256,bytes32,uint256,uint8,address,address,uint256,uint256,(uint256[],uint256,uint256,uint256,address,address),(address,uint256,uint256,uint8,bytes)[],uint256) detail, bytes sigIntent, bytes sigDetail) payable returns()
func (_Tofunft0 *Tofunft0TransactorSession) Run(intent MarketNGIntention, detail MarketNGDetail, sigIntent []byte, sigDetail []byte) (*types.Transaction, error) {
	return _Tofunft0.Contract.Run(&_Tofunft0.TransactOpts, intent, detail, sigIntent, sigDetail)
}

// Send is a paid mutator transaction binding the contract method 0xafd76a0b.
//
// Solidity: function send(address to, (address,uint256)[] tokens) returns()
func (_Tofunft0 *Tofunft0Transactor) Send(opts *bind.TransactOpts, to common.Address, tokens []MarketNGPair721) (*types.Transaction, error) {
	return _Tofunft0.contract.Transact(opts, "send", to, tokens)
}

// Send is a paid mutator transaction binding the contract method 0xafd76a0b.
//
// Solidity: function send(address to, (address,uint256)[] tokens) returns()
func (_Tofunft0 *Tofunft0Session) Send(to common.Address, tokens []MarketNGPair721) (*types.Transaction, error) {
	return _Tofunft0.Contract.Send(&_Tofunft0.TransactOpts, to, tokens)
}

// Send is a paid mutator transaction binding the contract method 0xafd76a0b.
//
// Solidity: function send(address to, (address,uint256)[] tokens) returns()
func (_Tofunft0 *Tofunft0TransactorSession) Send(to common.Address, tokens []MarketNGPair721) (*types.Transaction, error) {
	return _Tofunft0.Contract.Send(&_Tofunft0.TransactOpts, to, tokens)
}

// Swap is a paid mutator transaction binding the contract method 0xe91274f3.
//
// Solidity: function swap((bytes32,address,uint256,(address,uint256)[],(address,uint256)[]) req, bytes signature) returns()
func (_Tofunft0 *Tofunft0Transactor) Swap(opts *bind.TransactOpts, req MarketNGSwap, signature []byte) (*types.Transaction, error) {
	return _Tofunft0.contract.Transact(opts, "swap", req, signature)
}

// Swap is a paid mutator transaction binding the contract method 0xe91274f3.
//
// Solidity: function swap((bytes32,address,uint256,(address,uint256)[],(address,uint256)[]) req, bytes signature) returns()
func (_Tofunft0 *Tofunft0Session) Swap(req MarketNGSwap, signature []byte) (*types.Transaction, error) {
	return _Tofunft0.Contract.Swap(&_Tofunft0.TransactOpts, req, signature)
}

// Swap is a paid mutator transaction binding the contract method 0xe91274f3.
//
// Solidity: function swap((bytes32,address,uint256,(address,uint256)[],(address,uint256)[]) req, bytes signature) returns()
func (_Tofunft0 *Tofunft0TransactorSession) Swap(req MarketNGSwap, signature []byte) (*types.Transaction, error) {
	return _Tofunft0.Contract.Swap(&_Tofunft0.TransactOpts, req, signature)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Tofunft0 *Tofunft0Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Tofunft0.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Tofunft0 *Tofunft0Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Tofunft0.Contract.TransferOwnership(&_Tofunft0.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Tofunft0 *Tofunft0TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Tofunft0.Contract.TransferOwnership(&_Tofunft0.TransactOpts, newOwner)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_Tofunft0 *Tofunft0Transactor) Unpause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Tofunft0.contract.Transact(opts, "unpause")
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_Tofunft0 *Tofunft0Session) Unpause() (*types.Transaction, error) {
	return _Tofunft0.Contract.Unpause(&_Tofunft0.TransactOpts)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_Tofunft0 *Tofunft0TransactorSession) Unpause() (*types.Transaction, error) {
	return _Tofunft0.Contract.Unpause(&_Tofunft0.TransactOpts)
}

// UpdateSettings is a paid mutator transaction binding the contract method 0x015af8ee.
//
// Solidity: function updateSettings(uint256 minAuctionIncrement_, uint256 minAuctionDuration_) returns()
func (_Tofunft0 *Tofunft0Transactor) UpdateSettings(opts *bind.TransactOpts, minAuctionIncrement_ *big.Int, minAuctionDuration_ *big.Int) (*types.Transaction, error) {
	return _Tofunft0.contract.Transact(opts, "updateSettings", minAuctionIncrement_, minAuctionDuration_)
}

// UpdateSettings is a paid mutator transaction binding the contract method 0x015af8ee.
//
// Solidity: function updateSettings(uint256 minAuctionIncrement_, uint256 minAuctionDuration_) returns()
func (_Tofunft0 *Tofunft0Session) UpdateSettings(minAuctionIncrement_ *big.Int, minAuctionDuration_ *big.Int) (*types.Transaction, error) {
	return _Tofunft0.Contract.UpdateSettings(&_Tofunft0.TransactOpts, minAuctionIncrement_, minAuctionDuration_)
}

// UpdateSettings is a paid mutator transaction binding the contract method 0x015af8ee.
//
// Solidity: function updateSettings(uint256 minAuctionIncrement_, uint256 minAuctionDuration_) returns()
func (_Tofunft0 *Tofunft0TransactorSession) UpdateSettings(minAuctionIncrement_ *big.Int, minAuctionDuration_ *big.Int) (*types.Transaction, error) {
	return _Tofunft0.Contract.UpdateSettings(&_Tofunft0.TransactOpts, minAuctionIncrement_, minAuctionDuration_)
}

// UpdateSigner is a paid mutator transaction binding the contract method 0xf460590b.
//
// Solidity: function updateSigner(address addr, bool remove) returns()
func (_Tofunft0 *Tofunft0Transactor) UpdateSigner(opts *bind.TransactOpts, addr common.Address, remove bool) (*types.Transaction, error) {
	return _Tofunft0.contract.Transact(opts, "updateSigner", addr, remove)
}

// UpdateSigner is a paid mutator transaction binding the contract method 0xf460590b.
//
// Solidity: function updateSigner(address addr, bool remove) returns()
func (_Tofunft0 *Tofunft0Session) UpdateSigner(addr common.Address, remove bool) (*types.Transaction, error) {
	return _Tofunft0.Contract.UpdateSigner(&_Tofunft0.TransactOpts, addr, remove)
}

// UpdateSigner is a paid mutator transaction binding the contract method 0xf460590b.
//
// Solidity: function updateSigner(address addr, bool remove) returns()
func (_Tofunft0 *Tofunft0TransactorSession) UpdateSigner(addr common.Address, remove bool) (*types.Transaction, error) {
	return _Tofunft0.Contract.UpdateSigner(&_Tofunft0.TransactOpts, addr, remove)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Tofunft0 *Tofunft0Transactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Tofunft0.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Tofunft0 *Tofunft0Session) Receive() (*types.Transaction, error) {
	return _Tofunft0.Contract.Receive(&_Tofunft0.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Tofunft0 *Tofunft0TransactorSession) Receive() (*types.Transaction, error) {
	return _Tofunft0.Contract.Receive(&_Tofunft0.TransactOpts)
}

// Tofunft0EvAuctionRefundIterator is returned from FilterEvAuctionRefund and is used to iterate over the raw logs and unpacked data for EvAuctionRefund events raised by the Tofunft0 contract.
type Tofunft0EvAuctionRefundIterator struct {
	Event *Tofunft0EvAuctionRefund // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Tofunft0EvAuctionRefundIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Tofunft0EvAuctionRefund)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Tofunft0EvAuctionRefund)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Tofunft0EvAuctionRefundIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Tofunft0EvAuctionRefundIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Tofunft0EvAuctionRefund represents a EvAuctionRefund event raised by the Tofunft0 contract.
type Tofunft0EvAuctionRefund struct {
	Id     *big.Int
	Bidder common.Address
	Refund *big.Int
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterEvAuctionRefund is a free log retrieval operation binding the contract event 0xa48bcf3362c21033397c03b92fb367d1962ba13b5bde0dfe491f9d88abb59e3f.
//
// Solidity: event EvAuctionRefund(uint256 indexed id, address bidder, uint256 refund)
func (_Tofunft0 *Tofunft0Filterer) FilterEvAuctionRefund(opts *bind.FilterOpts, id []*big.Int) (*Tofunft0EvAuctionRefundIterator, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _Tofunft0.contract.FilterLogs(opts, "EvAuctionRefund", idRule)
	if err != nil {
		return nil, err
	}
	return &Tofunft0EvAuctionRefundIterator{contract: _Tofunft0.contract, event: "EvAuctionRefund", logs: logs, sub: sub}, nil
}

// WatchEvAuctionRefund is a free log subscription operation binding the contract event 0xa48bcf3362c21033397c03b92fb367d1962ba13b5bde0dfe491f9d88abb59e3f.
//
// Solidity: event EvAuctionRefund(uint256 indexed id, address bidder, uint256 refund)
func (_Tofunft0 *Tofunft0Filterer) WatchEvAuctionRefund(opts *bind.WatchOpts, sink chan<- *Tofunft0EvAuctionRefund, id []*big.Int) (event.Subscription, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _Tofunft0.contract.WatchLogs(opts, "EvAuctionRefund", idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Tofunft0EvAuctionRefund)
				if err := _Tofunft0.contract.UnpackLog(event, "EvAuctionRefund", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEvAuctionRefund is a log parse operation binding the contract event 0xa48bcf3362c21033397c03b92fb367d1962ba13b5bde0dfe491f9d88abb59e3f.
//
// Solidity: event EvAuctionRefund(uint256 indexed id, address bidder, uint256 refund)
func (_Tofunft0 *Tofunft0Filterer) ParseEvAuctionRefund(log types.Log) (*Tofunft0EvAuctionRefund, error) {
	event := new(Tofunft0EvAuctionRefund)
	if err := _Tofunft0.contract.UnpackLog(event, "EvAuctionRefund", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Tofunft0EvCouponSpentIterator is returned from FilterEvCouponSpent and is used to iterate over the raw logs and unpacked data for EvCouponSpent events raised by the Tofunft0 contract.
type Tofunft0EvCouponSpentIterator struct {
	Event *Tofunft0EvCouponSpent // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Tofunft0EvCouponSpentIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Tofunft0EvCouponSpent)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Tofunft0EvCouponSpent)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Tofunft0EvCouponSpentIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Tofunft0EvCouponSpentIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Tofunft0EvCouponSpent represents a EvCouponSpent event raised by the Tofunft0 contract.
type Tofunft0EvCouponSpent struct {
	Id       *big.Int
	CouponId *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterEvCouponSpent is a free log retrieval operation binding the contract event 0x6aa71aa6b7aa6036ace4e4ceefbab7d89c4afb7fcfa1a3680499d7b37d32c82f.
//
// Solidity: event EvCouponSpent(uint256 indexed id, uint256 indexed couponId)
func (_Tofunft0 *Tofunft0Filterer) FilterEvCouponSpent(opts *bind.FilterOpts, id []*big.Int, couponId []*big.Int) (*Tofunft0EvCouponSpentIterator, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}
	var couponIdRule []interface{}
	for _, couponIdItem := range couponId {
		couponIdRule = append(couponIdRule, couponIdItem)
	}

	logs, sub, err := _Tofunft0.contract.FilterLogs(opts, "EvCouponSpent", idRule, couponIdRule)
	if err != nil {
		return nil, err
	}
	return &Tofunft0EvCouponSpentIterator{contract: _Tofunft0.contract, event: "EvCouponSpent", logs: logs, sub: sub}, nil
}

// WatchEvCouponSpent is a free log subscription operation binding the contract event 0x6aa71aa6b7aa6036ace4e4ceefbab7d89c4afb7fcfa1a3680499d7b37d32c82f.
//
// Solidity: event EvCouponSpent(uint256 indexed id, uint256 indexed couponId)
func (_Tofunft0 *Tofunft0Filterer) WatchEvCouponSpent(opts *bind.WatchOpts, sink chan<- *Tofunft0EvCouponSpent, id []*big.Int, couponId []*big.Int) (event.Subscription, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}
	var couponIdRule []interface{}
	for _, couponIdItem := range couponId {
		couponIdRule = append(couponIdRule, couponIdItem)
	}

	logs, sub, err := _Tofunft0.contract.WatchLogs(opts, "EvCouponSpent", idRule, couponIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Tofunft0EvCouponSpent)
				if err := _Tofunft0.contract.UnpackLog(event, "EvCouponSpent", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEvCouponSpent is a log parse operation binding the contract event 0x6aa71aa6b7aa6036ace4e4ceefbab7d89c4afb7fcfa1a3680499d7b37d32c82f.
//
// Solidity: event EvCouponSpent(uint256 indexed id, uint256 indexed couponId)
func (_Tofunft0 *Tofunft0Filterer) ParseEvCouponSpent(log types.Log) (*Tofunft0EvCouponSpent, error) {
	event := new(Tofunft0EvCouponSpent)
	if err := _Tofunft0.contract.UnpackLog(event, "EvCouponSpent", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Tofunft0EvInventoryUpdateIterator is returned from FilterEvInventoryUpdate and is used to iterate over the raw logs and unpacked data for EvInventoryUpdate events raised by the Tofunft0 contract.
type Tofunft0EvInventoryUpdateIterator struct {
	Event *Tofunft0EvInventoryUpdate // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Tofunft0EvInventoryUpdateIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Tofunft0EvInventoryUpdate)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Tofunft0EvInventoryUpdate)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Tofunft0EvInventoryUpdateIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Tofunft0EvInventoryUpdateIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Tofunft0EvInventoryUpdate represents a EvInventoryUpdate event raised by the Tofunft0 contract.
type Tofunft0EvInventoryUpdate struct {
	Id        *big.Int
	Inventory MarketNGInventory
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterEvInventoryUpdate is a free log retrieval operation binding the contract event 0x5beea7b3b87c573953fec05007114d17712e5775d364acc106d8da9e74849033.
//
// Solidity: event EvInventoryUpdate(uint256 indexed id, (address,address,address,uint256,uint256,uint256,uint8,uint8) inventory)
func (_Tofunft0 *Tofunft0Filterer) FilterEvInventoryUpdate(opts *bind.FilterOpts, id []*big.Int) (*Tofunft0EvInventoryUpdateIterator, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _Tofunft0.contract.FilterLogs(opts, "EvInventoryUpdate", idRule)
	if err != nil {
		return nil, err
	}
	return &Tofunft0EvInventoryUpdateIterator{contract: _Tofunft0.contract, event: "EvInventoryUpdate", logs: logs, sub: sub}, nil
}

// WatchEvInventoryUpdate is a free log subscription operation binding the contract event 0x5beea7b3b87c573953fec05007114d17712e5775d364acc106d8da9e74849033.
//
// Solidity: event EvInventoryUpdate(uint256 indexed id, (address,address,address,uint256,uint256,uint256,uint8,uint8) inventory)
func (_Tofunft0 *Tofunft0Filterer) WatchEvInventoryUpdate(opts *bind.WatchOpts, sink chan<- *Tofunft0EvInventoryUpdate, id []*big.Int) (event.Subscription, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _Tofunft0.contract.WatchLogs(opts, "EvInventoryUpdate", idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Tofunft0EvInventoryUpdate)
				if err := _Tofunft0.contract.UnpackLog(event, "EvInventoryUpdate", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEvInventoryUpdate is a log parse operation binding the contract event 0x5beea7b3b87c573953fec05007114d17712e5775d364acc106d8da9e74849033.
//
// Solidity: event EvInventoryUpdate(uint256 indexed id, (address,address,address,uint256,uint256,uint256,uint8,uint8) inventory)
func (_Tofunft0 *Tofunft0Filterer) ParseEvInventoryUpdate(log types.Log) (*Tofunft0EvInventoryUpdate, error) {
	event := new(Tofunft0EvInventoryUpdate)
	if err := _Tofunft0.contract.UnpackLog(event, "EvInventoryUpdate", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Tofunft0EvMarketSignerUpdateIterator is returned from FilterEvMarketSignerUpdate and is used to iterate over the raw logs and unpacked data for EvMarketSignerUpdate events raised by the Tofunft0 contract.
type Tofunft0EvMarketSignerUpdateIterator struct {
	Event *Tofunft0EvMarketSignerUpdate // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Tofunft0EvMarketSignerUpdateIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Tofunft0EvMarketSignerUpdate)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Tofunft0EvMarketSignerUpdate)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Tofunft0EvMarketSignerUpdateIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Tofunft0EvMarketSignerUpdateIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Tofunft0EvMarketSignerUpdate represents a EvMarketSignerUpdate event raised by the Tofunft0 contract.
type Tofunft0EvMarketSignerUpdate struct {
	Addr      common.Address
	IsRemoval bool
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterEvMarketSignerUpdate is a free log retrieval operation binding the contract event 0x90d56af4745c314d9b45054b55dc973378c558c1ad1554bccc70d39aa63a2cc5.
//
// Solidity: event EvMarketSignerUpdate(address addr, bool isRemoval)
func (_Tofunft0 *Tofunft0Filterer) FilterEvMarketSignerUpdate(opts *bind.FilterOpts) (*Tofunft0EvMarketSignerUpdateIterator, error) {

	logs, sub, err := _Tofunft0.contract.FilterLogs(opts, "EvMarketSignerUpdate")
	if err != nil {
		return nil, err
	}
	return &Tofunft0EvMarketSignerUpdateIterator{contract: _Tofunft0.contract, event: "EvMarketSignerUpdate", logs: logs, sub: sub}, nil
}

// WatchEvMarketSignerUpdate is a free log subscription operation binding the contract event 0x90d56af4745c314d9b45054b55dc973378c558c1ad1554bccc70d39aa63a2cc5.
//
// Solidity: event EvMarketSignerUpdate(address addr, bool isRemoval)
func (_Tofunft0 *Tofunft0Filterer) WatchEvMarketSignerUpdate(opts *bind.WatchOpts, sink chan<- *Tofunft0EvMarketSignerUpdate) (event.Subscription, error) {

	logs, sub, err := _Tofunft0.contract.WatchLogs(opts, "EvMarketSignerUpdate")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Tofunft0EvMarketSignerUpdate)
				if err := _Tofunft0.contract.UnpackLog(event, "EvMarketSignerUpdate", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEvMarketSignerUpdate is a log parse operation binding the contract event 0x90d56af4745c314d9b45054b55dc973378c558c1ad1554bccc70d39aa63a2cc5.
//
// Solidity: event EvMarketSignerUpdate(address addr, bool isRemoval)
func (_Tofunft0 *Tofunft0Filterer) ParseEvMarketSignerUpdate(log types.Log) (*Tofunft0EvMarketSignerUpdate, error) {
	event := new(Tofunft0EvMarketSignerUpdate)
	if err := _Tofunft0.contract.UnpackLog(event, "EvMarketSignerUpdate", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Tofunft0EvSettingsUpdatedIterator is returned from FilterEvSettingsUpdated and is used to iterate over the raw logs and unpacked data for EvSettingsUpdated events raised by the Tofunft0 contract.
type Tofunft0EvSettingsUpdatedIterator struct {
	Event *Tofunft0EvSettingsUpdated // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Tofunft0EvSettingsUpdatedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Tofunft0EvSettingsUpdated)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Tofunft0EvSettingsUpdated)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Tofunft0EvSettingsUpdatedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Tofunft0EvSettingsUpdatedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Tofunft0EvSettingsUpdated represents a EvSettingsUpdated event raised by the Tofunft0 contract.
type Tofunft0EvSettingsUpdated struct {
	Raw types.Log // Blockchain specific contextual infos
}

// FilterEvSettingsUpdated is a free log retrieval operation binding the contract event 0x6c06ac894de6b71964f14d152b6674a4465a9b5d3f9cf9f216b8e7ea61467519.
//
// Solidity: event EvSettingsUpdated()
func (_Tofunft0 *Tofunft0Filterer) FilterEvSettingsUpdated(opts *bind.FilterOpts) (*Tofunft0EvSettingsUpdatedIterator, error) {

	logs, sub, err := _Tofunft0.contract.FilterLogs(opts, "EvSettingsUpdated")
	if err != nil {
		return nil, err
	}
	return &Tofunft0EvSettingsUpdatedIterator{contract: _Tofunft0.contract, event: "EvSettingsUpdated", logs: logs, sub: sub}, nil
}

// WatchEvSettingsUpdated is a free log subscription operation binding the contract event 0x6c06ac894de6b71964f14d152b6674a4465a9b5d3f9cf9f216b8e7ea61467519.
//
// Solidity: event EvSettingsUpdated()
func (_Tofunft0 *Tofunft0Filterer) WatchEvSettingsUpdated(opts *bind.WatchOpts, sink chan<- *Tofunft0EvSettingsUpdated) (event.Subscription, error) {

	logs, sub, err := _Tofunft0.contract.WatchLogs(opts, "EvSettingsUpdated")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Tofunft0EvSettingsUpdated)
				if err := _Tofunft0.contract.UnpackLog(event, "EvSettingsUpdated", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEvSettingsUpdated is a log parse operation binding the contract event 0x6c06ac894de6b71964f14d152b6674a4465a9b5d3f9cf9f216b8e7ea61467519.
//
// Solidity: event EvSettingsUpdated()
func (_Tofunft0 *Tofunft0Filterer) ParseEvSettingsUpdated(log types.Log) (*Tofunft0EvSettingsUpdated, error) {
	event := new(Tofunft0EvSettingsUpdated)
	if err := _Tofunft0.contract.UnpackLog(event, "EvSettingsUpdated", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Tofunft0EvSwappedIterator is returned from FilterEvSwapped and is used to iterate over the raw logs and unpacked data for EvSwapped events raised by the Tofunft0 contract.
type Tofunft0EvSwappedIterator struct {
	Event *Tofunft0EvSwapped // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Tofunft0EvSwappedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Tofunft0EvSwapped)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Tofunft0EvSwapped)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Tofunft0EvSwappedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Tofunft0EvSwappedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Tofunft0EvSwapped represents a EvSwapped event raised by the Tofunft0 contract.
type Tofunft0EvSwapped struct {
	Req       MarketNGSwap
	Signature []byte
	Swapper   common.Address
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterEvSwapped is a free log retrieval operation binding the contract event 0x92060d15ec9a14885865b744d2efb1fff3cab53411058a530f51d480288a864c.
//
// Solidity: event EvSwapped((bytes32,address,uint256,(address,uint256)[],(address,uint256)[]) req, bytes signature, address swapper)
func (_Tofunft0 *Tofunft0Filterer) FilterEvSwapped(opts *bind.FilterOpts) (*Tofunft0EvSwappedIterator, error) {

	logs, sub, err := _Tofunft0.contract.FilterLogs(opts, "EvSwapped")
	if err != nil {
		return nil, err
	}
	return &Tofunft0EvSwappedIterator{contract: _Tofunft0.contract, event: "EvSwapped", logs: logs, sub: sub}, nil
}

// WatchEvSwapped is a free log subscription operation binding the contract event 0x92060d15ec9a14885865b744d2efb1fff3cab53411058a530f51d480288a864c.
//
// Solidity: event EvSwapped((bytes32,address,uint256,(address,uint256)[],(address,uint256)[]) req, bytes signature, address swapper)
func (_Tofunft0 *Tofunft0Filterer) WatchEvSwapped(opts *bind.WatchOpts, sink chan<- *Tofunft0EvSwapped) (event.Subscription, error) {

	logs, sub, err := _Tofunft0.contract.WatchLogs(opts, "EvSwapped")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Tofunft0EvSwapped)
				if err := _Tofunft0.contract.UnpackLog(event, "EvSwapped", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseEvSwapped is a log parse operation binding the contract event 0x92060d15ec9a14885865b744d2efb1fff3cab53411058a530f51d480288a864c.
//
// Solidity: event EvSwapped((bytes32,address,uint256,(address,uint256)[],(address,uint256)[]) req, bytes signature, address swapper)
func (_Tofunft0 *Tofunft0Filterer) ParseEvSwapped(log types.Log) (*Tofunft0EvSwapped, error) {
	event := new(Tofunft0EvSwapped)
	if err := _Tofunft0.contract.UnpackLog(event, "EvSwapped", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Tofunft0OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Tofunft0 contract.
type Tofunft0OwnershipTransferredIterator struct {
	Event *Tofunft0OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Tofunft0OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Tofunft0OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Tofunft0OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Tofunft0OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Tofunft0OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Tofunft0OwnershipTransferred represents a OwnershipTransferred event raised by the Tofunft0 contract.
type Tofunft0OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Tofunft0 *Tofunft0Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Tofunft0OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Tofunft0.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Tofunft0OwnershipTransferredIterator{contract: _Tofunft0.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Tofunft0 *Tofunft0Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Tofunft0OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Tofunft0.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Tofunft0OwnershipTransferred)
				if err := _Tofunft0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Tofunft0 *Tofunft0Filterer) ParseOwnershipTransferred(log types.Log) (*Tofunft0OwnershipTransferred, error) {
	event := new(Tofunft0OwnershipTransferred)
	if err := _Tofunft0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Tofunft0PausedIterator is returned from FilterPaused and is used to iterate over the raw logs and unpacked data for Paused events raised by the Tofunft0 contract.
type Tofunft0PausedIterator struct {
	Event *Tofunft0Paused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Tofunft0PausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Tofunft0Paused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Tofunft0Paused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Tofunft0PausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Tofunft0PausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Tofunft0Paused represents a Paused event raised by the Tofunft0 contract.
type Tofunft0Paused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterPaused is a free log retrieval operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_Tofunft0 *Tofunft0Filterer) FilterPaused(opts *bind.FilterOpts) (*Tofunft0PausedIterator, error) {

	logs, sub, err := _Tofunft0.contract.FilterLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return &Tofunft0PausedIterator{contract: _Tofunft0.contract, event: "Paused", logs: logs, sub: sub}, nil
}

// WatchPaused is a free log subscription operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_Tofunft0 *Tofunft0Filterer) WatchPaused(opts *bind.WatchOpts, sink chan<- *Tofunft0Paused) (event.Subscription, error) {

	logs, sub, err := _Tofunft0.contract.WatchLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Tofunft0Paused)
				if err := _Tofunft0.contract.UnpackLog(event, "Paused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePaused is a log parse operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_Tofunft0 *Tofunft0Filterer) ParsePaused(log types.Log) (*Tofunft0Paused, error) {
	event := new(Tofunft0Paused)
	if err := _Tofunft0.contract.UnpackLog(event, "Paused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Tofunft0UnpausedIterator is returned from FilterUnpaused and is used to iterate over the raw logs and unpacked data for Unpaused events raised by the Tofunft0 contract.
type Tofunft0UnpausedIterator struct {
	Event *Tofunft0Unpaused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Tofunft0UnpausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Tofunft0Unpaused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Tofunft0Unpaused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Tofunft0UnpausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Tofunft0UnpausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Tofunft0Unpaused represents a Unpaused event raised by the Tofunft0 contract.
type Tofunft0Unpaused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterUnpaused is a free log retrieval operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_Tofunft0 *Tofunft0Filterer) FilterUnpaused(opts *bind.FilterOpts) (*Tofunft0UnpausedIterator, error) {

	logs, sub, err := _Tofunft0.contract.FilterLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return &Tofunft0UnpausedIterator{contract: _Tofunft0.contract, event: "Unpaused", logs: logs, sub: sub}, nil
}

// WatchUnpaused is a free log subscription operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_Tofunft0 *Tofunft0Filterer) WatchUnpaused(opts *bind.WatchOpts, sink chan<- *Tofunft0Unpaused) (event.Subscription, error) {

	logs, sub, err := _Tofunft0.contract.WatchLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Tofunft0Unpaused)
				if err := _Tofunft0.contract.UnpackLog(event, "Unpaused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUnpaused is a log parse operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_Tofunft0 *Tofunft0Filterer) ParseUnpaused(log types.Log) (*Tofunft0Unpaused, error) {
	event := new(Tofunft0Unpaused)
	if err := _Tofunft0.contract.UnpackLog(event, "Unpaused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
