// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package blur_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// BlurSwapConverstionDetails is an auto generated low-level Go binding around an user-defined struct.
type BlurSwapConverstionDetails struct {
	ConversionData []byte
}

// BlurSwapERC1155Details is an auto generated low-level Go binding around an user-defined struct.
type BlurSwapERC1155Details struct {
	TokenAddr common.Address
	Ids       []*big.Int
	Amounts   []*big.Int
}

// BlurSwapERC20Details is an auto generated low-level Go binding around an user-defined struct.
type BlurSwapERC20Details struct {
	TokenAddrs []common.Address
	Amounts    []*big.Int
}

// BlurSwapOpenseaTrades is an auto generated low-level Go binding around an user-defined struct.
type BlurSwapOpenseaTrades struct {
	Value     *big.Int
	TradeData []byte
}

// MarketRegistryTradeDetails is an auto generated low-level Go binding around an user-defined struct.
type MarketRegistryTradeDetails struct {
	MarketId  *big.Int
	Value     *big.Int
	TradeData []byte
}

// SpecialTransferHelperERC721Details is an auto generated low-level Go binding around an user-defined struct.
type SpecialTransferHelperERC721Details struct {
	TokenAddr common.Address
	To        []common.Address
	Ids       []*big.Int
}

// Blur0MetaData contains all meta data concerning the Blur0 contract.
var Blur0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_marketRegistry\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_guardian\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"GOV\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_affiliate\",\"type\":\"address\"}],\"name\":\"addAffiliate\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_marketId\",\"type\":\"uint256\"}],\"name\":\"addSponsoredMarket\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"affiliates\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"affiliate\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"isActive\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"baseFees\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structBlurSwap.OpenseaTrades[]\",\"name\":\"openseaTrades\",\"type\":\"tuple[]\"}],\"name\":\"batchBuyFromOpenSea\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"address[]\",\"name\":\"tokenAddrs\",\"type\":\"address[]\"},{\"internalType\":\"uint256[]\",\"name\":\"amounts\",\"type\":\"uint256[]\"}],\"internalType\":\"structBlurSwap.ERC20Details\",\"name\":\"erc20Details\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"marketId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structMarketRegistry.TradeDetails[]\",\"name\":\"tradeDetails\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"bytes\",\"name\":\"conversionData\",\"type\":\"bytes\"}],\"internalType\":\"structBlurSwap.ConverstionDetails[]\",\"name\":\"converstionDetails\",\"type\":\"tuple[]\"},{\"internalType\":\"address[]\",\"name\":\"dustTokens\",\"type\":\"address[]\"}],\"name\":\"batchBuyWithERC20s\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"marketId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structMarketRegistry.TradeDetails[]\",\"name\":\"tradeDetails\",\"type\":\"tuple[]\"}],\"name\":\"batchBuyWithETH\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"closeAllTrades\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"converter\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"guardian\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"marketRegistry\",\"outputs\":[{\"internalType\":\"contractMarketRegistry\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"address[]\",\"name\":\"tokenAddrs\",\"type\":\"address[]\"},{\"internalType\":\"uint256[]\",\"name\":\"amounts\",\"type\":\"uint256[]\"}],\"internalType\":\"structBlurSwap.ERC20Details\",\"name\":\"erc20Details\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"tokenAddr\",\"type\":\"address\"},{\"internalType\":\"address[]\",\"name\":\"to\",\"type\":\"address[]\"},{\"internalType\":\"uint256[]\",\"name\":\"ids\",\"type\":\"uint256[]\"}],\"internalType\":\"structSpecialTransferHelper.ERC721Details[]\",\"name\":\"erc721Details\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"tokenAddr\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"ids\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256[]\",\"name\":\"amounts\",\"type\":\"uint256[]\"}],\"internalType\":\"structBlurSwap.ERC1155Details[]\",\"name\":\"erc1155Details\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"bytes\",\"name\":\"conversionData\",\"type\":\"bytes\"}],\"internalType\":\"structBlurSwap.ConverstionDetails[]\",\"name\":\"converstionDetails\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"marketId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structMarketRegistry.TradeDetails[]\",\"name\":\"tradeDetails\",\"type\":\"tuple[]\"},{\"internalType\":\"address[]\",\"name\":\"dustTokens\",\"type\":\"address[]\"},{\"internalType\":\"uint256[2]\",\"name\":\"feeDetails\",\"type\":\"uint256[2]\"}],\"name\":\"multiAssetSwap\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256[]\",\"name\":\"\",\"type\":\"uint256[]\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC1155BatchReceived\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC1155Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC721Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC721Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"openForFreeTrades\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"openForTrades\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"punkProxy\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"ids\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256[]\",\"name\":\"amounts\",\"type\":\"uint256[]\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"rescueERC1155\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"rescueERC20\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"ids\",\"type\":\"uint256[]\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"rescueERC721\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"rescueETH\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_baseFees\",\"type\":\"uint256\"}],\"name\":\"setBaseFees\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_converter\",\"type\":\"address\"}],\"name\":\"setConverter\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contractMarketRegistry\",\"name\":\"_marketRegistry\",\"type\":\"address\"}],\"name\":\"setMarketRegistry\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contractIERC20\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"setOneTimeApproval\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bool\",\"name\":\"_openForFreeTrades\",\"type\":\"bool\"}],\"name\":\"setOpenForFreeTrades\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bool\",\"name\":\"_openForTrades\",\"type\":\"bool\"}],\"name\":\"setOpenForTrades\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"setUp\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"sponsoredMarkets\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"marketId\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"isActive\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes4\",\"name\":\"interfaceId\",\"type\":\"bytes4\"}],\"name\":\"supportsInterface\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_affiliateIndex\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_affiliate\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_IsActive\",\"type\":\"bool\"}],\"name\":\"updateAffiliate\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_guardian\",\"type\":\"address\"}],\"name\":\"updateGuardian\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_marketIndex\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_marketId\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"_isActive\",\"type\":\"bool\"}],\"name\":\"updateSponsoredMarket\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
}

// Blur0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Blur0MetaData.ABI instead.
var Blur0ABI = Blur0MetaData.ABI

// Blur0 is an auto generated Go binding around an Ethereum contract.
type Blur0 struct {
	Blur0Caller     // Read-only binding to the contract
	Blur0Transactor // Write-only binding to the contract
	Blur0Filterer   // Log filterer for contract events
}

// Blur0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Blur0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Blur0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Blur0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Blur0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Blur0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Blur0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Blur0Session struct {
	Contract     *Blur0            // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Blur0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Blur0CallerSession struct {
	Contract *Blur0Caller  // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// Blur0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Blur0TransactorSession struct {
	Contract     *Blur0Transactor  // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Blur0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Blur0Raw struct {
	Contract *Blur0 // Generic contract binding to access the raw methods on
}

// Blur0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Blur0CallerRaw struct {
	Contract *Blur0Caller // Generic read-only contract binding to access the raw methods on
}

// Blur0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Blur0TransactorRaw struct {
	Contract *Blur0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewBlur0 creates a new instance of Blur0, bound to a specific deployed contract.
func NewBlur0(address common.Address, backend bind.ContractBackend) (*Blur0, error) {
	contract, err := bindBlur0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Blur0{Blur0Caller: Blur0Caller{contract: contract}, Blur0Transactor: Blur0Transactor{contract: contract}, Blur0Filterer: Blur0Filterer{contract: contract}}, nil
}

// NewBlur0Caller creates a new read-only instance of Blur0, bound to a specific deployed contract.
func NewBlur0Caller(address common.Address, caller bind.ContractCaller) (*Blur0Caller, error) {
	contract, err := bindBlur0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Blur0Caller{contract: contract}, nil
}

// NewBlur0Transactor creates a new write-only instance of Blur0, bound to a specific deployed contract.
func NewBlur0Transactor(address common.Address, transactor bind.ContractTransactor) (*Blur0Transactor, error) {
	contract, err := bindBlur0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Blur0Transactor{contract: contract}, nil
}

// NewBlur0Filterer creates a new log filterer instance of Blur0, bound to a specific deployed contract.
func NewBlur0Filterer(address common.Address, filterer bind.ContractFilterer) (*Blur0Filterer, error) {
	contract, err := bindBlur0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Blur0Filterer{contract: contract}, nil
}

// bindBlur0 binds a generic wrapper to an already deployed contract.
func bindBlur0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Blur0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Blur0 *Blur0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Blur0.Contract.Blur0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Blur0 *Blur0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Blur0.Contract.Blur0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Blur0 *Blur0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Blur0.Contract.Blur0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Blur0 *Blur0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Blur0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Blur0 *Blur0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Blur0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Blur0 *Blur0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Blur0.Contract.contract.Transact(opts, method, params...)
}

// GOV is a free data retrieval call binding the contract method 0x180cb47f.
//
// Solidity: function GOV() view returns(address)
func (_Blur0 *Blur0Caller) GOV(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Blur0.contract.Call(opts, &out, "GOV")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GOV is a free data retrieval call binding the contract method 0x180cb47f.
//
// Solidity: function GOV() view returns(address)
func (_Blur0 *Blur0Session) GOV() (common.Address, error) {
	return _Blur0.Contract.GOV(&_Blur0.CallOpts)
}

// GOV is a free data retrieval call binding the contract method 0x180cb47f.
//
// Solidity: function GOV() view returns(address)
func (_Blur0 *Blur0CallerSession) GOV() (common.Address, error) {
	return _Blur0.Contract.GOV(&_Blur0.CallOpts)
}

// Affiliates is a free data retrieval call binding the contract method 0x1bd78748.
//
// Solidity: function affiliates(uint256 ) view returns(address affiliate, bool isActive)
func (_Blur0 *Blur0Caller) Affiliates(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Affiliate common.Address
	IsActive  bool
}, error) {
	var out []interface{}
	err := _Blur0.contract.Call(opts, &out, "affiliates", arg0)

	outstruct := new(struct {
		Affiliate common.Address
		IsActive  bool
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Affiliate = *abi.ConvertType(out[0], new(common.Address)).(*common.Address)
	outstruct.IsActive = *abi.ConvertType(out[1], new(bool)).(*bool)

	return *outstruct, err

}

// Affiliates is a free data retrieval call binding the contract method 0x1bd78748.
//
// Solidity: function affiliates(uint256 ) view returns(address affiliate, bool isActive)
func (_Blur0 *Blur0Session) Affiliates(arg0 *big.Int) (struct {
	Affiliate common.Address
	IsActive  bool
}, error) {
	return _Blur0.Contract.Affiliates(&_Blur0.CallOpts, arg0)
}

// Affiliates is a free data retrieval call binding the contract method 0x1bd78748.
//
// Solidity: function affiliates(uint256 ) view returns(address affiliate, bool isActive)
func (_Blur0 *Blur0CallerSession) Affiliates(arg0 *big.Int) (struct {
	Affiliate common.Address
	IsActive  bool
}, error) {
	return _Blur0.Contract.Affiliates(&_Blur0.CallOpts, arg0)
}

// BaseFees is a free data retrieval call binding the contract method 0xddb382f9.
//
// Solidity: function baseFees() view returns(uint256)
func (_Blur0 *Blur0Caller) BaseFees(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Blur0.contract.Call(opts, &out, "baseFees")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BaseFees is a free data retrieval call binding the contract method 0xddb382f9.
//
// Solidity: function baseFees() view returns(uint256)
func (_Blur0 *Blur0Session) BaseFees() (*big.Int, error) {
	return _Blur0.Contract.BaseFees(&_Blur0.CallOpts)
}

// BaseFees is a free data retrieval call binding the contract method 0xddb382f9.
//
// Solidity: function baseFees() view returns(uint256)
func (_Blur0 *Blur0CallerSession) BaseFees() (*big.Int, error) {
	return _Blur0.Contract.BaseFees(&_Blur0.CallOpts)
}

// Converter is a free data retrieval call binding the contract method 0xbd38837b.
//
// Solidity: function converter() view returns(address)
func (_Blur0 *Blur0Caller) Converter(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Blur0.contract.Call(opts, &out, "converter")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Converter is a free data retrieval call binding the contract method 0xbd38837b.
//
// Solidity: function converter() view returns(address)
func (_Blur0 *Blur0Session) Converter() (common.Address, error) {
	return _Blur0.Contract.Converter(&_Blur0.CallOpts)
}

// Converter is a free data retrieval call binding the contract method 0xbd38837b.
//
// Solidity: function converter() view returns(address)
func (_Blur0 *Blur0CallerSession) Converter() (common.Address, error) {
	return _Blur0.Contract.Converter(&_Blur0.CallOpts)
}

// Guardian is a free data retrieval call binding the contract method 0x452a9320.
//
// Solidity: function guardian() view returns(address)
func (_Blur0 *Blur0Caller) Guardian(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Blur0.contract.Call(opts, &out, "guardian")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Guardian is a free data retrieval call binding the contract method 0x452a9320.
//
// Solidity: function guardian() view returns(address)
func (_Blur0 *Blur0Session) Guardian() (common.Address, error) {
	return _Blur0.Contract.Guardian(&_Blur0.CallOpts)
}

// Guardian is a free data retrieval call binding the contract method 0x452a9320.
//
// Solidity: function guardian() view returns(address)
func (_Blur0 *Blur0CallerSession) Guardian() (common.Address, error) {
	return _Blur0.Contract.Guardian(&_Blur0.CallOpts)
}

// MarketRegistry is a free data retrieval call binding the contract method 0xecb96fe6.
//
// Solidity: function marketRegistry() view returns(address)
func (_Blur0 *Blur0Caller) MarketRegistry(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Blur0.contract.Call(opts, &out, "marketRegistry")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// MarketRegistry is a free data retrieval call binding the contract method 0xecb96fe6.
//
// Solidity: function marketRegistry() view returns(address)
func (_Blur0 *Blur0Session) MarketRegistry() (common.Address, error) {
	return _Blur0.Contract.MarketRegistry(&_Blur0.CallOpts)
}

// MarketRegistry is a free data retrieval call binding the contract method 0xecb96fe6.
//
// Solidity: function marketRegistry() view returns(address)
func (_Blur0 *Blur0CallerSession) MarketRegistry() (common.Address, error) {
	return _Blur0.Contract.MarketRegistry(&_Blur0.CallOpts)
}

// OpenForFreeTrades is a free data retrieval call binding the contract method 0x11f85417.
//
// Solidity: function openForFreeTrades() view returns(bool)
func (_Blur0 *Blur0Caller) OpenForFreeTrades(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Blur0.contract.Call(opts, &out, "openForFreeTrades")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// OpenForFreeTrades is a free data retrieval call binding the contract method 0x11f85417.
//
// Solidity: function openForFreeTrades() view returns(bool)
func (_Blur0 *Blur0Session) OpenForFreeTrades() (bool, error) {
	return _Blur0.Contract.OpenForFreeTrades(&_Blur0.CallOpts)
}

// OpenForFreeTrades is a free data retrieval call binding the contract method 0x11f85417.
//
// Solidity: function openForFreeTrades() view returns(bool)
func (_Blur0 *Blur0CallerSession) OpenForFreeTrades() (bool, error) {
	return _Blur0.Contract.OpenForFreeTrades(&_Blur0.CallOpts)
}

// OpenForTrades is a free data retrieval call binding the contract method 0xa1b62797.
//
// Solidity: function openForTrades() view returns(bool)
func (_Blur0 *Blur0Caller) OpenForTrades(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Blur0.contract.Call(opts, &out, "openForTrades")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// OpenForTrades is a free data retrieval call binding the contract method 0xa1b62797.
//
// Solidity: function openForTrades() view returns(bool)
func (_Blur0 *Blur0Session) OpenForTrades() (bool, error) {
	return _Blur0.Contract.OpenForTrades(&_Blur0.CallOpts)
}

// OpenForTrades is a free data retrieval call binding the contract method 0xa1b62797.
//
// Solidity: function openForTrades() view returns(bool)
func (_Blur0 *Blur0CallerSession) OpenForTrades() (bool, error) {
	return _Blur0.Contract.OpenForTrades(&_Blur0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Blur0 *Blur0Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Blur0.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Blur0 *Blur0Session) Owner() (common.Address, error) {
	return _Blur0.Contract.Owner(&_Blur0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Blur0 *Blur0CallerSession) Owner() (common.Address, error) {
	return _Blur0.Contract.Owner(&_Blur0.CallOpts)
}

// PunkProxy is a free data retrieval call binding the contract method 0x6335f25e.
//
// Solidity: function punkProxy() view returns(address)
func (_Blur0 *Blur0Caller) PunkProxy(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Blur0.contract.Call(opts, &out, "punkProxy")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// PunkProxy is a free data retrieval call binding the contract method 0x6335f25e.
//
// Solidity: function punkProxy() view returns(address)
func (_Blur0 *Blur0Session) PunkProxy() (common.Address, error) {
	return _Blur0.Contract.PunkProxy(&_Blur0.CallOpts)
}

// PunkProxy is a free data retrieval call binding the contract method 0x6335f25e.
//
// Solidity: function punkProxy() view returns(address)
func (_Blur0 *Blur0CallerSession) PunkProxy() (common.Address, error) {
	return _Blur0.Contract.PunkProxy(&_Blur0.CallOpts)
}

// SponsoredMarkets is a free data retrieval call binding the contract method 0xe4dd4b8a.
//
// Solidity: function sponsoredMarkets(uint256 ) view returns(uint256 marketId, bool isActive)
func (_Blur0 *Blur0Caller) SponsoredMarkets(opts *bind.CallOpts, arg0 *big.Int) (struct {
	MarketId *big.Int
	IsActive bool
}, error) {
	var out []interface{}
	err := _Blur0.contract.Call(opts, &out, "sponsoredMarkets", arg0)

	outstruct := new(struct {
		MarketId *big.Int
		IsActive bool
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.MarketId = *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	outstruct.IsActive = *abi.ConvertType(out[1], new(bool)).(*bool)

	return *outstruct, err

}

// SponsoredMarkets is a free data retrieval call binding the contract method 0xe4dd4b8a.
//
// Solidity: function sponsoredMarkets(uint256 ) view returns(uint256 marketId, bool isActive)
func (_Blur0 *Blur0Session) SponsoredMarkets(arg0 *big.Int) (struct {
	MarketId *big.Int
	IsActive bool
}, error) {
	return _Blur0.Contract.SponsoredMarkets(&_Blur0.CallOpts, arg0)
}

// SponsoredMarkets is a free data retrieval call binding the contract method 0xe4dd4b8a.
//
// Solidity: function sponsoredMarkets(uint256 ) view returns(uint256 marketId, bool isActive)
func (_Blur0 *Blur0CallerSession) SponsoredMarkets(arg0 *big.Int) (struct {
	MarketId *big.Int
	IsActive bool
}, error) {
	return _Blur0.Contract.SponsoredMarkets(&_Blur0.CallOpts, arg0)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Blur0 *Blur0Caller) SupportsInterface(opts *bind.CallOpts, interfaceId [4]byte) (bool, error) {
	var out []interface{}
	err := _Blur0.contract.Call(opts, &out, "supportsInterface", interfaceId)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Blur0 *Blur0Session) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _Blur0.Contract.SupportsInterface(&_Blur0.CallOpts, interfaceId)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Blur0 *Blur0CallerSession) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _Blur0.Contract.SupportsInterface(&_Blur0.CallOpts, interfaceId)
}

// AddAffiliate is a paid mutator transaction binding the contract method 0x81ea4ea6.
//
// Solidity: function addAffiliate(address _affiliate) returns()
func (_Blur0 *Blur0Transactor) AddAffiliate(opts *bind.TransactOpts, _affiliate common.Address) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "addAffiliate", _affiliate)
}

// AddAffiliate is a paid mutator transaction binding the contract method 0x81ea4ea6.
//
// Solidity: function addAffiliate(address _affiliate) returns()
func (_Blur0 *Blur0Session) AddAffiliate(_affiliate common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.AddAffiliate(&_Blur0.TransactOpts, _affiliate)
}

// AddAffiliate is a paid mutator transaction binding the contract method 0x81ea4ea6.
//
// Solidity: function addAffiliate(address _affiliate) returns()
func (_Blur0 *Blur0TransactorSession) AddAffiliate(_affiliate common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.AddAffiliate(&_Blur0.TransactOpts, _affiliate)
}

// AddSponsoredMarket is a paid mutator transaction binding the contract method 0x3a5750b6.
//
// Solidity: function addSponsoredMarket(uint256 _marketId) returns()
func (_Blur0 *Blur0Transactor) AddSponsoredMarket(opts *bind.TransactOpts, _marketId *big.Int) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "addSponsoredMarket", _marketId)
}

// AddSponsoredMarket is a paid mutator transaction binding the contract method 0x3a5750b6.
//
// Solidity: function addSponsoredMarket(uint256 _marketId) returns()
func (_Blur0 *Blur0Session) AddSponsoredMarket(_marketId *big.Int) (*types.Transaction, error) {
	return _Blur0.Contract.AddSponsoredMarket(&_Blur0.TransactOpts, _marketId)
}

// AddSponsoredMarket is a paid mutator transaction binding the contract method 0x3a5750b6.
//
// Solidity: function addSponsoredMarket(uint256 _marketId) returns()
func (_Blur0 *Blur0TransactorSession) AddSponsoredMarket(_marketId *big.Int) (*types.Transaction, error) {
	return _Blur0.Contract.AddSponsoredMarket(&_Blur0.TransactOpts, _marketId)
}

// BatchBuyFromOpenSea is a paid mutator transaction binding the contract method 0x5eacc63a.
//
// Solidity: function batchBuyFromOpenSea((uint256,bytes)[] openseaTrades) payable returns()
func (_Blur0 *Blur0Transactor) BatchBuyFromOpenSea(opts *bind.TransactOpts, openseaTrades []BlurSwapOpenseaTrades) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "batchBuyFromOpenSea", openseaTrades)
}

// BatchBuyFromOpenSea is a paid mutator transaction binding the contract method 0x5eacc63a.
//
// Solidity: function batchBuyFromOpenSea((uint256,bytes)[] openseaTrades) payable returns()
func (_Blur0 *Blur0Session) BatchBuyFromOpenSea(openseaTrades []BlurSwapOpenseaTrades) (*types.Transaction, error) {
	return _Blur0.Contract.BatchBuyFromOpenSea(&_Blur0.TransactOpts, openseaTrades)
}

// BatchBuyFromOpenSea is a paid mutator transaction binding the contract method 0x5eacc63a.
//
// Solidity: function batchBuyFromOpenSea((uint256,bytes)[] openseaTrades) payable returns()
func (_Blur0 *Blur0TransactorSession) BatchBuyFromOpenSea(openseaTrades []BlurSwapOpenseaTrades) (*types.Transaction, error) {
	return _Blur0.Contract.BatchBuyFromOpenSea(&_Blur0.TransactOpts, openseaTrades)
}

// BatchBuyWithERC20s is a paid mutator transaction binding the contract method 0x09ba153d.
//
// Solidity: function batchBuyWithERC20s((address[],uint256[]) erc20Details, (uint256,uint256,bytes)[] tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Blur0 *Blur0Transactor) BatchBuyWithERC20s(opts *bind.TransactOpts, erc20Details BlurSwapERC20Details, tradeDetails []MarketRegistryTradeDetails, converstionDetails []BlurSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "batchBuyWithERC20s", erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BatchBuyWithERC20s is a paid mutator transaction binding the contract method 0x09ba153d.
//
// Solidity: function batchBuyWithERC20s((address[],uint256[]) erc20Details, (uint256,uint256,bytes)[] tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Blur0 *Blur0Session) BatchBuyWithERC20s(erc20Details BlurSwapERC20Details, tradeDetails []MarketRegistryTradeDetails, converstionDetails []BlurSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.BatchBuyWithERC20s(&_Blur0.TransactOpts, erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BatchBuyWithERC20s is a paid mutator transaction binding the contract method 0x09ba153d.
//
// Solidity: function batchBuyWithERC20s((address[],uint256[]) erc20Details, (uint256,uint256,bytes)[] tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Blur0 *Blur0TransactorSession) BatchBuyWithERC20s(erc20Details BlurSwapERC20Details, tradeDetails []MarketRegistryTradeDetails, converstionDetails []BlurSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.BatchBuyWithERC20s(&_Blur0.TransactOpts, erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BatchBuyWithETH is a paid mutator transaction binding the contract method 0x9a2b8115.
//
// Solidity: function batchBuyWithETH((uint256,uint256,bytes)[] tradeDetails) payable returns()
func (_Blur0 *Blur0Transactor) BatchBuyWithETH(opts *bind.TransactOpts, tradeDetails []MarketRegistryTradeDetails) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "batchBuyWithETH", tradeDetails)
}

// BatchBuyWithETH is a paid mutator transaction binding the contract method 0x9a2b8115.
//
// Solidity: function batchBuyWithETH((uint256,uint256,bytes)[] tradeDetails) payable returns()
func (_Blur0 *Blur0Session) BatchBuyWithETH(tradeDetails []MarketRegistryTradeDetails) (*types.Transaction, error) {
	return _Blur0.Contract.BatchBuyWithETH(&_Blur0.TransactOpts, tradeDetails)
}

// BatchBuyWithETH is a paid mutator transaction binding the contract method 0x9a2b8115.
//
// Solidity: function batchBuyWithETH((uint256,uint256,bytes)[] tradeDetails) payable returns()
func (_Blur0 *Blur0TransactorSession) BatchBuyWithETH(tradeDetails []MarketRegistryTradeDetails) (*types.Transaction, error) {
	return _Blur0.Contract.BatchBuyWithETH(&_Blur0.TransactOpts, tradeDetails)
}

// CloseAllTrades is a paid mutator transaction binding the contract method 0xc5cadd7f.
//
// Solidity: function closeAllTrades() returns()
func (_Blur0 *Blur0Transactor) CloseAllTrades(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "closeAllTrades")
}

// CloseAllTrades is a paid mutator transaction binding the contract method 0xc5cadd7f.
//
// Solidity: function closeAllTrades() returns()
func (_Blur0 *Blur0Session) CloseAllTrades() (*types.Transaction, error) {
	return _Blur0.Contract.CloseAllTrades(&_Blur0.TransactOpts)
}

// CloseAllTrades is a paid mutator transaction binding the contract method 0xc5cadd7f.
//
// Solidity: function closeAllTrades() returns()
func (_Blur0 *Blur0TransactorSession) CloseAllTrades() (*types.Transaction, error) {
	return _Blur0.Contract.CloseAllTrades(&_Blur0.TransactOpts)
}

// MultiAssetSwap is a paid mutator transaction binding the contract method 0x186b100c.
//
// Solidity: function multiAssetSwap((address[],uint256[]) erc20Details, (address,address[],uint256[])[] erc721Details, (address,uint256[],uint256[])[] erc1155Details, (bytes)[] converstionDetails, (uint256,uint256,bytes)[] tradeDetails, address[] dustTokens, uint256[2] feeDetails) payable returns()
func (_Blur0 *Blur0Transactor) MultiAssetSwap(opts *bind.TransactOpts, erc20Details BlurSwapERC20Details, erc721Details []SpecialTransferHelperERC721Details, erc1155Details []BlurSwapERC1155Details, converstionDetails []BlurSwapConverstionDetails, tradeDetails []MarketRegistryTradeDetails, dustTokens []common.Address, feeDetails [2]*big.Int) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "multiAssetSwap", erc20Details, erc721Details, erc1155Details, converstionDetails, tradeDetails, dustTokens, feeDetails)
}

// MultiAssetSwap is a paid mutator transaction binding the contract method 0x186b100c.
//
// Solidity: function multiAssetSwap((address[],uint256[]) erc20Details, (address,address[],uint256[])[] erc721Details, (address,uint256[],uint256[])[] erc1155Details, (bytes)[] converstionDetails, (uint256,uint256,bytes)[] tradeDetails, address[] dustTokens, uint256[2] feeDetails) payable returns()
func (_Blur0 *Blur0Session) MultiAssetSwap(erc20Details BlurSwapERC20Details, erc721Details []SpecialTransferHelperERC721Details, erc1155Details []BlurSwapERC1155Details, converstionDetails []BlurSwapConverstionDetails, tradeDetails []MarketRegistryTradeDetails, dustTokens []common.Address, feeDetails [2]*big.Int) (*types.Transaction, error) {
	return _Blur0.Contract.MultiAssetSwap(&_Blur0.TransactOpts, erc20Details, erc721Details, erc1155Details, converstionDetails, tradeDetails, dustTokens, feeDetails)
}

// MultiAssetSwap is a paid mutator transaction binding the contract method 0x186b100c.
//
// Solidity: function multiAssetSwap((address[],uint256[]) erc20Details, (address,address[],uint256[])[] erc721Details, (address,uint256[],uint256[])[] erc1155Details, (bytes)[] converstionDetails, (uint256,uint256,bytes)[] tradeDetails, address[] dustTokens, uint256[2] feeDetails) payable returns()
func (_Blur0 *Blur0TransactorSession) MultiAssetSwap(erc20Details BlurSwapERC20Details, erc721Details []SpecialTransferHelperERC721Details, erc1155Details []BlurSwapERC1155Details, converstionDetails []BlurSwapConverstionDetails, tradeDetails []MarketRegistryTradeDetails, dustTokens []common.Address, feeDetails [2]*big.Int) (*types.Transaction, error) {
	return _Blur0.Contract.MultiAssetSwap(&_Blur0.TransactOpts, erc20Details, erc721Details, erc1155Details, converstionDetails, tradeDetails, dustTokens, feeDetails)
}

// OnERC1155BatchReceived is a paid mutator transaction binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address , address , uint256[] , uint256[] , bytes ) returns(bytes4)
func (_Blur0 *Blur0Transactor) OnERC1155BatchReceived(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 []*big.Int, arg3 []*big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "onERC1155BatchReceived", arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155BatchReceived is a paid mutator transaction binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address , address , uint256[] , uint256[] , bytes ) returns(bytes4)
func (_Blur0 *Blur0Session) OnERC1155BatchReceived(arg0 common.Address, arg1 common.Address, arg2 []*big.Int, arg3 []*big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Blur0.Contract.OnERC1155BatchReceived(&_Blur0.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155BatchReceived is a paid mutator transaction binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address , address , uint256[] , uint256[] , bytes ) returns(bytes4)
func (_Blur0 *Blur0TransactorSession) OnERC1155BatchReceived(arg0 common.Address, arg1 common.Address, arg2 []*big.Int, arg3 []*big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Blur0.Contract.OnERC1155BatchReceived(&_Blur0.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155Received is a paid mutator transaction binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address , address , uint256 , uint256 , bytes ) returns(bytes4)
func (_Blur0 *Blur0Transactor) OnERC1155Received(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 *big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "onERC1155Received", arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155Received is a paid mutator transaction binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address , address , uint256 , uint256 , bytes ) returns(bytes4)
func (_Blur0 *Blur0Session) OnERC1155Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 *big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Blur0.Contract.OnERC1155Received(&_Blur0.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155Received is a paid mutator transaction binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address , address , uint256 , uint256 , bytes ) returns(bytes4)
func (_Blur0 *Blur0TransactorSession) OnERC1155Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 *big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Blur0.Contract.OnERC1155Received(&_Blur0.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Blur0 *Blur0Transactor) OnERC721Received(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "onERC721Received", arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Blur0 *Blur0Session) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Blur0.Contract.OnERC721Received(&_Blur0.TransactOpts, arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Blur0 *Blur0TransactorSession) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Blur0.Contract.OnERC721Received(&_Blur0.TransactOpts, arg0, arg1, arg2, arg3)
}

// OnERC721Received0 is a paid mutator transaction binding the contract method 0xf0b9e5ba.
//
// Solidity: function onERC721Received(address , uint256 , bytes ) returns(bytes4)
func (_Blur0 *Blur0Transactor) OnERC721Received0(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int, arg2 []byte) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "onERC721Received0", arg0, arg1, arg2)
}

// OnERC721Received0 is a paid mutator transaction binding the contract method 0xf0b9e5ba.
//
// Solidity: function onERC721Received(address , uint256 , bytes ) returns(bytes4)
func (_Blur0 *Blur0Session) OnERC721Received0(arg0 common.Address, arg1 *big.Int, arg2 []byte) (*types.Transaction, error) {
	return _Blur0.Contract.OnERC721Received0(&_Blur0.TransactOpts, arg0, arg1, arg2)
}

// OnERC721Received0 is a paid mutator transaction binding the contract method 0xf0b9e5ba.
//
// Solidity: function onERC721Received(address , uint256 , bytes ) returns(bytes4)
func (_Blur0 *Blur0TransactorSession) OnERC721Received0(arg0 common.Address, arg1 *big.Int, arg2 []byte) (*types.Transaction, error) {
	return _Blur0.Contract.OnERC721Received0(&_Blur0.TransactOpts, arg0, arg1, arg2)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Blur0 *Blur0Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Blur0 *Blur0Session) RenounceOwnership() (*types.Transaction, error) {
	return _Blur0.Contract.RenounceOwnership(&_Blur0.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Blur0 *Blur0TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Blur0.Contract.RenounceOwnership(&_Blur0.TransactOpts)
}

// RescueERC1155 is a paid mutator transaction binding the contract method 0xb7ce33a2.
//
// Solidity: function rescueERC1155(address asset, uint256[] ids, uint256[] amounts, address recipient) returns()
func (_Blur0 *Blur0Transactor) RescueERC1155(opts *bind.TransactOpts, asset common.Address, ids []*big.Int, amounts []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "rescueERC1155", asset, ids, amounts, recipient)
}

// RescueERC1155 is a paid mutator transaction binding the contract method 0xb7ce33a2.
//
// Solidity: function rescueERC1155(address asset, uint256[] ids, uint256[] amounts, address recipient) returns()
func (_Blur0 *Blur0Session) RescueERC1155(asset common.Address, ids []*big.Int, amounts []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.RescueERC1155(&_Blur0.TransactOpts, asset, ids, amounts, recipient)
}

// RescueERC1155 is a paid mutator transaction binding the contract method 0xb7ce33a2.
//
// Solidity: function rescueERC1155(address asset, uint256[] ids, uint256[] amounts, address recipient) returns()
func (_Blur0 *Blur0TransactorSession) RescueERC1155(asset common.Address, ids []*big.Int, amounts []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.RescueERC1155(&_Blur0.TransactOpts, asset, ids, amounts, recipient)
}

// RescueERC20 is a paid mutator transaction binding the contract method 0x5d799f87.
//
// Solidity: function rescueERC20(address asset, address recipient) returns()
func (_Blur0 *Blur0Transactor) RescueERC20(opts *bind.TransactOpts, asset common.Address, recipient common.Address) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "rescueERC20", asset, recipient)
}

// RescueERC20 is a paid mutator transaction binding the contract method 0x5d799f87.
//
// Solidity: function rescueERC20(address asset, address recipient) returns()
func (_Blur0 *Blur0Session) RescueERC20(asset common.Address, recipient common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.RescueERC20(&_Blur0.TransactOpts, asset, recipient)
}

// RescueERC20 is a paid mutator transaction binding the contract method 0x5d799f87.
//
// Solidity: function rescueERC20(address asset, address recipient) returns()
func (_Blur0 *Blur0TransactorSession) RescueERC20(asset common.Address, recipient common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.RescueERC20(&_Blur0.TransactOpts, asset, recipient)
}

// RescueERC721 is a paid mutator transaction binding the contract method 0x26e2dca2.
//
// Solidity: function rescueERC721(address asset, uint256[] ids, address recipient) returns()
func (_Blur0 *Blur0Transactor) RescueERC721(opts *bind.TransactOpts, asset common.Address, ids []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "rescueERC721", asset, ids, recipient)
}

// RescueERC721 is a paid mutator transaction binding the contract method 0x26e2dca2.
//
// Solidity: function rescueERC721(address asset, uint256[] ids, address recipient) returns()
func (_Blur0 *Blur0Session) RescueERC721(asset common.Address, ids []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.RescueERC721(&_Blur0.TransactOpts, asset, ids, recipient)
}

// RescueERC721 is a paid mutator transaction binding the contract method 0x26e2dca2.
//
// Solidity: function rescueERC721(address asset, uint256[] ids, address recipient) returns()
func (_Blur0 *Blur0TransactorSession) RescueERC721(asset common.Address, ids []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.RescueERC721(&_Blur0.TransactOpts, asset, ids, recipient)
}

// RescueETH is a paid mutator transaction binding the contract method 0x04824e70.
//
// Solidity: function rescueETH(address recipient) returns()
func (_Blur0 *Blur0Transactor) RescueETH(opts *bind.TransactOpts, recipient common.Address) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "rescueETH", recipient)
}

// RescueETH is a paid mutator transaction binding the contract method 0x04824e70.
//
// Solidity: function rescueETH(address recipient) returns()
func (_Blur0 *Blur0Session) RescueETH(recipient common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.RescueETH(&_Blur0.TransactOpts, recipient)
}

// RescueETH is a paid mutator transaction binding the contract method 0x04824e70.
//
// Solidity: function rescueETH(address recipient) returns()
func (_Blur0 *Blur0TransactorSession) RescueETH(recipient common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.RescueETH(&_Blur0.TransactOpts, recipient)
}

// SetBaseFees is a paid mutator transaction binding the contract method 0xb9277963.
//
// Solidity: function setBaseFees(uint256 _baseFees) returns()
func (_Blur0 *Blur0Transactor) SetBaseFees(opts *bind.TransactOpts, _baseFees *big.Int) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "setBaseFees", _baseFees)
}

// SetBaseFees is a paid mutator transaction binding the contract method 0xb9277963.
//
// Solidity: function setBaseFees(uint256 _baseFees) returns()
func (_Blur0 *Blur0Session) SetBaseFees(_baseFees *big.Int) (*types.Transaction, error) {
	return _Blur0.Contract.SetBaseFees(&_Blur0.TransactOpts, _baseFees)
}

// SetBaseFees is a paid mutator transaction binding the contract method 0xb9277963.
//
// Solidity: function setBaseFees(uint256 _baseFees) returns()
func (_Blur0 *Blur0TransactorSession) SetBaseFees(_baseFees *big.Int) (*types.Transaction, error) {
	return _Blur0.Contract.SetBaseFees(&_Blur0.TransactOpts, _baseFees)
}

// SetConverter is a paid mutator transaction binding the contract method 0xb19337a4.
//
// Solidity: function setConverter(address _converter) returns()
func (_Blur0 *Blur0Transactor) SetConverter(opts *bind.TransactOpts, _converter common.Address) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "setConverter", _converter)
}

// SetConverter is a paid mutator transaction binding the contract method 0xb19337a4.
//
// Solidity: function setConverter(address _converter) returns()
func (_Blur0 *Blur0Session) SetConverter(_converter common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.SetConverter(&_Blur0.TransactOpts, _converter)
}

// SetConverter is a paid mutator transaction binding the contract method 0xb19337a4.
//
// Solidity: function setConverter(address _converter) returns()
func (_Blur0 *Blur0TransactorSession) SetConverter(_converter common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.SetConverter(&_Blur0.TransactOpts, _converter)
}

// SetMarketRegistry is a paid mutator transaction binding the contract method 0xd8579704.
//
// Solidity: function setMarketRegistry(address _marketRegistry) returns()
func (_Blur0 *Blur0Transactor) SetMarketRegistry(opts *bind.TransactOpts, _marketRegistry common.Address) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "setMarketRegistry", _marketRegistry)
}

// SetMarketRegistry is a paid mutator transaction binding the contract method 0xd8579704.
//
// Solidity: function setMarketRegistry(address _marketRegistry) returns()
func (_Blur0 *Blur0Session) SetMarketRegistry(_marketRegistry common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.SetMarketRegistry(&_Blur0.TransactOpts, _marketRegistry)
}

// SetMarketRegistry is a paid mutator transaction binding the contract method 0xd8579704.
//
// Solidity: function setMarketRegistry(address _marketRegistry) returns()
func (_Blur0 *Blur0TransactorSession) SetMarketRegistry(_marketRegistry common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.SetMarketRegistry(&_Blur0.TransactOpts, _marketRegistry)
}

// SetOneTimeApproval is a paid mutator transaction binding the contract method 0x9f2ba09b.
//
// Solidity: function setOneTimeApproval(address token, address operator, uint256 amount) returns()
func (_Blur0 *Blur0Transactor) SetOneTimeApproval(opts *bind.TransactOpts, token common.Address, operator common.Address, amount *big.Int) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "setOneTimeApproval", token, operator, amount)
}

// SetOneTimeApproval is a paid mutator transaction binding the contract method 0x9f2ba09b.
//
// Solidity: function setOneTimeApproval(address token, address operator, uint256 amount) returns()
func (_Blur0 *Blur0Session) SetOneTimeApproval(token common.Address, operator common.Address, amount *big.Int) (*types.Transaction, error) {
	return _Blur0.Contract.SetOneTimeApproval(&_Blur0.TransactOpts, token, operator, amount)
}

// SetOneTimeApproval is a paid mutator transaction binding the contract method 0x9f2ba09b.
//
// Solidity: function setOneTimeApproval(address token, address operator, uint256 amount) returns()
func (_Blur0 *Blur0TransactorSession) SetOneTimeApproval(token common.Address, operator common.Address, amount *big.Int) (*types.Transaction, error) {
	return _Blur0.Contract.SetOneTimeApproval(&_Blur0.TransactOpts, token, operator, amount)
}

// SetOpenForFreeTrades is a paid mutator transaction binding the contract method 0xe6041f9a.
//
// Solidity: function setOpenForFreeTrades(bool _openForFreeTrades) returns()
func (_Blur0 *Blur0Transactor) SetOpenForFreeTrades(opts *bind.TransactOpts, _openForFreeTrades bool) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "setOpenForFreeTrades", _openForFreeTrades)
}

// SetOpenForFreeTrades is a paid mutator transaction binding the contract method 0xe6041f9a.
//
// Solidity: function setOpenForFreeTrades(bool _openForFreeTrades) returns()
func (_Blur0 *Blur0Session) SetOpenForFreeTrades(_openForFreeTrades bool) (*types.Transaction, error) {
	return _Blur0.Contract.SetOpenForFreeTrades(&_Blur0.TransactOpts, _openForFreeTrades)
}

// SetOpenForFreeTrades is a paid mutator transaction binding the contract method 0xe6041f9a.
//
// Solidity: function setOpenForFreeTrades(bool _openForFreeTrades) returns()
func (_Blur0 *Blur0TransactorSession) SetOpenForFreeTrades(_openForFreeTrades bool) (*types.Transaction, error) {
	return _Blur0.Contract.SetOpenForFreeTrades(&_Blur0.TransactOpts, _openForFreeTrades)
}

// SetOpenForTrades is a paid mutator transaction binding the contract method 0x83206e80.
//
// Solidity: function setOpenForTrades(bool _openForTrades) returns()
func (_Blur0 *Blur0Transactor) SetOpenForTrades(opts *bind.TransactOpts, _openForTrades bool) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "setOpenForTrades", _openForTrades)
}

// SetOpenForTrades is a paid mutator transaction binding the contract method 0x83206e80.
//
// Solidity: function setOpenForTrades(bool _openForTrades) returns()
func (_Blur0 *Blur0Session) SetOpenForTrades(_openForTrades bool) (*types.Transaction, error) {
	return _Blur0.Contract.SetOpenForTrades(&_Blur0.TransactOpts, _openForTrades)
}

// SetOpenForTrades is a paid mutator transaction binding the contract method 0x83206e80.
//
// Solidity: function setOpenForTrades(bool _openForTrades) returns()
func (_Blur0 *Blur0TransactorSession) SetOpenForTrades(_openForTrades bool) (*types.Transaction, error) {
	return _Blur0.Contract.SetOpenForTrades(&_Blur0.TransactOpts, _openForTrades)
}

// SetUp is a paid mutator transaction binding the contract method 0x0a9254e4.
//
// Solidity: function setUp() returns()
func (_Blur0 *Blur0Transactor) SetUp(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "setUp")
}

// SetUp is a paid mutator transaction binding the contract method 0x0a9254e4.
//
// Solidity: function setUp() returns()
func (_Blur0 *Blur0Session) SetUp() (*types.Transaction, error) {
	return _Blur0.Contract.SetUp(&_Blur0.TransactOpts)
}

// SetUp is a paid mutator transaction binding the contract method 0x0a9254e4.
//
// Solidity: function setUp() returns()
func (_Blur0 *Blur0TransactorSession) SetUp() (*types.Transaction, error) {
	return _Blur0.Contract.SetUp(&_Blur0.TransactOpts)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Blur0 *Blur0Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Blur0 *Blur0Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.TransferOwnership(&_Blur0.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Blur0 *Blur0TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.TransferOwnership(&_Blur0.TransactOpts, newOwner)
}

// UpdateAffiliate is a paid mutator transaction binding the contract method 0x565528d7.
//
// Solidity: function updateAffiliate(uint256 _affiliateIndex, address _affiliate, bool _IsActive) returns()
func (_Blur0 *Blur0Transactor) UpdateAffiliate(opts *bind.TransactOpts, _affiliateIndex *big.Int, _affiliate common.Address, _IsActive bool) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "updateAffiliate", _affiliateIndex, _affiliate, _IsActive)
}

// UpdateAffiliate is a paid mutator transaction binding the contract method 0x565528d7.
//
// Solidity: function updateAffiliate(uint256 _affiliateIndex, address _affiliate, bool _IsActive) returns()
func (_Blur0 *Blur0Session) UpdateAffiliate(_affiliateIndex *big.Int, _affiliate common.Address, _IsActive bool) (*types.Transaction, error) {
	return _Blur0.Contract.UpdateAffiliate(&_Blur0.TransactOpts, _affiliateIndex, _affiliate, _IsActive)
}

// UpdateAffiliate is a paid mutator transaction binding the contract method 0x565528d7.
//
// Solidity: function updateAffiliate(uint256 _affiliateIndex, address _affiliate, bool _IsActive) returns()
func (_Blur0 *Blur0TransactorSession) UpdateAffiliate(_affiliateIndex *big.Int, _affiliate common.Address, _IsActive bool) (*types.Transaction, error) {
	return _Blur0.Contract.UpdateAffiliate(&_Blur0.TransactOpts, _affiliateIndex, _affiliate, _IsActive)
}

// UpdateGuardian is a paid mutator transaction binding the contract method 0xfc525395.
//
// Solidity: function updateGuardian(address _guardian) returns()
func (_Blur0 *Blur0Transactor) UpdateGuardian(opts *bind.TransactOpts, _guardian common.Address) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "updateGuardian", _guardian)
}

// UpdateGuardian is a paid mutator transaction binding the contract method 0xfc525395.
//
// Solidity: function updateGuardian(address _guardian) returns()
func (_Blur0 *Blur0Session) UpdateGuardian(_guardian common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.UpdateGuardian(&_Blur0.TransactOpts, _guardian)
}

// UpdateGuardian is a paid mutator transaction binding the contract method 0xfc525395.
//
// Solidity: function updateGuardian(address _guardian) returns()
func (_Blur0 *Blur0TransactorSession) UpdateGuardian(_guardian common.Address) (*types.Transaction, error) {
	return _Blur0.Contract.UpdateGuardian(&_Blur0.TransactOpts, _guardian)
}

// UpdateSponsoredMarket is a paid mutator transaction binding the contract method 0xccf3dc82.
//
// Solidity: function updateSponsoredMarket(uint256 _marketIndex, uint256 _marketId, bool _isActive) returns()
func (_Blur0 *Blur0Transactor) UpdateSponsoredMarket(opts *bind.TransactOpts, _marketIndex *big.Int, _marketId *big.Int, _isActive bool) (*types.Transaction, error) {
	return _Blur0.contract.Transact(opts, "updateSponsoredMarket", _marketIndex, _marketId, _isActive)
}

// UpdateSponsoredMarket is a paid mutator transaction binding the contract method 0xccf3dc82.
//
// Solidity: function updateSponsoredMarket(uint256 _marketIndex, uint256 _marketId, bool _isActive) returns()
func (_Blur0 *Blur0Session) UpdateSponsoredMarket(_marketIndex *big.Int, _marketId *big.Int, _isActive bool) (*types.Transaction, error) {
	return _Blur0.Contract.UpdateSponsoredMarket(&_Blur0.TransactOpts, _marketIndex, _marketId, _isActive)
}

// UpdateSponsoredMarket is a paid mutator transaction binding the contract method 0xccf3dc82.
//
// Solidity: function updateSponsoredMarket(uint256 _marketIndex, uint256 _marketId, bool _isActive) returns()
func (_Blur0 *Blur0TransactorSession) UpdateSponsoredMarket(_marketIndex *big.Int, _marketId *big.Int, _isActive bool) (*types.Transaction, error) {
	return _Blur0.Contract.UpdateSponsoredMarket(&_Blur0.TransactOpts, _marketIndex, _marketId, _isActive)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Blur0 *Blur0Transactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Blur0.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Blur0 *Blur0Session) Receive() (*types.Transaction, error) {
	return _Blur0.Contract.Receive(&_Blur0.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Blur0 *Blur0TransactorSession) Receive() (*types.Transaction, error) {
	return _Blur0.Contract.Receive(&_Blur0.TransactOpts)
}

// Blur0OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Blur0 contract.
type Blur0OwnershipTransferredIterator struct {
	Event *Blur0OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Blur0OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Blur0OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Blur0OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Blur0OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Blur0OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Blur0OwnershipTransferred represents a OwnershipTransferred event raised by the Blur0 contract.
type Blur0OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Blur0 *Blur0Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Blur0OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Blur0.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Blur0OwnershipTransferredIterator{contract: _Blur0.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Blur0 *Blur0Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Blur0OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Blur0.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Blur0OwnershipTransferred)
				if err := _Blur0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Blur0 *Blur0Filterer) ParseOwnershipTransferred(log types.Log) (*Blur0OwnershipTransferred, error) {
	event := new(Blur0OwnershipTransferred)
	if err := _Blur0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
