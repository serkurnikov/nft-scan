// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package niftyconnect_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Niftyconnect0MetaData contains all meta data concerning the Niftyconnect0 contract.
var Niftyconnect0MetaData = &bind.MetaData{
	ABI: "[{\"constant\":true,\"inputs\":[{\"name\":\"addrs\",\"type\":\"address[16]\"},{\"name\":\"uints\",\"type\":\"uint256[12]\"},{\"name\":\"sidesKinds\",\"type\":\"uint8[4]\"},{\"name\":\"calldataBuy\",\"type\":\"bytes\"},{\"name\":\"calldataSell\",\"type\":\"bytes\"},{\"name\":\"replacementPatternBuy\",\"type\":\"bytes\"},{\"name\":\"replacementPatternSell\",\"type\":\"bytes\"},{\"name\":\"staticExtradataBuy\",\"type\":\"bytes\"},{\"name\":\"staticExtradataSell\",\"type\":\"bytes\"}],\"name\":\"calculateMatchPrice_\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"governor\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"tokenTransferProxy\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"exchangeFeeRate\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"},{\"name\":\"calldata\",\"type\":\"bytes\"},{\"name\":\"extradata\",\"type\":\"bytes\"}],\"name\":\"staticCall\",\"outputs\":[{\"name\":\"result\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"addrs\",\"type\":\"address[16]\"},{\"name\":\"uints\",\"type\":\"uint256[12]\"},{\"name\":\"sidesKinds\",\"type\":\"uint8[4]\"},{\"name\":\"calldataBuy\",\"type\":\"bytes\"},{\"name\":\"calldataSell\",\"type\":\"bytes\"},{\"name\":\"replacementPatternBuy\",\"type\":\"bytes\"},{\"name\":\"replacementPatternSell\",\"type\":\"bytes\"},{\"name\":\"staticExtradataBuy\",\"type\":\"bytes\"},{\"name\":\"staticExtradataSell\",\"type\":\"bytes\"}],\"name\":\"ordersCanMatch_\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"addrs\",\"type\":\"address[9]\"},{\"name\":\"uints\",\"type\":\"uint256[9]\"},{\"name\":\"side\",\"type\":\"uint8\"},{\"name\":\"saleKind\",\"type\":\"uint8\"},{\"name\":\"replacementPattern\",\"type\":\"bytes\"},{\"name\":\"staticExtradata\",\"type\":\"bytes\"},{\"name\":\"merkleRoot\",\"type\":\"bytes32\"}],\"name\":\"calculateCurrentPrice_\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"acceptGovernance\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"array\",\"type\":\"bytes\"},{\"name\":\"desired\",\"type\":\"bytes\"},{\"name\":\"mask\",\"type\":\"bytes\"}],\"name\":\"guardedArrayReplace\",\"outputs\":[{\"name\":\"\",\"type\":\"bytes\"}],\"payable\":false,\"stateMutability\":\"pure\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"DOMAIN_SEPARATOR\",\"outputs\":[{\"name\":\"\",\"type\":\"bytes32\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"selector\",\"type\":\"uint256\"},{\"name\":\"from\",\"type\":\"address\"},{\"name\":\"to\",\"type\":\"address\"},{\"name\":\"nftAddress\",\"type\":\"address\"},{\"name\":\"tokenId\",\"type\":\"uint256\"},{\"name\":\"amount\",\"type\":\"uint256\"},{\"name\":\"merkleRoot\",\"type\":\"bytes32\"},{\"name\":\"merkleProof\",\"type\":\"bytes32[]\"}],\"name\":\"buildCallData\",\"outputs\":[{\"name\":\"\",\"type\":\"bytes\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"newTakerRelayerFeeShare\",\"type\":\"uint256\"},{\"name\":\"newMakerRelayerFeeShare\",\"type\":\"uint256\"},{\"name\":\"newProtocolFeeShare\",\"type\":\"uint256\"}],\"name\":\"changeTakerRelayerFeeShare\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"addrs\",\"type\":\"address[9]\"},{\"name\":\"uints\",\"type\":\"uint256[9]\"},{\"name\":\"side\",\"type\":\"uint8\"},{\"name\":\"saleKind\",\"type\":\"uint8\"},{\"name\":\"replacementPattern\",\"type\":\"bytes\"},{\"name\":\"staticExtradata\",\"type\":\"bytes\"},{\"name\":\"merkleRoot\",\"type\":\"bytes32\"}],\"name\":\"validateOrder_\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"royaltyRegisterHub\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"takerRelayerFeeShare\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"newProtocolFeeRecipient\",\"type\":\"address\"}],\"name\":\"changeProtocolFeeRecipient\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"version\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"buyCalldata\",\"type\":\"bytes\"},{\"name\":\"buyReplacementPattern\",\"type\":\"bytes\"},{\"name\":\"sellCalldata\",\"type\":\"bytes\"},{\"name\":\"sellReplacementPattern\",\"type\":\"bytes\"}],\"name\":\"orderCalldataCanMatch\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"pure\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"incrementNonce\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"side\",\"type\":\"uint8\"},{\"name\":\"saleKind\",\"type\":\"uint8\"},{\"name\":\"basePrice\",\"type\":\"uint256\"},{\"name\":\"extra\",\"type\":\"uint256\"},{\"name\":\"listingTime\",\"type\":\"uint256\"},{\"name\":\"expirationTime\",\"type\":\"uint256\"}],\"name\":\"calculateFinalPrice\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"protocolFeeRecipient\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"addrs\",\"type\":\"address[16]\"},{\"name\":\"uints\",\"type\":\"uint256[12]\"},{\"name\":\"sidesKinds\",\"type\":\"uint8[4]\"},{\"name\":\"calldataBuy\",\"type\":\"bytes\"},{\"name\":\"calldataSell\",\"type\":\"bytes\"},{\"name\":\"replacementPatternBuy\",\"type\":\"bytes\"},{\"name\":\"replacementPatternSell\",\"type\":\"bytes\"},{\"name\":\"staticExtradataBuy\",\"type\":\"bytes\"},{\"name\":\"staticExtradataSell\",\"type\":\"bytes\"},{\"name\":\"rssMetadata\",\"type\":\"bytes32\"}],\"name\":\"takeOrder_\",\"outputs\":[],\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"\",\"type\":\"address\"}],\"name\":\"nonces\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"\",\"type\":\"bytes32\"}],\"name\":\"cancelledOrFinalized\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"addrs\",\"type\":\"address[9]\"},{\"name\":\"uints\",\"type\":\"uint256[9]\"},{\"name\":\"side\",\"type\":\"uint8\"},{\"name\":\"saleKind\",\"type\":\"uint8\"},{\"name\":\"replacementPattern\",\"type\":\"bytes\"},{\"name\":\"staticExtradata\",\"type\":\"bytes\"},{\"name\":\"merkleRoot\",\"type\":\"bytes32\"}],\"name\":\"hashToSign_\",\"outputs\":[{\"name\":\"\",\"type\":\"bytes32\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"addrs\",\"type\":\"address[9]\"},{\"name\":\"uints\",\"type\":\"uint256[9]\"},{\"name\":\"side\",\"type\":\"uint8\"},{\"name\":\"saleKind\",\"type\":\"uint8\"},{\"name\":\"replacementPattern\",\"type\":\"bytes\"},{\"name\":\"staticExtradata\",\"type\":\"bytes\"},{\"name\":\"merkleRoot\",\"type\":\"bytes32\"}],\"name\":\"cancelOrder_\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"protocolFeeShare\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"addrs\",\"type\":\"address[9]\"},{\"name\":\"uints\",\"type\":\"uint256[9]\"},{\"name\":\"side\",\"type\":\"uint8\"},{\"name\":\"saleKind\",\"type\":\"uint8\"},{\"name\":\"replacementPattern\",\"type\":\"bytes\"},{\"name\":\"staticExtradata\",\"type\":\"bytes\"},{\"name\":\"merkleData\",\"type\":\"bytes32[2]\"}],\"name\":\"makeOrder_\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"newExchangeFeeRate\",\"type\":\"uint256\"}],\"name\":\"changeExchangeFeeRate\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"INVERSE_BASIS_POINT\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"pendingGovernor\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"MAXIMUM_EXCHANGE_RATE\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"hash\",\"type\":\"bytes32\"}],\"name\":\"approvedOrders\",\"outputs\":[{\"name\":\"approved\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"addrs\",\"type\":\"address[9]\"},{\"name\":\"uints\",\"type\":\"uint256[9]\"},{\"name\":\"side\",\"type\":\"uint8\"},{\"name\":\"saleKind\",\"type\":\"uint8\"},{\"name\":\"replacementPattern\",\"type\":\"bytes\"},{\"name\":\"staticExtradata\",\"type\":\"bytes\"},{\"name\":\"merkleRoot\",\"type\":\"bytes32\"}],\"name\":\"validateOrderParameters_\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"makerRelayerFeeShare\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"merkleValidatorContract\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"pendingGovernor_\",\"type\":\"address\"}],\"name\":\"setPendingGovernor\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"name\":\"tokenTransferProxyAddress\",\"type\":\"address\"},{\"name\":\"protocolFeeAddress\",\"type\":\"address\"},{\"name\":\"merkleValidatorAddress\",\"type\":\"address\"},{\"name\":\"royaltyRegisterHubAddress\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"hash\",\"type\":\"bytes32\"},{\"indexed\":false,\"name\":\"exchange\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"maker\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"taker\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"makerRelayerFeeRecipient\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"side\",\"type\":\"uint8\"},{\"indexed\":false,\"name\":\"saleKind\",\"type\":\"uint8\"},{\"indexed\":false,\"name\":\"nftAddress\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"ipfsHash\",\"type\":\"bytes32\"}],\"name\":\"OrderApprovedPartOne\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"hash\",\"type\":\"bytes32\"},{\"indexed\":false,\"name\":\"calldata\",\"type\":\"bytes\"},{\"indexed\":false,\"name\":\"replacementPattern\",\"type\":\"bytes\"},{\"indexed\":false,\"name\":\"staticTarget\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"staticExtradata\",\"type\":\"bytes\"},{\"indexed\":false,\"name\":\"paymentToken\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"basePrice\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"extra\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"listingTime\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"expirationTime\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"salt\",\"type\":\"uint256\"}],\"name\":\"OrderApprovedPartTwo\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"hash\",\"type\":\"bytes32\"}],\"name\":\"OrderCancelled\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"name\":\"buyHash\",\"type\":\"bytes32\"},{\"indexed\":false,\"name\":\"sellHash\",\"type\":\"bytes32\"},{\"indexed\":true,\"name\":\"maker\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"taker\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"makerRelayerFeeRecipient\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"takerRelayerFeeRecipient\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"price\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"metadata\",\"type\":\"bytes32\"}],\"name\":\"OrdersMatched\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"maker\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"newNonce\",\"type\":\"uint256\"}],\"name\":\"NonceIncremented\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"previousGovernor\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"newGovernor\",\"type\":\"address\"}],\"name\":\"GovernanceTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"newPendingGovernor\",\"type\":\"address\"}],\"name\":\"NewPendingGovernor\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"previousOwner\",\"type\":\"address\"}],\"name\":\"OwnershipRenounced\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"}]",
}

// Niftyconnect0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Niftyconnect0MetaData.ABI instead.
var Niftyconnect0ABI = Niftyconnect0MetaData.ABI

// Niftyconnect0 is an auto generated Go binding around an Ethereum contract.
type Niftyconnect0 struct {
	Niftyconnect0Caller     // Read-only binding to the contract
	Niftyconnect0Transactor // Write-only binding to the contract
	Niftyconnect0Filterer   // Log filterer for contract events
}

// Niftyconnect0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Niftyconnect0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Niftyconnect0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Niftyconnect0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Niftyconnect0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Niftyconnect0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Niftyconnect0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Niftyconnect0Session struct {
	Contract     *Niftyconnect0    // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Niftyconnect0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Niftyconnect0CallerSession struct {
	Contract *Niftyconnect0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts        // Call options to use throughout this session
}

// Niftyconnect0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Niftyconnect0TransactorSession struct {
	Contract     *Niftyconnect0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts        // Transaction auth options to use throughout this session
}

// Niftyconnect0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Niftyconnect0Raw struct {
	Contract *Niftyconnect0 // Generic contract binding to access the raw methods on
}

// Niftyconnect0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Niftyconnect0CallerRaw struct {
	Contract *Niftyconnect0Caller // Generic read-only contract binding to access the raw methods on
}

// Niftyconnect0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Niftyconnect0TransactorRaw struct {
	Contract *Niftyconnect0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewNiftyconnect0 creates a new instance of Niftyconnect0, bound to a specific deployed contract.
func NewNiftyconnect0(address common.Address, backend bind.ContractBackend) (*Niftyconnect0, error) {
	contract, err := bindNiftyconnect0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Niftyconnect0{Niftyconnect0Caller: Niftyconnect0Caller{contract: contract}, Niftyconnect0Transactor: Niftyconnect0Transactor{contract: contract}, Niftyconnect0Filterer: Niftyconnect0Filterer{contract: contract}}, nil
}

// NewNiftyconnect0Caller creates a new read-only instance of Niftyconnect0, bound to a specific deployed contract.
func NewNiftyconnect0Caller(address common.Address, caller bind.ContractCaller) (*Niftyconnect0Caller, error) {
	contract, err := bindNiftyconnect0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Niftyconnect0Caller{contract: contract}, nil
}

// NewNiftyconnect0Transactor creates a new write-only instance of Niftyconnect0, bound to a specific deployed contract.
func NewNiftyconnect0Transactor(address common.Address, transactor bind.ContractTransactor) (*Niftyconnect0Transactor, error) {
	contract, err := bindNiftyconnect0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Niftyconnect0Transactor{contract: contract}, nil
}

// NewNiftyconnect0Filterer creates a new log filterer instance of Niftyconnect0, bound to a specific deployed contract.
func NewNiftyconnect0Filterer(address common.Address, filterer bind.ContractFilterer) (*Niftyconnect0Filterer, error) {
	contract, err := bindNiftyconnect0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Niftyconnect0Filterer{contract: contract}, nil
}

// bindNiftyconnect0 binds a generic wrapper to an already deployed contract.
func bindNiftyconnect0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Niftyconnect0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Niftyconnect0 *Niftyconnect0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Niftyconnect0.Contract.Niftyconnect0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Niftyconnect0 *Niftyconnect0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.Niftyconnect0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Niftyconnect0 *Niftyconnect0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.Niftyconnect0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Niftyconnect0 *Niftyconnect0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Niftyconnect0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Niftyconnect0 *Niftyconnect0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Niftyconnect0 *Niftyconnect0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.contract.Transact(opts, method, params...)
}

// DOMAINSEPARATOR is a free data retrieval call binding the contract method 0x3644e515.
//
// Solidity: function DOMAIN_SEPARATOR() view returns(bytes32)
func (_Niftyconnect0 *Niftyconnect0Caller) DOMAINSEPARATOR(opts *bind.CallOpts) ([32]byte, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "DOMAIN_SEPARATOR")

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// DOMAINSEPARATOR is a free data retrieval call binding the contract method 0x3644e515.
//
// Solidity: function DOMAIN_SEPARATOR() view returns(bytes32)
func (_Niftyconnect0 *Niftyconnect0Session) DOMAINSEPARATOR() ([32]byte, error) {
	return _Niftyconnect0.Contract.DOMAINSEPARATOR(&_Niftyconnect0.CallOpts)
}

// DOMAINSEPARATOR is a free data retrieval call binding the contract method 0x3644e515.
//
// Solidity: function DOMAIN_SEPARATOR() view returns(bytes32)
func (_Niftyconnect0 *Niftyconnect0CallerSession) DOMAINSEPARATOR() ([32]byte, error) {
	return _Niftyconnect0.Contract.DOMAINSEPARATOR(&_Niftyconnect0.CallOpts)
}

// INVERSEBASISPOINT is a free data retrieval call binding the contract method 0xcae6047f.
//
// Solidity: function INVERSE_BASIS_POINT() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Caller) INVERSEBASISPOINT(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "INVERSE_BASIS_POINT")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// INVERSEBASISPOINT is a free data retrieval call binding the contract method 0xcae6047f.
//
// Solidity: function INVERSE_BASIS_POINT() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Session) INVERSEBASISPOINT() (*big.Int, error) {
	return _Niftyconnect0.Contract.INVERSEBASISPOINT(&_Niftyconnect0.CallOpts)
}

// INVERSEBASISPOINT is a free data retrieval call binding the contract method 0xcae6047f.
//
// Solidity: function INVERSE_BASIS_POINT() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0CallerSession) INVERSEBASISPOINT() (*big.Int, error) {
	return _Niftyconnect0.Contract.INVERSEBASISPOINT(&_Niftyconnect0.CallOpts)
}

// MAXIMUMEXCHANGERATE is a free data retrieval call binding the contract method 0xe4e098f7.
//
// Solidity: function MAXIMUM_EXCHANGE_RATE() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Caller) MAXIMUMEXCHANGERATE(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "MAXIMUM_EXCHANGE_RATE")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MAXIMUMEXCHANGERATE is a free data retrieval call binding the contract method 0xe4e098f7.
//
// Solidity: function MAXIMUM_EXCHANGE_RATE() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Session) MAXIMUMEXCHANGERATE() (*big.Int, error) {
	return _Niftyconnect0.Contract.MAXIMUMEXCHANGERATE(&_Niftyconnect0.CallOpts)
}

// MAXIMUMEXCHANGERATE is a free data retrieval call binding the contract method 0xe4e098f7.
//
// Solidity: function MAXIMUM_EXCHANGE_RATE() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0CallerSession) MAXIMUMEXCHANGERATE() (*big.Int, error) {
	return _Niftyconnect0.Contract.MAXIMUMEXCHANGERATE(&_Niftyconnect0.CallOpts)
}

// ApprovedOrders is a free data retrieval call binding the contract method 0xe57d4adb.
//
// Solidity: function approvedOrders(bytes32 hash) view returns(bool approved)
func (_Niftyconnect0 *Niftyconnect0Caller) ApprovedOrders(opts *bind.CallOpts, hash [32]byte) (bool, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "approvedOrders", hash)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// ApprovedOrders is a free data retrieval call binding the contract method 0xe57d4adb.
//
// Solidity: function approvedOrders(bytes32 hash) view returns(bool approved)
func (_Niftyconnect0 *Niftyconnect0Session) ApprovedOrders(hash [32]byte) (bool, error) {
	return _Niftyconnect0.Contract.ApprovedOrders(&_Niftyconnect0.CallOpts, hash)
}

// ApprovedOrders is a free data retrieval call binding the contract method 0xe57d4adb.
//
// Solidity: function approvedOrders(bytes32 hash) view returns(bool approved)
func (_Niftyconnect0 *Niftyconnect0CallerSession) ApprovedOrders(hash [32]byte) (bool, error) {
	return _Niftyconnect0.Contract.ApprovedOrders(&_Niftyconnect0.CallOpts, hash)
}

// BuildCallData is a free data retrieval call binding the contract method 0x37146f2e.
//
// Solidity: function buildCallData(uint256 selector, address from, address to, address nftAddress, uint256 tokenId, uint256 amount, bytes32 merkleRoot, bytes32[] merkleProof) view returns(bytes)
func (_Niftyconnect0 *Niftyconnect0Caller) BuildCallData(opts *bind.CallOpts, selector *big.Int, from common.Address, to common.Address, nftAddress common.Address, tokenId *big.Int, amount *big.Int, merkleRoot [32]byte, merkleProof [][32]byte) ([]byte, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "buildCallData", selector, from, to, nftAddress, tokenId, amount, merkleRoot, merkleProof)

	if err != nil {
		return *new([]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([]byte)).(*[]byte)

	return out0, err

}

// BuildCallData is a free data retrieval call binding the contract method 0x37146f2e.
//
// Solidity: function buildCallData(uint256 selector, address from, address to, address nftAddress, uint256 tokenId, uint256 amount, bytes32 merkleRoot, bytes32[] merkleProof) view returns(bytes)
func (_Niftyconnect0 *Niftyconnect0Session) BuildCallData(selector *big.Int, from common.Address, to common.Address, nftAddress common.Address, tokenId *big.Int, amount *big.Int, merkleRoot [32]byte, merkleProof [][32]byte) ([]byte, error) {
	return _Niftyconnect0.Contract.BuildCallData(&_Niftyconnect0.CallOpts, selector, from, to, nftAddress, tokenId, amount, merkleRoot, merkleProof)
}

// BuildCallData is a free data retrieval call binding the contract method 0x37146f2e.
//
// Solidity: function buildCallData(uint256 selector, address from, address to, address nftAddress, uint256 tokenId, uint256 amount, bytes32 merkleRoot, bytes32[] merkleProof) view returns(bytes)
func (_Niftyconnect0 *Niftyconnect0CallerSession) BuildCallData(selector *big.Int, from common.Address, to common.Address, nftAddress common.Address, tokenId *big.Int, amount *big.Int, merkleRoot [32]byte, merkleProof [][32]byte) ([]byte, error) {
	return _Niftyconnect0.Contract.BuildCallData(&_Niftyconnect0.CallOpts, selector, from, to, nftAddress, tokenId, amount, merkleRoot, merkleProof)
}

// CalculateCurrentPrice is a free data retrieval call binding the contract method 0x1f86dbc0.
//
// Solidity: function calculateCurrentPrice_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Caller) CalculateCurrentPrice(opts *bind.CallOpts, addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) (*big.Int, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "calculateCurrentPrice_", addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CalculateCurrentPrice is a free data retrieval call binding the contract method 0x1f86dbc0.
//
// Solidity: function calculateCurrentPrice_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Session) CalculateCurrentPrice(addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) (*big.Int, error) {
	return _Niftyconnect0.Contract.CalculateCurrentPrice(&_Niftyconnect0.CallOpts, addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)
}

// CalculateCurrentPrice is a free data retrieval call binding the contract method 0x1f86dbc0.
//
// Solidity: function calculateCurrentPrice_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0CallerSession) CalculateCurrentPrice(addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) (*big.Int, error) {
	return _Niftyconnect0.Contract.CalculateCurrentPrice(&_Niftyconnect0.CallOpts, addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)
}

// CalculateFinalPrice is a free data retrieval call binding the contract method 0x63d36c0b.
//
// Solidity: function calculateFinalPrice(uint8 side, uint8 saleKind, uint256 basePrice, uint256 extra, uint256 listingTime, uint256 expirationTime) view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Caller) CalculateFinalPrice(opts *bind.CallOpts, side uint8, saleKind uint8, basePrice *big.Int, extra *big.Int, listingTime *big.Int, expirationTime *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "calculateFinalPrice", side, saleKind, basePrice, extra, listingTime, expirationTime)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CalculateFinalPrice is a free data retrieval call binding the contract method 0x63d36c0b.
//
// Solidity: function calculateFinalPrice(uint8 side, uint8 saleKind, uint256 basePrice, uint256 extra, uint256 listingTime, uint256 expirationTime) view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Session) CalculateFinalPrice(side uint8, saleKind uint8, basePrice *big.Int, extra *big.Int, listingTime *big.Int, expirationTime *big.Int) (*big.Int, error) {
	return _Niftyconnect0.Contract.CalculateFinalPrice(&_Niftyconnect0.CallOpts, side, saleKind, basePrice, extra, listingTime, expirationTime)
}

// CalculateFinalPrice is a free data retrieval call binding the contract method 0x63d36c0b.
//
// Solidity: function calculateFinalPrice(uint8 side, uint8 saleKind, uint256 basePrice, uint256 extra, uint256 listingTime, uint256 expirationTime) view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0CallerSession) CalculateFinalPrice(side uint8, saleKind uint8, basePrice *big.Int, extra *big.Int, listingTime *big.Int, expirationTime *big.Int) (*big.Int, error) {
	return _Niftyconnect0.Contract.CalculateFinalPrice(&_Niftyconnect0.CallOpts, side, saleKind, basePrice, extra, listingTime, expirationTime)
}

// CalculateMatchPrice is a free data retrieval call binding the contract method 0x028e01cf.
//
// Solidity: function calculateMatchPrice_(address[16] addrs, uint256[12] uints, uint8[4] sidesKinds, bytes calldataBuy, bytes calldataSell, bytes replacementPatternBuy, bytes replacementPatternSell, bytes staticExtradataBuy, bytes staticExtradataSell) view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Caller) CalculateMatchPrice(opts *bind.CallOpts, addrs [16]common.Address, uints [12]*big.Int, sidesKinds [4]uint8, calldataBuy []byte, calldataSell []byte, replacementPatternBuy []byte, replacementPatternSell []byte, staticExtradataBuy []byte, staticExtradataSell []byte) (*big.Int, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "calculateMatchPrice_", addrs, uints, sidesKinds, calldataBuy, calldataSell, replacementPatternBuy, replacementPatternSell, staticExtradataBuy, staticExtradataSell)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CalculateMatchPrice is a free data retrieval call binding the contract method 0x028e01cf.
//
// Solidity: function calculateMatchPrice_(address[16] addrs, uint256[12] uints, uint8[4] sidesKinds, bytes calldataBuy, bytes calldataSell, bytes replacementPatternBuy, bytes replacementPatternSell, bytes staticExtradataBuy, bytes staticExtradataSell) view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Session) CalculateMatchPrice(addrs [16]common.Address, uints [12]*big.Int, sidesKinds [4]uint8, calldataBuy []byte, calldataSell []byte, replacementPatternBuy []byte, replacementPatternSell []byte, staticExtradataBuy []byte, staticExtradataSell []byte) (*big.Int, error) {
	return _Niftyconnect0.Contract.CalculateMatchPrice(&_Niftyconnect0.CallOpts, addrs, uints, sidesKinds, calldataBuy, calldataSell, replacementPatternBuy, replacementPatternSell, staticExtradataBuy, staticExtradataSell)
}

// CalculateMatchPrice is a free data retrieval call binding the contract method 0x028e01cf.
//
// Solidity: function calculateMatchPrice_(address[16] addrs, uint256[12] uints, uint8[4] sidesKinds, bytes calldataBuy, bytes calldataSell, bytes replacementPatternBuy, bytes replacementPatternSell, bytes staticExtradataBuy, bytes staticExtradataSell) view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0CallerSession) CalculateMatchPrice(addrs [16]common.Address, uints [12]*big.Int, sidesKinds [4]uint8, calldataBuy []byte, calldataSell []byte, replacementPatternBuy []byte, replacementPatternSell []byte, staticExtradataBuy []byte, staticExtradataSell []byte) (*big.Int, error) {
	return _Niftyconnect0.Contract.CalculateMatchPrice(&_Niftyconnect0.CallOpts, addrs, uints, sidesKinds, calldataBuy, calldataSell, replacementPatternBuy, replacementPatternSell, staticExtradataBuy, staticExtradataSell)
}

// CancelledOrFinalized is a free data retrieval call binding the contract method 0x8076f005.
//
// Solidity: function cancelledOrFinalized(bytes32 ) view returns(bool)
func (_Niftyconnect0 *Niftyconnect0Caller) CancelledOrFinalized(opts *bind.CallOpts, arg0 [32]byte) (bool, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "cancelledOrFinalized", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// CancelledOrFinalized is a free data retrieval call binding the contract method 0x8076f005.
//
// Solidity: function cancelledOrFinalized(bytes32 ) view returns(bool)
func (_Niftyconnect0 *Niftyconnect0Session) CancelledOrFinalized(arg0 [32]byte) (bool, error) {
	return _Niftyconnect0.Contract.CancelledOrFinalized(&_Niftyconnect0.CallOpts, arg0)
}

// CancelledOrFinalized is a free data retrieval call binding the contract method 0x8076f005.
//
// Solidity: function cancelledOrFinalized(bytes32 ) view returns(bool)
func (_Niftyconnect0 *Niftyconnect0CallerSession) CancelledOrFinalized(arg0 [32]byte) (bool, error) {
	return _Niftyconnect0.Contract.CancelledOrFinalized(&_Niftyconnect0.CallOpts, arg0)
}

// ExchangeFeeRate is a free data retrieval call binding the contract method 0x0f9b4955.
//
// Solidity: function exchangeFeeRate() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Caller) ExchangeFeeRate(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "exchangeFeeRate")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// ExchangeFeeRate is a free data retrieval call binding the contract method 0x0f9b4955.
//
// Solidity: function exchangeFeeRate() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Session) ExchangeFeeRate() (*big.Int, error) {
	return _Niftyconnect0.Contract.ExchangeFeeRate(&_Niftyconnect0.CallOpts)
}

// ExchangeFeeRate is a free data retrieval call binding the contract method 0x0f9b4955.
//
// Solidity: function exchangeFeeRate() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0CallerSession) ExchangeFeeRate() (*big.Int, error) {
	return _Niftyconnect0.Contract.ExchangeFeeRate(&_Niftyconnect0.CallOpts)
}

// Governor is a free data retrieval call binding the contract method 0x0c340a24.
//
// Solidity: function governor() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Caller) Governor(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "governor")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Governor is a free data retrieval call binding the contract method 0x0c340a24.
//
// Solidity: function governor() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Session) Governor() (common.Address, error) {
	return _Niftyconnect0.Contract.Governor(&_Niftyconnect0.CallOpts)
}

// Governor is a free data retrieval call binding the contract method 0x0c340a24.
//
// Solidity: function governor() view returns(address)
func (_Niftyconnect0 *Niftyconnect0CallerSession) Governor() (common.Address, error) {
	return _Niftyconnect0.Contract.Governor(&_Niftyconnect0.CallOpts)
}

// GuardedArrayReplace is a free data retrieval call binding the contract method 0x239e83df.
//
// Solidity: function guardedArrayReplace(bytes array, bytes desired, bytes mask) pure returns(bytes)
func (_Niftyconnect0 *Niftyconnect0Caller) GuardedArrayReplace(opts *bind.CallOpts, array []byte, desired []byte, mask []byte) ([]byte, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "guardedArrayReplace", array, desired, mask)

	if err != nil {
		return *new([]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([]byte)).(*[]byte)

	return out0, err

}

// GuardedArrayReplace is a free data retrieval call binding the contract method 0x239e83df.
//
// Solidity: function guardedArrayReplace(bytes array, bytes desired, bytes mask) pure returns(bytes)
func (_Niftyconnect0 *Niftyconnect0Session) GuardedArrayReplace(array []byte, desired []byte, mask []byte) ([]byte, error) {
	return _Niftyconnect0.Contract.GuardedArrayReplace(&_Niftyconnect0.CallOpts, array, desired, mask)
}

// GuardedArrayReplace is a free data retrieval call binding the contract method 0x239e83df.
//
// Solidity: function guardedArrayReplace(bytes array, bytes desired, bytes mask) pure returns(bytes)
func (_Niftyconnect0 *Niftyconnect0CallerSession) GuardedArrayReplace(array []byte, desired []byte, mask []byte) ([]byte, error) {
	return _Niftyconnect0.Contract.GuardedArrayReplace(&_Niftyconnect0.CallOpts, array, desired, mask)
}

// HashToSign is a free data retrieval call binding the contract method 0x81da91a0.
//
// Solidity: function hashToSign_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) view returns(bytes32)
func (_Niftyconnect0 *Niftyconnect0Caller) HashToSign(opts *bind.CallOpts, addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) ([32]byte, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "hashToSign_", addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// HashToSign is a free data retrieval call binding the contract method 0x81da91a0.
//
// Solidity: function hashToSign_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) view returns(bytes32)
func (_Niftyconnect0 *Niftyconnect0Session) HashToSign(addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) ([32]byte, error) {
	return _Niftyconnect0.Contract.HashToSign(&_Niftyconnect0.CallOpts, addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)
}

// HashToSign is a free data retrieval call binding the contract method 0x81da91a0.
//
// Solidity: function hashToSign_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) view returns(bytes32)
func (_Niftyconnect0 *Niftyconnect0CallerSession) HashToSign(addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) ([32]byte, error) {
	return _Niftyconnect0.Contract.HashToSign(&_Niftyconnect0.CallOpts, addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)
}

// MakerRelayerFeeShare is a free data retrieval call binding the contract method 0xe8898e6d.
//
// Solidity: function makerRelayerFeeShare() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Caller) MakerRelayerFeeShare(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "makerRelayerFeeShare")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MakerRelayerFeeShare is a free data retrieval call binding the contract method 0xe8898e6d.
//
// Solidity: function makerRelayerFeeShare() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Session) MakerRelayerFeeShare() (*big.Int, error) {
	return _Niftyconnect0.Contract.MakerRelayerFeeShare(&_Niftyconnect0.CallOpts)
}

// MakerRelayerFeeShare is a free data retrieval call binding the contract method 0xe8898e6d.
//
// Solidity: function makerRelayerFeeShare() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0CallerSession) MakerRelayerFeeShare() (*big.Int, error) {
	return _Niftyconnect0.Contract.MakerRelayerFeeShare(&_Niftyconnect0.CallOpts)
}

// MerkleValidatorContract is a free data retrieval call binding the contract method 0xf1113575.
//
// Solidity: function merkleValidatorContract() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Caller) MerkleValidatorContract(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "merkleValidatorContract")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// MerkleValidatorContract is a free data retrieval call binding the contract method 0xf1113575.
//
// Solidity: function merkleValidatorContract() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Session) MerkleValidatorContract() (common.Address, error) {
	return _Niftyconnect0.Contract.MerkleValidatorContract(&_Niftyconnect0.CallOpts)
}

// MerkleValidatorContract is a free data retrieval call binding the contract method 0xf1113575.
//
// Solidity: function merkleValidatorContract() view returns(address)
func (_Niftyconnect0 *Niftyconnect0CallerSession) MerkleValidatorContract() (common.Address, error) {
	return _Niftyconnect0.Contract.MerkleValidatorContract(&_Niftyconnect0.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Niftyconnect0 *Niftyconnect0Caller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Niftyconnect0 *Niftyconnect0Session) Name() (string, error) {
	return _Niftyconnect0.Contract.Name(&_Niftyconnect0.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Niftyconnect0 *Niftyconnect0CallerSession) Name() (string, error) {
	return _Niftyconnect0.Contract.Name(&_Niftyconnect0.CallOpts)
}

// Nonces is a free data retrieval call binding the contract method 0x7ecebe00.
//
// Solidity: function nonces(address ) view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Caller) Nonces(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "nonces", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Nonces is a free data retrieval call binding the contract method 0x7ecebe00.
//
// Solidity: function nonces(address ) view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Session) Nonces(arg0 common.Address) (*big.Int, error) {
	return _Niftyconnect0.Contract.Nonces(&_Niftyconnect0.CallOpts, arg0)
}

// Nonces is a free data retrieval call binding the contract method 0x7ecebe00.
//
// Solidity: function nonces(address ) view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0CallerSession) Nonces(arg0 common.Address) (*big.Int, error) {
	return _Niftyconnect0.Contract.Nonces(&_Niftyconnect0.CallOpts, arg0)
}

// OrderCalldataCanMatch is a free data retrieval call binding the contract method 0x562b2ebc.
//
// Solidity: function orderCalldataCanMatch(bytes buyCalldata, bytes buyReplacementPattern, bytes sellCalldata, bytes sellReplacementPattern) pure returns(bool)
func (_Niftyconnect0 *Niftyconnect0Caller) OrderCalldataCanMatch(opts *bind.CallOpts, buyCalldata []byte, buyReplacementPattern []byte, sellCalldata []byte, sellReplacementPattern []byte) (bool, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "orderCalldataCanMatch", buyCalldata, buyReplacementPattern, sellCalldata, sellReplacementPattern)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// OrderCalldataCanMatch is a free data retrieval call binding the contract method 0x562b2ebc.
//
// Solidity: function orderCalldataCanMatch(bytes buyCalldata, bytes buyReplacementPattern, bytes sellCalldata, bytes sellReplacementPattern) pure returns(bool)
func (_Niftyconnect0 *Niftyconnect0Session) OrderCalldataCanMatch(buyCalldata []byte, buyReplacementPattern []byte, sellCalldata []byte, sellReplacementPattern []byte) (bool, error) {
	return _Niftyconnect0.Contract.OrderCalldataCanMatch(&_Niftyconnect0.CallOpts, buyCalldata, buyReplacementPattern, sellCalldata, sellReplacementPattern)
}

// OrderCalldataCanMatch is a free data retrieval call binding the contract method 0x562b2ebc.
//
// Solidity: function orderCalldataCanMatch(bytes buyCalldata, bytes buyReplacementPattern, bytes sellCalldata, bytes sellReplacementPattern) pure returns(bool)
func (_Niftyconnect0 *Niftyconnect0CallerSession) OrderCalldataCanMatch(buyCalldata []byte, buyReplacementPattern []byte, sellCalldata []byte, sellReplacementPattern []byte) (bool, error) {
	return _Niftyconnect0.Contract.OrderCalldataCanMatch(&_Niftyconnect0.CallOpts, buyCalldata, buyReplacementPattern, sellCalldata, sellReplacementPattern)
}

// OrdersCanMatch is a free data retrieval call binding the contract method 0x1f2c56d7.
//
// Solidity: function ordersCanMatch_(address[16] addrs, uint256[12] uints, uint8[4] sidesKinds, bytes calldataBuy, bytes calldataSell, bytes replacementPatternBuy, bytes replacementPatternSell, bytes staticExtradataBuy, bytes staticExtradataSell) view returns(bool)
func (_Niftyconnect0 *Niftyconnect0Caller) OrdersCanMatch(opts *bind.CallOpts, addrs [16]common.Address, uints [12]*big.Int, sidesKinds [4]uint8, calldataBuy []byte, calldataSell []byte, replacementPatternBuy []byte, replacementPatternSell []byte, staticExtradataBuy []byte, staticExtradataSell []byte) (bool, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "ordersCanMatch_", addrs, uints, sidesKinds, calldataBuy, calldataSell, replacementPatternBuy, replacementPatternSell, staticExtradataBuy, staticExtradataSell)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// OrdersCanMatch is a free data retrieval call binding the contract method 0x1f2c56d7.
//
// Solidity: function ordersCanMatch_(address[16] addrs, uint256[12] uints, uint8[4] sidesKinds, bytes calldataBuy, bytes calldataSell, bytes replacementPatternBuy, bytes replacementPatternSell, bytes staticExtradataBuy, bytes staticExtradataSell) view returns(bool)
func (_Niftyconnect0 *Niftyconnect0Session) OrdersCanMatch(addrs [16]common.Address, uints [12]*big.Int, sidesKinds [4]uint8, calldataBuy []byte, calldataSell []byte, replacementPatternBuy []byte, replacementPatternSell []byte, staticExtradataBuy []byte, staticExtradataSell []byte) (bool, error) {
	return _Niftyconnect0.Contract.OrdersCanMatch(&_Niftyconnect0.CallOpts, addrs, uints, sidesKinds, calldataBuy, calldataSell, replacementPatternBuy, replacementPatternSell, staticExtradataBuy, staticExtradataSell)
}

// OrdersCanMatch is a free data retrieval call binding the contract method 0x1f2c56d7.
//
// Solidity: function ordersCanMatch_(address[16] addrs, uint256[12] uints, uint8[4] sidesKinds, bytes calldataBuy, bytes calldataSell, bytes replacementPatternBuy, bytes replacementPatternSell, bytes staticExtradataBuy, bytes staticExtradataSell) view returns(bool)
func (_Niftyconnect0 *Niftyconnect0CallerSession) OrdersCanMatch(addrs [16]common.Address, uints [12]*big.Int, sidesKinds [4]uint8, calldataBuy []byte, calldataSell []byte, replacementPatternBuy []byte, replacementPatternSell []byte, staticExtradataBuy []byte, staticExtradataSell []byte) (bool, error) {
	return _Niftyconnect0.Contract.OrdersCanMatch(&_Niftyconnect0.CallOpts, addrs, uints, sidesKinds, calldataBuy, calldataSell, replacementPatternBuy, replacementPatternSell, staticExtradataBuy, staticExtradataSell)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Session) Owner() (common.Address, error) {
	return _Niftyconnect0.Contract.Owner(&_Niftyconnect0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Niftyconnect0 *Niftyconnect0CallerSession) Owner() (common.Address, error) {
	return _Niftyconnect0.Contract.Owner(&_Niftyconnect0.CallOpts)
}

// PendingGovernor is a free data retrieval call binding the contract method 0xe3056a34.
//
// Solidity: function pendingGovernor() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Caller) PendingGovernor(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "pendingGovernor")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// PendingGovernor is a free data retrieval call binding the contract method 0xe3056a34.
//
// Solidity: function pendingGovernor() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Session) PendingGovernor() (common.Address, error) {
	return _Niftyconnect0.Contract.PendingGovernor(&_Niftyconnect0.CallOpts)
}

// PendingGovernor is a free data retrieval call binding the contract method 0xe3056a34.
//
// Solidity: function pendingGovernor() view returns(address)
func (_Niftyconnect0 *Niftyconnect0CallerSession) PendingGovernor() (common.Address, error) {
	return _Niftyconnect0.Contract.PendingGovernor(&_Niftyconnect0.CallOpts)
}

// ProtocolFeeRecipient is a free data retrieval call binding the contract method 0x64df049e.
//
// Solidity: function protocolFeeRecipient() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Caller) ProtocolFeeRecipient(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "protocolFeeRecipient")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// ProtocolFeeRecipient is a free data retrieval call binding the contract method 0x64df049e.
//
// Solidity: function protocolFeeRecipient() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Session) ProtocolFeeRecipient() (common.Address, error) {
	return _Niftyconnect0.Contract.ProtocolFeeRecipient(&_Niftyconnect0.CallOpts)
}

// ProtocolFeeRecipient is a free data retrieval call binding the contract method 0x64df049e.
//
// Solidity: function protocolFeeRecipient() view returns(address)
func (_Niftyconnect0 *Niftyconnect0CallerSession) ProtocolFeeRecipient() (common.Address, error) {
	return _Niftyconnect0.Contract.ProtocolFeeRecipient(&_Niftyconnect0.CallOpts)
}

// ProtocolFeeShare is a free data retrieval call binding the contract method 0x960b26a2.
//
// Solidity: function protocolFeeShare() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Caller) ProtocolFeeShare(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "protocolFeeShare")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// ProtocolFeeShare is a free data retrieval call binding the contract method 0x960b26a2.
//
// Solidity: function protocolFeeShare() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Session) ProtocolFeeShare() (*big.Int, error) {
	return _Niftyconnect0.Contract.ProtocolFeeShare(&_Niftyconnect0.CallOpts)
}

// ProtocolFeeShare is a free data retrieval call binding the contract method 0x960b26a2.
//
// Solidity: function protocolFeeShare() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0CallerSession) ProtocolFeeShare() (*big.Int, error) {
	return _Niftyconnect0.Contract.ProtocolFeeShare(&_Niftyconnect0.CallOpts)
}

// RoyaltyRegisterHub is a free data retrieval call binding the contract method 0x3eeb5bc8.
//
// Solidity: function royaltyRegisterHub() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Caller) RoyaltyRegisterHub(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "royaltyRegisterHub")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// RoyaltyRegisterHub is a free data retrieval call binding the contract method 0x3eeb5bc8.
//
// Solidity: function royaltyRegisterHub() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Session) RoyaltyRegisterHub() (common.Address, error) {
	return _Niftyconnect0.Contract.RoyaltyRegisterHub(&_Niftyconnect0.CallOpts)
}

// RoyaltyRegisterHub is a free data retrieval call binding the contract method 0x3eeb5bc8.
//
// Solidity: function royaltyRegisterHub() view returns(address)
func (_Niftyconnect0 *Niftyconnect0CallerSession) RoyaltyRegisterHub() (common.Address, error) {
	return _Niftyconnect0.Contract.RoyaltyRegisterHub(&_Niftyconnect0.CallOpts)
}

// StaticCall is a free data retrieval call binding the contract method 0x10796a47.
//
// Solidity: function staticCall(address target, bytes calldata, bytes extradata) view returns(bool result)
func (_Niftyconnect0 *Niftyconnect0Caller) StaticCall(opts *bind.CallOpts, target common.Address, calldata []byte, extradata []byte) (bool, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "staticCall", target, calldata, extradata)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// StaticCall is a free data retrieval call binding the contract method 0x10796a47.
//
// Solidity: function staticCall(address target, bytes calldata, bytes extradata) view returns(bool result)
func (_Niftyconnect0 *Niftyconnect0Session) StaticCall(target common.Address, calldata []byte, extradata []byte) (bool, error) {
	return _Niftyconnect0.Contract.StaticCall(&_Niftyconnect0.CallOpts, target, calldata, extradata)
}

// StaticCall is a free data retrieval call binding the contract method 0x10796a47.
//
// Solidity: function staticCall(address target, bytes calldata, bytes extradata) view returns(bool result)
func (_Niftyconnect0 *Niftyconnect0CallerSession) StaticCall(target common.Address, calldata []byte, extradata []byte) (bool, error) {
	return _Niftyconnect0.Contract.StaticCall(&_Niftyconnect0.CallOpts, target, calldata, extradata)
}

// TakerRelayerFeeShare is a free data retrieval call binding the contract method 0x4a3b5e05.
//
// Solidity: function takerRelayerFeeShare() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Caller) TakerRelayerFeeShare(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "takerRelayerFeeShare")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TakerRelayerFeeShare is a free data retrieval call binding the contract method 0x4a3b5e05.
//
// Solidity: function takerRelayerFeeShare() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0Session) TakerRelayerFeeShare() (*big.Int, error) {
	return _Niftyconnect0.Contract.TakerRelayerFeeShare(&_Niftyconnect0.CallOpts)
}

// TakerRelayerFeeShare is a free data retrieval call binding the contract method 0x4a3b5e05.
//
// Solidity: function takerRelayerFeeShare() view returns(uint256)
func (_Niftyconnect0 *Niftyconnect0CallerSession) TakerRelayerFeeShare() (*big.Int, error) {
	return _Niftyconnect0.Contract.TakerRelayerFeeShare(&_Niftyconnect0.CallOpts)
}

// TokenTransferProxy is a free data retrieval call binding the contract method 0x0eefdbad.
//
// Solidity: function tokenTransferProxy() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Caller) TokenTransferProxy(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "tokenTransferProxy")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// TokenTransferProxy is a free data retrieval call binding the contract method 0x0eefdbad.
//
// Solidity: function tokenTransferProxy() view returns(address)
func (_Niftyconnect0 *Niftyconnect0Session) TokenTransferProxy() (common.Address, error) {
	return _Niftyconnect0.Contract.TokenTransferProxy(&_Niftyconnect0.CallOpts)
}

// TokenTransferProxy is a free data retrieval call binding the contract method 0x0eefdbad.
//
// Solidity: function tokenTransferProxy() view returns(address)
func (_Niftyconnect0 *Niftyconnect0CallerSession) TokenTransferProxy() (common.Address, error) {
	return _Niftyconnect0.Contract.TokenTransferProxy(&_Niftyconnect0.CallOpts)
}

// ValidateOrderParameters is a free data retrieval call binding the contract method 0xe7b74b64.
//
// Solidity: function validateOrderParameters_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) view returns(bool)
func (_Niftyconnect0 *Niftyconnect0Caller) ValidateOrderParameters(opts *bind.CallOpts, addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) (bool, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "validateOrderParameters_", addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// ValidateOrderParameters is a free data retrieval call binding the contract method 0xe7b74b64.
//
// Solidity: function validateOrderParameters_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) view returns(bool)
func (_Niftyconnect0 *Niftyconnect0Session) ValidateOrderParameters(addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) (bool, error) {
	return _Niftyconnect0.Contract.ValidateOrderParameters(&_Niftyconnect0.CallOpts, addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)
}

// ValidateOrderParameters is a free data retrieval call binding the contract method 0xe7b74b64.
//
// Solidity: function validateOrderParameters_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) view returns(bool)
func (_Niftyconnect0 *Niftyconnect0CallerSession) ValidateOrderParameters(addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) (bool, error) {
	return _Niftyconnect0.Contract.ValidateOrderParameters(&_Niftyconnect0.CallOpts, addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)
}

// ValidateOrder is a free data retrieval call binding the contract method 0x3df6be13.
//
// Solidity: function validateOrder_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) view returns(bool)
func (_Niftyconnect0 *Niftyconnect0Caller) ValidateOrder(opts *bind.CallOpts, addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) (bool, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "validateOrder_", addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// ValidateOrder is a free data retrieval call binding the contract method 0x3df6be13.
//
// Solidity: function validateOrder_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) view returns(bool)
func (_Niftyconnect0 *Niftyconnect0Session) ValidateOrder(addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) (bool, error) {
	return _Niftyconnect0.Contract.ValidateOrder(&_Niftyconnect0.CallOpts, addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)
}

// ValidateOrder is a free data retrieval call binding the contract method 0x3df6be13.
//
// Solidity: function validateOrder_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) view returns(bool)
func (_Niftyconnect0 *Niftyconnect0CallerSession) ValidateOrder(addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) (bool, error) {
	return _Niftyconnect0.Contract.ValidateOrder(&_Niftyconnect0.CallOpts, addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)
}

// Version is a free data retrieval call binding the contract method 0x54fd4d50.
//
// Solidity: function version() view returns(string)
func (_Niftyconnect0 *Niftyconnect0Caller) Version(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Niftyconnect0.contract.Call(opts, &out, "version")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Version is a free data retrieval call binding the contract method 0x54fd4d50.
//
// Solidity: function version() view returns(string)
func (_Niftyconnect0 *Niftyconnect0Session) Version() (string, error) {
	return _Niftyconnect0.Contract.Version(&_Niftyconnect0.CallOpts)
}

// Version is a free data retrieval call binding the contract method 0x54fd4d50.
//
// Solidity: function version() view returns(string)
func (_Niftyconnect0 *Niftyconnect0CallerSession) Version() (string, error) {
	return _Niftyconnect0.Contract.Version(&_Niftyconnect0.CallOpts)
}

// AcceptGovernance is a paid mutator transaction binding the contract method 0x238efcbc.
//
// Solidity: function acceptGovernance() returns()
func (_Niftyconnect0 *Niftyconnect0Transactor) AcceptGovernance(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Niftyconnect0.contract.Transact(opts, "acceptGovernance")
}

// AcceptGovernance is a paid mutator transaction binding the contract method 0x238efcbc.
//
// Solidity: function acceptGovernance() returns()
func (_Niftyconnect0 *Niftyconnect0Session) AcceptGovernance() (*types.Transaction, error) {
	return _Niftyconnect0.Contract.AcceptGovernance(&_Niftyconnect0.TransactOpts)
}

// AcceptGovernance is a paid mutator transaction binding the contract method 0x238efcbc.
//
// Solidity: function acceptGovernance() returns()
func (_Niftyconnect0 *Niftyconnect0TransactorSession) AcceptGovernance() (*types.Transaction, error) {
	return _Niftyconnect0.Contract.AcceptGovernance(&_Niftyconnect0.TransactOpts)
}

// CancelOrder is a paid mutator transaction binding the contract method 0x94146166.
//
// Solidity: function cancelOrder_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) returns()
func (_Niftyconnect0 *Niftyconnect0Transactor) CancelOrder(opts *bind.TransactOpts, addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) (*types.Transaction, error) {
	return _Niftyconnect0.contract.Transact(opts, "cancelOrder_", addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)
}

// CancelOrder is a paid mutator transaction binding the contract method 0x94146166.
//
// Solidity: function cancelOrder_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) returns()
func (_Niftyconnect0 *Niftyconnect0Session) CancelOrder(addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.CancelOrder(&_Niftyconnect0.TransactOpts, addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)
}

// CancelOrder is a paid mutator transaction binding the contract method 0x94146166.
//
// Solidity: function cancelOrder_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32 merkleRoot) returns()
func (_Niftyconnect0 *Niftyconnect0TransactorSession) CancelOrder(addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleRoot [32]byte) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.CancelOrder(&_Niftyconnect0.TransactOpts, addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleRoot)
}

// ChangeExchangeFeeRate is a paid mutator transaction binding the contract method 0xade0ccb2.
//
// Solidity: function changeExchangeFeeRate(uint256 newExchangeFeeRate) returns()
func (_Niftyconnect0 *Niftyconnect0Transactor) ChangeExchangeFeeRate(opts *bind.TransactOpts, newExchangeFeeRate *big.Int) (*types.Transaction, error) {
	return _Niftyconnect0.contract.Transact(opts, "changeExchangeFeeRate", newExchangeFeeRate)
}

// ChangeExchangeFeeRate is a paid mutator transaction binding the contract method 0xade0ccb2.
//
// Solidity: function changeExchangeFeeRate(uint256 newExchangeFeeRate) returns()
func (_Niftyconnect0 *Niftyconnect0Session) ChangeExchangeFeeRate(newExchangeFeeRate *big.Int) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.ChangeExchangeFeeRate(&_Niftyconnect0.TransactOpts, newExchangeFeeRate)
}

// ChangeExchangeFeeRate is a paid mutator transaction binding the contract method 0xade0ccb2.
//
// Solidity: function changeExchangeFeeRate(uint256 newExchangeFeeRate) returns()
func (_Niftyconnect0 *Niftyconnect0TransactorSession) ChangeExchangeFeeRate(newExchangeFeeRate *big.Int) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.ChangeExchangeFeeRate(&_Niftyconnect0.TransactOpts, newExchangeFeeRate)
}

// ChangeProtocolFeeRecipient is a paid mutator transaction binding the contract method 0x514f0330.
//
// Solidity: function changeProtocolFeeRecipient(address newProtocolFeeRecipient) returns()
func (_Niftyconnect0 *Niftyconnect0Transactor) ChangeProtocolFeeRecipient(opts *bind.TransactOpts, newProtocolFeeRecipient common.Address) (*types.Transaction, error) {
	return _Niftyconnect0.contract.Transact(opts, "changeProtocolFeeRecipient", newProtocolFeeRecipient)
}

// ChangeProtocolFeeRecipient is a paid mutator transaction binding the contract method 0x514f0330.
//
// Solidity: function changeProtocolFeeRecipient(address newProtocolFeeRecipient) returns()
func (_Niftyconnect0 *Niftyconnect0Session) ChangeProtocolFeeRecipient(newProtocolFeeRecipient common.Address) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.ChangeProtocolFeeRecipient(&_Niftyconnect0.TransactOpts, newProtocolFeeRecipient)
}

// ChangeProtocolFeeRecipient is a paid mutator transaction binding the contract method 0x514f0330.
//
// Solidity: function changeProtocolFeeRecipient(address newProtocolFeeRecipient) returns()
func (_Niftyconnect0 *Niftyconnect0TransactorSession) ChangeProtocolFeeRecipient(newProtocolFeeRecipient common.Address) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.ChangeProtocolFeeRecipient(&_Niftyconnect0.TransactOpts, newProtocolFeeRecipient)
}

// ChangeTakerRelayerFeeShare is a paid mutator transaction binding the contract method 0x3d1cf526.
//
// Solidity: function changeTakerRelayerFeeShare(uint256 newTakerRelayerFeeShare, uint256 newMakerRelayerFeeShare, uint256 newProtocolFeeShare) returns()
func (_Niftyconnect0 *Niftyconnect0Transactor) ChangeTakerRelayerFeeShare(opts *bind.TransactOpts, newTakerRelayerFeeShare *big.Int, newMakerRelayerFeeShare *big.Int, newProtocolFeeShare *big.Int) (*types.Transaction, error) {
	return _Niftyconnect0.contract.Transact(opts, "changeTakerRelayerFeeShare", newTakerRelayerFeeShare, newMakerRelayerFeeShare, newProtocolFeeShare)
}

// ChangeTakerRelayerFeeShare is a paid mutator transaction binding the contract method 0x3d1cf526.
//
// Solidity: function changeTakerRelayerFeeShare(uint256 newTakerRelayerFeeShare, uint256 newMakerRelayerFeeShare, uint256 newProtocolFeeShare) returns()
func (_Niftyconnect0 *Niftyconnect0Session) ChangeTakerRelayerFeeShare(newTakerRelayerFeeShare *big.Int, newMakerRelayerFeeShare *big.Int, newProtocolFeeShare *big.Int) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.ChangeTakerRelayerFeeShare(&_Niftyconnect0.TransactOpts, newTakerRelayerFeeShare, newMakerRelayerFeeShare, newProtocolFeeShare)
}

// ChangeTakerRelayerFeeShare is a paid mutator transaction binding the contract method 0x3d1cf526.
//
// Solidity: function changeTakerRelayerFeeShare(uint256 newTakerRelayerFeeShare, uint256 newMakerRelayerFeeShare, uint256 newProtocolFeeShare) returns()
func (_Niftyconnect0 *Niftyconnect0TransactorSession) ChangeTakerRelayerFeeShare(newTakerRelayerFeeShare *big.Int, newMakerRelayerFeeShare *big.Int, newProtocolFeeShare *big.Int) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.ChangeTakerRelayerFeeShare(&_Niftyconnect0.TransactOpts, newTakerRelayerFeeShare, newMakerRelayerFeeShare, newProtocolFeeShare)
}

// IncrementNonce is a paid mutator transaction binding the contract method 0x627cdcb9.
//
// Solidity: function incrementNonce() returns()
func (_Niftyconnect0 *Niftyconnect0Transactor) IncrementNonce(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Niftyconnect0.contract.Transact(opts, "incrementNonce")
}

// IncrementNonce is a paid mutator transaction binding the contract method 0x627cdcb9.
//
// Solidity: function incrementNonce() returns()
func (_Niftyconnect0 *Niftyconnect0Session) IncrementNonce() (*types.Transaction, error) {
	return _Niftyconnect0.Contract.IncrementNonce(&_Niftyconnect0.TransactOpts)
}

// IncrementNonce is a paid mutator transaction binding the contract method 0x627cdcb9.
//
// Solidity: function incrementNonce() returns()
func (_Niftyconnect0 *Niftyconnect0TransactorSession) IncrementNonce() (*types.Transaction, error) {
	return _Niftyconnect0.Contract.IncrementNonce(&_Niftyconnect0.TransactOpts)
}

// MakeOrder is a paid mutator transaction binding the contract method 0x97cea71b.
//
// Solidity: function makeOrder_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32[2] merkleData) returns()
func (_Niftyconnect0 *Niftyconnect0Transactor) MakeOrder(opts *bind.TransactOpts, addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleData [2][32]byte) (*types.Transaction, error) {
	return _Niftyconnect0.contract.Transact(opts, "makeOrder_", addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleData)
}

// MakeOrder is a paid mutator transaction binding the contract method 0x97cea71b.
//
// Solidity: function makeOrder_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32[2] merkleData) returns()
func (_Niftyconnect0 *Niftyconnect0Session) MakeOrder(addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleData [2][32]byte) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.MakeOrder(&_Niftyconnect0.TransactOpts, addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleData)
}

// MakeOrder is a paid mutator transaction binding the contract method 0x97cea71b.
//
// Solidity: function makeOrder_(address[9] addrs, uint256[9] uints, uint8 side, uint8 saleKind, bytes replacementPattern, bytes staticExtradata, bytes32[2] merkleData) returns()
func (_Niftyconnect0 *Niftyconnect0TransactorSession) MakeOrder(addrs [9]common.Address, uints [9]*big.Int, side uint8, saleKind uint8, replacementPattern []byte, staticExtradata []byte, merkleData [2][32]byte) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.MakeOrder(&_Niftyconnect0.TransactOpts, addrs, uints, side, saleKind, replacementPattern, staticExtradata, merkleData)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Niftyconnect0 *Niftyconnect0Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Niftyconnect0.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Niftyconnect0 *Niftyconnect0Session) RenounceOwnership() (*types.Transaction, error) {
	return _Niftyconnect0.Contract.RenounceOwnership(&_Niftyconnect0.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Niftyconnect0 *Niftyconnect0TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Niftyconnect0.Contract.RenounceOwnership(&_Niftyconnect0.TransactOpts)
}

// SetPendingGovernor is a paid mutator transaction binding the contract method 0xf235757f.
//
// Solidity: function setPendingGovernor(address pendingGovernor_) returns()
func (_Niftyconnect0 *Niftyconnect0Transactor) SetPendingGovernor(opts *bind.TransactOpts, pendingGovernor_ common.Address) (*types.Transaction, error) {
	return _Niftyconnect0.contract.Transact(opts, "setPendingGovernor", pendingGovernor_)
}

// SetPendingGovernor is a paid mutator transaction binding the contract method 0xf235757f.
//
// Solidity: function setPendingGovernor(address pendingGovernor_) returns()
func (_Niftyconnect0 *Niftyconnect0Session) SetPendingGovernor(pendingGovernor_ common.Address) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.SetPendingGovernor(&_Niftyconnect0.TransactOpts, pendingGovernor_)
}

// SetPendingGovernor is a paid mutator transaction binding the contract method 0xf235757f.
//
// Solidity: function setPendingGovernor(address pendingGovernor_) returns()
func (_Niftyconnect0 *Niftyconnect0TransactorSession) SetPendingGovernor(pendingGovernor_ common.Address) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.SetPendingGovernor(&_Niftyconnect0.TransactOpts, pendingGovernor_)
}

// TakeOrder is a paid mutator transaction binding the contract method 0x7da26f55.
//
// Solidity: function takeOrder_(address[16] addrs, uint256[12] uints, uint8[4] sidesKinds, bytes calldataBuy, bytes calldataSell, bytes replacementPatternBuy, bytes replacementPatternSell, bytes staticExtradataBuy, bytes staticExtradataSell, bytes32 rssMetadata) payable returns()
func (_Niftyconnect0 *Niftyconnect0Transactor) TakeOrder(opts *bind.TransactOpts, addrs [16]common.Address, uints [12]*big.Int, sidesKinds [4]uint8, calldataBuy []byte, calldataSell []byte, replacementPatternBuy []byte, replacementPatternSell []byte, staticExtradataBuy []byte, staticExtradataSell []byte, rssMetadata [32]byte) (*types.Transaction, error) {
	return _Niftyconnect0.contract.Transact(opts, "takeOrder_", addrs, uints, sidesKinds, calldataBuy, calldataSell, replacementPatternBuy, replacementPatternSell, staticExtradataBuy, staticExtradataSell, rssMetadata)
}

// TakeOrder is a paid mutator transaction binding the contract method 0x7da26f55.
//
// Solidity: function takeOrder_(address[16] addrs, uint256[12] uints, uint8[4] sidesKinds, bytes calldataBuy, bytes calldataSell, bytes replacementPatternBuy, bytes replacementPatternSell, bytes staticExtradataBuy, bytes staticExtradataSell, bytes32 rssMetadata) payable returns()
func (_Niftyconnect0 *Niftyconnect0Session) TakeOrder(addrs [16]common.Address, uints [12]*big.Int, sidesKinds [4]uint8, calldataBuy []byte, calldataSell []byte, replacementPatternBuy []byte, replacementPatternSell []byte, staticExtradataBuy []byte, staticExtradataSell []byte, rssMetadata [32]byte) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.TakeOrder(&_Niftyconnect0.TransactOpts, addrs, uints, sidesKinds, calldataBuy, calldataSell, replacementPatternBuy, replacementPatternSell, staticExtradataBuy, staticExtradataSell, rssMetadata)
}

// TakeOrder is a paid mutator transaction binding the contract method 0x7da26f55.
//
// Solidity: function takeOrder_(address[16] addrs, uint256[12] uints, uint8[4] sidesKinds, bytes calldataBuy, bytes calldataSell, bytes replacementPatternBuy, bytes replacementPatternSell, bytes staticExtradataBuy, bytes staticExtradataSell, bytes32 rssMetadata) payable returns()
func (_Niftyconnect0 *Niftyconnect0TransactorSession) TakeOrder(addrs [16]common.Address, uints [12]*big.Int, sidesKinds [4]uint8, calldataBuy []byte, calldataSell []byte, replacementPatternBuy []byte, replacementPatternSell []byte, staticExtradataBuy []byte, staticExtradataSell []byte, rssMetadata [32]byte) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.TakeOrder(&_Niftyconnect0.TransactOpts, addrs, uints, sidesKinds, calldataBuy, calldataSell, replacementPatternBuy, replacementPatternSell, staticExtradataBuy, staticExtradataSell, rssMetadata)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Niftyconnect0 *Niftyconnect0Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Niftyconnect0.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Niftyconnect0 *Niftyconnect0Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.TransferOwnership(&_Niftyconnect0.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Niftyconnect0 *Niftyconnect0TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Niftyconnect0.Contract.TransferOwnership(&_Niftyconnect0.TransactOpts, newOwner)
}

// Niftyconnect0GovernanceTransferredIterator is returned from FilterGovernanceTransferred and is used to iterate over the raw logs and unpacked data for GovernanceTransferred events raised by the Niftyconnect0 contract.
type Niftyconnect0GovernanceTransferredIterator struct {
	Event *Niftyconnect0GovernanceTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Niftyconnect0GovernanceTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Niftyconnect0GovernanceTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Niftyconnect0GovernanceTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Niftyconnect0GovernanceTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Niftyconnect0GovernanceTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Niftyconnect0GovernanceTransferred represents a GovernanceTransferred event raised by the Niftyconnect0 contract.
type Niftyconnect0GovernanceTransferred struct {
	PreviousGovernor common.Address
	NewGovernor      common.Address
	Raw              types.Log // Blockchain specific contextual infos
}

// FilterGovernanceTransferred is a free log retrieval operation binding the contract event 0x5f56bee8cffbe9a78652a74a60705edede02af10b0bbb888ca44b79a0d42ce80.
//
// Solidity: event GovernanceTransferred(address indexed previousGovernor, address indexed newGovernor)
func (_Niftyconnect0 *Niftyconnect0Filterer) FilterGovernanceTransferred(opts *bind.FilterOpts, previousGovernor []common.Address, newGovernor []common.Address) (*Niftyconnect0GovernanceTransferredIterator, error) {

	var previousGovernorRule []interface{}
	for _, previousGovernorItem := range previousGovernor {
		previousGovernorRule = append(previousGovernorRule, previousGovernorItem)
	}
	var newGovernorRule []interface{}
	for _, newGovernorItem := range newGovernor {
		newGovernorRule = append(newGovernorRule, newGovernorItem)
	}

	logs, sub, err := _Niftyconnect0.contract.FilterLogs(opts, "GovernanceTransferred", previousGovernorRule, newGovernorRule)
	if err != nil {
		return nil, err
	}
	return &Niftyconnect0GovernanceTransferredIterator{contract: _Niftyconnect0.contract, event: "GovernanceTransferred", logs: logs, sub: sub}, nil
}

// WatchGovernanceTransferred is a free log subscription operation binding the contract event 0x5f56bee8cffbe9a78652a74a60705edede02af10b0bbb888ca44b79a0d42ce80.
//
// Solidity: event GovernanceTransferred(address indexed previousGovernor, address indexed newGovernor)
func (_Niftyconnect0 *Niftyconnect0Filterer) WatchGovernanceTransferred(opts *bind.WatchOpts, sink chan<- *Niftyconnect0GovernanceTransferred, previousGovernor []common.Address, newGovernor []common.Address) (event.Subscription, error) {

	var previousGovernorRule []interface{}
	for _, previousGovernorItem := range previousGovernor {
		previousGovernorRule = append(previousGovernorRule, previousGovernorItem)
	}
	var newGovernorRule []interface{}
	for _, newGovernorItem := range newGovernor {
		newGovernorRule = append(newGovernorRule, newGovernorItem)
	}

	logs, sub, err := _Niftyconnect0.contract.WatchLogs(opts, "GovernanceTransferred", previousGovernorRule, newGovernorRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Niftyconnect0GovernanceTransferred)
				if err := _Niftyconnect0.contract.UnpackLog(event, "GovernanceTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseGovernanceTransferred is a log parse operation binding the contract event 0x5f56bee8cffbe9a78652a74a60705edede02af10b0bbb888ca44b79a0d42ce80.
//
// Solidity: event GovernanceTransferred(address indexed previousGovernor, address indexed newGovernor)
func (_Niftyconnect0 *Niftyconnect0Filterer) ParseGovernanceTransferred(log types.Log) (*Niftyconnect0GovernanceTransferred, error) {
	event := new(Niftyconnect0GovernanceTransferred)
	if err := _Niftyconnect0.contract.UnpackLog(event, "GovernanceTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Niftyconnect0NewPendingGovernorIterator is returned from FilterNewPendingGovernor and is used to iterate over the raw logs and unpacked data for NewPendingGovernor events raised by the Niftyconnect0 contract.
type Niftyconnect0NewPendingGovernorIterator struct {
	Event *Niftyconnect0NewPendingGovernor // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Niftyconnect0NewPendingGovernorIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Niftyconnect0NewPendingGovernor)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Niftyconnect0NewPendingGovernor)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Niftyconnect0NewPendingGovernorIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Niftyconnect0NewPendingGovernorIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Niftyconnect0NewPendingGovernor represents a NewPendingGovernor event raised by the Niftyconnect0 contract.
type Niftyconnect0NewPendingGovernor struct {
	NewPendingGovernor common.Address
	Raw                types.Log // Blockchain specific contextual infos
}

// FilterNewPendingGovernor is a free log retrieval operation binding the contract event 0xe6df4d3d01a6133dfdecd1b451c04ec286cb4b10e7235d2b27321b476216e6d7.
//
// Solidity: event NewPendingGovernor(address indexed newPendingGovernor)
func (_Niftyconnect0 *Niftyconnect0Filterer) FilterNewPendingGovernor(opts *bind.FilterOpts, newPendingGovernor []common.Address) (*Niftyconnect0NewPendingGovernorIterator, error) {

	var newPendingGovernorRule []interface{}
	for _, newPendingGovernorItem := range newPendingGovernor {
		newPendingGovernorRule = append(newPendingGovernorRule, newPendingGovernorItem)
	}

	logs, sub, err := _Niftyconnect0.contract.FilterLogs(opts, "NewPendingGovernor", newPendingGovernorRule)
	if err != nil {
		return nil, err
	}
	return &Niftyconnect0NewPendingGovernorIterator{contract: _Niftyconnect0.contract, event: "NewPendingGovernor", logs: logs, sub: sub}, nil
}

// WatchNewPendingGovernor is a free log subscription operation binding the contract event 0xe6df4d3d01a6133dfdecd1b451c04ec286cb4b10e7235d2b27321b476216e6d7.
//
// Solidity: event NewPendingGovernor(address indexed newPendingGovernor)
func (_Niftyconnect0 *Niftyconnect0Filterer) WatchNewPendingGovernor(opts *bind.WatchOpts, sink chan<- *Niftyconnect0NewPendingGovernor, newPendingGovernor []common.Address) (event.Subscription, error) {

	var newPendingGovernorRule []interface{}
	for _, newPendingGovernorItem := range newPendingGovernor {
		newPendingGovernorRule = append(newPendingGovernorRule, newPendingGovernorItem)
	}

	logs, sub, err := _Niftyconnect0.contract.WatchLogs(opts, "NewPendingGovernor", newPendingGovernorRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Niftyconnect0NewPendingGovernor)
				if err := _Niftyconnect0.contract.UnpackLog(event, "NewPendingGovernor", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewPendingGovernor is a log parse operation binding the contract event 0xe6df4d3d01a6133dfdecd1b451c04ec286cb4b10e7235d2b27321b476216e6d7.
//
// Solidity: event NewPendingGovernor(address indexed newPendingGovernor)
func (_Niftyconnect0 *Niftyconnect0Filterer) ParseNewPendingGovernor(log types.Log) (*Niftyconnect0NewPendingGovernor, error) {
	event := new(Niftyconnect0NewPendingGovernor)
	if err := _Niftyconnect0.contract.UnpackLog(event, "NewPendingGovernor", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Niftyconnect0NonceIncrementedIterator is returned from FilterNonceIncremented and is used to iterate over the raw logs and unpacked data for NonceIncremented events raised by the Niftyconnect0 contract.
type Niftyconnect0NonceIncrementedIterator struct {
	Event *Niftyconnect0NonceIncremented // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Niftyconnect0NonceIncrementedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Niftyconnect0NonceIncremented)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Niftyconnect0NonceIncremented)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Niftyconnect0NonceIncrementedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Niftyconnect0NonceIncrementedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Niftyconnect0NonceIncremented represents a NonceIncremented event raised by the Niftyconnect0 contract.
type Niftyconnect0NonceIncremented struct {
	Maker    common.Address
	NewNonce *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterNonceIncremented is a free log retrieval operation binding the contract event 0xa82a649bbd060c9099cd7b7326e2b0dc9e9af0836480e0f849dc9eaa79710b3b.
//
// Solidity: event NonceIncremented(address indexed maker, uint256 newNonce)
func (_Niftyconnect0 *Niftyconnect0Filterer) FilterNonceIncremented(opts *bind.FilterOpts, maker []common.Address) (*Niftyconnect0NonceIncrementedIterator, error) {

	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}

	logs, sub, err := _Niftyconnect0.contract.FilterLogs(opts, "NonceIncremented", makerRule)
	if err != nil {
		return nil, err
	}
	return &Niftyconnect0NonceIncrementedIterator{contract: _Niftyconnect0.contract, event: "NonceIncremented", logs: logs, sub: sub}, nil
}

// WatchNonceIncremented is a free log subscription operation binding the contract event 0xa82a649bbd060c9099cd7b7326e2b0dc9e9af0836480e0f849dc9eaa79710b3b.
//
// Solidity: event NonceIncremented(address indexed maker, uint256 newNonce)
func (_Niftyconnect0 *Niftyconnect0Filterer) WatchNonceIncremented(opts *bind.WatchOpts, sink chan<- *Niftyconnect0NonceIncremented, maker []common.Address) (event.Subscription, error) {

	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}

	logs, sub, err := _Niftyconnect0.contract.WatchLogs(opts, "NonceIncremented", makerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Niftyconnect0NonceIncremented)
				if err := _Niftyconnect0.contract.UnpackLog(event, "NonceIncremented", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNonceIncremented is a log parse operation binding the contract event 0xa82a649bbd060c9099cd7b7326e2b0dc9e9af0836480e0f849dc9eaa79710b3b.
//
// Solidity: event NonceIncremented(address indexed maker, uint256 newNonce)
func (_Niftyconnect0 *Niftyconnect0Filterer) ParseNonceIncremented(log types.Log) (*Niftyconnect0NonceIncremented, error) {
	event := new(Niftyconnect0NonceIncremented)
	if err := _Niftyconnect0.contract.UnpackLog(event, "NonceIncremented", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Niftyconnect0OrderApprovedPartOneIterator is returned from FilterOrderApprovedPartOne and is used to iterate over the raw logs and unpacked data for OrderApprovedPartOne events raised by the Niftyconnect0 contract.
type Niftyconnect0OrderApprovedPartOneIterator struct {
	Event *Niftyconnect0OrderApprovedPartOne // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Niftyconnect0OrderApprovedPartOneIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Niftyconnect0OrderApprovedPartOne)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Niftyconnect0OrderApprovedPartOne)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Niftyconnect0OrderApprovedPartOneIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Niftyconnect0OrderApprovedPartOneIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Niftyconnect0OrderApprovedPartOne represents a OrderApprovedPartOne event raised by the Niftyconnect0 contract.
type Niftyconnect0OrderApprovedPartOne struct {
	Hash                     [32]byte
	Exchange                 common.Address
	Maker                    common.Address
	Taker                    common.Address
	MakerRelayerFeeRecipient common.Address
	Side                     uint8
	SaleKind                 uint8
	NftAddress               common.Address
	TokenId                  *big.Int
	IpfsHash                 [32]byte
	Raw                      types.Log // Blockchain specific contextual infos
}

// FilterOrderApprovedPartOne is a free log retrieval operation binding the contract event 0xbfc991b64000533072b5f27ccd5e8628fea28ae33286778a627005b3156c6fe1.
//
// Solidity: event OrderApprovedPartOne(bytes32 indexed hash, address exchange, address indexed maker, address taker, address indexed makerRelayerFeeRecipient, uint8 side, uint8 saleKind, address nftAddress, uint256 tokenId, bytes32 ipfsHash)
func (_Niftyconnect0 *Niftyconnect0Filterer) FilterOrderApprovedPartOne(opts *bind.FilterOpts, hash [][32]byte, maker []common.Address, makerRelayerFeeRecipient []common.Address) (*Niftyconnect0OrderApprovedPartOneIterator, error) {

	var hashRule []interface{}
	for _, hashItem := range hash {
		hashRule = append(hashRule, hashItem)
	}

	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}

	var makerRelayerFeeRecipientRule []interface{}
	for _, makerRelayerFeeRecipientItem := range makerRelayerFeeRecipient {
		makerRelayerFeeRecipientRule = append(makerRelayerFeeRecipientRule, makerRelayerFeeRecipientItem)
	}

	logs, sub, err := _Niftyconnect0.contract.FilterLogs(opts, "OrderApprovedPartOne", hashRule, makerRule, makerRelayerFeeRecipientRule)
	if err != nil {
		return nil, err
	}
	return &Niftyconnect0OrderApprovedPartOneIterator{contract: _Niftyconnect0.contract, event: "OrderApprovedPartOne", logs: logs, sub: sub}, nil
}

// WatchOrderApprovedPartOne is a free log subscription operation binding the contract event 0xbfc991b64000533072b5f27ccd5e8628fea28ae33286778a627005b3156c6fe1.
//
// Solidity: event OrderApprovedPartOne(bytes32 indexed hash, address exchange, address indexed maker, address taker, address indexed makerRelayerFeeRecipient, uint8 side, uint8 saleKind, address nftAddress, uint256 tokenId, bytes32 ipfsHash)
func (_Niftyconnect0 *Niftyconnect0Filterer) WatchOrderApprovedPartOne(opts *bind.WatchOpts, sink chan<- *Niftyconnect0OrderApprovedPartOne, hash [][32]byte, maker []common.Address, makerRelayerFeeRecipient []common.Address) (event.Subscription, error) {

	var hashRule []interface{}
	for _, hashItem := range hash {
		hashRule = append(hashRule, hashItem)
	}

	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}

	var makerRelayerFeeRecipientRule []interface{}
	for _, makerRelayerFeeRecipientItem := range makerRelayerFeeRecipient {
		makerRelayerFeeRecipientRule = append(makerRelayerFeeRecipientRule, makerRelayerFeeRecipientItem)
	}

	logs, sub, err := _Niftyconnect0.contract.WatchLogs(opts, "OrderApprovedPartOne", hashRule, makerRule, makerRelayerFeeRecipientRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Niftyconnect0OrderApprovedPartOne)
				if err := _Niftyconnect0.contract.UnpackLog(event, "OrderApprovedPartOne", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOrderApprovedPartOne is a log parse operation binding the contract event 0xbfc991b64000533072b5f27ccd5e8628fea28ae33286778a627005b3156c6fe1.
//
// Solidity: event OrderApprovedPartOne(bytes32 indexed hash, address exchange, address indexed maker, address taker, address indexed makerRelayerFeeRecipient, uint8 side, uint8 saleKind, address nftAddress, uint256 tokenId, bytes32 ipfsHash)
func (_Niftyconnect0 *Niftyconnect0Filterer) ParseOrderApprovedPartOne(log types.Log) (*Niftyconnect0OrderApprovedPartOne, error) {
	event := new(Niftyconnect0OrderApprovedPartOne)
	if err := _Niftyconnect0.contract.UnpackLog(event, "OrderApprovedPartOne", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Niftyconnect0OrderApprovedPartTwoIterator is returned from FilterOrderApprovedPartTwo and is used to iterate over the raw logs and unpacked data for OrderApprovedPartTwo events raised by the Niftyconnect0 contract.
type Niftyconnect0OrderApprovedPartTwoIterator struct {
	Event *Niftyconnect0OrderApprovedPartTwo // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Niftyconnect0OrderApprovedPartTwoIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Niftyconnect0OrderApprovedPartTwo)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Niftyconnect0OrderApprovedPartTwo)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Niftyconnect0OrderApprovedPartTwoIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Niftyconnect0OrderApprovedPartTwoIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Niftyconnect0OrderApprovedPartTwo represents a OrderApprovedPartTwo event raised by the Niftyconnect0 contract.
type Niftyconnect0OrderApprovedPartTwo struct {
	Hash               [32]byte
	Calldata           []byte
	ReplacementPattern []byte
	StaticTarget       common.Address
	StaticExtradata    []byte
	PaymentToken       common.Address
	BasePrice          *big.Int
	Extra              *big.Int
	ListingTime        *big.Int
	ExpirationTime     *big.Int
	Salt               *big.Int
	Raw                types.Log // Blockchain specific contextual infos
}

// FilterOrderApprovedPartTwo is a free log retrieval operation binding the contract event 0xb7c210e6374e28618aff2db2406f01343e302b15476278b7795869bccc51f979.
//
// Solidity: event OrderApprovedPartTwo(bytes32 indexed hash, bytes calldata, bytes replacementPattern, address staticTarget, bytes staticExtradata, address paymentToken, uint256 basePrice, uint256 extra, uint256 listingTime, uint256 expirationTime, uint256 salt)
func (_Niftyconnect0 *Niftyconnect0Filterer) FilterOrderApprovedPartTwo(opts *bind.FilterOpts, hash [][32]byte) (*Niftyconnect0OrderApprovedPartTwoIterator, error) {

	var hashRule []interface{}
	for _, hashItem := range hash {
		hashRule = append(hashRule, hashItem)
	}

	logs, sub, err := _Niftyconnect0.contract.FilterLogs(opts, "OrderApprovedPartTwo", hashRule)
	if err != nil {
		return nil, err
	}
	return &Niftyconnect0OrderApprovedPartTwoIterator{contract: _Niftyconnect0.contract, event: "OrderApprovedPartTwo", logs: logs, sub: sub}, nil
}

// WatchOrderApprovedPartTwo is a free log subscription operation binding the contract event 0xb7c210e6374e28618aff2db2406f01343e302b15476278b7795869bccc51f979.
//
// Solidity: event OrderApprovedPartTwo(bytes32 indexed hash, bytes calldata, bytes replacementPattern, address staticTarget, bytes staticExtradata, address paymentToken, uint256 basePrice, uint256 extra, uint256 listingTime, uint256 expirationTime, uint256 salt)
func (_Niftyconnect0 *Niftyconnect0Filterer) WatchOrderApprovedPartTwo(opts *bind.WatchOpts, sink chan<- *Niftyconnect0OrderApprovedPartTwo, hash [][32]byte) (event.Subscription, error) {

	var hashRule []interface{}
	for _, hashItem := range hash {
		hashRule = append(hashRule, hashItem)
	}

	logs, sub, err := _Niftyconnect0.contract.WatchLogs(opts, "OrderApprovedPartTwo", hashRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Niftyconnect0OrderApprovedPartTwo)
				if err := _Niftyconnect0.contract.UnpackLog(event, "OrderApprovedPartTwo", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOrderApprovedPartTwo is a log parse operation binding the contract event 0xb7c210e6374e28618aff2db2406f01343e302b15476278b7795869bccc51f979.
//
// Solidity: event OrderApprovedPartTwo(bytes32 indexed hash, bytes calldata, bytes replacementPattern, address staticTarget, bytes staticExtradata, address paymentToken, uint256 basePrice, uint256 extra, uint256 listingTime, uint256 expirationTime, uint256 salt)
func (_Niftyconnect0 *Niftyconnect0Filterer) ParseOrderApprovedPartTwo(log types.Log) (*Niftyconnect0OrderApprovedPartTwo, error) {
	event := new(Niftyconnect0OrderApprovedPartTwo)
	if err := _Niftyconnect0.contract.UnpackLog(event, "OrderApprovedPartTwo", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Niftyconnect0OrderCancelledIterator is returned from FilterOrderCancelled and is used to iterate over the raw logs and unpacked data for OrderCancelled events raised by the Niftyconnect0 contract.
type Niftyconnect0OrderCancelledIterator struct {
	Event *Niftyconnect0OrderCancelled // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Niftyconnect0OrderCancelledIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Niftyconnect0OrderCancelled)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Niftyconnect0OrderCancelled)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Niftyconnect0OrderCancelledIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Niftyconnect0OrderCancelledIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Niftyconnect0OrderCancelled represents a OrderCancelled event raised by the Niftyconnect0 contract.
type Niftyconnect0OrderCancelled struct {
	Hash [32]byte
	Raw  types.Log // Blockchain specific contextual infos
}

// FilterOrderCancelled is a free log retrieval operation binding the contract event 0x5152abf959f6564662358c2e52b702259b78bac5ee7842a0f01937e670efcc7d.
//
// Solidity: event OrderCancelled(bytes32 indexed hash)
func (_Niftyconnect0 *Niftyconnect0Filterer) FilterOrderCancelled(opts *bind.FilterOpts, hash [][32]byte) (*Niftyconnect0OrderCancelledIterator, error) {

	var hashRule []interface{}
	for _, hashItem := range hash {
		hashRule = append(hashRule, hashItem)
	}

	logs, sub, err := _Niftyconnect0.contract.FilterLogs(opts, "OrderCancelled", hashRule)
	if err != nil {
		return nil, err
	}
	return &Niftyconnect0OrderCancelledIterator{contract: _Niftyconnect0.contract, event: "OrderCancelled", logs: logs, sub: sub}, nil
}

// WatchOrderCancelled is a free log subscription operation binding the contract event 0x5152abf959f6564662358c2e52b702259b78bac5ee7842a0f01937e670efcc7d.
//
// Solidity: event OrderCancelled(bytes32 indexed hash)
func (_Niftyconnect0 *Niftyconnect0Filterer) WatchOrderCancelled(opts *bind.WatchOpts, sink chan<- *Niftyconnect0OrderCancelled, hash [][32]byte) (event.Subscription, error) {

	var hashRule []interface{}
	for _, hashItem := range hash {
		hashRule = append(hashRule, hashItem)
	}

	logs, sub, err := _Niftyconnect0.contract.WatchLogs(opts, "OrderCancelled", hashRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Niftyconnect0OrderCancelled)
				if err := _Niftyconnect0.contract.UnpackLog(event, "OrderCancelled", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOrderCancelled is a log parse operation binding the contract event 0x5152abf959f6564662358c2e52b702259b78bac5ee7842a0f01937e670efcc7d.
//
// Solidity: event OrderCancelled(bytes32 indexed hash)
func (_Niftyconnect0 *Niftyconnect0Filterer) ParseOrderCancelled(log types.Log) (*Niftyconnect0OrderCancelled, error) {
	event := new(Niftyconnect0OrderCancelled)
	if err := _Niftyconnect0.contract.UnpackLog(event, "OrderCancelled", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Niftyconnect0OrdersMatchedIterator is returned from FilterOrdersMatched and is used to iterate over the raw logs and unpacked data for OrdersMatched events raised by the Niftyconnect0 contract.
type Niftyconnect0OrdersMatchedIterator struct {
	Event *Niftyconnect0OrdersMatched // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Niftyconnect0OrdersMatchedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Niftyconnect0OrdersMatched)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Niftyconnect0OrdersMatched)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Niftyconnect0OrdersMatchedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Niftyconnect0OrdersMatchedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Niftyconnect0OrdersMatched represents a OrdersMatched event raised by the Niftyconnect0 contract.
type Niftyconnect0OrdersMatched struct {
	BuyHash                  [32]byte
	SellHash                 [32]byte
	Maker                    common.Address
	Taker                    common.Address
	MakerRelayerFeeRecipient common.Address
	TakerRelayerFeeRecipient common.Address
	Price                    *big.Int
	Metadata                 [32]byte
	Raw                      types.Log // Blockchain specific contextual infos
}

// FilterOrdersMatched is a free log retrieval operation binding the contract event 0x5e89bc5bf129d9595ae14697a763c17e6acd67579b9f1f4fa548f57ec762a057.
//
// Solidity: event OrdersMatched(bytes32 buyHash, bytes32 sellHash, address indexed maker, address indexed taker, address makerRelayerFeeRecipient, address takerRelayerFeeRecipient, uint256 price, bytes32 indexed metadata)
func (_Niftyconnect0 *Niftyconnect0Filterer) FilterOrdersMatched(opts *bind.FilterOpts, maker []common.Address, taker []common.Address, metadata [][32]byte) (*Niftyconnect0OrdersMatchedIterator, error) {

	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}
	var takerRule []interface{}
	for _, takerItem := range taker {
		takerRule = append(takerRule, takerItem)
	}

	var metadataRule []interface{}
	for _, metadataItem := range metadata {
		metadataRule = append(metadataRule, metadataItem)
	}

	logs, sub, err := _Niftyconnect0.contract.FilterLogs(opts, "OrdersMatched", makerRule, takerRule, metadataRule)
	if err != nil {
		return nil, err
	}
	return &Niftyconnect0OrdersMatchedIterator{contract: _Niftyconnect0.contract, event: "OrdersMatched", logs: logs, sub: sub}, nil
}

// WatchOrdersMatched is a free log subscription operation binding the contract event 0x5e89bc5bf129d9595ae14697a763c17e6acd67579b9f1f4fa548f57ec762a057.
//
// Solidity: event OrdersMatched(bytes32 buyHash, bytes32 sellHash, address indexed maker, address indexed taker, address makerRelayerFeeRecipient, address takerRelayerFeeRecipient, uint256 price, bytes32 indexed metadata)
func (_Niftyconnect0 *Niftyconnect0Filterer) WatchOrdersMatched(opts *bind.WatchOpts, sink chan<- *Niftyconnect0OrdersMatched, maker []common.Address, taker []common.Address, metadata [][32]byte) (event.Subscription, error) {

	var makerRule []interface{}
	for _, makerItem := range maker {
		makerRule = append(makerRule, makerItem)
	}
	var takerRule []interface{}
	for _, takerItem := range taker {
		takerRule = append(takerRule, takerItem)
	}

	var metadataRule []interface{}
	for _, metadataItem := range metadata {
		metadataRule = append(metadataRule, metadataItem)
	}

	logs, sub, err := _Niftyconnect0.contract.WatchLogs(opts, "OrdersMatched", makerRule, takerRule, metadataRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Niftyconnect0OrdersMatched)
				if err := _Niftyconnect0.contract.UnpackLog(event, "OrdersMatched", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOrdersMatched is a log parse operation binding the contract event 0x5e89bc5bf129d9595ae14697a763c17e6acd67579b9f1f4fa548f57ec762a057.
//
// Solidity: event OrdersMatched(bytes32 buyHash, bytes32 sellHash, address indexed maker, address indexed taker, address makerRelayerFeeRecipient, address takerRelayerFeeRecipient, uint256 price, bytes32 indexed metadata)
func (_Niftyconnect0 *Niftyconnect0Filterer) ParseOrdersMatched(log types.Log) (*Niftyconnect0OrdersMatched, error) {
	event := new(Niftyconnect0OrdersMatched)
	if err := _Niftyconnect0.contract.UnpackLog(event, "OrdersMatched", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Niftyconnect0OwnershipRenouncedIterator is returned from FilterOwnershipRenounced and is used to iterate over the raw logs and unpacked data for OwnershipRenounced events raised by the Niftyconnect0 contract.
type Niftyconnect0OwnershipRenouncedIterator struct {
	Event *Niftyconnect0OwnershipRenounced // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Niftyconnect0OwnershipRenouncedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Niftyconnect0OwnershipRenounced)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Niftyconnect0OwnershipRenounced)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Niftyconnect0OwnershipRenouncedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Niftyconnect0OwnershipRenouncedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Niftyconnect0OwnershipRenounced represents a OwnershipRenounced event raised by the Niftyconnect0 contract.
type Niftyconnect0OwnershipRenounced struct {
	PreviousOwner common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipRenounced is a free log retrieval operation binding the contract event 0xf8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c64820.
//
// Solidity: event OwnershipRenounced(address indexed previousOwner)
func (_Niftyconnect0 *Niftyconnect0Filterer) FilterOwnershipRenounced(opts *bind.FilterOpts, previousOwner []common.Address) (*Niftyconnect0OwnershipRenouncedIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}

	logs, sub, err := _Niftyconnect0.contract.FilterLogs(opts, "OwnershipRenounced", previousOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Niftyconnect0OwnershipRenouncedIterator{contract: _Niftyconnect0.contract, event: "OwnershipRenounced", logs: logs, sub: sub}, nil
}

// WatchOwnershipRenounced is a free log subscription operation binding the contract event 0xf8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c64820.
//
// Solidity: event OwnershipRenounced(address indexed previousOwner)
func (_Niftyconnect0 *Niftyconnect0Filterer) WatchOwnershipRenounced(opts *bind.WatchOpts, sink chan<- *Niftyconnect0OwnershipRenounced, previousOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}

	logs, sub, err := _Niftyconnect0.contract.WatchLogs(opts, "OwnershipRenounced", previousOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Niftyconnect0OwnershipRenounced)
				if err := _Niftyconnect0.contract.UnpackLog(event, "OwnershipRenounced", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipRenounced is a log parse operation binding the contract event 0xf8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c64820.
//
// Solidity: event OwnershipRenounced(address indexed previousOwner)
func (_Niftyconnect0 *Niftyconnect0Filterer) ParseOwnershipRenounced(log types.Log) (*Niftyconnect0OwnershipRenounced, error) {
	event := new(Niftyconnect0OwnershipRenounced)
	if err := _Niftyconnect0.contract.UnpackLog(event, "OwnershipRenounced", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Niftyconnect0OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Niftyconnect0 contract.
type Niftyconnect0OwnershipTransferredIterator struct {
	Event *Niftyconnect0OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Niftyconnect0OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Niftyconnect0OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Niftyconnect0OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Niftyconnect0OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Niftyconnect0OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Niftyconnect0OwnershipTransferred represents a OwnershipTransferred event raised by the Niftyconnect0 contract.
type Niftyconnect0OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Niftyconnect0 *Niftyconnect0Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Niftyconnect0OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Niftyconnect0.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Niftyconnect0OwnershipTransferredIterator{contract: _Niftyconnect0.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Niftyconnect0 *Niftyconnect0Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Niftyconnect0OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Niftyconnect0.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Niftyconnect0OwnershipTransferred)
				if err := _Niftyconnect0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Niftyconnect0 *Niftyconnect0Filterer) ParseOwnershipTransferred(log types.Log) (*Niftyconnect0OwnershipTransferred, error) {
	event := new(Niftyconnect0OwnershipTransferred)
	if err := _Niftyconnect0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
