// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package cryptokitties_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Cryptokitties0MetaData contains all meta data concerning the Cryptokitties0 contract.
var Cryptokitties0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_kitties\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_saleAuction\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[],\"name\":\"Pause\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[],\"name\":\"Unpause\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_kittyId\",\"type\":\"uint256\"}],\"name\":\"bid\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_kittyId\",\"type\":\"uint256\"},{\"internalType\":\"address[]\",\"name\":\"_accountsToWarmUp\",\"type\":\"address[]\"}],\"name\":\"bidWithSpecificWarmups\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"addresspayable\",\"name\":\"_to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"_data\",\"type\":\"bytes\"}],\"name\":\"call\",\"outputs\":[{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"pause\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"paused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_kittyId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_recipient\",\"type\":\"address\"}],\"name\":\"rescueLostKitty\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"unpause\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
}

// Cryptokitties0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Cryptokitties0MetaData.ABI instead.
var Cryptokitties0ABI = Cryptokitties0MetaData.ABI

// Cryptokitties0 is an auto generated Go binding around an Ethereum contract.
type Cryptokitties0 struct {
	Cryptokitties0Caller     // Read-only binding to the contract
	Cryptokitties0Transactor // Write-only binding to the contract
	Cryptokitties0Filterer   // Log filterer for contract events
}

// Cryptokitties0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Cryptokitties0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Cryptokitties0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Cryptokitties0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Cryptokitties0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Cryptokitties0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Cryptokitties0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Cryptokitties0Session struct {
	Contract     *Cryptokitties0   // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Cryptokitties0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Cryptokitties0CallerSession struct {
	Contract *Cryptokitties0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts         // Call options to use throughout this session
}

// Cryptokitties0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Cryptokitties0TransactorSession struct {
	Contract     *Cryptokitties0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts         // Transaction auth options to use throughout this session
}

// Cryptokitties0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Cryptokitties0Raw struct {
	Contract *Cryptokitties0 // Generic contract binding to access the raw methods on
}

// Cryptokitties0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Cryptokitties0CallerRaw struct {
	Contract *Cryptokitties0Caller // Generic read-only contract binding to access the raw methods on
}

// Cryptokitties0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Cryptokitties0TransactorRaw struct {
	Contract *Cryptokitties0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewCryptokitties0 creates a new instance of Cryptokitties0, bound to a specific deployed contract.
func NewCryptokitties0(address common.Address, backend bind.ContractBackend) (*Cryptokitties0, error) {
	contract, err := bindCryptokitties0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Cryptokitties0{Cryptokitties0Caller: Cryptokitties0Caller{contract: contract}, Cryptokitties0Transactor: Cryptokitties0Transactor{contract: contract}, Cryptokitties0Filterer: Cryptokitties0Filterer{contract: contract}}, nil
}

// NewCryptokitties0Caller creates a new read-only instance of Cryptokitties0, bound to a specific deployed contract.
func NewCryptokitties0Caller(address common.Address, caller bind.ContractCaller) (*Cryptokitties0Caller, error) {
	contract, err := bindCryptokitties0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Cryptokitties0Caller{contract: contract}, nil
}

// NewCryptokitties0Transactor creates a new write-only instance of Cryptokitties0, bound to a specific deployed contract.
func NewCryptokitties0Transactor(address common.Address, transactor bind.ContractTransactor) (*Cryptokitties0Transactor, error) {
	contract, err := bindCryptokitties0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Cryptokitties0Transactor{contract: contract}, nil
}

// NewCryptokitties0Filterer creates a new log filterer instance of Cryptokitties0, bound to a specific deployed contract.
func NewCryptokitties0Filterer(address common.Address, filterer bind.ContractFilterer) (*Cryptokitties0Filterer, error) {
	contract, err := bindCryptokitties0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Cryptokitties0Filterer{contract: contract}, nil
}

// bindCryptokitties0 binds a generic wrapper to an already deployed contract.
func bindCryptokitties0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Cryptokitties0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Cryptokitties0 *Cryptokitties0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Cryptokitties0.Contract.Cryptokitties0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Cryptokitties0 *Cryptokitties0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.Cryptokitties0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Cryptokitties0 *Cryptokitties0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.Cryptokitties0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Cryptokitties0 *Cryptokitties0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Cryptokitties0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Cryptokitties0 *Cryptokitties0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Cryptokitties0 *Cryptokitties0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.contract.Transact(opts, method, params...)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Cryptokitties0 *Cryptokitties0Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Cryptokitties0.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Cryptokitties0 *Cryptokitties0Session) Owner() (common.Address, error) {
	return _Cryptokitties0.Contract.Owner(&_Cryptokitties0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Cryptokitties0 *Cryptokitties0CallerSession) Owner() (common.Address, error) {
	return _Cryptokitties0.Contract.Owner(&_Cryptokitties0.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Cryptokitties0 *Cryptokitties0Caller) Paused(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Cryptokitties0.contract.Call(opts, &out, "paused")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Cryptokitties0 *Cryptokitties0Session) Paused() (bool, error) {
	return _Cryptokitties0.Contract.Paused(&_Cryptokitties0.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Cryptokitties0 *Cryptokitties0CallerSession) Paused() (bool, error) {
	return _Cryptokitties0.Contract.Paused(&_Cryptokitties0.CallOpts)
}

// Bid is a paid mutator transaction binding the contract method 0x454a2ab3.
//
// Solidity: function bid(uint256 _kittyId) payable returns()
func (_Cryptokitties0 *Cryptokitties0Transactor) Bid(opts *bind.TransactOpts, _kittyId *big.Int) (*types.Transaction, error) {
	return _Cryptokitties0.contract.Transact(opts, "bid", _kittyId)
}

// Bid is a paid mutator transaction binding the contract method 0x454a2ab3.
//
// Solidity: function bid(uint256 _kittyId) payable returns()
func (_Cryptokitties0 *Cryptokitties0Session) Bid(_kittyId *big.Int) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.Bid(&_Cryptokitties0.TransactOpts, _kittyId)
}

// Bid is a paid mutator transaction binding the contract method 0x454a2ab3.
//
// Solidity: function bid(uint256 _kittyId) payable returns()
func (_Cryptokitties0 *Cryptokitties0TransactorSession) Bid(_kittyId *big.Int) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.Bid(&_Cryptokitties0.TransactOpts, _kittyId)
}

// BidWithSpecificWarmups is a paid mutator transaction binding the contract method 0x162c720c.
//
// Solidity: function bidWithSpecificWarmups(uint256 _kittyId, address[] _accountsToWarmUp) payable returns()
func (_Cryptokitties0 *Cryptokitties0Transactor) BidWithSpecificWarmups(opts *bind.TransactOpts, _kittyId *big.Int, _accountsToWarmUp []common.Address) (*types.Transaction, error) {
	return _Cryptokitties0.contract.Transact(opts, "bidWithSpecificWarmups", _kittyId, _accountsToWarmUp)
}

// BidWithSpecificWarmups is a paid mutator transaction binding the contract method 0x162c720c.
//
// Solidity: function bidWithSpecificWarmups(uint256 _kittyId, address[] _accountsToWarmUp) payable returns()
func (_Cryptokitties0 *Cryptokitties0Session) BidWithSpecificWarmups(_kittyId *big.Int, _accountsToWarmUp []common.Address) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.BidWithSpecificWarmups(&_Cryptokitties0.TransactOpts, _kittyId, _accountsToWarmUp)
}

// BidWithSpecificWarmups is a paid mutator transaction binding the contract method 0x162c720c.
//
// Solidity: function bidWithSpecificWarmups(uint256 _kittyId, address[] _accountsToWarmUp) payable returns()
func (_Cryptokitties0 *Cryptokitties0TransactorSession) BidWithSpecificWarmups(_kittyId *big.Int, _accountsToWarmUp []common.Address) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.BidWithSpecificWarmups(&_Cryptokitties0.TransactOpts, _kittyId, _accountsToWarmUp)
}

// Call is a paid mutator transaction binding the contract method 0x6dbf2fa0.
//
// Solidity: function call(address _to, uint256 _value, bytes _data) payable returns(bytes)
func (_Cryptokitties0 *Cryptokitties0Transactor) Call(opts *bind.TransactOpts, _to common.Address, _value *big.Int, _data []byte) (*types.Transaction, error) {
	return _Cryptokitties0.contract.Transact(opts, "call", _to, _value, _data)
}

// Call is a paid mutator transaction binding the contract method 0x6dbf2fa0.
//
// Solidity: function call(address _to, uint256 _value, bytes _data) payable returns(bytes)
func (_Cryptokitties0 *Cryptokitties0Session) Call(_to common.Address, _value *big.Int, _data []byte) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.Call(&_Cryptokitties0.TransactOpts, _to, _value, _data)
}

// Call is a paid mutator transaction binding the contract method 0x6dbf2fa0.
//
// Solidity: function call(address _to, uint256 _value, bytes _data) payable returns(bytes)
func (_Cryptokitties0 *Cryptokitties0TransactorSession) Call(_to common.Address, _value *big.Int, _data []byte) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.Call(&_Cryptokitties0.TransactOpts, _to, _value, _data)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns(bool)
func (_Cryptokitties0 *Cryptokitties0Transactor) Pause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptokitties0.contract.Transact(opts, "pause")
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns(bool)
func (_Cryptokitties0 *Cryptokitties0Session) Pause() (*types.Transaction, error) {
	return _Cryptokitties0.Contract.Pause(&_Cryptokitties0.TransactOpts)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns(bool)
func (_Cryptokitties0 *Cryptokitties0TransactorSession) Pause() (*types.Transaction, error) {
	return _Cryptokitties0.Contract.Pause(&_Cryptokitties0.TransactOpts)
}

// RescueLostKitty is a paid mutator transaction binding the contract method 0x030c4741.
//
// Solidity: function rescueLostKitty(uint256 _kittyId, address _recipient) returns()
func (_Cryptokitties0 *Cryptokitties0Transactor) RescueLostKitty(opts *bind.TransactOpts, _kittyId *big.Int, _recipient common.Address) (*types.Transaction, error) {
	return _Cryptokitties0.contract.Transact(opts, "rescueLostKitty", _kittyId, _recipient)
}

// RescueLostKitty is a paid mutator transaction binding the contract method 0x030c4741.
//
// Solidity: function rescueLostKitty(uint256 _kittyId, address _recipient) returns()
func (_Cryptokitties0 *Cryptokitties0Session) RescueLostKitty(_kittyId *big.Int, _recipient common.Address) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.RescueLostKitty(&_Cryptokitties0.TransactOpts, _kittyId, _recipient)
}

// RescueLostKitty is a paid mutator transaction binding the contract method 0x030c4741.
//
// Solidity: function rescueLostKitty(uint256 _kittyId, address _recipient) returns()
func (_Cryptokitties0 *Cryptokitties0TransactorSession) RescueLostKitty(_kittyId *big.Int, _recipient common.Address) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.RescueLostKitty(&_Cryptokitties0.TransactOpts, _kittyId, _recipient)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Cryptokitties0 *Cryptokitties0Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Cryptokitties0.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Cryptokitties0 *Cryptokitties0Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.TransferOwnership(&_Cryptokitties0.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Cryptokitties0 *Cryptokitties0TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Cryptokitties0.Contract.TransferOwnership(&_Cryptokitties0.TransactOpts, newOwner)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns(bool)
func (_Cryptokitties0 *Cryptokitties0Transactor) Unpause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptokitties0.contract.Transact(opts, "unpause")
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns(bool)
func (_Cryptokitties0 *Cryptokitties0Session) Unpause() (*types.Transaction, error) {
	return _Cryptokitties0.Contract.Unpause(&_Cryptokitties0.TransactOpts)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns(bool)
func (_Cryptokitties0 *Cryptokitties0TransactorSession) Unpause() (*types.Transaction, error) {
	return _Cryptokitties0.Contract.Unpause(&_Cryptokitties0.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Cryptokitties0 *Cryptokitties0Transactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptokitties0.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Cryptokitties0 *Cryptokitties0Session) Receive() (*types.Transaction, error) {
	return _Cryptokitties0.Contract.Receive(&_Cryptokitties0.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Cryptokitties0 *Cryptokitties0TransactorSession) Receive() (*types.Transaction, error) {
	return _Cryptokitties0.Contract.Receive(&_Cryptokitties0.TransactOpts)
}

// Cryptokitties0PauseIterator is returned from FilterPause and is used to iterate over the raw logs and unpacked data for Pause events raised by the Cryptokitties0 contract.
type Cryptokitties0PauseIterator struct {
	Event *Cryptokitties0Pause // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptokitties0PauseIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptokitties0Pause)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptokitties0Pause)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptokitties0PauseIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptokitties0PauseIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptokitties0Pause represents a Pause event raised by the Cryptokitties0 contract.
type Cryptokitties0Pause struct {
	Raw types.Log // Blockchain specific contextual infos
}

// FilterPause is a free log retrieval operation binding the contract event 0x6985a02210a168e66602d3235cb6db0e70f92b3ba4d376a33c0f3d9434bff625.
//
// Solidity: event Pause()
func (_Cryptokitties0 *Cryptokitties0Filterer) FilterPause(opts *bind.FilterOpts) (*Cryptokitties0PauseIterator, error) {

	logs, sub, err := _Cryptokitties0.contract.FilterLogs(opts, "Pause")
	if err != nil {
		return nil, err
	}
	return &Cryptokitties0PauseIterator{contract: _Cryptokitties0.contract, event: "Pause", logs: logs, sub: sub}, nil
}

// WatchPause is a free log subscription operation binding the contract event 0x6985a02210a168e66602d3235cb6db0e70f92b3ba4d376a33c0f3d9434bff625.
//
// Solidity: event Pause()
func (_Cryptokitties0 *Cryptokitties0Filterer) WatchPause(opts *bind.WatchOpts, sink chan<- *Cryptokitties0Pause) (event.Subscription, error) {

	logs, sub, err := _Cryptokitties0.contract.WatchLogs(opts, "Pause")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptokitties0Pause)
				if err := _Cryptokitties0.contract.UnpackLog(event, "Pause", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePause is a log parse operation binding the contract event 0x6985a02210a168e66602d3235cb6db0e70f92b3ba4d376a33c0f3d9434bff625.
//
// Solidity: event Pause()
func (_Cryptokitties0 *Cryptokitties0Filterer) ParsePause(log types.Log) (*Cryptokitties0Pause, error) {
	event := new(Cryptokitties0Pause)
	if err := _Cryptokitties0.contract.UnpackLog(event, "Pause", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Cryptokitties0UnpauseIterator is returned from FilterUnpause and is used to iterate over the raw logs and unpacked data for Unpause events raised by the Cryptokitties0 contract.
type Cryptokitties0UnpauseIterator struct {
	Event *Cryptokitties0Unpause // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptokitties0UnpauseIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptokitties0Unpause)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptokitties0Unpause)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptokitties0UnpauseIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptokitties0UnpauseIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptokitties0Unpause represents a Unpause event raised by the Cryptokitties0 contract.
type Cryptokitties0Unpause struct {
	Raw types.Log // Blockchain specific contextual infos
}

// FilterUnpause is a free log retrieval operation binding the contract event 0x7805862f689e2f13df9f062ff482ad3ad112aca9e0847911ed832e158c525b33.
//
// Solidity: event Unpause()
func (_Cryptokitties0 *Cryptokitties0Filterer) FilterUnpause(opts *bind.FilterOpts) (*Cryptokitties0UnpauseIterator, error) {

	logs, sub, err := _Cryptokitties0.contract.FilterLogs(opts, "Unpause")
	if err != nil {
		return nil, err
	}
	return &Cryptokitties0UnpauseIterator{contract: _Cryptokitties0.contract, event: "Unpause", logs: logs, sub: sub}, nil
}

// WatchUnpause is a free log subscription operation binding the contract event 0x7805862f689e2f13df9f062ff482ad3ad112aca9e0847911ed832e158c525b33.
//
// Solidity: event Unpause()
func (_Cryptokitties0 *Cryptokitties0Filterer) WatchUnpause(opts *bind.WatchOpts, sink chan<- *Cryptokitties0Unpause) (event.Subscription, error) {

	logs, sub, err := _Cryptokitties0.contract.WatchLogs(opts, "Unpause")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptokitties0Unpause)
				if err := _Cryptokitties0.contract.UnpackLog(event, "Unpause", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUnpause is a log parse operation binding the contract event 0x7805862f689e2f13df9f062ff482ad3ad112aca9e0847911ed832e158c525b33.
//
// Solidity: event Unpause()
func (_Cryptokitties0 *Cryptokitties0Filterer) ParseUnpause(log types.Log) (*Cryptokitties0Unpause, error) {
	event := new(Cryptokitties0Unpause)
	if err := _Cryptokitties0.contract.UnpackLog(event, "Unpause", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
