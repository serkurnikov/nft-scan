// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package cryptokitties_1

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Cryptokitties1MetaData contains all meta data concerning the Cryptokitties1 contract.
var Cryptokitties1MetaData = &bind.MetaData{
	ABI: "[{\"constant\":false,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"name\":\"_startingPrice\",\"type\":\"uint256\"},{\"name\":\"_endingPrice\",\"type\":\"uint256\"},{\"name\":\"_duration\",\"type\":\"uint256\"},{\"name\":\"_seller\",\"type\":\"address\"}],\"name\":\"createAuction\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"unpause\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"bid\",\"outputs\":[],\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"lastGen0SalePrices\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"paused\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"withdrawBalance\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"getAuction\",\"outputs\":[{\"name\":\"seller\",\"type\":\"address\"},{\"name\":\"startingPrice\",\"type\":\"uint256\"},{\"name\":\"endingPrice\",\"type\":\"uint256\"},{\"name\":\"duration\",\"type\":\"uint256\"},{\"name\":\"startedAt\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"ownerCut\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"pause\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"isSaleClockAuction\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"cancelAuctionWhenPaused\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"gen0SaleCount\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"cancelAuction\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"_tokenId\",\"type\":\"uint256\"}],\"name\":\"getCurrentPrice\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"nonFungibleContract\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"averageGen0SalePrice\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"name\":\"_nftAddr\",\"type\":\"address\"},{\"name\":\"_cut\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"startingPrice\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"endingPrice\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"duration\",\"type\":\"uint256\"}],\"name\":\"AuctionCreated\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"totalPrice\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"winner\",\"type\":\"address\"}],\"name\":\"AuctionSuccessful\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"AuctionCancelled\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[],\"name\":\"Pause\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[],\"name\":\"Unpause\",\"type\":\"event\"}]",
}

// Cryptokitties1ABI is the input ABI used to generate the binding from.
// Deprecated: Use Cryptokitties1MetaData.ABI instead.
var Cryptokitties1ABI = Cryptokitties1MetaData.ABI

// Cryptokitties1 is an auto generated Go binding around an Ethereum contract.
type Cryptokitties1 struct {
	Cryptokitties1Caller     // Read-only binding to the contract
	Cryptokitties1Transactor // Write-only binding to the contract
	Cryptokitties1Filterer   // Log filterer for contract events
}

// Cryptokitties1Caller is an auto generated read-only Go binding around an Ethereum contract.
type Cryptokitties1Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Cryptokitties1Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Cryptokitties1Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Cryptokitties1Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Cryptokitties1Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Cryptokitties1Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Cryptokitties1Session struct {
	Contract     *Cryptokitties1   // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Cryptokitties1CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Cryptokitties1CallerSession struct {
	Contract *Cryptokitties1Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts         // Call options to use throughout this session
}

// Cryptokitties1TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Cryptokitties1TransactorSession struct {
	Contract     *Cryptokitties1Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts         // Transaction auth options to use throughout this session
}

// Cryptokitties1Raw is an auto generated low-level Go binding around an Ethereum contract.
type Cryptokitties1Raw struct {
	Contract *Cryptokitties1 // Generic contract binding to access the raw methods on
}

// Cryptokitties1CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Cryptokitties1CallerRaw struct {
	Contract *Cryptokitties1Caller // Generic read-only contract binding to access the raw methods on
}

// Cryptokitties1TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Cryptokitties1TransactorRaw struct {
	Contract *Cryptokitties1Transactor // Generic write-only contract binding to access the raw methods on
}

// NewCryptokitties1 creates a new instance of Cryptokitties1, bound to a specific deployed contract.
func NewCryptokitties1(address common.Address, backend bind.ContractBackend) (*Cryptokitties1, error) {
	contract, err := bindCryptokitties1(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Cryptokitties1{Cryptokitties1Caller: Cryptokitties1Caller{contract: contract}, Cryptokitties1Transactor: Cryptokitties1Transactor{contract: contract}, Cryptokitties1Filterer: Cryptokitties1Filterer{contract: contract}}, nil
}

// NewCryptokitties1Caller creates a new read-only instance of Cryptokitties1, bound to a specific deployed contract.
func NewCryptokitties1Caller(address common.Address, caller bind.ContractCaller) (*Cryptokitties1Caller, error) {
	contract, err := bindCryptokitties1(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Cryptokitties1Caller{contract: contract}, nil
}

// NewCryptokitties1Transactor creates a new write-only instance of Cryptokitties1, bound to a specific deployed contract.
func NewCryptokitties1Transactor(address common.Address, transactor bind.ContractTransactor) (*Cryptokitties1Transactor, error) {
	contract, err := bindCryptokitties1(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Cryptokitties1Transactor{contract: contract}, nil
}

// NewCryptokitties1Filterer creates a new log filterer instance of Cryptokitties1, bound to a specific deployed contract.
func NewCryptokitties1Filterer(address common.Address, filterer bind.ContractFilterer) (*Cryptokitties1Filterer, error) {
	contract, err := bindCryptokitties1(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Cryptokitties1Filterer{contract: contract}, nil
}

// bindCryptokitties1 binds a generic wrapper to an already deployed contract.
func bindCryptokitties1(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Cryptokitties1ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Cryptokitties1 *Cryptokitties1Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Cryptokitties1.Contract.Cryptokitties1Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Cryptokitties1 *Cryptokitties1Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.Cryptokitties1Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Cryptokitties1 *Cryptokitties1Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.Cryptokitties1Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Cryptokitties1 *Cryptokitties1CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Cryptokitties1.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Cryptokitties1 *Cryptokitties1TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Cryptokitties1 *Cryptokitties1TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.contract.Transact(opts, method, params...)
}

// AverageGen0SalePrice is a free data retrieval call binding the contract method 0xeac9d94c.
//
// Solidity: function averageGen0SalePrice() view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1Caller) AverageGen0SalePrice(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Cryptokitties1.contract.Call(opts, &out, "averageGen0SalePrice")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// AverageGen0SalePrice is a free data retrieval call binding the contract method 0xeac9d94c.
//
// Solidity: function averageGen0SalePrice() view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1Session) AverageGen0SalePrice() (*big.Int, error) {
	return _Cryptokitties1.Contract.AverageGen0SalePrice(&_Cryptokitties1.CallOpts)
}

// AverageGen0SalePrice is a free data retrieval call binding the contract method 0xeac9d94c.
//
// Solidity: function averageGen0SalePrice() view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1CallerSession) AverageGen0SalePrice() (*big.Int, error) {
	return _Cryptokitties1.Contract.AverageGen0SalePrice(&_Cryptokitties1.CallOpts)
}

// Gen0SaleCount is a free data retrieval call binding the contract method 0x8a98a9cc.
//
// Solidity: function gen0SaleCount() view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1Caller) Gen0SaleCount(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Cryptokitties1.contract.Call(opts, &out, "gen0SaleCount")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Gen0SaleCount is a free data retrieval call binding the contract method 0x8a98a9cc.
//
// Solidity: function gen0SaleCount() view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1Session) Gen0SaleCount() (*big.Int, error) {
	return _Cryptokitties1.Contract.Gen0SaleCount(&_Cryptokitties1.CallOpts)
}

// Gen0SaleCount is a free data retrieval call binding the contract method 0x8a98a9cc.
//
// Solidity: function gen0SaleCount() view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1CallerSession) Gen0SaleCount() (*big.Int, error) {
	return _Cryptokitties1.Contract.Gen0SaleCount(&_Cryptokitties1.CallOpts)
}

// GetAuction is a free data retrieval call binding the contract method 0x78bd7935.
//
// Solidity: function getAuction(uint256 _tokenId) view returns(address seller, uint256 startingPrice, uint256 endingPrice, uint256 duration, uint256 startedAt)
func (_Cryptokitties1 *Cryptokitties1Caller) GetAuction(opts *bind.CallOpts, _tokenId *big.Int) (struct {
	Seller        common.Address
	StartingPrice *big.Int
	EndingPrice   *big.Int
	Duration      *big.Int
	StartedAt     *big.Int
}, error) {
	var out []interface{}
	err := _Cryptokitties1.contract.Call(opts, &out, "getAuction", _tokenId)

	outstruct := new(struct {
		Seller        common.Address
		StartingPrice *big.Int
		EndingPrice   *big.Int
		Duration      *big.Int
		StartedAt     *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Seller = *abi.ConvertType(out[0], new(common.Address)).(*common.Address)
	outstruct.StartingPrice = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.EndingPrice = *abi.ConvertType(out[2], new(*big.Int)).(**big.Int)
	outstruct.Duration = *abi.ConvertType(out[3], new(*big.Int)).(**big.Int)
	outstruct.StartedAt = *abi.ConvertType(out[4], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// GetAuction is a free data retrieval call binding the contract method 0x78bd7935.
//
// Solidity: function getAuction(uint256 _tokenId) view returns(address seller, uint256 startingPrice, uint256 endingPrice, uint256 duration, uint256 startedAt)
func (_Cryptokitties1 *Cryptokitties1Session) GetAuction(_tokenId *big.Int) (struct {
	Seller        common.Address
	StartingPrice *big.Int
	EndingPrice   *big.Int
	Duration      *big.Int
	StartedAt     *big.Int
}, error) {
	return _Cryptokitties1.Contract.GetAuction(&_Cryptokitties1.CallOpts, _tokenId)
}

// GetAuction is a free data retrieval call binding the contract method 0x78bd7935.
//
// Solidity: function getAuction(uint256 _tokenId) view returns(address seller, uint256 startingPrice, uint256 endingPrice, uint256 duration, uint256 startedAt)
func (_Cryptokitties1 *Cryptokitties1CallerSession) GetAuction(_tokenId *big.Int) (struct {
	Seller        common.Address
	StartingPrice *big.Int
	EndingPrice   *big.Int
	Duration      *big.Int
	StartedAt     *big.Int
}, error) {
	return _Cryptokitties1.Contract.GetAuction(&_Cryptokitties1.CallOpts, _tokenId)
}

// GetCurrentPrice is a free data retrieval call binding the contract method 0xc55d0f56.
//
// Solidity: function getCurrentPrice(uint256 _tokenId) view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1Caller) GetCurrentPrice(opts *bind.CallOpts, _tokenId *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Cryptokitties1.contract.Call(opts, &out, "getCurrentPrice", _tokenId)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetCurrentPrice is a free data retrieval call binding the contract method 0xc55d0f56.
//
// Solidity: function getCurrentPrice(uint256 _tokenId) view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1Session) GetCurrentPrice(_tokenId *big.Int) (*big.Int, error) {
	return _Cryptokitties1.Contract.GetCurrentPrice(&_Cryptokitties1.CallOpts, _tokenId)
}

// GetCurrentPrice is a free data retrieval call binding the contract method 0xc55d0f56.
//
// Solidity: function getCurrentPrice(uint256 _tokenId) view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1CallerSession) GetCurrentPrice(_tokenId *big.Int) (*big.Int, error) {
	return _Cryptokitties1.Contract.GetCurrentPrice(&_Cryptokitties1.CallOpts, _tokenId)
}

// IsSaleClockAuction is a free data retrieval call binding the contract method 0x85b86188.
//
// Solidity: function isSaleClockAuction() view returns(bool)
func (_Cryptokitties1 *Cryptokitties1Caller) IsSaleClockAuction(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Cryptokitties1.contract.Call(opts, &out, "isSaleClockAuction")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsSaleClockAuction is a free data retrieval call binding the contract method 0x85b86188.
//
// Solidity: function isSaleClockAuction() view returns(bool)
func (_Cryptokitties1 *Cryptokitties1Session) IsSaleClockAuction() (bool, error) {
	return _Cryptokitties1.Contract.IsSaleClockAuction(&_Cryptokitties1.CallOpts)
}

// IsSaleClockAuction is a free data retrieval call binding the contract method 0x85b86188.
//
// Solidity: function isSaleClockAuction() view returns(bool)
func (_Cryptokitties1 *Cryptokitties1CallerSession) IsSaleClockAuction() (bool, error) {
	return _Cryptokitties1.Contract.IsSaleClockAuction(&_Cryptokitties1.CallOpts)
}

// LastGen0SalePrices is a free data retrieval call binding the contract method 0x484eccb4.
//
// Solidity: function lastGen0SalePrices(uint256 ) view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1Caller) LastGen0SalePrices(opts *bind.CallOpts, arg0 *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Cryptokitties1.contract.Call(opts, &out, "lastGen0SalePrices", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// LastGen0SalePrices is a free data retrieval call binding the contract method 0x484eccb4.
//
// Solidity: function lastGen0SalePrices(uint256 ) view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1Session) LastGen0SalePrices(arg0 *big.Int) (*big.Int, error) {
	return _Cryptokitties1.Contract.LastGen0SalePrices(&_Cryptokitties1.CallOpts, arg0)
}

// LastGen0SalePrices is a free data retrieval call binding the contract method 0x484eccb4.
//
// Solidity: function lastGen0SalePrices(uint256 ) view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1CallerSession) LastGen0SalePrices(arg0 *big.Int) (*big.Int, error) {
	return _Cryptokitties1.Contract.LastGen0SalePrices(&_Cryptokitties1.CallOpts, arg0)
}

// NonFungibleContract is a free data retrieval call binding the contract method 0xdd1b7a0f.
//
// Solidity: function nonFungibleContract() view returns(address)
func (_Cryptokitties1 *Cryptokitties1Caller) NonFungibleContract(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Cryptokitties1.contract.Call(opts, &out, "nonFungibleContract")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// NonFungibleContract is a free data retrieval call binding the contract method 0xdd1b7a0f.
//
// Solidity: function nonFungibleContract() view returns(address)
func (_Cryptokitties1 *Cryptokitties1Session) NonFungibleContract() (common.Address, error) {
	return _Cryptokitties1.Contract.NonFungibleContract(&_Cryptokitties1.CallOpts)
}

// NonFungibleContract is a free data retrieval call binding the contract method 0xdd1b7a0f.
//
// Solidity: function nonFungibleContract() view returns(address)
func (_Cryptokitties1 *Cryptokitties1CallerSession) NonFungibleContract() (common.Address, error) {
	return _Cryptokitties1.Contract.NonFungibleContract(&_Cryptokitties1.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Cryptokitties1 *Cryptokitties1Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Cryptokitties1.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Cryptokitties1 *Cryptokitties1Session) Owner() (common.Address, error) {
	return _Cryptokitties1.Contract.Owner(&_Cryptokitties1.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Cryptokitties1 *Cryptokitties1CallerSession) Owner() (common.Address, error) {
	return _Cryptokitties1.Contract.Owner(&_Cryptokitties1.CallOpts)
}

// OwnerCut is a free data retrieval call binding the contract method 0x83b5ff8b.
//
// Solidity: function ownerCut() view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1Caller) OwnerCut(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Cryptokitties1.contract.Call(opts, &out, "ownerCut")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// OwnerCut is a free data retrieval call binding the contract method 0x83b5ff8b.
//
// Solidity: function ownerCut() view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1Session) OwnerCut() (*big.Int, error) {
	return _Cryptokitties1.Contract.OwnerCut(&_Cryptokitties1.CallOpts)
}

// OwnerCut is a free data retrieval call binding the contract method 0x83b5ff8b.
//
// Solidity: function ownerCut() view returns(uint256)
func (_Cryptokitties1 *Cryptokitties1CallerSession) OwnerCut() (*big.Int, error) {
	return _Cryptokitties1.Contract.OwnerCut(&_Cryptokitties1.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Cryptokitties1 *Cryptokitties1Caller) Paused(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Cryptokitties1.contract.Call(opts, &out, "paused")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Cryptokitties1 *Cryptokitties1Session) Paused() (bool, error) {
	return _Cryptokitties1.Contract.Paused(&_Cryptokitties1.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_Cryptokitties1 *Cryptokitties1CallerSession) Paused() (bool, error) {
	return _Cryptokitties1.Contract.Paused(&_Cryptokitties1.CallOpts)
}

// Bid is a paid mutator transaction binding the contract method 0x454a2ab3.
//
// Solidity: function bid(uint256 _tokenId) payable returns()
func (_Cryptokitties1 *Cryptokitties1Transactor) Bid(opts *bind.TransactOpts, _tokenId *big.Int) (*types.Transaction, error) {
	return _Cryptokitties1.contract.Transact(opts, "bid", _tokenId)
}

// Bid is a paid mutator transaction binding the contract method 0x454a2ab3.
//
// Solidity: function bid(uint256 _tokenId) payable returns()
func (_Cryptokitties1 *Cryptokitties1Session) Bid(_tokenId *big.Int) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.Bid(&_Cryptokitties1.TransactOpts, _tokenId)
}

// Bid is a paid mutator transaction binding the contract method 0x454a2ab3.
//
// Solidity: function bid(uint256 _tokenId) payable returns()
func (_Cryptokitties1 *Cryptokitties1TransactorSession) Bid(_tokenId *big.Int) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.Bid(&_Cryptokitties1.TransactOpts, _tokenId)
}

// CancelAuction is a paid mutator transaction binding the contract method 0x96b5a755.
//
// Solidity: function cancelAuction(uint256 _tokenId) returns()
func (_Cryptokitties1 *Cryptokitties1Transactor) CancelAuction(opts *bind.TransactOpts, _tokenId *big.Int) (*types.Transaction, error) {
	return _Cryptokitties1.contract.Transact(opts, "cancelAuction", _tokenId)
}

// CancelAuction is a paid mutator transaction binding the contract method 0x96b5a755.
//
// Solidity: function cancelAuction(uint256 _tokenId) returns()
func (_Cryptokitties1 *Cryptokitties1Session) CancelAuction(_tokenId *big.Int) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.CancelAuction(&_Cryptokitties1.TransactOpts, _tokenId)
}

// CancelAuction is a paid mutator transaction binding the contract method 0x96b5a755.
//
// Solidity: function cancelAuction(uint256 _tokenId) returns()
func (_Cryptokitties1 *Cryptokitties1TransactorSession) CancelAuction(_tokenId *big.Int) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.CancelAuction(&_Cryptokitties1.TransactOpts, _tokenId)
}

// CancelAuctionWhenPaused is a paid mutator transaction binding the contract method 0x878eb368.
//
// Solidity: function cancelAuctionWhenPaused(uint256 _tokenId) returns()
func (_Cryptokitties1 *Cryptokitties1Transactor) CancelAuctionWhenPaused(opts *bind.TransactOpts, _tokenId *big.Int) (*types.Transaction, error) {
	return _Cryptokitties1.contract.Transact(opts, "cancelAuctionWhenPaused", _tokenId)
}

// CancelAuctionWhenPaused is a paid mutator transaction binding the contract method 0x878eb368.
//
// Solidity: function cancelAuctionWhenPaused(uint256 _tokenId) returns()
func (_Cryptokitties1 *Cryptokitties1Session) CancelAuctionWhenPaused(_tokenId *big.Int) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.CancelAuctionWhenPaused(&_Cryptokitties1.TransactOpts, _tokenId)
}

// CancelAuctionWhenPaused is a paid mutator transaction binding the contract method 0x878eb368.
//
// Solidity: function cancelAuctionWhenPaused(uint256 _tokenId) returns()
func (_Cryptokitties1 *Cryptokitties1TransactorSession) CancelAuctionWhenPaused(_tokenId *big.Int) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.CancelAuctionWhenPaused(&_Cryptokitties1.TransactOpts, _tokenId)
}

// CreateAuction is a paid mutator transaction binding the contract method 0x27ebe40a.
//
// Solidity: function createAuction(uint256 _tokenId, uint256 _startingPrice, uint256 _endingPrice, uint256 _duration, address _seller) returns()
func (_Cryptokitties1 *Cryptokitties1Transactor) CreateAuction(opts *bind.TransactOpts, _tokenId *big.Int, _startingPrice *big.Int, _endingPrice *big.Int, _duration *big.Int, _seller common.Address) (*types.Transaction, error) {
	return _Cryptokitties1.contract.Transact(opts, "createAuction", _tokenId, _startingPrice, _endingPrice, _duration, _seller)
}

// CreateAuction is a paid mutator transaction binding the contract method 0x27ebe40a.
//
// Solidity: function createAuction(uint256 _tokenId, uint256 _startingPrice, uint256 _endingPrice, uint256 _duration, address _seller) returns()
func (_Cryptokitties1 *Cryptokitties1Session) CreateAuction(_tokenId *big.Int, _startingPrice *big.Int, _endingPrice *big.Int, _duration *big.Int, _seller common.Address) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.CreateAuction(&_Cryptokitties1.TransactOpts, _tokenId, _startingPrice, _endingPrice, _duration, _seller)
}

// CreateAuction is a paid mutator transaction binding the contract method 0x27ebe40a.
//
// Solidity: function createAuction(uint256 _tokenId, uint256 _startingPrice, uint256 _endingPrice, uint256 _duration, address _seller) returns()
func (_Cryptokitties1 *Cryptokitties1TransactorSession) CreateAuction(_tokenId *big.Int, _startingPrice *big.Int, _endingPrice *big.Int, _duration *big.Int, _seller common.Address) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.CreateAuction(&_Cryptokitties1.TransactOpts, _tokenId, _startingPrice, _endingPrice, _duration, _seller)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns(bool)
func (_Cryptokitties1 *Cryptokitties1Transactor) Pause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptokitties1.contract.Transact(opts, "pause")
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns(bool)
func (_Cryptokitties1 *Cryptokitties1Session) Pause() (*types.Transaction, error) {
	return _Cryptokitties1.Contract.Pause(&_Cryptokitties1.TransactOpts)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns(bool)
func (_Cryptokitties1 *Cryptokitties1TransactorSession) Pause() (*types.Transaction, error) {
	return _Cryptokitties1.Contract.Pause(&_Cryptokitties1.TransactOpts)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Cryptokitties1 *Cryptokitties1Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Cryptokitties1.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Cryptokitties1 *Cryptokitties1Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.TransferOwnership(&_Cryptokitties1.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Cryptokitties1 *Cryptokitties1TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Cryptokitties1.Contract.TransferOwnership(&_Cryptokitties1.TransactOpts, newOwner)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns(bool)
func (_Cryptokitties1 *Cryptokitties1Transactor) Unpause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptokitties1.contract.Transact(opts, "unpause")
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns(bool)
func (_Cryptokitties1 *Cryptokitties1Session) Unpause() (*types.Transaction, error) {
	return _Cryptokitties1.Contract.Unpause(&_Cryptokitties1.TransactOpts)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns(bool)
func (_Cryptokitties1 *Cryptokitties1TransactorSession) Unpause() (*types.Transaction, error) {
	return _Cryptokitties1.Contract.Unpause(&_Cryptokitties1.TransactOpts)
}

// WithdrawBalance is a paid mutator transaction binding the contract method 0x5fd8c710.
//
// Solidity: function withdrawBalance() returns()
func (_Cryptokitties1 *Cryptokitties1Transactor) WithdrawBalance(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Cryptokitties1.contract.Transact(opts, "withdrawBalance")
}

// WithdrawBalance is a paid mutator transaction binding the contract method 0x5fd8c710.
//
// Solidity: function withdrawBalance() returns()
func (_Cryptokitties1 *Cryptokitties1Session) WithdrawBalance() (*types.Transaction, error) {
	return _Cryptokitties1.Contract.WithdrawBalance(&_Cryptokitties1.TransactOpts)
}

// WithdrawBalance is a paid mutator transaction binding the contract method 0x5fd8c710.
//
// Solidity: function withdrawBalance() returns()
func (_Cryptokitties1 *Cryptokitties1TransactorSession) WithdrawBalance() (*types.Transaction, error) {
	return _Cryptokitties1.Contract.WithdrawBalance(&_Cryptokitties1.TransactOpts)
}

// Cryptokitties1AuctionCancelledIterator is returned from FilterAuctionCancelled and is used to iterate over the raw logs and unpacked data for AuctionCancelled events raised by the Cryptokitties1 contract.
type Cryptokitties1AuctionCancelledIterator struct {
	Event *Cryptokitties1AuctionCancelled // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptokitties1AuctionCancelledIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptokitties1AuctionCancelled)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptokitties1AuctionCancelled)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptokitties1AuctionCancelledIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptokitties1AuctionCancelledIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptokitties1AuctionCancelled represents a AuctionCancelled event raised by the Cryptokitties1 contract.
type Cryptokitties1AuctionCancelled struct {
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterAuctionCancelled is a free log retrieval operation binding the contract event 0x2809c7e17bf978fbc7194c0a694b638c4215e9140cacc6c38ca36010b45697df.
//
// Solidity: event AuctionCancelled(uint256 tokenId)
func (_Cryptokitties1 *Cryptokitties1Filterer) FilterAuctionCancelled(opts *bind.FilterOpts) (*Cryptokitties1AuctionCancelledIterator, error) {

	logs, sub, err := _Cryptokitties1.contract.FilterLogs(opts, "AuctionCancelled")
	if err != nil {
		return nil, err
	}
	return &Cryptokitties1AuctionCancelledIterator{contract: _Cryptokitties1.contract, event: "AuctionCancelled", logs: logs, sub: sub}, nil
}

// WatchAuctionCancelled is a free log subscription operation binding the contract event 0x2809c7e17bf978fbc7194c0a694b638c4215e9140cacc6c38ca36010b45697df.
//
// Solidity: event AuctionCancelled(uint256 tokenId)
func (_Cryptokitties1 *Cryptokitties1Filterer) WatchAuctionCancelled(opts *bind.WatchOpts, sink chan<- *Cryptokitties1AuctionCancelled) (event.Subscription, error) {

	logs, sub, err := _Cryptokitties1.contract.WatchLogs(opts, "AuctionCancelled")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptokitties1AuctionCancelled)
				if err := _Cryptokitties1.contract.UnpackLog(event, "AuctionCancelled", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAuctionCancelled is a log parse operation binding the contract event 0x2809c7e17bf978fbc7194c0a694b638c4215e9140cacc6c38ca36010b45697df.
//
// Solidity: event AuctionCancelled(uint256 tokenId)
func (_Cryptokitties1 *Cryptokitties1Filterer) ParseAuctionCancelled(log types.Log) (*Cryptokitties1AuctionCancelled, error) {
	event := new(Cryptokitties1AuctionCancelled)
	if err := _Cryptokitties1.contract.UnpackLog(event, "AuctionCancelled", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Cryptokitties1AuctionCreatedIterator is returned from FilterAuctionCreated and is used to iterate over the raw logs and unpacked data for AuctionCreated events raised by the Cryptokitties1 contract.
type Cryptokitties1AuctionCreatedIterator struct {
	Event *Cryptokitties1AuctionCreated // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptokitties1AuctionCreatedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptokitties1AuctionCreated)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptokitties1AuctionCreated)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptokitties1AuctionCreatedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptokitties1AuctionCreatedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptokitties1AuctionCreated represents a AuctionCreated event raised by the Cryptokitties1 contract.
type Cryptokitties1AuctionCreated struct {
	TokenId       *big.Int
	StartingPrice *big.Int
	EndingPrice   *big.Int
	Duration      *big.Int
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterAuctionCreated is a free log retrieval operation binding the contract event 0xa9c8dfcda5664a5a124c713e386da27de87432d5b668e79458501eb296389ba7.
//
// Solidity: event AuctionCreated(uint256 tokenId, uint256 startingPrice, uint256 endingPrice, uint256 duration)
func (_Cryptokitties1 *Cryptokitties1Filterer) FilterAuctionCreated(opts *bind.FilterOpts) (*Cryptokitties1AuctionCreatedIterator, error) {

	logs, sub, err := _Cryptokitties1.contract.FilterLogs(opts, "AuctionCreated")
	if err != nil {
		return nil, err
	}
	return &Cryptokitties1AuctionCreatedIterator{contract: _Cryptokitties1.contract, event: "AuctionCreated", logs: logs, sub: sub}, nil
}

// WatchAuctionCreated is a free log subscription operation binding the contract event 0xa9c8dfcda5664a5a124c713e386da27de87432d5b668e79458501eb296389ba7.
//
// Solidity: event AuctionCreated(uint256 tokenId, uint256 startingPrice, uint256 endingPrice, uint256 duration)
func (_Cryptokitties1 *Cryptokitties1Filterer) WatchAuctionCreated(opts *bind.WatchOpts, sink chan<- *Cryptokitties1AuctionCreated) (event.Subscription, error) {

	logs, sub, err := _Cryptokitties1.contract.WatchLogs(opts, "AuctionCreated")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptokitties1AuctionCreated)
				if err := _Cryptokitties1.contract.UnpackLog(event, "AuctionCreated", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAuctionCreated is a log parse operation binding the contract event 0xa9c8dfcda5664a5a124c713e386da27de87432d5b668e79458501eb296389ba7.
//
// Solidity: event AuctionCreated(uint256 tokenId, uint256 startingPrice, uint256 endingPrice, uint256 duration)
func (_Cryptokitties1 *Cryptokitties1Filterer) ParseAuctionCreated(log types.Log) (*Cryptokitties1AuctionCreated, error) {
	event := new(Cryptokitties1AuctionCreated)
	if err := _Cryptokitties1.contract.UnpackLog(event, "AuctionCreated", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Cryptokitties1AuctionSuccessfulIterator is returned from FilterAuctionSuccessful and is used to iterate over the raw logs and unpacked data for AuctionSuccessful events raised by the Cryptokitties1 contract.
type Cryptokitties1AuctionSuccessfulIterator struct {
	Event *Cryptokitties1AuctionSuccessful // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptokitties1AuctionSuccessfulIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptokitties1AuctionSuccessful)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptokitties1AuctionSuccessful)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptokitties1AuctionSuccessfulIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptokitties1AuctionSuccessfulIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptokitties1AuctionSuccessful represents a AuctionSuccessful event raised by the Cryptokitties1 contract.
type Cryptokitties1AuctionSuccessful struct {
	TokenId    *big.Int
	TotalPrice *big.Int
	Winner     common.Address
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterAuctionSuccessful is a free log retrieval operation binding the contract event 0x4fcc30d90a842164dd58501ab874a101a3749c3d4747139cefe7c876f4ccebd2.
//
// Solidity: event AuctionSuccessful(uint256 tokenId, uint256 totalPrice, address winner)
func (_Cryptokitties1 *Cryptokitties1Filterer) FilterAuctionSuccessful(opts *bind.FilterOpts) (*Cryptokitties1AuctionSuccessfulIterator, error) {

	logs, sub, err := _Cryptokitties1.contract.FilterLogs(opts, "AuctionSuccessful")
	if err != nil {
		return nil, err
	}
	return &Cryptokitties1AuctionSuccessfulIterator{contract: _Cryptokitties1.contract, event: "AuctionSuccessful", logs: logs, sub: sub}, nil
}

// WatchAuctionSuccessful is a free log subscription operation binding the contract event 0x4fcc30d90a842164dd58501ab874a101a3749c3d4747139cefe7c876f4ccebd2.
//
// Solidity: event AuctionSuccessful(uint256 tokenId, uint256 totalPrice, address winner)
func (_Cryptokitties1 *Cryptokitties1Filterer) WatchAuctionSuccessful(opts *bind.WatchOpts, sink chan<- *Cryptokitties1AuctionSuccessful) (event.Subscription, error) {

	logs, sub, err := _Cryptokitties1.contract.WatchLogs(opts, "AuctionSuccessful")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptokitties1AuctionSuccessful)
				if err := _Cryptokitties1.contract.UnpackLog(event, "AuctionSuccessful", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAuctionSuccessful is a log parse operation binding the contract event 0x4fcc30d90a842164dd58501ab874a101a3749c3d4747139cefe7c876f4ccebd2.
//
// Solidity: event AuctionSuccessful(uint256 tokenId, uint256 totalPrice, address winner)
func (_Cryptokitties1 *Cryptokitties1Filterer) ParseAuctionSuccessful(log types.Log) (*Cryptokitties1AuctionSuccessful, error) {
	event := new(Cryptokitties1AuctionSuccessful)
	if err := _Cryptokitties1.contract.UnpackLog(event, "AuctionSuccessful", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Cryptokitties1PauseIterator is returned from FilterPause and is used to iterate over the raw logs and unpacked data for Pause events raised by the Cryptokitties1 contract.
type Cryptokitties1PauseIterator struct {
	Event *Cryptokitties1Pause // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptokitties1PauseIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptokitties1Pause)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptokitties1Pause)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptokitties1PauseIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptokitties1PauseIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptokitties1Pause represents a Pause event raised by the Cryptokitties1 contract.
type Cryptokitties1Pause struct {
	Raw types.Log // Blockchain specific contextual infos
}

// FilterPause is a free log retrieval operation binding the contract event 0x6985a02210a168e66602d3235cb6db0e70f92b3ba4d376a33c0f3d9434bff625.
//
// Solidity: event Pause()
func (_Cryptokitties1 *Cryptokitties1Filterer) FilterPause(opts *bind.FilterOpts) (*Cryptokitties1PauseIterator, error) {

	logs, sub, err := _Cryptokitties1.contract.FilterLogs(opts, "Pause")
	if err != nil {
		return nil, err
	}
	return &Cryptokitties1PauseIterator{contract: _Cryptokitties1.contract, event: "Pause", logs: logs, sub: sub}, nil
}

// WatchPause is a free log subscription operation binding the contract event 0x6985a02210a168e66602d3235cb6db0e70f92b3ba4d376a33c0f3d9434bff625.
//
// Solidity: event Pause()
func (_Cryptokitties1 *Cryptokitties1Filterer) WatchPause(opts *bind.WatchOpts, sink chan<- *Cryptokitties1Pause) (event.Subscription, error) {

	logs, sub, err := _Cryptokitties1.contract.WatchLogs(opts, "Pause")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptokitties1Pause)
				if err := _Cryptokitties1.contract.UnpackLog(event, "Pause", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePause is a log parse operation binding the contract event 0x6985a02210a168e66602d3235cb6db0e70f92b3ba4d376a33c0f3d9434bff625.
//
// Solidity: event Pause()
func (_Cryptokitties1 *Cryptokitties1Filterer) ParsePause(log types.Log) (*Cryptokitties1Pause, error) {
	event := new(Cryptokitties1Pause)
	if err := _Cryptokitties1.contract.UnpackLog(event, "Pause", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Cryptokitties1UnpauseIterator is returned from FilterUnpause and is used to iterate over the raw logs and unpacked data for Unpause events raised by the Cryptokitties1 contract.
type Cryptokitties1UnpauseIterator struct {
	Event *Cryptokitties1Unpause // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Cryptokitties1UnpauseIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Cryptokitties1Unpause)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Cryptokitties1Unpause)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Cryptokitties1UnpauseIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Cryptokitties1UnpauseIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Cryptokitties1Unpause represents a Unpause event raised by the Cryptokitties1 contract.
type Cryptokitties1Unpause struct {
	Raw types.Log // Blockchain specific contextual infos
}

// FilterUnpause is a free log retrieval operation binding the contract event 0x7805862f689e2f13df9f062ff482ad3ad112aca9e0847911ed832e158c525b33.
//
// Solidity: event Unpause()
func (_Cryptokitties1 *Cryptokitties1Filterer) FilterUnpause(opts *bind.FilterOpts) (*Cryptokitties1UnpauseIterator, error) {

	logs, sub, err := _Cryptokitties1.contract.FilterLogs(opts, "Unpause")
	if err != nil {
		return nil, err
	}
	return &Cryptokitties1UnpauseIterator{contract: _Cryptokitties1.contract, event: "Unpause", logs: logs, sub: sub}, nil
}

// WatchUnpause is a free log subscription operation binding the contract event 0x7805862f689e2f13df9f062ff482ad3ad112aca9e0847911ed832e158c525b33.
//
// Solidity: event Unpause()
func (_Cryptokitties1 *Cryptokitties1Filterer) WatchUnpause(opts *bind.WatchOpts, sink chan<- *Cryptokitties1Unpause) (event.Subscription, error) {

	logs, sub, err := _Cryptokitties1.contract.WatchLogs(opts, "Unpause")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Cryptokitties1Unpause)
				if err := _Cryptokitties1.contract.UnpackLog(event, "Unpause", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUnpause is a log parse operation binding the contract event 0x7805862f689e2f13df9f062ff482ad3ad112aca9e0847911ed832e158c525b33.
//
// Solidity: event Unpause()
func (_Cryptokitties1 *Cryptokitties1Filterer) ParseUnpause(log types.Log) (*Cryptokitties1Unpause, error) {
	event := new(Cryptokitties1Unpause)
	if err := _Cryptokitties1.contract.UnpackLog(event, "Unpause", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
