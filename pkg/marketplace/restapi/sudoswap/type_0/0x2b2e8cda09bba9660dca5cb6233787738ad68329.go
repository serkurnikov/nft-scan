// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package sudoswap_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// LSSVMRouterNFTsForAnyNFTsTrade is an auto generated low-level Go binding around an user-defined struct.
type LSSVMRouterNFTsForAnyNFTsTrade struct {
	NftToTokenTrades []LSSVMRouterPairSwapSpecific
	TokenToNFTTrades []LSSVMRouterPairSwapAny
}

// LSSVMRouterNFTsForSpecificNFTsTrade is an auto generated low-level Go binding around an user-defined struct.
type LSSVMRouterNFTsForSpecificNFTsTrade struct {
	NftToTokenTrades []LSSVMRouterPairSwapSpecific
	TokenToNFTTrades []LSSVMRouterPairSwapSpecific
}

// LSSVMRouterPairSwapAny is an auto generated low-level Go binding around an user-defined struct.
type LSSVMRouterPairSwapAny struct {
	Pair     common.Address
	NumItems *big.Int
}

// LSSVMRouterPairSwapSpecific is an auto generated low-level Go binding around an user-defined struct.
type LSSVMRouterPairSwapSpecific struct {
	Pair   common.Address
	NftIds []*big.Int
}

// LSSVMRouterRobustPairNFTsFoTokenAndTokenforNFTsTrade is an auto generated low-level Go binding around an user-defined struct.
type LSSVMRouterRobustPairNFTsFoTokenAndTokenforNFTsTrade struct {
	TokenToNFTTrades []LSSVMRouterRobustPairSwapSpecific
	NftToTokenTrades []LSSVMRouterRobustPairSwapSpecificForToken
	InputAmount      *big.Int
	TokenRecipient   common.Address
	NftRecipient     common.Address
}

// LSSVMRouterRobustPairSwapAny is an auto generated low-level Go binding around an user-defined struct.
type LSSVMRouterRobustPairSwapAny struct {
	SwapInfo LSSVMRouterPairSwapAny
	MaxCost  *big.Int
}

// LSSVMRouterRobustPairSwapSpecific is an auto generated low-level Go binding around an user-defined struct.
type LSSVMRouterRobustPairSwapSpecific struct {
	SwapInfo LSSVMRouterPairSwapSpecific
	MaxCost  *big.Int
}

// LSSVMRouterRobustPairSwapSpecificForToken is an auto generated low-level Go binding around an user-defined struct.
type LSSVMRouterRobustPairSwapSpecificForToken struct {
	SwapInfo  LSSVMRouterPairSwapSpecific
	MinOutput *big.Int
}

// Sudoswap0MetaData contains all meta data concerning the Sudoswap0 contract.
var Sudoswap0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"contractILSSVMPairFactoryLike\",\"name\":\"_factory\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"inputs\":[],\"name\":\"factory\",\"outputs\":[{\"internalType\":\"contractILSSVMPairFactoryLike\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contractERC20\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"enumILSSVMPairFactoryLike.PairVariant\",\"name\":\"variant\",\"type\":\"uint8\"}],\"name\":\"pairTransferERC20From\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contractIERC721\",\"name\":\"nft\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"enumILSSVMPairFactoryLike.PairVariant\",\"name\":\"variant\",\"type\":\"uint8\"}],\"name\":\"pairTransferNFTFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"numItems\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.PairSwapAny\",\"name\":\"swapInfo\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"maxCost\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.RobustPairSwapAny[]\",\"name\":\"swapList\",\"type\":\"tuple[]\"},{\"internalType\":\"uint256\",\"name\":\"inputAmount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"robustSwapERC20ForAnyNFTs\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"remainingValue\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific\",\"name\":\"swapInfo\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"maxCost\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.RobustPairSwapSpecific[]\",\"name\":\"swapList\",\"type\":\"tuple[]\"},{\"internalType\":\"uint256\",\"name\":\"inputAmount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"robustSwapERC20ForSpecificNFTs\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"remainingValue\",\"type\":\"uint256\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"components\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific\",\"name\":\"swapInfo\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"maxCost\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.RobustPairSwapSpecific[]\",\"name\":\"tokenToNFTTrades\",\"type\":\"tuple[]\"},{\"components\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific\",\"name\":\"swapInfo\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"minOutput\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.RobustPairSwapSpecificForToken[]\",\"name\":\"nftToTokenTrades\",\"type\":\"tuple[]\"},{\"internalType\":\"uint256\",\"name\":\"inputAmount\",\"type\":\"uint256\"},{\"internalType\":\"addresspayable\",\"name\":\"tokenRecipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"}],\"internalType\":\"structLSSVMRouter.RobustPairNFTsFoTokenAndTokenforNFTsTrade\",\"name\":\"params\",\"type\":\"tuple\"}],\"name\":\"robustSwapERC20ForSpecificNFTsAndNFTsToToken\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"remainingValue\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"outputAmount\",\"type\":\"uint256\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"numItems\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.PairSwapAny\",\"name\":\"swapInfo\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"maxCost\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.RobustPairSwapAny[]\",\"name\":\"swapList\",\"type\":\"tuple[]\"},{\"internalType\":\"addresspayable\",\"name\":\"ethRecipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"robustSwapETHForAnyNFTs\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"remainingValue\",\"type\":\"uint256\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific\",\"name\":\"swapInfo\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"maxCost\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.RobustPairSwapSpecific[]\",\"name\":\"swapList\",\"type\":\"tuple[]\"},{\"internalType\":\"addresspayable\",\"name\":\"ethRecipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"robustSwapETHForSpecificNFTs\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"remainingValue\",\"type\":\"uint256\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"components\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific\",\"name\":\"swapInfo\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"maxCost\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.RobustPairSwapSpecific[]\",\"name\":\"tokenToNFTTrades\",\"type\":\"tuple[]\"},{\"components\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific\",\"name\":\"swapInfo\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"minOutput\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.RobustPairSwapSpecificForToken[]\",\"name\":\"nftToTokenTrades\",\"type\":\"tuple[]\"},{\"internalType\":\"uint256\",\"name\":\"inputAmount\",\"type\":\"uint256\"},{\"internalType\":\"addresspayable\",\"name\":\"tokenRecipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"}],\"internalType\":\"structLSSVMRouter.RobustPairNFTsFoTokenAndTokenforNFTsTrade\",\"name\":\"params\",\"type\":\"tuple\"}],\"name\":\"robustSwapETHForSpecificNFTsAndNFTsToToken\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"remainingValue\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"outputAmount\",\"type\":\"uint256\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific\",\"name\":\"swapInfo\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"minOutput\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.RobustPairSwapSpecificForToken[]\",\"name\":\"swapList\",\"type\":\"tuple[]\"},{\"internalType\":\"addresspayable\",\"name\":\"tokenRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"robustSwapNFTsForToken\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"outputAmount\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"numItems\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.PairSwapAny[]\",\"name\":\"swapList\",\"type\":\"tuple[]\"},{\"internalType\":\"uint256\",\"name\":\"inputAmount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"swapERC20ForAnyNFTs\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"remainingValue\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific[]\",\"name\":\"swapList\",\"type\":\"tuple[]\"},{\"internalType\":\"uint256\",\"name\":\"inputAmount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"swapERC20ForSpecificNFTs\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"remainingValue\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"numItems\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.PairSwapAny[]\",\"name\":\"swapList\",\"type\":\"tuple[]\"},{\"internalType\":\"addresspayable\",\"name\":\"ethRecipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"swapETHForAnyNFTs\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"remainingValue\",\"type\":\"uint256\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific[]\",\"name\":\"swapList\",\"type\":\"tuple[]\"},{\"internalType\":\"addresspayable\",\"name\":\"ethRecipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"swapETHForSpecificNFTs\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"remainingValue\",\"type\":\"uint256\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific[]\",\"name\":\"nftToTokenTrades\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"numItems\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.PairSwapAny[]\",\"name\":\"tokenToNFTTrades\",\"type\":\"tuple[]\"}],\"internalType\":\"structLSSVMRouter.NFTsForAnyNFTsTrade\",\"name\":\"trade\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"inputAmount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minOutput\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"swapNFTsForAnyNFTsThroughERC20\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"outputAmount\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific[]\",\"name\":\"nftToTokenTrades\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"numItems\",\"type\":\"uint256\"}],\"internalType\":\"structLSSVMRouter.PairSwapAny[]\",\"name\":\"tokenToNFTTrades\",\"type\":\"tuple[]\"}],\"internalType\":\"structLSSVMRouter.NFTsForAnyNFTsTrade\",\"name\":\"trade\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"minOutput\",\"type\":\"uint256\"},{\"internalType\":\"addresspayable\",\"name\":\"ethRecipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"swapNFTsForAnyNFTsThroughETH\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"outputAmount\",\"type\":\"uint256\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific[]\",\"name\":\"nftToTokenTrades\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific[]\",\"name\":\"tokenToNFTTrades\",\"type\":\"tuple[]\"}],\"internalType\":\"structLSSVMRouter.NFTsForSpecificNFTsTrade\",\"name\":\"trade\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"inputAmount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minOutput\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"swapNFTsForSpecificNFTsThroughERC20\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"outputAmount\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific[]\",\"name\":\"nftToTokenTrades\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific[]\",\"name\":\"tokenToNFTTrades\",\"type\":\"tuple[]\"}],\"internalType\":\"structLSSVMRouter.NFTsForSpecificNFTsTrade\",\"name\":\"trade\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"minOutput\",\"type\":\"uint256\"},{\"internalType\":\"addresspayable\",\"name\":\"ethRecipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"nftRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"swapNFTsForSpecificNFTsThroughETH\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"outputAmount\",\"type\":\"uint256\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"contractLSSVMPair\",\"name\":\"pair\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"nftIds\",\"type\":\"uint256[]\"}],\"internalType\":\"structLSSVMRouter.PairSwapSpecific[]\",\"name\":\"swapList\",\"type\":\"tuple[]\"},{\"internalType\":\"uint256\",\"name\":\"minOutput\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"tokenRecipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"}],\"name\":\"swapNFTsForToken\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"outputAmount\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
}

// Sudoswap0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Sudoswap0MetaData.ABI instead.
var Sudoswap0ABI = Sudoswap0MetaData.ABI

// Sudoswap0 is an auto generated Go binding around an Ethereum contract.
type Sudoswap0 struct {
	Sudoswap0Caller     // Read-only binding to the contract
	Sudoswap0Transactor // Write-only binding to the contract
	Sudoswap0Filterer   // Log filterer for contract events
}

// Sudoswap0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Sudoswap0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Sudoswap0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Sudoswap0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Sudoswap0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Sudoswap0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Sudoswap0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Sudoswap0Session struct {
	Contract     *Sudoswap0        // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Sudoswap0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Sudoswap0CallerSession struct {
	Contract *Sudoswap0Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts    // Call options to use throughout this session
}

// Sudoswap0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Sudoswap0TransactorSession struct {
	Contract     *Sudoswap0Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts    // Transaction auth options to use throughout this session
}

// Sudoswap0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Sudoswap0Raw struct {
	Contract *Sudoswap0 // Generic contract binding to access the raw methods on
}

// Sudoswap0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Sudoswap0CallerRaw struct {
	Contract *Sudoswap0Caller // Generic read-only contract binding to access the raw methods on
}

// Sudoswap0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Sudoswap0TransactorRaw struct {
	Contract *Sudoswap0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewSudoswap0 creates a new instance of Sudoswap0, bound to a specific deployed contract.
func NewSudoswap0(address common.Address, backend bind.ContractBackend) (*Sudoswap0, error) {
	contract, err := bindSudoswap0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Sudoswap0{Sudoswap0Caller: Sudoswap0Caller{contract: contract}, Sudoswap0Transactor: Sudoswap0Transactor{contract: contract}, Sudoswap0Filterer: Sudoswap0Filterer{contract: contract}}, nil
}

// NewSudoswap0Caller creates a new read-only instance of Sudoswap0, bound to a specific deployed contract.
func NewSudoswap0Caller(address common.Address, caller bind.ContractCaller) (*Sudoswap0Caller, error) {
	contract, err := bindSudoswap0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Sudoswap0Caller{contract: contract}, nil
}

// NewSudoswap0Transactor creates a new write-only instance of Sudoswap0, bound to a specific deployed contract.
func NewSudoswap0Transactor(address common.Address, transactor bind.ContractTransactor) (*Sudoswap0Transactor, error) {
	contract, err := bindSudoswap0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Sudoswap0Transactor{contract: contract}, nil
}

// NewSudoswap0Filterer creates a new log filterer instance of Sudoswap0, bound to a specific deployed contract.
func NewSudoswap0Filterer(address common.Address, filterer bind.ContractFilterer) (*Sudoswap0Filterer, error) {
	contract, err := bindSudoswap0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Sudoswap0Filterer{contract: contract}, nil
}

// bindSudoswap0 binds a generic wrapper to an already deployed contract.
func bindSudoswap0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Sudoswap0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Sudoswap0 *Sudoswap0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Sudoswap0.Contract.Sudoswap0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Sudoswap0 *Sudoswap0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Sudoswap0.Contract.Sudoswap0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Sudoswap0 *Sudoswap0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Sudoswap0.Contract.Sudoswap0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Sudoswap0 *Sudoswap0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Sudoswap0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Sudoswap0 *Sudoswap0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Sudoswap0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Sudoswap0 *Sudoswap0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Sudoswap0.Contract.contract.Transact(opts, method, params...)
}

// Factory is a free data retrieval call binding the contract method 0xc45a0155.
//
// Solidity: function factory() view returns(address)
func (_Sudoswap0 *Sudoswap0Caller) Factory(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Sudoswap0.contract.Call(opts, &out, "factory")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Factory is a free data retrieval call binding the contract method 0xc45a0155.
//
// Solidity: function factory() view returns(address)
func (_Sudoswap0 *Sudoswap0Session) Factory() (common.Address, error) {
	return _Sudoswap0.Contract.Factory(&_Sudoswap0.CallOpts)
}

// Factory is a free data retrieval call binding the contract method 0xc45a0155.
//
// Solidity: function factory() view returns(address)
func (_Sudoswap0 *Sudoswap0CallerSession) Factory() (common.Address, error) {
	return _Sudoswap0.Contract.Factory(&_Sudoswap0.CallOpts)
}

// PairTransferERC20From is a paid mutator transaction binding the contract method 0x4b456d0c.
//
// Solidity: function pairTransferERC20From(address token, address from, address to, uint256 amount, uint8 variant) returns()
func (_Sudoswap0 *Sudoswap0Transactor) PairTransferERC20From(opts *bind.TransactOpts, token common.Address, from common.Address, to common.Address, amount *big.Int, variant uint8) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "pairTransferERC20From", token, from, to, amount, variant)
}

// PairTransferERC20From is a paid mutator transaction binding the contract method 0x4b456d0c.
//
// Solidity: function pairTransferERC20From(address token, address from, address to, uint256 amount, uint8 variant) returns()
func (_Sudoswap0 *Sudoswap0Session) PairTransferERC20From(token common.Address, from common.Address, to common.Address, amount *big.Int, variant uint8) (*types.Transaction, error) {
	return _Sudoswap0.Contract.PairTransferERC20From(&_Sudoswap0.TransactOpts, token, from, to, amount, variant)
}

// PairTransferERC20From is a paid mutator transaction binding the contract method 0x4b456d0c.
//
// Solidity: function pairTransferERC20From(address token, address from, address to, uint256 amount, uint8 variant) returns()
func (_Sudoswap0 *Sudoswap0TransactorSession) PairTransferERC20From(token common.Address, from common.Address, to common.Address, amount *big.Int, variant uint8) (*types.Transaction, error) {
	return _Sudoswap0.Contract.PairTransferERC20From(&_Sudoswap0.TransactOpts, token, from, to, amount, variant)
}

// PairTransferNFTFrom is a paid mutator transaction binding the contract method 0x748ff339.
//
// Solidity: function pairTransferNFTFrom(address nft, address from, address to, uint256 id, uint8 variant) returns()
func (_Sudoswap0 *Sudoswap0Transactor) PairTransferNFTFrom(opts *bind.TransactOpts, nft common.Address, from common.Address, to common.Address, id *big.Int, variant uint8) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "pairTransferNFTFrom", nft, from, to, id, variant)
}

// PairTransferNFTFrom is a paid mutator transaction binding the contract method 0x748ff339.
//
// Solidity: function pairTransferNFTFrom(address nft, address from, address to, uint256 id, uint8 variant) returns()
func (_Sudoswap0 *Sudoswap0Session) PairTransferNFTFrom(nft common.Address, from common.Address, to common.Address, id *big.Int, variant uint8) (*types.Transaction, error) {
	return _Sudoswap0.Contract.PairTransferNFTFrom(&_Sudoswap0.TransactOpts, nft, from, to, id, variant)
}

// PairTransferNFTFrom is a paid mutator transaction binding the contract method 0x748ff339.
//
// Solidity: function pairTransferNFTFrom(address nft, address from, address to, uint256 id, uint8 variant) returns()
func (_Sudoswap0 *Sudoswap0TransactorSession) PairTransferNFTFrom(nft common.Address, from common.Address, to common.Address, id *big.Int, variant uint8) (*types.Transaction, error) {
	return _Sudoswap0.Contract.PairTransferNFTFrom(&_Sudoswap0.TransactOpts, nft, from, to, id, variant)
}

// RobustSwapERC20ForAnyNFTs is a paid mutator transaction binding the contract method 0xc5cf5704.
//
// Solidity: function robustSwapERC20ForAnyNFTs(((address,uint256),uint256)[] swapList, uint256 inputAmount, address nftRecipient, uint256 deadline) returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Transactor) RobustSwapERC20ForAnyNFTs(opts *bind.TransactOpts, swapList []LSSVMRouterRobustPairSwapAny, inputAmount *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "robustSwapERC20ForAnyNFTs", swapList, inputAmount, nftRecipient, deadline)
}

// RobustSwapERC20ForAnyNFTs is a paid mutator transaction binding the contract method 0xc5cf5704.
//
// Solidity: function robustSwapERC20ForAnyNFTs(((address,uint256),uint256)[] swapList, uint256 inputAmount, address nftRecipient, uint256 deadline) returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Session) RobustSwapERC20ForAnyNFTs(swapList []LSSVMRouterRobustPairSwapAny, inputAmount *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapERC20ForAnyNFTs(&_Sudoswap0.TransactOpts, swapList, inputAmount, nftRecipient, deadline)
}

// RobustSwapERC20ForAnyNFTs is a paid mutator transaction binding the contract method 0xc5cf5704.
//
// Solidity: function robustSwapERC20ForAnyNFTs(((address,uint256),uint256)[] swapList, uint256 inputAmount, address nftRecipient, uint256 deadline) returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0TransactorSession) RobustSwapERC20ForAnyNFTs(swapList []LSSVMRouterRobustPairSwapAny, inputAmount *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapERC20ForAnyNFTs(&_Sudoswap0.TransactOpts, swapList, inputAmount, nftRecipient, deadline)
}

// RobustSwapERC20ForSpecificNFTs is a paid mutator transaction binding the contract method 0xdb6ddb26.
//
// Solidity: function robustSwapERC20ForSpecificNFTs(((address,uint256[]),uint256)[] swapList, uint256 inputAmount, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Transactor) RobustSwapERC20ForSpecificNFTs(opts *bind.TransactOpts, swapList []LSSVMRouterRobustPairSwapSpecific, inputAmount *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "robustSwapERC20ForSpecificNFTs", swapList, inputAmount, nftRecipient, deadline)
}

// RobustSwapERC20ForSpecificNFTs is a paid mutator transaction binding the contract method 0xdb6ddb26.
//
// Solidity: function robustSwapERC20ForSpecificNFTs(((address,uint256[]),uint256)[] swapList, uint256 inputAmount, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Session) RobustSwapERC20ForSpecificNFTs(swapList []LSSVMRouterRobustPairSwapSpecific, inputAmount *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapERC20ForSpecificNFTs(&_Sudoswap0.TransactOpts, swapList, inputAmount, nftRecipient, deadline)
}

// RobustSwapERC20ForSpecificNFTs is a paid mutator transaction binding the contract method 0xdb6ddb26.
//
// Solidity: function robustSwapERC20ForSpecificNFTs(((address,uint256[]),uint256)[] swapList, uint256 inputAmount, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0TransactorSession) RobustSwapERC20ForSpecificNFTs(swapList []LSSVMRouterRobustPairSwapSpecific, inputAmount *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapERC20ForSpecificNFTs(&_Sudoswap0.TransactOpts, swapList, inputAmount, nftRecipient, deadline)
}

// RobustSwapERC20ForSpecificNFTsAndNFTsToToken is a paid mutator transaction binding the contract method 0x8c1daa2b.
//
// Solidity: function robustSwapERC20ForSpecificNFTsAndNFTsToToken((((address,uint256[]),uint256)[],((address,uint256[]),uint256)[],uint256,address,address) params) payable returns(uint256 remainingValue, uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Transactor) RobustSwapERC20ForSpecificNFTsAndNFTsToToken(opts *bind.TransactOpts, params LSSVMRouterRobustPairNFTsFoTokenAndTokenforNFTsTrade) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "robustSwapERC20ForSpecificNFTsAndNFTsToToken", params)
}

// RobustSwapERC20ForSpecificNFTsAndNFTsToToken is a paid mutator transaction binding the contract method 0x8c1daa2b.
//
// Solidity: function robustSwapERC20ForSpecificNFTsAndNFTsToToken((((address,uint256[]),uint256)[],((address,uint256[]),uint256)[],uint256,address,address) params) payable returns(uint256 remainingValue, uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Session) RobustSwapERC20ForSpecificNFTsAndNFTsToToken(params LSSVMRouterRobustPairNFTsFoTokenAndTokenforNFTsTrade) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapERC20ForSpecificNFTsAndNFTsToToken(&_Sudoswap0.TransactOpts, params)
}

// RobustSwapERC20ForSpecificNFTsAndNFTsToToken is a paid mutator transaction binding the contract method 0x8c1daa2b.
//
// Solidity: function robustSwapERC20ForSpecificNFTsAndNFTsToToken((((address,uint256[]),uint256)[],((address,uint256[]),uint256)[],uint256,address,address) params) payable returns(uint256 remainingValue, uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0TransactorSession) RobustSwapERC20ForSpecificNFTsAndNFTsToToken(params LSSVMRouterRobustPairNFTsFoTokenAndTokenforNFTsTrade) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapERC20ForSpecificNFTsAndNFTsToToken(&_Sudoswap0.TransactOpts, params)
}

// RobustSwapETHForAnyNFTs is a paid mutator transaction binding the contract method 0x2f9b9d4e.
//
// Solidity: function robustSwapETHForAnyNFTs(((address,uint256),uint256)[] swapList, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Transactor) RobustSwapETHForAnyNFTs(opts *bind.TransactOpts, swapList []LSSVMRouterRobustPairSwapAny, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "robustSwapETHForAnyNFTs", swapList, ethRecipient, nftRecipient, deadline)
}

// RobustSwapETHForAnyNFTs is a paid mutator transaction binding the contract method 0x2f9b9d4e.
//
// Solidity: function robustSwapETHForAnyNFTs(((address,uint256),uint256)[] swapList, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Session) RobustSwapETHForAnyNFTs(swapList []LSSVMRouterRobustPairSwapAny, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapETHForAnyNFTs(&_Sudoswap0.TransactOpts, swapList, ethRecipient, nftRecipient, deadline)
}

// RobustSwapETHForAnyNFTs is a paid mutator transaction binding the contract method 0x2f9b9d4e.
//
// Solidity: function robustSwapETHForAnyNFTs(((address,uint256),uint256)[] swapList, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0TransactorSession) RobustSwapETHForAnyNFTs(swapList []LSSVMRouterRobustPairSwapAny, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapETHForAnyNFTs(&_Sudoswap0.TransactOpts, swapList, ethRecipient, nftRecipient, deadline)
}

// RobustSwapETHForSpecificNFTs is a paid mutator transaction binding the contract method 0x3efd9e71.
//
// Solidity: function robustSwapETHForSpecificNFTs(((address,uint256[]),uint256)[] swapList, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Transactor) RobustSwapETHForSpecificNFTs(opts *bind.TransactOpts, swapList []LSSVMRouterRobustPairSwapSpecific, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "robustSwapETHForSpecificNFTs", swapList, ethRecipient, nftRecipient, deadline)
}

// RobustSwapETHForSpecificNFTs is a paid mutator transaction binding the contract method 0x3efd9e71.
//
// Solidity: function robustSwapETHForSpecificNFTs(((address,uint256[]),uint256)[] swapList, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Session) RobustSwapETHForSpecificNFTs(swapList []LSSVMRouterRobustPairSwapSpecific, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapETHForSpecificNFTs(&_Sudoswap0.TransactOpts, swapList, ethRecipient, nftRecipient, deadline)
}

// RobustSwapETHForSpecificNFTs is a paid mutator transaction binding the contract method 0x3efd9e71.
//
// Solidity: function robustSwapETHForSpecificNFTs(((address,uint256[]),uint256)[] swapList, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0TransactorSession) RobustSwapETHForSpecificNFTs(swapList []LSSVMRouterRobustPairSwapSpecific, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapETHForSpecificNFTs(&_Sudoswap0.TransactOpts, swapList, ethRecipient, nftRecipient, deadline)
}

// RobustSwapETHForSpecificNFTsAndNFTsToToken is a paid mutator transaction binding the contract method 0xab5c0da2.
//
// Solidity: function robustSwapETHForSpecificNFTsAndNFTsToToken((((address,uint256[]),uint256)[],((address,uint256[]),uint256)[],uint256,address,address) params) payable returns(uint256 remainingValue, uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Transactor) RobustSwapETHForSpecificNFTsAndNFTsToToken(opts *bind.TransactOpts, params LSSVMRouterRobustPairNFTsFoTokenAndTokenforNFTsTrade) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "robustSwapETHForSpecificNFTsAndNFTsToToken", params)
}

// RobustSwapETHForSpecificNFTsAndNFTsToToken is a paid mutator transaction binding the contract method 0xab5c0da2.
//
// Solidity: function robustSwapETHForSpecificNFTsAndNFTsToToken((((address,uint256[]),uint256)[],((address,uint256[]),uint256)[],uint256,address,address) params) payable returns(uint256 remainingValue, uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Session) RobustSwapETHForSpecificNFTsAndNFTsToToken(params LSSVMRouterRobustPairNFTsFoTokenAndTokenforNFTsTrade) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapETHForSpecificNFTsAndNFTsToToken(&_Sudoswap0.TransactOpts, params)
}

// RobustSwapETHForSpecificNFTsAndNFTsToToken is a paid mutator transaction binding the contract method 0xab5c0da2.
//
// Solidity: function robustSwapETHForSpecificNFTsAndNFTsToToken((((address,uint256[]),uint256)[],((address,uint256[]),uint256)[],uint256,address,address) params) payable returns(uint256 remainingValue, uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0TransactorSession) RobustSwapETHForSpecificNFTsAndNFTsToToken(params LSSVMRouterRobustPairNFTsFoTokenAndTokenforNFTsTrade) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapETHForSpecificNFTsAndNFTsToToken(&_Sudoswap0.TransactOpts, params)
}

// RobustSwapNFTsForToken is a paid mutator transaction binding the contract method 0x2b997f8e.
//
// Solidity: function robustSwapNFTsForToken(((address,uint256[]),uint256)[] swapList, address tokenRecipient, uint256 deadline) returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Transactor) RobustSwapNFTsForToken(opts *bind.TransactOpts, swapList []LSSVMRouterRobustPairSwapSpecificForToken, tokenRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "robustSwapNFTsForToken", swapList, tokenRecipient, deadline)
}

// RobustSwapNFTsForToken is a paid mutator transaction binding the contract method 0x2b997f8e.
//
// Solidity: function robustSwapNFTsForToken(((address,uint256[]),uint256)[] swapList, address tokenRecipient, uint256 deadline) returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Session) RobustSwapNFTsForToken(swapList []LSSVMRouterRobustPairSwapSpecificForToken, tokenRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapNFTsForToken(&_Sudoswap0.TransactOpts, swapList, tokenRecipient, deadline)
}

// RobustSwapNFTsForToken is a paid mutator transaction binding the contract method 0x2b997f8e.
//
// Solidity: function robustSwapNFTsForToken(((address,uint256[]),uint256)[] swapList, address tokenRecipient, uint256 deadline) returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0TransactorSession) RobustSwapNFTsForToken(swapList []LSSVMRouterRobustPairSwapSpecificForToken, tokenRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.RobustSwapNFTsForToken(&_Sudoswap0.TransactOpts, swapList, tokenRecipient, deadline)
}

// SwapERC20ForAnyNFTs is a paid mutator transaction binding the contract method 0xad134450.
//
// Solidity: function swapERC20ForAnyNFTs((address,uint256)[] swapList, uint256 inputAmount, address nftRecipient, uint256 deadline) returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Transactor) SwapERC20ForAnyNFTs(opts *bind.TransactOpts, swapList []LSSVMRouterPairSwapAny, inputAmount *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "swapERC20ForAnyNFTs", swapList, inputAmount, nftRecipient, deadline)
}

// SwapERC20ForAnyNFTs is a paid mutator transaction binding the contract method 0xad134450.
//
// Solidity: function swapERC20ForAnyNFTs((address,uint256)[] swapList, uint256 inputAmount, address nftRecipient, uint256 deadline) returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Session) SwapERC20ForAnyNFTs(swapList []LSSVMRouterPairSwapAny, inputAmount *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapERC20ForAnyNFTs(&_Sudoswap0.TransactOpts, swapList, inputAmount, nftRecipient, deadline)
}

// SwapERC20ForAnyNFTs is a paid mutator transaction binding the contract method 0xad134450.
//
// Solidity: function swapERC20ForAnyNFTs((address,uint256)[] swapList, uint256 inputAmount, address nftRecipient, uint256 deadline) returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0TransactorSession) SwapERC20ForAnyNFTs(swapList []LSSVMRouterPairSwapAny, inputAmount *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapERC20ForAnyNFTs(&_Sudoswap0.TransactOpts, swapList, inputAmount, nftRecipient, deadline)
}

// SwapERC20ForSpecificNFTs is a paid mutator transaction binding the contract method 0x14db3784.
//
// Solidity: function swapERC20ForSpecificNFTs((address,uint256[])[] swapList, uint256 inputAmount, address nftRecipient, uint256 deadline) returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Transactor) SwapERC20ForSpecificNFTs(opts *bind.TransactOpts, swapList []LSSVMRouterPairSwapSpecific, inputAmount *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "swapERC20ForSpecificNFTs", swapList, inputAmount, nftRecipient, deadline)
}

// SwapERC20ForSpecificNFTs is a paid mutator transaction binding the contract method 0x14db3784.
//
// Solidity: function swapERC20ForSpecificNFTs((address,uint256[])[] swapList, uint256 inputAmount, address nftRecipient, uint256 deadline) returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Session) SwapERC20ForSpecificNFTs(swapList []LSSVMRouterPairSwapSpecific, inputAmount *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapERC20ForSpecificNFTs(&_Sudoswap0.TransactOpts, swapList, inputAmount, nftRecipient, deadline)
}

// SwapERC20ForSpecificNFTs is a paid mutator transaction binding the contract method 0x14db3784.
//
// Solidity: function swapERC20ForSpecificNFTs((address,uint256[])[] swapList, uint256 inputAmount, address nftRecipient, uint256 deadline) returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0TransactorSession) SwapERC20ForSpecificNFTs(swapList []LSSVMRouterPairSwapSpecific, inputAmount *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapERC20ForSpecificNFTs(&_Sudoswap0.TransactOpts, swapList, inputAmount, nftRecipient, deadline)
}

// SwapETHForAnyNFTs is a paid mutator transaction binding the contract method 0xdb42312a.
//
// Solidity: function swapETHForAnyNFTs((address,uint256)[] swapList, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Transactor) SwapETHForAnyNFTs(opts *bind.TransactOpts, swapList []LSSVMRouterPairSwapAny, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "swapETHForAnyNFTs", swapList, ethRecipient, nftRecipient, deadline)
}

// SwapETHForAnyNFTs is a paid mutator transaction binding the contract method 0xdb42312a.
//
// Solidity: function swapETHForAnyNFTs((address,uint256)[] swapList, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Session) SwapETHForAnyNFTs(swapList []LSSVMRouterPairSwapAny, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapETHForAnyNFTs(&_Sudoswap0.TransactOpts, swapList, ethRecipient, nftRecipient, deadline)
}

// SwapETHForAnyNFTs is a paid mutator transaction binding the contract method 0xdb42312a.
//
// Solidity: function swapETHForAnyNFTs((address,uint256)[] swapList, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0TransactorSession) SwapETHForAnyNFTs(swapList []LSSVMRouterPairSwapAny, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapETHForAnyNFTs(&_Sudoswap0.TransactOpts, swapList, ethRecipient, nftRecipient, deadline)
}

// SwapETHForSpecificNFTs is a paid mutator transaction binding the contract method 0x11132000.
//
// Solidity: function swapETHForSpecificNFTs((address,uint256[])[] swapList, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Transactor) SwapETHForSpecificNFTs(opts *bind.TransactOpts, swapList []LSSVMRouterPairSwapSpecific, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "swapETHForSpecificNFTs", swapList, ethRecipient, nftRecipient, deadline)
}

// SwapETHForSpecificNFTs is a paid mutator transaction binding the contract method 0x11132000.
//
// Solidity: function swapETHForSpecificNFTs((address,uint256[])[] swapList, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0Session) SwapETHForSpecificNFTs(swapList []LSSVMRouterPairSwapSpecific, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapETHForSpecificNFTs(&_Sudoswap0.TransactOpts, swapList, ethRecipient, nftRecipient, deadline)
}

// SwapETHForSpecificNFTs is a paid mutator transaction binding the contract method 0x11132000.
//
// Solidity: function swapETHForSpecificNFTs((address,uint256[])[] swapList, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 remainingValue)
func (_Sudoswap0 *Sudoswap0TransactorSession) SwapETHForSpecificNFTs(swapList []LSSVMRouterPairSwapSpecific, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapETHForSpecificNFTs(&_Sudoswap0.TransactOpts, swapList, ethRecipient, nftRecipient, deadline)
}

// SwapNFTsForAnyNFTsThroughERC20 is a paid mutator transaction binding the contract method 0xf138513a.
//
// Solidity: function swapNFTsForAnyNFTsThroughERC20(((address,uint256[])[],(address,uint256)[]) trade, uint256 inputAmount, uint256 minOutput, address nftRecipient, uint256 deadline) returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Transactor) SwapNFTsForAnyNFTsThroughERC20(opts *bind.TransactOpts, trade LSSVMRouterNFTsForAnyNFTsTrade, inputAmount *big.Int, minOutput *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "swapNFTsForAnyNFTsThroughERC20", trade, inputAmount, minOutput, nftRecipient, deadline)
}

// SwapNFTsForAnyNFTsThroughERC20 is a paid mutator transaction binding the contract method 0xf138513a.
//
// Solidity: function swapNFTsForAnyNFTsThroughERC20(((address,uint256[])[],(address,uint256)[]) trade, uint256 inputAmount, uint256 minOutput, address nftRecipient, uint256 deadline) returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Session) SwapNFTsForAnyNFTsThroughERC20(trade LSSVMRouterNFTsForAnyNFTsTrade, inputAmount *big.Int, minOutput *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapNFTsForAnyNFTsThroughERC20(&_Sudoswap0.TransactOpts, trade, inputAmount, minOutput, nftRecipient, deadline)
}

// SwapNFTsForAnyNFTsThroughERC20 is a paid mutator transaction binding the contract method 0xf138513a.
//
// Solidity: function swapNFTsForAnyNFTsThroughERC20(((address,uint256[])[],(address,uint256)[]) trade, uint256 inputAmount, uint256 minOutput, address nftRecipient, uint256 deadline) returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0TransactorSession) SwapNFTsForAnyNFTsThroughERC20(trade LSSVMRouterNFTsForAnyNFTsTrade, inputAmount *big.Int, minOutput *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapNFTsForAnyNFTsThroughERC20(&_Sudoswap0.TransactOpts, trade, inputAmount, minOutput, nftRecipient, deadline)
}

// SwapNFTsForAnyNFTsThroughETH is a paid mutator transaction binding the contract method 0xd89bb2b5.
//
// Solidity: function swapNFTsForAnyNFTsThroughETH(((address,uint256[])[],(address,uint256)[]) trade, uint256 minOutput, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Transactor) SwapNFTsForAnyNFTsThroughETH(opts *bind.TransactOpts, trade LSSVMRouterNFTsForAnyNFTsTrade, minOutput *big.Int, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "swapNFTsForAnyNFTsThroughETH", trade, minOutput, ethRecipient, nftRecipient, deadline)
}

// SwapNFTsForAnyNFTsThroughETH is a paid mutator transaction binding the contract method 0xd89bb2b5.
//
// Solidity: function swapNFTsForAnyNFTsThroughETH(((address,uint256[])[],(address,uint256)[]) trade, uint256 minOutput, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Session) SwapNFTsForAnyNFTsThroughETH(trade LSSVMRouterNFTsForAnyNFTsTrade, minOutput *big.Int, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapNFTsForAnyNFTsThroughETH(&_Sudoswap0.TransactOpts, trade, minOutput, ethRecipient, nftRecipient, deadline)
}

// SwapNFTsForAnyNFTsThroughETH is a paid mutator transaction binding the contract method 0xd89bb2b5.
//
// Solidity: function swapNFTsForAnyNFTsThroughETH(((address,uint256[])[],(address,uint256)[]) trade, uint256 minOutput, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0TransactorSession) SwapNFTsForAnyNFTsThroughETH(trade LSSVMRouterNFTsForAnyNFTsTrade, minOutput *big.Int, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapNFTsForAnyNFTsThroughETH(&_Sudoswap0.TransactOpts, trade, minOutput, ethRecipient, nftRecipient, deadline)
}

// SwapNFTsForSpecificNFTsThroughERC20 is a paid mutator transaction binding the contract method 0x84a4c8c4.
//
// Solidity: function swapNFTsForSpecificNFTsThroughERC20(((address,uint256[])[],(address,uint256[])[]) trade, uint256 inputAmount, uint256 minOutput, address nftRecipient, uint256 deadline) returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Transactor) SwapNFTsForSpecificNFTsThroughERC20(opts *bind.TransactOpts, trade LSSVMRouterNFTsForSpecificNFTsTrade, inputAmount *big.Int, minOutput *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "swapNFTsForSpecificNFTsThroughERC20", trade, inputAmount, minOutput, nftRecipient, deadline)
}

// SwapNFTsForSpecificNFTsThroughERC20 is a paid mutator transaction binding the contract method 0x84a4c8c4.
//
// Solidity: function swapNFTsForSpecificNFTsThroughERC20(((address,uint256[])[],(address,uint256[])[]) trade, uint256 inputAmount, uint256 minOutput, address nftRecipient, uint256 deadline) returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Session) SwapNFTsForSpecificNFTsThroughERC20(trade LSSVMRouterNFTsForSpecificNFTsTrade, inputAmount *big.Int, minOutput *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapNFTsForSpecificNFTsThroughERC20(&_Sudoswap0.TransactOpts, trade, inputAmount, minOutput, nftRecipient, deadline)
}

// SwapNFTsForSpecificNFTsThroughERC20 is a paid mutator transaction binding the contract method 0x84a4c8c4.
//
// Solidity: function swapNFTsForSpecificNFTsThroughERC20(((address,uint256[])[],(address,uint256[])[]) trade, uint256 inputAmount, uint256 minOutput, address nftRecipient, uint256 deadline) returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0TransactorSession) SwapNFTsForSpecificNFTsThroughERC20(trade LSSVMRouterNFTsForSpecificNFTsTrade, inputAmount *big.Int, minOutput *big.Int, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapNFTsForSpecificNFTsThroughERC20(&_Sudoswap0.TransactOpts, trade, inputAmount, minOutput, nftRecipient, deadline)
}

// SwapNFTsForSpecificNFTsThroughETH is a paid mutator transaction binding the contract method 0xa833d43a.
//
// Solidity: function swapNFTsForSpecificNFTsThroughETH(((address,uint256[])[],(address,uint256[])[]) trade, uint256 minOutput, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Transactor) SwapNFTsForSpecificNFTsThroughETH(opts *bind.TransactOpts, trade LSSVMRouterNFTsForSpecificNFTsTrade, minOutput *big.Int, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "swapNFTsForSpecificNFTsThroughETH", trade, minOutput, ethRecipient, nftRecipient, deadline)
}

// SwapNFTsForSpecificNFTsThroughETH is a paid mutator transaction binding the contract method 0xa833d43a.
//
// Solidity: function swapNFTsForSpecificNFTsThroughETH(((address,uint256[])[],(address,uint256[])[]) trade, uint256 minOutput, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Session) SwapNFTsForSpecificNFTsThroughETH(trade LSSVMRouterNFTsForSpecificNFTsTrade, minOutput *big.Int, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapNFTsForSpecificNFTsThroughETH(&_Sudoswap0.TransactOpts, trade, minOutput, ethRecipient, nftRecipient, deadline)
}

// SwapNFTsForSpecificNFTsThroughETH is a paid mutator transaction binding the contract method 0xa833d43a.
//
// Solidity: function swapNFTsForSpecificNFTsThroughETH(((address,uint256[])[],(address,uint256[])[]) trade, uint256 minOutput, address ethRecipient, address nftRecipient, uint256 deadline) payable returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0TransactorSession) SwapNFTsForSpecificNFTsThroughETH(trade LSSVMRouterNFTsForSpecificNFTsTrade, minOutput *big.Int, ethRecipient common.Address, nftRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapNFTsForSpecificNFTsThroughETH(&_Sudoswap0.TransactOpts, trade, minOutput, ethRecipient, nftRecipient, deadline)
}

// SwapNFTsForToken is a paid mutator transaction binding the contract method 0xdabf67d7.
//
// Solidity: function swapNFTsForToken((address,uint256[])[] swapList, uint256 minOutput, address tokenRecipient, uint256 deadline) returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Transactor) SwapNFTsForToken(opts *bind.TransactOpts, swapList []LSSVMRouterPairSwapSpecific, minOutput *big.Int, tokenRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.contract.Transact(opts, "swapNFTsForToken", swapList, minOutput, tokenRecipient, deadline)
}

// SwapNFTsForToken is a paid mutator transaction binding the contract method 0xdabf67d7.
//
// Solidity: function swapNFTsForToken((address,uint256[])[] swapList, uint256 minOutput, address tokenRecipient, uint256 deadline) returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0Session) SwapNFTsForToken(swapList []LSSVMRouterPairSwapSpecific, minOutput *big.Int, tokenRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapNFTsForToken(&_Sudoswap0.TransactOpts, swapList, minOutput, tokenRecipient, deadline)
}

// SwapNFTsForToken is a paid mutator transaction binding the contract method 0xdabf67d7.
//
// Solidity: function swapNFTsForToken((address,uint256[])[] swapList, uint256 minOutput, address tokenRecipient, uint256 deadline) returns(uint256 outputAmount)
func (_Sudoswap0 *Sudoswap0TransactorSession) SwapNFTsForToken(swapList []LSSVMRouterPairSwapSpecific, minOutput *big.Int, tokenRecipient common.Address, deadline *big.Int) (*types.Transaction, error) {
	return _Sudoswap0.Contract.SwapNFTsForToken(&_Sudoswap0.TransactOpts, swapList, minOutput, tokenRecipient, deadline)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Sudoswap0 *Sudoswap0Transactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Sudoswap0.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Sudoswap0 *Sudoswap0Session) Receive() (*types.Transaction, error) {
	return _Sudoswap0.Contract.Receive(&_Sudoswap0.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Sudoswap0 *Sudoswap0TransactorSession) Receive() (*types.Transaction, error) {
	return _Sudoswap0.Contract.Receive(&_Sudoswap0.TransactOpts)
}
