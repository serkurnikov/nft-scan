// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package gem_0

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// GemSwapConverstionDetails is an auto generated low-level Go binding around an user-defined struct.
type GemSwapConverstionDetails struct {
	ConversionData []byte
}

// GemSwapERC1155Details is an auto generated low-level Go binding around an user-defined struct.
type GemSwapERC1155Details struct {
	TokenAddr common.Address
	Ids       []*big.Int
	Amounts   []*big.Int
}

// GemSwapERC20Details is an auto generated low-level Go binding around an user-defined struct.
type GemSwapERC20Details struct {
	TokenAddrs []common.Address
	Amounts    []*big.Int
}

// GemSwapOpenseaTrades is an auto generated low-level Go binding around an user-defined struct.
type GemSwapOpenseaTrades struct {
	Value     *big.Int
	TradeData []byte
}

// MarketRegistryTradeDetails is an auto generated low-level Go binding around an user-defined struct.
type MarketRegistryTradeDetails struct {
	MarketId  *big.Int
	Value     *big.Int
	TradeData []byte
}

// SpecialTransferHelperERC721Details is an auto generated low-level Go binding around an user-defined struct.
type SpecialTransferHelperERC721Details struct {
	TokenAddr common.Address
	To        []common.Address
	Ids       []*big.Int
}

// Gem0MetaData contains all meta data concerning the Gem0 contract.
var Gem0MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_marketRegistry\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_converter\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_guardian\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"GOV\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_affiliate\",\"type\":\"address\"}],\"name\":\"addAffiliate\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_marketId\",\"type\":\"uint256\"}],\"name\":\"addSponsoredMarket\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"affiliates\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"affiliate\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"isActive\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"baseFees\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structGemSwap.OpenseaTrades[]\",\"name\":\"openseaTrades\",\"type\":\"tuple[]\"}],\"name\":\"batchBuyFromOpenSea\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"address[]\",\"name\":\"tokenAddrs\",\"type\":\"address[]\"},{\"internalType\":\"uint256[]\",\"name\":\"amounts\",\"type\":\"uint256[]\"}],\"internalType\":\"structGemSwap.ERC20Details\",\"name\":\"erc20Details\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"marketId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structMarketRegistry.TradeDetails[]\",\"name\":\"tradeDetails\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"bytes\",\"name\":\"conversionData\",\"type\":\"bytes\"}],\"internalType\":\"structGemSwap.ConverstionDetails[]\",\"name\":\"converstionDetails\",\"type\":\"tuple[]\"},{\"internalType\":\"address[]\",\"name\":\"dustTokens\",\"type\":\"address[]\"}],\"name\":\"batchBuyWithERC20s\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"marketId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structMarketRegistry.TradeDetails[]\",\"name\":\"tradeDetails\",\"type\":\"tuple[]\"}],\"name\":\"batchBuyWithETH\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"closeAllTrades\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"converter\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"guardian\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"marketRegistry\",\"outputs\":[{\"internalType\":\"contractMarketRegistry\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"address[]\",\"name\":\"tokenAddrs\",\"type\":\"address[]\"},{\"internalType\":\"uint256[]\",\"name\":\"amounts\",\"type\":\"uint256[]\"}],\"internalType\":\"structGemSwap.ERC20Details\",\"name\":\"erc20Details\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"tokenAddr\",\"type\":\"address\"},{\"internalType\":\"address[]\",\"name\":\"to\",\"type\":\"address[]\"},{\"internalType\":\"uint256[]\",\"name\":\"ids\",\"type\":\"uint256[]\"}],\"internalType\":\"structSpecialTransferHelper.ERC721Details[]\",\"name\":\"erc721Details\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"tokenAddr\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"ids\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256[]\",\"name\":\"amounts\",\"type\":\"uint256[]\"}],\"internalType\":\"structGemSwap.ERC1155Details[]\",\"name\":\"erc1155Details\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"bytes\",\"name\":\"conversionData\",\"type\":\"bytes\"}],\"internalType\":\"structGemSwap.ConverstionDetails[]\",\"name\":\"converstionDetails\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"marketId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"tradeData\",\"type\":\"bytes\"}],\"internalType\":\"structMarketRegistry.TradeDetails[]\",\"name\":\"tradeDetails\",\"type\":\"tuple[]\"},{\"internalType\":\"address[]\",\"name\":\"dustTokens\",\"type\":\"address[]\"},{\"internalType\":\"uint256[2]\",\"name\":\"feeDetails\",\"type\":\"uint256[2]\"}],\"name\":\"multiAssetSwap\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256[]\",\"name\":\"\",\"type\":\"uint256[]\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC1155BatchReceived\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC1155Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC721Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC721Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"openForFreeTrades\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"openForTrades\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"punkProxy\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"ids\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256[]\",\"name\":\"amounts\",\"type\":\"uint256[]\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"rescueERC1155\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"rescueERC20\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"ids\",\"type\":\"uint256[]\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"rescueERC721\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"rescueETH\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_baseFees\",\"type\":\"uint256\"}],\"name\":\"setBaseFees\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_converter\",\"type\":\"address\"}],\"name\":\"setConverter\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contractMarketRegistry\",\"name\":\"_marketRegistry\",\"type\":\"address\"}],\"name\":\"setMarketRegistry\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contractIERC20\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"setOneTimeApproval\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bool\",\"name\":\"_openForFreeTrades\",\"type\":\"bool\"}],\"name\":\"setOpenForFreeTrades\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bool\",\"name\":\"_openForTrades\",\"type\":\"bool\"}],\"name\":\"setOpenForTrades\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"setUp\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"sponsoredMarkets\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"marketId\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"isActive\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes4\",\"name\":\"interfaceId\",\"type\":\"bytes4\"}],\"name\":\"supportsInterface\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_affiliateIndex\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_affiliate\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_IsActive\",\"type\":\"bool\"}],\"name\":\"updateAffiliate\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_guardian\",\"type\":\"address\"}],\"name\":\"updateGuardian\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_marketIndex\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_marketId\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"_isActive\",\"type\":\"bool\"}],\"name\":\"updateSponsoredMarket\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
}

// Gem0ABI is the input ABI used to generate the binding from.
// Deprecated: Use Gem0MetaData.ABI instead.
var Gem0ABI = Gem0MetaData.ABI

// Gem0 is an auto generated Go binding around an Ethereum contract.
type Gem0 struct {
	Gem0Caller     // Read-only binding to the contract
	Gem0Transactor // Write-only binding to the contract
	Gem0Filterer   // Log filterer for contract events
}

// Gem0Caller is an auto generated read-only Go binding around an Ethereum contract.
type Gem0Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Gem0Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Gem0Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Gem0Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Gem0Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Gem0Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Gem0Session struct {
	Contract     *Gem0             // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Gem0CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Gem0CallerSession struct {
	Contract *Gem0Caller   // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// Gem0TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Gem0TransactorSession struct {
	Contract     *Gem0Transactor   // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Gem0Raw is an auto generated low-level Go binding around an Ethereum contract.
type Gem0Raw struct {
	Contract *Gem0 // Generic contract binding to access the raw methods on
}

// Gem0CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Gem0CallerRaw struct {
	Contract *Gem0Caller // Generic read-only contract binding to access the raw methods on
}

// Gem0TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Gem0TransactorRaw struct {
	Contract *Gem0Transactor // Generic write-only contract binding to access the raw methods on
}

// NewGem0 creates a new instance of Gem0, bound to a specific deployed contract.
func NewGem0(address common.Address, backend bind.ContractBackend) (*Gem0, error) {
	contract, err := bindGem0(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Gem0{Gem0Caller: Gem0Caller{contract: contract}, Gem0Transactor: Gem0Transactor{contract: contract}, Gem0Filterer: Gem0Filterer{contract: contract}}, nil
}

// NewGem0Caller creates a new read-only instance of Gem0, bound to a specific deployed contract.
func NewGem0Caller(address common.Address, caller bind.ContractCaller) (*Gem0Caller, error) {
	contract, err := bindGem0(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Gem0Caller{contract: contract}, nil
}

// NewGem0Transactor creates a new write-only instance of Gem0, bound to a specific deployed contract.
func NewGem0Transactor(address common.Address, transactor bind.ContractTransactor) (*Gem0Transactor, error) {
	contract, err := bindGem0(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Gem0Transactor{contract: contract}, nil
}

// NewGem0Filterer creates a new log filterer instance of Gem0, bound to a specific deployed contract.
func NewGem0Filterer(address common.Address, filterer bind.ContractFilterer) (*Gem0Filterer, error) {
	contract, err := bindGem0(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Gem0Filterer{contract: contract}, nil
}

// bindGem0 binds a generic wrapper to an already deployed contract.
func bindGem0(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Gem0ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Gem0 *Gem0Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Gem0.Contract.Gem0Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Gem0 *Gem0Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Gem0.Contract.Gem0Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Gem0 *Gem0Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Gem0.Contract.Gem0Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Gem0 *Gem0CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Gem0.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Gem0 *Gem0TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Gem0.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Gem0 *Gem0TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Gem0.Contract.contract.Transact(opts, method, params...)
}

// GOV is a free data retrieval call binding the contract method 0x180cb47f.
//
// Solidity: function GOV() view returns(address)
func (_Gem0 *Gem0Caller) GOV(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Gem0.contract.Call(opts, &out, "GOV")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GOV is a free data retrieval call binding the contract method 0x180cb47f.
//
// Solidity: function GOV() view returns(address)
func (_Gem0 *Gem0Session) GOV() (common.Address, error) {
	return _Gem0.Contract.GOV(&_Gem0.CallOpts)
}

// GOV is a free data retrieval call binding the contract method 0x180cb47f.
//
// Solidity: function GOV() view returns(address)
func (_Gem0 *Gem0CallerSession) GOV() (common.Address, error) {
	return _Gem0.Contract.GOV(&_Gem0.CallOpts)
}

// Affiliates is a free data retrieval call binding the contract method 0x1bd78748.
//
// Solidity: function affiliates(uint256 ) view returns(address affiliate, bool isActive)
func (_Gem0 *Gem0Caller) Affiliates(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Affiliate common.Address
	IsActive  bool
}, error) {
	var out []interface{}
	err := _Gem0.contract.Call(opts, &out, "affiliates", arg0)

	outstruct := new(struct {
		Affiliate common.Address
		IsActive  bool
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Affiliate = *abi.ConvertType(out[0], new(common.Address)).(*common.Address)
	outstruct.IsActive = *abi.ConvertType(out[1], new(bool)).(*bool)

	return *outstruct, err

}

// Affiliates is a free data retrieval call binding the contract method 0x1bd78748.
//
// Solidity: function affiliates(uint256 ) view returns(address affiliate, bool isActive)
func (_Gem0 *Gem0Session) Affiliates(arg0 *big.Int) (struct {
	Affiliate common.Address
	IsActive  bool
}, error) {
	return _Gem0.Contract.Affiliates(&_Gem0.CallOpts, arg0)
}

// Affiliates is a free data retrieval call binding the contract method 0x1bd78748.
//
// Solidity: function affiliates(uint256 ) view returns(address affiliate, bool isActive)
func (_Gem0 *Gem0CallerSession) Affiliates(arg0 *big.Int) (struct {
	Affiliate common.Address
	IsActive  bool
}, error) {
	return _Gem0.Contract.Affiliates(&_Gem0.CallOpts, arg0)
}

// BaseFees is a free data retrieval call binding the contract method 0xddb382f9.
//
// Solidity: function baseFees() view returns(uint256)
func (_Gem0 *Gem0Caller) BaseFees(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Gem0.contract.Call(opts, &out, "baseFees")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BaseFees is a free data retrieval call binding the contract method 0xddb382f9.
//
// Solidity: function baseFees() view returns(uint256)
func (_Gem0 *Gem0Session) BaseFees() (*big.Int, error) {
	return _Gem0.Contract.BaseFees(&_Gem0.CallOpts)
}

// BaseFees is a free data retrieval call binding the contract method 0xddb382f9.
//
// Solidity: function baseFees() view returns(uint256)
func (_Gem0 *Gem0CallerSession) BaseFees() (*big.Int, error) {
	return _Gem0.Contract.BaseFees(&_Gem0.CallOpts)
}

// Converter is a free data retrieval call binding the contract method 0xbd38837b.
//
// Solidity: function converter() view returns(address)
func (_Gem0 *Gem0Caller) Converter(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Gem0.contract.Call(opts, &out, "converter")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Converter is a free data retrieval call binding the contract method 0xbd38837b.
//
// Solidity: function converter() view returns(address)
func (_Gem0 *Gem0Session) Converter() (common.Address, error) {
	return _Gem0.Contract.Converter(&_Gem0.CallOpts)
}

// Converter is a free data retrieval call binding the contract method 0xbd38837b.
//
// Solidity: function converter() view returns(address)
func (_Gem0 *Gem0CallerSession) Converter() (common.Address, error) {
	return _Gem0.Contract.Converter(&_Gem0.CallOpts)
}

// Guardian is a free data retrieval call binding the contract method 0x452a9320.
//
// Solidity: function guardian() view returns(address)
func (_Gem0 *Gem0Caller) Guardian(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Gem0.contract.Call(opts, &out, "guardian")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Guardian is a free data retrieval call binding the contract method 0x452a9320.
//
// Solidity: function guardian() view returns(address)
func (_Gem0 *Gem0Session) Guardian() (common.Address, error) {
	return _Gem0.Contract.Guardian(&_Gem0.CallOpts)
}

// Guardian is a free data retrieval call binding the contract method 0x452a9320.
//
// Solidity: function guardian() view returns(address)
func (_Gem0 *Gem0CallerSession) Guardian() (common.Address, error) {
	return _Gem0.Contract.Guardian(&_Gem0.CallOpts)
}

// MarketRegistry is a free data retrieval call binding the contract method 0xecb96fe6.
//
// Solidity: function marketRegistry() view returns(address)
func (_Gem0 *Gem0Caller) MarketRegistry(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Gem0.contract.Call(opts, &out, "marketRegistry")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// MarketRegistry is a free data retrieval call binding the contract method 0xecb96fe6.
//
// Solidity: function marketRegistry() view returns(address)
func (_Gem0 *Gem0Session) MarketRegistry() (common.Address, error) {
	return _Gem0.Contract.MarketRegistry(&_Gem0.CallOpts)
}

// MarketRegistry is a free data retrieval call binding the contract method 0xecb96fe6.
//
// Solidity: function marketRegistry() view returns(address)
func (_Gem0 *Gem0CallerSession) MarketRegistry() (common.Address, error) {
	return _Gem0.Contract.MarketRegistry(&_Gem0.CallOpts)
}

// OpenForFreeTrades is a free data retrieval call binding the contract method 0x11f85417.
//
// Solidity: function openForFreeTrades() view returns(bool)
func (_Gem0 *Gem0Caller) OpenForFreeTrades(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Gem0.contract.Call(opts, &out, "openForFreeTrades")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// OpenForFreeTrades is a free data retrieval call binding the contract method 0x11f85417.
//
// Solidity: function openForFreeTrades() view returns(bool)
func (_Gem0 *Gem0Session) OpenForFreeTrades() (bool, error) {
	return _Gem0.Contract.OpenForFreeTrades(&_Gem0.CallOpts)
}

// OpenForFreeTrades is a free data retrieval call binding the contract method 0x11f85417.
//
// Solidity: function openForFreeTrades() view returns(bool)
func (_Gem0 *Gem0CallerSession) OpenForFreeTrades() (bool, error) {
	return _Gem0.Contract.OpenForFreeTrades(&_Gem0.CallOpts)
}

// OpenForTrades is a free data retrieval call binding the contract method 0xa1b62797.
//
// Solidity: function openForTrades() view returns(bool)
func (_Gem0 *Gem0Caller) OpenForTrades(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Gem0.contract.Call(opts, &out, "openForTrades")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// OpenForTrades is a free data retrieval call binding the contract method 0xa1b62797.
//
// Solidity: function openForTrades() view returns(bool)
func (_Gem0 *Gem0Session) OpenForTrades() (bool, error) {
	return _Gem0.Contract.OpenForTrades(&_Gem0.CallOpts)
}

// OpenForTrades is a free data retrieval call binding the contract method 0xa1b62797.
//
// Solidity: function openForTrades() view returns(bool)
func (_Gem0 *Gem0CallerSession) OpenForTrades() (bool, error) {
	return _Gem0.Contract.OpenForTrades(&_Gem0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Gem0 *Gem0Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Gem0.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Gem0 *Gem0Session) Owner() (common.Address, error) {
	return _Gem0.Contract.Owner(&_Gem0.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Gem0 *Gem0CallerSession) Owner() (common.Address, error) {
	return _Gem0.Contract.Owner(&_Gem0.CallOpts)
}

// PunkProxy is a free data retrieval call binding the contract method 0x6335f25e.
//
// Solidity: function punkProxy() view returns(address)
func (_Gem0 *Gem0Caller) PunkProxy(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Gem0.contract.Call(opts, &out, "punkProxy")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// PunkProxy is a free data retrieval call binding the contract method 0x6335f25e.
//
// Solidity: function punkProxy() view returns(address)
func (_Gem0 *Gem0Session) PunkProxy() (common.Address, error) {
	return _Gem0.Contract.PunkProxy(&_Gem0.CallOpts)
}

// PunkProxy is a free data retrieval call binding the contract method 0x6335f25e.
//
// Solidity: function punkProxy() view returns(address)
func (_Gem0 *Gem0CallerSession) PunkProxy() (common.Address, error) {
	return _Gem0.Contract.PunkProxy(&_Gem0.CallOpts)
}

// SponsoredMarkets is a free data retrieval call binding the contract method 0xe4dd4b8a.
//
// Solidity: function sponsoredMarkets(uint256 ) view returns(uint256 marketId, bool isActive)
func (_Gem0 *Gem0Caller) SponsoredMarkets(opts *bind.CallOpts, arg0 *big.Int) (struct {
	MarketId *big.Int
	IsActive bool
}, error) {
	var out []interface{}
	err := _Gem0.contract.Call(opts, &out, "sponsoredMarkets", arg0)

	outstruct := new(struct {
		MarketId *big.Int
		IsActive bool
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.MarketId = *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	outstruct.IsActive = *abi.ConvertType(out[1], new(bool)).(*bool)

	return *outstruct, err

}

// SponsoredMarkets is a free data retrieval call binding the contract method 0xe4dd4b8a.
//
// Solidity: function sponsoredMarkets(uint256 ) view returns(uint256 marketId, bool isActive)
func (_Gem0 *Gem0Session) SponsoredMarkets(arg0 *big.Int) (struct {
	MarketId *big.Int
	IsActive bool
}, error) {
	return _Gem0.Contract.SponsoredMarkets(&_Gem0.CallOpts, arg0)
}

// SponsoredMarkets is a free data retrieval call binding the contract method 0xe4dd4b8a.
//
// Solidity: function sponsoredMarkets(uint256 ) view returns(uint256 marketId, bool isActive)
func (_Gem0 *Gem0CallerSession) SponsoredMarkets(arg0 *big.Int) (struct {
	MarketId *big.Int
	IsActive bool
}, error) {
	return _Gem0.Contract.SponsoredMarkets(&_Gem0.CallOpts, arg0)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Gem0 *Gem0Caller) SupportsInterface(opts *bind.CallOpts, interfaceId [4]byte) (bool, error) {
	var out []interface{}
	err := _Gem0.contract.Call(opts, &out, "supportsInterface", interfaceId)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Gem0 *Gem0Session) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _Gem0.Contract.SupportsInterface(&_Gem0.CallOpts, interfaceId)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Gem0 *Gem0CallerSession) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _Gem0.Contract.SupportsInterface(&_Gem0.CallOpts, interfaceId)
}

// AddAffiliate is a paid mutator transaction binding the contract method 0x81ea4ea6.
//
// Solidity: function addAffiliate(address _affiliate) returns()
func (_Gem0 *Gem0Transactor) AddAffiliate(opts *bind.TransactOpts, _affiliate common.Address) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "addAffiliate", _affiliate)
}

// AddAffiliate is a paid mutator transaction binding the contract method 0x81ea4ea6.
//
// Solidity: function addAffiliate(address _affiliate) returns()
func (_Gem0 *Gem0Session) AddAffiliate(_affiliate common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.AddAffiliate(&_Gem0.TransactOpts, _affiliate)
}

// AddAffiliate is a paid mutator transaction binding the contract method 0x81ea4ea6.
//
// Solidity: function addAffiliate(address _affiliate) returns()
func (_Gem0 *Gem0TransactorSession) AddAffiliate(_affiliate common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.AddAffiliate(&_Gem0.TransactOpts, _affiliate)
}

// AddSponsoredMarket is a paid mutator transaction binding the contract method 0x3a5750b6.
//
// Solidity: function addSponsoredMarket(uint256 _marketId) returns()
func (_Gem0 *Gem0Transactor) AddSponsoredMarket(opts *bind.TransactOpts, _marketId *big.Int) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "addSponsoredMarket", _marketId)
}

// AddSponsoredMarket is a paid mutator transaction binding the contract method 0x3a5750b6.
//
// Solidity: function addSponsoredMarket(uint256 _marketId) returns()
func (_Gem0 *Gem0Session) AddSponsoredMarket(_marketId *big.Int) (*types.Transaction, error) {
	return _Gem0.Contract.AddSponsoredMarket(&_Gem0.TransactOpts, _marketId)
}

// AddSponsoredMarket is a paid mutator transaction binding the contract method 0x3a5750b6.
//
// Solidity: function addSponsoredMarket(uint256 _marketId) returns()
func (_Gem0 *Gem0TransactorSession) AddSponsoredMarket(_marketId *big.Int) (*types.Transaction, error) {
	return _Gem0.Contract.AddSponsoredMarket(&_Gem0.TransactOpts, _marketId)
}

// BatchBuyFromOpenSea is a paid mutator transaction binding the contract method 0x5eacc63a.
//
// Solidity: function batchBuyFromOpenSea((uint256,bytes)[] openseaTrades) payable returns()
func (_Gem0 *Gem0Transactor) BatchBuyFromOpenSea(opts *bind.TransactOpts, openseaTrades []GemSwapOpenseaTrades) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "batchBuyFromOpenSea", openseaTrades)
}

// BatchBuyFromOpenSea is a paid mutator transaction binding the contract method 0x5eacc63a.
//
// Solidity: function batchBuyFromOpenSea((uint256,bytes)[] openseaTrades) payable returns()
func (_Gem0 *Gem0Session) BatchBuyFromOpenSea(openseaTrades []GemSwapOpenseaTrades) (*types.Transaction, error) {
	return _Gem0.Contract.BatchBuyFromOpenSea(&_Gem0.TransactOpts, openseaTrades)
}

// BatchBuyFromOpenSea is a paid mutator transaction binding the contract method 0x5eacc63a.
//
// Solidity: function batchBuyFromOpenSea((uint256,bytes)[] openseaTrades) payable returns()
func (_Gem0 *Gem0TransactorSession) BatchBuyFromOpenSea(openseaTrades []GemSwapOpenseaTrades) (*types.Transaction, error) {
	return _Gem0.Contract.BatchBuyFromOpenSea(&_Gem0.TransactOpts, openseaTrades)
}

// BatchBuyWithERC20s is a paid mutator transaction binding the contract method 0x09ba153d.
//
// Solidity: function batchBuyWithERC20s((address[],uint256[]) erc20Details, (uint256,uint256,bytes)[] tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Gem0 *Gem0Transactor) BatchBuyWithERC20s(opts *bind.TransactOpts, erc20Details GemSwapERC20Details, tradeDetails []MarketRegistryTradeDetails, converstionDetails []GemSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "batchBuyWithERC20s", erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BatchBuyWithERC20s is a paid mutator transaction binding the contract method 0x09ba153d.
//
// Solidity: function batchBuyWithERC20s((address[],uint256[]) erc20Details, (uint256,uint256,bytes)[] tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Gem0 *Gem0Session) BatchBuyWithERC20s(erc20Details GemSwapERC20Details, tradeDetails []MarketRegistryTradeDetails, converstionDetails []GemSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.BatchBuyWithERC20s(&_Gem0.TransactOpts, erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BatchBuyWithERC20s is a paid mutator transaction binding the contract method 0x09ba153d.
//
// Solidity: function batchBuyWithERC20s((address[],uint256[]) erc20Details, (uint256,uint256,bytes)[] tradeDetails, (bytes)[] converstionDetails, address[] dustTokens) payable returns()
func (_Gem0 *Gem0TransactorSession) BatchBuyWithERC20s(erc20Details GemSwapERC20Details, tradeDetails []MarketRegistryTradeDetails, converstionDetails []GemSwapConverstionDetails, dustTokens []common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.BatchBuyWithERC20s(&_Gem0.TransactOpts, erc20Details, tradeDetails, converstionDetails, dustTokens)
}

// BatchBuyWithETH is a paid mutator transaction binding the contract method 0x9a2b8115.
//
// Solidity: function batchBuyWithETH((uint256,uint256,bytes)[] tradeDetails) payable returns()
func (_Gem0 *Gem0Transactor) BatchBuyWithETH(opts *bind.TransactOpts, tradeDetails []MarketRegistryTradeDetails) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "batchBuyWithETH", tradeDetails)
}

// BatchBuyWithETH is a paid mutator transaction binding the contract method 0x9a2b8115.
//
// Solidity: function batchBuyWithETH((uint256,uint256,bytes)[] tradeDetails) payable returns()
func (_Gem0 *Gem0Session) BatchBuyWithETH(tradeDetails []MarketRegistryTradeDetails) (*types.Transaction, error) {
	return _Gem0.Contract.BatchBuyWithETH(&_Gem0.TransactOpts, tradeDetails)
}

// BatchBuyWithETH is a paid mutator transaction binding the contract method 0x9a2b8115.
//
// Solidity: function batchBuyWithETH((uint256,uint256,bytes)[] tradeDetails) payable returns()
func (_Gem0 *Gem0TransactorSession) BatchBuyWithETH(tradeDetails []MarketRegistryTradeDetails) (*types.Transaction, error) {
	return _Gem0.Contract.BatchBuyWithETH(&_Gem0.TransactOpts, tradeDetails)
}

// CloseAllTrades is a paid mutator transaction binding the contract method 0xc5cadd7f.
//
// Solidity: function closeAllTrades() returns()
func (_Gem0 *Gem0Transactor) CloseAllTrades(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "closeAllTrades")
}

// CloseAllTrades is a paid mutator transaction binding the contract method 0xc5cadd7f.
//
// Solidity: function closeAllTrades() returns()
func (_Gem0 *Gem0Session) CloseAllTrades() (*types.Transaction, error) {
	return _Gem0.Contract.CloseAllTrades(&_Gem0.TransactOpts)
}

// CloseAllTrades is a paid mutator transaction binding the contract method 0xc5cadd7f.
//
// Solidity: function closeAllTrades() returns()
func (_Gem0 *Gem0TransactorSession) CloseAllTrades() (*types.Transaction, error) {
	return _Gem0.Contract.CloseAllTrades(&_Gem0.TransactOpts)
}

// MultiAssetSwap is a paid mutator transaction binding the contract method 0x186b100c.
//
// Solidity: function multiAssetSwap((address[],uint256[]) erc20Details, (address,address[],uint256[])[] erc721Details, (address,uint256[],uint256[])[] erc1155Details, (bytes)[] converstionDetails, (uint256,uint256,bytes)[] tradeDetails, address[] dustTokens, uint256[2] feeDetails) payable returns()
func (_Gem0 *Gem0Transactor) MultiAssetSwap(opts *bind.TransactOpts, erc20Details GemSwapERC20Details, erc721Details []SpecialTransferHelperERC721Details, erc1155Details []GemSwapERC1155Details, converstionDetails []GemSwapConverstionDetails, tradeDetails []MarketRegistryTradeDetails, dustTokens []common.Address, feeDetails [2]*big.Int) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "multiAssetSwap", erc20Details, erc721Details, erc1155Details, converstionDetails, tradeDetails, dustTokens, feeDetails)
}

// MultiAssetSwap is a paid mutator transaction binding the contract method 0x186b100c.
//
// Solidity: function multiAssetSwap((address[],uint256[]) erc20Details, (address,address[],uint256[])[] erc721Details, (address,uint256[],uint256[])[] erc1155Details, (bytes)[] converstionDetails, (uint256,uint256,bytes)[] tradeDetails, address[] dustTokens, uint256[2] feeDetails) payable returns()
func (_Gem0 *Gem0Session) MultiAssetSwap(erc20Details GemSwapERC20Details, erc721Details []SpecialTransferHelperERC721Details, erc1155Details []GemSwapERC1155Details, converstionDetails []GemSwapConverstionDetails, tradeDetails []MarketRegistryTradeDetails, dustTokens []common.Address, feeDetails [2]*big.Int) (*types.Transaction, error) {
	return _Gem0.Contract.MultiAssetSwap(&_Gem0.TransactOpts, erc20Details, erc721Details, erc1155Details, converstionDetails, tradeDetails, dustTokens, feeDetails)
}

// MultiAssetSwap is a paid mutator transaction binding the contract method 0x186b100c.
//
// Solidity: function multiAssetSwap((address[],uint256[]) erc20Details, (address,address[],uint256[])[] erc721Details, (address,uint256[],uint256[])[] erc1155Details, (bytes)[] converstionDetails, (uint256,uint256,bytes)[] tradeDetails, address[] dustTokens, uint256[2] feeDetails) payable returns()
func (_Gem0 *Gem0TransactorSession) MultiAssetSwap(erc20Details GemSwapERC20Details, erc721Details []SpecialTransferHelperERC721Details, erc1155Details []GemSwapERC1155Details, converstionDetails []GemSwapConverstionDetails, tradeDetails []MarketRegistryTradeDetails, dustTokens []common.Address, feeDetails [2]*big.Int) (*types.Transaction, error) {
	return _Gem0.Contract.MultiAssetSwap(&_Gem0.TransactOpts, erc20Details, erc721Details, erc1155Details, converstionDetails, tradeDetails, dustTokens, feeDetails)
}

// OnERC1155BatchReceived is a paid mutator transaction binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address , address , uint256[] , uint256[] , bytes ) returns(bytes4)
func (_Gem0 *Gem0Transactor) OnERC1155BatchReceived(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 []*big.Int, arg3 []*big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "onERC1155BatchReceived", arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155BatchReceived is a paid mutator transaction binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address , address , uint256[] , uint256[] , bytes ) returns(bytes4)
func (_Gem0 *Gem0Session) OnERC1155BatchReceived(arg0 common.Address, arg1 common.Address, arg2 []*big.Int, arg3 []*big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Gem0.Contract.OnERC1155BatchReceived(&_Gem0.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155BatchReceived is a paid mutator transaction binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address , address , uint256[] , uint256[] , bytes ) returns(bytes4)
func (_Gem0 *Gem0TransactorSession) OnERC1155BatchReceived(arg0 common.Address, arg1 common.Address, arg2 []*big.Int, arg3 []*big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Gem0.Contract.OnERC1155BatchReceived(&_Gem0.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155Received is a paid mutator transaction binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address , address , uint256 , uint256 , bytes ) returns(bytes4)
func (_Gem0 *Gem0Transactor) OnERC1155Received(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 *big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "onERC1155Received", arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155Received is a paid mutator transaction binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address , address , uint256 , uint256 , bytes ) returns(bytes4)
func (_Gem0 *Gem0Session) OnERC1155Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 *big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Gem0.Contract.OnERC1155Received(&_Gem0.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155Received is a paid mutator transaction binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address , address , uint256 , uint256 , bytes ) returns(bytes4)
func (_Gem0 *Gem0TransactorSession) OnERC1155Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 *big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Gem0.Contract.OnERC1155Received(&_Gem0.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Gem0 *Gem0Transactor) OnERC721Received(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "onERC721Received", arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Gem0 *Gem0Session) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Gem0.Contract.OnERC721Received(&_Gem0.TransactOpts, arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Gem0 *Gem0TransactorSession) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Gem0.Contract.OnERC721Received(&_Gem0.TransactOpts, arg0, arg1, arg2, arg3)
}

// OnERC721Received0 is a paid mutator transaction binding the contract method 0xf0b9e5ba.
//
// Solidity: function onERC721Received(address , uint256 , bytes ) returns(bytes4)
func (_Gem0 *Gem0Transactor) OnERC721Received0(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int, arg2 []byte) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "onERC721Received0", arg0, arg1, arg2)
}

// OnERC721Received0 is a paid mutator transaction binding the contract method 0xf0b9e5ba.
//
// Solidity: function onERC721Received(address , uint256 , bytes ) returns(bytes4)
func (_Gem0 *Gem0Session) OnERC721Received0(arg0 common.Address, arg1 *big.Int, arg2 []byte) (*types.Transaction, error) {
	return _Gem0.Contract.OnERC721Received0(&_Gem0.TransactOpts, arg0, arg1, arg2)
}

// OnERC721Received0 is a paid mutator transaction binding the contract method 0xf0b9e5ba.
//
// Solidity: function onERC721Received(address , uint256 , bytes ) returns(bytes4)
func (_Gem0 *Gem0TransactorSession) OnERC721Received0(arg0 common.Address, arg1 *big.Int, arg2 []byte) (*types.Transaction, error) {
	return _Gem0.Contract.OnERC721Received0(&_Gem0.TransactOpts, arg0, arg1, arg2)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Gem0 *Gem0Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Gem0 *Gem0Session) RenounceOwnership() (*types.Transaction, error) {
	return _Gem0.Contract.RenounceOwnership(&_Gem0.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Gem0 *Gem0TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Gem0.Contract.RenounceOwnership(&_Gem0.TransactOpts)
}

// RescueERC1155 is a paid mutator transaction binding the contract method 0xb7ce33a2.
//
// Solidity: function rescueERC1155(address asset, uint256[] ids, uint256[] amounts, address recipient) returns()
func (_Gem0 *Gem0Transactor) RescueERC1155(opts *bind.TransactOpts, asset common.Address, ids []*big.Int, amounts []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "rescueERC1155", asset, ids, amounts, recipient)
}

// RescueERC1155 is a paid mutator transaction binding the contract method 0xb7ce33a2.
//
// Solidity: function rescueERC1155(address asset, uint256[] ids, uint256[] amounts, address recipient) returns()
func (_Gem0 *Gem0Session) RescueERC1155(asset common.Address, ids []*big.Int, amounts []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.RescueERC1155(&_Gem0.TransactOpts, asset, ids, amounts, recipient)
}

// RescueERC1155 is a paid mutator transaction binding the contract method 0xb7ce33a2.
//
// Solidity: function rescueERC1155(address asset, uint256[] ids, uint256[] amounts, address recipient) returns()
func (_Gem0 *Gem0TransactorSession) RescueERC1155(asset common.Address, ids []*big.Int, amounts []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.RescueERC1155(&_Gem0.TransactOpts, asset, ids, amounts, recipient)
}

// RescueERC20 is a paid mutator transaction binding the contract method 0x5d799f87.
//
// Solidity: function rescueERC20(address asset, address recipient) returns()
func (_Gem0 *Gem0Transactor) RescueERC20(opts *bind.TransactOpts, asset common.Address, recipient common.Address) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "rescueERC20", asset, recipient)
}

// RescueERC20 is a paid mutator transaction binding the contract method 0x5d799f87.
//
// Solidity: function rescueERC20(address asset, address recipient) returns()
func (_Gem0 *Gem0Session) RescueERC20(asset common.Address, recipient common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.RescueERC20(&_Gem0.TransactOpts, asset, recipient)
}

// RescueERC20 is a paid mutator transaction binding the contract method 0x5d799f87.
//
// Solidity: function rescueERC20(address asset, address recipient) returns()
func (_Gem0 *Gem0TransactorSession) RescueERC20(asset common.Address, recipient common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.RescueERC20(&_Gem0.TransactOpts, asset, recipient)
}

// RescueERC721 is a paid mutator transaction binding the contract method 0x26e2dca2.
//
// Solidity: function rescueERC721(address asset, uint256[] ids, address recipient) returns()
func (_Gem0 *Gem0Transactor) RescueERC721(opts *bind.TransactOpts, asset common.Address, ids []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "rescueERC721", asset, ids, recipient)
}

// RescueERC721 is a paid mutator transaction binding the contract method 0x26e2dca2.
//
// Solidity: function rescueERC721(address asset, uint256[] ids, address recipient) returns()
func (_Gem0 *Gem0Session) RescueERC721(asset common.Address, ids []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.RescueERC721(&_Gem0.TransactOpts, asset, ids, recipient)
}

// RescueERC721 is a paid mutator transaction binding the contract method 0x26e2dca2.
//
// Solidity: function rescueERC721(address asset, uint256[] ids, address recipient) returns()
func (_Gem0 *Gem0TransactorSession) RescueERC721(asset common.Address, ids []*big.Int, recipient common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.RescueERC721(&_Gem0.TransactOpts, asset, ids, recipient)
}

// RescueETH is a paid mutator transaction binding the contract method 0x04824e70.
//
// Solidity: function rescueETH(address recipient) returns()
func (_Gem0 *Gem0Transactor) RescueETH(opts *bind.TransactOpts, recipient common.Address) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "rescueETH", recipient)
}

// RescueETH is a paid mutator transaction binding the contract method 0x04824e70.
//
// Solidity: function rescueETH(address recipient) returns()
func (_Gem0 *Gem0Session) RescueETH(recipient common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.RescueETH(&_Gem0.TransactOpts, recipient)
}

// RescueETH is a paid mutator transaction binding the contract method 0x04824e70.
//
// Solidity: function rescueETH(address recipient) returns()
func (_Gem0 *Gem0TransactorSession) RescueETH(recipient common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.RescueETH(&_Gem0.TransactOpts, recipient)
}

// SetBaseFees is a paid mutator transaction binding the contract method 0xb9277963.
//
// Solidity: function setBaseFees(uint256 _baseFees) returns()
func (_Gem0 *Gem0Transactor) SetBaseFees(opts *bind.TransactOpts, _baseFees *big.Int) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "setBaseFees", _baseFees)
}

// SetBaseFees is a paid mutator transaction binding the contract method 0xb9277963.
//
// Solidity: function setBaseFees(uint256 _baseFees) returns()
func (_Gem0 *Gem0Session) SetBaseFees(_baseFees *big.Int) (*types.Transaction, error) {
	return _Gem0.Contract.SetBaseFees(&_Gem0.TransactOpts, _baseFees)
}

// SetBaseFees is a paid mutator transaction binding the contract method 0xb9277963.
//
// Solidity: function setBaseFees(uint256 _baseFees) returns()
func (_Gem0 *Gem0TransactorSession) SetBaseFees(_baseFees *big.Int) (*types.Transaction, error) {
	return _Gem0.Contract.SetBaseFees(&_Gem0.TransactOpts, _baseFees)
}

// SetConverter is a paid mutator transaction binding the contract method 0xb19337a4.
//
// Solidity: function setConverter(address _converter) returns()
func (_Gem0 *Gem0Transactor) SetConverter(opts *bind.TransactOpts, _converter common.Address) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "setConverter", _converter)
}

// SetConverter is a paid mutator transaction binding the contract method 0xb19337a4.
//
// Solidity: function setConverter(address _converter) returns()
func (_Gem0 *Gem0Session) SetConverter(_converter common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.SetConverter(&_Gem0.TransactOpts, _converter)
}

// SetConverter is a paid mutator transaction binding the contract method 0xb19337a4.
//
// Solidity: function setConverter(address _converter) returns()
func (_Gem0 *Gem0TransactorSession) SetConverter(_converter common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.SetConverter(&_Gem0.TransactOpts, _converter)
}

// SetMarketRegistry is a paid mutator transaction binding the contract method 0xd8579704.
//
// Solidity: function setMarketRegistry(address _marketRegistry) returns()
func (_Gem0 *Gem0Transactor) SetMarketRegistry(opts *bind.TransactOpts, _marketRegistry common.Address) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "setMarketRegistry", _marketRegistry)
}

// SetMarketRegistry is a paid mutator transaction binding the contract method 0xd8579704.
//
// Solidity: function setMarketRegistry(address _marketRegistry) returns()
func (_Gem0 *Gem0Session) SetMarketRegistry(_marketRegistry common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.SetMarketRegistry(&_Gem0.TransactOpts, _marketRegistry)
}

// SetMarketRegistry is a paid mutator transaction binding the contract method 0xd8579704.
//
// Solidity: function setMarketRegistry(address _marketRegistry) returns()
func (_Gem0 *Gem0TransactorSession) SetMarketRegistry(_marketRegistry common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.SetMarketRegistry(&_Gem0.TransactOpts, _marketRegistry)
}

// SetOneTimeApproval is a paid mutator transaction binding the contract method 0x9f2ba09b.
//
// Solidity: function setOneTimeApproval(address token, address operator, uint256 amount) returns()
func (_Gem0 *Gem0Transactor) SetOneTimeApproval(opts *bind.TransactOpts, token common.Address, operator common.Address, amount *big.Int) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "setOneTimeApproval", token, operator, amount)
}

// SetOneTimeApproval is a paid mutator transaction binding the contract method 0x9f2ba09b.
//
// Solidity: function setOneTimeApproval(address token, address operator, uint256 amount) returns()
func (_Gem0 *Gem0Session) SetOneTimeApproval(token common.Address, operator common.Address, amount *big.Int) (*types.Transaction, error) {
	return _Gem0.Contract.SetOneTimeApproval(&_Gem0.TransactOpts, token, operator, amount)
}

// SetOneTimeApproval is a paid mutator transaction binding the contract method 0x9f2ba09b.
//
// Solidity: function setOneTimeApproval(address token, address operator, uint256 amount) returns()
func (_Gem0 *Gem0TransactorSession) SetOneTimeApproval(token common.Address, operator common.Address, amount *big.Int) (*types.Transaction, error) {
	return _Gem0.Contract.SetOneTimeApproval(&_Gem0.TransactOpts, token, operator, amount)
}

// SetOpenForFreeTrades is a paid mutator transaction binding the contract method 0xe6041f9a.
//
// Solidity: function setOpenForFreeTrades(bool _openForFreeTrades) returns()
func (_Gem0 *Gem0Transactor) SetOpenForFreeTrades(opts *bind.TransactOpts, _openForFreeTrades bool) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "setOpenForFreeTrades", _openForFreeTrades)
}

// SetOpenForFreeTrades is a paid mutator transaction binding the contract method 0xe6041f9a.
//
// Solidity: function setOpenForFreeTrades(bool _openForFreeTrades) returns()
func (_Gem0 *Gem0Session) SetOpenForFreeTrades(_openForFreeTrades bool) (*types.Transaction, error) {
	return _Gem0.Contract.SetOpenForFreeTrades(&_Gem0.TransactOpts, _openForFreeTrades)
}

// SetOpenForFreeTrades is a paid mutator transaction binding the contract method 0xe6041f9a.
//
// Solidity: function setOpenForFreeTrades(bool _openForFreeTrades) returns()
func (_Gem0 *Gem0TransactorSession) SetOpenForFreeTrades(_openForFreeTrades bool) (*types.Transaction, error) {
	return _Gem0.Contract.SetOpenForFreeTrades(&_Gem0.TransactOpts, _openForFreeTrades)
}

// SetOpenForTrades is a paid mutator transaction binding the contract method 0x83206e80.
//
// Solidity: function setOpenForTrades(bool _openForTrades) returns()
func (_Gem0 *Gem0Transactor) SetOpenForTrades(opts *bind.TransactOpts, _openForTrades bool) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "setOpenForTrades", _openForTrades)
}

// SetOpenForTrades is a paid mutator transaction binding the contract method 0x83206e80.
//
// Solidity: function setOpenForTrades(bool _openForTrades) returns()
func (_Gem0 *Gem0Session) SetOpenForTrades(_openForTrades bool) (*types.Transaction, error) {
	return _Gem0.Contract.SetOpenForTrades(&_Gem0.TransactOpts, _openForTrades)
}

// SetOpenForTrades is a paid mutator transaction binding the contract method 0x83206e80.
//
// Solidity: function setOpenForTrades(bool _openForTrades) returns()
func (_Gem0 *Gem0TransactorSession) SetOpenForTrades(_openForTrades bool) (*types.Transaction, error) {
	return _Gem0.Contract.SetOpenForTrades(&_Gem0.TransactOpts, _openForTrades)
}

// SetUp is a paid mutator transaction binding the contract method 0x0a9254e4.
//
// Solidity: function setUp() returns()
func (_Gem0 *Gem0Transactor) SetUp(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "setUp")
}

// SetUp is a paid mutator transaction binding the contract method 0x0a9254e4.
//
// Solidity: function setUp() returns()
func (_Gem0 *Gem0Session) SetUp() (*types.Transaction, error) {
	return _Gem0.Contract.SetUp(&_Gem0.TransactOpts)
}

// SetUp is a paid mutator transaction binding the contract method 0x0a9254e4.
//
// Solidity: function setUp() returns()
func (_Gem0 *Gem0TransactorSession) SetUp() (*types.Transaction, error) {
	return _Gem0.Contract.SetUp(&_Gem0.TransactOpts)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Gem0 *Gem0Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Gem0 *Gem0Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.TransferOwnership(&_Gem0.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Gem0 *Gem0TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.TransferOwnership(&_Gem0.TransactOpts, newOwner)
}

// UpdateAffiliate is a paid mutator transaction binding the contract method 0x565528d7.
//
// Solidity: function updateAffiliate(uint256 _affiliateIndex, address _affiliate, bool _IsActive) returns()
func (_Gem0 *Gem0Transactor) UpdateAffiliate(opts *bind.TransactOpts, _affiliateIndex *big.Int, _affiliate common.Address, _IsActive bool) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "updateAffiliate", _affiliateIndex, _affiliate, _IsActive)
}

// UpdateAffiliate is a paid mutator transaction binding the contract method 0x565528d7.
//
// Solidity: function updateAffiliate(uint256 _affiliateIndex, address _affiliate, bool _IsActive) returns()
func (_Gem0 *Gem0Session) UpdateAffiliate(_affiliateIndex *big.Int, _affiliate common.Address, _IsActive bool) (*types.Transaction, error) {
	return _Gem0.Contract.UpdateAffiliate(&_Gem0.TransactOpts, _affiliateIndex, _affiliate, _IsActive)
}

// UpdateAffiliate is a paid mutator transaction binding the contract method 0x565528d7.
//
// Solidity: function updateAffiliate(uint256 _affiliateIndex, address _affiliate, bool _IsActive) returns()
func (_Gem0 *Gem0TransactorSession) UpdateAffiliate(_affiliateIndex *big.Int, _affiliate common.Address, _IsActive bool) (*types.Transaction, error) {
	return _Gem0.Contract.UpdateAffiliate(&_Gem0.TransactOpts, _affiliateIndex, _affiliate, _IsActive)
}

// UpdateGuardian is a paid mutator transaction binding the contract method 0xfc525395.
//
// Solidity: function updateGuardian(address _guardian) returns()
func (_Gem0 *Gem0Transactor) UpdateGuardian(opts *bind.TransactOpts, _guardian common.Address) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "updateGuardian", _guardian)
}

// UpdateGuardian is a paid mutator transaction binding the contract method 0xfc525395.
//
// Solidity: function updateGuardian(address _guardian) returns()
func (_Gem0 *Gem0Session) UpdateGuardian(_guardian common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.UpdateGuardian(&_Gem0.TransactOpts, _guardian)
}

// UpdateGuardian is a paid mutator transaction binding the contract method 0xfc525395.
//
// Solidity: function updateGuardian(address _guardian) returns()
func (_Gem0 *Gem0TransactorSession) UpdateGuardian(_guardian common.Address) (*types.Transaction, error) {
	return _Gem0.Contract.UpdateGuardian(&_Gem0.TransactOpts, _guardian)
}

// UpdateSponsoredMarket is a paid mutator transaction binding the contract method 0xccf3dc82.
//
// Solidity: function updateSponsoredMarket(uint256 _marketIndex, uint256 _marketId, bool _isActive) returns()
func (_Gem0 *Gem0Transactor) UpdateSponsoredMarket(opts *bind.TransactOpts, _marketIndex *big.Int, _marketId *big.Int, _isActive bool) (*types.Transaction, error) {
	return _Gem0.contract.Transact(opts, "updateSponsoredMarket", _marketIndex, _marketId, _isActive)
}

// UpdateSponsoredMarket is a paid mutator transaction binding the contract method 0xccf3dc82.
//
// Solidity: function updateSponsoredMarket(uint256 _marketIndex, uint256 _marketId, bool _isActive) returns()
func (_Gem0 *Gem0Session) UpdateSponsoredMarket(_marketIndex *big.Int, _marketId *big.Int, _isActive bool) (*types.Transaction, error) {
	return _Gem0.Contract.UpdateSponsoredMarket(&_Gem0.TransactOpts, _marketIndex, _marketId, _isActive)
}

// UpdateSponsoredMarket is a paid mutator transaction binding the contract method 0xccf3dc82.
//
// Solidity: function updateSponsoredMarket(uint256 _marketIndex, uint256 _marketId, bool _isActive) returns()
func (_Gem0 *Gem0TransactorSession) UpdateSponsoredMarket(_marketIndex *big.Int, _marketId *big.Int, _isActive bool) (*types.Transaction, error) {
	return _Gem0.Contract.UpdateSponsoredMarket(&_Gem0.TransactOpts, _marketIndex, _marketId, _isActive)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Gem0 *Gem0Transactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Gem0.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Gem0 *Gem0Session) Receive() (*types.Transaction, error) {
	return _Gem0.Contract.Receive(&_Gem0.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_Gem0 *Gem0TransactorSession) Receive() (*types.Transaction, error) {
	return _Gem0.Contract.Receive(&_Gem0.TransactOpts)
}

// Gem0OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Gem0 contract.
type Gem0OwnershipTransferredIterator struct {
	Event *Gem0OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Gem0OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Gem0OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Gem0OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Gem0OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Gem0OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Gem0OwnershipTransferred represents a OwnershipTransferred event raised by the Gem0 contract.
type Gem0OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Gem0 *Gem0Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Gem0OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Gem0.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Gem0OwnershipTransferredIterator{contract: _Gem0.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Gem0 *Gem0Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Gem0OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Gem0.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Gem0OwnershipTransferred)
				if err := _Gem0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Gem0 *Gem0Filterer) ParseOwnershipTransferred(log types.Log) (*Gem0OwnershipTransferred, error) {
	event := new(Gem0OwnershipTransferred)
	if err := _Gem0.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
