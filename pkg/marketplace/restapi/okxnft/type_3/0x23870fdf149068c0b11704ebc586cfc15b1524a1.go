// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package okxnft_3

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Okxnft3MetaData contains all meta data concerning the Okxnft3 contract.
var Okxnft3MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_implementation\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"ProxyOwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"implementation\",\"type\":\"address\"}],\"name\":\"Upgraded\",\"type\":\"event\"},{\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"fallback\"},{\"constant\":true,\"inputs\":[],\"name\":\"implementation\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"impl\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"proxyOwner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"address\",\"name\":\"_newOwner\",\"type\":\"address\"}],\"name\":\"transferProxyOwnership\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"address\",\"name\":\"_implementation\",\"type\":\"address\"}],\"name\":\"upgradeTo\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// Okxnft3ABI is the input ABI used to generate the binding from.
// Deprecated: Use Okxnft3MetaData.ABI instead.
var Okxnft3ABI = Okxnft3MetaData.ABI

// Okxnft3 is an auto generated Go binding around an Ethereum contract.
type Okxnft3 struct {
	Okxnft3Caller     // Read-only binding to the contract
	Okxnft3Transactor // Write-only binding to the contract
	Okxnft3Filterer   // Log filterer for contract events
}

// Okxnft3Caller is an auto generated read-only Go binding around an Ethereum contract.
type Okxnft3Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Okxnft3Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Okxnft3Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Okxnft3Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Okxnft3Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Okxnft3Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Okxnft3Session struct {
	Contract     *Okxnft3          // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Okxnft3CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Okxnft3CallerSession struct {
	Contract *Okxnft3Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts  // Call options to use throughout this session
}

// Okxnft3TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Okxnft3TransactorSession struct {
	Contract     *Okxnft3Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts  // Transaction auth options to use throughout this session
}

// Okxnft3Raw is an auto generated low-level Go binding around an Ethereum contract.
type Okxnft3Raw struct {
	Contract *Okxnft3 // Generic contract binding to access the raw methods on
}

// Okxnft3CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Okxnft3CallerRaw struct {
	Contract *Okxnft3Caller // Generic read-only contract binding to access the raw methods on
}

// Okxnft3TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Okxnft3TransactorRaw struct {
	Contract *Okxnft3Transactor // Generic write-only contract binding to access the raw methods on
}

// NewOkxnft3 creates a new instance of Okxnft3, bound to a specific deployed contract.
func NewOkxnft3(address common.Address, backend bind.ContractBackend) (*Okxnft3, error) {
	contract, err := bindOkxnft3(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Okxnft3{Okxnft3Caller: Okxnft3Caller{contract: contract}, Okxnft3Transactor: Okxnft3Transactor{contract: contract}, Okxnft3Filterer: Okxnft3Filterer{contract: contract}}, nil
}

// NewOkxnft3Caller creates a new read-only instance of Okxnft3, bound to a specific deployed contract.
func NewOkxnft3Caller(address common.Address, caller bind.ContractCaller) (*Okxnft3Caller, error) {
	contract, err := bindOkxnft3(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Okxnft3Caller{contract: contract}, nil
}

// NewOkxnft3Transactor creates a new write-only instance of Okxnft3, bound to a specific deployed contract.
func NewOkxnft3Transactor(address common.Address, transactor bind.ContractTransactor) (*Okxnft3Transactor, error) {
	contract, err := bindOkxnft3(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Okxnft3Transactor{contract: contract}, nil
}

// NewOkxnft3Filterer creates a new log filterer instance of Okxnft3, bound to a specific deployed contract.
func NewOkxnft3Filterer(address common.Address, filterer bind.ContractFilterer) (*Okxnft3Filterer, error) {
	contract, err := bindOkxnft3(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Okxnft3Filterer{contract: contract}, nil
}

// bindOkxnft3 binds a generic wrapper to an already deployed contract.
func bindOkxnft3(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Okxnft3ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Okxnft3 *Okxnft3Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Okxnft3.Contract.Okxnft3Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Okxnft3 *Okxnft3Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Okxnft3.Contract.Okxnft3Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Okxnft3 *Okxnft3Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Okxnft3.Contract.Okxnft3Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Okxnft3 *Okxnft3CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Okxnft3.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Okxnft3 *Okxnft3TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Okxnft3.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Okxnft3 *Okxnft3TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Okxnft3.Contract.contract.Transact(opts, method, params...)
}

// Implementation is a free data retrieval call binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() view returns(address impl)
func (_Okxnft3 *Okxnft3Caller) Implementation(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Okxnft3.contract.Call(opts, &out, "implementation")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Implementation is a free data retrieval call binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() view returns(address impl)
func (_Okxnft3 *Okxnft3Session) Implementation() (common.Address, error) {
	return _Okxnft3.Contract.Implementation(&_Okxnft3.CallOpts)
}

// Implementation is a free data retrieval call binding the contract method 0x5c60da1b.
//
// Solidity: function implementation() view returns(address impl)
func (_Okxnft3 *Okxnft3CallerSession) Implementation() (common.Address, error) {
	return _Okxnft3.Contract.Implementation(&_Okxnft3.CallOpts)
}

// ProxyOwner is a free data retrieval call binding the contract method 0x025313a2.
//
// Solidity: function proxyOwner() view returns(address owner)
func (_Okxnft3 *Okxnft3Caller) ProxyOwner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Okxnft3.contract.Call(opts, &out, "proxyOwner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// ProxyOwner is a free data retrieval call binding the contract method 0x025313a2.
//
// Solidity: function proxyOwner() view returns(address owner)
func (_Okxnft3 *Okxnft3Session) ProxyOwner() (common.Address, error) {
	return _Okxnft3.Contract.ProxyOwner(&_Okxnft3.CallOpts)
}

// ProxyOwner is a free data retrieval call binding the contract method 0x025313a2.
//
// Solidity: function proxyOwner() view returns(address owner)
func (_Okxnft3 *Okxnft3CallerSession) ProxyOwner() (common.Address, error) {
	return _Okxnft3.Contract.ProxyOwner(&_Okxnft3.CallOpts)
}

// TransferProxyOwnership is a paid mutator transaction binding the contract method 0xf1739cae.
//
// Solidity: function transferProxyOwnership(address _newOwner) returns()
func (_Okxnft3 *Okxnft3Transactor) TransferProxyOwnership(opts *bind.TransactOpts, _newOwner common.Address) (*types.Transaction, error) {
	return _Okxnft3.contract.Transact(opts, "transferProxyOwnership", _newOwner)
}

// TransferProxyOwnership is a paid mutator transaction binding the contract method 0xf1739cae.
//
// Solidity: function transferProxyOwnership(address _newOwner) returns()
func (_Okxnft3 *Okxnft3Session) TransferProxyOwnership(_newOwner common.Address) (*types.Transaction, error) {
	return _Okxnft3.Contract.TransferProxyOwnership(&_Okxnft3.TransactOpts, _newOwner)
}

// TransferProxyOwnership is a paid mutator transaction binding the contract method 0xf1739cae.
//
// Solidity: function transferProxyOwnership(address _newOwner) returns()
func (_Okxnft3 *Okxnft3TransactorSession) TransferProxyOwnership(_newOwner common.Address) (*types.Transaction, error) {
	return _Okxnft3.Contract.TransferProxyOwnership(&_Okxnft3.TransactOpts, _newOwner)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address _implementation) returns()
func (_Okxnft3 *Okxnft3Transactor) UpgradeTo(opts *bind.TransactOpts, _implementation common.Address) (*types.Transaction, error) {
	return _Okxnft3.contract.Transact(opts, "upgradeTo", _implementation)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address _implementation) returns()
func (_Okxnft3 *Okxnft3Session) UpgradeTo(_implementation common.Address) (*types.Transaction, error) {
	return _Okxnft3.Contract.UpgradeTo(&_Okxnft3.TransactOpts, _implementation)
}

// UpgradeTo is a paid mutator transaction binding the contract method 0x3659cfe6.
//
// Solidity: function upgradeTo(address _implementation) returns()
func (_Okxnft3 *Okxnft3TransactorSession) UpgradeTo(_implementation common.Address) (*types.Transaction, error) {
	return _Okxnft3.Contract.UpgradeTo(&_Okxnft3.TransactOpts, _implementation)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Okxnft3 *Okxnft3Transactor) Fallback(opts *bind.TransactOpts, calldata []byte) (*types.Transaction, error) {
	return _Okxnft3.contract.RawTransact(opts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Okxnft3 *Okxnft3Session) Fallback(calldata []byte) (*types.Transaction, error) {
	return _Okxnft3.Contract.Fallback(&_Okxnft3.TransactOpts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() payable returns()
func (_Okxnft3 *Okxnft3TransactorSession) Fallback(calldata []byte) (*types.Transaction, error) {
	return _Okxnft3.Contract.Fallback(&_Okxnft3.TransactOpts, calldata)
}

// Okxnft3ProxyOwnershipTransferredIterator is returned from FilterProxyOwnershipTransferred and is used to iterate over the raw logs and unpacked data for ProxyOwnershipTransferred events raised by the Okxnft3 contract.
type Okxnft3ProxyOwnershipTransferredIterator struct {
	Event *Okxnft3ProxyOwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Okxnft3ProxyOwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Okxnft3ProxyOwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Okxnft3ProxyOwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Okxnft3ProxyOwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Okxnft3ProxyOwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Okxnft3ProxyOwnershipTransferred represents a ProxyOwnershipTransferred event raised by the Okxnft3 contract.
type Okxnft3ProxyOwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterProxyOwnershipTransferred is a free log retrieval operation binding the contract event 0x5a3e66efaa1e445ebd894728a69d6959842ea1e97bd79b892797106e270efcd9.
//
// Solidity: event ProxyOwnershipTransferred(address previousOwner, address newOwner)
func (_Okxnft3 *Okxnft3Filterer) FilterProxyOwnershipTransferred(opts *bind.FilterOpts) (*Okxnft3ProxyOwnershipTransferredIterator, error) {

	logs, sub, err := _Okxnft3.contract.FilterLogs(opts, "ProxyOwnershipTransferred")
	if err != nil {
		return nil, err
	}
	return &Okxnft3ProxyOwnershipTransferredIterator{contract: _Okxnft3.contract, event: "ProxyOwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchProxyOwnershipTransferred is a free log subscription operation binding the contract event 0x5a3e66efaa1e445ebd894728a69d6959842ea1e97bd79b892797106e270efcd9.
//
// Solidity: event ProxyOwnershipTransferred(address previousOwner, address newOwner)
func (_Okxnft3 *Okxnft3Filterer) WatchProxyOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Okxnft3ProxyOwnershipTransferred) (event.Subscription, error) {

	logs, sub, err := _Okxnft3.contract.WatchLogs(opts, "ProxyOwnershipTransferred")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Okxnft3ProxyOwnershipTransferred)
				if err := _Okxnft3.contract.UnpackLog(event, "ProxyOwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseProxyOwnershipTransferred is a log parse operation binding the contract event 0x5a3e66efaa1e445ebd894728a69d6959842ea1e97bd79b892797106e270efcd9.
//
// Solidity: event ProxyOwnershipTransferred(address previousOwner, address newOwner)
func (_Okxnft3 *Okxnft3Filterer) ParseProxyOwnershipTransferred(log types.Log) (*Okxnft3ProxyOwnershipTransferred, error) {
	event := new(Okxnft3ProxyOwnershipTransferred)
	if err := _Okxnft3.contract.UnpackLog(event, "ProxyOwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Okxnft3UpgradedIterator is returned from FilterUpgraded and is used to iterate over the raw logs and unpacked data for Upgraded events raised by the Okxnft3 contract.
type Okxnft3UpgradedIterator struct {
	Event *Okxnft3Upgraded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Okxnft3UpgradedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Okxnft3Upgraded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Okxnft3Upgraded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Okxnft3UpgradedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Okxnft3UpgradedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Okxnft3Upgraded represents a Upgraded event raised by the Okxnft3 contract.
type Okxnft3Upgraded struct {
	Implementation common.Address
	Raw            types.Log // Blockchain specific contextual infos
}

// FilterUpgraded is a free log retrieval operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Okxnft3 *Okxnft3Filterer) FilterUpgraded(opts *bind.FilterOpts, implementation []common.Address) (*Okxnft3UpgradedIterator, error) {

	var implementationRule []interface{}
	for _, implementationItem := range implementation {
		implementationRule = append(implementationRule, implementationItem)
	}

	logs, sub, err := _Okxnft3.contract.FilterLogs(opts, "Upgraded", implementationRule)
	if err != nil {
		return nil, err
	}
	return &Okxnft3UpgradedIterator{contract: _Okxnft3.contract, event: "Upgraded", logs: logs, sub: sub}, nil
}

// WatchUpgraded is a free log subscription operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Okxnft3 *Okxnft3Filterer) WatchUpgraded(opts *bind.WatchOpts, sink chan<- *Okxnft3Upgraded, implementation []common.Address) (event.Subscription, error) {

	var implementationRule []interface{}
	for _, implementationItem := range implementation {
		implementationRule = append(implementationRule, implementationItem)
	}

	logs, sub, err := _Okxnft3.contract.WatchLogs(opts, "Upgraded", implementationRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Okxnft3Upgraded)
				if err := _Okxnft3.contract.UnpackLog(event, "Upgraded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUpgraded is a log parse operation binding the contract event 0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b.
//
// Solidity: event Upgraded(address indexed implementation)
func (_Okxnft3 *Okxnft3Filterer) ParseUpgraded(log types.Log) (*Okxnft3Upgraded, error) {
	event := new(Okxnft3Upgraded)
	if err := _Okxnft3.contract.UnpackLog(event, "Upgraded", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
