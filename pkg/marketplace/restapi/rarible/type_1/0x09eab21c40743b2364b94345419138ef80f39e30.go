// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package rarible_1

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// ExchangeDomainV1Asset is an auto generated low-level Go binding around an user-defined struct.
type ExchangeDomainV1Asset struct {
	Token     common.Address
	TokenId   *big.Int
	AssetType uint8
}

// ExchangeDomainV1Order is an auto generated low-level Go binding around an user-defined struct.
type ExchangeDomainV1Order struct {
	Key       ExchangeDomainV1OrderKey
	Selling   *big.Int
	Buying    *big.Int
	SellerFee *big.Int
}

// ExchangeDomainV1OrderKey is an auto generated low-level Go binding around an user-defined struct.
type ExchangeDomainV1OrderKey struct {
	Owner     common.Address
	Salt      *big.Int
	SellAsset ExchangeDomainV1Asset
	BuyAsset  ExchangeDomainV1Asset
}

// ExchangeDomainV1Sig is an auto generated low-level Go binding around an user-defined struct.
type ExchangeDomainV1Sig struct {
	V uint8
	R [32]byte
	S [32]byte
}

// Rarible1MetaData contains all meta data concerning the Rarible1 contract.
var Rarible1MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"contractTransferProxy\",\"name\":\"_transferProxy\",\"type\":\"address\"},{\"internalType\":\"contractTransferProxyForDeprecated\",\"name\":\"_transferProxyForDeprecated\",\"type\":\"address\"},{\"internalType\":\"contractERC20TransferProxy\",\"name\":\"_erc20TransferProxy\",\"type\":\"address\"},{\"internalType\":\"contractExchangeStateV1\",\"name\":\"_state\",\"type\":\"address\"},{\"internalType\":\"contractExchangeOrdersHolderV1\",\"name\":\"_ordersHolder\",\"type\":\"address\"},{\"internalType\":\"addresspayable\",\"name\":\"_beneficiary\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_buyerFeeSigner\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"sellToken\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"sellTokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"sellValue\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"buyToken\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"buyTokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"buyValue\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"}],\"name\":\"Buy\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"sellToken\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"sellTokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"buyToken\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"buyTokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"}],\"name\":\"Cancel\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"constant\":true,\"inputs\":[],\"name\":\"beneficiary\",\"outputs\":[{\"internalType\":\"addresspayable\",\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"buyerFeeSigner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"enumExchangeDomainV1.AssetType\",\"name\":\"assetType\",\"type\":\"uint8\"}],\"internalType\":\"structExchangeDomainV1.Asset\",\"name\":\"sellAsset\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"enumExchangeDomainV1.AssetType\",\"name\":\"assetType\",\"type\":\"uint8\"}],\"internalType\":\"structExchangeDomainV1.Asset\",\"name\":\"buyAsset\",\"type\":\"tuple\"}],\"internalType\":\"structExchangeDomainV1.OrderKey\",\"name\":\"key\",\"type\":\"tuple\"}],\"name\":\"cancel\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"erc20TransferProxy\",\"outputs\":[{\"internalType\":\"contractERC20TransferProxy\",\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"components\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"enumExchangeDomainV1.AssetType\",\"name\":\"assetType\",\"type\":\"uint8\"}],\"internalType\":\"structExchangeDomainV1.Asset\",\"name\":\"sellAsset\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"enumExchangeDomainV1.AssetType\",\"name\":\"assetType\",\"type\":\"uint8\"}],\"internalType\":\"structExchangeDomainV1.Asset\",\"name\":\"buyAsset\",\"type\":\"tuple\"}],\"internalType\":\"structExchangeDomainV1.OrderKey\",\"name\":\"key\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"selling\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"buying\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"sellerFee\",\"type\":\"uint256\"}],\"internalType\":\"structExchangeDomainV1.Order\",\"name\":\"order\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"internalType\":\"structExchangeDomainV1.Sig\",\"name\":\"sig\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"buyerFee\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"internalType\":\"structExchangeDomainV1.Sig\",\"name\":\"buyerFeeSig\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"}],\"name\":\"exchange\",\"outputs\":[],\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"isOwner\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"ordersHolder\",\"outputs\":[{\"internalType\":\"contractExchangeOrdersHolderV1\",\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"components\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"enumExchangeDomainV1.AssetType\",\"name\":\"assetType\",\"type\":\"uint8\"}],\"internalType\":\"structExchangeDomainV1.Asset\",\"name\":\"sellAsset\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"enumExchangeDomainV1.AssetType\",\"name\":\"assetType\",\"type\":\"uint8\"}],\"internalType\":\"structExchangeDomainV1.Asset\",\"name\":\"buyAsset\",\"type\":\"tuple\"}],\"internalType\":\"structExchangeDomainV1.OrderKey\",\"name\":\"key\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"selling\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"buying\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"sellerFee\",\"type\":\"uint256\"}],\"internalType\":\"structExchangeDomainV1.Order\",\"name\":\"order\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"fee\",\"type\":\"uint256\"}],\"name\":\"prepareBuyerFeeMessage\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"pure\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"components\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"salt\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"enumExchangeDomainV1.AssetType\",\"name\":\"assetType\",\"type\":\"uint8\"}],\"internalType\":\"structExchangeDomainV1.Asset\",\"name\":\"sellAsset\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"enumExchangeDomainV1.AssetType\",\"name\":\"assetType\",\"type\":\"uint8\"}],\"internalType\":\"structExchangeDomainV1.Asset\",\"name\":\"buyAsset\",\"type\":\"tuple\"}],\"internalType\":\"structExchangeDomainV1.OrderKey\",\"name\":\"key\",\"type\":\"tuple\"},{\"internalType\":\"uint256\",\"name\":\"selling\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"buying\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"sellerFee\",\"type\":\"uint256\"}],\"internalType\":\"structExchangeDomainV1.Order\",\"name\":\"order\",\"type\":\"tuple\"}],\"name\":\"prepareMessage\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"pure\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"addresspayable\",\"name\":\"newBeneficiary\",\"type\":\"address\"}],\"name\":\"setBeneficiary\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"address\",\"name\":\"newBuyerFeeSigner\",\"type\":\"address\"}],\"name\":\"setBuyerFeeSigner\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"state\",\"outputs\":[{\"internalType\":\"contractExchangeStateV1\",\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"transferProxy\",\"outputs\":[{\"internalType\":\"contractTransferProxy\",\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"transferProxyForDeprecated\",\"outputs\":[{\"internalType\":\"contractTransferProxyForDeprecated\",\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"}]",
}

// Rarible1ABI is the input ABI used to generate the binding from.
// Deprecated: Use Rarible1MetaData.ABI instead.
var Rarible1ABI = Rarible1MetaData.ABI

// Rarible1 is an auto generated Go binding around an Ethereum contract.
type Rarible1 struct {
	Rarible1Caller     // Read-only binding to the contract
	Rarible1Transactor // Write-only binding to the contract
	Rarible1Filterer   // Log filterer for contract events
}

// Rarible1Caller is an auto generated read-only Go binding around an Ethereum contract.
type Rarible1Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Rarible1Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Rarible1Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Rarible1Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Rarible1Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Rarible1Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Rarible1Session struct {
	Contract     *Rarible1         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Rarible1CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Rarible1CallerSession struct {
	Contract *Rarible1Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// Rarible1TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Rarible1TransactorSession struct {
	Contract     *Rarible1Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// Rarible1Raw is an auto generated low-level Go binding around an Ethereum contract.
type Rarible1Raw struct {
	Contract *Rarible1 // Generic contract binding to access the raw methods on
}

// Rarible1CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Rarible1CallerRaw struct {
	Contract *Rarible1Caller // Generic read-only contract binding to access the raw methods on
}

// Rarible1TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Rarible1TransactorRaw struct {
	Contract *Rarible1Transactor // Generic write-only contract binding to access the raw methods on
}

// NewRarible1 creates a new instance of Rarible1, bound to a specific deployed contract.
func NewRarible1(address common.Address, backend bind.ContractBackend) (*Rarible1, error) {
	contract, err := bindRarible1(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Rarible1{Rarible1Caller: Rarible1Caller{contract: contract}, Rarible1Transactor: Rarible1Transactor{contract: contract}, Rarible1Filterer: Rarible1Filterer{contract: contract}}, nil
}

// NewRarible1Caller creates a new read-only instance of Rarible1, bound to a specific deployed contract.
func NewRarible1Caller(address common.Address, caller bind.ContractCaller) (*Rarible1Caller, error) {
	contract, err := bindRarible1(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Rarible1Caller{contract: contract}, nil
}

// NewRarible1Transactor creates a new write-only instance of Rarible1, bound to a specific deployed contract.
func NewRarible1Transactor(address common.Address, transactor bind.ContractTransactor) (*Rarible1Transactor, error) {
	contract, err := bindRarible1(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Rarible1Transactor{contract: contract}, nil
}

// NewRarible1Filterer creates a new log filterer instance of Rarible1, bound to a specific deployed contract.
func NewRarible1Filterer(address common.Address, filterer bind.ContractFilterer) (*Rarible1Filterer, error) {
	contract, err := bindRarible1(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Rarible1Filterer{contract: contract}, nil
}

// bindRarible1 binds a generic wrapper to an already deployed contract.
func bindRarible1(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Rarible1ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Rarible1 *Rarible1Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Rarible1.Contract.Rarible1Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Rarible1 *Rarible1Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Rarible1.Contract.Rarible1Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Rarible1 *Rarible1Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Rarible1.Contract.Rarible1Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Rarible1 *Rarible1CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Rarible1.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Rarible1 *Rarible1TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Rarible1.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Rarible1 *Rarible1TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Rarible1.Contract.contract.Transact(opts, method, params...)
}

// Beneficiary is a free data retrieval call binding the contract method 0x38af3eed.
//
// Solidity: function beneficiary() view returns(address)
func (_Rarible1 *Rarible1Caller) Beneficiary(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Rarible1.contract.Call(opts, &out, "beneficiary")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Beneficiary is a free data retrieval call binding the contract method 0x38af3eed.
//
// Solidity: function beneficiary() view returns(address)
func (_Rarible1 *Rarible1Session) Beneficiary() (common.Address, error) {
	return _Rarible1.Contract.Beneficiary(&_Rarible1.CallOpts)
}

// Beneficiary is a free data retrieval call binding the contract method 0x38af3eed.
//
// Solidity: function beneficiary() view returns(address)
func (_Rarible1 *Rarible1CallerSession) Beneficiary() (common.Address, error) {
	return _Rarible1.Contract.Beneficiary(&_Rarible1.CallOpts)
}

// BuyerFeeSigner is a free data retrieval call binding the contract method 0x4df97bc5.
//
// Solidity: function buyerFeeSigner() view returns(address)
func (_Rarible1 *Rarible1Caller) BuyerFeeSigner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Rarible1.contract.Call(opts, &out, "buyerFeeSigner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// BuyerFeeSigner is a free data retrieval call binding the contract method 0x4df97bc5.
//
// Solidity: function buyerFeeSigner() view returns(address)
func (_Rarible1 *Rarible1Session) BuyerFeeSigner() (common.Address, error) {
	return _Rarible1.Contract.BuyerFeeSigner(&_Rarible1.CallOpts)
}

// BuyerFeeSigner is a free data retrieval call binding the contract method 0x4df97bc5.
//
// Solidity: function buyerFeeSigner() view returns(address)
func (_Rarible1 *Rarible1CallerSession) BuyerFeeSigner() (common.Address, error) {
	return _Rarible1.Contract.BuyerFeeSigner(&_Rarible1.CallOpts)
}

// Erc20TransferProxy is a free data retrieval call binding the contract method 0xfee03e9e.
//
// Solidity: function erc20TransferProxy() view returns(address)
func (_Rarible1 *Rarible1Caller) Erc20TransferProxy(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Rarible1.contract.Call(opts, &out, "erc20TransferProxy")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Erc20TransferProxy is a free data retrieval call binding the contract method 0xfee03e9e.
//
// Solidity: function erc20TransferProxy() view returns(address)
func (_Rarible1 *Rarible1Session) Erc20TransferProxy() (common.Address, error) {
	return _Rarible1.Contract.Erc20TransferProxy(&_Rarible1.CallOpts)
}

// Erc20TransferProxy is a free data retrieval call binding the contract method 0xfee03e9e.
//
// Solidity: function erc20TransferProxy() view returns(address)
func (_Rarible1 *Rarible1CallerSession) Erc20TransferProxy() (common.Address, error) {
	return _Rarible1.Contract.Erc20TransferProxy(&_Rarible1.CallOpts)
}

// IsOwner is a free data retrieval call binding the contract method 0x8f32d59b.
//
// Solidity: function isOwner() view returns(bool)
func (_Rarible1 *Rarible1Caller) IsOwner(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Rarible1.contract.Call(opts, &out, "isOwner")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsOwner is a free data retrieval call binding the contract method 0x8f32d59b.
//
// Solidity: function isOwner() view returns(bool)
func (_Rarible1 *Rarible1Session) IsOwner() (bool, error) {
	return _Rarible1.Contract.IsOwner(&_Rarible1.CallOpts)
}

// IsOwner is a free data retrieval call binding the contract method 0x8f32d59b.
//
// Solidity: function isOwner() view returns(bool)
func (_Rarible1 *Rarible1CallerSession) IsOwner() (bool, error) {
	return _Rarible1.Contract.IsOwner(&_Rarible1.CallOpts)
}

// OrdersHolder is a free data retrieval call binding the contract method 0x9704dc44.
//
// Solidity: function ordersHolder() view returns(address)
func (_Rarible1 *Rarible1Caller) OrdersHolder(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Rarible1.contract.Call(opts, &out, "ordersHolder")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// OrdersHolder is a free data retrieval call binding the contract method 0x9704dc44.
//
// Solidity: function ordersHolder() view returns(address)
func (_Rarible1 *Rarible1Session) OrdersHolder() (common.Address, error) {
	return _Rarible1.Contract.OrdersHolder(&_Rarible1.CallOpts)
}

// OrdersHolder is a free data retrieval call binding the contract method 0x9704dc44.
//
// Solidity: function ordersHolder() view returns(address)
func (_Rarible1 *Rarible1CallerSession) OrdersHolder() (common.Address, error) {
	return _Rarible1.Contract.OrdersHolder(&_Rarible1.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Rarible1 *Rarible1Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Rarible1.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Rarible1 *Rarible1Session) Owner() (common.Address, error) {
	return _Rarible1.Contract.Owner(&_Rarible1.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Rarible1 *Rarible1CallerSession) Owner() (common.Address, error) {
	return _Rarible1.Contract.Owner(&_Rarible1.CallOpts)
}

// PrepareBuyerFeeMessage is a free data retrieval call binding the contract method 0x1b4c9874.
//
// Solidity: function prepareBuyerFeeMessage(((address,uint256,(address,uint256,uint8),(address,uint256,uint8)),uint256,uint256,uint256) order, uint256 fee) pure returns(string)
func (_Rarible1 *Rarible1Caller) PrepareBuyerFeeMessage(opts *bind.CallOpts, order ExchangeDomainV1Order, fee *big.Int) (string, error) {
	var out []interface{}
	err := _Rarible1.contract.Call(opts, &out, "prepareBuyerFeeMessage", order, fee)

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// PrepareBuyerFeeMessage is a free data retrieval call binding the contract method 0x1b4c9874.
//
// Solidity: function prepareBuyerFeeMessage(((address,uint256,(address,uint256,uint8),(address,uint256,uint8)),uint256,uint256,uint256) order, uint256 fee) pure returns(string)
func (_Rarible1 *Rarible1Session) PrepareBuyerFeeMessage(order ExchangeDomainV1Order, fee *big.Int) (string, error) {
	return _Rarible1.Contract.PrepareBuyerFeeMessage(&_Rarible1.CallOpts, order, fee)
}

// PrepareBuyerFeeMessage is a free data retrieval call binding the contract method 0x1b4c9874.
//
// Solidity: function prepareBuyerFeeMessage(((address,uint256,(address,uint256,uint8),(address,uint256,uint8)),uint256,uint256,uint256) order, uint256 fee) pure returns(string)
func (_Rarible1 *Rarible1CallerSession) PrepareBuyerFeeMessage(order ExchangeDomainV1Order, fee *big.Int) (string, error) {
	return _Rarible1.Contract.PrepareBuyerFeeMessage(&_Rarible1.CallOpts, order, fee)
}

// PrepareMessage is a free data retrieval call binding the contract method 0x049944b6.
//
// Solidity: function prepareMessage(((address,uint256,(address,uint256,uint8),(address,uint256,uint8)),uint256,uint256,uint256) order) pure returns(string)
func (_Rarible1 *Rarible1Caller) PrepareMessage(opts *bind.CallOpts, order ExchangeDomainV1Order) (string, error) {
	var out []interface{}
	err := _Rarible1.contract.Call(opts, &out, "prepareMessage", order)

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// PrepareMessage is a free data retrieval call binding the contract method 0x049944b6.
//
// Solidity: function prepareMessage(((address,uint256,(address,uint256,uint8),(address,uint256,uint8)),uint256,uint256,uint256) order) pure returns(string)
func (_Rarible1 *Rarible1Session) PrepareMessage(order ExchangeDomainV1Order) (string, error) {
	return _Rarible1.Contract.PrepareMessage(&_Rarible1.CallOpts, order)
}

// PrepareMessage is a free data retrieval call binding the contract method 0x049944b6.
//
// Solidity: function prepareMessage(((address,uint256,(address,uint256,uint8),(address,uint256,uint8)),uint256,uint256,uint256) order) pure returns(string)
func (_Rarible1 *Rarible1CallerSession) PrepareMessage(order ExchangeDomainV1Order) (string, error) {
	return _Rarible1.Contract.PrepareMessage(&_Rarible1.CallOpts, order)
}

// State is a free data retrieval call binding the contract method 0xc19d93fb.
//
// Solidity: function state() view returns(address)
func (_Rarible1 *Rarible1Caller) State(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Rarible1.contract.Call(opts, &out, "state")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// State is a free data retrieval call binding the contract method 0xc19d93fb.
//
// Solidity: function state() view returns(address)
func (_Rarible1 *Rarible1Session) State() (common.Address, error) {
	return _Rarible1.Contract.State(&_Rarible1.CallOpts)
}

// State is a free data retrieval call binding the contract method 0xc19d93fb.
//
// Solidity: function state() view returns(address)
func (_Rarible1 *Rarible1CallerSession) State() (common.Address, error) {
	return _Rarible1.Contract.State(&_Rarible1.CallOpts)
}

// TransferProxy is a free data retrieval call binding the contract method 0x6e667db3.
//
// Solidity: function transferProxy() view returns(address)
func (_Rarible1 *Rarible1Caller) TransferProxy(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Rarible1.contract.Call(opts, &out, "transferProxy")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// TransferProxy is a free data retrieval call binding the contract method 0x6e667db3.
//
// Solidity: function transferProxy() view returns(address)
func (_Rarible1 *Rarible1Session) TransferProxy() (common.Address, error) {
	return _Rarible1.Contract.TransferProxy(&_Rarible1.CallOpts)
}

// TransferProxy is a free data retrieval call binding the contract method 0x6e667db3.
//
// Solidity: function transferProxy() view returns(address)
func (_Rarible1 *Rarible1CallerSession) TransferProxy() (common.Address, error) {
	return _Rarible1.Contract.TransferProxy(&_Rarible1.CallOpts)
}

// TransferProxyForDeprecated is a free data retrieval call binding the contract method 0x02329e10.
//
// Solidity: function transferProxyForDeprecated() view returns(address)
func (_Rarible1 *Rarible1Caller) TransferProxyForDeprecated(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Rarible1.contract.Call(opts, &out, "transferProxyForDeprecated")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// TransferProxyForDeprecated is a free data retrieval call binding the contract method 0x02329e10.
//
// Solidity: function transferProxyForDeprecated() view returns(address)
func (_Rarible1 *Rarible1Session) TransferProxyForDeprecated() (common.Address, error) {
	return _Rarible1.Contract.TransferProxyForDeprecated(&_Rarible1.CallOpts)
}

// TransferProxyForDeprecated is a free data retrieval call binding the contract method 0x02329e10.
//
// Solidity: function transferProxyForDeprecated() view returns(address)
func (_Rarible1 *Rarible1CallerSession) TransferProxyForDeprecated() (common.Address, error) {
	return _Rarible1.Contract.TransferProxyForDeprecated(&_Rarible1.CallOpts)
}

// Cancel is a paid mutator transaction binding the contract method 0xca120b1f.
//
// Solidity: function cancel((address,uint256,(address,uint256,uint8),(address,uint256,uint8)) key) returns()
func (_Rarible1 *Rarible1Transactor) Cancel(opts *bind.TransactOpts, key ExchangeDomainV1OrderKey) (*types.Transaction, error) {
	return _Rarible1.contract.Transact(opts, "cancel", key)
}

// Cancel is a paid mutator transaction binding the contract method 0xca120b1f.
//
// Solidity: function cancel((address,uint256,(address,uint256,uint8),(address,uint256,uint8)) key) returns()
func (_Rarible1 *Rarible1Session) Cancel(key ExchangeDomainV1OrderKey) (*types.Transaction, error) {
	return _Rarible1.Contract.Cancel(&_Rarible1.TransactOpts, key)
}

// Cancel is a paid mutator transaction binding the contract method 0xca120b1f.
//
// Solidity: function cancel((address,uint256,(address,uint256,uint8),(address,uint256,uint8)) key) returns()
func (_Rarible1 *Rarible1TransactorSession) Cancel(key ExchangeDomainV1OrderKey) (*types.Transaction, error) {
	return _Rarible1.Contract.Cancel(&_Rarible1.TransactOpts, key)
}

// Exchange is a paid mutator transaction binding the contract method 0x9cec6392.
//
// Solidity: function exchange(((address,uint256,(address,uint256,uint8),(address,uint256,uint8)),uint256,uint256,uint256) order, (uint8,bytes32,bytes32) sig, uint256 buyerFee, (uint8,bytes32,bytes32) buyerFeeSig, uint256 amount, address buyer) payable returns()
func (_Rarible1 *Rarible1Transactor) Exchange(opts *bind.TransactOpts, order ExchangeDomainV1Order, sig ExchangeDomainV1Sig, buyerFee *big.Int, buyerFeeSig ExchangeDomainV1Sig, amount *big.Int, buyer common.Address) (*types.Transaction, error) {
	return _Rarible1.contract.Transact(opts, "exchange", order, sig, buyerFee, buyerFeeSig, amount, buyer)
}

// Exchange is a paid mutator transaction binding the contract method 0x9cec6392.
//
// Solidity: function exchange(((address,uint256,(address,uint256,uint8),(address,uint256,uint8)),uint256,uint256,uint256) order, (uint8,bytes32,bytes32) sig, uint256 buyerFee, (uint8,bytes32,bytes32) buyerFeeSig, uint256 amount, address buyer) payable returns()
func (_Rarible1 *Rarible1Session) Exchange(order ExchangeDomainV1Order, sig ExchangeDomainV1Sig, buyerFee *big.Int, buyerFeeSig ExchangeDomainV1Sig, amount *big.Int, buyer common.Address) (*types.Transaction, error) {
	return _Rarible1.Contract.Exchange(&_Rarible1.TransactOpts, order, sig, buyerFee, buyerFeeSig, amount, buyer)
}

// Exchange is a paid mutator transaction binding the contract method 0x9cec6392.
//
// Solidity: function exchange(((address,uint256,(address,uint256,uint8),(address,uint256,uint8)),uint256,uint256,uint256) order, (uint8,bytes32,bytes32) sig, uint256 buyerFee, (uint8,bytes32,bytes32) buyerFeeSig, uint256 amount, address buyer) payable returns()
func (_Rarible1 *Rarible1TransactorSession) Exchange(order ExchangeDomainV1Order, sig ExchangeDomainV1Sig, buyerFee *big.Int, buyerFeeSig ExchangeDomainV1Sig, amount *big.Int, buyer common.Address) (*types.Transaction, error) {
	return _Rarible1.Contract.Exchange(&_Rarible1.TransactOpts, order, sig, buyerFee, buyerFeeSig, amount, buyer)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Rarible1 *Rarible1Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Rarible1.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Rarible1 *Rarible1Session) RenounceOwnership() (*types.Transaction, error) {
	return _Rarible1.Contract.RenounceOwnership(&_Rarible1.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Rarible1 *Rarible1TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Rarible1.Contract.RenounceOwnership(&_Rarible1.TransactOpts)
}

// SetBeneficiary is a paid mutator transaction binding the contract method 0x1c31f710.
//
// Solidity: function setBeneficiary(address newBeneficiary) returns()
func (_Rarible1 *Rarible1Transactor) SetBeneficiary(opts *bind.TransactOpts, newBeneficiary common.Address) (*types.Transaction, error) {
	return _Rarible1.contract.Transact(opts, "setBeneficiary", newBeneficiary)
}

// SetBeneficiary is a paid mutator transaction binding the contract method 0x1c31f710.
//
// Solidity: function setBeneficiary(address newBeneficiary) returns()
func (_Rarible1 *Rarible1Session) SetBeneficiary(newBeneficiary common.Address) (*types.Transaction, error) {
	return _Rarible1.Contract.SetBeneficiary(&_Rarible1.TransactOpts, newBeneficiary)
}

// SetBeneficiary is a paid mutator transaction binding the contract method 0x1c31f710.
//
// Solidity: function setBeneficiary(address newBeneficiary) returns()
func (_Rarible1 *Rarible1TransactorSession) SetBeneficiary(newBeneficiary common.Address) (*types.Transaction, error) {
	return _Rarible1.Contract.SetBeneficiary(&_Rarible1.TransactOpts, newBeneficiary)
}

// SetBuyerFeeSigner is a paid mutator transaction binding the contract method 0x55d5d326.
//
// Solidity: function setBuyerFeeSigner(address newBuyerFeeSigner) returns()
func (_Rarible1 *Rarible1Transactor) SetBuyerFeeSigner(opts *bind.TransactOpts, newBuyerFeeSigner common.Address) (*types.Transaction, error) {
	return _Rarible1.contract.Transact(opts, "setBuyerFeeSigner", newBuyerFeeSigner)
}

// SetBuyerFeeSigner is a paid mutator transaction binding the contract method 0x55d5d326.
//
// Solidity: function setBuyerFeeSigner(address newBuyerFeeSigner) returns()
func (_Rarible1 *Rarible1Session) SetBuyerFeeSigner(newBuyerFeeSigner common.Address) (*types.Transaction, error) {
	return _Rarible1.Contract.SetBuyerFeeSigner(&_Rarible1.TransactOpts, newBuyerFeeSigner)
}

// SetBuyerFeeSigner is a paid mutator transaction binding the contract method 0x55d5d326.
//
// Solidity: function setBuyerFeeSigner(address newBuyerFeeSigner) returns()
func (_Rarible1 *Rarible1TransactorSession) SetBuyerFeeSigner(newBuyerFeeSigner common.Address) (*types.Transaction, error) {
	return _Rarible1.Contract.SetBuyerFeeSigner(&_Rarible1.TransactOpts, newBuyerFeeSigner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Rarible1 *Rarible1Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Rarible1.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Rarible1 *Rarible1Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Rarible1.Contract.TransferOwnership(&_Rarible1.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Rarible1 *Rarible1TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Rarible1.Contract.TransferOwnership(&_Rarible1.TransactOpts, newOwner)
}

// Rarible1BuyIterator is returned from FilterBuy and is used to iterate over the raw logs and unpacked data for Buy events raised by the Rarible1 contract.
type Rarible1BuyIterator struct {
	Event *Rarible1Buy // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Rarible1BuyIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Rarible1Buy)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Rarible1Buy)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Rarible1BuyIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Rarible1BuyIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Rarible1Buy represents a Buy event raised by the Rarible1 contract.
type Rarible1Buy struct {
	SellToken   common.Address
	SellTokenId *big.Int
	SellValue   *big.Int
	Owner       common.Address
	BuyToken    common.Address
	BuyTokenId  *big.Int
	BuyValue    *big.Int
	Buyer       common.Address
	Amount      *big.Int
	Salt        *big.Int
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterBuy is a free log retrieval operation binding the contract event 0xdddcdb07e460849cf04a4445b7af9faf01b7f5c7ba75deaf969ac5ed830312c3.
//
// Solidity: event Buy(address indexed sellToken, uint256 indexed sellTokenId, uint256 sellValue, address owner, address buyToken, uint256 buyTokenId, uint256 buyValue, address buyer, uint256 amount, uint256 salt)
func (_Rarible1 *Rarible1Filterer) FilterBuy(opts *bind.FilterOpts, sellToken []common.Address, sellTokenId []*big.Int) (*Rarible1BuyIterator, error) {

	var sellTokenRule []interface{}
	for _, sellTokenItem := range sellToken {
		sellTokenRule = append(sellTokenRule, sellTokenItem)
	}
	var sellTokenIdRule []interface{}
	for _, sellTokenIdItem := range sellTokenId {
		sellTokenIdRule = append(sellTokenIdRule, sellTokenIdItem)
	}

	logs, sub, err := _Rarible1.contract.FilterLogs(opts, "Buy", sellTokenRule, sellTokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Rarible1BuyIterator{contract: _Rarible1.contract, event: "Buy", logs: logs, sub: sub}, nil
}

// WatchBuy is a free log subscription operation binding the contract event 0xdddcdb07e460849cf04a4445b7af9faf01b7f5c7ba75deaf969ac5ed830312c3.
//
// Solidity: event Buy(address indexed sellToken, uint256 indexed sellTokenId, uint256 sellValue, address owner, address buyToken, uint256 buyTokenId, uint256 buyValue, address buyer, uint256 amount, uint256 salt)
func (_Rarible1 *Rarible1Filterer) WatchBuy(opts *bind.WatchOpts, sink chan<- *Rarible1Buy, sellToken []common.Address, sellTokenId []*big.Int) (event.Subscription, error) {

	var sellTokenRule []interface{}
	for _, sellTokenItem := range sellToken {
		sellTokenRule = append(sellTokenRule, sellTokenItem)
	}
	var sellTokenIdRule []interface{}
	for _, sellTokenIdItem := range sellTokenId {
		sellTokenIdRule = append(sellTokenIdRule, sellTokenIdItem)
	}

	logs, sub, err := _Rarible1.contract.WatchLogs(opts, "Buy", sellTokenRule, sellTokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Rarible1Buy)
				if err := _Rarible1.contract.UnpackLog(event, "Buy", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBuy is a log parse operation binding the contract event 0xdddcdb07e460849cf04a4445b7af9faf01b7f5c7ba75deaf969ac5ed830312c3.
//
// Solidity: event Buy(address indexed sellToken, uint256 indexed sellTokenId, uint256 sellValue, address owner, address buyToken, uint256 buyTokenId, uint256 buyValue, address buyer, uint256 amount, uint256 salt)
func (_Rarible1 *Rarible1Filterer) ParseBuy(log types.Log) (*Rarible1Buy, error) {
	event := new(Rarible1Buy)
	if err := _Rarible1.contract.UnpackLog(event, "Buy", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Rarible1CancelIterator is returned from FilterCancel and is used to iterate over the raw logs and unpacked data for Cancel events raised by the Rarible1 contract.
type Rarible1CancelIterator struct {
	Event *Rarible1Cancel // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Rarible1CancelIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Rarible1Cancel)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Rarible1Cancel)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Rarible1CancelIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Rarible1CancelIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Rarible1Cancel represents a Cancel event raised by the Rarible1 contract.
type Rarible1Cancel struct {
	SellToken   common.Address
	SellTokenId *big.Int
	Owner       common.Address
	BuyToken    common.Address
	BuyTokenId  *big.Int
	Salt        *big.Int
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterCancel is a free log retrieval operation binding the contract event 0xbfe0e802e586c99960de1a111c80f598b281996d65080d74dbe29986f55b274a.
//
// Solidity: event Cancel(address indexed sellToken, uint256 indexed sellTokenId, address owner, address buyToken, uint256 buyTokenId, uint256 salt)
func (_Rarible1 *Rarible1Filterer) FilterCancel(opts *bind.FilterOpts, sellToken []common.Address, sellTokenId []*big.Int) (*Rarible1CancelIterator, error) {

	var sellTokenRule []interface{}
	for _, sellTokenItem := range sellToken {
		sellTokenRule = append(sellTokenRule, sellTokenItem)
	}
	var sellTokenIdRule []interface{}
	for _, sellTokenIdItem := range sellTokenId {
		sellTokenIdRule = append(sellTokenIdRule, sellTokenIdItem)
	}

	logs, sub, err := _Rarible1.contract.FilterLogs(opts, "Cancel", sellTokenRule, sellTokenIdRule)
	if err != nil {
		return nil, err
	}
	return &Rarible1CancelIterator{contract: _Rarible1.contract, event: "Cancel", logs: logs, sub: sub}, nil
}

// WatchCancel is a free log subscription operation binding the contract event 0xbfe0e802e586c99960de1a111c80f598b281996d65080d74dbe29986f55b274a.
//
// Solidity: event Cancel(address indexed sellToken, uint256 indexed sellTokenId, address owner, address buyToken, uint256 buyTokenId, uint256 salt)
func (_Rarible1 *Rarible1Filterer) WatchCancel(opts *bind.WatchOpts, sink chan<- *Rarible1Cancel, sellToken []common.Address, sellTokenId []*big.Int) (event.Subscription, error) {

	var sellTokenRule []interface{}
	for _, sellTokenItem := range sellToken {
		sellTokenRule = append(sellTokenRule, sellTokenItem)
	}
	var sellTokenIdRule []interface{}
	for _, sellTokenIdItem := range sellTokenId {
		sellTokenIdRule = append(sellTokenIdRule, sellTokenIdItem)
	}

	logs, sub, err := _Rarible1.contract.WatchLogs(opts, "Cancel", sellTokenRule, sellTokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Rarible1Cancel)
				if err := _Rarible1.contract.UnpackLog(event, "Cancel", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCancel is a log parse operation binding the contract event 0xbfe0e802e586c99960de1a111c80f598b281996d65080d74dbe29986f55b274a.
//
// Solidity: event Cancel(address indexed sellToken, uint256 indexed sellTokenId, address owner, address buyToken, uint256 buyTokenId, uint256 salt)
func (_Rarible1 *Rarible1Filterer) ParseCancel(log types.Log) (*Rarible1Cancel, error) {
	event := new(Rarible1Cancel)
	if err := _Rarible1.contract.UnpackLog(event, "Cancel", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Rarible1OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Rarible1 contract.
type Rarible1OwnershipTransferredIterator struct {
	Event *Rarible1OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Rarible1OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Rarible1OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Rarible1OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Rarible1OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Rarible1OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Rarible1OwnershipTransferred represents a OwnershipTransferred event raised by the Rarible1 contract.
type Rarible1OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Rarible1 *Rarible1Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Rarible1OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Rarible1.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Rarible1OwnershipTransferredIterator{contract: _Rarible1.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Rarible1 *Rarible1Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Rarible1OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Rarible1.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Rarible1OwnershipTransferred)
				if err := _Rarible1.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Rarible1 *Rarible1Filterer) ParseOwnershipTransferred(log types.Log) (*Rarible1OwnershipTransferred, error) {
	event := new(Rarible1OwnershipTransferred)
	if err := _Rarible1.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
