// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package genie_1

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// ILooksRareExchangeMakerOrder is an auto generated low-level Go binding around an user-defined struct.
type ILooksRareExchangeMakerOrder struct {
	IsOrderAsk         bool
	Signer             common.Address
	Collection         common.Address
	Price              *big.Int
	TokenId            *big.Int
	Amount             *big.Int
	Strategy           common.Address
	Currency           common.Address
	Nonce              *big.Int
	StartTime          *big.Int
	EndTime            *big.Int
	MinPercentageToAsk *big.Int
	Params             []byte
	V                  uint8
	R                  [32]byte
	S                  [32]byte
}

// ILooksRareExchangeTakerOrder is an auto generated low-level Go binding around an user-defined struct.
type ILooksRareExchangeTakerOrder struct {
	IsOrderAsk         bool
	Taker              common.Address
	Price              *big.Int
	TokenId            *big.Int
	MinPercentageToAsk *big.Int
	Params             []byte
}

// Genie1MetaData contains all meta data concerning the Genie1 contract.
var Genie1MetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes\",\"name\":\"returndata\",\"type\":\"bytes\"}],\"name\":\"ArbitraryCallReturn\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"IID_IERC1155\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"IID_IERC721\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"LOOKSRARE_EXCHANGE\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"addresspayable\",\"name\":\"targetContract\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"encodedArguments\",\"type\":\"bytes\"}],\"name\":\"arbitraryCall\",\"outputs\":[{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"bool\",\"name\":\"isOrderAsk\",\"type\":\"bool\"},{\"internalType\":\"address\",\"name\":\"taker\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minPercentageToAsk\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"params\",\"type\":\"bytes\"}],\"internalType\":\"structILooksRareExchange.TakerOrder[]\",\"name\":\"takerOrders\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"bool\",\"name\":\"isOrderAsk\",\"type\":\"bool\"},{\"internalType\":\"address\",\"name\":\"signer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"strategy\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"currency\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"nonce\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minPercentageToAsk\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"params\",\"type\":\"bytes\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"internalType\":\"structILooksRareExchange.MakerOrder[]\",\"name\":\"makerOrders\",\"type\":\"tuple[]\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"}],\"name\":\"buyAssetsForEth\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256[]\",\"name\":\"\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256[]\",\"name\":\"\",\"type\":\"uint256[]\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC1155BatchReceived\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC1155Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC721Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"recoverEther\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// Genie1ABI is the input ABI used to generate the binding from.
// Deprecated: Use Genie1MetaData.ABI instead.
var Genie1ABI = Genie1MetaData.ABI

// Genie1 is an auto generated Go binding around an Ethereum contract.
type Genie1 struct {
	Genie1Caller     // Read-only binding to the contract
	Genie1Transactor // Write-only binding to the contract
	Genie1Filterer   // Log filterer for contract events
}

// Genie1Caller is an auto generated read-only Go binding around an Ethereum contract.
type Genie1Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Genie1Transactor is an auto generated write-only Go binding around an Ethereum contract.
type Genie1Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Genie1Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type Genie1Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// Genie1Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type Genie1Session struct {
	Contract     *Genie1           // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Genie1CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type Genie1CallerSession struct {
	Contract *Genie1Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// Genie1TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type Genie1TransactorSession struct {
	Contract     *Genie1Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// Genie1Raw is an auto generated low-level Go binding around an Ethereum contract.
type Genie1Raw struct {
	Contract *Genie1 // Generic contract binding to access the raw methods on
}

// Genie1CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type Genie1CallerRaw struct {
	Contract *Genie1Caller // Generic read-only contract binding to access the raw methods on
}

// Genie1TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type Genie1TransactorRaw struct {
	Contract *Genie1Transactor // Generic write-only contract binding to access the raw methods on
}

// NewGenie1 creates a new instance of Genie1, bound to a specific deployed contract.
func NewGenie1(address common.Address, backend bind.ContractBackend) (*Genie1, error) {
	contract, err := bindGenie1(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Genie1{Genie1Caller: Genie1Caller{contract: contract}, Genie1Transactor: Genie1Transactor{contract: contract}, Genie1Filterer: Genie1Filterer{contract: contract}}, nil
}

// NewGenie1Caller creates a new read-only instance of Genie1, bound to a specific deployed contract.
func NewGenie1Caller(address common.Address, caller bind.ContractCaller) (*Genie1Caller, error) {
	contract, err := bindGenie1(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &Genie1Caller{contract: contract}, nil
}

// NewGenie1Transactor creates a new write-only instance of Genie1, bound to a specific deployed contract.
func NewGenie1Transactor(address common.Address, transactor bind.ContractTransactor) (*Genie1Transactor, error) {
	contract, err := bindGenie1(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &Genie1Transactor{contract: contract}, nil
}

// NewGenie1Filterer creates a new log filterer instance of Genie1, bound to a specific deployed contract.
func NewGenie1Filterer(address common.Address, filterer bind.ContractFilterer) (*Genie1Filterer, error) {
	contract, err := bindGenie1(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &Genie1Filterer{contract: contract}, nil
}

// bindGenie1 binds a generic wrapper to an already deployed contract.
func bindGenie1(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(Genie1ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Genie1 *Genie1Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Genie1.Contract.Genie1Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Genie1 *Genie1Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Genie1.Contract.Genie1Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Genie1 *Genie1Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Genie1.Contract.Genie1Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Genie1 *Genie1CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Genie1.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Genie1 *Genie1TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Genie1.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Genie1 *Genie1TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Genie1.Contract.contract.Transact(opts, method, params...)
}

// IIDIERC1155 is a free data retrieval call binding the contract method 0x9b8cfe52.
//
// Solidity: function IID_IERC1155() view returns(bytes4)
func (_Genie1 *Genie1Caller) IIDIERC1155(opts *bind.CallOpts) ([4]byte, error) {
	var out []interface{}
	err := _Genie1.contract.Call(opts, &out, "IID_IERC1155")

	if err != nil {
		return *new([4]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([4]byte)).(*[4]byte)

	return out0, err

}

// IIDIERC1155 is a free data retrieval call binding the contract method 0x9b8cfe52.
//
// Solidity: function IID_IERC1155() view returns(bytes4)
func (_Genie1 *Genie1Session) IIDIERC1155() ([4]byte, error) {
	return _Genie1.Contract.IIDIERC1155(&_Genie1.CallOpts)
}

// IIDIERC1155 is a free data retrieval call binding the contract method 0x9b8cfe52.
//
// Solidity: function IID_IERC1155() view returns(bytes4)
func (_Genie1 *Genie1CallerSession) IIDIERC1155() ([4]byte, error) {
	return _Genie1.Contract.IIDIERC1155(&_Genie1.CallOpts)
}

// IIDIERC721 is a free data retrieval call binding the contract method 0xe0966dad.
//
// Solidity: function IID_IERC721() view returns(bytes4)
func (_Genie1 *Genie1Caller) IIDIERC721(opts *bind.CallOpts) ([4]byte, error) {
	var out []interface{}
	err := _Genie1.contract.Call(opts, &out, "IID_IERC721")

	if err != nil {
		return *new([4]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([4]byte)).(*[4]byte)

	return out0, err

}

// IIDIERC721 is a free data retrieval call binding the contract method 0xe0966dad.
//
// Solidity: function IID_IERC721() view returns(bytes4)
func (_Genie1 *Genie1Session) IIDIERC721() ([4]byte, error) {
	return _Genie1.Contract.IIDIERC721(&_Genie1.CallOpts)
}

// IIDIERC721 is a free data retrieval call binding the contract method 0xe0966dad.
//
// Solidity: function IID_IERC721() view returns(bytes4)
func (_Genie1 *Genie1CallerSession) IIDIERC721() ([4]byte, error) {
	return _Genie1.Contract.IIDIERC721(&_Genie1.CallOpts)
}

// LOOKSRAREEXCHANGE is a free data retrieval call binding the contract method 0xd797aba7.
//
// Solidity: function LOOKSRARE_EXCHANGE() view returns(address)
func (_Genie1 *Genie1Caller) LOOKSRAREEXCHANGE(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Genie1.contract.Call(opts, &out, "LOOKSRARE_EXCHANGE")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// LOOKSRAREEXCHANGE is a free data retrieval call binding the contract method 0xd797aba7.
//
// Solidity: function LOOKSRARE_EXCHANGE() view returns(address)
func (_Genie1 *Genie1Session) LOOKSRAREEXCHANGE() (common.Address, error) {
	return _Genie1.Contract.LOOKSRAREEXCHANGE(&_Genie1.CallOpts)
}

// LOOKSRAREEXCHANGE is a free data retrieval call binding the contract method 0xd797aba7.
//
// Solidity: function LOOKSRARE_EXCHANGE() view returns(address)
func (_Genie1 *Genie1CallerSession) LOOKSRAREEXCHANGE() (common.Address, error) {
	return _Genie1.Contract.LOOKSRAREEXCHANGE(&_Genie1.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Genie1 *Genie1Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Genie1.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Genie1 *Genie1Session) Owner() (common.Address, error) {
	return _Genie1.Contract.Owner(&_Genie1.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Genie1 *Genie1CallerSession) Owner() (common.Address, error) {
	return _Genie1.Contract.Owner(&_Genie1.CallOpts)
}

// ArbitraryCall is a paid mutator transaction binding the contract method 0x2154bf28.
//
// Solidity: function arbitraryCall(address targetContract, bytes encodedArguments) payable returns(bytes)
func (_Genie1 *Genie1Transactor) ArbitraryCall(opts *bind.TransactOpts, targetContract common.Address, encodedArguments []byte) (*types.Transaction, error) {
	return _Genie1.contract.Transact(opts, "arbitraryCall", targetContract, encodedArguments)
}

// ArbitraryCall is a paid mutator transaction binding the contract method 0x2154bf28.
//
// Solidity: function arbitraryCall(address targetContract, bytes encodedArguments) payable returns(bytes)
func (_Genie1 *Genie1Session) ArbitraryCall(targetContract common.Address, encodedArguments []byte) (*types.Transaction, error) {
	return _Genie1.Contract.ArbitraryCall(&_Genie1.TransactOpts, targetContract, encodedArguments)
}

// ArbitraryCall is a paid mutator transaction binding the contract method 0x2154bf28.
//
// Solidity: function arbitraryCall(address targetContract, bytes encodedArguments) payable returns(bytes)
func (_Genie1 *Genie1TransactorSession) ArbitraryCall(targetContract common.Address, encodedArguments []byte) (*types.Transaction, error) {
	return _Genie1.Contract.ArbitraryCall(&_Genie1.TransactOpts, targetContract, encodedArguments)
}

// BuyAssetsForEth is a paid mutator transaction binding the contract method 0x96582bba.
//
// Solidity: function buyAssetsForEth((bool,address,uint256,uint256,uint256,bytes)[] takerOrders, (bool,address,address,uint256,uint256,uint256,address,address,uint256,uint256,uint256,uint256,bytes,uint8,bytes32,bytes32)[] makerOrders, address recipient) payable returns()
func (_Genie1 *Genie1Transactor) BuyAssetsForEth(opts *bind.TransactOpts, takerOrders []ILooksRareExchangeTakerOrder, makerOrders []ILooksRareExchangeMakerOrder, recipient common.Address) (*types.Transaction, error) {
	return _Genie1.contract.Transact(opts, "buyAssetsForEth", takerOrders, makerOrders, recipient)
}

// BuyAssetsForEth is a paid mutator transaction binding the contract method 0x96582bba.
//
// Solidity: function buyAssetsForEth((bool,address,uint256,uint256,uint256,bytes)[] takerOrders, (bool,address,address,uint256,uint256,uint256,address,address,uint256,uint256,uint256,uint256,bytes,uint8,bytes32,bytes32)[] makerOrders, address recipient) payable returns()
func (_Genie1 *Genie1Session) BuyAssetsForEth(takerOrders []ILooksRareExchangeTakerOrder, makerOrders []ILooksRareExchangeMakerOrder, recipient common.Address) (*types.Transaction, error) {
	return _Genie1.Contract.BuyAssetsForEth(&_Genie1.TransactOpts, takerOrders, makerOrders, recipient)
}

// BuyAssetsForEth is a paid mutator transaction binding the contract method 0x96582bba.
//
// Solidity: function buyAssetsForEth((bool,address,uint256,uint256,uint256,bytes)[] takerOrders, (bool,address,address,uint256,uint256,uint256,address,address,uint256,uint256,uint256,uint256,bytes,uint8,bytes32,bytes32)[] makerOrders, address recipient) payable returns()
func (_Genie1 *Genie1TransactorSession) BuyAssetsForEth(takerOrders []ILooksRareExchangeTakerOrder, makerOrders []ILooksRareExchangeMakerOrder, recipient common.Address) (*types.Transaction, error) {
	return _Genie1.Contract.BuyAssetsForEth(&_Genie1.TransactOpts, takerOrders, makerOrders, recipient)
}

// OnERC1155BatchReceived is a paid mutator transaction binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address , address , uint256[] , uint256[] , bytes ) returns(bytes4)
func (_Genie1 *Genie1Transactor) OnERC1155BatchReceived(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 []*big.Int, arg3 []*big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Genie1.contract.Transact(opts, "onERC1155BatchReceived", arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155BatchReceived is a paid mutator transaction binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address , address , uint256[] , uint256[] , bytes ) returns(bytes4)
func (_Genie1 *Genie1Session) OnERC1155BatchReceived(arg0 common.Address, arg1 common.Address, arg2 []*big.Int, arg3 []*big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Genie1.Contract.OnERC1155BatchReceived(&_Genie1.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155BatchReceived is a paid mutator transaction binding the contract method 0xbc197c81.
//
// Solidity: function onERC1155BatchReceived(address , address , uint256[] , uint256[] , bytes ) returns(bytes4)
func (_Genie1 *Genie1TransactorSession) OnERC1155BatchReceived(arg0 common.Address, arg1 common.Address, arg2 []*big.Int, arg3 []*big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Genie1.Contract.OnERC1155BatchReceived(&_Genie1.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155Received is a paid mutator transaction binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address , address , uint256 , uint256 , bytes ) returns(bytes4)
func (_Genie1 *Genie1Transactor) OnERC1155Received(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 *big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Genie1.contract.Transact(opts, "onERC1155Received", arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155Received is a paid mutator transaction binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address , address , uint256 , uint256 , bytes ) returns(bytes4)
func (_Genie1 *Genie1Session) OnERC1155Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 *big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Genie1.Contract.OnERC1155Received(&_Genie1.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC1155Received is a paid mutator transaction binding the contract method 0xf23a6e61.
//
// Solidity: function onERC1155Received(address , address , uint256 , uint256 , bytes ) returns(bytes4)
func (_Genie1 *Genie1TransactorSession) OnERC1155Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 *big.Int, arg4 []byte) (*types.Transaction, error) {
	return _Genie1.Contract.OnERC1155Received(&_Genie1.TransactOpts, arg0, arg1, arg2, arg3, arg4)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Genie1 *Genie1Transactor) OnERC721Received(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Genie1.contract.Transact(opts, "onERC721Received", arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Genie1 *Genie1Session) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Genie1.Contract.OnERC721Received(&_Genie1.TransactOpts, arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_Genie1 *Genie1TransactorSession) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _Genie1.Contract.OnERC721Received(&_Genie1.TransactOpts, arg0, arg1, arg2, arg3)
}

// RecoverEther is a paid mutator transaction binding the contract method 0x52d8bfc2.
//
// Solidity: function recoverEther() returns()
func (_Genie1 *Genie1Transactor) RecoverEther(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Genie1.contract.Transact(opts, "recoverEther")
}

// RecoverEther is a paid mutator transaction binding the contract method 0x52d8bfc2.
//
// Solidity: function recoverEther() returns()
func (_Genie1 *Genie1Session) RecoverEther() (*types.Transaction, error) {
	return _Genie1.Contract.RecoverEther(&_Genie1.TransactOpts)
}

// RecoverEther is a paid mutator transaction binding the contract method 0x52d8bfc2.
//
// Solidity: function recoverEther() returns()
func (_Genie1 *Genie1TransactorSession) RecoverEther() (*types.Transaction, error) {
	return _Genie1.Contract.RecoverEther(&_Genie1.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Genie1 *Genie1Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Genie1.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Genie1 *Genie1Session) RenounceOwnership() (*types.Transaction, error) {
	return _Genie1.Contract.RenounceOwnership(&_Genie1.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Genie1 *Genie1TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Genie1.Contract.RenounceOwnership(&_Genie1.TransactOpts)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Genie1 *Genie1Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Genie1.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Genie1 *Genie1Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Genie1.Contract.TransferOwnership(&_Genie1.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Genie1 *Genie1TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Genie1.Contract.TransferOwnership(&_Genie1.TransactOpts, newOwner)
}

// Genie1ArbitraryCallReturnIterator is returned from FilterArbitraryCallReturn and is used to iterate over the raw logs and unpacked data for ArbitraryCallReturn events raised by the Genie1 contract.
type Genie1ArbitraryCallReturnIterator struct {
	Event *Genie1ArbitraryCallReturn // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Genie1ArbitraryCallReturnIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Genie1ArbitraryCallReturn)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Genie1ArbitraryCallReturn)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Genie1ArbitraryCallReturnIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Genie1ArbitraryCallReturnIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Genie1ArbitraryCallReturn represents a ArbitraryCallReturn event raised by the Genie1 contract.
type Genie1ArbitraryCallReturn struct {
	Returndata []byte
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterArbitraryCallReturn is a free log retrieval operation binding the contract event 0x3e3d5c5f5ba5f7a0092e23a506f963dc256b71f2c45de7331087a486f4d367d6.
//
// Solidity: event ArbitraryCallReturn(bytes returndata)
func (_Genie1 *Genie1Filterer) FilterArbitraryCallReturn(opts *bind.FilterOpts) (*Genie1ArbitraryCallReturnIterator, error) {

	logs, sub, err := _Genie1.contract.FilterLogs(opts, "ArbitraryCallReturn")
	if err != nil {
		return nil, err
	}
	return &Genie1ArbitraryCallReturnIterator{contract: _Genie1.contract, event: "ArbitraryCallReturn", logs: logs, sub: sub}, nil
}

// WatchArbitraryCallReturn is a free log subscription operation binding the contract event 0x3e3d5c5f5ba5f7a0092e23a506f963dc256b71f2c45de7331087a486f4d367d6.
//
// Solidity: event ArbitraryCallReturn(bytes returndata)
func (_Genie1 *Genie1Filterer) WatchArbitraryCallReturn(opts *bind.WatchOpts, sink chan<- *Genie1ArbitraryCallReturn) (event.Subscription, error) {

	logs, sub, err := _Genie1.contract.WatchLogs(opts, "ArbitraryCallReturn")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Genie1ArbitraryCallReturn)
				if err := _Genie1.contract.UnpackLog(event, "ArbitraryCallReturn", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseArbitraryCallReturn is a log parse operation binding the contract event 0x3e3d5c5f5ba5f7a0092e23a506f963dc256b71f2c45de7331087a486f4d367d6.
//
// Solidity: event ArbitraryCallReturn(bytes returndata)
func (_Genie1 *Genie1Filterer) ParseArbitraryCallReturn(log types.Log) (*Genie1ArbitraryCallReturn, error) {
	event := new(Genie1ArbitraryCallReturn)
	if err := _Genie1.contract.UnpackLog(event, "ArbitraryCallReturn", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// Genie1OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Genie1 contract.
type Genie1OwnershipTransferredIterator struct {
	Event *Genie1OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *Genie1OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(Genie1OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(Genie1OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *Genie1OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *Genie1OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// Genie1OwnershipTransferred represents a OwnershipTransferred event raised by the Genie1 contract.
type Genie1OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Genie1 *Genie1Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*Genie1OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Genie1.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &Genie1OwnershipTransferredIterator{contract: _Genie1.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Genie1 *Genie1Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *Genie1OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Genie1.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(Genie1OwnershipTransferred)
				if err := _Genie1.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Genie1 *Genie1Filterer) ParseOwnershipTransferred(log types.Log) (*Genie1OwnershipTransferred, error) {
	event := new(Genie1OwnershipTransferred)
	if err := _Genie1.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
