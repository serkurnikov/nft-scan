package caller

import (
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
)

// Caller is a read-only Go binding around an Ethereum caller.
type Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// InitiateContractCaller puts Caller from cache by caller address to package variable caller
// or initiates one if it's not exists
func InitiateContractCaller(address, ABI string, httpClient *ethclient.Client) (*Caller, error) {
	return newContractCaller(common.HexToAddress(address), httpClient, ABI)
}

// newContractCaller creates a new read-only instance of Contract, bound to a specific deployed caller.
func newContractCaller(address common.Address, caller bind.ContractCaller, masterChefABI string) (*Caller, error) {
	parsed, err := abi.JSON(strings.NewReader(masterChefABI))
	if err != nil {
		return nil, err
	}
	return &Caller{
		contract: bind.NewBoundContract(address, parsed, caller, nil, nil),
	}, nil
}

// CallWithParameter calls the caller method with parameters
func (c *Caller) CallWithParameter(method string, param interface{}) ([]interface{}, error) {
	var out []interface{}
	return out, c.contract.Call(nil, &out, method, param)
}

// Call calls the caller method without any parameters
func (c *Caller) Call(method string) ([]interface{}, error) {
	var out []interface{}
	return out, c.contract.Call(nil, &out, method)
}

// UnpackLog unpack log into interface{}
func (c *Caller) UnpackLog(event string, log types.Log) (interface{}, error) {
	var out interface{}
	return out, c.contract.UnpackLog(out, event, log)
}

// UnpackLogIntoMap unpack log into map
func (c *Caller) UnpackLogIntoMap(event string, log types.Log) (map[string]interface{}, error) {
	out := make(map[string]interface{})
	return out, c.contract.UnpackLogIntoMap(out, event, log)
}
