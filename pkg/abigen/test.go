// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package main

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// MainMetaData contains all meta data concerning the Main contract.
var MainMetaData = &bind.MetaData{
	ABI: "[{\"name\":\"Transfer\",\"inputs\":[{\"type\":\"address\",\"name\":\"sender\",\"indexed\":true},{\"type\":\"address\",\"name\":\"receiver\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"token_id\",\"indexed\":true}],\"anonymous\":false,\"type\":\"event\"},{\"name\":\"Approval\",\"inputs\":[{\"type\":\"address\",\"name\":\"owner\",\"indexed\":true},{\"type\":\"address\",\"name\":\"approved\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"token_id\",\"indexed\":true}],\"anonymous\":false,\"type\":\"event\"},{\"name\":\"ApprovalForAll\",\"inputs\":[{\"type\":\"address\",\"name\":\"owner\",\"indexed\":true},{\"type\":\"address\",\"name\":\"operator\",\"indexed\":true},{\"type\":\"bool\",\"name\":\"approved\",\"indexed\":false}],\"anonymous\":false,\"type\":\"event\"},{\"name\":\"NewSettler\",\"inputs\":[{\"type\":\"address\",\"name\":\"addr\",\"indexed\":false}],\"anonymous\":false,\"type\":\"event\"},{\"name\":\"NewSynth\",\"inputs\":[{\"type\":\"address\",\"name\":\"synth\",\"indexed\":false},{\"type\":\"address\",\"name\":\"pool\",\"indexed\":false}],\"anonymous\":false,\"type\":\"event\"},{\"name\":\"TokenUpdate\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"token_id\",\"indexed\":true},{\"type\":\"address\",\"name\":\"owner\",\"indexed\":true},{\"type\":\"address\",\"name\":\"synth\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"underlying_balance\",\"indexed\":false}],\"anonymous\":false,\"type\":\"event\"},{\"outputs\":[],\"inputs\":[{\"type\":\"address\",\"name\":\"_settler_implementation\"},{\"type\":\"uint256\",\"name\":\"_settler_count\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"name\":\"name\",\"outputs\":[{\"type\":\"string\",\"name\":\"\"}],\"inputs\":[],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":4579},{\"name\":\"symbol\",\"outputs\":[{\"type\":\"string\",\"name\":\"\"}],\"inputs\":[],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":4609},{\"name\":\"supportsInterface\",\"outputs\":[{\"type\":\"bool\",\"name\":\"\"}],\"inputs\":[{\"type\":\"bytes32\",\"name\":\"_interface_id\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":845},{\"name\":\"balanceOf\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\"}],\"inputs\":[{\"type\":\"address\",\"name\":\"_owner\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":1553},{\"name\":\"ownerOf\",\"outputs\":[{\"type\":\"address\",\"name\":\"\"}],\"inputs\":[{\"type\":\"uint256\",\"name\":\"_token_id\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":1498},{\"name\":\"getApproved\",\"outputs\":[{\"type\":\"address\",\"name\":\"\"}],\"inputs\":[{\"type\":\"uint256\",\"name\":\"_token_id\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":2425},{\"name\":\"isApprovedForAll\",\"outputs\":[{\"type\":\"bool\",\"name\":\"\"}],\"inputs\":[{\"type\":\"address\",\"name\":\"_owner\"},{\"type\":\"address\",\"name\":\"_operator\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":1761},{\"name\":\"transferFrom\",\"outputs\":[],\"inputs\":[{\"type\":\"address\",\"name\":\"_from\"},{\"type\":\"address\",\"name\":\"_to\"},{\"type\":\"uint256\",\"name\":\"_token_id\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\",\"gas\":134578},{\"name\":\"safeTransferFrom\",\"outputs\":[],\"inputs\":[{\"type\":\"address\",\"name\":\"_from\"},{\"type\":\"address\",\"name\":\"_to\"},{\"type\":\"uint256\",\"name\":\"_token_id\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"name\":\"safeTransferFrom\",\"outputs\":[],\"inputs\":[{\"type\":\"address\",\"name\":\"_from\"},{\"type\":\"address\",\"name\":\"_to\"},{\"type\":\"uint256\",\"name\":\"_token_id\"},{\"type\":\"bytes\",\"name\":\"_data\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"name\":\"approve\",\"outputs\":[],\"inputs\":[{\"type\":\"address\",\"name\":\"_approved\"},{\"type\":\"uint256\",\"name\":\"_token_id\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\",\"gas\":40888},{\"name\":\"setApprovalForAll\",\"outputs\":[],\"inputs\":[{\"type\":\"address\",\"name\":\"_operator\"},{\"type\":\"bool\",\"name\":\"_approved\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\",\"gas\":38179},{\"name\":\"get_swap_into_synth_amount\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\"}],\"inputs\":[{\"type\":\"address\",\"name\":\"_from\"},{\"type\":\"address\",\"name\":\"_synth\"},{\"type\":\"uint256\",\"name\":\"_amount\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":8579},{\"name\":\"get_swap_from_synth_amount\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\"}],\"inputs\":[{\"type\":\"address\",\"name\":\"_synth\"},{\"type\":\"address\",\"name\":\"_to\"},{\"type\":\"uint256\",\"name\":\"_amount\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":4312},{\"name\":\"get_estimated_swap_amount\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\"}],\"inputs\":[{\"type\":\"address\",\"name\":\"_from\"},{\"type\":\"address\",\"name\":\"_to\"},{\"type\":\"uint256\",\"name\":\"_amount\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":12949},{\"name\":\"token_info\",\"outputs\":[{\"type\":\"address\",\"name\":\"owner\"},{\"type\":\"address\",\"name\":\"synth\"},{\"type\":\"uint256\",\"name\":\"underlying_balance\"},{\"type\":\"uint256\",\"name\":\"time_to_settle\"}],\"inputs\":[{\"type\":\"uint256\",\"name\":\"_token_id\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":8207},{\"name\":\"swap_into_synth\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\"}],\"inputs\":[{\"type\":\"address\",\"name\":\"_from\"},{\"type\":\"address\",\"name\":\"_synth\"},{\"type\":\"uint256\",\"name\":\"_amount\"},{\"type\":\"uint256\",\"name\":\"_expected\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"name\":\"swap_into_synth\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\"}],\"inputs\":[{\"type\":\"address\",\"name\":\"_from\"},{\"type\":\"address\",\"name\":\"_synth\"},{\"type\":\"uint256\",\"name\":\"_amount\"},{\"type\":\"uint256\",\"name\":\"_expected\"},{\"type\":\"address\",\"name\":\"_receiver\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"name\":\"swap_into_synth\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\"}],\"inputs\":[{\"type\":\"address\",\"name\":\"_from\"},{\"type\":\"address\",\"name\":\"_synth\"},{\"type\":\"uint256\",\"name\":\"_amount\"},{\"type\":\"uint256\",\"name\":\"_expected\"},{\"type\":\"address\",\"name\":\"_receiver\"},{\"type\":\"uint256\",\"name\":\"_existing_token_id\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"name\":\"swap_from_synth\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\"}],\"inputs\":[{\"type\":\"uint256\",\"name\":\"_token_id\"},{\"type\":\"address\",\"name\":\"_to\"},{\"type\":\"uint256\",\"name\":\"_amount\"},{\"type\":\"uint256\",\"name\":\"_expected\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"name\":\"swap_from_synth\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\"}],\"inputs\":[{\"type\":\"uint256\",\"name\":\"_token_id\"},{\"type\":\"address\",\"name\":\"_to\"},{\"type\":\"uint256\",\"name\":\"_amount\"},{\"type\":\"uint256\",\"name\":\"_expected\"},{\"type\":\"address\",\"name\":\"_receiver\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"name\":\"withdraw\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\"}],\"inputs\":[{\"type\":\"uint256\",\"name\":\"_token_id\"},{\"type\":\"uint256\",\"name\":\"_amount\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"name\":\"withdraw\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\"}],\"inputs\":[{\"type\":\"uint256\",\"name\":\"_token_id\"},{\"type\":\"uint256\",\"name\":\"_amount\"},{\"type\":\"address\",\"name\":\"_receiver\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"name\":\"settle\",\"outputs\":[{\"type\":\"bool\",\"name\":\"\"}],\"inputs\":[{\"type\":\"uint256\",\"name\":\"_token_id\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\",\"gas\":42848},{\"name\":\"add_synth\",\"outputs\":[],\"inputs\":[{\"type\":\"address\",\"name\":\"_synth\"},{\"type\":\"address\",\"name\":\"_pool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\",\"gas\":605929},{\"name\":\"rebuildCache\",\"outputs\":[],\"inputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\",\"gas\":36338},{\"name\":\"synth_pools\",\"outputs\":[{\"type\":\"address\",\"name\":\"\"}],\"inputs\":[{\"type\":\"address\",\"name\":\"arg0\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":2086},{\"name\":\"swappable_synth\",\"outputs\":[{\"type\":\"address\",\"name\":\"\"}],\"inputs\":[{\"type\":\"address\",\"name\":\"arg0\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":2116},{\"name\":\"is_settled\",\"outputs\":[{\"type\":\"bool\",\"name\":\"\"}],\"inputs\":[{\"type\":\"uint256\",\"name\":\"arg0\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"gas\":2046}]",
}

// MainABI is the input ABI used to generate the binding from.
// Deprecated: Use MainMetaData.ABI instead.
var MainABI = MainMetaData.ABI

// Main is an auto generated Go binding around an Ethereum contract.
type Main struct {
	MainCaller     // Read-only binding to the contract
	MainTransactor // Write-only binding to the contract
	MainFilterer   // Log filterer for contract events
}

// MainCaller is an auto generated read-only Go binding around an Ethereum contract.
type MainCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// MainTransactor is an auto generated write-only Go binding around an Ethereum contract.
type MainTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// MainFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type MainFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// MainSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type MainSession struct {
	Contract     *Main             // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// MainCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type MainCallerSession struct {
	Contract *MainCaller   // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// MainTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type MainTransactorSession struct {
	Contract     *MainTransactor   // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// MainRaw is an auto generated low-level Go binding around an Ethereum contract.
type MainRaw struct {
	Contract *Main // Generic contract binding to access the raw methods on
}

// MainCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type MainCallerRaw struct {
	Contract *MainCaller // Generic read-only contract binding to access the raw methods on
}

// MainTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type MainTransactorRaw struct {
	Contract *MainTransactor // Generic write-only contract binding to access the raw methods on
}

// NewMain creates a new instance of Main, bound to a specific deployed contract.
func NewMain(address common.Address, backend bind.ContractBackend) (*Main, error) {
	contract, err := bindMain(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Main{MainCaller: MainCaller{contract: contract}, MainTransactor: MainTransactor{contract: contract}, MainFilterer: MainFilterer{contract: contract}}, nil
}

// NewMainCaller creates a new read-only instance of Main, bound to a specific deployed contract.
func NewMainCaller(address common.Address, caller bind.ContractCaller) (*MainCaller, error) {
	contract, err := bindMain(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &MainCaller{contract: contract}, nil
}

// NewMainTransactor creates a new write-only instance of Main, bound to a specific deployed contract.
func NewMainTransactor(address common.Address, transactor bind.ContractTransactor) (*MainTransactor, error) {
	contract, err := bindMain(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &MainTransactor{contract: contract}, nil
}

// NewMainFilterer creates a new log filterer instance of Main, bound to a specific deployed contract.
func NewMainFilterer(address common.Address, filterer bind.ContractFilterer) (*MainFilterer, error) {
	contract, err := bindMain(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &MainFilterer{contract: contract}, nil
}

// bindMain binds a generic wrapper to an already deployed contract.
func bindMain(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(MainABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Main *MainRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Main.Contract.MainCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Main *MainRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Main.Contract.MainTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Main *MainRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Main.Contract.MainTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Main *MainCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Main.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Main *MainTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Main.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Main *MainTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Main.Contract.contract.Transact(opts, method, params...)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address _owner) view returns(uint256)
func (_Main *MainCaller) BalanceOf(opts *bind.CallOpts, _owner common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "balanceOf", _owner)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address _owner) view returns(uint256)
func (_Main *MainSession) BalanceOf(_owner common.Address) (*big.Int, error) {
	return _Main.Contract.BalanceOf(&_Main.CallOpts, _owner)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address _owner) view returns(uint256)
func (_Main *MainCallerSession) BalanceOf(_owner common.Address) (*big.Int, error) {
	return _Main.Contract.BalanceOf(&_Main.CallOpts, _owner)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 _token_id) view returns(address)
func (_Main *MainCaller) GetApproved(opts *bind.CallOpts, _token_id *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "getApproved", _token_id)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 _token_id) view returns(address)
func (_Main *MainSession) GetApproved(_token_id *big.Int) (common.Address, error) {
	return _Main.Contract.GetApproved(&_Main.CallOpts, _token_id)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 _token_id) view returns(address)
func (_Main *MainCallerSession) GetApproved(_token_id *big.Int) (common.Address, error) {
	return _Main.Contract.GetApproved(&_Main.CallOpts, _token_id)
}

// GetEstimatedSwapAmount is a free data retrieval call binding the contract method 0xb691a2b5.
//
// Solidity: function get_estimated_swap_amount(address _from, address _to, uint256 _amount) view returns(uint256)
func (_Main *MainCaller) GetEstimatedSwapAmount(opts *bind.CallOpts, _from common.Address, _to common.Address, _amount *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "get_estimated_swap_amount", _from, _to, _amount)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetEstimatedSwapAmount is a free data retrieval call binding the contract method 0xb691a2b5.
//
// Solidity: function get_estimated_swap_amount(address _from, address _to, uint256 _amount) view returns(uint256)
func (_Main *MainSession) GetEstimatedSwapAmount(_from common.Address, _to common.Address, _amount *big.Int) (*big.Int, error) {
	return _Main.Contract.GetEstimatedSwapAmount(&_Main.CallOpts, _from, _to, _amount)
}

// GetEstimatedSwapAmount is a free data retrieval call binding the contract method 0xb691a2b5.
//
// Solidity: function get_estimated_swap_amount(address _from, address _to, uint256 _amount) view returns(uint256)
func (_Main *MainCallerSession) GetEstimatedSwapAmount(_from common.Address, _to common.Address, _amount *big.Int) (*big.Int, error) {
	return _Main.Contract.GetEstimatedSwapAmount(&_Main.CallOpts, _from, _to, _amount)
}

// GetSwapFromSynthAmount is a free data retrieval call binding the contract method 0x2b2ca980.
//
// Solidity: function get_swap_from_synth_amount(address _synth, address _to, uint256 _amount) view returns(uint256)
func (_Main *MainCaller) GetSwapFromSynthAmount(opts *bind.CallOpts, _synth common.Address, _to common.Address, _amount *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "get_swap_from_synth_amount", _synth, _to, _amount)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetSwapFromSynthAmount is a free data retrieval call binding the contract method 0x2b2ca980.
//
// Solidity: function get_swap_from_synth_amount(address _synth, address _to, uint256 _amount) view returns(uint256)
func (_Main *MainSession) GetSwapFromSynthAmount(_synth common.Address, _to common.Address, _amount *big.Int) (*big.Int, error) {
	return _Main.Contract.GetSwapFromSynthAmount(&_Main.CallOpts, _synth, _to, _amount)
}

// GetSwapFromSynthAmount is a free data retrieval call binding the contract method 0x2b2ca980.
//
// Solidity: function get_swap_from_synth_amount(address _synth, address _to, uint256 _amount) view returns(uint256)
func (_Main *MainCallerSession) GetSwapFromSynthAmount(_synth common.Address, _to common.Address, _amount *big.Int) (*big.Int, error) {
	return _Main.Contract.GetSwapFromSynthAmount(&_Main.CallOpts, _synth, _to, _amount)
}

// GetSwapIntoSynthAmount is a free data retrieval call binding the contract method 0xaf89565a.
//
// Solidity: function get_swap_into_synth_amount(address _from, address _synth, uint256 _amount) view returns(uint256)
func (_Main *MainCaller) GetSwapIntoSynthAmount(opts *bind.CallOpts, _from common.Address, _synth common.Address, _amount *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "get_swap_into_synth_amount", _from, _synth, _amount)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetSwapIntoSynthAmount is a free data retrieval call binding the contract method 0xaf89565a.
//
// Solidity: function get_swap_into_synth_amount(address _from, address _synth, uint256 _amount) view returns(uint256)
func (_Main *MainSession) GetSwapIntoSynthAmount(_from common.Address, _synth common.Address, _amount *big.Int) (*big.Int, error) {
	return _Main.Contract.GetSwapIntoSynthAmount(&_Main.CallOpts, _from, _synth, _amount)
}

// GetSwapIntoSynthAmount is a free data retrieval call binding the contract method 0xaf89565a.
//
// Solidity: function get_swap_into_synth_amount(address _from, address _synth, uint256 _amount) view returns(uint256)
func (_Main *MainCallerSession) GetSwapIntoSynthAmount(_from common.Address, _synth common.Address, _amount *big.Int) (*big.Int, error) {
	return _Main.Contract.GetSwapIntoSynthAmount(&_Main.CallOpts, _from, _synth, _amount)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address _owner, address _operator) view returns(bool)
func (_Main *MainCaller) IsApprovedForAll(opts *bind.CallOpts, _owner common.Address, _operator common.Address) (bool, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "isApprovedForAll", _owner, _operator)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address _owner, address _operator) view returns(bool)
func (_Main *MainSession) IsApprovedForAll(_owner common.Address, _operator common.Address) (bool, error) {
	return _Main.Contract.IsApprovedForAll(&_Main.CallOpts, _owner, _operator)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address _owner, address _operator) view returns(bool)
func (_Main *MainCallerSession) IsApprovedForAll(_owner common.Address, _operator common.Address) (bool, error) {
	return _Main.Contract.IsApprovedForAll(&_Main.CallOpts, _owner, _operator)
}

// IsSettled is a free data retrieval call binding the contract method 0x8906c4e0.
//
// Solidity: function is_settled(uint256 arg0) view returns(bool)
func (_Main *MainCaller) IsSettled(opts *bind.CallOpts, arg0 *big.Int) (bool, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "is_settled", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsSettled is a free data retrieval call binding the contract method 0x8906c4e0.
//
// Solidity: function is_settled(uint256 arg0) view returns(bool)
func (_Main *MainSession) IsSettled(arg0 *big.Int) (bool, error) {
	return _Main.Contract.IsSettled(&_Main.CallOpts, arg0)
}

// IsSettled is a free data retrieval call binding the contract method 0x8906c4e0.
//
// Solidity: function is_settled(uint256 arg0) view returns(bool)
func (_Main *MainCallerSession) IsSettled(arg0 *big.Int) (bool, error) {
	return _Main.Contract.IsSettled(&_Main.CallOpts, arg0)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Main *MainCaller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Main *MainSession) Name() (string, error) {
	return _Main.Contract.Name(&_Main.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Main *MainCallerSession) Name() (string, error) {
	return _Main.Contract.Name(&_Main.CallOpts)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 _token_id) view returns(address)
func (_Main *MainCaller) OwnerOf(opts *bind.CallOpts, _token_id *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "ownerOf", _token_id)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 _token_id) view returns(address)
func (_Main *MainSession) OwnerOf(_token_id *big.Int) (common.Address, error) {
	return _Main.Contract.OwnerOf(&_Main.CallOpts, _token_id)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 _token_id) view returns(address)
func (_Main *MainCallerSession) OwnerOf(_token_id *big.Int) (common.Address, error) {
	return _Main.Contract.OwnerOf(&_Main.CallOpts, _token_id)
}

// SupportsInterface is a free data retrieval call binding the contract method 0xf1753550.
//
// Solidity: function supportsInterface(bytes32 _interface_id) view returns(bool)
func (_Main *MainCaller) SupportsInterface(opts *bind.CallOpts, _interface_id [32]byte) (bool, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "supportsInterface", _interface_id)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// SupportsInterface is a free data retrieval call binding the contract method 0xf1753550.
//
// Solidity: function supportsInterface(bytes32 _interface_id) view returns(bool)
func (_Main *MainSession) SupportsInterface(_interface_id [32]byte) (bool, error) {
	return _Main.Contract.SupportsInterface(&_Main.CallOpts, _interface_id)
}

// SupportsInterface is a free data retrieval call binding the contract method 0xf1753550.
//
// Solidity: function supportsInterface(bytes32 _interface_id) view returns(bool)
func (_Main *MainCallerSession) SupportsInterface(_interface_id [32]byte) (bool, error) {
	return _Main.Contract.SupportsInterface(&_Main.CallOpts, _interface_id)
}

// SwappableSynth is a free data retrieval call binding the contract method 0xd922eccd.
//
// Solidity: function swappable_synth(address arg0) view returns(address)
func (_Main *MainCaller) SwappableSynth(opts *bind.CallOpts, arg0 common.Address) (common.Address, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "swappable_synth", arg0)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// SwappableSynth is a free data retrieval call binding the contract method 0xd922eccd.
//
// Solidity: function swappable_synth(address arg0) view returns(address)
func (_Main *MainSession) SwappableSynth(arg0 common.Address) (common.Address, error) {
	return _Main.Contract.SwappableSynth(&_Main.CallOpts, arg0)
}

// SwappableSynth is a free data retrieval call binding the contract method 0xd922eccd.
//
// Solidity: function swappable_synth(address arg0) view returns(address)
func (_Main *MainCallerSession) SwappableSynth(arg0 common.Address) (common.Address, error) {
	return _Main.Contract.SwappableSynth(&_Main.CallOpts, arg0)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_Main *MainCaller) Symbol(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_Main *MainSession) Symbol() (string, error) {
	return _Main.Contract.Symbol(&_Main.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_Main *MainCallerSession) Symbol() (string, error) {
	return _Main.Contract.Symbol(&_Main.CallOpts)
}

// SynthPools is a free data retrieval call binding the contract method 0x294bb4d3.
//
// Solidity: function synth_pools(address arg0) view returns(address)
func (_Main *MainCaller) SynthPools(opts *bind.CallOpts, arg0 common.Address) (common.Address, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "synth_pools", arg0)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// SynthPools is a free data retrieval call binding the contract method 0x294bb4d3.
//
// Solidity: function synth_pools(address arg0) view returns(address)
func (_Main *MainSession) SynthPools(arg0 common.Address) (common.Address, error) {
	return _Main.Contract.SynthPools(&_Main.CallOpts, arg0)
}

// SynthPools is a free data retrieval call binding the contract method 0x294bb4d3.
//
// Solidity: function synth_pools(address arg0) view returns(address)
func (_Main *MainCallerSession) SynthPools(arg0 common.Address) (common.Address, error) {
	return _Main.Contract.SynthPools(&_Main.CallOpts, arg0)
}

// TokenInfo is a free data retrieval call binding the contract method 0xd4d06faf.
//
// Solidity: function token_info(uint256 _token_id) view returns(address owner, address synth, uint256 underlying_balance, uint256 time_to_settle)
func (_Main *MainCaller) TokenInfo(opts *bind.CallOpts, _token_id *big.Int) (struct {
	Owner             common.Address
	Synth             common.Address
	UnderlyingBalance *big.Int
	TimeToSettle      *big.Int
}, error) {
	var out []interface{}
	err := _Main.contract.Call(opts, &out, "token_info", _token_id)

	outstruct := new(struct {
		Owner             common.Address
		Synth             common.Address
		UnderlyingBalance *big.Int
		TimeToSettle      *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Owner = *abi.ConvertType(out[0], new(common.Address)).(*common.Address)
	outstruct.Synth = *abi.ConvertType(out[1], new(common.Address)).(*common.Address)
	outstruct.UnderlyingBalance = *abi.ConvertType(out[2], new(*big.Int)).(**big.Int)
	outstruct.TimeToSettle = *abi.ConvertType(out[3], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// TokenInfo is a free data retrieval call binding the contract method 0xd4d06faf.
//
// Solidity: function token_info(uint256 _token_id) view returns(address owner, address synth, uint256 underlying_balance, uint256 time_to_settle)
func (_Main *MainSession) TokenInfo(_token_id *big.Int) (struct {
	Owner             common.Address
	Synth             common.Address
	UnderlyingBalance *big.Int
	TimeToSettle      *big.Int
}, error) {
	return _Main.Contract.TokenInfo(&_Main.CallOpts, _token_id)
}

// TokenInfo is a free data retrieval call binding the contract method 0xd4d06faf.
//
// Solidity: function token_info(uint256 _token_id) view returns(address owner, address synth, uint256 underlying_balance, uint256 time_to_settle)
func (_Main *MainCallerSession) TokenInfo(_token_id *big.Int) (struct {
	Owner             common.Address
	Synth             common.Address
	UnderlyingBalance *big.Int
	TimeToSettle      *big.Int
}, error) {
	return _Main.Contract.TokenInfo(&_Main.CallOpts, _token_id)
}

// AddSynth is a paid mutator transaction binding the contract method 0xfe1e897b.
//
// Solidity: function add_synth(address _synth, address _pool) returns()
func (_Main *MainTransactor) AddSynth(opts *bind.TransactOpts, _synth common.Address, _pool common.Address) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "add_synth", _synth, _pool)
}

// AddSynth is a paid mutator transaction binding the contract method 0xfe1e897b.
//
// Solidity: function add_synth(address _synth, address _pool) returns()
func (_Main *MainSession) AddSynth(_synth common.Address, _pool common.Address) (*types.Transaction, error) {
	return _Main.Contract.AddSynth(&_Main.TransactOpts, _synth, _pool)
}

// AddSynth is a paid mutator transaction binding the contract method 0xfe1e897b.
//
// Solidity: function add_synth(address _synth, address _pool) returns()
func (_Main *MainTransactorSession) AddSynth(_synth common.Address, _pool common.Address) (*types.Transaction, error) {
	return _Main.Contract.AddSynth(&_Main.TransactOpts, _synth, _pool)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address _approved, uint256 _token_id) returns()
func (_Main *MainTransactor) Approve(opts *bind.TransactOpts, _approved common.Address, _token_id *big.Int) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "approve", _approved, _token_id)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address _approved, uint256 _token_id) returns()
func (_Main *MainSession) Approve(_approved common.Address, _token_id *big.Int) (*types.Transaction, error) {
	return _Main.Contract.Approve(&_Main.TransactOpts, _approved, _token_id)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address _approved, uint256 _token_id) returns()
func (_Main *MainTransactorSession) Approve(_approved common.Address, _token_id *big.Int) (*types.Transaction, error) {
	return _Main.Contract.Approve(&_Main.TransactOpts, _approved, _token_id)
}

// RebuildCache is a paid mutator transaction binding the contract method 0x74185360.
//
// Solidity: function rebuildCache() returns()
func (_Main *MainTransactor) RebuildCache(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "rebuildCache")
}

// RebuildCache is a paid mutator transaction binding the contract method 0x74185360.
//
// Solidity: function rebuildCache() returns()
func (_Main *MainSession) RebuildCache() (*types.Transaction, error) {
	return _Main.Contract.RebuildCache(&_Main.TransactOpts)
}

// RebuildCache is a paid mutator transaction binding the contract method 0x74185360.
//
// Solidity: function rebuildCache() returns()
func (_Main *MainTransactorSession) RebuildCache() (*types.Transaction, error) {
	return _Main.Contract.RebuildCache(&_Main.TransactOpts)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address _from, address _to, uint256 _token_id) returns()
func (_Main *MainTransactor) SafeTransferFrom(opts *bind.TransactOpts, _from common.Address, _to common.Address, _token_id *big.Int) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "safeTransferFrom", _from, _to, _token_id)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address _from, address _to, uint256 _token_id) returns()
func (_Main *MainSession) SafeTransferFrom(_from common.Address, _to common.Address, _token_id *big.Int) (*types.Transaction, error) {
	return _Main.Contract.SafeTransferFrom(&_Main.TransactOpts, _from, _to, _token_id)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address _from, address _to, uint256 _token_id) returns()
func (_Main *MainTransactorSession) SafeTransferFrom(_from common.Address, _to common.Address, _token_id *big.Int) (*types.Transaction, error) {
	return _Main.Contract.SafeTransferFrom(&_Main.TransactOpts, _from, _to, _token_id)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address _from, address _to, uint256 _token_id, bytes _data) returns()
func (_Main *MainTransactor) SafeTransferFrom0(opts *bind.TransactOpts, _from common.Address, _to common.Address, _token_id *big.Int, _data []byte) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "safeTransferFrom0", _from, _to, _token_id, _data)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address _from, address _to, uint256 _token_id, bytes _data) returns()
func (_Main *MainSession) SafeTransferFrom0(_from common.Address, _to common.Address, _token_id *big.Int, _data []byte) (*types.Transaction, error) {
	return _Main.Contract.SafeTransferFrom0(&_Main.TransactOpts, _from, _to, _token_id, _data)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address _from, address _to, uint256 _token_id, bytes _data) returns()
func (_Main *MainTransactorSession) SafeTransferFrom0(_from common.Address, _to common.Address, _token_id *big.Int, _data []byte) (*types.Transaction, error) {
	return _Main.Contract.SafeTransferFrom0(&_Main.TransactOpts, _from, _to, _token_id, _data)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address _operator, bool _approved) returns()
func (_Main *MainTransactor) SetApprovalForAll(opts *bind.TransactOpts, _operator common.Address, _approved bool) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "setApprovalForAll", _operator, _approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address _operator, bool _approved) returns()
func (_Main *MainSession) SetApprovalForAll(_operator common.Address, _approved bool) (*types.Transaction, error) {
	return _Main.Contract.SetApprovalForAll(&_Main.TransactOpts, _operator, _approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address _operator, bool _approved) returns()
func (_Main *MainTransactorSession) SetApprovalForAll(_operator common.Address, _approved bool) (*types.Transaction, error) {
	return _Main.Contract.SetApprovalForAll(&_Main.TransactOpts, _operator, _approved)
}

// Settle is a paid mutator transaction binding the contract method 0x8df82800.
//
// Solidity: function settle(uint256 _token_id) returns(bool)
func (_Main *MainTransactor) Settle(opts *bind.TransactOpts, _token_id *big.Int) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "settle", _token_id)
}

// Settle is a paid mutator transaction binding the contract method 0x8df82800.
//
// Solidity: function settle(uint256 _token_id) returns(bool)
func (_Main *MainSession) Settle(_token_id *big.Int) (*types.Transaction, error) {
	return _Main.Contract.Settle(&_Main.TransactOpts, _token_id)
}

// Settle is a paid mutator transaction binding the contract method 0x8df82800.
//
// Solidity: function settle(uint256 _token_id) returns(bool)
func (_Main *MainTransactorSession) Settle(_token_id *big.Int) (*types.Transaction, error) {
	return _Main.Contract.Settle(&_Main.TransactOpts, _token_id)
}

// SwapFromSynth is a paid mutator transaction binding the contract method 0x6adadd5a.
//
// Solidity: function swap_from_synth(uint256 _token_id, address _to, uint256 _amount, uint256 _expected) returns(uint256)
func (_Main *MainTransactor) SwapFromSynth(opts *bind.TransactOpts, _token_id *big.Int, _to common.Address, _amount *big.Int, _expected *big.Int) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "swap_from_synth", _token_id, _to, _amount, _expected)
}

// SwapFromSynth is a paid mutator transaction binding the contract method 0x6adadd5a.
//
// Solidity: function swap_from_synth(uint256 _token_id, address _to, uint256 _amount, uint256 _expected) returns(uint256)
func (_Main *MainSession) SwapFromSynth(_token_id *big.Int, _to common.Address, _amount *big.Int, _expected *big.Int) (*types.Transaction, error) {
	return _Main.Contract.SwapFromSynth(&_Main.TransactOpts, _token_id, _to, _amount, _expected)
}

// SwapFromSynth is a paid mutator transaction binding the contract method 0x6adadd5a.
//
// Solidity: function swap_from_synth(uint256 _token_id, address _to, uint256 _amount, uint256 _expected) returns(uint256)
func (_Main *MainTransactorSession) SwapFromSynth(_token_id *big.Int, _to common.Address, _amount *big.Int, _expected *big.Int) (*types.Transaction, error) {
	return _Main.Contract.SwapFromSynth(&_Main.TransactOpts, _token_id, _to, _amount, _expected)
}

// SwapFromSynth0 is a paid mutator transaction binding the contract method 0xdc460658.
//
// Solidity: function swap_from_synth(uint256 _token_id, address _to, uint256 _amount, uint256 _expected, address _receiver) returns(uint256)
func (_Main *MainTransactor) SwapFromSynth0(opts *bind.TransactOpts, _token_id *big.Int, _to common.Address, _amount *big.Int, _expected *big.Int, _receiver common.Address) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "swap_from_synth0", _token_id, _to, _amount, _expected, _receiver)
}

// SwapFromSynth0 is a paid mutator transaction binding the contract method 0xdc460658.
//
// Solidity: function swap_from_synth(uint256 _token_id, address _to, uint256 _amount, uint256 _expected, address _receiver) returns(uint256)
func (_Main *MainSession) SwapFromSynth0(_token_id *big.Int, _to common.Address, _amount *big.Int, _expected *big.Int, _receiver common.Address) (*types.Transaction, error) {
	return _Main.Contract.SwapFromSynth0(&_Main.TransactOpts, _token_id, _to, _amount, _expected, _receiver)
}

// SwapFromSynth0 is a paid mutator transaction binding the contract method 0xdc460658.
//
// Solidity: function swap_from_synth(uint256 _token_id, address _to, uint256 _amount, uint256 _expected, address _receiver) returns(uint256)
func (_Main *MainTransactorSession) SwapFromSynth0(_token_id *big.Int, _to common.Address, _amount *big.Int, _expected *big.Int, _receiver common.Address) (*types.Transaction, error) {
	return _Main.Contract.SwapFromSynth0(&_Main.TransactOpts, _token_id, _to, _amount, _expected, _receiver)
}

// SwapIntoSynth is a paid mutator transaction binding the contract method 0x489c7c7c.
//
// Solidity: function swap_into_synth(address _from, address _synth, uint256 _amount, uint256 _expected) payable returns(uint256)
func (_Main *MainTransactor) SwapIntoSynth(opts *bind.TransactOpts, _from common.Address, _synth common.Address, _amount *big.Int, _expected *big.Int) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "swap_into_synth", _from, _synth, _amount, _expected)
}

// SwapIntoSynth is a paid mutator transaction binding the contract method 0x489c7c7c.
//
// Solidity: function swap_into_synth(address _from, address _synth, uint256 _amount, uint256 _expected) payable returns(uint256)
func (_Main *MainSession) SwapIntoSynth(_from common.Address, _synth common.Address, _amount *big.Int, _expected *big.Int) (*types.Transaction, error) {
	return _Main.Contract.SwapIntoSynth(&_Main.TransactOpts, _from, _synth, _amount, _expected)
}

// SwapIntoSynth is a paid mutator transaction binding the contract method 0x489c7c7c.
//
// Solidity: function swap_into_synth(address _from, address _synth, uint256 _amount, uint256 _expected) payable returns(uint256)
func (_Main *MainTransactorSession) SwapIntoSynth(_from common.Address, _synth common.Address, _amount *big.Int, _expected *big.Int) (*types.Transaction, error) {
	return _Main.Contract.SwapIntoSynth(&_Main.TransactOpts, _from, _synth, _amount, _expected)
}

// SwapIntoSynth0 is a paid mutator transaction binding the contract method 0xefc68b20.
//
// Solidity: function swap_into_synth(address _from, address _synth, uint256 _amount, uint256 _expected, address _receiver) payable returns(uint256)
func (_Main *MainTransactor) SwapIntoSynth0(opts *bind.TransactOpts, _from common.Address, _synth common.Address, _amount *big.Int, _expected *big.Int, _receiver common.Address) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "swap_into_synth0", _from, _synth, _amount, _expected, _receiver)
}

// SwapIntoSynth0 is a paid mutator transaction binding the contract method 0xefc68b20.
//
// Solidity: function swap_into_synth(address _from, address _synth, uint256 _amount, uint256 _expected, address _receiver) payable returns(uint256)
func (_Main *MainSession) SwapIntoSynth0(_from common.Address, _synth common.Address, _amount *big.Int, _expected *big.Int, _receiver common.Address) (*types.Transaction, error) {
	return _Main.Contract.SwapIntoSynth0(&_Main.TransactOpts, _from, _synth, _amount, _expected, _receiver)
}

// SwapIntoSynth0 is a paid mutator transaction binding the contract method 0xefc68b20.
//
// Solidity: function swap_into_synth(address _from, address _synth, uint256 _amount, uint256 _expected, address _receiver) payable returns(uint256)
func (_Main *MainTransactorSession) SwapIntoSynth0(_from common.Address, _synth common.Address, _amount *big.Int, _expected *big.Int, _receiver common.Address) (*types.Transaction, error) {
	return _Main.Contract.SwapIntoSynth0(&_Main.TransactOpts, _from, _synth, _amount, _expected, _receiver)
}

// SwapIntoSynth1 is a paid mutator transaction binding the contract method 0xfd22d70f.
//
// Solidity: function swap_into_synth(address _from, address _synth, uint256 _amount, uint256 _expected, address _receiver, uint256 _existing_token_id) payable returns(uint256)
func (_Main *MainTransactor) SwapIntoSynth1(opts *bind.TransactOpts, _from common.Address, _synth common.Address, _amount *big.Int, _expected *big.Int, _receiver common.Address, _existing_token_id *big.Int) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "swap_into_synth1", _from, _synth, _amount, _expected, _receiver, _existing_token_id)
}

// SwapIntoSynth1 is a paid mutator transaction binding the contract method 0xfd22d70f.
//
// Solidity: function swap_into_synth(address _from, address _synth, uint256 _amount, uint256 _expected, address _receiver, uint256 _existing_token_id) payable returns(uint256)
func (_Main *MainSession) SwapIntoSynth1(_from common.Address, _synth common.Address, _amount *big.Int, _expected *big.Int, _receiver common.Address, _existing_token_id *big.Int) (*types.Transaction, error) {
	return _Main.Contract.SwapIntoSynth1(&_Main.TransactOpts, _from, _synth, _amount, _expected, _receiver, _existing_token_id)
}

// SwapIntoSynth1 is a paid mutator transaction binding the contract method 0xfd22d70f.
//
// Solidity: function swap_into_synth(address _from, address _synth, uint256 _amount, uint256 _expected, address _receiver, uint256 _existing_token_id) payable returns(uint256)
func (_Main *MainTransactorSession) SwapIntoSynth1(_from common.Address, _synth common.Address, _amount *big.Int, _expected *big.Int, _receiver common.Address, _existing_token_id *big.Int) (*types.Transaction, error) {
	return _Main.Contract.SwapIntoSynth1(&_Main.TransactOpts, _from, _synth, _amount, _expected, _receiver, _existing_token_id)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address _from, address _to, uint256 _token_id) returns()
func (_Main *MainTransactor) TransferFrom(opts *bind.TransactOpts, _from common.Address, _to common.Address, _token_id *big.Int) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "transferFrom", _from, _to, _token_id)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address _from, address _to, uint256 _token_id) returns()
func (_Main *MainSession) TransferFrom(_from common.Address, _to common.Address, _token_id *big.Int) (*types.Transaction, error) {
	return _Main.Contract.TransferFrom(&_Main.TransactOpts, _from, _to, _token_id)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address _from, address _to, uint256 _token_id) returns()
func (_Main *MainTransactorSession) TransferFrom(_from common.Address, _to common.Address, _token_id *big.Int) (*types.Transaction, error) {
	return _Main.Contract.TransferFrom(&_Main.TransactOpts, _from, _to, _token_id)
}

// Withdraw is a paid mutator transaction binding the contract method 0x441a3e70.
//
// Solidity: function withdraw(uint256 _token_id, uint256 _amount) returns(uint256)
func (_Main *MainTransactor) Withdraw(opts *bind.TransactOpts, _token_id *big.Int, _amount *big.Int) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "withdraw", _token_id, _amount)
}

// Withdraw is a paid mutator transaction binding the contract method 0x441a3e70.
//
// Solidity: function withdraw(uint256 _token_id, uint256 _amount) returns(uint256)
func (_Main *MainSession) Withdraw(_token_id *big.Int, _amount *big.Int) (*types.Transaction, error) {
	return _Main.Contract.Withdraw(&_Main.TransactOpts, _token_id, _amount)
}

// Withdraw is a paid mutator transaction binding the contract method 0x441a3e70.
//
// Solidity: function withdraw(uint256 _token_id, uint256 _amount) returns(uint256)
func (_Main *MainTransactorSession) Withdraw(_token_id *big.Int, _amount *big.Int) (*types.Transaction, error) {
	return _Main.Contract.Withdraw(&_Main.TransactOpts, _token_id, _amount)
}

// Withdraw0 is a paid mutator transaction binding the contract method 0x0ad58d2f.
//
// Solidity: function withdraw(uint256 _token_id, uint256 _amount, address _receiver) returns(uint256)
func (_Main *MainTransactor) Withdraw0(opts *bind.TransactOpts, _token_id *big.Int, _amount *big.Int, _receiver common.Address) (*types.Transaction, error) {
	return _Main.contract.Transact(opts, "withdraw0", _token_id, _amount, _receiver)
}

// Withdraw0 is a paid mutator transaction binding the contract method 0x0ad58d2f.
//
// Solidity: function withdraw(uint256 _token_id, uint256 _amount, address _receiver) returns(uint256)
func (_Main *MainSession) Withdraw0(_token_id *big.Int, _amount *big.Int, _receiver common.Address) (*types.Transaction, error) {
	return _Main.Contract.Withdraw0(&_Main.TransactOpts, _token_id, _amount, _receiver)
}

// Withdraw0 is a paid mutator transaction binding the contract method 0x0ad58d2f.
//
// Solidity: function withdraw(uint256 _token_id, uint256 _amount, address _receiver) returns(uint256)
func (_Main *MainTransactorSession) Withdraw0(_token_id *big.Int, _amount *big.Int, _receiver common.Address) (*types.Transaction, error) {
	return _Main.Contract.Withdraw0(&_Main.TransactOpts, _token_id, _amount, _receiver)
}

// MainApprovalIterator is returned from FilterApproval and is used to iterate over the raw logs and unpacked data for Approval events raised by the Main contract.
type MainApprovalIterator struct {
	Event *MainApproval // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *MainApprovalIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(MainApproval)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(MainApproval)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *MainApprovalIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *MainApprovalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// MainApproval represents a Approval event raised by the Main contract.
type MainApproval struct {
	Owner    common.Address
	Approved common.Address
	TokenId  *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApproval is a free log retrieval operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed token_id)
func (_Main *MainFilterer) FilterApproval(opts *bind.FilterOpts, owner []common.Address, approved []common.Address, token_id []*big.Int) (*MainApprovalIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var approvedRule []interface{}
	for _, approvedItem := range approved {
		approvedRule = append(approvedRule, approvedItem)
	}
	var token_idRule []interface{}
	for _, token_idItem := range token_id {
		token_idRule = append(token_idRule, token_idItem)
	}

	logs, sub, err := _Main.contract.FilterLogs(opts, "Approval", ownerRule, approvedRule, token_idRule)
	if err != nil {
		return nil, err
	}
	return &MainApprovalIterator{contract: _Main.contract, event: "Approval", logs: logs, sub: sub}, nil
}

// WatchApproval is a free log subscription operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed token_id)
func (_Main *MainFilterer) WatchApproval(opts *bind.WatchOpts, sink chan<- *MainApproval, owner []common.Address, approved []common.Address, token_id []*big.Int) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var approvedRule []interface{}
	for _, approvedItem := range approved {
		approvedRule = append(approvedRule, approvedItem)
	}
	var token_idRule []interface{}
	for _, token_idItem := range token_id {
		token_idRule = append(token_idRule, token_idItem)
	}

	logs, sub, err := _Main.contract.WatchLogs(opts, "Approval", ownerRule, approvedRule, token_idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(MainApproval)
				if err := _Main.contract.UnpackLog(event, "Approval", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApproval is a log parse operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed token_id)
func (_Main *MainFilterer) ParseApproval(log types.Log) (*MainApproval, error) {
	event := new(MainApproval)
	if err := _Main.contract.UnpackLog(event, "Approval", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// MainApprovalForAllIterator is returned from FilterApprovalForAll and is used to iterate over the raw logs and unpacked data for ApprovalForAll events raised by the Main contract.
type MainApprovalForAllIterator struct {
	Event *MainApprovalForAll // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *MainApprovalForAllIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(MainApprovalForAll)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(MainApprovalForAll)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *MainApprovalForAllIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *MainApprovalForAllIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// MainApprovalForAll represents a ApprovalForAll event raised by the Main contract.
type MainApprovalForAll struct {
	Owner    common.Address
	Operator common.Address
	Approved bool
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApprovalForAll is a free log retrieval operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_Main *MainFilterer) FilterApprovalForAll(opts *bind.FilterOpts, owner []common.Address, operator []common.Address) (*MainApprovalForAllIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var operatorRule []interface{}
	for _, operatorItem := range operator {
		operatorRule = append(operatorRule, operatorItem)
	}

	logs, sub, err := _Main.contract.FilterLogs(opts, "ApprovalForAll", ownerRule, operatorRule)
	if err != nil {
		return nil, err
	}
	return &MainApprovalForAllIterator{contract: _Main.contract, event: "ApprovalForAll", logs: logs, sub: sub}, nil
}

// WatchApprovalForAll is a free log subscription operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_Main *MainFilterer) WatchApprovalForAll(opts *bind.WatchOpts, sink chan<- *MainApprovalForAll, owner []common.Address, operator []common.Address) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var operatorRule []interface{}
	for _, operatorItem := range operator {
		operatorRule = append(operatorRule, operatorItem)
	}

	logs, sub, err := _Main.contract.WatchLogs(opts, "ApprovalForAll", ownerRule, operatorRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(MainApprovalForAll)
				if err := _Main.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApprovalForAll is a log parse operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_Main *MainFilterer) ParseApprovalForAll(log types.Log) (*MainApprovalForAll, error) {
	event := new(MainApprovalForAll)
	if err := _Main.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// MainNewSettlerIterator is returned from FilterNewSettler and is used to iterate over the raw logs and unpacked data for NewSettler events raised by the Main contract.
type MainNewSettlerIterator struct {
	Event *MainNewSettler // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *MainNewSettlerIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(MainNewSettler)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(MainNewSettler)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *MainNewSettlerIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *MainNewSettlerIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// MainNewSettler represents a NewSettler event raised by the Main contract.
type MainNewSettler struct {
	Addr common.Address
	Raw  types.Log // Blockchain specific contextual infos
}

// FilterNewSettler is a free log retrieval operation binding the contract event 0x9663d922bbc250e8d974501d76ed29effb34ff751262543efd9d16393fb272ac.
//
// Solidity: event NewSettler(address addr)
func (_Main *MainFilterer) FilterNewSettler(opts *bind.FilterOpts) (*MainNewSettlerIterator, error) {

	logs, sub, err := _Main.contract.FilterLogs(opts, "NewSettler")
	if err != nil {
		return nil, err
	}
	return &MainNewSettlerIterator{contract: _Main.contract, event: "NewSettler", logs: logs, sub: sub}, nil
}

// WatchNewSettler is a free log subscription operation binding the contract event 0x9663d922bbc250e8d974501d76ed29effb34ff751262543efd9d16393fb272ac.
//
// Solidity: event NewSettler(address addr)
func (_Main *MainFilterer) WatchNewSettler(opts *bind.WatchOpts, sink chan<- *MainNewSettler) (event.Subscription, error) {

	logs, sub, err := _Main.contract.WatchLogs(opts, "NewSettler")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(MainNewSettler)
				if err := _Main.contract.UnpackLog(event, "NewSettler", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewSettler is a log parse operation binding the contract event 0x9663d922bbc250e8d974501d76ed29effb34ff751262543efd9d16393fb272ac.
//
// Solidity: event NewSettler(address addr)
func (_Main *MainFilterer) ParseNewSettler(log types.Log) (*MainNewSettler, error) {
	event := new(MainNewSettler)
	if err := _Main.contract.UnpackLog(event, "NewSettler", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// MainNewSynthIterator is returned from FilterNewSynth and is used to iterate over the raw logs and unpacked data for NewSynth events raised by the Main contract.
type MainNewSynthIterator struct {
	Event *MainNewSynth // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *MainNewSynthIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(MainNewSynth)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(MainNewSynth)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *MainNewSynthIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *MainNewSynthIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// MainNewSynth represents a NewSynth event raised by the Main contract.
type MainNewSynth struct {
	Synth common.Address
	Pool  common.Address
	Raw   types.Log // Blockchain specific contextual infos
}

// FilterNewSynth is a free log retrieval operation binding the contract event 0x9f8815bba4c2fe0a80beb8f1257105a9bef3466417d3d962974b6be33a2a8a38.
//
// Solidity: event NewSynth(address synth, address pool)
func (_Main *MainFilterer) FilterNewSynth(opts *bind.FilterOpts) (*MainNewSynthIterator, error) {

	logs, sub, err := _Main.contract.FilterLogs(opts, "NewSynth")
	if err != nil {
		return nil, err
	}
	return &MainNewSynthIterator{contract: _Main.contract, event: "NewSynth", logs: logs, sub: sub}, nil
}

// WatchNewSynth is a free log subscription operation binding the contract event 0x9f8815bba4c2fe0a80beb8f1257105a9bef3466417d3d962974b6be33a2a8a38.
//
// Solidity: event NewSynth(address synth, address pool)
func (_Main *MainFilterer) WatchNewSynth(opts *bind.WatchOpts, sink chan<- *MainNewSynth) (event.Subscription, error) {

	logs, sub, err := _Main.contract.WatchLogs(opts, "NewSynth")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(MainNewSynth)
				if err := _Main.contract.UnpackLog(event, "NewSynth", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewSynth is a log parse operation binding the contract event 0x9f8815bba4c2fe0a80beb8f1257105a9bef3466417d3d962974b6be33a2a8a38.
//
// Solidity: event NewSynth(address synth, address pool)
func (_Main *MainFilterer) ParseNewSynth(log types.Log) (*MainNewSynth, error) {
	event := new(MainNewSynth)
	if err := _Main.contract.UnpackLog(event, "NewSynth", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// MainTokenUpdateIterator is returned from FilterTokenUpdate and is used to iterate over the raw logs and unpacked data for TokenUpdate events raised by the Main contract.
type MainTokenUpdateIterator struct {
	Event *MainTokenUpdate // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *MainTokenUpdateIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(MainTokenUpdate)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(MainTokenUpdate)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *MainTokenUpdateIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *MainTokenUpdateIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// MainTokenUpdate represents a TokenUpdate event raised by the Main contract.
type MainTokenUpdate struct {
	TokenId           *big.Int
	Owner             common.Address
	Synth             common.Address
	UnderlyingBalance *big.Int
	Raw               types.Log // Blockchain specific contextual infos
}

// FilterTokenUpdate is a free log retrieval operation binding the contract event 0x72946db909d02e0ff93d1e9ff43433f9740b00add96b36bd7df362987648bc8d.
//
// Solidity: event TokenUpdate(uint256 indexed token_id, address indexed owner, address indexed synth, uint256 underlying_balance)
func (_Main *MainFilterer) FilterTokenUpdate(opts *bind.FilterOpts, token_id []*big.Int, owner []common.Address, synth []common.Address) (*MainTokenUpdateIterator, error) {

	var token_idRule []interface{}
	for _, token_idItem := range token_id {
		token_idRule = append(token_idRule, token_idItem)
	}
	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var synthRule []interface{}
	for _, synthItem := range synth {
		synthRule = append(synthRule, synthItem)
	}

	logs, sub, err := _Main.contract.FilterLogs(opts, "TokenUpdate", token_idRule, ownerRule, synthRule)
	if err != nil {
		return nil, err
	}
	return &MainTokenUpdateIterator{contract: _Main.contract, event: "TokenUpdate", logs: logs, sub: sub}, nil
}

// WatchTokenUpdate is a free log subscription operation binding the contract event 0x72946db909d02e0ff93d1e9ff43433f9740b00add96b36bd7df362987648bc8d.
//
// Solidity: event TokenUpdate(uint256 indexed token_id, address indexed owner, address indexed synth, uint256 underlying_balance)
func (_Main *MainFilterer) WatchTokenUpdate(opts *bind.WatchOpts, sink chan<- *MainTokenUpdate, token_id []*big.Int, owner []common.Address, synth []common.Address) (event.Subscription, error) {

	var token_idRule []interface{}
	for _, token_idItem := range token_id {
		token_idRule = append(token_idRule, token_idItem)
	}
	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var synthRule []interface{}
	for _, synthItem := range synth {
		synthRule = append(synthRule, synthItem)
	}

	logs, sub, err := _Main.contract.WatchLogs(opts, "TokenUpdate", token_idRule, ownerRule, synthRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(MainTokenUpdate)
				if err := _Main.contract.UnpackLog(event, "TokenUpdate", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokenUpdate is a log parse operation binding the contract event 0x72946db909d02e0ff93d1e9ff43433f9740b00add96b36bd7df362987648bc8d.
//
// Solidity: event TokenUpdate(uint256 indexed token_id, address indexed owner, address indexed synth, uint256 underlying_balance)
func (_Main *MainFilterer) ParseTokenUpdate(log types.Log) (*MainTokenUpdate, error) {
	event := new(MainTokenUpdate)
	if err := _Main.contract.UnpackLog(event, "TokenUpdate", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// MainTransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the Main contract.
type MainTransferIterator struct {
	Event *MainTransfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *MainTransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(MainTransfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(MainTransfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *MainTransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *MainTransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// MainTransfer represents a Transfer event raised by the Main contract.
type MainTransfer struct {
	Sender   common.Address
	Receiver common.Address
	TokenId  *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed sender, address indexed receiver, uint256 indexed token_id)
func (_Main *MainFilterer) FilterTransfer(opts *bind.FilterOpts, sender []common.Address, receiver []common.Address, token_id []*big.Int) (*MainTransferIterator, error) {

	var senderRule []interface{}
	for _, senderItem := range sender {
		senderRule = append(senderRule, senderItem)
	}
	var receiverRule []interface{}
	for _, receiverItem := range receiver {
		receiverRule = append(receiverRule, receiverItem)
	}
	var token_idRule []interface{}
	for _, token_idItem := range token_id {
		token_idRule = append(token_idRule, token_idItem)
	}

	logs, sub, err := _Main.contract.FilterLogs(opts, "Transfer", senderRule, receiverRule, token_idRule)
	if err != nil {
		return nil, err
	}
	return &MainTransferIterator{contract: _Main.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed sender, address indexed receiver, uint256 indexed token_id)
func (_Main *MainFilterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *MainTransfer, sender []common.Address, receiver []common.Address, token_id []*big.Int) (event.Subscription, error) {

	var senderRule []interface{}
	for _, senderItem := range sender {
		senderRule = append(senderRule, senderItem)
	}
	var receiverRule []interface{}
	for _, receiverItem := range receiver {
		receiverRule = append(receiverRule, receiverItem)
	}
	var token_idRule []interface{}
	for _, token_idItem := range token_id {
		token_idRule = append(token_idRule, token_idItem)
	}

	logs, sub, err := _Main.contract.WatchLogs(opts, "Transfer", senderRule, receiverRule, token_idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(MainTransfer)
				if err := _Main.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed sender, address indexed receiver, uint256 indexed token_id)
func (_Main *MainFilterer) ParseTransfer(log types.Log) (*MainTransfer, error) {
	event := new(MainTransfer)
	if err := _Main.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
