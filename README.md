# nft-scan

## Asset Model
The core data model in the NFTScan API is the asset, which represents a unique digital item whose ownership is managed by the blockchain.

Here is an overview of the fields contained in an asset.

Field Name|Description|Example 
:---|:---|:---|
contract_address|	The contract address|	0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d
contract_name|	The name of the contract|	BoredApeYachtClub
contract_token_id|	The token ID of the NFT in Hex|	0x0000000000000000000000000000000000000000000000000000000000000001
token_id| The token ID of the NFT in Number|	1
erc_type|	The erc type of the NFT (erc721 or erc1155)|	erc721
amount| The amount of the NFT (Default 1 for the ERC-721 NFT)|	1
minter|	The user address who minted the item|	0xaba7161a7fb69c88e16ed9f455ce62b791ee4d03
owner| The user address who owns the item now (null if the item is an ERC-1155 NFT)|	0xaba7161a7fb69c88e16ed9f455ce62b791ee4d03
mint_timestamp|	The timestamp in milliseconds when the item was minted|	1619133220000
mint_transaction_hash| The transaction hash when the item was minted|	0xcfb197f62ec5c7f0e71a11ec0c4a0e394a3aa41db5386e85526f86c84b3f2796
mint_price|	The price when the item was minted (null if the item is an ERC-1155 NFT)|	0.7
token_uri| The token URI|	https://dweb.link/ipfs/QmeSjSinHpPnmXmspMjwiXyN6zS4E9zccariGR3jxcaWtq/1
metadata_json|	The metadata in JSON format|	{"image":"ipfs://QmPbxeGcXhYQQNgsC6a36dDyYUcHgMLnGKnF8pVFmGsvqi","attributes":[{"trait_type":"Mouth","value":"Grin"},{"trait_type":"Clothes","value":"Vietnam Jacket"},{"trait_type":"Background","value":"Orange"},{"trait_type":"Eyes","value":"Blue Beams"},{"trait_type":"Fur","value":"Robot"}]}
name|	The name|	#1
content_type|	The content type|	image/png
content_uri|	The content URI to display|	QmPbxeGcXhYQQNgsC6a36dDyYUcHgMLnGKnF8pVFmGsvqi
image_uri|	The image URI to display|	QmPbxeGcXhYQQNgsC6a36dDyYUcHgMLnGKnF8pVFmGsvqi
external_link|	External link to the original website|
latest_trade_price|	The latest trade price for the item|	38.77
latest_trade_timestamp|	The latest trade timestamp in milliseconds for the item|	1652567645000
nftscan_id|	The unique ID generated for the item by NFTScan|	NSB313FC3396961075
nftscan_uri|	The image URI stored by NFTScan (null if not stored by NFTScan)|	https://dm2zb8bwza29x.cloudfront.net/0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d/0x0000000000000000000000000000000000000000000000000000000000000001.png
attributes|	List of attributes|	[{"attribute_name":"Mouth","attribute_value":"Grin","percentage":"7.13%"},{"attribute_name":"Clothes","attribute_value":"Vietnam Jacket","percentage":"2.24%"},{"attribute_name":"Background","attribute_value":"Orange","percentage":"12.73%"},{"attribute_name":"Eyes","attribute_value":"Blue Beams","percentage":"0.49%"},{"attribute_name":"Fur","attribute_value":"Robot","percentage":"2.65%"}]

## Transaction Model
The transaction model represents one transaction for an NFT asset on the blockchain.

Here is an overview of the fields contained in a transaction.

Field Name|Description|Example
:---|:---|:---|
hash|	Transaction hash|	0xe93e858f9330afa4581e260198195623aa7f5cd2809012440ea291d317be9f2f
from|	The param from of the transaction|	0xaba7161a7fb69c88e16ed9f455ce62b791ee4d03
to|	The param to of the transaction|	0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d
block_number|	The block number|	12344148
block_hash|	The block hash|	0xa367b094366bc68de295ae6167797afc55eeb8383869363a6d7eb143c31d8274
gas_price|	The gas price in Hex|	0xab5d04c00
gas_used|	How much gas was used in the transaction in Hex|	0x197c3
gas_fee|	The transaction fee in Decimal|	0.004801802
timestamp|	The timestamp in milliseconds of the transaction|	1619133220000
contract_address|	The contract address|	0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d
contract_name|	The name of the contract|	BoredApeYachtClub
contract_token_id|	The token ID of the NFT in Hex|	0x0000000000000000000000000000000000000000000000000000000000000001
token_id|	The token ID of the NFT in Number|	1
erc_type|	The erc type of the NFT (erc721 or erc1155)|	erc721
send|	The user address who sent the NFT|	0xaba7161a7fb69c88e16ed9f455ce62b791ee4d03
receive|	The user address who received the NFT|	0x46efbaedc92067e6d60e84ed6395099723252496
amount|	The amount of the NFT (Default 1 for the ERC-721 NFT)|	1
trade_value|	The trade value of the transaction in Hex|	0x0
trade_price|	The trade price of the transaction in Number|	0.1
trade_symbol|	The trade symbol of the trade price|	ETH
event_type|	The NFT event type of the transaction (Mint, Transfer, Sale)|	Transfer
exchange_name|	The NFT trading exchange name (Only for Sale)|	Opensea
nftscan_tx_id|	The unique ID generated for the transaction record by NFTScan|	1234414801540001


## Collection Model
The collection model represents an NFT contract address on the blockchain.

Here is an overview of the fields contained in a collection.

Field Name|Description|Example
:---|:---|:---|
contract_address|	The contract address|	0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d
name|	The name|	BoredApeYachtClub
symbol|	The symbol|	BAYC
description|	The description|	The Bored Ape Yacht Club is a collection of 10,000 unique Bored Ape NFTs— unique digital collectibles living on the Ethereum blockchain. Your Bored Ape doubles as your Yacht Club membership card, and grants access to members-only benefits, the first of which is access to THE BATHROOM, a collaborative graffiti board. Future areas and perks can be unlocked by the community through roadmap activation. Visit www.BoredApeYachtClub.com for more details.
website|	The website|	http://www.boredapeyachtclub.com/
email|	The email|
twitter|	The twitter|	BoredApeYC
discord|	The discord|	https://discord.gg/3P5K3dzgdB
telegram|	The telegram|	BoredApeYC
github|	The github|
instagram|	The instagram|
medium|	The medium|
logo_url|	The logo URL|	https://logo.nftscan.com/logo/0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d.png
banner_url|	The banner URL|	https://logo.nftscan.com/banner/0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d.png
featured_url|	The featured URL|	https://lh3.googleusercontent.com/RBX3jwgykdaQO3rjTcKNf5OVwdukKO46oOAV3zZeiaMb8VER6cKxPDTdGZQdfWcDou75A8KtVZWM_fEnHG4d4q6Um8MeZIlw79BpWPA=s300
large_image_url|	The large image URL|	https://lh3.googleusercontent.com/RBX3jwgykdaQO3rjTcKNf5OVwdukKO46oOAV3zZeiaMb8VER6cKxPDTdGZQdfWcDou75A8KtVZWM_fEnHG4d4q6Um8MeZIlw79BpWPA=s300
attributes|	The attributes distribution of the collection|
erc_type|	The erc type of the collection|	erc721
deploy_block_number|	The block number when the contract was deployed|	12292922
owner|	The user address who owns the contract|	0xaba7161a7fb69c88e16ed9f455ce62b791ee4d03
verified|	Whether the collection is verified on WhatToFarm|	false
royalty|	The royalty for the owner in basis point|	250
items_total|	How many items for the collection|	10000
owners_total|	How many owners for the collection|	6271
floor_price|	The floor price|	91.00
price_symbol|	The price symbol of the collection|	ETH
collections_with_same_name|	The contract address of collections with the same| name	